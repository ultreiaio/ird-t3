/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access.type;

/**
 * Un type pour l'import access qui permet de récupérer un {@link Boolean}, à
 * partir d'un {@code int}.
 * <p/>
 * Voici les valeurs utilisées :
 * <ul>
 * <li>0 = {@code false}</li>
 * <li>1 = {@code true}</li>
 * </ul>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class IntToBoolean {

    /** the int value to transform into boolean value. */
    protected final Integer intValue;

    public IntToBoolean(Integer intValue) {
        this.intValue = intValue;
    }

    /**
     * Gets the int value.
     *
     * @return the int value
     */
    public Integer getIntValue() {
        return intValue;
    }

    /**
     * Gets the boolean value :
     * null stays {@code null}, 0 is {@code true} and 1 is {@code false}
     *
     * @return the boolean value from the int value
     */
    public Boolean getBooleanvalue() {
        if (intValue == null) {
            return null;
        }
        if (intValue == 0) {
            return false;
        }
        if (intValue == 1) {
            return true;
        }

        return null;
    }
}
