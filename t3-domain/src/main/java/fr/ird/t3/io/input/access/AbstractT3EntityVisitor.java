/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AccessEntityVisitor;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.util.EntityOperator;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Abstract visitor to fill a data entity.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractT3EntityVisitor extends AccessEntityVisitor<T3EntityEnum, T3AccessEntityMeta, T3AccessEntity> {

    private static final Log log = LogFactory.getLog(AbstractT3EntityVisitor.class);

    protected final T3AccessDataSource dataSource;
    /**
     * la ligne en cours de lecture dans la base access (correspond à l'entité en cours de parcours).
     */
    protected Map<String, Object> row;
    protected ReferenceEntityMap referentiel;
    boolean deepVisit;
    private Map<String, T3ReferenceEntity> referentielCache;

    AbstractT3EntityVisitor(T3AccessDataSource dataSource, ReferenceEntityMap referentiel) {
        this.referentiel = referentiel;
        this.dataSource = dataSource;
        this.referentielCache = new TreeMap<>();
    }

    /**
     * Obtain a reference key from his natural properties.
     *
     * @param type       type of entity
     * @param properties values to add in key (means natural ids in fact)
     * @return the build key
     */
    private static String getReferenceKey(Class<?> type, Serializable... properties) {
        StringBuilder result = new StringBuilder(type.toString());
        for (Serializable serializable : properties) {
            result.append(":");
            if (serializable instanceof TopiaEntity) {
                result.append(((TopiaEntity) serializable).getTopiaId());
            } else {
                result.append(serializable);
            }
        }
        log.debug(String.format("Looking for reference %s", result.toString()));
        return result.toString();
    }

    public static String toString(T3AccessEntity entity) {
        T3AccessEntityMeta meta = entity.getMeta();
        return String.format("%s - %d", meta.getType(), entity.getRowId());
    }

    public boolean isStrictCheck() {
        return strictCheck;
    }

    public void setStrictCheck(boolean strictCheck) {
        this.strictCheck = strictCheck;
    }

    public boolean isDeepVisit() {
        return deepVisit;
    }

    public <E extends TopiaEntity> void doVisit(E entity, boolean deepVisit) throws TopiaException {
        this.deepVisit = deepVisit;
        entity.accept(this);
        log.debug(String.format("Visited %s", toString((T3AccessEntity) entity)));
    }

    @Override
    public void onStart(T3AccessEntity entity, T3AccessEntityMeta meta) {
        log.debug(String.format("%s - %d", meta.getType(), entity.getRowId()));
        // get the line which contains data about this entity
        row = dataSource.getTableDataRow(meta, entity.getRowId());
    }

    @Override
    public void onEnd(T3AccessEntity entity, T3AccessEntityMeta meta) {
        log.debug(String.format("%s - %d", meta.getType(), entity.getRowId()));
        row = null;
    }

    public void clear() {
        row = null;
        if (referentielCache != null) {
            referentielCache.clear();
            referentielCache = null;
        }
        if (referentiel != null) {
            referentiel = null;
        }
    }

    @SuppressWarnings({"unchecked"})
    protected TopiaEntity getReferenceEntity(T3AccessEntityMeta compoMeta, Serializable newValue) {
        T3EntityEnum compoType = compoMeta.getType();
        String cacheKey = getReferenceKey(compoType.getContract(), newValue);
        log.debug(String.format("Looking for reference %s", cacheKey));
        TopiaEntity compoEntity = referentielCache.get(cacheKey);
        if (compoEntity == null) {
            // not found in cache, must compute the cache
            List<? extends T3ReferenceEntity> entities = referentiel.get(compoType);
            EntityOperator<T3ReferenceEntity> operator = T3EntityEnum.getOperator((Class<T3ReferenceEntity>) compoType.getContract());
            String naturalId = compoMeta.getTopiaNaturalId();
            for (T3ReferenceEntity topiaEntity : entities) {
                Serializable key = (Serializable) operator.get(naturalId, topiaEntity);
                referentielCache.put(getReferenceKey(compoType.getContract(), key), topiaEntity);
                if (newValue.equals(key)) {
                    compoEntity = topiaEntity;
                }
            }
        }
        return compoEntity;
    }

    @SuppressWarnings({"unchecked"})
    public TopiaEntity getReferenceEntity(T3AccessEntityMeta compoMeta, String[] properties, Serializable... newValue) {
        T3EntityEnum compoType = compoMeta.getType();
        Class<? extends TopiaEntity> entityType = compoType.getContract();
        String cacheKey = getReferenceKey(entityType, newValue);
        TopiaEntity compoEntity = referentielCache.get(cacheKey);

        if (compoEntity == null) {

            // not found in cache, must compute the cache
            List<? extends T3ReferenceEntity> entities = referentiel.get(compoType);

            EntityOperator<T3ReferenceEntity> operator =
                    T3EntityEnum.getOperator((Class<T3ReferenceEntity>) entityType);

            for (T3ReferenceEntity topiaEntity : entities) {
                int length = properties.length;
                Serializable[] newProperties = new Serializable[length];
                for (int i = 0; i < length; i++) {
                    String property = properties[i];
                    Object key = operator.get(property, topiaEntity);
                    newProperties[i] = (Serializable) key;
                }
                String newKey = getReferenceKey(entityType,
                        newProperties);

                referentielCache.put(newKey, topiaEntity);

                if (cacheKey.equals(newKey)) {

                    // found required entity
                    compoEntity = topiaEntity;
                }
            }
        }
        return compoEntity;
    }

    void acceptEntity(TopiaEntity child) {
        try {
            child.accept(this);
        } catch (TopiaException e) {
            // on ne devrait pas avoir de tel exception (on utilise pas topia)
            throw new IllegalStateException("Could not accept " + child, e);
        }
    }

    protected Object[] getPKey(T3EntityEnum anEnum) {

        T3AccessEntityMeta meta = dataSource.getMeta(anEnum);

        List<String> metaPkeys = meta.getPKeys();

        Object[] pKey = dataSource.getPKey(metaPkeys, row);
        if (log.isDebugEnabled()) {
            log.debug(anEnum + " pKey : " + metaPkeys +
                    " = " + Arrays.toString(pKey));
        }
        return pKey;
    }

    protected Map<String, T3ReferenceEntity> getReferentielCache() {
        return referentielCache;
    }

}
