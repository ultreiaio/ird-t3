/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessEntityMeta;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.T3EntityMap;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.io.input.MissingForeignKey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.util.EntityOperator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * To visit and fill properly an data entity.
 * <p/>
 * If visitor has the flag {@link #deepVisit} is set to {@code false}, then
 * only the pKey will be filled, otherwise all fields, compositions or
 * associations are filled.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3DataEntityVisitor extends AbstractT3EntityVisitor {

    private static final Log log = LogFactory.getLog(T3DataEntityVisitor.class);

    /** la pile des objets en cours de construction. */
    private final Stack<T3AccessEntity> stack;

    /** la pile des lignes lues en base correspondant aux objets en cours de construction. */
    private final Stack<Map<String, Object>> rows;

    /** Universe of touched references while visiting a data. */
    private T3EntityMap entitiesTouched;

    private List<MissingForeignKey> missingForeignKeys;

    protected T3DataEntityVisitor(T3AccessDataSource dataSource, ReferenceEntityMap referentiel) {
        super(dataSource, referentiel);
        rows = new Stack<>();
        stack = new Stack<>();
    }

    public T3EntityMap getEntitiesTouched() {
        if (entitiesTouched == null) {
            entitiesTouched = new T3EntityMap();
        }
        return entitiesTouched;
    }

    public void resetTripStates() {
        entitiesTouched = null;
        missingForeignKeys = null;
    }

    @Override
    public void onStart(T3AccessEntity entity, T3AccessEntityMeta meta) {
        super.onStart(entity, meta);
        stack.push(entity);
        rows.push(row);
        getEntitiesTouched().addEntity(entity);
    }

    @Override
    public void onEnd(T3AccessEntity entity, T3AccessEntityMeta meta) {
        super.onEnd(entity, meta);
        stack.pop();
        rows.pop();
        if (!rows.isEmpty()) {
            row = rows.peek();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected TopiaEntity getReferenceEntity(T3AccessEntityMeta compoMeta, Serializable newValue) {
        T3ReferenceEntity e = (T3ReferenceEntity) super.getReferenceEntity(compoMeta, newValue);
        if (e == null) {
            // reference entity not found
            // add the entity in touched refs (to be able to know it is
            // missing...)
            try {
                T3ReferenceEntity e2 = (T3ReferenceEntity) compoMeta.getType().getImplementation().getConstructor().newInstance();
                EntityOperator operator = T3EntityEnum.getOperator(compoMeta.getType().getContract());
                operator.set("code", e2, newValue);
                getEntitiesTouched().addUniqueEntity(e2);
            } catch (Exception eee) {
                log.error("Could not create dummy reference entity", eee);
            }
            // still returns null entity
            return null;
        }
        boolean added = getEntitiesTouched().addUniqueEntity(e);
        if (added && log.isDebugEnabled()) {
            log.debug(String.format("Touched reference %s : %d", e.getTopiaId(), e.getCode()));
        }
        return e;
    }

    @Override
    public void onVisitSimpleProperty(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        // just set the property into the entity
        Serializable newValue = getProperty(propertyName, meta, row);
        if (newValue != null) {
            if (log.isDebugEnabled()) {
                String colName = meta.getPropertyColumnName(propertyName);
                log.debug(String.format("get property [%s] (type:%s) (dbcol:%s) = %s", propertyName, type.getName(), colName, newValue));
            }
            entity.setProperty(propertyName, newValue);
        }
    }


    @Override
    public void onVisitComposition(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        if (!deepVisit) {
            return;
        }
        // find the reference entity to add as composition
        Serializable newValue = getProperty(propertyName, meta, row);
        if (newValue == null) {
            return;
        }

        AbstractAccessEntityMeta.PropertyMapping mapping = meta.getPropertyMapping(propertyName);
        Class<?> compositionType = mapping.getType();

        T3AccessEntityMeta compoMeta;

        T3AccessEntityMeta[] compoMetas = dataSource.getMetaForType(compositionType);

        if (compoMetas.length == 0) {
            throw new IllegalStateException(
                    String.format("Skip composition [%s - %s:%s], meta of type [%s] not found...", meta.getType(), propertyName, type, compositionType));
        }

        if (compoMetas.length > 1) {

            // on n'autorise pas d'avoir plusieurs méta à traiter il s'agit d'une reférence sur un référentiel
            throw new IllegalStateException(
                    String.format("Found more than one meta for referentiel type [%s] : %s", compositionType, Arrays.toString(compoMetas)));
        }

        compoMeta = compoMetas[0];
        TopiaEntity compoEntity = getReferenceEntity(compoMeta, newValue);
        if (compoEntity == null) {

            // could not find the correct composition entity
            log.debug(String.format("Could not find referentiel entity for composition [%s:%s:%s] to %s", propertyName, compoMeta.getType(), newValue, entity));
            return;
        }
        log.debug(String.format("Will add composition [%s:%s] to %s", propertyName, compoEntity, entity));
        // then affect it to this entity for the given property
        entity.setProperty(propertyName, compoEntity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onVisitReverseAssociation(String propertyName, T3AccessEntity entity, T3AccessEntityMeta meta) {
        if (!deepVisit) {
            return;
        }
        // on recherche dans la pile des objets en cours de construction
        // l'objet maitre de l'association y est forcement puisque la création de l'entité en cours en dépend.
        T3AccessEntityMeta.AssociationMapping reverse = meta.getReverseAssociationMapping(propertyName);
        Class<? extends TopiaEntity> type = (Class<? extends TopiaEntity>) reverse.getType();

        TopiaEntity parent = getEntityFromStack(type);
        if (parent == null) {
            // ce cas ne devrait jamais arrivé
            throw new IllegalStateException(
                    String.format("Could not find reverse association [%s:%s] for one of his child %s", propertyName, type, entity));
        }
        log.debug(String.format("Will add reverse composition [%s:%s] to %s", propertyName, parent, entity));
        entity.setProperty(propertyName, parent);
    }

    @Override
    public void onVisitAssociation(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        if (!deepVisit) {
            return;
        }
        T3EntityEnum constant = T3EntityEnum.valueOf(type);
        T3AccessEntityMeta[] childMeta = dataSource.getMetaForType(constant.getContract());
        List<TopiaEntity> children = new ArrayList<>();
        TopiaEntity[] tmp;
        for (T3AccessEntityMeta accessEntityMeta : childMeta) {
            tmp = getAssociation(entity, propertyName, meta, accessEntityMeta);
            log.debug(String.format("%s load association [%s] (meta:%s) : %d", toString(entity), constant, accessEntityMeta, tmp.length));
            if (tmp.length > 0) {
                children.addAll(Arrays.asList(tmp));
            }
        }
        if (children.isEmpty()) {
            return;
        }
        for (TopiaEntity child : children) {
            log.debug(String.format("Add association %s", child));
            // on lance la découverte du fils
            acceptEntity(child);
        }
        entity.setAssociationProperty(propertyName, children);
    }

    @Override
    public void clear() {
        super.clear();
        stack.clear();
        rows.clear();
    }

    <P extends TopiaEntity, E extends TopiaEntity, C extends TopiaEntity> ReverseAssociationGetter<P, E, C> newReverseAssociationGetter(
            Class<P> parentType,
            Class<E> type,
            Class<C> childType,
            String reverseAssociation,
            String parentAssociation) {
        return new ReverseAssociationGetter<>(parentType, type, childType, parentAssociation, reverseAssociation);
    }

    public List<MissingForeignKey> getMissingForeignKeys() {
        if (missingForeignKeys == null) {
            missingForeignKeys = new ArrayList<>();
        }
        return missingForeignKeys;
    }

    private <T extends TopiaEntity> T getEntityFromStack(Class<T> type) {
        TopiaEntity entity = null;
        for (TopiaEntity e : stack) {
            if (type.isAssignableFrom(e.getClass())) {
                entity = e;
                break;
            }
        }
        return type.cast(entity);
    }

    private TopiaEntity[] getAssociation(TopiaEntity entity, String propertyName, T3AccessEntityMeta meta, T3AccessEntityMeta childMeta) {
        try {
            return dataSource.loadAssociation(childMeta, meta, ((T3AccessEntity) entity).getPKey());
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Could not obtain association [%s]", propertyName), e);
        }
    }

    protected class ReverseAssociationGetter<P extends TopiaEntity, E extends TopiaEntity, C extends TopiaEntity> {

        protected final Class<E> type;
        final Class<P> parentType;
        final Class<C> childType;
        private final T3EntityEnum typeEnum;
        private final T3EntityEnum childTypeEnum;
        private final EntityOperator<P> parentOperator;
        private final EntityOperator<E> operator;
        private final String parentAssociation;
        private final String reverseAssociation;

        ReverseAssociationGetter(Class<P> parentType,
                                 Class<E> type,
                                 Class<C> childType,
                                 String parentAssociation,
                                 String reverseAssociation) {
            this.parentType = parentType;
            this.type = type;
            this.childType = childType;
            this.reverseAssociation = reverseAssociation;
            this.parentAssociation = parentAssociation;
            this.typeEnum = T3EntityEnum.valueOf(type);
            this.childTypeEnum = T3EntityEnum.valueOf(childType);
            this.parentOperator = T3EntityEnum.getOperator(parentType);
            this.operator = T3EntityEnum.getOperator(type);
        }

        @SuppressWarnings({"unchecked"})
        public void attachReverseAssociation(T3AccessEntity entity) {
            // get child pKey from the pKey of the current entity
            Object[] pKey = getPKey(childTypeEnum);
            // get the parent entity which contains the association
            P parent = getEntityFromStack(parentType);
            // get the association from the given parent
            Collection<? extends TopiaEntity> children = (Collection<? extends TopiaEntity>) parentOperator.get(parentAssociation, parent);
            // get the exact reverse association found by his pKey
            TopiaEntity reverse = getEntityForPKey(pKey, children);
            if (reverse == null) {
                onReverseNotFound(parent, entity, pKey);
            } else {
                E topiaEntity = type.cast(entity);
                operator.set(reverseAssociation, topiaEntity, reverse);
            }
        }

        void onReverseNotFound(P parent, T3AccessEntity entity, Object[] pKey) {
            String message = String.format("Could not find %s %s for %s %s", childType.getSimpleName(), Arrays.toString(pKey), type.getSimpleName(), Arrays.toString(entity.getPKey()));
            log.debug(message);
            MissingForeignKey missingFK = new MissingForeignKey(typeEnum, childTypeEnum, entity.getPKey(), pKey);
            getMissingForeignKeys().add(missingFK);
        }
    }
}
