/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output;

import com.opensymphony.xwork2.LocaleProvider;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.services.T3TopiaPersistenceContextAware;
import java.util.List;

/**
 * The contract to define how to extract data from T3.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public interface T3Output<O extends T3OutputOperation, C extends T3OutputConfiguration> extends T3TopiaPersistenceContextAware, LocaleProvider {

    /**
     * Obtains the output pilot configuration.
     *
     * @return the output pilot configuration
     */
    C getConfiguration();

    /**
     * Gets the messager.
     *
     * @return the messager used to keep messages
     * @see T3Messager
     */
    T3Messager getMessager();

    /**
     * Execute the given {@code operation} on the given trips from t3 database.
     *
     * @param trips     trips from t3 to treate
     * @param operation operation to execute
     * @return the summary of the executed operation
     * @throws Exception if any problem while executing operation
     */
    String executeOperation(List<Trip> trips, O operation) throws Exception;
}
