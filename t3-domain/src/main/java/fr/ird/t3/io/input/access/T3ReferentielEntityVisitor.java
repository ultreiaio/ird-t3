/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessEntityMeta;
import fr.ird.t3.entities.ReferenceEntityMap;
import java.io.Serializable;
import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * To visit and fill properly an reference entity.
 * <p/>
 * If visitor has the flag {@link #deepVisit} is set to {@code false}, then
 * only the pKey will be filled, otherwise all fields or compositions are filled.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3ReferentielEntityVisitor extends AbstractT3EntityVisitor {

    private static final Log log = LogFactory.getLog(T3ReferentielEntityVisitor.class);

    T3ReferentielEntityVisitor(T3AccessDataSource dataSource, ReferenceEntityMap referentiel) {
        super(dataSource, referentiel);
    }

    @Override
    public void onVisitSimpleProperty(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        // just set the property into the entity
        if (!deepVisit) {
            if (!propertyName.equals(meta.getTopiaNaturalId())) {
                // not a deep visit and not a pKey property name, skip visit
                return;
            }
        }
        Serializable newValue = getProperty(propertyName, meta, row);
        if (newValue != null) {
            if (log.isDebugEnabled()) {
                String colName = meta.getPropertyColumnName(propertyName);
                log.debug(String.format("get property [%s] (type:%s) (dbcol:%s) = %s", propertyName, type.getName(), colName, newValue));
            }
            entity.setProperty(propertyName, newValue);
        }
    }

    @Override
    public void onVisitComposition(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        if (!deepVisit) {
            return;
        }
        // find the reference entity to add as composition
        Serializable newValue = getProperty(propertyName, meta, row);
        if (newValue == null) {
            // rien a faire si la composition est nulle
            return;
        }
        AbstractAccessEntityMeta.PropertyMapping mapping = meta.getPropertyMapping(propertyName);
        Class<?> compositionType = mapping.getType();
        T3AccessEntityMeta[] compoMetas = dataSource.getMetaForType(compositionType);
        if (compoMetas.length == 0) {
            log.warn(String.format("Skip composition [%s - %s], meta of type [%s] not found...", meta.getType(), propertyName, compositionType));
            return;
        }
        if (compoMetas.length > 1) {
            // on n'autorise pas d'avoir plusieurs méta à traiter - il s'agit d'une reférence sur un référentiel
            throw new IllegalStateException(
                    String.format("Found more than one meta for referentiel type [%s] : %s", compositionType, Arrays.toString(compoMetas)));
        }
        T3AccessEntityMeta compoMeta = compoMetas[0];
        TopiaEntity compoEntity = getReferenceEntity(compoMeta, newValue);
        if (compoEntity == null) {
            // could not find the correct composition entity
            throw new IllegalStateException(
                    String.format("Could not find reference entity of type [%s] with pKey [%s]", compoMeta.getType(), newValue));
        }
        // then affect it to this entity for the given property
        entity.setProperty(propertyName, compoEntity);
    }

    @Override
    public void onVisitAssociation(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        // no used for reference entity
        throw new IllegalStateException(String.format("Can not visit an association for a reference entity %s", entity));
    }

    @Override
    public void onVisitReverseAssociation(String propertyName, T3AccessEntity entity, T3AccessEntityMeta meta) {
        // no used for reference entity
        throw new IllegalStateException(String.format("Can not visit a reverse association for a reference entity %s", entity));
    }

}
