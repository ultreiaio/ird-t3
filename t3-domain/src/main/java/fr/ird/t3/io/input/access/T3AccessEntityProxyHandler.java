/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessEntityProxyHandler;
import fr.ird.msaccess.type.IntToCoordonne;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.io.input.access.type.IntToBoolean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.util.EntityOperator;

/**
 * Implementation of the {@link AbstractAccessEntityProxyHandler} for the
 * T3 project.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3AccessEntityProxyHandler extends AbstractAccessEntityProxyHandler<T3EntityEnum> {

    private static final Log log = LogFactory.getLog(T3AccessEntityProxyHandler.class);

    T3AccessEntityProxyHandler(T3AccessEntityMeta meta, int rowId, Object[] pKey) throws Exception {
        super(meta, rowId, pKey);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected EntityOperator<TopiaEntity> getOperator(TopiaEntityEnum type) {
        Class<? extends TopiaEntity> contract = type.getContract();
        return (EntityOperator<TopiaEntity>) T3EntityEnum.getOperator(contract);
    }

    @Override
    protected Object getPropertyValue(Class<?> type, String propertyName, Object value) {
        if (type.equals(Float.class)) {
            return Float.valueOf(value + "");
        }

        if (type.equals(Integer.class)) {
            // on repasse toujours par un float car dans la base access certain champs sont Float alors que dans l'application c'est du Integer
            return Float.valueOf(value + "").intValue();
        }

        if (type.equals(IntToCoordonne.class)) {
            IntToCoordonne c = new IntToCoordonne(Integer.valueOf(value + ""));
            Object newValue = c.getFloatValue();
            log.debug(String.format("%s %d --> %s", propertyName, c.getIntValue(), newValue));
            return newValue;
        }

        if (type.equals(IntToBoolean.class)) {
            // on recupere un integer que l'on transformera en boolean
            IntToBoolean bValue = new IntToBoolean(Float.valueOf(value + "").intValue());
            log.debug(String.format("Will set [%s:%d-%s] to %s", propertyName, bValue.getIntValue(), bValue.getBooleanvalue(), entity));
            return bValue.getBooleanvalue();
        }
        return value;
    }

    @Override
    protected Object getPropertyValueFromMetaType(TopiaEntityEnum metaType, String propertyName, Object value) {
        return value;
    }

}
