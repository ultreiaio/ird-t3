/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.HitModel;

/**
 * A hit model to follow the progression of loading trips.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class LoadingTripHitModel extends HitModel<String, LoadingTripHitModel> {

    public static final String NB_TRIP_LOADED = "nbTripsLoaded";

    private static final long serialVersionUID = 1L;

    public LoadingTripHitModel() {
        super(String.class);
    }

    @Override
    protected LoadingTripHitModel newModel() {
        return new LoadingTripHitModel();
    }

    public void addTripLoaded() {
        addHit(NB_TRIP_LOADED);
    }
}
