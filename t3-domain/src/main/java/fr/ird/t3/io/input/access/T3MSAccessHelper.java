/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper to use ms-access.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public final class T3MSAccessHelper {

    private static final Log log = LogFactory.getLog(T3MSAccessHelper.class);

    private T3MSAccessHelper() {
        // hide helper constructor
    }

    public static void loadReferentiel(T3ReferentielEntityVisitor visitor,
                                       T3AccessDataSource dataSource,
                                       ReferenceEntityMap references,
                                       T3EntityEnum constant,
                                       boolean deep) {
        T3AccessEntityMeta meta = dataSource.getMeta(constant);
        log.info(String.format("Load referentiel %s", constant));
        List<? extends T3ReferenceEntity> entities = dataSource.loadEntities(meta);
        for (T3ReferenceEntity entity : entities) {
            visitor.doVisit(entity, deep);
        }
        references.put(constant, entities);
        log.info(String.format("Loaded entity %s with %d entries", constant, entities.size()));
    }

    public static <E extends TopiaEntity> List<E> removeProxies(RemoveProxyEntityVisitor visitor, List<E> entities) {

        List<E> entitiesToStore = new ArrayList<>();
        for (E entity : entities) {
            E e = visitor.doVisit(entity);
            entitiesToStore.add(e);
        }
        return entitiesToStore;
    }
}
