/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input;

import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.data.TripType;

import java.io.File;

/**
 * Configuration of a T3Input.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class T3InputConfiguration {

    private boolean useWells;
    private boolean canCreateVessel;
    private boolean createVirtualVessel;
    private boolean useSamplesOnly;
    private boolean canCreateVirtualActivity;
    private TripType tripType;
    private File inputFile;

    private T3TopiaApplicationContext topiaApplicationContext;

    public boolean isUseWells() {
        return useWells;
    }

    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    public boolean isCanCreateVessel() {
        return canCreateVessel;
    }

    public void setCanCreateVessel(boolean canCreateVessel) {
        this.canCreateVessel = canCreateVessel;
    }

    public boolean isCreateVirtualVessel() {
        return createVirtualVessel;
    }

    public void setCreateVirtualVessel(boolean createVirtualVessel) {
        this.createVirtualVessel = createVirtualVessel;
    }

    public TripType getTripType() {
        return tripType;
    }

    public void setTripType(TripType tripType) {
        this.tripType = tripType;
    }

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public boolean isUseSamplesOnly() {
        return useSamplesOnly;
    }

    public void setUseSamplesOnly(boolean useSamplesOnly) {
        this.useSamplesOnly = useSamplesOnly;
    }

    public boolean isCanCreateVirtualActivity() {
        return canCreateVirtualActivity;
    }

    public void setCanCreateVirtualActivity(boolean canCreateVirtualActivity) {
        this.canCreateVirtualActivity = canCreateVirtualActivity;
    }

    public T3TopiaApplicationContext getTopiaApplicationContext() {
        return topiaApplicationContext;
    }

    public void setTopiaApplicationContext(T3TopiaApplicationContext topiaApplicationContext) {
        this.topiaApplicationContext = topiaApplicationContext;
    }
}
