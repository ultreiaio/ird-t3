/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output;

import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.services.T3ServiceContext;
import java.io.Serializable;
import org.nuiton.version.Version;

/**
 * The contract to define a new pilot of {@link T3Output}.
 * <p/>
 * The unicity of a provider is done on the field {@link #getId()}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public interface T3OutputProvider<O extends T3OutputOperation, C extends T3OutputConfiguration> extends Serializable {

    /**
     * Obtain the unique id of the implementation.
     *
     * @return the id of the implementation
     */
    String getId();

    /**
     * Obtain the invariant name of the implementation.
     *
     * @return the name of the implementation
     */
    String getName();

    /**
     * Obtain the invariant version of the implementation.
     *
     * @return the version of the implementation.
     * @since 1.0
     */
    Version getVersion();

    /**
     * Obtain the invariant type of data source (says sql, csv,...)
     *
     * @return the type of data source
     */
    String getOutputType();

    /**
     * Obtain the description of the provider.
     * <p/>
     * this data will be displayed in GUI and should show the name, version and input type.
     *
     * @return the description of the provider
     */
    String getLibelle();

    /**
     * To instanciate a new {@link T3Output}.
     *
     * @param configuration  configuration to use in output pilot
     * @param messager       messager (used to push messages while doing operations)
     * @param serviceContext current servide context
     * @return the new instance of the pilot
     */
    T3Output<O, C> newInstance(C configuration,
                               T3Messager messager,
                               T3ServiceContext serviceContext);

    /**
     * Obtain the available operations for the ouput pilot.
     *
     * @return the array of available operations for the output pilot.
     */
    O[] getOperations();

    /**
     * Gets a output operation given his id.
     *
     * @param operationId the id of the required operation
     * @return the required operation or {@code null} if a such operation found
     */
    O getOperation(String operationId);
}
