package fr.ird.t3.io.input.access;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.data.ElementaryLanding;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.data.WellPlan;
import fr.ird.t3.entities.reference.VesselActivity;
import fr.ird.t3.entities.reference.WeightCategoryLanding;
import fr.ird.t3.entities.reference.WeightCategoryLogBook;
import fr.ird.t3.io.input.MissingForeignKeyInT3;
import fr.ird.t3.io.input.T3InputConfiguration;
import fr.ird.type.CoordinateHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by tchemit on 04/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class T3AvdthDataEntityVisitor extends T3DataEntityVisitor {

    private static final int FICTIVE_VESSEL_ACTIVITY_CODE = 100;
    private static final Log log = LogFactory.getLog(T3AvdthDataEntityVisitor.class);
    private final ReverseAssociationGetter<Trip, WellPlan, Activity> wellPlanReverse;
    private final ReverseAssociationGetter<Trip, Sample, Well> sampleReverse;
    private final ReverseAssociationGetter<Trip, SampleSet, Activity> sampleSetReverse;

    protected T3AvdthDataEntityVisitor(T3AccessDataSource dataSource, ReferenceEntityMap referentiel, T3InputConfiguration configuration) {
        super(dataSource, referentiel);
        wellPlanReverse = newReverseAssociationGetter(Trip.class, WellPlan.class, Activity.class, Trip.PROPERTY_ACTIVITY, WellPlan.PROPERTY_ACTIVITY);
        sampleReverse = newReverseAssociationGetter(Trip.class, Sample.class, Well.class, Trip.PROPERTY_WELL, Sample.PROPERTY_WELL);
        if (TripType.LOGBOOKMISSING == configuration.getTripType()) {
            sampleSetReverse = new ReverseAssociationGetter<Trip, SampleSet, Activity>(Trip.class, SampleSet.class, Activity.class, Trip.PROPERTY_ACTIVITY, SampleSet.PROPERTY_ACTIVITY) {
                @Override
                void onReverseNotFound(Trip parent, T3AccessEntity entity, Object[] pKey) {
                    SampleSet sampleSet = (SampleSet) entity;
                    Activity activity = (Activity) dataSource.getMeta(T3EntityEnum.Activity).newEntity(0, pKey);
                    activity.setDate(Objects.requireNonNull((Date) pKey[2]));
                    activity.setNumber(Objects.requireNonNull((Short) pKey[3]));
                    activity.setVesselActivity((VesselActivity) Objects.requireNonNull(getReferenceEntity(dataSource.getMeta(T3EntityEnum.VesselActivity), FICTIVE_VESSEL_ACTIVITY_CODE)));
                    activity.setQuadrant(Objects.requireNonNull(sampleSet.getActivityQuadrant()));
                    activity.setLatitude(Objects.requireNonNull(sampleSet.getActivityLatitude()));
                    activity.setLongitude(Objects.requireNonNull(sampleSet.getActivityLongitude()));
                    activity.setSchoolType(Objects.requireNonNull(sampleSet.getActivitySchoolType()));
                    sampleSet.setActivity(endActivity(activity));
                    parent.addActivity(activity);
                    // Force tripType, so later we will know that this trip has no logBook
                    parent.setTripType(TripType.LOGBOOKMISSING);
                }
            };
        } else {
            sampleSetReverse = newReverseAssociationGetter(Trip.class, SampleSet.class, Activity.class, Trip.PROPERTY_ACTIVITY, SampleSet.PROPERTY_ACTIVITY);
        }
    }

    @Override
    public void onEnd(T3AccessEntity entity, T3AccessEntityMeta meta) {
        super.onEnd(entity, meta);
        if (!deepVisit) {
            return;
        }
        endActivity(entity, meta);
    }

    @Override
    public void onVisitReverseAssociation(String propertyName, T3AccessEntity entity, T3AccessEntityMeta meta) {
        if (!deepVisit) {
            return;
        }
        if (SampleSet.PROPERTY_ACTIVITY.equals(propertyName) && entity instanceof SampleSet) {
            // special case, must obtain the activity from the trip
            sampleSetReverse.attachReverseAssociation(entity);
            return;
        }
        if (WellPlan.PROPERTY_ACTIVITY.equals(propertyName) && entity instanceof WellPlan) {
            // special case, must obtain the activity from the trip
            wellPlanReverse.attachReverseAssociation(entity);
            return;
        }
        if (Sample.PROPERTY_WELL.equals(propertyName) && entity instanceof Sample) {
            // special case, must obtain the well from the trip
            sampleReverse.attachReverseAssociation(entity);
            return;
        }
        super.onVisitReverseAssociation(propertyName, entity, meta);
    }

    @Override
    public void onVisitComposition(String propertyName, Class<?> type, T3AccessEntity entity, T3AccessEntityMeta meta) {
        if (onVisitElementaryLanding(propertyName, entity)) {
            return;
        }
        if (onVisitElementaryCatch(propertyName, entity)) {
            return;
        }
        super.onVisitComposition(propertyName, type, entity, meta);
    }

    private void endActivity(T3AccessEntity entity, T3AccessEntityMeta meta) {
        T3EntityEnum type = meta.getType();
        if (type == T3EntityEnum.Activity) {
            Activity activity = (Activity) entity;
            endActivity(activity);
            log.debug(String.format("Will use for activity %s coordinates <%s,%s>", entity, activity.getLongitude(), activity.getLatitude()));
        }
        if (type == T3EntityEnum.Trip) {
            log.info(String.format("Visit end: %s", toString(entity)));
        }
    }

    private Activity endActivity(Activity activity) {
        Integer quadrant = activity.getQuadrant();
        activity.setLatitude(CoordinateHelper.getSignedLatitude(quadrant, activity.getLatitude()));
        activity.setLongitude(CoordinateHelper.getSignedLongitude(quadrant, activity.getLongitude()));
        return activity;
    }

    private boolean onVisitElementaryLanding(String propertyName, T3AccessEntity entity) {
        if (!(ElementaryLanding.PROPERTY_WEIGHT_CATEGORY_LANDING.equals(propertyName) && entity instanceof ElementaryLanding)) {
            return false;
        }
        Serializable codeEspece = getProperty("C_ESP", row);
        Serializable codeCate = getProperty("C_CAT_C", row);

        // must found the species
        T3AccessEntityMeta specieMeta = dataSource.getMeta(T3EntityEnum.Species);
        TopiaEntity specie = getReferenceEntity(specieMeta, codeEspece);
        T3AccessEntityMeta categoryMeta = dataSource.getMeta(T3EntityEnum.WeightCategoryLanding);

        // special case : need to find a reference with two primary keys...
        TopiaEntity ref = getReferenceEntity(
                categoryMeta,
                new String[]{WeightCategoryLanding.PROPERTY_CODE, WeightCategoryLanding.PROPERTY_SPECIES},
                codeCate, specie);
        if (ref == null) {
            log.warn(String.format("Can't find WeightCategoryLanding for speciesCode / categoryCode: %s/%s", codeEspece, codeCate));
            getMissingForeignKeys().add(new MissingForeignKeyInT3(
                    T3EntityEnum.ElementaryLanding,
                    T3EntityEnum.WeightCategoryLanding,
                    getPKey(T3EntityEnum.ElementaryLanding),
                    new Object[]{codeEspece, codeCate}));
        } else {
            log.debug(String.format("Detected landing catch weight category : %s", ref));
            entity.setProperty(ElementaryLanding.PROPERTY_WEIGHT_CATEGORY_LANDING, ref);
        }
        return true;
    }

    private boolean onVisitElementaryCatch(String propertyName, T3AccessEntity entity) {
        if (!(ElementaryCatch.PROPERTY_WEIGHT_CATEGORY_LOG_BOOK.equals(propertyName) && entity instanceof ElementaryCatch)) {
            return false;
        }

        // special case : need to find a reference with two primary keys...
        Serializable codeSpecies = getProperty("C_ESP", row);
        Serializable codeCategory = getProperty("C_CAT_T", row);

        // must found the species
        T3AccessEntityMeta specieMeta = dataSource.getMeta(T3EntityEnum.Species);
        TopiaEntity specie = getReferenceEntity(specieMeta, codeSpecies);
        T3AccessEntityMeta categoryMeta = dataSource.getMeta(T3EntityEnum.WeightCategoryLogBook);

        // special case : need to find a reference with two primary keys...
        TopiaEntity ref = getReferenceEntity(
                categoryMeta,
                new String[]{WeightCategoryLogBook.PROPERTY_CODE, WeightCategoryLogBook.PROPERTY_SPECIES},
                codeCategory, specie);

        if (ref == null) {
            log.warn(String.format("Can't find weightCategoryLogBook for speciesCode / categoryCode: %s/%s", codeSpecies, codeCategory));
            getMissingForeignKeys().add(new MissingForeignKeyInT3(
                    T3EntityEnum.ElementaryCatch,
                    T3EntityEnum.WeightCategoryLogBook,
                    getPKey(T3EntityEnum.ElementaryCatch),
                    new Object[]{codeCategory, codeSpecies}));
        } else {
            log.debug(String.format("Detected logBook catch weight category : %s", ref));
            entity.setProperty(propertyName, ref);
        }
        return true;
    }

}
