/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import fr.ird.msaccess.importer.AbstractAccessDataSource;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * La source de données utilisant une base access.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class T3AccessDataSource extends AbstractAccessDataSource<T3EntityEnum, T3AccessEntityMeta> {

    private static final Log log = LogFactory.getLog(T3AccessDataSource.class);

    public T3AccessDataSource(File dbFile, Class<? extends T3AccessEntityMetaProvider> providerType) {
        super(T3AccessEntityMeta.class, providerType, dbFile);
    }

    @Override
    protected T3AccessEntityMeta[] newMetaArray(Collection<T3AccessEntityMeta> table) {
        return table.toArray(new T3AccessEntityMeta[0]);
    }

    @Override
    protected void onTableMissing(T3AccessEntityMeta meta) {
        String message = String.format("No table [%s] found for type %s", meta.getTableName(), meta.getType());
        log.error(message);
        meta.addError(message);
    }

    @Override
    protected void onPropertyMissing(T3AccessEntityMeta meta, String property, String column) {
        String message = String.format("Could not found column [%s - %s] for property [%s - %s]", meta.getTableName(), column, meta.getType(), property);
        log.error(message);
        // tout autre propriete manquante est une erreur
        meta.addError(message);
    }

    @Override
    protected void onPKeyMissing(T3AccessEntityMeta meta, String pKey) {
        // c'est une erreur car on ne peut pas recuperer les donnees
        String message = String.format("Could not found pKey column [%s - %s]", meta.getTableName(), pKey);
        log.error(message);
        meta.addError(message);
    }

    public String[] getErrors() {
        List<String> buffer = new ArrayList<>();
        if (hasError()) {
            for (T3AccessEntityMeta meta : getMetaWithError()) {
                Collections.addAll(buffer, meta.getErrors());
            }
        }
        return buffer.toArray(new String[0]);
    }

    public String[] getWarnings() {
        List<String> buffer = new ArrayList<>();
        if (hasWarning()) {
            for (T3AccessEntityMeta meta : getMetaWithWarning()) {
                Collections.addAll(buffer, meta.getWarnings());
            }
        }
        return buffer.toArray(new String[0]);
    }

    public void analyzeDb(T3AccessHitModel hits) throws Exception {
        // always init the data source
        init();
        Set<String> tables = getTableNames();
        log.info(String.format("Discover de %d tables.", tables.size()));
        Set<String> unusedTableNames = getUnusedTableNames();
        if (log.isDebugEnabled() && !unusedTableNames.isEmpty()) {
            log.debug(String.format("There is %d unused table(s) :", unusedTableNames.size()));
            for (String tableName : unusedTableNames) {
                Set<String> columns = getUnusedTableColumns(tableName);
                List<String> tmp = new ArrayList<>(columns);
                Collections.sort(tmp);
                log.debug(tableName + " with columns : " + tmp);
            }
        }
        T3AccessEntityMeta[] metas = getMetas();
        log.debug(String.format("Register %d metas.", metas.length));
        if (hasError()) {
            T3AccessEntityMeta[] metaWithError = getMetaWithError();
            // des erreurs ont été rencontrées
            log.error(String.format("There is %d meta(s) with error(s).", metaWithError.length));
        } else {
            log.info("No error detected on database structure.");
        }
        if (hasWarning()) {
            T3AccessEntityMeta[] metaWithWarning = getMetaWithWarning();
            // des warnings ont été rencontrées
            log.warn(String.format("There is %d meta(s) with warning(s) : ", metaWithWarning.length));
        } else {
            log.info("No warning detected on database structure.");
        }
        if (hasError()) {
            // on ne peut pas charger les données
            return;
        }
        // chargement des données
        load();
        for (T3AccessEntityMeta meta : getMetas()) {
            if (meta instanceof T3ReferenceEntity) {
                // on ne veut charger que les données observateurs, pas le referentiel
                continue;
            }
            Map<String, Object>[] data = getTableData(meta);
            if (hits != null) {
                long hit = hits.getHit(meta.getType());
                hits.addHit0(meta.getType(), true, hit, hit + data.length);
            }
        }
        for (T3AccessEntityMeta meta : getMetas()) {
            int[] errorRows = meta.getErrorRows();
            if (errorRows != null && errorRows.length > 0) {
                log.error(String.format("[%s] Could not load %d row(s) : %s", meta.getType(), errorRows.length, Arrays.toString(errorRows)));
            }
        }

        if (log.isDebugEnabled() && hits != null) {
            log.debug(String.format("Number of rejected data: %d", hits.getTotalHit()));
            for (Map.Entry<T3EntityEnum, Long> entry : hits) {
                T3EntityEnum type = entry.getKey();
                Long total = entry.getValue();
                log.info(String.format("[%s] find %d objects to import.", type, total));
            }
        }
    }
}
