/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.T3EntityMap;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.io.input.MissingForeignKey;
import fr.ird.t3.io.input.T3Input;
import fr.ird.t3.io.input.T3InputConfiguration;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The implementation of the {@link T3Input} for AVDTH databases.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractT3InputMSAccess implements T3Input {

    private static final Log log = LogFactory.getLog(AbstractT3InputMSAccess.class);
    private T3InputConfiguration configuration;
    private Map<Integer, Vessel> vessels;
    private T3AccessDataSource dataSource;
    private ReferenceEntityMap safeReferences;
    private Map<Trip, List<MissingForeignKey>> missingFK;
    private List<String> errors;
    private List<String> warnings;
    private Map<Trip, String> tripTypeNotSafeMessages;

    public abstract T3AccessDataSource newDataSource(File inputFile);

    public abstract T3DataEntityVisitor newDataVisitor(T3AccessDataSource dataSource, ReferenceEntityMap safeReferences);

    @Override
    public List<String> getPKeyNames(T3EntityEnum type) {
        T3AccessEntityMeta meta = dataSource.getMeta(type);
        return meta.getPKeys();
    }

    @Override
    public T3InputConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void init(T3InputConfiguration configuration) throws Exception {
        this.configuration = configuration;
        // open data source
        dataSource = newDataSource(configuration.getInputFile());
        Set<String> t3DataTypes = new HashSet<>();
        for (T3EntityEnum t3EntityEnum : getDataTypes()) {
            t3DataTypes.add(t3EntityEnum.getContract().getSimpleName());
        }
        T3AccessHitModel hitModel = new T3AccessHitModel();
        hitModel.addPropertyChangeListener(new LoadDbPropertyChangeListener(t3DataTypes));
        // analyse data source
        dataSource.analyzeDb(hitModel);
        errors = new ArrayList<>();
        if (dataSource.hasError()) {
            errors.addAll(Arrays.asList(dataSource.getErrors()));
        }
        warnings = new ArrayList<>();
        if (dataSource.hasWarning()) {
            warnings.addAll(Arrays.asList(dataSource.getWarnings()));
        }
    }

    @Override
    public String[] getAnalyzeErrors() {
        return errors.toArray(new String[0]);
    }

    @Override
    public String[] getAnalyzeWarnings() {
        return warnings.toArray(new String[0]);
    }

    @Override
    public Map<Integer, Vessel> getNewVessels() {
        if (dataSource == null) {
            throw new IllegalStateException("Input was not initialized via the init method.");
        }
        if (vessels == null) {
            // init unsafe reference universe
            ReferenceEntityMap map = new ReferenceEntityMap();
            // load unsafe references
            T3ReferentielEntityVisitor visitor = new T3ReferentielEntityVisitor(dataSource, safeReferences);
            visitor.setStrictCheck(false);
            try {
                // load unsafe references
                T3MSAccessHelper.loadReferentiel(visitor, dataSource, map, T3EntityEnum.Vessel, true);
            } finally {
                visitor.clear();
            }
            List<Vessel> vesselList = map.get(Vessel.class);
            RemoveProxyEntityVisitor visitor2 = new RemoveProxyEntityVisitor();
            try {
                List<Vessel> result = T3MSAccessHelper.removeProxies(visitor2, vesselList);
                // remove from it all the one existing in incoming db
                List<Vessel> existingVessels = safeReferences.get(Vessel.class);
                List<Integer> existingVesselCodes = existingVessels.stream().map(Vessel::getCode).collect(Collectors.toList());
                // existing vessel, do not keep it
                result.removeIf(next -> existingVesselCodes.contains(next.getCode()));
                // index by code
                vessels = Maps.uniqueIndex(result, Vessel::getCode);
            } finally {
                visitor2.clear();
            }
        }
        return vessels;
    }

    @Override
    public void setSafeReferences(ReferenceEntityMap safeReferences) {
        this.safeReferences = safeReferences;
    }

    @Override
    public Map<Trip, T3EntityMap> loadTrips(LoadingTripHitModel hitModel) {
        if (dataSource == null) {
            throw new IllegalStateException("Input was not initialized via the init method.");
        }
        if (safeReferences == null) {
            throw new IllegalStateException("No safe references was initialized by method setSafeReferences.");
        }
        TripType tripType = getConfiguration().getTripType();
        boolean canCreateVessel = getConfiguration().isCanCreateVessel();
        // load trip data
        T3EntityEnum constant = T3EntityEnum.valueOf(Trip.class);
        T3AccessEntityMeta meta = dataSource.getMeta(constant);
        List<Trip> entities = dataSource.loadEntities(meta);
        Map<Trip, T3EntityMap> result = new LinkedHashMap<>();
        missingFK = new HashMap<>();
        T3DataEntityVisitor dataVisitor = newDataVisitor(dataSource, safeReferences);
        try {
            for (Trip trip : entities) {
                T3AccessEntity a = (T3AccessEntity) trip;
                Integer vesselCode = Integer.valueOf(a.getPKey()[0] + "");
                Trip loadedTrip = loadTrip(vesselCode, trip, dataVisitor, tripType, canCreateVessel);
                hitModel.addTripLoaded();
                // get the touched references for this trip
                T3EntityMap entitiesTouched = dataVisitor.getEntitiesTouched();
                List<MissingForeignKey> missingFKForTrip = dataVisitor.getMissingForeignKeys();
                if (CollectionUtils.isNotEmpty(missingFKForTrip)) {
                    missingFK.put(loadedTrip, missingFKForTrip);
                }
                result.put(loadedTrip, entitiesTouched);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Touch %d types of entities for the trip %s", entitiesTouched.size(), Arrays.toString(a.getPKey())));
                    for (Map.Entry<T3EntityEnum, List<? extends TopiaEntity>> entry : entitiesTouched.entrySet()) {
                        log.debug(String.format(" [%s] : %d", entry.getKey(), entry.getValue().size()));
                    }
                }
                dataVisitor.resetTripStates();
            }
        } finally {
            // always clean the visitor at the end (contains a lot of data...)
            dataVisitor.clear();
        }
        return result;
    }

    @Override
    public void destroy() {
        if (safeReferences != null) {
            safeReferences = null;
        }
        if (dataSource != null) {
            dataSource.destroy();
            dataSource = null;
        }
    }

    @Override
    public int getNbTrips() {

        if (dataSource == null) {
            throw new IllegalStateException("Input was not initialized via the init method.");
        }

        T3AccessEntityMeta meta = dataSource.getMeta(T3EntityEnum.Trip);
        return dataSource.getTableData(meta).length;
    }

    public T3AccessDataSource getDataSource() {
        return dataSource;
    }

    @Override
    public List<MissingForeignKey> getMissingForeignKeys(Trip trip) {
        if (missingFK == null) {
            return null;
        }
        return missingFK.get(trip);
    }

    @Override
    public String getTripTypeNotSafeMessage(Trip trip) {
        return tripTypeNotSafeMessages == null ? null : tripTypeNotSafeMessages.get(trip);
    }

    private void addTripTypeNotSafeMessage(Trip trip, String message) {
        if (tripTypeNotSafeMessages == null) {
            tripTypeNotSafeMessages = new LinkedHashMap<>();
        }
        tripTypeNotSafeMessages.put(trip, message);
    }

    private Trip loadTrip(int vesselCode, Trip trip, T3DataEntityVisitor dataVisitor, TripType tripType, boolean canCreateVessel) {
        RemoveProxyEntityVisitor visitor = new RemoveProxyEntityVisitor();
        try {
            log.debug(String.format("Will visit deeply entity %s", trip));
            // fill the trip
            dataVisitor.doVisit(trip, true);
            // remove proxy on data
            Trip loadedTrip = visitor.doVisit(trip);
            T3AccessEntity a = (T3AccessEntity) trip;
            Object[] pKey = a.getPKey();
            switch (tripType) {
                case STANDARD:
                    try {
                        TripTopiaDao.checkAndSetStandardTripType(loadedTrip, pKey);
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        addTripTypeNotSafeMessage(trip, e.getMessage());
                    }
                    break;
                case SAMPLEONLY:
                    // no check
                    loadedTrip.setTripType(TripType.SAMPLEONLY);
                    break;
                case LOGBOOKMISSING:
                    try {
                        TripTopiaDao.checkAndSetLogbookMissingTripType(loadedTrip, pKey);
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        addTripTypeNotSafeMessage(trip, e.getMessage());
                    }
                    break;
            }
            if (trip.getVessel() == null && canCreateVessel) {
                // use new created vessel
                Vessel vessel = getNewVessels().get(vesselCode);
                loadedTrip.setVessel(vessel);
                List<Vessel> vesselTouched = dataVisitor.getEntitiesTouched().get(Vessel.class);
                vesselTouched.clear();
                vesselTouched.add(vessel);
            }
            return loadedTrip;
        } catch (TopiaException e) {
            throw new IllegalStateException(String.format("Could not visit data %s", trip), e);
        } finally {
            visitor.clear();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        destroy();
        super.finalize();
    }

    private static class LoadDbPropertyChangeListener implements PropertyChangeListener {

        private final Set<String> t3DataTypes;

        LoadDbPropertyChangeListener(Set<String> t3DataTypes) {
            this.t3DataTypes = t3DataTypes;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();
            if (log.isInfoEnabled()) {
                if (t3DataTypes.contains(propertyName)) {
                    log.info(String.format("Detected data entity %s with %s entries", propertyName, evt.getNewValue()));
                } else {
                    log.info(String.format("Detected ref  entity %s with %s entries", propertyName, evt.getNewValue()));
                }
            }
        }
    }
}
