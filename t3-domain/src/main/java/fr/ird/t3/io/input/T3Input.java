/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input;

import com.google.common.collect.ImmutableSet;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.T3EntityMap;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.io.input.access.LoadingTripHitModel;
import java.util.List;
import java.util.Map;

/**
 * The contract to defined how to integrate incoming data into T3 database.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public interface T3Input {

    List<String> getPKeyNames(T3EntityEnum type);

    /**
     * Returns the configuration used by the input.
     *
     * @return the input configuration.
     * @since 1.3.1
     */
    T3InputConfiguration getConfiguration();

//    /**
//     * Returns The input to process by this input.
//     *
//     * @return the input file source to process
//     */
//    File getInputFile();

    /**
     * Initialize the inputs.
     *
     * @param configuration configuration of input
     * @throws Exception if any error while analyzing the input
     */
    void init(T3InputConfiguration configuration) throws Exception;

    /**
     * Obtain the array of errors after the invocation of {@link #init(T3InputConfiguration)} methid.
     *
     * @return the array of array or an empty array if no error detected.
     * @since 1.2
     */
    String[] getAnalyzeErrors();

    /**
     * Obtain the array of warnings after the invocation of {@link #init(T3InputConfiguration)} methid.
     *
     * @return the array of warnings or an empty array if no warning detected.
     * @since 1.2
     */
    String[] getAnalyzeWarnings();

//    /**
//     * To load all references from the input and returns them.
//     *
//     * @return the references grouped by their type from the input
//     * @throws Exception if any error while loading
//     */
//    ReferenceEntityMap loadUnsafeReferences() throws Exception;

    /**
     * To load all new vessels from the input and return them indexed by
     * their {@link Vessel#getCode()}.
     *
     * @return the new vessels found in dataSource indexed by their code
     * @since 1.3.1
     */
    Map<Integer, Vessel> getNewVessels();

    /**
     * Set all safe references from the t3 database.
     * <p/>
     * The safe references are used to build the trips.
     *
     * @param safeReferences universe of safe references coming from the t3 database
     */
    void setSafeReferences(ReferenceEntityMap safeReferences);

    /**
     * Load the fully list of trips from the innput source and returns it.
     *
     * @param hitModel a hit model to follow loading progression
     * @return the list of fully loaded trip.
     */
    Map<Trip, T3EntityMap> loadTrips(LoadingTripHitModel hitModel);

    /**
     * For a given loaded trip, get the missing foreign keys detected while loading trips.
     *
     * @param trip the trip to search of.
     * @return the list of such missing foreign key, or {@code null} if none found.
     */
    List<MissingForeignKey> getMissingForeignKeys(Trip trip);

    String getTripTypeNotSafeMessage(Trip trip);

    /**
     * Method to clean input internal states.
     * <p/>
     * This method must be invoked in the {@link Object#finalize()} method.
     */
    void destroy();

    /**
     * Obtain the count of trips found in input source.
     *
     * @return the count of trips in input source
     */
    int getNbTrips();

    /**
     * @return the references types used by the pilot.
     * @since 2.0
     */
    ImmutableSet<T3EntityEnum> getReferenceTypes();

    /**
     * @return the data types used by the pilot.
     * @since 2.0
     */
    ImmutableSet<T3EntityEnum> getDataTypes();
}
