/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input;

import fr.ird.t3.entities.T3EntityEnum;
import java.util.Arrays;

/**
 * To define a missing foreign key.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class MissingForeignKey {

    /** Source type of missing foreign key. */
    protected final T3EntityEnum sourceType;

    /** Target type of missing foreign key. */
    protected final T3EntityEnum targetType;

    /** Source Primary key of missing foreign key. */
    protected final Object[] sourcePKey;

    /** Target Primary key of missing foreign key. */
    protected final Object[] targetPKey;

    public MissingForeignKey(T3EntityEnum sourceType,
                             T3EntityEnum targetType,
                             Object[] sourcePKey,
                             Object[] targetPKey) {
        this.sourceType = sourceType;
        this.targetType = targetType;
        this.sourcePKey = Arrays.copyOf(sourcePKey, sourcePKey.length);
        this.targetPKey = Arrays.copyOf(targetPKey, targetPKey.length);
    }

    /**
     * Gets the source type of the missing foreign key.
     *
     * @return the source type of the missing foreign key.
     */
    public T3EntityEnum getSourceType() {
        return sourceType;
    }

    /**
     * Gets the target type of the missing foreign key.
     *
     * @return the target type of the missing foreign key.
     */
    public T3EntityEnum getTargetType() {
        return targetType;
    }

    /**
     * Gets the primary key of the source of the missing key.
     *
     * @return the primary key of the source of the missing key.
     */
    public Object[] getSourcePKey() {
        return sourcePKey;
    }

    /**
     * Get the primary key of the target of the missing key.
     *
     * @return the primary key of the target of the missing key.
     */
    public Object[] getTargetPKey() {
        return targetPKey;
    }
}
