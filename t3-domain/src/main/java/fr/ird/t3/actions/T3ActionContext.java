/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.opensymphony.xwork2.LocaleProvider;
import fr.ird.t3.T3Config;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.user.T3Users;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Supplier;

/**
 * generic data action context which contains three things :
 * <p/>
 * <ul>
 * <li>action configuration {@link #configuration}</li>
 * <li>action generated {@link #messages}</li>
 * <li>action result {@link #result}</li>
 * <p/>
 * </ul>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3ActionContext<C extends T3ActionConfiguration> implements T3Messager, LocaleProvider, T3ServiceContext, T3Service {

    /** Map of results to be filled while the execute method. */
    private final Map<String, Object> result;
    /** Map of messages to be filled while the execute method. */
    private final ArrayListMultimap<MessageLevel, String> messages;
    private Class<? extends T3Action<C>> actionType;
    /** Configuration of action. */
    private C configuration;
    private int nbSteps;
    private float stepIncrement;
    private float progression;
    private long totalTime;
    private String resume;
    /** Delegate service context. */
    private T3ServiceContext serviceContext;

    public T3ActionContext() {
        messages = ArrayListMultimap.create();
        result = new TreeMap<>();
    }

    private static <T> T getValue(Map<String, Object> context, String paramKey, Class<T> type) {
        Object value = context.get(paramKey);
        if (value != null) {
            // can make a strict check of type
            checkParam(context, paramKey, type);
        }
        return type.cast(value);
    }

    @SuppressWarnings("unchecked")
    private static <K, V> Map<K, V> getValueAsMap(Map<String, Object> context, String paramKey) {
        return getValue(context, paramKey, Map.class);
    }

    @SuppressWarnings("unchecked")
    private static <K, V> Multimap<K, V> getValueAsMultimap(Map<String, Object> context, String paramKey) {
        return getValue(context, paramKey, Multimap.class);
    }

    @SuppressWarnings("unchecked")
    private static <V> List<V> getValueAsList(Map<String, Object> context, String paramKey) {
        return getValue(context, paramKey, List.class);
    }

    @SuppressWarnings("unchecked")
    private static <V> Set<V> getValueAsSet(Map<String, Object> context, String paramKey) {
        return getValue(context, paramKey, Set.class);
    }

    private static void checkParam(Map<String, Object> context, String paramKey, Class<?> type) {
        if (!context.containsKey(paramKey)) {
            throw new IllegalArgumentException(String.format("action requires a parameter named '%s'", paramKey));
        }
        Object value = context.get(paramKey);
        if (!type.isInstance(value)) {
            throw new IllegalArgumentException(
                    String.format(" action requires a parameter named '%s', typed as '%s', but was '%s'", paramKey, type, value.getClass())
            );
        }
    }

    public T3ServiceContext getServiceContext() {
        return serviceContext;
    }

    @Override
    public void setServiceContext(T3ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public String getActionName() {
        return getConfiguration().getName(getLocale());
    }

    public C getConfiguration() {
        return configuration;
    }

    public void setConfiguration(C configuration) {
        this.configuration = configuration;
    }

    @Override
    public Locale getLocale() {
        return serviceContext.getLocale();
    }

    @Override
    public void setLocale(Locale locale) {
        serviceContext.setLocale(locale);
    }

    @Override
    public T3Config getApplicationConfiguration() {
        return serviceContext.getApplicationConfiguration();
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceContext.getServiceFactory();
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceContext.newService(clazz);
    }

    @Override
    public void setTransaction(T3TopiaPersistenceContext transaction) {
        serviceContext.setTransaction(transaction);
    }

    @Override
    public Date getCurrentDate() {
        return serviceContext.getCurrentDate();
    }

    @Override
    public T3Users getT3Users() {
        return serviceContext.getT3Users();
    }

    @Override
    public T3TopiaApplicationContext getApplicationContext() {
        return serviceContext.getApplicationContext();
    }

    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return serviceContext.getT3TopiaPersistenceContext();
    }

    public Class<? extends T3Action<C>> getActionType() {
        return actionType;
    }

    public void setActionType(Class<? extends T3Action<C>> actionType) {
        this.actionType = actionType;
    }

    @Override
    public void addInfoMessage(String message) {
        messages.get(MessageLevel.INFO).add(message);
    }

    @Override
    public void addWarningMessage(String message) {
        messages.get(MessageLevel.WARNING).add(message);
    }

    @Override
    public void addErrorMessage(String message) {
        messages.get(MessageLevel.ERROR).add(message);
    }

    public String getInfoMessagesAsStr() {
        return getMessagesAsStr(MessageLevel.INFO);
    }

    public String getWarnMessagesAsStr() {
        return getMessagesAsStr(MessageLevel.WARNING);
    }

    public String getErrorMessagesAsStr() {
        return getMessagesAsStr(MessageLevel.ERROR);
    }

    @Override
    public List<String> getInfoMessages() {
        return messages.get(MessageLevel.INFO);
    }

    @Override
    public List<String> getWarnMessages() {
        return messages.get(MessageLevel.WARNING);
    }

    @Override
    public List<String> getErrorMessages() {
        return messages.get(MessageLevel.ERROR);
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public void clearMessages() {
        for (MessageLevel level : MessageLevel.values()) {
            messages.get(level).clear();
        }
    }

    private String getMessagesAsStr(MessageLevel level) {
        StringBuilder sb = new StringBuilder();
        for (String s : messages.get(level)) {
            sb.append(s).append('\n');
        }
        return sb.toString();
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public int getNbSteps() {
        return nbSteps;
    }

    public void setNbSteps(int nbSteps) {
        this.nbSteps = nbSteps;
        stepIncrement = 100f / nbSteps;
    }

    public float getStepIncrement() {
        return stepIncrement;
    }

    public int getProgression() {
        return (int) progression;
    }

    public void setProgression(float progression) {
        this.progression = progression;
    }

    public void incrementsProgression() {
        setProgression(progression + stepIncrement);
    }

    public Object getResult(String paramKey) {
        return result.get(paramKey);
    }

    public void putResult(String key, Object value) {
        result.put(key, value);
    }

    public <V> V getResult(String paramKey, Class<V> valueType) {
        return getValue(result, paramKey, valueType);
    }

    public <V> Set<V> getResultAsSet(String paramKey, Class<V> valueType) {
        return getValueAsSet(result, paramKey);
    }

    public <V> List<V> getResultAsList(String paramKey, Class<V> valueType) {
        return getValueAsList(result, paramKey);
    }

    public <K, V> Map<K, V> getResultAsMap(String paramKey) {
        return getValueAsMap(result, paramKey);
    }

    public <K, V> Multimap<K, V> getResultAsMultimap(String paramKey) {
        return getValueAsMultimap(result, paramKey);
    }

    enum MessageLevel {
        INFO,
        WARNING,
        ERROR
    }
}
