package fr.ird.t3.actions.stratum;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * To store all output data of action.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("unused")
public abstract class LevelOutputContext<C extends LevelConfigurationWithStratum, A extends T3Action<C>, I extends LevelInputContext<C, A>, R extends StratumResult<C>> {
    /**
     * Keep ids of treated {@link fr.ird.t3.entities.data.Activity}.
     */
    private final Set<String> usedActivityIds;
    private final I inputContext;
    private final Locale locale;
    private final DecoratorService decoratorService;
    /**
     * For each stratum gets his result.
     */
    private Collection<R> stratumResultSet;
    private long totalCatchActivities;
    private long totalCatchActivitiesWithSample;

    public LevelOutputContext(A action, I inputContext) {
        this.locale = action.getLocale();
        this.decoratorService = action.getDecoratorService();
        this.inputContext = inputContext;
        stratumResultSet = new LinkedHashSet<>();
        usedActivityIds = new TreeSet<>();
    }

    public int getNbStrataFixed() {
        return getStratumResultSet().size();
    }

    @SuppressWarnings("WeakerAccess")
    public Collection<R> getStratumResultSet() {
        return stratumResultSet;
    }

    public void setStratumResultSet(Set<R> stratumResultSet) {
        this.stratumResultSet = stratumResultSet;
    }

    public Set<String> getUsedActivityIds() {
        return usedActivityIds;
    }

    public long getTotalCatchActivities() {
        return totalCatchActivities;
    }

    public void addStratumResult(R stratumResult) {
        stratumResultSet.add(stratumResult);
    }

    public boolean addActivityId(String activityTopiaId) {
        return usedActivityIds.add(activityTopiaId);
    }

    public int getNbStratum() {
        return inputContext.getNbStratum();
    }

    @SuppressWarnings("WeakerAccess")
    public Integer[] getAllSubstitutionLevels() {
        Set<Integer> levels = new HashSet<>();
        for (R stratumResult : stratumResultSet) {
            levels.add(stratumResult.getSubstitutionLevel());
        }
        List<Integer> result = new ArrayList<>(levels);
        Collections.reverse(result);
        return result.toArray(new Integer[0]);
    }

    public long getTotalCatchActivitiesWithSample() {
        return totalCatchActivitiesWithSample;
    }

    public long getTotalCatchActivitiesWithoutSample() {
        return totalCatchActivities - totalCatchActivitiesWithSample;
    }

    public Collection<R> getStratumResult(int level) {
        Set<R> singleResult = new LinkedHashSet<>();
        for (R stratumResult : stratumResultSet) {
            if (level == stratumResult.getSubstitutionLevel()) {
                singleResult.add(stratumResult);
            }
        }
        return singleResult;
    }

    public Map<Integer, Collection<R>> getStratumResults() {
        Multimap<Integer, R> result = LinkedHashMultimap.create();
        Integer[] levels = getAllSubstitutionLevels();
        for (Integer level : levels) {
            Set<R> singleResult = new LinkedHashSet<>();
            for (R stratumResult : stratumResultSet) {
                if (level == stratumResult.getSubstitutionLevel()) {
                    singleResult.add(stratumResult);
                }
            }
            result.putAll(level, singleResult);
        }
        return result.asMap();
    }

    public int getMaximumSizeForStratum() {
        int result = 0;
        for (R stratumResult : stratumResultSet) {
            result = Math.max(result, stratumResult.getLabel().length() + 1);
        }
        return result;
    }

    public I getInputContext() {
        return inputContext;
    }

    public Locale getLocale() {
        return locale;
    }

    public DecoratorService getDecoratorService() {
        return decoratorService;
    }

    public Collection<Species> getSpecies() {
        return inputContext.getSpecies();
    }

    protected void mergeResult(R stratumResult) {
        totalCatchActivities += stratumResult.getNbActivities();
        totalCatchActivitiesWithSample += stratumResult.getNbActivitiesWithSample();
    }
}
