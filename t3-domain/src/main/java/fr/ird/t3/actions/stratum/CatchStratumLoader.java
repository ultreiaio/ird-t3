/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.base.Preconditions;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.T3TopiaPersistenceContextAware;
import org.nuiton.topia.persistence.TopiaException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * To loader a catch stratum.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see CatchStratum
 * @since 1.3
 */
public abstract class CatchStratumLoader<C extends LevelConfigurationWithStratum, A extends T3Action<C>, S extends StratumConfiguration<C, A>> implements T3TopiaPersistenceContextAware {

    protected Supplier<T3TopiaPersistenceContext> transaction;

    /**
     * To obtain all activities of the catch stratum.
     *
     * @param configuration stratum configuration
     * @return activities to use for the catch stratum
     * @throws TopiaException if any pb while quering db
     */
    public abstract Map<Activity, Integer> loadData(S configuration);

    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return transaction;
    }

    public void setTransaction(Supplier<T3TopiaPersistenceContext> transaction) {
        this.transaction = transaction;
    }

    protected Map<Activity, Integer> filterActivities(S configuration, Map<String, Integer> activityIds) {
        Map<Activity, Integer> result = new HashMap<>();
        if (activityIds != null && activityIds.size() > 0) {
            configuration.loadActivities(activityIds.keySet());
            Set<Vessel> possibleVessels = configuration.getPossibleCatchVessels();
            WeightCategoryTreatment weightCategoryTreatment = configuration.getWeightCategoryTreatment();
            for (Map.Entry<String, Integer> e : activityIds.entrySet()) {
                String activityId = e.getKey();
                // get activity
                Activity activity = configuration.getActivity(activityId);
                onActivityFound(activity);
                // get his trip
                Trip trip = activity.getTrip();
                if (!possibleVessels.contains(trip.getVessel())) {
                    // not a matching boat
                    continue;
                }
//                if (activity.isCorrectedElementaryCatchEmpty()) {
//                    continue;
//                }
                // recheck activity have some catches.
//                Preconditions.checkState(!activity.isCorrectedElementaryCatchEmpty(),
//                        String.format("Can not accept an activity (%s) with no catch", activity.getTopiaId()));
                // weight category is in stratum, check there is catch using this category
                if (weightCategoryTreatment != null && !activity.withCorrectedElementaryCatch(weightCategoryTreatment)) {
                    // reject - the stratum weight category not found for this activity
                    continue;
                }
                result.put(activity, e.getValue());
            }
        }
        return result;
    }

    protected abstract void onActivityFound(Activity activity);
}
