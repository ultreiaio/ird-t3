/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.WeightCategoryTreatmentAware;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.topia.persistence.TopiaException;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * base class for any stratum.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see CatchStratum
 * @since 1.0
 */
public abstract class Stratum<C extends LevelConfigurationWithStratum, A extends T3Action<C>, S extends StratumConfiguration<C, A>> implements Closeable, Iterable<Map.Entry<Activity, Integer>> {

    /**
     * Stratum configuration of this stratum.
     *
     * @see StratumConfiguration
     */
    private final S configuration;
    /**
     * Selected set of species to fix.
     *
     * @since 1.3.1
     */
    private final Set<Species> speciesToFix;
    /**
     * All activities selected for this stratum, with for catch stratum the number of zones where the activity should be used.
     * <p/>
     * see http://forge.codelutin.com/issues/1935
     * <p/>
     * <strong>Note:</strong> This data are available after invocation of method
     * {@link Stratum#init(T3ServiceContext, List, T3Action)}.
     */
    private Map<Activity, Integer> activities;

    protected Stratum(S configuration, Collection<Species> speciesToFix) {
        this.configuration = configuration;
        this.speciesToFix = ImmutableSet.copyOf(speciesToFix);
    }

    /**
     * Initialize the data of the stratum.
     *
     * @param serviceContext   service context
     * @param weightCategories available weight categories usables for this stratum
     * @param messager         messager object to send messages to action
     * @throws TopiaException if any error while loading data from database
     */
    public abstract void init(T3ServiceContext serviceContext, List<WeightCategoryTreatment> weightCategories, A messager) throws Exception;

    @SuppressWarnings("NullableProblems")
    @Override
    public Iterator<Map.Entry<Activity, Integer>> iterator() {
        return activities.entrySet().iterator();
    }

    public S getConfiguration() {
        return configuration;
    }

    public final Set<Species> getSpeciesToFix() {
        return speciesToFix;
    }

    public final C getLevelConfiguration() {
        return configuration.getConfiguration();
    }

    /**
     * Obtain the activities selected for this stratum with the number of zones they are involved in.
     * <p/>
     * The number of zones (see http://forge.codelutin.com/issues/1935) is only
     * used for catch stratum, for sample stratum always used a single zone.
     * <p/>
     * <strong>Note: </strong> to invoke this method, you need first to invoke the {@link #init(T3ServiceContext, List, T3Action)}
     *
     * @return the set of activities found for this stratum.
     */
    public final Map<Activity, Integer> getActivities() {
        return activities;
    }

    public void setActivities(Map<Activity, Integer> activities) {
        this.activities = activities;
    }

    @Override
    public void close() throws IOException {
        activities.clear();
    }

    protected void checkInitMethodInvoked(Object data) {
        if (data == null) {
            throw new IllegalStateException("You must invoke the #init(tx) method before accessing stratum data.");
        }
    }

    public Collection<CorrectedElementaryCatch> getCorrectedElementaryCatch(Activity activity) {
        WeightCategoryTreatment stratumWeightCategoryTreatment = getConfiguration().getWeightCategoryTreatment();
        if (stratumWeightCategoryTreatment == null) {
            return activity.getCorrectedElementaryCatch();
        }
        return activity.getCorrectedElementaryCatch().stream().filter(c -> stratumWeightCategoryTreatment.equals(c.getWeightCategoryTreatment())).collect(Collectors.toList());
    }

    public Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> getCorrectedElementaryCatchByCategory(Activity activity) {
        return Multimaps.index(getCorrectedElementaryCatch(activity), WeightCategoryTreatmentAware::getWeightCategoryTreatment);
    }

    private Multimap<WeightCategoryTreatment, SetSpeciesCatWeight> getSetSpeciesCatWeightByCategory(Activity activity) {
        return Multimaps.index(getSetSpeciesCatWeight(activity), WeightCategoryTreatmentAware::getWeightCategoryTreatment);
    }

    Collection<SetSpeciesCatWeight> getSetSpeciesCatWeight(Activity activity) {
        WeightCategoryTreatment stratumWeightCategoryTreatment = getConfiguration().getWeightCategoryTreatment();
        if (stratumWeightCategoryTreatment == null) {
            return activity.getSetSpeciesCatWeight();
        }
        return activity.getSetSpeciesCatWeight().stream().filter(c -> stratumWeightCategoryTreatment.equals(c.getWeightCategoryTreatment())).collect(Collectors.toList());
    }

    public Collection<SetSpeciesFrequency> getSetSpeciesFrequency(Activity activity) {
        WeightCategoryTreatment stratumWeightCategoryTreatment = getConfiguration().getWeightCategoryTreatment();
        if (stratumWeightCategoryTreatment == null) {
            return activity.getSetSpeciesFrequency();
        }
        LengthWeightConversionWithContextCache conversionHelper = getConfiguration().getConversionHelper();

        Multimap<Species, SetSpeciesFrequency> setSpeciesFrequencyBySpecies = SpeciesTopiaDao.groupBySpecies(activity.getSetSpeciesFrequency());
        Multimap<Species, Integer> lengthClassesBySpecies = ArrayListMultimap.create();
        // collect all length classes by species
        SetSpeciesFrequencyTopiaDao.collectLengthClasses(setSpeciesFrequencyBySpecies, lengthClassesBySpecies);
        List<SetSpeciesFrequency> result = new LinkedList<>();
        for (Map.Entry<Species, Collection<SetSpeciesFrequency>> entry : setSpeciesFrequencyBySpecies.asMap().entrySet()) {
            Species species = entry.getKey();
            LengthWeightConversion conversion = conversionHelper.getConversions(species);
            Collection<SetSpeciesFrequency> setSizes = entry.getValue();
            List<Integer> lengthClasses = new ArrayList<>(lengthClassesBySpecies.get(species));
            Collections.sort(lengthClasses);
            List<Integer> lengthClassesAccepted = conversionHelper.getLengthClasses(conversion, stratumWeightCategoryTreatment, lengthClasses);
            result.addAll(setSizes.stream().filter(f -> lengthClassesAccepted.contains(f.getLfLengthClass())).collect(Collectors.toList()));
        }
        return result;
    }

    protected void fillWeightsFromCatchesWeight(Activity activity, WeightCompositionAggregateModel model, int dividerFactor) {
        Map<WeightCategoryTreatment, Map<Species, Float>> weights = new HashMap<>();
        Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> data = getCorrectedElementaryCatchByCategory(activity);
        T3IOUtil.fillMapWithDefaultValue(weights, data.keySet(), HashMap::new);
        for (WeightCategoryTreatment weightCategory : data.keySet()) {
            Map<Species, Float> speciesFloatMap = weights.get(weightCategory);
            Collection<CorrectedElementaryCatch> correctedElementaryCatches = data.get(weightCategory);
            fillWeights(weightCategory,
                    correctedElementaryCatches,
                    speciesFloatMap,
                    CorrectedElementaryCatch::getCatchWeight,
                    model,
                    dividerFactor);
        }
    }

    protected void fillWeightsFromSamplesWeight(Activity activity,
                                                Map<WeightCategoryTreatment, Map<Species, Float>> weights,
                                                WeightCompositionAggregateModel model) {
        if (weights == null) {
            weights = new HashMap<>();
        }
        Multimap<WeightCategoryTreatment, SetSpeciesCatWeight> data = getSetSpeciesCatWeightByCategory(activity);
        T3IOUtil.fillMapWithDefaultValue(weights, data.keySet(), HashMap::new);
        for (WeightCategoryTreatment weightCategory : data.keySet()) {
            Map<Species, Float> speciesFloatMap = weights.get(weightCategory);
            Collection<SetSpeciesCatWeight> setSpeciesCatWeights = data.get(weightCategory);
            fillWeights(weightCategory, setSpeciesCatWeights, speciesFloatMap, SetSpeciesCatWeight::getWeight, model, 1);
        }
    }

    protected <X extends WeightCategoryTreatmentAware> void fillWeights(WeightCategoryTreatment weightCategory,
                                                                        Collection<X> data,
                                                                        Map<Species, Float> weights,
                                                                        Function<X, Float> function,
                                                                        WeightCompositionAggregateModel model,
                                                                        int divideFactor) {
        if (weights == null) {
            weights = new HashMap<>();
        }
        for (X datum : data) {
            Species species = datum.getSpecies();
            Float weight = weights.get(species);
            if (weight == null) {
                weight = 0f;
            }
            weight += function.apply(datum) / divideFactor;
            weights.put(species, weight);
        }
        if (model != null) {
            model.addModel(weightCategory, weights);
        }
    }

}
