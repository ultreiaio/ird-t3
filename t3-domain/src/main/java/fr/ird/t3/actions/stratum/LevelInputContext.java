package fr.ird.t3.actions.stratum;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.cache.ActivityCache;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.cache.WeightCategoryTreatmentCache;
import fr.ird.t3.entities.cache.ZoneStratumCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3TopiaPersistenceContextAware;
import fr.ird.t3.services.ZoneStratumService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import fr.ird.t3.services.ioc.InjectFromDAO;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * A context to get all input data for the execution of action.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class LevelInputContext<C extends LevelConfigurationWithStratum, A extends T3Action<C>> implements T3TopiaPersistenceContextAware {

    /**
     * Cache of activity (to avoid to reload them for catch and sample stratum).
     */
    private final ActivityCache activityCache;
    /**
     * Cache of {@link WeightCategoryTreatment}.
     */
    private final WeightCategoryTreatmentCache weightCategoryCache;
    /**
     * Cache of {@link ZoneStratumAware}.
     */
    private final ZoneStratumCache zoneCache;
    /**
     * Invoking action.
     */
    private final A action;
    /**
     * Available start dates.
     */
    private final Set<T3Date> startDates;
    /**
     * Available catch vessels.
     */
    private final Set<Vessel> possibleCatchVessels;
    /**
     * Available sample vessels.
     */
    private final Set<Vessel> possibleSampleVessels;
    /**
     * Zone meta used by the action configuration.
     */
    private final ZoneStratumAwareMeta zoneMeta;
    /**
     * Predicate to filter only species selected in configuration.
     */
    private final Predicate<? super SpeciesAware> speciesToFixFilter;
    /**
     * Number of stratum.
     */
    private final int nbStratum;
    /**
     * {@link Activity} dao.
     */
    @InjectDAO(entityType = Activity.class)
    private ActivityTopiaDao activityDAO;
    /**
     * {@link Vessel} dao.
     */
    @InjectDAO(entityType = Vessel.class)
    private VesselTopiaDao vesselDAO;
    /**
     * Available school type (from configuration).
     */
    @InjectFromDAO(entityType = SchoolType.class, method = "findAllForStratum")
    private Set<SchoolType> schoolTypes;
    /**
     * Available catch fleet (from configuration).
     */
    @InjectEntitiesById(entityType = Country.class)
    private Collection<Country> catchFleets;
    /**
     * Available ocean (from configuration).
     */
    @InjectEntitiesById(entityType = Ocean.class)
    private Collection<Ocean> oceans;
    /**
     * Available species (from configuration).
     */
    @InjectEntitiesById(entityType = Species.class, path = "configuration.speciesIds")
    private Collection<Species> species;
    /**
     * Available sample fleet (from configuration).
     */
    @InjectEntitiesById(entityType = Country.class)
    private Collection<Country> sampleFleets;
    /**
     * Available sample flags (from configuration).
     */
    @InjectEntitiesById(entityType = Country.class)
    private Collection<Country> sampleFlags;

    public LevelInputContext(A action) {
        this.action = action;
        try {
            action.newService(IOCService.class).injectExcept(this);
        } catch (Exception e) {
            throw new IllegalStateException("can't inject to " + this, e);
        }
        T3TopiaPersistenceContext persistenceContext = action.getT3TopiaPersistenceContext().get();
        this.activityCache = persistenceContext.newActivityCache();
        this.weightCategoryCache = persistenceContext.newWeightCategoryTreatmentCache();
        C configuration = action.getConfiguration();
        configuration.setLocale(action.getLocale());
        // get zones type meta to use
        this.zoneMeta = createZoneMeta(configuration.getZoneTypeId());
        this.zoneCache = createZoneCache(persistenceContext, zoneMeta, configuration.getZoneVersionId());
        // get start dates to use
        this.startDates = T3Date.getStartDates(configuration.getBeginDate(), configuration.getEndDate(), configuration.getTimeStep());
        // get possible vessels for catch stratum
        this.possibleCatchVessels = vesselDAO.getPossibleCatchVessels(catchFleets);
        // get possible vessels for sample stratum
        this.possibleSampleVessels = vesselDAO.getPossibleSampleVessels(sampleFleets, sampleFlags);
        this.speciesToFixFilter = T3Predicates.newSpeciesContainsFilter(species);

        if (action.getConfiguration().isUseWeightCategoriesInStratum()) {
            int nbStratum = 0;
            for (SchoolType schoolType : schoolTypes) {
                List<WeightCategoryTreatment> weightCategories = getWeightCategoryTreatments(schoolType);
                Collection<ZoneStratumAware> zones = getZones(schoolType);
                nbStratum += startDates.size() * zones.size() * weightCategories.size();
            }
            this.nbStratum = nbStratum;
        } else {
            int nbStratum = 0;
            for (SchoolType schoolType : schoolTypes) {
                Collection<ZoneStratumAware> zones = getZones(schoolType);
                nbStratum += startDates.size() * zones.size();
            }
            this.nbStratum = nbStratum;
        }
    }

    protected ZoneStratumCache createZoneCache(T3TopiaPersistenceContext persistenceContext, ZoneStratumAwareMeta zoneMeta, String zoneVersionId) {
        return persistenceContext.newZoneStratumCache(zoneMeta, zoneVersionId);
    }

    protected ZoneStratumAwareMeta createZoneMeta(String zoneTypeId) {
        return action.newService(ZoneStratumService.class).getZoneMetaById(zoneTypeId);
    }

    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return action.getT3TopiaPersistenceContext();
    }

    public LengthWeightConversionWithContextCache newLengthWeightConversionWithContextCache(Ocean ocean, Date beginDate) {
        return getT3TopiaPersistenceContext().get().newLengthWeightConversionWithContextCache(ocean, beginDate);
    }

    public int getNbStratum() {
        return nbStratum;
    }

    public ActivityCache getActivityCache() {
        return activityCache;
    }

    public ZoneStratumAwareMeta getZoneMeta() {
        return zoneMeta;
    }

    public Set<SchoolType> getSchoolTypes() {
        return schoolTypes;
    }

    public Collection<Country> getCatchFleets() {
        return catchFleets;
    }

    public Collection<Ocean> getOceans() {
        return oceans;
    }

    public Collection<Species> getSpecies() {
        return species;
    }

    public void setSpecies(Set<Species> species) {
        this.species = species;
    }

    public Collection<Country> getSampleFleets() {
        return sampleFleets;
    }

    public Collection<Country> getSampleFlags() {
        return sampleFlags;
    }

    public Set<T3Date> getStartDates() {
        return startDates;
    }

    public Set<Vessel> getPossibleCatchVessels() {
        return possibleCatchVessels;
    }

    public Set<Vessel> getPossibleSampleVessels() {
        return possibleSampleVessels;
    }

    public List<WeightCategoryTreatment> getWeightCategoryTreatments(SchoolType schoolType) {
        return weightCategoryCache.forOceansAndSchoolType(oceans, schoolType);
    }

    public List<ZoneStratumAware> getZones(SchoolType schoolType) {
        return zoneCache.forOceansAndSchoolType(oceans, schoolType);
    }

    public C getConfiguration() {
        return action.getConfiguration();
    }

    public Predicate<? super SpeciesAware> getSpeciesToFixFilter() {
        return speciesToFixFilter;
    }

    public void clear() {
        activityCache.clear();
    }

    public void loadActivities(Set<String> activityIds) {
        activityCache.load(activityIds);
    }
}
