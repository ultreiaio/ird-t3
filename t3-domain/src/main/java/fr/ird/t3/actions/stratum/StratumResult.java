/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import fr.ird.t3.entities.reference.WeightCategoryTreatment;

import java.io.Closeable;

/**
 * Result for a stratum.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public abstract class StratumResult<C extends LevelConfigurationWithStratum> implements Closeable {

    private final StratumConfiguration<C, ?> configuration;

    private final String label;

    private int substitutionLevel;

    private int nbActivities;

    private int nbActivitiesWithSample;

    protected StratumResult(StratumConfiguration<C, ?> configuration, String label) {
        this.configuration = configuration;
        this.label = label;
    }

    public StratumConfiguration<C, ?> getConfiguration() {
        return configuration;
    }

    public String getLabel() {
        return label;
    }

    public int getSubstitutionLevel() {
        return substitutionLevel;
    }

    public void setSubstitutionLevel(int substitutionLevel) {
        this.substitutionLevel = substitutionLevel;
    }

    public int getNbActivities() {
        return nbActivities;
    }

    public void setNbActivities(int nbActivities) {
        this.nbActivities = nbActivities;
    }

    public int getNbActivitiesWithSample() {
        return nbActivitiesWithSample;
    }

    public void setNbActivitiesWithSample(int nbActivitiesWithSample) {
        this.nbActivitiesWithSample = nbActivitiesWithSample;
    }

    public int getNbActivitiesWithoutSample() {
        return nbActivities - nbActivitiesWithSample;
    }

    public void addNbActivities(int nbActivities) {
        this.nbActivities += nbActivities;
    }

    public void addNbActivitiesWithSample(int nbActivitiesWithSample) {
        this.nbActivitiesWithSample += nbActivitiesWithSample;
    }

    public WeightCategoryTreatment getWeightCategoryTreatment() {
        return configuration.getWeightCategoryTreatment();
    }
}
