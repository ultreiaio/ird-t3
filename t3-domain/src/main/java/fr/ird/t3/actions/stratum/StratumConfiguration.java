/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.collect.ImmutableSet;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SchoolTypeTopiaDao;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.type.T3Date;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Configuration of a stratum.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StratumConfiguration<C extends LevelConfigurationWithStratum, A extends T3Action<C>> {

    private final LevelInputContext<C, A> inputContext;
    private final Collection<ZoneStratumAware> zones;
    private final ZoneStratumAware zone;
    private final SchoolType schoolType;
    private final WeightCategoryTreatment weightCategoryTreatment;
    private final T3Date beginDate;
    private final T3Date endDate;
    private final Map<String, Integer> stratumMinimumCountBySpecie;
    private final List<WeightCategoryTreatment> weightCategoryTreatments;
    private final int stratumIndex;
    private final LengthWeightConversionWithContextCache conversionHelper;
    private String[] zoneIds;

    protected StratumConfiguration(LevelInputContext<C, A> inputContext,
                                   ZoneStratumAware zone,
                                   SchoolType schoolType,
                                   WeightCategoryTreatment weightCategoryTreatment,
                                   T3Date beginDate,
                                   T3Date endDate,
                                   Collection<ZoneStratumAware> zones,
                                   Map<String, Integer> stratumMinimumCountBySpecie,
                                   List<WeightCategoryTreatment> weightCategoryTreatments,
                                   int stratumIndex) {
        this.inputContext = inputContext;
        this.zone = zone;
        this.schoolType = schoolType;
        this.weightCategoryTreatments = weightCategoryTreatments;
        this.weightCategoryTreatment = weightCategoryTreatment;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.zones = zones;
        this.stratumMinimumCountBySpecie = stratumMinimumCountBySpecie;
        this.stratumIndex = stratumIndex;
        this.conversionHelper = inputContext.newLengthWeightConversionWithContextCache(zone.getOcean(), beginDate.toBeginDate());
    }

    public LengthWeightConversionWithContextCache getConversionHelper() {
        return conversionHelper;
    }

    public C getConfiguration() {
        return inputContext.getConfiguration();
    }

    private ZoneStratumAwareMeta getZoneMeta() {
        return inputContext.getZoneMeta();
    }

    public String getZoneTableName() {
        return getZoneMeta().getTableName();
    }

    public ZoneStratumAware getZone() {
        return zone;
    }

    public SchoolType getSchoolType() {
        return schoolType;
    }

    public Set<String> getSchoolTypeIds() {
        ImmutableSet.Builder<String> builder = ImmutableSet.<String>builder().add(getSchoolType().getTopiaId());
        switch (getConfiguration().getSchoolTypeIndeterminate()) {
            case ALL_IN_BO:
                if (SchoolTypeTopiaDao.SCHOOL_TYPE_BO_ID.equals(getSchoolType().getTopiaId())) {
                    builder.add(SchoolTypeTopiaDao.SCHOOL_TYPE_INDETERMINATE_ID);
                }
                break;
            case ALL_IN_BL:
                if (SchoolTypeTopiaDao.SCHOOL_TYPE_BL_ID.equals(getSchoolType().getTopiaId())) {
                    builder.add(SchoolTypeTopiaDao.SCHOOL_TYPE_INDETERMINATE_ID);
                }
                break;
            case IGNORE:
                break;
        }
        return builder.build();
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public Collection<ZoneStratumAware> getZones() {
        return zones;
    }

    public Set<Vessel> getPossibleCatchVessels() {
        return inputContext.getPossibleCatchVessels();
    }

    public Set<Vessel> getPossibleSampleVessels() {
        return inputContext.getPossibleSampleVessels();
    }

    public Map<String, Integer> getStratumMinimumCountBySpecie() {
        return stratumMinimumCountBySpecie;
    }

    public WeightCategoryTreatment getWeightCategoryTreatment() {
        return weightCategoryTreatment;
    }

    public int getStratumIndex() {
        return stratumIndex;
    }

    public List<WeightCategoryTreatment> getWeightCategoryTreatments() {
        return weightCategoryTreatments;
    }

    public Predicate<? super SpeciesAware> getSpeciesToFixFilter() {
        return inputContext.getSpeciesToFixFilter();
    }

    public String[] getZoneIds() {
        if (zoneIds == null) {
            Collection<ZoneStratumAware> allZones = getZones();
            Iterator<ZoneStratumAware> itr = getZones().iterator();
            int size = allZones.size();
            zoneIds = new String[size];
            for (int i = 0; i < size; i++) {
                zoneIds[i] = itr.next().getTopiaId();
            }
        }
        return zoneIds;
    }

    public Activity getActivity(String activityId) {
        return inputContext.getActivityCache().forId(activityId);
    }

    public void loadActivities(Set<String> activityIds) {
        inputContext.getActivityCache().load(activityIds);
    }

    @Override
    public String toString() {
        return String.format("%s [schoolType: %s, zone: %s, startDate:%s, endDate:%s, weightCategory: %s]", super.toString(), schoolType.getLabel1(), zone.getTopiaId(), beginDate, endDate, weightCategoryTreatment);
    }

    public void addToActivity(Activity activity, Function<Activity, String> function, BiConsumer<Activity, String> consumer, int level) {
        String currentLevel = Objects.requireNonNull(function).apply(Objects.requireNonNull(activity));
        String result = currentLevel;
        if (currentLevel == null) {
            result = "";
        }
        result += String.format("(%s;%s;%d)", zone.getLabel1(), getConfiguration().isUseWeightCategoriesInStratum() ? weightCategoryTreatment.getLabel1() : "NA", level);
        Objects.requireNonNull(consumer).accept(activity, result);
    }
}
