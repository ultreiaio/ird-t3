/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import java.io.Serializable;

/**
 * To keep minimum sample count required for each school type in level 2 and
 * 3 treatments.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StratumMinimumSampleCount implements Serializable {

    private static final long serialVersionUID = 1L;

    protected Integer minimumCountForFreeSchool;

    protected Integer minimumCountForObjectSchool;

    public Integer getMinimumCountForFreeSchool() {
        return minimumCountForFreeSchool;
    }

    public void setMinimumCountForFreeSchool(Integer minimumCountForFreeSchool) {
        this.minimumCountForFreeSchool = minimumCountForFreeSchool;
    }

    public Integer getMinimumCountForObjectSchool() {
        return minimumCountForObjectSchool;
    }

    public void setMinimumCountForObjectSchool(Integer minimumCountForObjectSchool) {
        this.minimumCountForObjectSchool = minimumCountForObjectSchool;
    }
}
