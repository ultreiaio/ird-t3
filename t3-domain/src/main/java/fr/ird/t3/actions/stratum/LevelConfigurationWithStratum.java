/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Define the gloabal configuration of a level 2 or 3 treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class LevelConfigurationWithStratum implements T3ActionConfiguration {

    private static final long serialVersionUID = 1L;

    /** Id of selected type of ZoneImpl. */
    private String zoneTypeId;

    /** Id of selected version of ZoneImpl. */
    private String zoneVersionId;

    /** Selected fleet ids (for catch filtering). */
    private Set<String> catchFleetIds;

    /** Number of months to define a stratus. */
    private int timeStep;

    /** Start date for treatment. */
    private T3Date beginDate;

    /** End date for treatment. */
    private T3Date endDate;

    /** min begin date to use. */
    private T3Date minDate;

    /** max end date to use. */
    private T3Date maxDate;

    /** Maximum weight ratio (catch / sample) usable on a stratum. */
    private float stratumWeightRatio;

    /** Selected species ids to fix. */
    private Set<String> speciesIds;

    /** Selected fleet ids (for sample filtering). */
    private Set<String> sampleFleetIds;

    /** Selected flag ids (for sample filtering). */
    private Set<String> sampleFlagIds;

    /* Selected ocean ids. */
    private Set<String> oceanIds;

    /** Usable oceans. */
    private List<Ocean> oceans;

    /** Usable species. */
    private List<Species> species;

    /** Usable catch fleets. */
    private List<Country> catchFleets;

    /** Usable sample flags. */
    private List<Country> sampleFleets;

    /** Usable sample fleets. */
    private List<Country> sampleFlags;

    /** Usable zone's types. */
    private List<ZoneStratumAwareMeta> zoneTypes;

    /** Usable zone's versions. */
    private List<ZoneVersion> zoneVersions;

    /**
     * Flag to add weight categories as a component of the stratum hierarchy.
     */
    private boolean useWeightCategoriesInStratum;

    /**
     * How to use indeterminate school type (as BO or as BI).
     */
    private SchoolTypeIndeterminate schoolTypeIndeterminate;
    /**
     * flag to validate the step one of configuration.
     * <p/>
     * The first step is the general configuration.
     */
    private boolean validStep1;

    /**
     * Flag to validate the step two of configuration.
     * <p/>
     * The second step is the sample selection configuration.
     */
    private boolean validStep2;

    private Locale locale;

    public Set<String> getSpeciesIds() {
        if (speciesIds == null) {
            speciesIds = new HashSet<>();
        }
        return speciesIds;
    }

    public void setSpeciesIds(Set<String> speciesIds) {
        this.speciesIds = speciesIds;
    }

    public String getZoneTypeId() {
        return zoneTypeId;
    }

    public void setZoneTypeId(String zoneTypeId) {
        this.zoneTypeId = zoneTypeId;
    }

    public String getZoneVersionId() {
        return zoneVersionId;
    }

    public void setZoneVersionId(String zoneVersionId) {
        this.zoneVersionId = zoneVersionId;
    }

    public int getTimeStep() {
        return timeStep;
    }

    public void setTimeStep(int timeStep) {
        this.timeStep = timeStep;
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(T3Date beginDate) {
        this.beginDate = beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public void setEndDate(T3Date endDate) {
        this.endDate = endDate;
    }

    public Set<String> getCatchFleetIds() {
        return catchFleetIds == null ? catchFleetIds = new LinkedHashSet<>() : catchFleetIds;
    }

    public void setCatchFleetIds(Set<String> catchFleetIds) {
        this.catchFleetIds = catchFleetIds;
    }

    public Set<String> getSampleFleetIds() {
        return sampleFleetIds == null ? sampleFleetIds = new LinkedHashSet<>() : sampleFleetIds;
    }

    public void setSampleFleetIds(Set<String> sampleFleetIds) {
        this.sampleFleetIds = sampleFleetIds;
    }

    public Set<String> getSampleFlagIds() {
        return sampleFlagIds == null ? sampleFlagIds = new LinkedHashSet<>() : sampleFlagIds;
    }

    public void setSampleFlagIds(Set<String> sampleFlagIds) {
        this.sampleFlagIds = sampleFlagIds;
    }

    public Set<String> getOceanIds() {
        return oceanIds == null ? oceanIds = new LinkedHashSet<>() : oceanIds;
    }

    public void setOceanIds(Set<String> oceanIds) {
        this.oceanIds = oceanIds;
    }

    public float getStratumWeightRatio() {
        return stratumWeightRatio;
    }

    public void setStratumWeightRatio(float stratumWeightRatio) {
        this.stratumWeightRatio = stratumWeightRatio;
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    public void setOceans(List<Ocean> oceans) {
        this.oceans = oceans;
    }

    public List<Species> getSpecies() {
        return species;
    }

    public void setSpecies(List<Species> species) {
        this.species = species;
    }

    public List<Country> getCatchFleets() {
        return catchFleets;
    }

    public void setCatchFleets(List<Country> catchFleets) {
        this.catchFleets = catchFleets;
    }

    public List<Country> getSampleFleets() {
        return sampleFleets;
    }

    public void setSampleFleets(List<Country> sampleFleets) {
        this.sampleFleets = sampleFleets;
    }

    public List<Country> getSampleFlags() {
        return sampleFlags;
    }

    public void setSampleFlags(List<Country> sampleFlags) {
        this.sampleFlags = sampleFlags;
    }

    public List<ZoneStratumAwareMeta> getZoneTypes() {
        return zoneTypes;
    }

    public void setZoneTypes(List<ZoneStratumAwareMeta> zoneTypes) {
        this.zoneTypes = zoneTypes;
    }

    public List<ZoneVersion> getZoneVersions() {
        return zoneVersions == null ? zoneVersions = new LinkedList<>() : zoneVersions;
    }

    public void setZoneVersions(List<ZoneVersion> zoneVersions) {
        this.zoneVersions = zoneVersions;
    }

    public boolean isValidStep1() {
        return validStep1;
    }

    public void setValidStep1(boolean validStep1) {
        this.validStep1 = validStep1;
    }

    public boolean isValidStep2() {
        return validStep2;
    }

    public void setValidStep2(boolean validStep2) {
        this.validStep2 = validStep2;
    }

    public T3Date getMinDate() {
        return minDate;
    }

    public void setMinDate(T3Date minDate) {
        this.minDate = minDate;
    }

    public T3Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(T3Date maxDate) {
        this.maxDate = maxDate;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public boolean isUseWeightCategoriesInStratum() {
        return useWeightCategoriesInStratum;
    }

    public void setUseWeightCategoriesInStratum(boolean useWeightCategoriesInStratum) {
        this.useWeightCategoriesInStratum = useWeightCategoriesInStratum;
    }

    public SchoolTypeIndeterminate getSchoolTypeIndeterminate() {
        return schoolTypeIndeterminate;
    }

    public void setSchoolTypeIndeterminate(SchoolTypeIndeterminate schoolTypeIndeterminate) {
        this.schoolTypeIndeterminate = schoolTypeIndeterminate;
    }
}
