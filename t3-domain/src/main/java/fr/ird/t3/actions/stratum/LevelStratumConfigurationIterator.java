package fr.ird.t3.actions.stratum;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.lang3.mutable.MutableInt;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Given a {@link LevelInputContext}, produces an iterator of {@link StratumConfiguration} to consume in action.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class LevelStratumConfigurationIterator<C extends LevelConfigurationWithStratum, A extends T3Action<C>, I extends LevelInputContext<C, A>, S extends StratumConfiguration> implements Iterator<S> {

    private final I inputContext;
    private final int nbStratum;
    private final MutableInt currentIndex;

    private final Iterator<SchoolType> schoolTypeIterator;
    private Iterator<ZoneStratumAware> zoneIterator;
    private Iterator<WeightCategoryTreatment> weightCategoryTreatmentIterator;
    private Iterator<T3Date> startDateIterator;

    private SchoolType currentSchoolType;
    private List<ZoneStratumAware> currentZones;
    private List<WeightCategoryTreatment> currentWeightCategoryTreatments;
    private ZoneStratumAware currentZone;
    private WeightCategoryTreatment currentWeightCategoryTreatment;
    private T3Date currentStartDate;

    LevelStratumConfigurationIterator(I inputContext) {
        this.inputContext = inputContext;
        this.nbStratum = inputContext.getNbStratum();
        this.currentIndex = new MutableInt();
        this.schoolTypeIterator = inputContext.getSchoolTypes().iterator();
    }

    protected abstract S newConfiguration(I inputContext,
                                          SchoolType schoolType,
                                          ZoneStratumAware zone,
                                          WeightCategoryTreatment weightCategoryTreatment,
                                          T3Date beginDate,
                                          int stratumIndex,
                                          Collection<ZoneStratumAware> zones,
                                          List<WeightCategoryTreatment> weightCategoryTreatments);

    protected abstract void next0();

    @Override
    public boolean hasNext() {
        return currentIndex.intValue() < nbStratum;
    }

    @Override
    public S next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        if (currentSchoolType == null) {
            // first element
            nextSchoolType();
        } else {
            next0();
        }
        return newConfiguration(inputContext,
                currentSchoolType,
                currentZone,
                currentWeightCategoryTreatment,
                currentStartDate,
                currentIndex.incrementAndGet(),
                currentZones,
                currentWeightCategoryTreatments);
    }

    boolean hasMoreSchoolType() {
        return schoolTypeIterator.hasNext();
    }

    boolean hasMoreZone() {
        return zoneIterator != null && zoneIterator.hasNext();
    }

    boolean hasMoreWeightCategoryTreatment() {
        return weightCategoryTreatmentIterator != null && weightCategoryTreatmentIterator.hasNext();
    }

    boolean hasMoreStartDate() {
        return startDateIterator != null && startDateIterator.hasNext();
    }

    protected void nextSchoolType() {
        // each time we change school type, we are sure to not cross again a previous loaded activity, so can clear cache
        inputContext.getActivityCache().clear();
        currentSchoolType = schoolTypeIterator.next();
        currentZones = inputContext.getZones(currentSchoolType);
        currentWeightCategoryTreatments = inputContext.getWeightCategoryTreatments(currentSchoolType);
    }

    protected void nextZone(boolean reset) {
        if (zoneIterator == null || reset) {
            zoneIterator = currentZones.iterator();
        }
        currentZone = zoneIterator.next();
    }

    void nextStartDate(boolean reset) {
        if (startDateIterator == null || reset) {
            startDateIterator = inputContext.getStartDates().iterator();
        }
        currentStartDate = startDateIterator.next();
    }

    void nextWeightCategoryTreatment(boolean reset) {
        if (weightCategoryTreatmentIterator == null || reset) {
            weightCategoryTreatmentIterator = currentWeightCategoryTreatments.iterator();
        }
        currentWeightCategoryTreatment = weightCategoryTreatmentIterator.next();
        nextStartDate(true);
    }

    protected I getInputContext() {
        return inputContext;
    }

    protected SchoolType getCurrentSchoolType() {
        return currentSchoolType;
    }


    public static abstract class LevelStratumConfigurationIteratorWithoutCategories<C extends LevelConfigurationWithStratum, A extends T3Action<C>, I extends LevelInputContext<C, A>, S extends StratumConfiguration> extends LevelStratumConfigurationIterator<C, A, I, S> {

        protected LevelStratumConfigurationIteratorWithoutCategories(I actionContext) {
            super(actionContext);
        }

        @Override
        protected void next0() {
            if (hasMoreStartDate()) {
                nextStartDate(false);
            } else if (hasMoreZone()) {
                nextZone(false);
            } else if (hasMoreSchoolType()) {
                nextSchoolType();
            }
        }

        @Override
        protected void nextZone(boolean reset) {
            super.nextZone(reset);
            nextStartDate(true);
        }
    }

    public static abstract class LevelStratumConfigurationIteratorWithCategories<C extends LevelConfigurationWithStratum, A extends T3Action<C>, I extends LevelInputContext<C, A>, S extends StratumConfiguration> extends LevelStratumConfigurationIterator<C, A, I, S> {

        protected LevelStratumConfigurationIteratorWithCategories(I actionContext) {
            super(actionContext);
        }

        @Override
        protected void next0() {
            if (hasMoreStartDate()) {
                nextStartDate(false);
            } else if (hasMoreWeightCategoryTreatment()) {
                nextWeightCategoryTreatment(false);
            } else if (hasMoreZone()) {
                nextZone(false);
            } else if (hasMoreSchoolType()) {
                nextSchoolType();
            }
        }

        @Override
        protected void nextZone(boolean reset) {
            super.nextZone(reset);
            nextWeightCategoryTreatment(true);
        }
    }

}
