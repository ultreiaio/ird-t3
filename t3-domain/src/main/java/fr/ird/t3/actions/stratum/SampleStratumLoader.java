/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.base.Preconditions;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3TopiaPersistenceContextAware;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * To loader a sample stratum.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SampleStratum
 * @since 1.3
 */
public abstract class SampleStratumLoader<C extends LevelConfigurationWithStratum, A extends T3Action<C>, T extends StratumConfiguration<C, A>, S extends SampleStratum<C, A, T, S>> implements T3TopiaPersistenceContextAware {

    private static final Log log = LogFactory.getLog(SampleStratumLoader.class);
    private final C levelConfiguration;
    /** The sample stratum to load. */
    private final S sampleStratum;
    protected Supplier<T3TopiaPersistenceContext> transaction;
    /**
     * The level of substitution used.
     * <p/>
     * If there is no need of substitution then level is {@code 0}.
     */
    private int substitutionLevel;

    protected SampleStratumLoader(S sampleStratum) {
        this.sampleStratum = sampleStratum;
        this.levelConfiguration = sampleStratum.getConfiguration().getConfiguration();
    }

    /**
     * For a given {@code level} of stratum substitution, find the usable
     * activities.
     * <p/>
     * These activities will after be filtered using the stratum configuration.
     *
     * @param level the substitution level to use which begins at 0
     * @return the set of activity ids usable
     * @throws TopiaException if any database problem while loading data
     */
    protected abstract Set<String> findActivityIds(int level);

    protected abstract Set<String> findActivityIds(String schoolTypeId, T3Date beginDate, T3Date endDate, String... zoneIds);

    protected abstract boolean isStratumOk();

    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return transaction;
    }

    public void setTransaction(Supplier<T3TopiaPersistenceContext> transaction) {
        this.transaction = transaction;
    }

    public Locale getLocale() {
        return levelConfiguration.getLocale();
    }

    public final Map<Activity, Integer> loadData(T3ServiceContext serviceContext, A messager) {
        substitutionLevel = 1;
        boolean needAnotherRound = true;
        Set<Activity> finalActivityIds = new HashSet<>();
        // to keep ids of all loaded activities (to avoid to reload them twice)
        Set<String> selectedActivitiesIds = new HashSet<>();
        while (needAnotherRound) {
            log.info(String.format("Try to load level %d", substitutionLevel));
            Set<String> activityIds = findActivityIds(substitutionLevel);
            if (activityIds == null) {
                // limit case : last level but still no data...
                substitutionLevel = -1;
                break;
            }
            // remove all activities ids already grabbed
            activityIds.removeAll(selectedActivitiesIds);
            log.info(String.format("Found %d new activities in this stratum at level %d", activityIds.size(), substitutionLevel));
            Set<Activity> activities = filterActivities(activityIds);
            log.info(String.format("Found %d new activities (after filter) in this stratum at level %d", activityIds.size(), substitutionLevel));
            finalActivityIds.addAll(activities);
            // these activities are now loaded
            selectedActivitiesIds.addAll(activityIds);
            // merge activities into the stratum
            sampleStratum.mergeNewActivities(serviceContext, activities);
            // log sample stratum new composition
            String message = sampleStratum.logSampleStratumLevel(substitutionLevel, messager);
            log.info(message);
            messager.addInfoMessage(message);
            needAnotherRound = !isStratumOk();
            if (needAnotherRound) {
                // go to next level
                log.info("Need another round");
                substitutionLevel++;
            }
        }
        Map<Activity, Integer> result = new HashMap<>();
        for (Activity activityId : finalActivityIds) {
            result.put(activityId, 1);
        }
        return result;
    }

    public int getSubstitutionLevel() {
        return substitutionLevel;
    }

    public S getSampleStratum() {
        return sampleStratum;
    }

    public int getTimeStep() {
        return levelConfiguration.getTimeStep();
    }

    /**
     * From a set of activity ids, filter the one that can be use in this
     * stratum. This mainly means to filter some of them using the stratum
     * configuration.
     *
     * @param activityIds ids of activities to filter
     * @return the set of usable activities after having
     * @throws TopiaException if any database problem while loading data
     */
    private Set<Activity> filterActivities(Set<String> activityIds) {
        Set<Activity> result = new HashSet<>();
        T configuration = getSampleStratum().getConfiguration();
        Set<Vessel> possibleVessels = configuration.getPossibleSampleVessels();
        configuration.loadActivities(activityIds);
        WeightCategoryTreatment weightCategoryTreatment = configuration.getWeightCategoryTreatment();
        for (String activityId : activityIds) {
            Activity activity = configuration.getActivity(activityId);
            // get his trip
            Trip trip = activity.getTrip();
            if (!possibleVessels.contains(trip.getVessel())) {
                // not a matching boat
                continue;
            }
            // recheck activity have some samples.
            Preconditions.checkState(activity.isSetSpeciesCatWeightNotEmpty(),
                    String.format("Can not accept an activity (%s) with no sample", activity.getTopiaId()));
            if (weightCategoryTreatment != null) {
                // weight category is in stratum, check there is catch using this category
                boolean foundWeightCategory = activity.withSetSpeciesCatWeight(weightCategoryTreatment);
                if (!foundWeightCategory) {
                    // reject - the stratum weight category not found for this activity
                    continue;
                }
            }
            result.add(activity);
        }
        return result;
    }
}
