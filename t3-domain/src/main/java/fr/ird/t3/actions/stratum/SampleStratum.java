/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.ioc.InjectDAO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Base samle stratum.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public abstract class SampleStratum<C extends LevelConfigurationWithStratum, A extends T3Action<C>, T extends StratumConfiguration<C, A>, S extends SampleStratum<C, A, T, S>> extends Stratum<C, A, T> {

    /**
     * Number of merged activites (counted when passing through method
     * {@link #mergeNewActivities(T3ServiceContext, Set)}.
     *
     * @since 1.3.2
     */
    private int nbMergedActivities;

    /**
     * Level of substitution used to obtain sample data.
     * <p/>
     * <strong>Note :</strong> This level will be later persist on all
     * activities of the catch stratum.
     * <strong>Note: </strong>This data are available after invocation of method
     * {@link Stratum#init(T3ServiceContext, List, T3Action)}.
     */
    private Integer substitutionLevel;

    protected SampleStratum(T configuration, Collection<Species> speciesToFix) {
        super(configuration, speciesToFix);
    }

    protected abstract SampleStratumLoader<C, A, T, S> newLoader();

    protected abstract String logSampleStratumLevel(int substitutionLevel, A messager);

    /**
     * Merge the given {@code activities} sample data in the stratum result.
     * <p/>
     * This means mainly to obtain some {@code setSpecieFrequencies} and
     * {@code setSpecieCatWeights} for the species to fix and merge them to
     * existing internal result.
     * <p/>
     * As a side effect, we should also compute internal states needed for the
     * substitution quality tests (says compute count of fishes, and total
     * weight of the sample).
     * <p/>
     * Yet another side-effect, will be to add number of activities to the
     * field {@link #nbMergedActivities}.
     *
     * @param serviceContext service context
     * @param activities     the activities to merge
     */
    protected abstract void mergeNewActivities(T3ServiceContext serviceContext, Set<Activity> activities);

    protected final void addMergedActivitiesCount(int nb) {
        nbMergedActivities += nb;
    }

    @Override
    public void init(T3ServiceContext serviceContext, List<WeightCategoryTreatment> weightCategories, A messager) throws Exception {

        SampleStratumLoader<C, A, T, S> stratumLoader = newLoader();

        // inject transaction in loader
        stratumLoader.setTransaction(serviceContext.getT3TopiaPersistenceContext());

        // inject daos in loader
        serviceContext.newService(IOCService.class).injectOnly(stratumLoader, InjectDAO.class);

        // get all catches usable in this stratum grouped by their owing activity
        Map<Activity, Integer> activities = stratumLoader.loadData(serviceContext, messager);
        setActivities(activities);

        substitutionLevel = stratumLoader.getSubstitutionLevel();
    }

    public final Integer getSubstitutionLevel() {
        return substitutionLevel;
    }

    public final int getNbMergedActivities() {
        return nbMergedActivities;
    }

}
