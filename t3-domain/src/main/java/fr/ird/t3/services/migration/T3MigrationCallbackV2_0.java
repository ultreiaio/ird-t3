package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Versions;

/**
 * Created on 25/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV2_0 extends T3MigrationCallbackSupport {

    public T3MigrationCallbackV2_0() {
        super(Versions.valueOf("2.0"));
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql, boolean showProgression) throws TopiaException {

        // See https://forge.codelutin.com/issues/7765
        addScript("01", "add-WeightCategorySample-table", queries);
        addScript("02", "add-count-column-on-samplesetcatweight", queries);
        addScript("03", "add-fields-to-vessel", queries);
        addScript("04", "add-fields-to-lengthWeightConversion", queries);
        addScript("05", "add-fpaZone-table", queries);
        addScript("06", "add-dcp-tables", queries);
        addScript("07", "add-fiels-to-Activity", queries);
        addScript("08", "add-localMarket-tables", queries);
        addScript("09", "add-elementaryLandingFate-table", queries);
        addScript("10", "add-status-on-referential-tables", queries);
        addScript("11", "add-RfUsageStatus-on-sampleWell", queries);
        addScript("12", "add-ElementaryCatchFate-table", queries);
        addScript("13", "add-Company-table", queries);
        addScript("14", "add-harbour-ocean-link", queries);

    }

}
