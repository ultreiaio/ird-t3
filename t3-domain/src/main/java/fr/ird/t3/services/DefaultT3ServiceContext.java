/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.T3Config;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.user.T3Users;

import java.util.Date;
import java.util.Locale;
import java.util.function.Supplier;

/**
 * Default implementation of the service context.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class DefaultT3ServiceContext implements T3ServiceContext {

    private final T3Config applicationConfiguration;
    private final T3ServiceFactory serviceFactory;
    private final T3TopiaApplicationContext applicationContext;
    private final T3Users t3Users;
    private Supplier<T3TopiaPersistenceContext> transaction;
    private Locale locale;

    private DefaultT3ServiceContext(Locale locale,
                                    T3Users t3Users,
                                    T3TopiaApplicationContext applicationContext,
                                    Supplier<T3TopiaPersistenceContext> transaction,
                                    T3Config applicationConfiguration,
                                    T3ServiceFactory serviceFactory) {
        this.locale = locale;
        this.t3Users = t3Users;
        this.applicationContext = applicationContext;
        this.transaction = transaction;
        this.applicationConfiguration = applicationConfiguration;
        this.serviceFactory = serviceFactory;
    }

    public static DefaultT3ServiceContext newContext(
            DefaultT3ServiceContext serviceContext,
            T3Users t3Users,
            T3TopiaApplicationContext transactionContext,
            Supplier<T3TopiaPersistenceContext> transaction) {
        return newContext(serviceContext.getLocale(),
                t3Users,
                transactionContext,
                transaction,
                serviceContext.getApplicationConfiguration(),
                serviceContext.getServiceFactory()
        );
    }

    public static DefaultT3ServiceContext newContext(
            Locale locale,
            T3Users t3Users,
            T3TopiaApplicationContext transactionContext,
            Supplier<T3TopiaPersistenceContext> transaction,
            T3Config applicationConfiguration,
            T3ServiceFactory serviceFactory) {
        return new DefaultT3ServiceContext(locale,
                t3Users,
                transactionContext,
                transaction,
                applicationConfiguration,
                serviceFactory);
    }


    @Override
    public T3TopiaApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public T3Users getT3Users() {
        return t3Users;
    }

    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return transaction;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public T3Config getApplicationConfiguration() {
        return applicationConfiguration;
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

    @Override
    public void setTransaction(T3TopiaPersistenceContext transaction) {
        this.transaction = () -> transaction;
    }

    @Override
    public Date getCurrentDate() {
        return new Date();
    }
}
