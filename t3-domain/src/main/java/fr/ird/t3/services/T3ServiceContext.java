/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.T3Config;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.user.T3Users;

import java.util.Date;
import java.util.Locale;
import java.util.function.Supplier;

/**
 * This contract represents objects you must provide when asking for a service.
 * Objects provided may be injected in services returned by
 * {@link T3ServiceFactory#newService(Class, T3ServiceContext)}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public interface T3ServiceContext {

    T3Users getT3Users();

    T3TopiaApplicationContext getApplicationContext();

    Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext();

    Locale getLocale();

    T3Config getApplicationConfiguration();

    T3ServiceFactory getServiceFactory();

    <E extends T3Service> E newService(Class<E> clazz);

    void setTransaction(T3TopiaPersistenceContext transaction);

    void setLocale(Locale locale);

    Date getCurrentDate();
}
