/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.migration;

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration for version {@code 1.1}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV1_1 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return Versions.valueOf("1.1");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // add index creates
        queries.add("CREATE INDEX idx_Well_wellPlan ON wellPlan(well);");
        queries.add("CREATE INDEX idx_Well_wellSetAllSpecies ON wellSetAllSpecies(well);");
        queries.add("CREATE INDEX idx_Trip_activity ON activity(trip);");
        queries.add("CREATE INDEX idx_Trip_elementaryLanding ON elementaryLanding(trip);");
        queries.add("CREATE INDEX idx_Trip_sample ON sample(trip);");
        queries.add("CREATE INDEX idx_Trip_well ON well(trip);");
        queries.add(
                "CREATE INDEX idx_StandardiseSampleSpecies_standardiseSampleSpeciesFrequency ON standardiseSampleSpeciesFrequency(standardiseSampleSpecies);");
        queries.add(
                "CREATE INDEX idx_SampleWell_sampleSetSpeciesFrequency ON sampleSetSpeciesFrequency(sampleWell);");
        queries.add(
                "CREATE INDEX idx_SampleSpecies_sampleSpeciesFrequency ON sampleSpeciesFrequency(sampleSpecies);");
        queries.add("CREATE INDEX idx_Sample_sampleWell ON sampleWell(sample);");
        queries.add("CREATE INDEX idx_Sample_sampleSpecies ON sampleSpecies(sample);");
        queries.add("CREATE INDEX idx_Sample_standardiseSampleSpecies ON standardiseSampleSpecies(sample);");
        queries.add("CREATE INDEX idx_Activity_activityFishingContext ON activityFishingContext(activity);");
        queries.add("CREATE INDEX idx_Activity_elementaryCatch ON elementaryCatch(activity);");
        queries.add("CREATE INDEX idx_Activity_correctedElementaryCatch ON correctedElementaryCatch(activity);");
    }
}
