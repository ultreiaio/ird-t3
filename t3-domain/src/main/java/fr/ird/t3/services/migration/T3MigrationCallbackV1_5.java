package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration for version {@code 1.5}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV1_5 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return Versions.valueOf("1.5");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // Update SetDuration values (http://forge.codelutin.com/issues/2234)
        updateSetDurationValues(queries);
    }

    protected void updateSetDurationValues(List<String> queries) {

        //--2012
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704815#0.5049529393542529', 0, '2013-04-02 14:46:09.578', 103, 0.966000021, 137.029999, 2012, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.15856146285780426', 0, '2013-04-02 14:46:09.579', 108, 0.966000021, 137.029999, 2012, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.6961528932833749', 0, '2013-04-02 14:46:09.580', 121, 0.598999977, 122.716003, 2012, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.7043376036298982', 0, '2013-04-02 14:46:09.581', 109, 0.598999977, 122.716003, 2012, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.7263571290343775', 0, '2013-04-02 14:46:09.582', 110, 0.488000005, 117.164001, 2012, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.3077376458732498', 0, '2013-04-02 14:46:09.583', 91.5, 0.488000005, 117.164001, 2012, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.5397255030401867', 0, '2013-04-02 14:46:09.584', 126, 0.476000011, 138.011002, 2012, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364906704816#0.6573985843031241', 0, '2013-04-02 14:46:09.585', 110, 0.476000011, 138.011002, 2012, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        //-- 2013
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704815#0.5049529393542529', 0, '2013-04-03 14:46:09.578', 103, 0.966000021, 137.029999, 2013, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.15856146285780426', 0, '2013-04-03 14:46:09.579', 108, 0.966000021, 137.029999, 2013, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.6961528932833749', 0, '2013-04-03 14:46:09.580', 121, 0.598999977, 122.716003, 2013, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.7043376036298982', 0, '2013-04-03 14:46:09.581', 109, 0.598999977, 122.716003, 2013, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.7263571290343775', 0, '2013-04-03 14:46:09.582', 110, 0.488000005, 117.164001, 2013, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.3077376458732498', 0, '2013-04-03 14:46:09.583', 91.5, 0.488000005, 117.164001, 2013, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.5397255030401867', 0, '2013-04-03 14:46:09.584', 126, 0.476000011, 138.011002, 2013, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364907704816#0.6573985843031241', 0, '2013-04-03 14:46:09.585', 110, 0.476000011, 138.011002, 2013, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        //-- 2014
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704815#0.5049529393542529', 0, '2013-04-04 14:46:09.578', 103, 0.966000021, 137.029999, 2014, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.15856146285780426', 0, '2013-04-04 14:46:09.579', 108, 0.966000021, 137.029999, 2014, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.6961528932833749', 0, '2013-04-04 14:46:09.580', 121, 0.598999977, 122.716003, 2014, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.7043376036298982', 0, '2013-04-04 14:46:09.581', 109, 0.598999977, 122.716003, 2014, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.7263571290343775', 0, '2013-04-04 14:46:09.582', 110, 0.488000005, 117.164001, 2014, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.3077376458732498', 0, '2013-04-04 14:46:09.583', 91.5, 0.488000005, 117.164001, 2014, 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");

        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.5397255030401867', 0, '2013-04-04 14:46:09.584', 126, 0.476000011, 138.011002, 2014, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445');");
        queries.add("INSERT INTO setduration VALUES ('fr.ird.t3.entities.reference.SetDuration#1364908704816#0.6573985843031241', 0, '2013-04-04 14:46:09.585', 110, 0.476000011, 138.011002, 2014, 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');");
    }

}
