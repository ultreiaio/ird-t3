/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserJavaBeanDefinition;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.entities.user.UserDatabaseJavaBeanDefinition;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaNotFoundException;
import org.nuiton.util.StringUtil;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Default {@link UserService} implementation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class UserService extends T3ServiceSupport implements T3ServiceInjectable {

    private static String encodePassword(String password) {
        return StringUtil.encodeMD5(password);
    }

    public Optional<T3User> getUserByLogin(String login) {
        return getT3Users().usersStream().whereLogin().isEquals(login).filter().findFirst();
    }

    public T3User getUserById(String userId) throws TopiaNotFoundException {
        return getT3Users().usersStream().whereId().isEquals(userId).filter().findFirst().orElseThrow(() -> new TopiaNotFoundException(userId));
    }

    public List<T3User> getUsers() {
        return getT3Users().getUsers();
    }

    public void createUser(T3User user) {
        String newPassword = encodePassword(user.getPassword());
        user.setPassword(newPassword);
        T3User userToSave = T3UserJavaBeanDefinition.instance()
                .admin(user.isAdmin())
                .id(UUID.randomUUID().toString())
                .login(user.getLogin())
                .password(newPassword)
                .build();
        getT3Users().getUsers().add(userToSave);
        user.setId(userToSave.getId());
        commit();
    }

    public void updateUser(T3User user) {
        T3User userToSave = getUserById(user.getId());

        userToSave.setLogin(user.getLogin());
        userToSave.setAdmin(user.isAdmin());
        if (!StringUtils.isEmpty(user.getPassword())) {

            // only change password when it is not blank
            userToSave.setPassword(encodePassword(user.getPassword()));
        }
        commit();
    }

    public void deleteUser(String userId) {
        T3User user = getUserById(userId);
        getT3Users().getUsers().remove(user);
        commit();
    }

    public void addUserInputDatabase(String userId, UserDatabase dbConfiguration) {
        T3User user = getUserById(userId);
        UserDatabase dbConfigurationToSave = UserDatabaseJavaBeanDefinition.instance()
                .id(UUID.randomUUID().toString())
                .description(dbConfiguration.getDescription())
                .login(dbConfiguration.getLogin())
                .url(dbConfiguration.getUrl())
                .password(dbConfiguration.getPassword())
                .build();
        user.getInputs().add(dbConfigurationToSave);
        dbConfiguration.setId(dbConfigurationToSave.getId());
        commit();
    }

    public void updateUserInputDatabase(UserDatabase dbConfiguration) {
        UserDatabase dbConfigurationToSave = getUserInputDatabase(dbConfiguration.getId());
        dbConfiguration.copy(dbConfigurationToSave);
        commit();
    }

    public void removeUserInputDatabase(String userId, String dbConfigurationId) {
        T3User user = getUserById(userId);
        UserDatabase dbConfigurationToSave = getUserInputDatabase(dbConfigurationId);
        user.getInputs().remove(dbConfigurationToSave);
        commit();
    }

    public void addOutputDatabase(String userId, UserDatabase dbConfiguration) {
        T3User user = getUserById(userId);
        UserDatabase dbConfigurationToSave = UserDatabaseJavaBeanDefinition.instance()
                .id(UUID.randomUUID().toString())
                .description(dbConfiguration.getDescription())
                .login(dbConfiguration.getLogin())
                .url(dbConfiguration.getUrl())
                .password(dbConfiguration.getPassword())
                .build();
        user.getOutputs().add(dbConfigurationToSave);
        dbConfiguration.setId(dbConfigurationToSave.getId());
        commit();
    }

    public void updateUserOutputDatabase(UserDatabase dbConfiguration) {
        UserDatabase dbConfigurationToSave = getUserOutputDatabase(dbConfiguration.getId());
        dbConfiguration.copy(dbConfigurationToSave);
        commit();
    }

    public void removeOutputDatabase(String userId, String dbConfigurationId) {
        T3User user = getUserById(userId);
        UserDatabase dbConfigurationToSave = getUserOutputDatabase(dbConfigurationId);
        user.getOutputs().remove(dbConfigurationToSave);
        commit();
    }

    public UserDatabase getUserInputDatabase(String dbConfigurationId) {
        return getT3Users().getInput(dbConfigurationId);
    }

    public UserDatabase getUserOutputDatabase(String dbConfigurationId) {
        return getT3Users().getOutput(dbConfigurationId);
    }

    public boolean checkPassword(T3User user, String password) {
        String s = encodePassword(password);
        return s.equals(user.getPassword());
    }

    private void commit() {
        getT3Users().save();
    }

}
