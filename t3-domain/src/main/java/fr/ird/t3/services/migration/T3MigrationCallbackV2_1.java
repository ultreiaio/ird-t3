package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.util.DateUtil;
import org.nuiton.version.Versions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tchemit on 18/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV2_1 extends T3MigrationCallbackSupport {

    private static final Log log = LogFactory.getLog(T3MigrationCallbackV2_1.class);

    public T3MigrationCallbackV2_1() {
        super(Versions.valueOf("2.1"));
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql, boolean showProgression) throws TopiaException {
        addScript("01", "add-Harbour-country-link", queries);
        addScript("02", "remove-LocalMarketPackaging-status", queries);
        addScript("03", "fix-LocalMarketPackagingType-ids", queries);
        addScript("04", "rename-SampleWell-table", queries);
        addScript("05", "rename-SampleSpecies-fields", queries);
        addScript("06", "referential-i18n", queries);
        addScript("07", "add-Route-table", queries);


        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        List<String> tripList = sqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT DISTINCT(a.trip) FROM activity a");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });

        for (String tripId : tripList) {

            int i = tripId.indexOf('#');
            String routeIdPrefix = "fr.ird.t3.entities.data.Route#" + tripId.substring(i + 1).replace(".", "").replace("#", "") + "#";

            log.info("Route prefix: " + routeIdPrefix);
            List<TripActivity> activityList = sqlSupport.findMultipleResult(new TopiaSqlQuery<TripActivity>() {
                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    PreparedStatement preparedStatement = connection.prepareStatement("SELECT a.topiaId, a.date FROM activity a WHERE a.trip = ? ORDER BY date");
                    preparedStatement.setString(1, tripId);
                    return preparedStatement;
                }

                @Override
                public TripActivity prepareResult(ResultSet set) throws SQLException {
                    return new TripActivity(set.getString(1), set.getDate(2));
                }
            });

            int activityIndex = 0;
            Set<Date> days = new LinkedHashSet<>();
            for (TripActivity activity : activityList) {

                String dayDateStr = df.format(activity.date);
                String routeId = routeIdPrefix + dayDateStr.replaceAll("-", "");

                if (days.add(activity.date)) {

                    log.info(String.format("[%s] new Route: %s", tripId, routeId));
                    activityIndex = 0;
                    queries.add(String.format("INSERT INTO Route(topiaId, topiaVersion, topiaCreateDate, trip, trip_idx, date) VALUES('%s', 0, CURRENT_TIMESTAMP, '%s', %d, to_date('%s', 'YYY-MM-DD'));", routeId, tripId, days.size() - 1, dayDateStr));
                }
                queries.add(String.format("UPDATE Activity SET route = '%s', route_idx = %d , topiaVersion = topiaVersion + 1 WHERE topiaId = '%s';", routeId, activityIndex++, activity.activity));
            }

            if (!activityList.isEmpty()) {
                queries.add(String.format("UPDATE Trip SET effortComputed = false, topiaVersion = topiaVersion + 1 WHERE topiaId = '%s';", tripId));
            }
        }

        addScript("08", "remove-Trip-fields", queries);
        addScript("09", "fill-Harbour-ocean", queries);
        addScript("10", "add-WeightCategoryLogBookConversion-table", queries);
        addScript("11", "fill-WeightCategoryLogBookConversion-table", queries);
    }


    private static class TripActivity {

        private final String activity;
        private final Date date;

        private TripActivity(String activity, Date date) {
            this.activity = activity;
            this.date = DateUtil.getDay(date);
        }
    }
}
