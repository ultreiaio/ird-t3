/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.migration;

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration for version {@code 1.3.1}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV1_3_1 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return Versions.valueOf("1.3.1");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // change model for level 3 (http://forge.codelutin.com/issues/1184)
        changeLevel3Model(queries);

        // Add convertSetSpeciesFrequencyToWeight state to Trip and sample (http://forge.codelutin.com/issues/1151)
        addConvertSetSpeciesFrequencyToWeightState(queries);

        // Change conversion fro main species (http://forge.codelutin.com/issues/1227)
        changeSpeciesConversions(queries);

        // Add a vesselVirtual flag on vessel (http://forge.codelutin.com/issues/1225)
        addVesselVirtualFlag(queries);

        // Add faoid on some species (http://forge.codelutin.com/issues/1244)
        addFaoids(queries);
    }

    private void addFaoids(List<String> queries) {
        queries.add("UPDATE species SET faoid = 'YFT' WHERE code = 1;");
        queries.add("UPDATE species SET faoid = 'SKJ' WHERE code = 2;");
        queries.add("UPDATE species SET faoid = 'BET' WHERE code = 3;");
        queries.add("UPDATE species SET faoid = 'ALB' WHERE code = 4;");
        queries.add("UPDATE species SET faoid = 'LTA' WHERE code = 5;");
        queries.add("UPDATE species SET faoid = 'FRI' WHERE code = 6;");
        queries.add("UPDATE species SET faoid = 'MSK' WHERE code = 7;");

        queries.add("UPDATE species SET faoid = 'KAW' WHERE code = 10;");
        queries.add("UPDATE species SET faoid = 'LOT' WHERE code = 11;");
        queries.add("UPDATE species SET faoid = 'BLF' WHERE code = 12;");
        queries.add("UPDATE species SET faoid = 'BFT' WHERE code = 13;");
        queries.add("UPDATE species SET faoid = 'SBF' WHERE code = 14;");
        queries.add("UPDATE species SET faoid = 'BON' WHERE code = 15;");

        queries.add("UPDATE species SET faoid = 'BLT' WHERE code = 17;");

        queries.add("UPDATE species SET faoid = 'MAZ' WHERE code = 29;");
        queries.add("UPDATE species SET faoid = 'SAI' WHERE code = 30;");
        queries.add("UPDATE species SET faoid = 'SFA' WHERE code = 31;");
        queries.add("UPDATE species SET faoid = 'BLM' WHERE code = 32;");
        queries.add("UPDATE species SET faoid = 'BUM' WHERE code = 33;");

        queries.add("UPDATE species SET faoid = 'WHM' WHERE code = 35;");
        queries.add("UPDATE species SET faoid = 'MLS' WHERE code = 36;");
        queries.add("UPDATE species SET faoid = 'SPF' WHERE code = 37;");
        queries.add("UPDATE species SET faoid = 'BIL' WHERE code = 38;");
        queries.add("UPDATE species SET faoid = 'SWO' WHERE code = 39;");

    }

    private void addConvertSetSpeciesFrequencyToWeightState(List<String> queries) {
        queries.add("ALTER TABLE sample ADD COLUMN convertSetSpeciesFrequencyToWeight boolean default false;");
        queries.add("ALTER TABLE trip ADD COLUMN convertSetSpeciesFrequencyToWeight boolean default false;");
    }

    private void changeLevel3Model(List<String> queries) {
        queries.add("ALTER TABLE ExtrapolatedAllSetSpeciesCatWeight ADD COLUMN lfLengthClass integer;");
    }

    private void changeSpeciesConversions(List<String> queries) {

        String toweightrelationSimple = "a * Math.pow(L, b)";
        String tolengthrelationSimple = "Math.pow(P/a, 1/b)";

        String toweightrelation = "L < LSeuil?(a * Math.pow(L, b)):(c * Math.pow(L, d))";
        String tolengthrelation = "P < PSeuil?(Math.pow(P/a, 1/b)):(Math.pow(P/c, 1/d))";

        String sqlPattern = "UPDATE lengthweightconversion SET " +
                "coefficients = '%s', " +
                "toweightrelation = '%s', " +
                "tolengthrelation ='%s' " +
                "WHERE " +
                "species = (SELECT topiaid FROM species WHERE code3l ='%s') and " +
                "ocean = (SELECT topiaid FROM ocean WHERE code = %s);";

        {
            // Atlantique
            queries.add(
                    String.format(sqlPattern, "a=2.396E-5:b=2.9774", toweightrelationSimple, tolengthrelationSimple, "BET",
                                  1));
            queries.add(
                    String.format(sqlPattern, "a=7.480E-6:b=3.2526", toweightrelationSimple, tolengthrelationSimple, "SKJ",
                                  1));
            queries.add(
                    String.format(sqlPattern, "a=2.153E-5:b=2.976", toweightrelationSimple, tolengthrelationSimple, "YFT",
                                  1));
        }
        {
            // Indien
            queries.add(
                    String.format(sqlPattern, "a=2.7E-5:b=2.95100", toweightrelationSimple, tolengthrelationSimple, "BET",
                                  2));
            queries.add(
                    String.format(sqlPattern, "a=7.48E-6:b=3.2526", toweightrelationSimple, tolengthrelationSimple, "SKJ",
                                  2));
            queries.add(String.format(sqlPattern, "a=5.313E-5:b=2.75366:c=1.5849E-5:d=3.046:LSeuil=64:PSeuil=5",
                                      toweightrelation, tolengthrelation, "YFT", 2));
        }
    }

    private void addVesselVirtualFlag(List<String> queries) {
        queries.add("ALTER TABLE vessel ADD COLUMN vesselVirtual boolean default false;");
        queries.add("DELETE FROM vessel WHERE code >= 1000;");
    }

}
