/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import com.google.auto.service.AutoService;
import fr.ird.t3.services.T3TopiaPersistenceContextAware;
import java.lang.reflect.Field;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Fires the {@link InjectEntityById} annotation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see InjectEntityById
 * @since 1.0
 */
@AutoService(Injector.class)
public class InjectorEntityById extends AbstractInjector<InjectEntityById, T3TopiaPersistenceContextAware> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(InjectorEntityById.class);

    public InjectorEntityById() {
        super(InjectEntityById.class);
    }

    @Override
    protected Object getValueToInject(Field field,
                                      T3TopiaPersistenceContextAware bean,
                                      InjectEntityById annotation) throws Exception {
        // get entity type
        Class<? extends TopiaEntity> entityType = annotation.entityType();

        // get param id where to find ids to load
        String paramIds = annotation.path();

        if (StringUtils.isEmpty(paramIds)) {

            // use default ids from the field name + Id
            paramIds = "configuration." + field.getName() + "Id";
        }

        String id = (String) PropertyUtils.getProperty(bean, paramIds);

        // get dao
        TopiaDao<?> dao = bean.getT3TopiaPersistenceContext().get().getDao(entityType);

        // load entity from the id
        TopiaEntity valueToInject = id == null ? null : dao.forTopiaIdEquals(id).findUniqueOrNull();

        if (log.isInfoEnabled()) {
            log.info("Will set entity of type [" + entityType.getName() +
                             "] to field " + field);
        }

        return valueToInject;
    }


}
