/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.nuiton.topia.persistence.TopiaEntity;


import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Inject the dao of the correct type.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see InjectorDAO
 * @since 1.0
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface InjectInternalDAO {

    /**
     * Obtains the type of entities to load.
     *
     * @return the type of entities to load
     */
    Class<? extends TopiaEntity> entityType();
}
