package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration for version {@code 1.2}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.2
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV1_3_2 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return Versions.valueOf("1.3.2");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // add SampleSetSpeciesCatWeight table (http://forge.codelutin.com/issues/1600)
        addSampleSetSpeciesCatWeightEntity(queries);

        // Add convertSampleSetSpeciesFrequencyToWeight state to Trip and Sample (http://forge.codelutin.com/issues/1600)
        addConvertSampleSetSpeciesFrequencyToWeightState(queries);
    }

    private void addSampleSetSpeciesCatWeightEntity(List<String> queries) {
        queries.add("CREATE TABLE sampleSetSpeciesCatWeight (\n" +
                            "topiaid character varying(255) NOT NULL,\n" +
                            "topiaversion bigint NOT NULL,\n" +
                            "topiacreatedate date,\n" +
                            "weight real NOT NULL,\n" +
                            "samplewell character varying(255),\n" +
                            "species character varying(255) NOT NULL,\n" +
                            "weightcategorytreatment character varying(255) NOT NULL\n" +
                            ");");
        queries.add("ALTER TABLE sampleSetSpeciesCatWeight ADD CONSTRAINT PRIMARY_KEY_A PRIMARY KEY(topiaid);");
        queries.add("ALTER TABLE sampleSetSpeciesCatWeight ADD CONSTRAINT FKF75E028AD38B9722_INDEX_A FOREIGN KEY(species) REFERENCES species(topiaid);");
        queries.add("ALTER TABLE sampleSetSpeciesCatWeight ADD CONSTRAINT FKF75E028AA97A8A21_INDEX_A FOREIGN KEY(samplewell) REFERENCES samplewell(topiaid);");
        queries.add("ALTER TABLE sampleSetSpeciesCatWeight ADD CONSTRAINT FKF75E028A78C8B8EE_INDEX_A FOREIGN KEY(weightcategorytreatment) REFERENCES weightcategorytreatment(topiaid);");
        queries.add("CREATE INDEX idx_SampleWell_sampleSetSpeciesCatWeight ON sampleSetSpeciesCatWeight(sampleWell);");
    }

    private void addConvertSampleSetSpeciesFrequencyToWeightState(List<String> queries) {
        queries.add("ALTER TABLE sample ADD COLUMN convertSampleSetSpeciesFrequencyToWeight boolean default false;");
        queries.add("ALTER TABLE trip ADD COLUMN convertSampleSetSpeciesFrequencyToWeight boolean default false;");
    }

}
