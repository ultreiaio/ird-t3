/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.actions.T3ActionContext;
import org.nuiton.util.ObjectUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * To obtain services.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3ServiceFactory {

    /**
     * To store shared services.
     * <p/>
     * A shared service is in fact a singleton so can keep it once for all
     * while using {@link #newService(Class, T3ServiceContext)}.
     */
    protected final Map<Class<?>, T3Service> services;

    /** Protected constructor to avoid external instanciations... */
    public T3ServiceFactory() {
        services = new HashMap<>();
    }

    /**
     * Gets an instance of the service of the given type.
     * <p/>
     * If the service is marked as share, then the same instance will be
     * always delivred, otheriwse a new instance will be each time created.
     *
     * @param serviceClass the type of service to obtain
     * @param context      service context
     * @param <S>          the type of service to obtain
     * @return the instance of required service
     */
    public <S extends T3Service> S newService(Class<S> serviceClass,
                                              T3ServiceContext context) {
        return newService0(serviceClass, context);
    }

    public <C extends T3ActionConfiguration> T3ActionContext<C> newT3ActionContext(C configuration, T3ServiceContext context) {
        @SuppressWarnings("unchecked")
        T3ActionContext<C> result = newService(T3ActionContext.class, context);
        result.setConfiguration(configuration);
        return result;
    }

    /**
     * Obtain a new t3 action.
     *
     * @param actionClass   the type of action
     * @param actionContext the context of the action (configuration + result)
     * @param <A>           type of the action
     * @return the instanciated data action
     */
    public <C extends T3ActionConfiguration, A extends T3Action<C>> A newT3Action(Class<A> actionClass, T3ActionContext<C> actionContext) {
        actionContext.setActionType(actionClass);
        return newService(actionClass, actionContext);
    }

    /**
     * Gets a service given his type.
     * <p/>
     * It will first search in cache (if service is marked as shared).
     * <p/>
     * If not found, then creates the new service and can do some stuffs on
     * the freshly instanciaed service.
     *
     * @param serviceClass   the type of service to obtain
     * @param serviceContext service context
     * @param <S>            the type of service to obtain
     * @return the required service (from cache) or freshly instanciated
     * @see T3ServiceInitializable
     */
    @SuppressWarnings({"unchecked"})
    private <S extends T3Service> S newService0(Class<S> serviceClass, T3ServiceContext serviceContext) {
        T3Service service = null;
        if (T3ServiceSingleton.class.isAssignableFrom(serviceClass)) {
            // try to obtain it from cache
            service = services.get(serviceClass);
        }
        if (service == null) {
            service = ObjectUtil.newInstance(serviceClass);
            service.setServiceContext(serviceContext);
            if (service instanceof T3ServiceInitializable) {
                T3ServiceInitializable initializable = (T3ServiceInitializable) service;
                initializable.init(serviceContext);
            }
            if (service instanceof T3ServiceInjectable) {
                try {
                    newService(IOCService.class, serviceContext).injectExcept(service);
                } catch (Exception e) {
                    throw new IllegalStateException("Could not inject into service " + service, e);
                }
            }
            if (service instanceof T3ServiceSingleton) {
                ((Map) services).put(serviceClass, service);
            }
        }
        return (S) service;
    }

}
