/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.T3Config;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.user.T3Users;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.TimeLog;

import java.util.Locale;
import java.util.function.Supplier;

/**
 * Service support.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class T3ServiceSupport implements T3Service {

    protected T3ServiceContext serviceContext;
    private TimeLog timeLog;

    @Override
    public void setServiceContext(T3ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public T3Users getT3Users() {
        return serviceContext.getT3Users();
    }

    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return serviceContext.getT3TopiaPersistenceContext();
    }

    public final Locale getLocale() {
        return serviceContext.getLocale();
    }

    public T3Config getApplicationConfiguration() {
        return serviceContext.getApplicationConfiguration();
    }

    public T3ServiceFactory getServiceFactory() {
        return serviceContext.getServiceFactory();
    }

    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceContext.newService(clazz);
    }

    protected void flushTransaction(String classifier) throws TopiaException {
        long s0 = TimeLog.getTime();
        getT3TopiaPersistenceContext().get().getHibernateSupport().getHibernateSession().flush();
        getTimeLog().log(s0, "Flush transaction for trip", classifier);
    }

    protected TimeLog getTimeLog() {
        if (timeLog == null) {
            timeLog = new TimeLog(getClass());
        }
        return timeLog;
    }
}
