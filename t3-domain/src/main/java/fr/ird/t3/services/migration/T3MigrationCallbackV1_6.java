package fr.ird.t3.services.migration;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration for version {@code 1.5}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV1_6 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return Versions.valueOf("1.6");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // See http://forge.codelutin.com/issues/2298
        updateSpeciesThresholdNumberLevel3(queries);

    }

    protected void updateSpeciesThresholdNumberLevel3(List<String> queries) {

        queries.add("UPDATE Species SET thresholdNumberLevel3FreeSchoolType = 25, thresholdNumberLevel3ObjectSchoolType = 25, topiaVersion = topiaVersion + 1;");
        queries.add("UPDATE Species SET thresholdNumberLevel3FreeSchoolType = 150, thresholdNumberLevel3ObjectSchoolType = 150 WHERE faoId = 'YFT';");
        queries.add("UPDATE Species SET thresholdNumberLevel3FreeSchoolType = 150, thresholdNumberLevel3ObjectSchoolType = 150 WHERE faoId = 'SKJ';");
        queries.add("UPDATE Species SET thresholdNumberLevel3ObjectSchoolType = 180 WHERE faoId = 'BET';");

    }

}
