/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import com.google.auto.service.AutoService;
import com.opensymphony.xwork2.LocaleProvider;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Fires the {@link InjectDecoratedBeans} annotation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see InjectDecoratedBeans
 * @since 1.0
 */
@AutoService(Injector.class)
public class InjectorDecoratedBeans extends AbstractInjector<InjectDecoratedBeans, LocaleProvider> {

    private static final Log log = LogFactory.getLog(InjectorDecoratedBeans.class);

    protected DecoratorService decoratorService;

    public InjectorDecoratedBeans() {
        super(InjectDecoratedBeans.class);
    }

    @Override
    public void init(T3ServiceContext serviceContext) {
        decoratorService = serviceContext.getServiceFactory().newService(DecoratorService.class, serviceContext);
    }

    @Override
    protected Object getValueToInject(Field field, LocaleProvider bean, InjectDecoratedBeans annotation) throws Exception {
        // get object type
        Class<?> objectType = annotation.beanType();
        String dataContainerPath = annotation.dataContainerPath();
        Object fromObject;
        if (StringUtils.isEmpty(dataContainerPath)) {
            fromObject = bean;
        } else {
            fromObject = PropertyUtils.getProperty(bean, dataContainerPath);
        }
        Objects.requireNonNull(fromObject,
                String.format("Could not find data container from %s with path %s", bean, dataContainerPath));
        // get param id where to find ids to load
        String path = annotation.path();
        if (StringUtils.isEmpty(path)) {
            // let's use the name of the field
            path = field.getName();
        }
        String decoratorContext = annotation.decoratorContext();
        if (StringUtils.isEmpty(decoratorContext)) {
            // no decorator context given, then it is a null context
            decoratorContext = null;
        }
        Collection<?> entities = (Collection<?>) PropertyUtils.getProperty(fromObject, path);
        Objects.requireNonNull(entities,
                String.format("Could not find entities from configuration %s with path %s", fromObject, path));
        Decorator<?> decorator = decoratorService.getDecorator(bean.getLocale(), objectType, decoratorContext);
        Objects.requireNonNull(decorator,
                String.format("Could not find decorator for type %s and context %s", objectType, decoratorContext));
        Collection<?> ids = null;
        if (annotation.filterById()) {
            boolean useSingleId = annotation.filterBySingleId();
            // let's find ids to keep
            String filterIds = annotation.pathIds();
            if (StringUtils.isEmpty(filterIds)) {
                // let's use the name of the field
                filterIds = field.getName();
                if (useSingleId) {
                    filterIds = filterIds.substring(0, filterIds.length() - 1) + "Id";
                } else {
                    filterIds = filterIds.substring(0, filterIds.length() - 1) + "Ids";
                }
            }
            if (useSingleId) {
                // using a single id
                ids = Collections.singletonList(PropertyUtils.getProperty(fromObject, filterIds));
            } else {
                ids = (Collection<?>) PropertyUtils.getProperty(fromObject, filterIds);
            }
        }
        Map<String, String> valueToInject = new LinkedHashMap<>();
        if (ids == null) {
            // take all entities
            for (Object entity : entities) {
                String id = getId(entity);
                String str = decorator.toString(entity);
                valueToInject.put(id, str);
            }
        } else {
            // do annotation filter by ids
            for (Object object : entities) {
                String id = getId(object);
                if (ids.contains(id)) {
                    String str = decorator.toString(object);
                    valueToInject.put(id, str);
                }
            }
        }
        log.info(String.format("Will set %d entities of type [%s] to field %s", entities.size(), objectType.getName(), field));
        return valueToInject;
    }

    protected String getId(Object o) {
        if (o instanceof TopiaEntity) {
            TopiaEntity topiaEntity = (TopiaEntity) o;
            return topiaEntity.getTopiaId();
        }
        if (o instanceof Idable) {
            Idable idable = (Idable) o;
            return idable.getId();
        }
        throw new IllegalArgumentException("Can not find a way to get id of " + o);
    }
}
