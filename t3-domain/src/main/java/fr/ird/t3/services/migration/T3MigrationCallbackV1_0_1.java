/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.migration;

import java.util.List;

import com.google.auto.service.AutoService;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration for version {@code 1.0.1}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.1
 */
@AutoService(TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion.class)
public class T3MigrationCallbackV1_0_1 extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    @Override
    public Version getVersion() {
        return Versions.valueOf("1.0.1");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport, List<String> queries, boolean showSql,
                                          boolean showProgression)
            throws TopiaException {

        // update referentiel vessel with no fleet
        queries.add(
                "UPDATE vessel SET fleetCountry='fr.ird.t3.entities.reference.Country#1297580528741#0.6193885943455354' WHERE fleetCountry ='fr.ird.t3.entities.reference.Vessel#1297580528869#0.11172747671632266';");
        queries.add(
                "UPDATE vessel SET fleetCountry='fr.ird.t3.entities.reference.Country#1297580528741#0.6193885943455354' WHERE fleetCountry ='fr.ird.t3.entities.reference.Vessel#1297580528869#0.5294840198672179';");
        queries.add(
                "UPDATE vessel SET fleetCountry='fr.ird.t3.entities.reference.Country#1297580528741#0.6193885943455354' WHERE fleetCountry ='fr.ird.t3.entities.reference.Vessel#1297580528869#0.5123720603896776';");

        // rename trip column
        queries.add("ALTER TABLE trip RENAME COLUMN depatureharbour TO departureHarbour;");
    }
}
