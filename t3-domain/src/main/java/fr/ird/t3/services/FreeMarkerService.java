/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.Template;
import freemarker.template.Version;

import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * To use freeMarker to render action resume.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class FreeMarkerService extends T3ServiceSupport implements T3ServiceSingleton {

    /**
     * Shared freemarker configuration.
     * <p/>
     * <b>Note: </b> The configuration is auto loading as needed.
     */
    private Configuration freemarkerConfiguration;

    public String renderTemplate(String templateName, Locale locale, Map<String, Object> parameters) throws Exception {
        Template template = getFreemarkerConfiguration().getTemplate(templateName, locale);
        Objects.requireNonNull(template, "Could not find template '" + templateName + "'");
        StringWriter out = new StringWriter();
        template.process(parameters, out);
        return out.toString();
    }

    private Configuration getFreemarkerConfiguration() {
        if (freemarkerConfiguration == null) {
            Version incompatibleImprovements = new Version("2.3.0");
            freemarkerConfiguration = new Configuration(incompatibleImprovements);
            TemplateLoader loader = new ClassTemplateLoader(getClass(), "/");
            freemarkerConfiguration.setTemplateLoader(loader);
            BeansWrapper objectWrapper = new DefaultObjectWrapperBuilder(incompatibleImprovements).build();
            freemarkerConfiguration.setObjectWrapper(objectWrapper);
        }
        return freemarkerConfiguration;
    }
}
