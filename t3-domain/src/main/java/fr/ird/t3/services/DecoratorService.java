/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.actions.stratum.SchoolTypeIndeterminate;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CompleteTrip;
import fr.ird.t3.entities.data.Route;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.WeightCategory;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneCWPImpl;
import fr.ird.t3.entities.reference.zone.ZoneEE;
import fr.ird.t3.entities.reference.zone.ZoneET;
import fr.ird.t3.entities.reference.zone.ZoneFAO;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.user.UserDatabase;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorMulti18nProvider;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.topia.persistence.TopiaEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static org.nuiton.i18n.I18n.l;

/**
 * Service for all decorations (and sort by decorations).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class DecoratorService extends T3ServiceSupport implements T3ServiceSingleton {

    public static final String WITH_ID = "withId";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private final DecoratorMulti18nProvider decoratorProvider;

    public DecoratorService() {
        decoratorProvider = new T3DecoratorProvider();
    }

    public static String formatDate(Date date) {
        return DATE_FORMAT.format(date);
    }

    public <O> Decorator<O> getDecorator(Locale locale, Class<O> type, String context) {
        Decorator<O> decorator = decoratorProvider.getDecoratorByType(Objects.requireNonNull(locale), Objects.requireNonNull(type), context);
        return Objects.requireNonNull(decorator,
                String.format("Could not find decorator for type %s and context %s", type, context));
    }

    public String decorate(Locale locale, Object o, String context) {
        Decorator<?> decorator = getDecorator(locale, Objects.requireNonNull(o).getClass(), context);
        return decorator.toString(o);
    }

    public <O> List<O> sortToList(Locale locale, Collection<O> beans, String context) {
        List<O> list = new ArrayList<>(Objects.requireNonNull(beans));
        getDecoratorAndSort(locale, context, list);
        return list;
    }

    public <E extends TopiaEntity> Map<String, String> sortAndDecorate(Locale locale, Collection<E> beans, String context) {
        List<E> list = new ArrayList<>(Objects.requireNonNull(beans));
        Decorator<E> decorator = getDecoratorAndSort(locale, context, list);
        Map<String, String> result = new LinkedHashMap<>();
        for (E bean : list) {
            result.put(bean.getTopiaId(), decorator.toString(bean));
        }
        return result;
    }

    public <E extends Idable> Map<String, String> sortAndDecorateIdAbles(Locale locale, Collection<E> beans, String context) {
        List<E> list = new ArrayList<>(Objects.requireNonNull(beans));
        Decorator<E> decorator = getDecoratorAndSort(locale, context, list);
        Map<String, String> result = new LinkedHashMap<>();
        for (E bean : list) {
            result.put(bean.getId(), decorator.toString(bean));
        }
        return result;
    }

    private <O> Decorator<O> getDecoratorAndSort(Locale locale, String context, List<O> list) {
        Decorator<O> decorator = null;
        if (CollectionUtils.isNotEmpty(list)) {
            O object = Objects.requireNonNull(list.get(0));
            decorator = Objects.requireNonNull(decoratorProvider.getDecorator(locale, object, context),
                    String.format("Could not find decorator for type %s and context %s", object.getClass(), context)
            );
            DecoratorUtil.sort((JXPathDecorator<O>) decorator, list, 0);
        }
        return decorator;
    }

    static class T3DecoratorProvider extends DecoratorMulti18nProvider {

        @Override
        protected void loadDecorators(Locale locale) {

            // trip decorator
            registerJXPathDecorator(locale, Trip.class, "${vesselLabel}$s - ${landingDate}$td/%2$tm/%2$tY");

            // trip type decorator
            registerJXPathDecorator(locale, TripType.class, "${label}$s");

            // SchoolTypeIndeterminate decorator
            registerJXPathDecorator(locale, SchoolTypeIndeterminate.class, "${label}$s");

            // route decorator
            registerJXPathDecorator(locale, Route.class, "${trip/vesselLabel}$s - ${trip/landingDate}$td/%2$tm/%2$tY - ${date}$td/%3$tm/%3$tY");

            // trip (with topiaid) decorator
            registerJXPathDecorator(locale, Trip.class, WITH_ID, "${vesselLabel}$s - ${landingDate}$td/%2$tm/%2$tY [${topiaId}$s]");

            // trip (with topiaid) decorator
            registerJXPathDecorator(locale, CompleteTrip.class, "${landingTrip/vesselLabel}$s - ${departureTrip/departureDate}$td/%2$tm/%2$tY - ${landingTrip/landingDate}$td/%3$tm/%3$tY (nb trip(s) ${nbTrips}$s)");

            // trip (with topiaid) decorator
            registerJXPathDecorator(locale, CompleteTrip.class, WITH_ID, "${landingTrip/vesselLabel}$s - ${departureTrip/departureDate}$td/%2$tm/%2$tY - ${landingTrip/landingDate}$td/%3$tm/%3$tY [${landingTrip/topiaId}$s] (nb trip(s) ${nbTrips}$s)");

            // well (with topiaid) decorator
            registerJXPathDecorator(locale, Well.class, "Number ${wellNumber}$s - Position ${wellPosition}$s [${topiaId}$s]");

            // sample decorator
            registerJXPathDecorator(locale, Sample.class, "${sampleNumber}$s");

            // sample (with topiaid) decorator
            registerJXPathDecorator(locale, Sample.class, WITH_ID, "${sampleNumber}$s [${topiaId}$s]");

            // activity  decorator
            registerJXPathDecorator(locale, Activity.class, "${date}$td/%1$tm/%1$tY - (${longitude}$s, ${latitude}$s)");

            // activity  decorator
            registerJXPathDecorator(locale, Activity.class, WITH_ID, "${date}$td/%1$tm/%1$tY - (${longitude}$s, ${latitude}$s) [${topiaId}$s]");

            // vessel decorator
            registerJXPathDecorator(locale, Vessel.class, "${code}$d - ${label1}$s - ${fleetCountry/label1}$s");

            // vessel simple type decorator
            registerJXPathDecorator(locale, VesselSimpleType.class, "${label1}$s");

            // schoolType decorator
            registerJXPathDecorator(locale, SchoolType.class, "${label1}$s");

            // zoneEE decorator
            registerJXPathDecorator(locale, ZoneEE.class, "${label1}$s");

            // zoneET decorator
            registerJXPathDecorator(locale, ZoneET.class, "${label1}$s");

            // zoneFAO decorator
            registerJXPathDecorator(locale, ZoneFAO.class, "${label1}$s");

            // zoneCWP decorator
            registerJXPathDecorator(locale, ZoneCWPImpl.class, "${label1}$s");

            // ocean decorator
            registerJXPathDecorator(locale, Ocean.class, "${label1}$s");

            // country decorator
            registerJXPathDecorator(locale, Country.class, "${label1}$s");

            // harbour decorator
            registerJXPathDecorator(locale, Harbour.class, "${label1}$s");

            // sample quality decorator
            registerJXPathDecorator(locale, SampleQuality.class, "${label1}$s");

            // sample type decorator
            registerJXPathDecorator(locale, SampleType.class, "${label1}$s");

            // species decorator
            registerJXPathDecorator(locale, Species.class, "${code3L}$s - ${label1}$s");

            // weightCategoryTreatment decorator
            registerJXPathDecorator(locale, WeightCategoryTreatment.class, "${label1}$s - [${min}$s - ${max}$s]");

            // weightCategory decorator
            registerJXPathDecorator(locale, WeightCategory.class, "${label1}$s - [${min}$s - ${max}$s]");

            // zoneVersion decorator
            registerJXPathDecorator(locale, ZoneVersion.class, "${versionLabel}$s");

            // userT3Database decorator
            registerJXPathDecorator(locale, UserDatabase.class, "${description}$s");

            // zone meta decorator
            String expression = "${i18nKey}$s";
            JXPathDecorator.Context<ZoneStratumAwareMeta> context = DecoratorUtil.createJXPathContext(expression);
            registerDecorator(locale, new ZoneStratumAwareMetaJXPathDecorator(expression, context, locale));
        }
    }

    private static class ZoneStratumAwareMetaJXPathDecorator extends JXPathDecorator<ZoneStratumAwareMeta> {

        private static final long serialVersionUID = 1L;
        private final Locale locale;

        ZoneStratumAwareMetaJXPathDecorator(String expression, Context<ZoneStratumAwareMeta> context, Locale locale) {
            super(ZoneStratumAwareMeta.class, expression, context);
            this.locale = locale;
        }

        @SuppressWarnings({"RawUseOfParameterizedType", "RedundantCast", "unchecked"})
        @Override
        protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxContext, String token) {
            Comparable tokenValue = super.getTokenValue(jxContext, token);
            return (Comparable) l(locale, (String) tokenValue);
        }
    }
}
