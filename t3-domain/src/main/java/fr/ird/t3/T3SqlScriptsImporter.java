package fr.ird.t3;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.T3JdbcHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * Pour importer tous les scripts contenus dans un répertoire.
 * <p>
 * Created on 30/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class T3SqlScriptsImporter {

    private static final Log log = LogFactory.getLog(T3SqlScriptsImporter.class);

    private final T3JdbcHelper jdbcHelper;
    private final List<File> scriptsFile;

    public T3SqlScriptsImporter(T3JdbcHelper jdbcHelper, File scriptsDirectory) {
        this.jdbcHelper = Objects.requireNonNull(jdbcHelper);
        this.scriptsFile = T3JdbcHelper.getSqlFiles(Objects.requireNonNull(scriptsDirectory));
    }

    public List<File> getScriptsFile() {
        return scriptsFile;
    }

    public void importScripts() {
        for (File scriptFile : scriptsFile) {
            log.info(String.format(" o Loading sql script ...(%s)", scriptFile.getName()));
            jdbcHelper.executeSql(scriptFile);
        }

    }


}
