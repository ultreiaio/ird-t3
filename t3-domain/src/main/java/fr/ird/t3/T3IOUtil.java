/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * Useful IO methods.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public final class T3IOUtil {

    /** A time-stamp, allow to make multiple build and keep the tests data. */
    public static final String TIMESTAMP = String.valueOf(System.nanoTime());
    /** Logger. */
    private static final Log log = LogFactory.getLog(T3IOUtil.class);

    private T3IOUtil() {
        // do not instantiate a such util class
    }

    public static File copyResourceToFile(String resourcePath, File targetDirectory, String targetFilename) throws IOException {
        URL resource = T3IOUtil.class.getResource(resourcePath);
        File targetFile = new File(targetDirectory, targetFilename);
        log.info(String.format("Will copy data file from %s to %s", resource, targetFile));
        try (InputStream inputStream = resource.openStream()) {
            Objects.requireNonNull(inputStream, "Could not find resource " + resourcePath);
            // copy it to a nice concrete file
            try (OutputStream outputStream = new FileOutputStream(targetFile)) {
                IOUtils.copy(inputStream, outputStream);
            }
        }
        return targetFile;
    }

    public static File getTestSpecificDirectory(Class<?> testClassName, String methodName) {
        // Trying to look for the temporary folder to store data for the test
        String tempDirPath = System.getProperty("java.io.tmpdir");
        if (tempDirPath == null) {
            // can this really occur ?
            tempDirPath = "";
            log.warn("'\"java.io.tmpdir\" not defined");
        }
        File tempDirFile = new File(tempDirPath);

        // create the directory to store database data
        String dataBasePath = testClassName.getName()
                + File.separator // a directory with the test class name
                + methodName// a sub-directory with the method name
                + '_'
                + TIMESTAMP; // and a timestamp
        return new File(tempDirFile, dataBasePath);
    }

    public static void reloadProperty(Properties properties, String propertyName) {
        Object o = properties.getProperty(propertyName);
        properties.put(propertyName, o);
    }

    public static <K, V> void fillMapWithDefaultValue(Map<K, V> map, Collection<K> keys, Supplier<V> defaultValue) {
        for (K key : keys) {
            if (!map.containsKey(key)) {
                map.put(key, defaultValue.get());
            }
        }
    }

}
