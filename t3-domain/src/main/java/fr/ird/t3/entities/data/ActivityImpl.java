/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import com.google.common.collect.Sets;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.VesselActivity;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.type.T3Point;
import fr.ird.t3.entities.type.T3PointImpl;
import fr.ird.type.CoordinateHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Implementation of {@link Activity} with manual quadrant
 * (needed from ms-access imports).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ActivityImpl extends ActivityAbstract {

    private static final Log log = LogFactory.getLog(ActivityImpl.class);

    private static final long serialVersionUID = 1L;

    private static final Set<String> VESSEL_ACTIVITY_WITH_NULL_SET_DURATION = Sets.newHashSet(
            "fr.ird.t3.entities.reference.VesselActivity#1297580528706#0.4362868564425493", // code 0 (coup(s) nul(s))
            "fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.1901330834121593" // code 14 (chavire la poche)
    );

    private static final Set<String> VESSEL_ACTIVITY_WITH_COMPUTED_SET_DURATION = Sets.newHashSet(
            "fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.6977540500177304", // code 1 (coup(s) positif(s))
            "fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.08581829697697385" // code 2 (détail inconnu)
    );

    protected Integer quadrant;
    private transient Date date;

    @Override
    public Date getDate() {
        return date != null ? date : getRoute().getDate();
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public Trip getTrip() {
        return getRoute().getTrip();
    }

    @Override
    public Integer getQuadrant() {
        if (quadrant == null) {
            // compute it from latitude and longitude
            quadrant = CoordinateHelper.getQuadrant(getLongitude(), getLatitude());
        }
        return quadrant;
    }

    @Override
    public void setQuadrant(Integer quadrant) {
        this.quadrant = quadrant;
    }

    @Override
    public boolean isSetNull() {
        return VESSEL_ACTIVITY_WITH_NULL_SET_DURATION.contains(getVesselActivity().getTopiaId());
    }

    @Override
    public boolean isWithSetDuration() {
        return VESSEL_ACTIVITY_WITH_COMPUTED_SET_DURATION.contains(getVesselActivity().getTopiaId());
    }

    @Override
    public boolean isMixedSet() {
        VesselActivity vesselActivity = getVesselActivity();
        if (vesselActivity == null) {
            // no vessel activity, so let's say activity is not mixed
            return false;
        }

        if (vesselActivity.getCode() != 2) {
            // only positive set can be mixed
            return false;
        }

        int nbSets = getSetCount();
        if (nbSets < 2) {
            // less than 2 sets, so not a mixed set
            return false;
        }
        log.info(String.format("Found a mixed activity (vessel activity : %d - setCount : %d )", getVesselActivity().getCode(), nbSets));
        return true;
    }

    @Override
    public float getElementaryCatchTotalWeight(Collection<Species> species) {
        float result = 0;
        if (isElementaryCatchNotEmpty()) {
            for (ElementaryCatch eCatch : getElementaryCatch()) {
                if (species.contains(eCatch.getWeightCategoryLogBook().getSpecies())) {
                    result += eCatch.getCatchWeight();
                }
            }
        }
        return result;
    }

    @Override
    public float getElementaryCatchTotalWeightRf2(Collection<Species> species) {
        float result = 0;
        if (isElementaryCatchNotEmpty()) {
            for (ElementaryCatch eCatch : getElementaryCatch()) {
                if (species.contains(eCatch.getWeightCategoryLogBook().getSpecies())) {
                    result += eCatch.getCatchWeightRf2();
                }
            }
        }
        return result;
    }

    @Override
    public int getFortnight() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate());
        return calendar.get(Calendar.WEEK_OF_YEAR) / 2;
    }

    @Override
    public int getTrimester() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate());
        return calendar.get(Calendar.MONTH) / 4;
    }

    @Override
    public int getHour() {
        int hour = 0;
        Date time = getTime();
        if (time != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(time);
            hour = calendar.get(Calendar.HOUR_OF_DAY);
        }
        return hour;
    }

    @Override
    public void deleteComputedData() {
        ComputedDataHelper.deleteComputedData(this);
    }

    @Override
    public void deleteComputedDataLevel0() {
        setSetDuration(null);
        setPositiveSetCount(null);
        clearCorrectedElementaryCatch();
        ComputedDataHelper.deleteComputedDataLevel0(getElementaryCatch());
        ComputedDataHelper.deleteComputedDataLevel0(getCorrectedElementaryCatch());
    }

    @Override
    public void deleteComputedDataLevel1() {
        ComputedDataHelper.deleteComputedDataLevel1(getElementaryCatch());
        ComputedDataHelper.deleteComputedDataLevel1(getCorrectedElementaryCatch());
        clearSetSpeciesFrequency();
        clearSetSpeciesCatWeight();
    }

    @Override
    public void deleteComputedDataLevel2() {
        setStratumLevelN2(null);
        setUseMeanStratumCompositionN2(null);
        ComputedDataHelper.deleteComputedDataLevel2(getElementaryCatch());
        ComputedDataHelper.deleteComputedDataLevel2(getCorrectedElementaryCatch());
    }

    @Override
    public void deleteComputedDataLevel3() {
        setStratumLevelN3(null);
        setUseMeanStratumCompositionN3(null);
        clearExtrapolatedAllSetSpeciesFrequency();
        ComputedDataHelper.deleteComputedDataLevel3(getElementaryCatch());
        ComputedDataHelper.deleteComputedDataLevel3(getCorrectedElementaryCatch());
    }

    @Override
    public T3Point toPoint() {
        return new T3PointImpl(getLongitude(), getLatitude());
    }

    @Override
    public boolean withCorrectedElementaryCatch(WeightCategoryTreatment weightCategoryTreatment) {
        return isCorrectedElementaryCatchNotEmpty() && getCorrectedElementaryCatch().stream().map(CorrectedElementaryCatch::getWeightCategoryTreatment).anyMatch(c -> c.equals(weightCategoryTreatment));
    }

    @Override
    public boolean withSetSpeciesCatWeight(WeightCategoryTreatment weightCategoryTreatment) {
        return isSetSpeciesCatWeightNotEmpty() && getSetSpeciesCatWeight().stream().map(SetSpeciesCatWeight::getWeightCategoryTreatment).anyMatch(c -> c.equals(weightCategoryTreatment));
    }
}
