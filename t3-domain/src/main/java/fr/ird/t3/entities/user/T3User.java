package fr.ird.t3.entities.user;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.reference.Idable;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.LinkedList;
import java.util.List;

@GenerateJavaBeanDefinition
public class T3User extends AbstractJavaBean implements Idable {

    private String login;

    private String password;

    private boolean admin;

    private String id;

    private List<UserDatabase> inputs;

    private List<UserDatabase> outputs;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isInputsEmpty() {
        return inputs == null || inputs.isEmpty();
    }

    public List<UserDatabase> getInputs() {
        if (inputs == null) {
            inputs = new LinkedList<>();
        }
        return inputs;
    }

    public void setInputs(List<UserDatabase> inputs) {
        this.inputs = inputs;
    }

    public boolean isOutputsEmpty() {
        return outputs == null || outputs.isEmpty();
    }

    public List<UserDatabase> getOutputs() {
        if (outputs == null) {
            outputs = new LinkedList<>();
        }
        return outputs;
    }

    public void setOutputs(List<UserDatabase> outputs) {
        this.outputs = outputs;
    }

}
