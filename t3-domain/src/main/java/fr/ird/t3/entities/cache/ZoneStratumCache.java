package fr.ird.t3.entities.cache;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 23/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ZoneStratumCache {

    private static final Log log = LogFactory.getLog(ZoneStratumCache.class);

    private final ArrayListMultimap<String, ZoneStratumAware> cache;
    private final ZoneStratumAwareMeta zoneMeta;
    private final T3TopiaPersistenceContext persistenceContext;
    private final ZoneVersion zoneVersion;

    public ZoneStratumCache(T3TopiaPersistenceContext persistenceContext, ZoneStratumAwareMeta zoneMeta, String zoneVersionId) {
        this.zoneMeta = Objects.requireNonNull(zoneMeta);
        this.persistenceContext = Objects.requireNonNull(persistenceContext);
        this.zoneVersion = Objects.requireNonNull(zoneMeta.getZoneVersion(Objects.requireNonNull(zoneVersionId), persistenceContext));
        this.cache = ArrayListMultimap.create();
    }

    public List<ZoneStratumAware> forOceansAndSchoolType(Collection<Ocean> oceans, SchoolType schoolType) {
        String key = Joiner.on("#").join(oceans.stream().map(Ocean::getLabel1).collect(Collectors.toList())) + "#" + schoolType.getLabel1();
        List<ZoneStratumAware> result;
        if (cache.containsKey(key)) {
            result = cache.get(key);
        } else {
            result = zoneMeta.getZones(oceans, schoolType, zoneVersion, persistenceContext);
            log.info(String.format("Cache Zone [%d] for %s (%d zones)", cache.keySet().size(), key, result.size()));
            cache.putAll(key, result);
        }
        return result;
    }

}
