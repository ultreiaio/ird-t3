/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import fr.ird.t3.entities.data.TripTopiaDao;
import org.nuiton.topia.persistence.TopiaException;

import java.util.HashSet;
import java.util.Set;

/**
 * {@link VesselSimpleType} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com
 * @since 1.0
 */
public class AbstractVesselSimpleTypeTopiaDao<E extends VesselSimpleType> extends GeneratedVesselSimpleTypeTopiaDao<E> {

    /**
     * Obtains all vessel simple types used by all trips in the database.
     *
     * @param samplesOnly if null no filter else filter on value
     * @return the set of used vessel simple types in trips in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllUsedInTrip(Boolean samplesOnly) throws TopiaException {
        String samplesOnlyFilter = TripTopiaDao.getSamplesOnlyFilter("AND", samplesOnly);
        String hql = "SELECT DISTINCT(vst) FROM VesselSimpleTypeImpl vst, TripImpl t WHERE t.vessel.vesselType.vesselSimpleType = vst.id" + samplesOnlyFilter;
        return new HashSet<>(findAll(hql));
    }
}
