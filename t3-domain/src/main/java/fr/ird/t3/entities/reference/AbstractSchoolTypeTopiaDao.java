/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import org.nuiton.topia.persistence.TopiaException;

import java.util.HashSet;
import java.util.Set;

public class AbstractSchoolTypeTopiaDao<E extends SchoolType> extends GeneratedSchoolTypeTopiaDao<E> {

    public static final String SCHOOL_TYPE_INDETERMINATE_ID = "fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636";
    public static final String SCHOOL_TYPE_BL_ID = "fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473";
    public static final String SCHOOL_TYPE_BO_ID = "fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445";

    /**
     * Obtain the two school types usable while doing level 2 and 3 : says the
     * free school type and the object school type.
     *
     * @return the two school types
     * @throws TopiaException if any problem while loading data form database
     */
    @SuppressWarnings("unused")
    public Set<E> findAllForStratum() {
        Set<E> result = new HashSet<>();
        result.add(forTopiaIdEquals(SCHOOL_TYPE_BL_ID).findUnique());
        result.add(forTopiaIdEquals(SCHOOL_TYPE_BO_ID).findUnique());
        return result;
    }
}
