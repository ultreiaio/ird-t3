/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class AbstractActivityTopiaDao<E extends Activity> extends GeneratedActivityTopiaDao<E> {

    public static <E extends ActivityAware> Multimap<Activity, E> groupByActivity(Collection<E> activityAwares) {
        Multimap<Activity, E> result;
        if (CollectionUtils.isEmpty(activityAwares)) {
            // creates empty result
            result = ArrayListMultimap.create();
        } else {
            result = Multimaps.index(activityAwares, ActivityAware::getActivity);
        }
        return result;
    }

    public static <X extends SpeciesAware> Set<Species> getSpecies(Collection<X> correctedElementaryCatch) {
        return correctedElementaryCatch.stream().map(SpeciesAware::getSpecies).collect(Collectors.toSet());
    }

    public Map<String, Integer> findAllActivityIdsForCatchStratum(String zoneTableName,
                                                                  String zoneId,
                                                                  Set<String> schoolTypeIds,
                                                                  T3Date beginDate,
                                                                  T3Date endDate) {
        // ---
        // Get activities strictly inside the zone
        // ---
        TopiaSqlQuery<String> query = new GetActivityIdsInZoneForCatchStratumQuery(zoneTableName, zoneId, schoolTypeIds, beginDate, endDate);
        List<String> activityIds = topiaSqlSupport.findMultipleResult(query);
        Map<String, Integer> result = new HashMap<>();
        for (String activityId : activityIds) {
            // activity strictly inside a zone are exactly in one zone
            result.put(activityId, 1);
        }
        // ---
        // Get activities on the border of the zone
        // ---
        query = new GetActivityIdsInBorderForCatchStratumQuery(zoneTableName, zoneId, schoolTypeIds, beginDate, endDate);
        activityIds = topiaSqlSupport.findMultipleResult(query);
        // ---
        // Get nb zones of such activities
        // ---
        GetActivityIdsAndNbZonesQuery nbZoneQuery = new GetActivityIdsAndNbZonesQuery(zoneTableName);
        for (String activityId : activityIds) {
            // get all zones for this activity
            Integer nbZones = nbZoneQuery.getNbZones(topiaSqlSupport, activityId);
            result.put(activityId, nbZones);
        }
        return result;
    }

    public List<String> findAllActivityIdsForSampleStratum(String zoneTableName,
                                                           String zoneId,
                                                           Set<String> schoolTypeIds,
                                                           T3Date beginDate,
                                                           T3Date endDate) {
        TopiaSqlQuery<String> query;
        if (schoolTypeIds == null) {
            // for any school type
            query = new GetActivityIdsForSampleStratumWithAnySchoolTypeQuery(zoneTableName, zoneId, beginDate, endDate);
        } else {
            // for a specific school type
            query = new GetActivityIdsForSampleStratumQuery(zoneTableName, zoneId, schoolTypeIds, beginDate, endDate);
        }
        return topiaSqlSupport.findMultipleResult(query);
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a catch stratum.
     *
     * @since 1.0
     */
    @SuppressWarnings("SqlResolve")
    public static class GetActivityIdsInZoneForCatchStratumQuery extends TopiaSqlQuery<String> {

        private final String zoneTableName;
        private final String zoneId;
        private final Set<String> schoolTypeIds;
        private final T3Date beginDate;
        private final T3Date endDate;

        GetActivityIdsInZoneForCatchStratumQuery(String zoneTableName,
                                                 String zoneId,
                                                 Set<String> schoolTypeIds,
                                                 T3Date beginDate,
                                                 T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.schoolTypeIds = schoolTypeIds;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            StringBuilder schoolTypeParameters = new StringBuilder();
            for (String ignored : schoolTypeIds) {
                schoolTypeParameters.append(", ?");
            }
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, route r, activity a, " + zoneTableName + " z " +
                            "WHERE " +
                            "a.route = r.topiaId " +
                            "AND r.trip = t.topiaId " +
                            "AND t." + Trip.PROPERTY_TRIP_TYPE + " IN ('STANDARD', 'LOGBOOKMISSING')" +
                            "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                            "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 " +
                            "AND ST_WITHIN(a.the_geom, z.the_geom) " +
                            "AND z.topiaid = ? " +
                            "AND a.expertflag != 0 " +
                            "AND a.schooltype IN (" + schoolTypeParameters.substring(2) + ")" +
                            "AND r.date::date >= ? " +
                            "AND r.date::date <= ? "
//                            + "AND (SELECT COUNT(*) FROM CorrectedElementaryCatch c WHERE c.activity = a.topiaid) > 0"
            );

            int index = 0;
            ps.setString(++index, zoneId);
            for (String schoolTypeId : schoolTypeIds) {
                ps.setString(++index, schoolTypeId);
            }
            ps.setDate(++index, beginDate.toBeginSqlDate());
            ps.setDate(++index, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a catch stratum.
     *
     * @since 1.0
     */
    @SuppressWarnings("SqlResolve")
    public static class GetActivityIdsInBorderForCatchStratumQuery extends TopiaSqlQuery<String> {

        private final String zoneTableName;
        private final String zoneId;
        private final Set<String> schoolTypeIds;
        private final T3Date beginDate;
        private final T3Date endDate;

        GetActivityIdsInBorderForCatchStratumQuery(String zoneTableName,
                                                   String zoneId,
                                                   Set<String> schoolTypeIds,
                                                   T3Date beginDate,
                                                   T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.schoolTypeIds = schoolTypeIds;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            StringBuilder schoolTypeParameters = new StringBuilder();
            for (String ignored : schoolTypeIds) {
                schoolTypeParameters.append(", ?");
            }
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, route r, activity a, " +
                            zoneTableName + " z WHERE " +
                            "r.trip = t.topiaId " +
                            "AND a.route = r.topiaId " +
                            "AND t." + Trip.PROPERTY_TRIP_TYPE + " IN ('STANDARD', 'LOGBOOKMISSING')" +
                            "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                            "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 " +
                            "AND ST_INTERSECTS(a.the_geom, z.the_geom) " +
                            "AND NOT ST_WITHIN(a.the_geom, z.the_geom) " +
                            "AND z.topiaid = ? " +
                            "AND a.expertflag != 0 " +
                            "AND a.schooltype IN (" + schoolTypeParameters.substring(2) + ") " +
                            "AND r.date::date >= ? " +
                            "AND r.date::date <= ? " +
                            "AND (SELECT COUNT(*) FROM CorrectedElementaryCatch c WHERE c.activity = a.topiaid) > 0");

            int index = 0;
            ps.setString(++index, zoneId);
            for (String schoolTypeId : schoolTypeIds) {
                ps.setString(++index, schoolTypeId);
            }
            ps.setDate(++index, beginDate.toBeginSqlDate());
            ps.setDate(++index, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a sample stratum.
     *
     * @since 1.0
     */
    @SuppressWarnings("SqlResolve")
    public static class GetActivityIdsForSampleStratumQuery extends TopiaSqlQuery<String> {

        private final String zoneTableName;
        private final String zoneId;
        private final Set<String> schoolTypeIds;
        private final T3Date beginDate;
        private final T3Date endDate;

        GetActivityIdsForSampleStratumQuery(String zoneTableName,
                                            String zoneId,
                                            Set<String> schoolTypeIds,
                                            T3Date beginDate,
                                            T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.schoolTypeIds = schoolTypeIds;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            StringBuilder schoolTypeParameters = new StringBuilder();
            for (String ignored : schoolTypeIds) {
                schoolTypeParameters.append(", ?");
            }
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, route r, activity a, " +
                            zoneTableName + " z WHERE " +
                            "r.trip = t.topiaId " +
                            "AND a.route = r.topiaId " +
                            "AND (t." + Trip.PROPERTY_TRIP_TYPE + " = 'SAMPLEONLY' OR (" +
                            "t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                            "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 ))" +
                            "AND ST_WITHIN(a.the_geom, z.the_geom) " +
                            "AND z.topiaid = ? " +
                            "AND a.schooltype IN (" + schoolTypeParameters.substring(2) + ")" +
                            "AND r.date::date >= ? " +
                            "AND r.date::date <= ? " +
                            "AND (SELECT COUNT(*) FROM SetSpeciesCatWeight c WHERE c.activity = a.topiaid) > 0");
            int index = 0;
            ps.setString(++index, zoneId);
            for (String schoolTypeId : schoolTypeIds) {
                ps.setString(++index, schoolTypeId);
            }
            ps.setDate(++index, beginDate.toBeginSqlDate());
            ps.setDate(++index, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a
     * sample stratum for any school type (used by level 3 last substitution level).
     *
     * @since 1.4
     */
    @SuppressWarnings("SqlResolve")
    public static class GetActivityIdsForSampleStratumWithAnySchoolTypeQuery extends TopiaSqlQuery<String> {

        private final String zoneTableName;
        private final String zoneId;
        private final T3Date beginDate;
        private final T3Date endDate;

        GetActivityIdsForSampleStratumWithAnySchoolTypeQuery(String zoneTableName, String zoneId, T3Date beginDate, T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, route r, activity a, " +
                            zoneTableName + " z WHERE " +
                            "r.trip = t.topiaId " +
                            "AND a.route = r.topiaId " +
                            "AND (t." + Trip.PROPERTY_TRIP_TYPE + " = 'SAMPLEONLY' OR (" +
                            "t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                            "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 ))" +
                            "AND ST_WITHIN(a.the_geom, z.the_geom) " +
                            "AND z.topiaid = ? " +
                            "AND r.date::date >= ? " +
                            "AND r.date::date <= ? " +
                            "AND (SELECT COUNT(*) FROM SetSpeciesCatWeight c WHERE c.activity = a.topiaid) > 0"
            );
            ps.setString(1, zoneId);
            ps.setDate(2, beginDate.toBeginSqlDate());
            ps.setDate(3, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a sample stratum.
     *
     * @since 1.4
     */
    @SuppressWarnings("SqlResolve")
    public static class GetActivityIdsAndNbZonesQuery extends TopiaSqlQuery<Integer> {

        private final String zoneTableName;

        private String activityId;

        GetActivityIdsAndNbZonesQuery(String zoneTableName) {
            this.zoneTableName = zoneTableName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT count(*) FROM activity a, " + zoneTableName + " z " +
                            "WHERE a.topiaId  = ? " +
                            "AND ST_INTERSECTS(a.the_geom, z.the_geom) " +
                            "AND z.schoolType = a.schoolType");
            ps.setString(1, activityId);
            return ps;
        }

        @Override
        public Integer prepareResult(ResultSet set) throws SQLException {
            return set.getInt(1);
        }

        int getNbZones(TopiaSqlSupport context, String activityId) {
            this.activityId = activityId;
            return context.findSingleResult(this);
        }

    }

//    public boolean isActivityWithSample(Activity activity) {
//        SampleSetTopiaDao sampleSetDAO = topiaDaoSupplier.getDao(SampleSet.class, SampleSetTopiaDao.class);
//        List<SampleSet> allByActivity = sampleSetDAO.forActivityEquals(activity).findAll();
//        return CollectionUtils.isNotEmpty(allByActivity);
//    }

//    public static void fillWeightsFromCorrectedCatchesWeight(Activity activity,
//                                                             Map<WeightCategoryTreatment, Map<Species, Float>> weights,
//                                                             WeightCompositionAggregateModel model) {
//
//        if (weights == null) {
//            weights = new HashMap<>();
//        }
//        Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> data =
//                ActivityTopiaDao.groupByWeightCategoryTreatment(activity.getCorrectedElementaryCatch());
//
//        T3IOUtil.fillMapWithDefaultValue(
//                weights, data.keySet(), T3Suppliers.MAP_SPECIES_FLOAT_SUPPLIER);
//
//        for (WeightCategoryTreatment weightCategory : data.keySet()) {
//
//            Map<Species, Float> speciesFloatMap = weights.get(weightCategory);
//
//            Collection<CorrectedElementaryCatch> correctedElementaryCatches = data.get(weightCategory);
//
//            fillWeights(weightCategory,
//                        correctedElementaryCatches,
//                        speciesFloatMap,
//                        T3Functions.CORRECTED_ELEMENTARY_CATCH_TO_CORRECTED_CATCH_WEIGHT,
//                        model
//            );
//        }
//    }

//    public Set<Species> getAllSpeciesFromCorrectedCatches(Activity activity) {
//        Set<Species> result = new HashSet<>();
//        for (CorrectedElementaryCatch aCatch : activity.getCorrectedElementaryCatch()) {
//            result.add(aCatch.getSpecies());
//        }
//        return result;
//    }

//    public Set<Species> getAllSpeciesFromSetSpeciesFrequencies(Activity activity) {
//        Set<Species> result = new HashSet<>();
//        for (SetSpeciesFrequency frequency : activity.getSetSpeciesFrequency()) {
//            result.add(frequency.getSpecies());
//        }
//        return result;
//    }

}
