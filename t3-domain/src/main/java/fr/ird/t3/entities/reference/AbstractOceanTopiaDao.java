/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link Ocean} dao user operations.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AbstractOceanTopiaDao<E extends Ocean> extends GeneratedOceanTopiaDao<E> {

    public static Set<Ocean> getAllOcean(Collection<Trip> trips) {
        return trips.stream().flatMap(t -> t.getAllOceans().stream()).collect(Collectors.toSet());
    }

    /**
     * Obtains all oceans used in any activities in the database.
     *
     * @return the set of used ocean in any activities in database
     */
    public Set<E> findAllUsedInActivity() {
        String hql = "SELECT DISTINCT(o) FROM OceanImpl o, ActivityImpl a WHERE a.ocean = o.id";
        return new HashSet<>(findAll(hql));
    }

    public E findOceanByActivity(Activity activity) {
        GetOceanIdByCoordinateQuery query = new GetOceanIdByCoordinateQuery("activity", "the_geom", Objects.requireNonNull(activity).getTopiaId());
        String oceanId = topiaSqlSupport.findSingleResult(query);
        return oceanId == null ? null : forTopiaIdEquals(oceanId).findUnique();
    }

    public E findOceanByHarbour(Harbour harbour) {
        GetOceanIdByCoordinateQuery query = new GetOceanIdByCoordinateQuery("harbour", "the_geom", Objects.requireNonNull(harbour).getTopiaId());
        String oceanId = topiaSqlSupport.findSingleResult(query);
        return oceanId == null ? null : forTopiaIdEquals(oceanId).findUnique();
    }

    private static class GetOceanIdByCoordinateQuery extends TopiaSqlQuery<String> {

        private final String tableName;
        private final String geoColumnName;
        private final String id;

        GetOceanIdByCoordinateQuery(String tableName, String geoColumnName, String id) {
            this.tableName = Objects.requireNonNull(tableName);
            this.geoColumnName = Objects.requireNonNull(geoColumnName);
            this.id = Objects.requireNonNull(id);
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            @SuppressWarnings("SqlResolve")
            PreparedStatement ps = connection.prepareStatement(
                    String.format("SELECT o.topiaid FROM %s a, Ocean o  WHERE a.topiaId = ? AND ST_DWithin(a.the_geom, o.%s, 1)", tableName, geoColumnName));
            ps.setString(1, id);
            return ps;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }
    }
}
