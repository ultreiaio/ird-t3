/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.ImmutableMap;
import fr.ird.t3.entities.T3Predicates;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractLengthWeightConversionTopiaDao<E extends LengthWeightConversion> extends GeneratedLengthWeightConversionTopiaDao<E> {

    public static final String COEFFICIENT_A = "a";
    public static final String COEFFICIENT_B = "b";

    public int count(Ocean ocean, Species species) {
        return findAll("FROM LengthWeightConversionImpl WHERE ocean = :ocean AND species = :species", ImmutableMap.of("ocean", ocean, "species", species)).size();
    }

    public E findLengthWeightConversion(Species species, Ocean ocean, int sex, Date date) {
        List<E> list = filterBySpecies(species);
        if (list.isEmpty()) {
            // none for species
            return null;
        }
        // filter by ocean (or use null ocean if none found by ocean)
        list = filterByOcean(list, ocean);
        if (list.isEmpty()) {
            // none for ocean
            return null;
        }
        // filter by sex (or use sex = 0 if none found by sex)
        list = filterBySex(list, sex);
        if (list.isEmpty()) {
            // none for sex
            return null;
        }
        // filter by begin date
        list = filterByBeginDate(list, date);
        if (list.isEmpty()) {
            // none for begin date
            return null;
        }
        // filter by end date
        list = filterByEndDate(list, date);
        if (list.isEmpty()) {
            // non for end date
            return null;
        }
        // at last, HiLander rule: it should stay only one
        if (list.size() > 1) {
            throw new IllegalStateException(
                    String.format("Should have only one conversion line for species %s, ocean %s, sex %d at date %s", species.getLabel1(), ocean, sex, date));
        }
        return list.get(0);
    }

    private List<E> filterBySpecies(Species species) {
        return forSpeciesEquals(species).stream().filter(T3Predicates.LENGTH_WEIGHT_CONVERSION_BY_COEFFICIENT).collect(Collectors.toList());
    }

    private List<E> filterByOcean(List<E> list, Ocean ocean) {
        List<E> collect = list.stream().filter(T3Predicates.newLengthWeightConversionForOcean(ocean)).collect(Collectors.toList());
        if (collect.isEmpty() && ocean != null) {
            collect = list.stream().filter(T3Predicates.newLengthWeightConversionForOcean(null)).collect(Collectors.toList());
        }
        return collect;
    }

    private List<E> filterBySex(List<E> list, int sex) {
        List<E> collect = list.stream().filter(T3Predicates.newLengthWeightConversionForSexe(sex)).collect(Collectors.toList());
        if (collect.isEmpty() && sex != 0) {
            collect = list.stream().filter(T3Predicates.newLengthWeightConversionForSexe(0)).collect(Collectors.toList());
        }
        return collect;
    }

    private List<E> filterByBeginDate(List<E> list, Date beginDate) {
        return list.stream().filter(T3Predicates.newLengthWeightConversionForDatedebut(beginDate)).collect(Collectors.toList());
    }

    private List<E> filterByEndDate(List<E> list, Date endDate) {
        return list.stream().filter(T3Predicates.newLengthWeightConversionForDateFin(endDate)).collect(Collectors.toList());
    }

}
