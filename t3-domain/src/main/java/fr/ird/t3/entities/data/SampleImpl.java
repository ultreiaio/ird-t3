/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.reference.Species;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SampleImpl extends SampleAbstract {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(SampleImpl.class);

    @Override
    public void deleteComputedData() {
        log.debug(String.format("delete computed data from %s", getTopiaId()));
        ComputedDataHelper.deleteComputedData(this);
    }

    @Override
    public void deleteComputedDataLevel0() {
        ComputedDataHelper.deleteComputedDataLevel0(getSampleSet());
    }

    @Override
    public void deleteComputedDataLevel1() {
        setExtrapolateSampleCountedAndMeasured(false);
        setStandardizeSampleMeasures(false);
        setComputeWeightOfCategoriesForSet(false);
        setRedistributeSampleNumberToSet(false);
        setExtrapolateSampleWeightToSet(false);
        setConvertSetSpeciesFrequencyToWeight(false);
        setConvertSampleSetSpeciesFrequencyToWeight(false);
        clearStandardiseSampleSpecies();
        ComputedDataHelper.deleteComputedDataLevel1(getSampleSet());
    }

    @Override
    public void deleteComputedDataLevel2() {
        ComputedDataHelper.deleteComputedDataLevel2(getSampleSet());
    }

    @Override
    public void deleteComputedDataLevel3() {
        ComputedDataHelper.deleteComputedDataLevel3(getSampleSet());
    }

    @Override
    public float getTotalStandardiseSampleSpeciesFrequencyNumber() {
        float nb = 0;
        for (StandardiseSampleSpecies standardiseSampleSpecies : getStandardiseSampleSpecies()) {
            nb += T3EntityHelper.getTotal(standardiseSampleSpecies.getStandardiseSampleSpeciesFrequency(), StandardiseSampleSpeciesFrequency::getNumber);
        }
        return nb;
    }

    @Override
    public float getTotalStandardiseSampleSpeciesFrequencyNumber(Species species) {
        float nb = 0;
        for (StandardiseSampleSpecies standardiseSampleSpecies : getStandardiseSampleSpecies()) {
            if (species.equals(standardiseSampleSpecies.getSpecies())) {
                nb += T3EntityHelper.getTotal(standardiseSampleSpecies.getStandardiseSampleSpeciesFrequency(), StandardiseSampleSpeciesFrequency::getNumber);
            }
        }
        return nb;
    }
}
