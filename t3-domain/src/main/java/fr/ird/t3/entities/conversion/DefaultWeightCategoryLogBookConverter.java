package fr.ird.t3.entities.conversion;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversion;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by tchemit on 25/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DefaultWeightCategoryLogBookConverter implements WeightCategoryLogBookConverter {

    private final WeightCategoryLogBookConversion conversion;
    private final Map<Species, WeightCategoryLogBookConversionSpecies> speciesMap;
    private final ImmutableMap<String, WeightCategoryTreatment> weightCategoryTreatmentMap;

    private DefaultWeightCategoryLogBookConverter(WeightCategoryLogBookConversion conversion, Map<Species, WeightCategoryLogBookConversionSpecies> speciesMap, ImmutableMap<String, WeightCategoryTreatment> weightCategoryTreatmentMap) {
        this.conversion = conversion;
        this.speciesMap = speciesMap;
        this.weightCategoryTreatmentMap = weightCategoryTreatmentMap;
    }

    public static DefaultWeightCategoryLogBookConverter newInstance(T3TopiaPersistenceContext transaction, WeightCategoryLogBookConversion conversion) {
        Map<Species, WeightCategoryLogBookConversionSpecies> speciesMap = new LinkedHashMap<>();
        for (WeightCategoryLogBookConversionSpecies conversionSpecies : conversion.getDistributions()) {
            if (conversionSpecies.isSpeciesEmpty()) {
                // special case for default species (use a null species)
                speciesMap.put(null, conversionSpecies);
            } else {
                for (Species species : conversionSpecies.getSpecies()) {
                    speciesMap.put(species, conversionSpecies);
                }
            }
        }
        ImmutableMap<String, WeightCategoryTreatment> weightCategoryTreatmentMap = Maps.uniqueIndex(transaction.getWeightCategoryTreatmentDao().findAll(), WeightCategoryTreatment::getTopiaId);
        return new DefaultWeightCategoryLogBookConverter(conversion, speciesMap, weightCategoryTreatmentMap);
    }

    @Override
    public boolean accept(Ocean ocean, SchoolType schoolType) {
        return conversion.getOcean().equals(ocean) && conversion.getSchoolType().equals(schoolType);
    }

    @Override
    public Map<WeightCategoryTreatment, Float> distribute(Species species, Collection<ElementaryCatch> catches) {
        float totalWeight = WeightCategoryLogBookConverter.getTotalWeight(catches);
        if (totalWeight == 0) {
            // this means no catches
            return new HashMap<>();
        }
        WeightCategoryLogBookConversionSpecies conversionSpecies = speciesMap.get(species);
        if (conversionSpecies == null) {
            conversionSpecies = speciesMap.get(null);
        }
        Map<String, Map<Integer, Float>> distributions = conversionSpecies.getDistributions2();
        if (distributions.size() == 1) {
            // special case when only one distribution to apply (this means all catches are going to one treatment category)
            Map<WeightCategoryTreatment, Float> result = new HashMap<>();
            Map.Entry<String, Map<Integer, Float>> entry = distributions.entrySet().iterator().next();
            WeightCategoryTreatment category = weightCategoryTreatmentMap.get(entry.getKey());
            result.put(category, totalWeight);
            return result;
        }
        return distributeComplex(distributions, catches);
    }

    private Map<WeightCategoryTreatment, Float> distributeComplex(Map<String, Map<Integer, Float>> distributions, Collection<ElementaryCatch> catches) {
        // we use unknown weight to reapply
        float unknownWeight = WeightCategoryLogBookConverter.getUnknownWeight(catches);

        Map<WeightCategoryTreatment, Float> unknownResult = new HashMap<>();
        float totalWeight = 0f;
        for (Map.Entry<String, Map<Integer, Float>> e : distributions.entrySet()) {
            WeightCategoryTreatment category = weightCategoryTreatmentMap.get(e.getKey());
            float weight = WeightCategoryLogBookConverter.distribute(e.getValue(), catches);
            unknownResult.put(category, weight);
            // sum total weight
            totalWeight += weight;
        }
        if (unknownWeight == 0) {
            // no unknown weight to redistribute, can keep current result
            return unknownResult;
        }

        // there is some unknown weight to redistribute
        Map<WeightCategoryTreatment, Float> result = new HashMap<>();

        if (totalWeight == 0) {
            // distribute unknown weight in all weight categories
            float weight = unknownWeight / unknownResult.keySet().size();
            for (WeightCategoryTreatment e : unknownResult.keySet()) {
                // store new weight
                result.put(e, weight);
            }
            return result;
        }

        for (Map.Entry<WeightCategoryTreatment, Float> e : unknownResult.entrySet()) {
            WeightCategoryTreatment weightCategory = e.getKey();
            // weight (with no unknown category)
            float weight = e.getValue();
            // rate to distribute
            float rate = weight / totalWeight;
            // weight redistributed
            float unknownWeightToAdd = unknownWeight * rate;
            // store new weight
            result.put(weightCategory, weight + unknownWeightToAdd);
        }
        return result;
    }
}
