package fr.ird.t3.entities.user;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.google.common.io.Files;
import fr.ird.t3.T3Config;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.ConstructorProperties;
import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * To persist a list of {@link T3User} in a single text file using {@code yaml} format.
 * <p>
 * Created by tchemit on 26/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3Users {

    private static final Log log = LogFactory.getLog(T3Users.class);
    private transient File storageFile;
    private transient Map<String, UserDatabase> inputs;
    private transient Map<String, UserDatabase> outputs;
    private List<T3User> users;

    public T3Users() {
        this.users = new LinkedList<>();
    }

    private T3Users(File storageFile) {
        this.storageFile = storageFile;
        this.users = new LinkedList<>();
    }

    public static T3Users create(T3Config configuration) {
        File storageFile = configuration.getUsersFile();
        T3Users result;
        if (storageFile.exists()) {
            log.info(String.format("Loading users from file: %s", storageFile));
            try (Reader fileReader = Files.newReader(storageFile, StandardCharsets.UTF_8)) {
                YamlReader reader = new YamlReader(fileReader, createConfig());
                try {
                    result = reader.read(T3Users.class);
                    result.setStorageFile(storageFile);
                } finally {
                    reader.close();
                }
            } catch (Exception e) {
                throw new RuntimeException(String.format("Could not read users from file: %s", storageFile), e);
            }
        } else {
            result = new T3Users(storageFile);
        }
        return result;
    }

    private static YamlConfig createConfig() {
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setCanonical(false);
        yamlConfig.writeConfig.setWriteRootTags(false);
        yamlConfig.writeConfig.setWriteRootElementTags(false);
        yamlConfig.setPropertyElementType(T3Users.class, "users", T3User.class);
        yamlConfig.setPropertyElementType(T3User.class, "inputs", UserDatabase.class);
        yamlConfig.setPropertyElementType(T3User.class, "outputs", UserDatabase.class);
        return yamlConfig;

    }

    public T3UserJavaBeanDefinition.T3UserStream usersStream() {
        return T3UserJavaBeanDefinition.stream(getUsers());
    }

    public List<T3User> getUsers() {
        return users;
    }

    public void setUsers(List<T3User> users) {
        this.users = users;
    }

    public Map<String, UserDatabase> getInputs() {
        return inputs == null ? inputs = buildCache(T3User::getInputs) : inputs;
    }

    public Map<String, UserDatabase> getOutputs() {
        return outputs == null ? outputs = buildCache(T3User::getOutputs) : outputs;
    }

    public void save() {
        inputs = outputs = null;
        log.info(String.format("Store users to %s", storageFile));
        try (BufferedWriter writer = Files.newWriter(storageFile, StandardCharsets.UTF_8)) {
            YamlWriter yamlWriter = new YamlWriter(writer, createConfig());
            try {
                yamlWriter.write(this);
            } finally {
                yamlWriter.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("Could not write users to file: %s", storageFile), e);
        }
    }

    private Map<String, UserDatabase> buildCache(Function<T3User, List<UserDatabase>> function) {
        Map<String, UserDatabase> result = new TreeMap<>();
        getUsers().stream().flatMap(u -> function.apply(u).stream()).forEach(u -> result.put(u.getId(), u));
        return result;
    }

    public UserDatabase getInput(String dbConfigurationId) {
        return Objects.requireNonNull(getInputs().get(dbConfigurationId), String.format("Can't find input user database with id: %s", dbConfigurationId));
    }

    public UserDatabase getOutput(String dbConfigurationId) {
        return Objects.requireNonNull(getOutputs().get(dbConfigurationId), String.format("Can't find output user database with id: %s", dbConfigurationId));
    }

    public void setStorageFile(File storageFile) {
        this.storageFile = storageFile;
    }
}
