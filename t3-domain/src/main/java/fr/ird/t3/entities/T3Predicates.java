/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionTopiaDao;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Store useful predicates on entities.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1
 */
public class T3Predicates {

    /**
     * Predicates that trip ends a complete trip.
     *
     * @see Trip#getFishHoldEmpty()
     * @since 1.1
     */
    public static final Predicate<Trip> TRIP_ENDS_A_COMPLETE_TRIP = input -> input.getFishHoldEmpty() == 1;

    public static final Predicate<LengthWeightConversion> LENGTH_WEIGHT_CONVERSION_BY_COEFFICIENT = input -> {
        boolean result = true;
        Double a = input.getCoefficientValue(LengthWeightConversionTopiaDao.COEFFICIENT_A);
        if (a == null || a == 0) {
            result = false;
        }
        if (result) {
            Double b = input.getCoefficientValue(LengthWeightConversionTopiaDao.COEFFICIENT_B);
            // on autorise d'avoir b à 0 (mais cela ne permet plus de calculer la taille à partir du poids)
            if (b == null) {
                result = false;
            }
        }

        return result;
    };

    public static Predicate<SpeciesAware> newSpeciesFilter(Species species) {
        return input -> species.equals(input.getSpecies());
    }

    public static <E extends SpeciesAware> Predicate<E> newSpeciesContainsFilter(Collection<Species> species) {
        return input -> species.contains(input.getSpecies());
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForOcean(Ocean ocean) {
        return input -> {
            boolean result;
            Ocean inputOcean = input.getOcean();
            if (ocean == null) {
                result = inputOcean == null;
            } else {
                result = ocean.equals(inputOcean);
            }
            return result;
        };
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForSexe(int sexe) {
        return input -> sexe == input.getSexe();
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForDatedebut(Date dateDebut) {
        return input -> input.getBeginDate().before(dateDebut) || input.getBeginDate().equals(dateDebut);
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForDateFin(Date dateFin) {
        return input -> {
            Date date = input.getEndDate();
            boolean result;
            if (dateFin == null) {
                // on n'accepte que les parametrages selon les critères suivants :
                // - sans date de fin (i.e en cours de validite)
                result = date == null;
            } else {
                // on n'accepte que les parametrages selon les critères suivants :
                // - sans date de fin (i.e en cours de validite)
                // - ceux dont la date de fin est avant la date de fin donnée
                result = date == null || date.after(dateFin) || date.equals(dateFin);
            }
            return result;
        };
    }

    /**
     * Predicates that the trips vessel is one of the given vessels.
     *
     * @param vessels usable vessels
     * @return {@code true} if the trip  vessel is one of the given one, {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingVessel(Collection<Vessel> vessels) {
        return input -> vessels.contains(input.getVessel());
    }

    /**
     * Predicates that the trips harbour is one of the given harbours.
     *
     * @param harbours usable harbours
     * @return {@code true} if the trip harbour is one of the given one, {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingHarbour(Collection<Harbour> harbours) {
        return input -> harbours.contains(input.getLandingHarbour());
    }

    /**
     * Predicates that trip has at least one activity using one of the given oceans.
     *
     * @param oceans ocean to filter
     * @return {@code true} if trip has at least one activity using one of the given trip, {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingOcean(Collection<Ocean> oceans) {
        return input -> {
            boolean result = false;
            if (input.isActivityNotEmpty()) {
                Set<Ocean> allOceans = input.getAllOceans();
                for (Ocean ocean : allOceans) {
                    if (oceans.contains(ocean)) {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        };
    }

    /**
     * Predicates that the trips departure year is one of the given years.
     *
     * @param years usable departure year
     * @return {@code true} if the trip departure year is one of the given one,
     * {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingDepartureYear(Collection<Integer> years) {
        return input -> {
            Integer year = T3Functions.TRIP_TO_DEPARTURE_YEAR.apply(input);
            return years.contains(year);
        };
    }

    /**
     * Predicates that the vessel simple type is one of the given vessel simple types.
     *
     * @param vesselSimpleTypes usable vessel simple types
     * @return {@code true} if the vessel simple type is one of the given one,
     * {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Vessel> vesselUsingVesselType(Collection<VesselSimpleType> vesselSimpleTypes) {
        return input -> vesselSimpleTypes.contains(input.getVesselType().getVesselSimpleType());
    }

    /**
     * Predicates that the vessel fleet country is one of the given countries.
     *
     * @param fleetCountries usable fleet countries
     * @return {@code true} if the vessel fleet country is one of the given one,
     * {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Vessel> vesselUsingFleetCountry(Collection<Country> fleetCountries) {
        return input -> fleetCountries.contains(input.getFleetCountry());
    }

    /**
     * Predicates that the vessel flag country is one of the given countries.
     *
     * @param flagCountries usable flag countries
     * @return {@code true} if the vessel flag country is one of the given one,
     * {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Vessel> vesselUsingFlagCountry(Collection<Country> flagCountries) {
        return input -> flagCountries.contains(input.getFlagCountry());
    }

//    /**
//     * Predicates that a entity is equals to the given same topia id.
//     *
//     * @param id topia id to test
//     * @return {@code true} if entity has same topia id as the given one, {@code false} otherwise.
//     * @since 1.1.2
//     */
//    public static <E extends TopiaEntity> Predicate<E> equalsTopiaEntity(String id) {
//        return input -> id.equals(input.getTopiaId());
//    }

//    /**
//     * Predicates that trip is inside a complete trip (as a simple trip or a partial complete trip).
//     * <p/>
//     * Notes that a such trip can be used in level 0, and 1.
//     *
//     * @see Trip#getCompletionStatus()
//     * @since 1.2
//     */
//    public static final Predicate<Trip> TRIP_INSIDE_A_COMPLETE_TRIP = input -> {
//        Integer completionStatus = input.getCompletionStatus();
//        return completionStatus != null && completionStatus > 0;
//    };

}
