/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import com.google.auto.service.AutoService;
import fr.ird.t3.services.ioc.Injector;

import static org.nuiton.i18n.I18n.n;

/**
 * Meta of a {@link ZoneET}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@AutoService(ZoneStratumAwareMeta.class)
public class ZoneETMeta extends AbstractZoneStratumAwareMeta {

    private static final long serialVersionUID = 1L;

    public static final String ZONE_META_ID = "zoneet";

    public ZoneETMeta() {
        super(ZONE_META_ID, ZoneET.class, n("t3.common.zoneET"));
    }

}
