/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.base.Preconditions;

public class WeightCategoryTreatmentImpl extends WeightCategoryTreatmentAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public int getCode() {
        // not used
        return 0;
    }

    @Override
    public int getOldCategoryCode() {

        if (getMin() == null && getMax() == null) {

            // undefined category --> code 9
            return 9;
        }

        Preconditions.checkState(getMin() != null,
                                 "category can not having a null min value since his max is not also null");
        if (getMin() == 0) {

            // category less 10 --> code 1
            return 1;
        }

        if (getMax() != null) {

            // category 10 to 30 --> code 2
            return 2;
        }

        if (getMax() == null) {

            // category more than 10 or more than 30 --> code 3
            return 3;
        }

        throw new IllegalStateException("Can not come here!");
    }
}
