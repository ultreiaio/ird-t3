/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;


import java.util.Date;

public class ZoneVersion extends GeneratedZoneVersion {

    private static final long serialVersionUID = -1L;

    public ZoneVersion() {
    }

    public ZoneVersion(Zone zone) {
        this(
                zone.getVersionId(),
                zone.getVersionLabel(),
                zone.getVersionStartDate(),
                zone.getVersionEndDate()
        );
    }

    public ZoneVersion(String versionId,
                       String versionLabel,
                       Date versionStartDate,
                       Date versionEndDate) {
        setVersionId(versionId);
        setVersionLabel(versionLabel);
        setVersionStartDate(versionStartDate);
        setVersionEndDate(versionEndDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ZoneVersion)) {
            return false;
        }

        ZoneVersion that = (ZoneVersion) o;
        return versionId.equals(that.versionId);

    }

    @Override
    public int hashCode() {
        return versionId.hashCode();
    }

    @Override
    public String getId() {
        return getVersionId();
    }
}
