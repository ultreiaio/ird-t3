/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WellImpl extends WellAbstract {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(WellImpl.class);

    @Override
    public void deleteComputedData() {
        log.debug(String.format("delete computed data from %s", getTopiaId()));
        ComputedDataHelper.deleteComputedData(this);
    }

    @Override
    public void deleteComputedDataLevel0() {
        clearWellSetAllSpecies();
    }

    @Override
    public void deleteComputedDataLevel1() {
        // nothing to do
    }

    @Override
    public void deleteComputedDataLevel2() {
        // nothing to do
    }

    @Override
    public void deleteComputedDataLevel3() {
        // nothing to do
    }
}
