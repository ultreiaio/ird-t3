/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.Trip;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class AbstractSampleTypeTopiaDao<E extends SampleType> extends GeneratedSampleTypeTopiaDao<E> {

    public static Set<SampleType> getAllSampleTypes(Collection<Trip> trips) {
        Set<SampleType> result = new HashSet<>();
        for (Trip trip : trips) {
            if (trip.isSampleNotEmpty()) {
                for (Sample sample : trip.getSample()) {
                    SampleType sampleType = sample.getSampleType();
                    result.add(sampleType);
                }
            }
        }
        return result;
    }
}
