/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.reference.Species;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class AbstractSetSpeciesFrequencyTopiaDao<E extends SetSpeciesFrequency> extends GeneratedSetSpeciesFrequencyTopiaDao<E> {

    public static <E extends SetSpeciesFrequency> void collectLengthClasses(Multimap<Species, E> values, Multimap<Species, Integer> lengthClasses) {
        for (Species species : values.keySet()) {
            Collection<E> frequencies = values.get(species);
            Set<Integer> classes = new HashSet<>();
            for (E frequency : frequencies) {
                classes.add(frequency.getLfLengthClass());
            }
            lengthClasses.putAll(species, classes);
        }
    }

    public static Map<Integer, Float> collectSampleCount(Collection<SetSpeciesFrequency> newDatas) {
        Map<Integer, Float> result = new HashMap<>();
        for (SetSpeciesFrequency newData : newDatas) {
            Integer lengthClass = newData.getLfLengthClass();
            Float newCount = result.get(lengthClass);
            if (newCount == null) {
                newCount = 0f;
            }
            newCount += newData.getNumber();
            result.put(lengthClass, newCount);
        }
        return result;
    }

    public static float collectSimpleSampleCount(Collection<SetSpeciesFrequency> newDatas) {
        float result = 0f;
        for (SetSpeciesFrequency newData : newDatas) {
//            Integer lengthClass = newData.getLfLengthClass();
            result += newData.getNumber();
        }
        return result;
    }

    public Map<Integer, E> findAllByActivityAndSpeciesOrderedByLengthClass(Activity activity, Species species) throws TopiaException {
        @SuppressWarnings("unchecked")
        Collection<E> setSpeciesFrequency = (Collection<E>) activity.getSetSpeciesFrequency();
        Iterable<E> forSpecies = setSpeciesFrequency.stream().filter(T3Predicates.newSpeciesFilter(species)).collect(Collectors.toList());

        return new TreeMap<>(Maps.uniqueIndex(forSpecies, SetSpeciesFrequency::getLfLengthClass));
    }
}
