/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.util.TopiaEntityMap;

/**
 * A map of topia entities associated with their type ({@link T3EntityEnum}).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see T3EntityEnum
 * @since 1.0
 */
public class T3EntityMap extends TopiaEntityMap<T3EntityEnum, TopiaEntity> {

    private static final long serialVersionUID = 1L;

    @Override
    protected T3EntityEnum getType(Class<?> e) {
        return T3EntityEnum.valueOf(e);
    }
}
