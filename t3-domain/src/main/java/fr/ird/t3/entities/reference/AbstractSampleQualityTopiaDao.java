/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.Trip;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class AbstractSampleQualityTopiaDao<E extends SampleQuality> extends GeneratedSampleQualityTopiaDao<E> {

    public static Set<SampleQuality> getAllSampleQualities(Collection<Trip> trips) {
        Set<SampleQuality> result = new HashSet<>();
        for (Trip trip : trips) {
            if (trip.isSampleNotEmpty()) {
                for (Sample sample : trip.getSample()) {
                    SampleQuality sampleQuality = sample.getSampleQuality();
                    result.add(sampleQuality);
                }
            }
        }
        return result;
    }
}
