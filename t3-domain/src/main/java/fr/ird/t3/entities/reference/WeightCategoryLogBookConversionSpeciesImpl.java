package fr.ird.t3.entities.reference;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.Map;

public class WeightCategoryLogBookConversionSpeciesImpl extends WeightCategoryLogBookConversionSpeciesAbstract {

    private static final long serialVersionUID = 1L;

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Map<String, Map<Integer, Float>> distributions2;

    public static String toString(Map<String, Map<Integer, Float>> distributions) {
        return gson.toJson(distributions);
    }

    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public String getLabel1() {
        return null;
    }

    @Override
    public boolean isStatus() {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Map<Integer, Float>> getDistributions2() {
        if (distributions2==null) {
            distributions2 = gson.fromJson(getDistribution(), new TypeToken<Map<String, Map<Integer, Float>>>(){}.getType());
        }
        return distributions2;
    }
}
