/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.entities.user.JdbcConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Helper for entities (to sort them and other stuff).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3EntityHelper {

    private static final Log log = LogFactory.getLog(T3EntityHelper.class);

    private T3EntityHelper() {
        // hide helper constructor
    }

    public static <T extends T3ReferenceEntity> List<String> selectIdsByCodes(Collection<T> universe, Integer... codes) {
        List<String> ids = new ArrayList<>();
        Map<Integer, T> byCode = splitByCode(universe);
        for (Integer code : codes) {
            T t = byCode.get(code);
            if (t != null) {
                ids.add(t.getTopiaId());
            }
        }
        return ids;
    }

    public static <T extends T3ReferenceEntity> Map<Integer, T> splitByCode(Collection<T> entities) {
        return Maps.uniqueIndex(entities, T3ReferenceEntity::getCode);
    }

    public static Map<String, Species> splitByCodeFAO(Collection<Species> entities) {
        return Maps.uniqueIndex(entities, input -> {
            String faoId = input.getFaoId();
            return faoId == null ? "Undefined" + System.nanoTime() : faoId;
        });
    }

    public static <T extends TopiaEntity> Map<String, T> splitByTopiaId(Collection<T> entities) {
        return Maps.uniqueIndex(entities, TopiaEntity::getTopiaId);
    }

    public static <T extends Enum<T>> EnumSet<T> allBefore(Class<T> type, T step, T[] values) {
        EnumSet<T> result = EnumSet.noneOf(type);
        int maximumOrdinal = step.ordinal();
        for (T level0Step : values) {
            if (maximumOrdinal >= level0Step.ordinal()) {
                result.add(level0Step);
            }
        }
        return result;
    }

    public static <T extends Enum<T>> EnumSet<T> allAfter(Class<T> type, T step, T[] values) {
        EnumSet<T> result = EnumSet.noneOf(type);
        int minimumOrdinal = step.ordinal();
        for (T level0Step : values) {
            if (minimumOrdinal < level0Step.ordinal()) {
                result.add(level0Step);
            }
        }
        return result;
    }

    public static Connection newJDBCConnection(JdbcConfiguration configuration) throws SQLException {
        String jdbcUrl = configuration.getUrl();
        String login = configuration.getLogin();
        String password = configuration.getPassword();
        Connection conn = DriverManager.getConnection(jdbcUrl, login, password);
        log.debug(String.format("connexion success for user %s at [%s]", login, jdbcUrl));
        return conn;
    }

//    public static void releaseH2RootContext(TopiaApplicationContext context) {
//        releaseRootContext(context);
//        new JdbcH2Helper(context.getConfiguration()).runUpdate("SHUTDOWN");
//    }

    public static void releaseRootContext(TopiaApplicationContext context) {
        log.info("release database " + context.getConfiguration().getJdbcConnectionUrl());
        try {
            releaseContext(context);
        } catch (TopiaException e) {
            // we don't want this to throw an exception, wants to close all possible opened contexts
            log.error("Could not close context " + context, e);
        }
    }

    private static void releaseContext(TopiaApplicationContext rootContext) {
        if (rootContext != null && !rootContext.isClosed()) {
            rootContext.close();
        }
    }

    public static void checkJDBCConnection(JdbcConfiguration configuration) throws SQLException {
        Connection conn = null;
        try {
            conn = newJDBCConnection(configuration);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    log.error("Could not close properly connection to " + configuration.getUrl(), e);
                }
            }
        }
    }

//    public static JdbcConfiguration newJdbcConfiguration(Properties dbConf) {
//        JdbcConfiguration result = new JdbcConfiguration();
//        result.setUrl(dbConf.getProperty(HibernateAvailableSettings.URL));
//        result.setLogin(dbConf.getProperty(HibernateAvailableSettings.USER));
//        result.setPassword(dbConf.getProperty(HibernateAvailableSettings.PASS));
//        return result;
//    }

    public static <E> float getTotal(Collection<E> data, Function<E, Float> function) {
        return getTotal(data, function, e -> true);
    }

    private static <E> float getTotal(Collection<E> data, Function<E, Float> function, Predicate<E> predicate) {
        float result = 0;
        for (E aCatch : data) {
            if (predicate.test(aCatch))
                result += function.apply(aCatch);
        }
        return result;
    }

    public static <E extends SpeciesAware> float getTotal(Collection<E> data, Function<E, Float> function, Predicate<? super SpeciesAware> predicate, Supplier<String> errorMessage) {
        float result = 0;
        for (E aCatch : data) {
            if (predicate.test(aCatch)) {
                Float apply = function.apply(aCatch);
                Objects.requireNonNull(apply, errorMessage.get() + " on data: " + aCatch);
                result += apply;
            }
        }
        return result;
    }

//    public static void closeConnection(TopiaContext transaction) {
//        if (transaction == null) {
//            if (log.isTraceEnabled()) {
//                log.trace("no transaction to close");
//            }
//        } else if (transaction.isClosed()) {
//            if (log.isTraceEnabled()) {
//                log.trace("transaction " + transaction + " is already closed");
//            }
//        } else {
//            if (log.isDebugEnabled()) {
//                log.debug("closing transaction " + transaction);
//            }
//
//            try {
//                Transaction tx = ((TopiaContextImplementor) transaction).getHibernate().getT3TopiaPersistenceContext();
//                if (!tx.wasCommitted() && !tx.wasRolledBack()) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("rollback transaction!");
//                    }
//                    tx.rollback();
//                }
//                transaction.closeContext();
//            } catch (TopiaException e) {
//                throw new TopiaRuntimeException(e);
//            }
//        }
//    }
}
