/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.function.Function;

/**
 * Useful functions on entities.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1
 */
public class T3Functions {


//    public static final Function<TopiaEntity, String> TO_TOPIA_ID = TopiaEntity::getTopiaId;

//    public static final Function<T3ReferenceEntity, Integer> TO_REFERENTIEL_CODE = T3ReferenceEntity::getCode;

//    public static final Function<Trip, Vessel> TRIP_BY_VESSEL = Trip::getVessel;

//    public static final Function<ActivityAware, Activity> ACTIVITY_AWARE_BY_ACTIVITY = ActivityAware::getActivity;

    public static final Function<Activity, Long> ACTIVITY_BY_LONG_DAY = new Function<Activity, Long>() {
        @Override
        public Long apply(Activity input) {
            Date d = roundDateToDay(input.getDate());
            return d.getTime();
        }

        Date roundDateToDay(Date date) {
            Calendar calendar = DateUtils.toCalendar(date);
            int year = calendar.get(Calendar.YEAR);
            int day = calendar.get(Calendar.DAY_OF_YEAR);
            calendar.setTimeInMillis(0);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.DAY_OF_YEAR, day);
            return calendar.getTime();
        }
    };

    public static final Function<? extends SpeciesAware, Species> SPECIES_AWARE_BY_SPECIES = SpeciesAware::getSpecies;

//    public static final Function<SpeciesLengthStep, Integer> SPECIES_LENGTH_STEP_BY_LD1_CLASS = SpeciesLengthStep::getLd1Class;

    public static final Function<Trip, Country> TRIP_TO_FLEET_COUNTRY = input -> input.getVessel().getFleetCountry();

    public static final Function<Trip, T3Date> TRIP_TO_LANDING_DATE = input -> T3Date.newDate(input.getLandingDate());

    public static final Function<Trip, Integer> TRIP_TO_LANDING_YEAR = new Function<Trip, Integer>() {

        final Calendar calendar = Calendar.getInstance();

        @Override
        public Integer apply(Trip input) {
            Date activityDate = input.getLandingDate();
            calendar.setTime(activityDate);
            return calendar.get(Calendar.YEAR);
        }
    };

    public static final Function<Trip, Integer> TRIP_TO_DEPARTURE_YEAR = new Function<Trip, Integer>() {
        final Calendar cal = Calendar.getInstance();

        @Override
        public Integer apply(Trip input) {
            Date departureDate = input.getDepartureDate();
            cal.setTime(departureDate);
            return cal.get(Calendar.YEAR);
        }
    };

//    public static final Function<Trip, TripDTO> TRIP_TO_DTO = Trip::toDTO;
//    public static final Function<SetSpeciesFrequency, Integer> SET_SPECIES_FREQUENCY_BY_LF_LENGTH_CLASS = SetSpeciesFrequency::getLfLengthClass;
//    public static final Function<SetSpeciesFrequency, Float> SET_SPECIES_FREQUENCY_BY_COUNT = SetSpeciesFrequency::getNumber;
//    public static final Function<ExtrapolatedAllSetSpeciesFrequency, Float> EXTRAPOLATED_ALL_SET_SPECIES_FREQUENCY_BY_COUNT = ExtrapolatedAllSetSpeciesFrequency::getNumber;

//    public static final Function<WeightCategoryTreatmentAware, WeightCategoryTreatment> BY_WEIGHT_CATEGORY_TREATMENT = WeightCategoryTreatmentAware::getWeightCategoryTreatment;
//    public static final Function<WeightCategorySampleAware, WeightCategorySample> BY_WEIGHT_CATEGORY_SAMPLE = WeightCategorySampleAware::getWeightCategorySample;

//    public static final Function<CorrectedElementaryCatch, Float> CORRECTED_ELEMENTARY_CATCH_TO_CATCH_WEIGHT = CorrectedElementaryCatch::getCatchWeight;

//    public static final Function<CorrectedElementaryCatch, Float> CORRECTED_ELEMENTARY_CATCH_TO_CORRECTED_CATCH_WEIGHT = CorrectedElementaryCatch::getCorrectedCatchWeight;

//    public static final Function<SetSpeciesCatWeight, Float> SET_SPECIES_CAT_WEIGHT_TO_WEIGHT = SetSpeciesCatWeight::getWeight;

//    public static final Function<StandardiseSampleSpeciesFrequency, Float> STANDARDISE_SAMPLE_SPECIES_FREQUENCY_NUMBER = StandardiseSampleSpeciesFrequency::getNumber;
}
