package fr.ird.t3.entities.data;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * A complete trip is a set of trips.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class CompleteTrip implements Iterable<Trip> {

    protected final List<Trip> trips;

    CompleteTrip(List<Trip> trips) {
        // can't create a complete trip with no trips
        Objects.requireNonNull(trips);
        Preconditions.checkState(CollectionUtils.isNotEmpty(trips));
        // check that all trips have same vessel
        Vessel vessel = null;
        for (Trip trip : trips) {
            if (vessel == null) {
                vessel = trip.getVessel();
                continue;
            }
            Preconditions.checkState(vessel.equals(trip.getVessel()));
        }
        this.trips = trips;
    }

    public int getNbTrips() {
        return trips.size();
    }

    public Harbour getLandingHarbour() {

        return getLandingTrip().getLandingHarbour();
    }

    public Trip getDepartureTrip() {
        return trips.get(0);
    }

    public Trip getLandingTrip() {
        return trips.get(trips.size() - 1);
    }

    public Date getLandingDate() {
        return getLandingTrip().getLandingDate();
    }

    public Vessel getVessel() {
        return getLandingTrip().getVessel();
    }

    public Float getRf1() {
        return getLandingTrip().getRf1();
    }

    public float getElementaryCatchTotalWeight(Collection<Species> filteredSpecies) {
        float result = 0;
        for (Trip trip : this) {
            result += trip.getElementaryCatchTotalWeight(filteredSpecies);
        }
        return result;
    }

    public float getElementaryLandingTotalWeight(Collection<Species> filteredSpecies) {
        float result = 0;
        for (Trip trip : this) {
            result += trip.getElementaryLandingTotalWeight(filteredSpecies);
        }
        return result;
    }

    public float getElementaryCatchTotalWeightRf1(Collection<Species> filteredSpecies) {
        float result = 0;
        for (Trip trip : this) {
            result += trip.getElementaryCatchTotalWeightRf1(filteredSpecies);
        }
        return result;
    }

    public void applyRf2(Float rf2) {
        for (Trip trip : this) {
            trip.applyRf2(rf2);
        }
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Iterator<Trip> iterator() {
        return trips.iterator();
    }

    public void removeTrips(Collection<Trip> tripList) {
        tripList.removeAll(trips);
    }
}
