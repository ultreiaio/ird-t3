/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion;

import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryLogBook;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Map;

/**
 * Contract of a converter of log book weight category to weight category
 * treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public interface WeightCategoryLogBookConverter {

    /**
     * Obtains the total weight of given {@code catches} which are in {@code unknown} weight category.
     *
     * @param catches catches to use
     * @return the total weight of given catches which uses the unknown weight category
     * @see WeightCategoryLogBook#isUnknown()
     */
    static float getUnknownWeight(Collection<ElementaryCatch> catches) {
        float result = 0;
        if (CollectionUtils.isNotEmpty(catches)) {
            for (ElementaryCatch aCatch : catches) {
                if (aCatch.getWeightCategoryLogBook().isUnknown()) {
                    result += aCatch.getCatchWeightRf2();
                }
            }
        }
        return result;
    }

    /**
     * Obtains the total weight of given {@code catches} for all weight categories.
     *
     * @param catches catches to use
     * @return the total weight of the the given weight for any weight categories
     */
    static float getTotalWeight(Collection<ElementaryCatch> catches) {
        float result = 0;
        if (CollectionUtils.isNotEmpty(catches)) {
            for (ElementaryCatch aCatch : catches) {
                result += aCatch.getCatchWeightRf2();
            }
        }
        return result;
    }

    /**
     * Given the {@code distribution} of weight categories (keys are log book categories code and
     * values are ratio to apply to weight) and the given {@code catches}, computes the weight.
     *
     * @param distribution distribution of
     * @param catches      catches to use
     * @return the weight computed using the distribution
     */
    static float distribute(Map<Integer, Float> distribution, Collection<ElementaryCatch> catches) {
        float result = 0;
        if (CollectionUtils.isNotEmpty(catches)) {
            for (ElementaryCatch aCatch : catches) {
                Integer weightCategoryCode = aCatch.getWeightCategoryLogBook().getCode();
                Float ratio = distribution.get(weightCategoryCode);
                if (ratio != null) {
                    result += ratio * aCatch.getCatchWeightRf2();
                }
            }
        }
        return result;
    }

    boolean accept(Ocean ocean, SchoolType schoolType);

    Map<WeightCategoryTreatment, Float> distribute(Species species, Collection<ElementaryCatch> catches);

}
