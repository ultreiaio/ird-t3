/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion;

import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OABI;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OABL;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OABO;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OIBI;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OIBL;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OIBO;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OPBI;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OPBL;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OPBO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversion;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentTopiaDao;
import org.nuiton.topia.persistence.TopiaException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * To obtain a  ll available wieght convertors.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public final class WeightCategoryLogBookConverterProvider {

    public static final int OA = 1;
    public static final int OI = 2;
    public static final int OP = 5;
    public static final int BO = 1;
    public static final int BL = 2;
    public static final int BI = 3;

    private final Collection<WeightCategoryLogBookConverter> converters;

    private WeightCategoryLogBookConverterProvider(Collection<WeightCategoryLogBookConverter> converters) {
        this.converters = converters;
    }

    private static WeightCategoryTreatment getCategory(WeightCategoryTreatmentTopiaDao dao,
                                                       int oceanCode,
                                                       int schoolTypeCode,
                                                       Integer min,
                                                       Integer max) {
        WeightCategoryTreatment result = dao.findByOceanSchoolTypeAndBound(oceanCode, schoolTypeCode, min, max);
        return Objects.requireNonNull(result,
                String.format("Could not find weight treatment category with parameters : [ocean:%d, schoolType:%d, min:%d, max:%d]", oceanCode, schoolTypeCode, min, max));
    }

    private static WeightCategoryLogBookConversion getConversion(WeightCategoryLogBookConversionTopiaDao dao,
                                                                 int oceanCode,
                                                                 int schoolTypeCode,
                                                                 String version) {
        WeightCategoryLogBookConversion result = dao.forProperties(
                WeightCategoryLogBookConversion.PROPERTY_OCEAN + ".code", oceanCode,
                WeightCategoryLogBookConversion.PROPERTY_SCHOOL_TYPE + ".code", schoolTypeCode,
                WeightCategoryLogBookConversion.PROPERTY_VERSION, version
        ).findUnique();
        return Objects.requireNonNull(result,
                String.format("Could not find weight treatment category with parameters : [ocean:%d, schoolType:%d, version:%s]", oceanCode, schoolTypeCode, version));
    }

    public static WeightCategoryLogBookConverterProvider newLegacyInstance(T3TopiaPersistenceContext transaction) throws TopiaException {
        Collection<WeightCategoryLogBookConverter> converters = new ArrayList<>();
        WeightCategoryTreatmentTopiaDao dao = transaction.getWeightCategoryTreatmentDao();
        prepareForLegacyOA(dao, converters);
        prepareForLegacyOI(dao, converters);
        prepareForLegacyOP(dao, converters);
        return new WeightCategoryLogBookConverterProvider(converters);
    }

    public static WeightCategoryLogBookConverterProvider newDefaultInstance(T3TopiaPersistenceContext transaction, String version) throws TopiaException {
        Collection<WeightCategoryLogBookConverter> converters = new ArrayList<>();
        WeightCategoryLogBookConversionTopiaDao dao = transaction.getWeightCategoryLogBookConversionDao();

        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OA, BO, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OA, BI, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OA, BL, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OI, BO, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OI, BI, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OI, BL, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OP, BO, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OP, BI, version)));
        converters.add(DefaultWeightCategoryLogBookConverter.newInstance(transaction,getConversion(dao, OP, BL, version)));

        return new WeightCategoryLogBookConverterProvider(converters);
    }

    private static void prepareForLegacyOA(WeightCategoryTreatmentTopiaDao dao, Collection<WeightCategoryLogBookConverter> converters) throws TopiaException {
        {
            // BO Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OABO(
                            getCategory(dao, OA, BO, null, null),
                            getCategory(dao, OA, BO, 0, 10),
                            getCategory(dao, OA, BO, 10, null)
                    );
            converters.add(converter);
        }
        {
            // BL Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OABL(
                            getCategory(dao, OA, BL, null, null),
                            getCategory(dao, OA, BL, 0, 10),
                            getCategory(dao, OA, BL, 10, 30),
                            getCategory(dao, OA, BL, 30, null)
                    );
            converters.add(converter);
        }
        {
            // BI Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OABI(
                            getCategory(dao, OA, BI, null, null),
                            getCategory(dao, OA, BI, 0, 10),
                            getCategory(dao, OA, BI, 10, 30),
                            getCategory(dao, OA, BI, 30, null)
                    );
            converters.add(converter);
        }

    }

    private static void prepareForLegacyOI(WeightCategoryTreatmentTopiaDao dao,
                                           Collection<WeightCategoryLogBookConverter> converters) throws TopiaException {

        {
            // BO Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OIBO(
                            getCategory(dao, OI, BO, null, null),
                            getCategory(dao, OI, BO, 0, 10),
                            getCategory(dao, OI, BO, 10, null)
                    );
            converters.add(converter);
        }
        {
            // BL Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OIBL(
                            getCategory(dao, OI, BL, null, null),
                            getCategory(dao, OI, BL, 0, 10),
                            getCategory(dao, OI, BL, 10, null)
                    );
            converters.add(converter);
        }
        {
            // BI Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OIBI(
                            getCategory(dao, OI, BI, null, null),
                            getCategory(dao, OI, BI, 0, 10),
                            getCategory(dao, OI, BI, 10, null)
                    );
            converters.add(converter);
        }
    }

    private static void prepareForLegacyOP(WeightCategoryTreatmentTopiaDao dao,
                                           Collection<WeightCategoryLogBookConverter> converters) throws TopiaException {

        {
            // BO Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OPBO(
                            getCategory(dao, OP, BO, null, null),
                            getCategory(dao, OP, BO, 0, 10),
                            getCategory(dao, OP, BO, 10, null)
                    );
            converters.add(converter);
        }
        {
            // BL Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OPBL(
                            getCategory(dao, OP, BL, null, null),
                            getCategory(dao, OP, BL, 0, 10),
                            getCategory(dao, OP, BL, 10, null)
                    );
            converters.add(converter);
        }
        {
            // BI Converter
            WeightCategoryLogBookConverter converter =
                    new WeightCategoryLogBookConverterFOR_OPBI(
                            getCategory(dao, OP, BI, null, null),
                            getCategory(dao, OP, BI, 0, 10),
                            getCategory(dao, OP, BI, 10, null)
                    );
            converters.add(converter);
        }
    }

    public WeightCategoryLogBookConverter getConverter(Ocean ocean, SchoolType schoolType) {
        WeightCategoryLogBookConverter result = null;
        for (WeightCategoryLogBookConverter converter : converters) {
            if (converter.accept(ocean, schoolType)) {
                result = converter;
                break;
            }
        }
        return result;
    }


}
