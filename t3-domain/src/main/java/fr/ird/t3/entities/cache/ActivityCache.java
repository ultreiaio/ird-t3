/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.cache;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by tchemit on 23/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ActivityCache {

    private static final Log log = LogFactory.getLog(ActivityCache.class);
    private final Map<String, Activity> cache;
    private final ActivityTopiaDao activityDAO;

    public ActivityCache(ActivityTopiaDao activityDAO) {
        this.activityDAO = activityDAO;
        this.cache = new TreeMap<>();
    }

    public Activity forId(String activityId) {
        Activity result = cache.get(activityId);
        if (result == null) {
            log.debug(String.format("Cache Activity [%d] for %s", cache.size(), activityId));
            cache.put(activityId, result = activityDAO.forTopiaIdEquals(activityId).findUnique());
            if (cache.size() % 100 == 0) {
                log.info(String.format("Cache Activity [%d]", cache.size()));
            }
        }
        return result;
    }

    public void load(Set<String> ids) {
        Set<String> result = new LinkedHashSet<>(ids);
        result.removeAll(cache.keySet());
        if (!result.isEmpty()) {
            for (Activity activity : activityDAO.forTopiaIdIn(result).findAll()) {
                String activityId = activity.getTopiaId();
                log.debug(String.format("Cache Activity [%d] for %s", cache.size(), activityId));
                cache.put(activityId, activity);
                if (cache.size() % 100 == 0) {
                    log.info(String.format("Cache Activity [%d]", cache.size()));
                }
            }
        }
    }

    public void clear() {
        log.info(String.format("Clear Cache Activity [%d]", cache.size()));
        cache.clear();
    }
}
