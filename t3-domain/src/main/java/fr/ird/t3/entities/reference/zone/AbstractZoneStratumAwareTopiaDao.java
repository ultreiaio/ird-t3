/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.List;

public class AbstractZoneStratumAwareTopiaDao<E extends ZoneStratumAware> extends GeneratedZoneStratumAwareTopiaDao<E> {

    public List<E> findAllByOceanSchoolTypeAndZoneVersion(Collection<Ocean> oceans,
                                                          SchoolType schoolType,
                                                          ZoneVersion zoneVersion) throws TopiaException {

        return forProperties(
                ZoneStratumAware.PROPERTY_SCHOOL_TYPE, schoolType,
                ZoneStratumAware.PROPERTY_VERSION_ID, zoneVersion.getId()
        ).addIn(ZoneStratumAware.PROPERTY_OCEAN, oceans).findAll();
    }

}
