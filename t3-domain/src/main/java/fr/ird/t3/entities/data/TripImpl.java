/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryImpl;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.HarbourImpl;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselImpl;
import org.nuiton.util.DateUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.n;

/**
 * Default implmentation of {@link Trip}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class TripImpl extends TripAbstract {

    private static final long serialVersionUID = 1L;

    static {
        n("t3.common.elementaryCatchTotalWeight");
        n("t3.common.elementaryLandingTotalWeight");
        n("t3.common.sampleTotalLD1Number");
        n("t3.common.sampleTotalLFNumber");
        n("t3.common.sampleTotalNumber");
    }

    private transient Boolean level2Computed;
    private transient Boolean level3Computed;

    @Override
    public boolean isSomeDataComputed() {
        return !isNoDataComputed() && !isAllDataComputed();
    }

    @Override
    public boolean isNoDataComputed() {
        return !isRf1Computed();
    }

    @Override
    public boolean isAllDataComputed() {
        return isLevel2Computed() && isLevel3Computed();
    }

    @Override
    public boolean isSamplesOnly() {
        return TripType.SAMPLEONLY == getTripType();
    }

    @Override
    public boolean isWithoutLogbook() {
        return TripType.LOGBOOKMISSING == getTripType();
    }

    @Override
    public boolean isWithLogbook() {
        return TripType.STANDARD == getTripType();
    }

    @SuppressWarnings("WeakerAccess")
    public boolean isLevel2Computed() {
        if (level2Computed == null) {
            level2Computed = false;
            if (isActivityNotEmpty()) {
                for (Activity a : getActivity()) {
                    Boolean b = a.getUseMeanStratumCompositionN2();
                    if (b != null) {
                        level2Computed = true;
                        break;
                    }
                }
            }

        }
        return level2Computed;
    }

    @SuppressWarnings("WeakerAccess")
    public boolean isLevel3Computed() {
        if (level3Computed == null) {
            level3Computed = false;
            if (isActivityNotEmpty()) {
                for (Activity a : getActivity()) {
                    Boolean b = a.getUseMeanStratumCompositionN3();
                    if (b != null) {
                        level3Computed = true;
                        break;
                    }
                }
            }
        }
        return level3Computed;
    }

    @Override
    public String getVesselLabel() {
        String result;
        if (vessel == null) {

            // no vessel
            result = "No vessel";
        } else {
            result = vessel.getCode() + " - " + vessel.getLabel1();
        }
        return result;
    }

    @Override
    public float getElementaryCatchTotalWeight(Collection<Species> species) {
        float result = 0;
        if (isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {
                result += activity.getElementaryCatchTotalWeight(species);
            }
        }
        return result;
    }

    @Override
    public float getElementaryCatchTotalWeightRf1(Collection<Species> species) {
        float result = 0;
        if (getRf1() != null && isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {

                if (activity.isElementaryCatchEmpty()) {

                    // no catches...
                    continue;
                }

                for (ElementaryCatch catchN0 : activity.getElementaryCatch()) {
                    if (species.contains(catchN0.getWeightCategoryLogBook().getSpecies())) {
                        result += catchN0.getCatchWeightRf1();
                    }
                }
            }
        }
        return result;
    }

    @Override
    public float getElementaryCatchTotalWeightRf2(Collection<Species> species) {
        float result = 0;
        if (getRf1() != null && isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {

                if (activity.isElementaryCatchEmpty()) {

                    // no catches...
                    continue;
                }

                for (ElementaryCatch catchN0 : activity.getElementaryCatch()) {
                    if (species.contains(catchN0.getWeightCategoryLogBook().getSpecies())) {
                        result += catchN0.getCatchWeightRf2();
                    }
                }
            }
        }
        return result;
    }

    @Override
    public float getLocalMarketBatchTotalWeight(Collection<Species> species) {
        float result = 0;
        if (isLocalMarketBatchNotEmpty()) {
            for (LocalMarketBatch localMarketBatch : getLocalMarketBatch()) {
                if (species.contains(localMarketBatch.getSpecies()) && localMarketBatch.getWeight() != null) {
                    result += localMarketBatch.getWeight();
                }
            }
        }
        return result;
    }

    @Override
    public float getElementaryLandingTotalWeight(Collection<Species> species) {
        float result = 0;
        if (isElementaryLandingNotEmpty()) {
            for (ElementaryLanding elementaryLanding : getElementaryLanding()) {
                if (species.contains(elementaryLanding.getWeightCategoryLanding().getSpecies())) {
                    result += elementaryLanding.getWeight();
                }
            }
        }
        return result;
    }


    @Override
    public Set<Species> getElementaryCatchSpecies() {
        Set<Species> result = new HashSet<>();
        if (isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {
                if (!activity.isElementaryCatchEmpty()) {
                    for (ElementaryCatch aCatch : activity.getElementaryCatch()) {
                        result.add(aCatch.getWeightCategoryLogBook().getSpecies());
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Set<Species> getElementaryLandingSpecies() {
        Set<Species> result = new HashSet<>();
        if (isElementaryLandingNotEmpty()) {
            for (ElementaryLanding landing : getElementaryLanding()) {
                result.add(landing.getWeightCategoryLanding().getSpecies());
            }
        }
        return result;
    }

    @Override
    public Set<Ocean> getAllOceans() {
        Set<Ocean> result = new HashSet<>();
        if (isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {
                result.add(activity.getOcean());
            }
        }
        return result;
    }

    @Override
    public void applyRf1(Float rf1, Collection<Species> species) {

        setRf1(rf1);

        if (isActivityEmpty()) {
            // no activity, so nothing to do!
            return;
        }

        if (rf1 == null) {
            // let's apply a null for all catches
            for (Activity activity : getActivity()) {
                if (activity.isElementaryCatchNotEmpty()) {
                    for (ElementaryCatch aCatch : activity.getElementaryCatch()) {
                        aCatch.setCatchWeightRf1(null);
                    }
                }
            }
            return;
        }

        if (rf1 == 1.0f) {
            // special case, push back the original value for every catch without any filter on species
            for (Activity activity : getActivity()) {
                if (activity.isElementaryCatchNotEmpty()) {
                    for (ElementaryCatch aCatch : activity.getElementaryCatch()) {
                        float value = aCatch.getCatchWeight();
                        aCatch.setCatchWeightRf1(value);
                    }
                }
            }
            return;
        }

        // complete case where we distinguish species for the one given, use the rf1 gven, for the others use a rf1=1
        for (Activity activity : getActivity()) {
            if (activity.isElementaryCatchNotEmpty()) {
                for (ElementaryCatch aCatch : activity.getElementaryCatch()) {
                    float value = aCatch.getCatchWeight();
                    if (species.contains(aCatch.getWeightCategoryLogBook().getSpecies())) {
                        value *= rf1;
                    }
                    aCatch.setCatchWeightRf1(value);
                }
            }
        }
    }

    @Override
    public void applyRf2(Float rf2) {
        setRf2(rf2);
        boolean withRF1 = getRf1() != null;
        if (isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {
                if (activity.isElementaryCatchNotEmpty()) {
                    for (ElementaryCatch aCatch : activity.getElementaryCatch()) {
                        Float catchWeight = withRF1 ? aCatch.getCatchWeightRf1() : aCatch.getCatchWeight();
                        float value = catchWeight * rf2;
                        aCatch.setCatchWeightRf2(value);
                    }
                }
            }
        }
    }

    @Override
    public long getSampleTotalLD1Number() {
        long result = 0;
        if (!isSampleEmpty()) {
            for (Sample aSample : getSample()) {
                if (aSample.isSampleSpeciesNotEmpty()) {
                    for (SampleSpecies sampleSpecies : aSample.getSampleSpecies()) {
                        if (sampleSpecies.isLd1LengthClass()) {
                            result += sampleSpecies.getTotalSampleSpeciesFrequencyNumber();
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public long getSampleTotalLFNumber() {
        long result = 0;
        if (!isSampleEmpty()) {
            for (Sample aSample : getSample()) {
                if (aSample.isSampleSpeciesNotEmpty()) {
                    for (SampleSpecies sampleSpecies : aSample.getSampleSpecies()) {
                        if (!sampleSpecies.isLd1LengthClass()) {
                            result += sampleSpecies.getTotalSampleSpeciesFrequencyNumber();
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public TripDTO toDTO() {
        TripDTO dto = new TripDTO();
        dto.setId(getTopiaId());
        dto.setDepartureDate(getDepartureDate());
        Harbour h;
        h = new HarbourImpl();
        h.setTopiaId(getDepartureHarbour().getTopiaId());
        h.setCode(getDepartureHarbour().getCode());
        h.setLabel1(getDepartureHarbour().getLabel1());
        dto.setDepartureHarbour(h);

        dto.setLandingDate(getLandingDate());
        h = new HarbourImpl();
        h.setTopiaId(getLandingHarbour().getTopiaId());
        h.setCode(getLandingHarbour().getCode());
        h.setLabel1(getLandingHarbour().getLabel1());
        dto.setLandingHarbour(h);

        Vessel v = new VesselImpl();
        v.setTopiaId(getVessel().getTopiaId());
        v.setCode(getVessel().getCode());
        v.setLabel1(getVessel().getLabel1());

        Country fleet = new CountryImpl();
        v.setFleetCountry(fleet);
        fleet.setTopiaId(getVessel().getFleetCountry().getTopiaId());
        fleet.setCode(getVessel().getFleetCountry().getCode());
        fleet.setLabel1(getVessel().getFleetCountry().getLabel1());

        Country flag = new CountryImpl();
        v.setFlagCountry(flag);
        flag.setTopiaId(getVessel().getFlagCountry().getTopiaId());
        flag.setCode(getVessel().getFlagCountry().getCode());
        flag.setLabel1(getVessel().getFlagCountry().getLabel1());

        dto.setVessel(v);
        return dto;
    }

    @Override
    public void deleteComputedData() {
        ComputedDataHelper.deleteComputedData(this);
    }

    @Override
    public void deleteComputedDataLevel0() {

        // level 0 flags
        setRf1Computed(false);
        setRf2Computed(false);
        setCatchesWeightCategorieConverted(false);
        setSetDurationAndPositiveCountComputed(false);
        setEffortComputed(false);
        setWellPlanWeightCategoriesComputed(false);

        // level 0 data
        setCompletionStatus(null);
        setRf1(null);
        setRf2(null);
//        setComputedFishingTimeN0(null);
//        setComputedSearchTimeN0(null);
//        setComputedTimeAtSeaN0(null);

        ComputedDataHelper.deleteComputedDataLevel0(getRoute());
        ComputedDataHelper.deleteComputedDataLevel0(getSample());
        ComputedDataHelper.deleteComputedDataLevel0(getWell());
    }

    @Override
    public void deleteComputedDataLevel1() {
        setExtrapolateSampleCountedAndMeasured(false);
        setStandardizeSampleMeasures(false);
        setComputeWeightOfCategoriesForSet(false);
        setRedistributeSampleNumberToSet(false);
        setExtrapolateSampleWeightToSet(false);
        setConvertSetSpeciesFrequencyToWeight(false);
        setConvertSampleSetSpeciesFrequencyToWeight(false);

        ComputedDataHelper.deleteComputedDataLevel1(getRoute());
        ComputedDataHelper.deleteComputedDataLevel1(getSample());
        ComputedDataHelper.deleteComputedDataLevel1(getWell());
    }

    @Override
    public void deleteComputedDataLevel2() {
        ComputedDataHelper.deleteComputedDataLevel2(getRoute());
        ComputedDataHelper.deleteComputedDataLevel2(getSample());
        ComputedDataHelper.deleteComputedDataLevel2(getWell());
    }

    @Override
    public void deleteComputedDataLevel3() {
        ComputedDataHelper.deleteComputedDataLevel3(getRoute());
        ComputedDataHelper.deleteComputedDataLevel3(getSample());
        ComputedDataHelper.deleteComputedDataLevel3(getWell());
    }

    @Override
    public int getComputedTimeAtSeaN0() {
        return 24 * sizeRoute();
    }

    @Override
    public int sizeActivity() {
        return (int) activityStream().count();
    }

    @Override
    public boolean isActivityEmpty() {
        return isRouteEmpty() || activityStream().count() == 0;
    }

    @Override
    public boolean isActivityNotEmpty() {
        return isRouteNotEmpty() && activityStream().count() > 0;
    }

    @Override
    public List<Activity> getActivity() {
        return activityStream().collect(Collectors.toList());
    }

    @Override
    public void setActivity(List<Activity> activities) {
        clearRoute();
        for (Activity activity : activities) {
            addActivity(activity);
        }
    }

    @Override
    public Activity getActivityByTopiaId(String topiaId) {
        return activityStream().filter(a -> topiaId.equals(a.getTopiaId())).findFirst().orElse(null);
    }

    @Override
    public Collection<String> getActivityTopiaIds() {
        return activityStream().map(Activity::getTopiaId).collect(Collectors.toList());
    }

    @Override
    public void addActivity(Activity activity) {
        Date date = DateUtil.getDay(activity.getDate());
        Optional<Route> optionalRoute = routeStream().filter(r -> Objects.equals(r.getDate(), date)).findFirst();
        Route route;
        if (optionalRoute.isPresent()) {
            route = optionalRoute.get();
        } else {
            route = new RouteImpl();
            route.setDate(date);
            addRoute(route);
        }
        route.addActivity(activity);
    }

    @Override
    public List<Route> getRoute() {
        if (super.getRoute() == null) {
            setRoute(new LinkedList<>());
        }
        return new LinkedList<>(route);
    }

    private Stream<Route> routeStream() {
        return getRoute().stream().sorted(Comparator.comparing(Route::getDate));
    }

    private Stream<Activity> activityStream() {
        return routeStream().flatMap(r -> r.getActivity().stream());
    }
}
