/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanImpl;
import fr.ird.t3.entities.reference.OceanTopiaDao;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.entities.type.T3Point;
import fr.ird.t3.entities.type.T3PointImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Client implementation of the Trip dao.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AbstractTripTopiaDao<E extends Trip> extends GeneratedTripTopiaDao<E> {

    public static final Ocean EMPTY_OCEAN = new OceanImpl();
    private static final int TRIP_WITHOUT_LOGBOOK = 0;
    private static final int TRIP_WITH_LOGBOOK = 1;
    private static final Log log = LogFactory.getLog(AbstractTripTopiaDao.class);

    static {
        EMPTY_OCEAN.setTopiaId("fr.ird.t3.entities.reference.Ocean#EMPTY#EMPTY");
    }

    /**
     * Check that a standard trip ({@link TripType#STANDARD}) is consistent with {@code logBookAvailability} flag.
     *
     * @param trip trip to check
     * @param pKey primary key of this trip
     */
    public static void checkAndSetStandardTripType(Trip trip, Object[] pKey) {
        int logBookAvailability = trip.getLogbookAvailability();
        switch (logBookAvailability) {
            case TRIP_WITHOUT_LOGBOOK:
                if (trip.isActivityNotEmpty()) {
                    trip.setTripType(TripType.STANDARD);
                    throw new IllegalStateException(String.format("Trip [%s] has some logBook but logBookAvailability says the opposite, please fix this trip.", Arrays.toString(pKey)));
                }
                trip.setTripType(TripType.LOGBOOKMISSING);
                break;
            case TRIP_WITH_LOGBOOK:
                if (trip.isActivityEmpty()) {
                    trip.setTripType(TripType.LOGBOOKMISSING);
                    throw new IllegalStateException(String.format("Trip [%s] has no logBook but logBookAvailability says the opposite, please fix this trip.", Arrays.toString(pKey)));
                }
                trip.setTripType(TripType.STANDARD);
                break;
        }
    }

    /**
     * Check that a standard trip ({@link TripType#LOGBOOKMISSING}) is consistent with {@code logBookAvailability} flag.
     *
     * @param trip trip to check
     * @param pKey primary key of this trip
     */
    public static void checkAndSetLogbookMissingTripType(Trip trip, Object[] pKey) {
        int logBookAvailability = trip.getLogbookAvailability();
        if (trip.getTripType() == TripType.LOGBOOKMISSING) {
            if (TRIP_WITH_LOGBOOK == logBookAvailability) {
                throw new IllegalStateException(String.format("Trip %s has no logBook but logBookAvailability says the opposite, please fix this trip.", Arrays.toString(pKey)));
            }
        } else {
            checkAndSetStandardTripType(trip, pKey);
        }
    }

    public static String getSamplesOnlyFilter(String prefix, Boolean samplesOnly) {
        if (samplesOnly == null) {
            return "";
        }
        if (samplesOnly) {
            return " " + prefix + " t.tripType = 'SAMPLEONLY'";
        }
        return " " + prefix + " t.tripType != 'SAMPLEONLY'";
    }

    public static <E extends Trip> Collection<E> getAllTripsWithNoDataComputed(Collection<E> trips) {
        return trips.stream().filter(Trip::isNoDataComputed).collect(Collectors.toList());
    }

    public static <E extends Trip> Collection<E> getAllTripsWithAllDataComputed(Collection<E> trips) {
        return trips.stream().filter(Trip::isAllDataComputed).collect(Collectors.toList());
    }

    public static <E extends Trip> Collection<E> getAllTripsWithSomeDataComputed(Collection<E> trips) {
        return trips.stream().filter(Trip::isSomeDataComputed).collect(Collectors.toList());
    }

//    public static <E extends Trip> Set<Integer> getAllTripsDepartureYear(Collection<E> trips) {
//        return trips.stream().map(T3Functions.TRIP_TO_DEPARTURE_YEAR).collect(Collectors.toSet());
//    }

    public static <E extends Trip> List<TripDTO> toDTO(List<E> t3Trips) {
        return t3Trips.stream().map(Trip::toDTO).collect(Collectors.toList());
    }

    public static Multimap<T3Date, CompleteTrip> splitTripsByMonth(List<CompleteTrip> trips) {
        Multimap<T3Date, CompleteTrip> result = LinkedHashMultimap.create();
        for (CompleteTrip trip : trips) {

            // get trip landing month
            T3Date tripMonth = T3Functions.TRIP_TO_LANDING_DATE.apply(trip.getLandingTrip());

            result.put(tripMonth, trip);
        }
        return result;
    }

    public static void retainsVessels(Collection<Trip> trips, Collection<Vessel> vessels) {
        Predicate<Trip> predicate = T3Predicates.tripUsingVessel(vessels).negate();
        trips.removeIf(predicate);
    }

    public static void retainsOceans(Collection<Trip> trips, Collection<Ocean> oceans) {
        Predicate<Trip> predicate = T3Predicates.tripUsingOcean(oceans).negate();
        trips.removeIf(predicate);
    }

    public static void retainsLandingHarbours(Collection<Trip> trips, Collection<Harbour> harbours) {
        Predicate<Trip> predicate = T3Predicates.tripUsingHarbour(harbours).negate();
        trips.removeIf(predicate);
    }

    public static void retainsDepartureYears(Collection<Trip> trips, Collection<Integer> years) {
        Predicate<Trip> predicate = T3Predicates.tripUsingDepartureYear(years).negate();
        trips.removeIf(predicate);
    }

    public static boolean isTripsAllWithLogBook(Iterable<Trip> trips) {
        return StreamSupport.stream(trips.spliterator(), false).allMatch(Trip::isWithLogbook);
    }

    /**
     * Sort the given trips given their landing date.
     *
     * @param list the trips to sort
     */
    public static <E extends Trip> void sortTrips(List<E> list) {
        list.sort(new TripComparator<Trip>());
    }

    /**
     * Given some trips, group them by their owing vessel.
     *
     * @param trips the trips to scan
     * @return the trips grouped by their owing vessel
     */
    public static ListMultimap<Vessel, Trip> groupByVessel(List<Trip> trips) {
        return Multimaps.index(trips, Trip::getVessel);
    }

    /**
     * Gets from the given trips their activities bound date.
     *
     * @param trips the trips to scan
     * @return the bound pair of min and max date of activities of the given trips
     */
    public static MutablePair<Date, Date> getActivityBoundDate(Collection<Trip> trips) {
        Date firstDate = null;
        Date lastDate = null;
        for (Trip trip : trips) {
            if (trip.isActivityNotEmpty()) {
                for (Activity activity : trip.getActivity()) {
                    Date activityDate = activity.getDate();
                    if (firstDate == null || activityDate.before(firstDate)) {
                        firstDate = activityDate;
                    }
                    if (lastDate == null || activityDate.after(lastDate)) {
                        lastDate = activityDate;
                    }
                }
            }
        }
        return MutablePair.of(firstDate, lastDate);
    }

    public List<E> findAllByIds(List<String> allTripIds) {
        List<E> result;
        if (CollectionUtils.isEmpty(allTripIds)) {
            result = new ArrayList<>();
        } else {
            String hql = "SELECT t FROM TripImpl t WHERE t.id IN :ids";
            result = findAll(hql, ImmutableMap.of("ids", allTripIds));
            sortTrips(result);
        }
        return result;
    }

    public List<String> findAllIdsByOcean(Ocean ocean) {
        String hql;
        ImmutableMap.Builder<String, Object> paramsBuilder = ImmutableMap.builder();
        if (ocean == null) {
            // get all trips with no activity
            hql = "SELECT t.id FROM TripImpl as t WHERE size(t.route) = 0";
        } else {
            hql = "SELECT DISTINCT(a.route.trip.id) FROM ActivityImpl a WHERE a.ocean = :ocean";
            paramsBuilder.put("ocean", ocean);
        }
        return findAll(hql, paramsBuilder.build());
    }

    public Multimap<Ocean, String> findAllIdsByOcean() {
        Set<Ocean> oceans = topiaDaoSupplier.getDao(Ocean.class, OceanTopiaDao.class).findAllUsedInActivity();
        Multimap<Ocean, String> result = ArrayListMultimap.create();
        for (Ocean ocean : oceans) {
            List<String> allByOcean = findAllIdsByOcean(ocean);
            if (CollectionUtils.isNotEmpty(allByOcean)) {
                result.putAll(ocean, allByOcean);
            }
        }
        // add trips with no ocean (http://forge.codelutin.com/issues/1134)
        List<String> tripsWithNoOcean = findAllIdsByOcean(null);
        if (CollectionUtils.isNotEmpty(tripsWithNoOcean)) {
            result.putAll(EMPTY_OCEAN, tripsWithNoOcean);
        }
        return result;
    }

    @SuppressWarnings("unused")
    public T3Point getBarycenterForActivitiesOfADay(E trip, Date day) {
        TopiaSqlQuery<T3Point> query = new GetT3PointQuery<>(trip, day);
        return topiaSqlSupport.findSingleResult(query);
    }

    /**
     * Obtains all trips usable for a level 1 configuration, says:
     * <ul>
     * <li>Only for senneur vessel simple type</li>
     * <li>samplesOnly = true or completionStatus >0</li>
     * </ul>
     *
     * @return list of all trips usable in level 1
     * @throws TopiaException if could not query db
     * @since 1.2
     */
    @SuppressWarnings("unused")
    public List<E> findAllForLevel1() {
        String hql = "SELECT t FROM TripImpl t WHERE t.vessel.vesselType.vesselSimpleType.code = 1";
        List<E> result = findAll(hql);
        sortTrips(result);
        return result;
    }

    public List<Integer> findAllYearsUsedInTrip() {
        T3Date minDate = getFirstLandingDate(null);
        T3Date maxDate = getLastLandingDate(null);
        int minYear = minDate.getYear();
        int maxYear = maxDate.getYear();
        Set<Integer> years = new HashSet<>();
        for (int i = minYear; i <= maxYear; i++) {
            years.add(i);
        }
        List<Integer> result = new ArrayList<>(years);
        Collections.sort(result);
        return result;
    }

    /**
     * Obtain the list of trips between the inclusive landing dates.
     *
     * @param beginDate              the min bound of landing date
     * @param endDate                the max bound of landing date
     * @param samplesOnly            flag to use {@code Trip.samplesOnly} value if not null
     * @param rejectCompletionStatus flag to use {@code Trip.rejectComputationStatus} value if not null
     * @return the list of trips with landing date between given bound.
     * @throws TopiaException if any pb while loading data
     */
    public List<E> findAllBetweenLandingDate(T3Date beginDate,
                                             T3Date endDate,
                                             Boolean samplesOnly,
                                             boolean rejectCompletionStatus) {
        String hql = "SELECT t FROM TripImpl t WHERE t.landingDate BETWEEN :beginDate AND :endDate";
        ImmutableMap.Builder<String, Object> paramsBuilder = ImmutableMap.<String, Object>builder()
                .put("beginDate", beginDate.toBeginSqlDate())
                .put("endDate", endDate.toEndSqlDate());
        hql += TripTopiaDao.getSamplesOnlyFilter("AND", samplesOnly);
        if (rejectCompletionStatus) {
            // does not accept trip with completionStatus = 0
            hql += " AND t.completionStatus != 0";
        }
        return findAll(hql, paramsBuilder.build());
    }

    public T3Date getFirstLandingDate(Boolean samplesOnly) {

        String samplesOnlyFilter = TripTopiaDao.getSamplesOnlyFilter("WHERE", samplesOnly);
        String hql = " SELECT min(t.landingDate) FROM TripImpl t" + samplesOnlyFilter;
        Date date = findAnyOrNull(hql);
        T3Date result = null;
        if (date != null) {
            result = T3Date.newDate(date);
        }
        return result;
    }

    public T3Date getLastLandingDate(Boolean samplesOnly) {
        String samplesOnlyFilter = TripTopiaDao.getSamplesOnlyFilter("WHERE", samplesOnly);
        String hql = " SELECT max(t.landingDate) FROM TripImpl t" + samplesOnlyFilter;
        Date date = findAnyOrNull(hql);
        T3Date result = null;
        if (date != null) {
            result = T3Date.newDate(date);
        }
        return result;
    }

//    @SuppressWarnings("unused")
//    public List<List<E>> splitCompleteTrip(List<E> trips) {
//        List<List<E>> result = new ArrayList<>();
//        List<E> completeTrip = new ArrayList<>();
//        for (E trip : trips) {
//
//            // add this trip to in building complete trip list
//            completeTrip.add(trip);
//
//            // compute if this trip finish a complete trip
//            boolean tripWasComplete =
//                    T3Predicates.TRIP_ENDS_A_COMPLETE_TRIP.test(trip);
//
//            if (tripWasComplete) {
//
//                // found a complete trip
//                if (log.isInfoEnabled()) {
//                    log.info(String.format("Found a complete trip with %d trip(s).", completeTrip.size()));
//                }
//                if (log.isDebugEnabled()) {
//                    for (Trip t : completeTrip) {
//                        log.debug(String.format("complete trip part - %s", t.getLandingDate()));
//                    }
//                }
//
//                // add this complete trip
//                result.add(completeTrip);
//
//                // reset new complete trip list
//                completeTrip = new ArrayList<>();
//            }
//        }
//        return result;
//    }

    public List<CompleteTrip> toCompleteTrip(List<E> trips) {
        List<CompleteTrip> result = new ArrayList<>();
        List<Trip> completeTrip = new ArrayList<>();
        for (E trip : trips) {

            // add this trip to in building complete trip list
            completeTrip.add(trip);

            // compute if this trip finish a complete trip
            boolean tripWasComplete = T3Predicates.TRIP_ENDS_A_COMPLETE_TRIP.test(trip);

            if (tripWasComplete) {

                // found a complete trip
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Found a complete trip with %d trip(s).", completeTrip.size()));
                    for (Trip t : completeTrip) {
                        log.debug(String.format("complete trip part - %s", t.getLandingDate()));
                    }
                }

                // add this complete trip
                result.add(new CompleteTrip(completeTrip));

                // reset new complete trip list
                completeTrip = new ArrayList<>();
            }
        }
        return result;
    }

    private static class TripComparator<E extends Trip> implements Comparator<E>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(E o1, E o2) {
            return o1.getLandingDate().compareTo(o2.getLandingDate());
        }

    }

    private static class GetT3PointQuery<E extends Trip> extends TopiaSqlQuery<T3Point> {
        private final E trip;

        private final Date day;

        GetT3PointQuery(E trip, Date day) {
            this.trip = trip;
            this.day = day;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            @SuppressWarnings("SqlResolve")
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT ST_X(record.point), ST_Y(record.point) FROM " +
                            "(" +
                            "   SELECT ST_Centroid(ST_Collect(a.the_geom)) AS point" +
                            "   FROM activity a " +
                            "   WHERE a.trip = ? AND a.date::DATE = ?" +
                            ") AS record;");
            ps.setString(1, trip.getTopiaId());
            ps.setDate(2, new java.sql.Date(day.getTime()));
            return ps;
        }

        @Override
        public T3Point prepareResult(ResultSet set) throws SQLException {
            float x = set.getFloat(1);
            float y = set.getFloat(2);
            return new T3PointImpl(x, y);
        }
    }
}
