/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion.legacy;

import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverter;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Abstract log book convertor.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractWeightCategoryLogBookConverter implements WeightCategoryLogBookConverter {

    protected final int oceanCode;
    private final int schoolTypeCode;

    protected Map<WeightCategoryTreatment, Map<Integer, Float>> distributions;

    protected final WeightCategoryTreatment minus10Category;

    protected final WeightCategoryTreatment unknownCategory;

    protected AbstractWeightCategoryLogBookConverter(int oceanCode,
                                                     int schoolTypeCode,
                                                     WeightCategoryTreatment minus10Category,
                                                     WeightCategoryTreatment unknownCategory) {
        this.oceanCode = oceanCode;
        this.schoolTypeCode = schoolTypeCode;
        this.minus10Category = minus10Category;
        this.unknownCategory = unknownCategory;
    }

    @Override
    public final boolean accept(Ocean ocean, SchoolType schoolType) {
        return ocean.getCode() == oceanCode && schoolType.getCode() == schoolTypeCode;
    }

    @Override
    public String toString() {
        return super.toString() + ", ocean  " + oceanCode + ", schoolType " + schoolTypeCode;
    }

    /**
     * Gets the distributions used while conversion for each weight category of
     * t3.
     * <p/>
     * <strong>Note: </string>
     *
     * @return the distributions
     */
//    @Override
    public Map<WeightCategoryTreatment, Map<Integer, Float>> getDistributions() {
        if (distributions == null) {
            distributions = buildDistributions();
        }
        return distributions;
    }

    //    @Override
    public Map<String, Map<Integer, Float>> getDistributionsByIds() {
        Map<String, Map<Integer, Float>> distributionsToSave = new LinkedHashMap<>();
        for (Map.Entry<WeightCategoryTreatment, Map<Integer, Float>> e : getDistributions().entrySet()) {
            distributionsToSave.put(e.getKey().getTopiaId(), e.getValue());
        }
        return distributionsToSave;
    }

//    @Override
//    public Map<Integer, Float> getDistributions(WeightCategoryTreatment treatment) {
//        return getDistributions().get(treatment);
//    }

    //    @Override
    public final WeightCategoryTreatment getMinus10Category() {
        return minus10Category;
    }

    //    @Override
    public final WeightCategoryTreatment getUnkwownCategory() {
        return unknownCategory;
    }

    protected abstract Map<WeightCategoryTreatment, Map<Integer, Float>> buildDistributions();

    protected final Map<WeightCategoryTreatment, Float> defaultDistributeForSpecie1or3or4(
            Collection<ElementaryCatch> catches,
            Map<WeightCategoryTreatment, Map<Integer, Float>> distributions,
            float unknownWeight) {

        Map<WeightCategoryTreatment, Float> unknownResult = new HashMap<>();

        float totalWeight = 0f;

        for (Map.Entry<WeightCategoryTreatment, Map<Integer, Float>> e : distributions.entrySet()) {

            WeightCategoryTreatment category = e.getKey();

            Map<Integer, Float> distribution = e.getValue();

            float weight = WeightCategoryLogBookConverter.distribute(distribution, catches);

            unknownResult.put(category, weight);

            // sum total weight
            totalWeight += weight;
        }

        Map<WeightCategoryTreatment, Float> result;

        if (unknownWeight == 0) {

            // no unknown weight to redistribute, can keep current result
            result = unknownResult;
        } else {

            // there is some unknown weight to redistribute
            result = new HashMap<>();

            if (totalWeight == 0) {

                // distribute unknown weight in all weight categories
                float weight = unknownWeight / unknownResult.keySet().size();

                for (WeightCategoryTreatment e : unknownResult.keySet()) {
                    // store new weight
                    result.put(e, weight);
                }
            } else {
                for (Map.Entry<WeightCategoryTreatment, Float> e : unknownResult.entrySet()) {
                    WeightCategoryTreatment weightCategory = e.getKey();
                    // weight (with no unknown category)
                    float weight = e.getValue();
                    // rate to distribute
                    float rate = weight / totalWeight;
                    // weight redistributed
                    float unknownWeightToAdd = unknownWeight * rate;
                    // store new weight
                    result.put(weightCategory, weight + unknownWeightToAdd);
                }
            }
        }
        return result;
    }

    protected final Map<WeightCategoryTreatment, Float> defaultDistributeForSpecie2(float totalWeight) {

        Map<WeightCategoryTreatment, Float> result = new HashMap<>();

        // for species SKJ (code 2) : all goes to -10 kg category
        result.put(getMinus10Category(), totalWeight);
        return result;
    }

    protected final Map<WeightCategoryTreatment, Float> defaultDistributeForOtherSpecie(float unknownWeight) {

        Map<WeightCategoryTreatment, Float> result = new HashMap<>();

        // only keep unknown category weight
        result.put(getUnkwownCategory(), unknownWeight);
        return result;
    }
}
