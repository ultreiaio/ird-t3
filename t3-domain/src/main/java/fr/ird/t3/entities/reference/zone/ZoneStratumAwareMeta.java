/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import org.nuiton.topia.persistence.TopiaDaoSupplier;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * To define a type of zone usable in level 2 and 3.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public interface ZoneStratumAwareMeta extends Serializable, Idable {

    /**
     * Obtain all zones for the given {@code schoolType}, {@code ocean} and {@code zoneVersion}.
     *
     * @param oceans      the filtered oceans
     * @param schoolType  the filtered school type
     * @param zoneVersion the filtered zone version
     * @param tx          the database transaction to load data
     * @return the universe of zones given each school type
     */
    List<ZoneStratumAware> getZones(Collection<Ocean> oceans, SchoolType schoolType, ZoneVersion zoneVersion, TopiaDaoSupplier tx);

    /**
     * Obtains the {@link ZoneVersion} given his {@code zoneVersionId}.
     *
     * @param zoneVersionId the id of the zone version.
     * @param tx            the database transaction to load data
     * @return the zone version
     */
    ZoneVersion getZoneVersion(String zoneVersionId, TopiaDaoSupplier tx);

    /**
     * Obtains all zone versions for this type of zone.
     *
     * @param tx the database transaction to load data
     * @return all zone versions found
     */
    List<ZoneVersion> getAllZoneVersions(TopiaDaoSupplier tx);

    /**
     * Obtains the type of zone, this class reflects the persistent entity.
     *
     * @return the type of the zone
     */
    Class<? extends ZoneStratumAware> getType();

    /**
     * Obtains the name of persistent table.
     *
     * @return the name of the persistent table
     */
    String getTableName();

    /**
     * Obtains the i18n libelle key for this type of zone.
     *
     * @return the i18n libelle key of this type of zone
     */
    String getI18nKey();
}
