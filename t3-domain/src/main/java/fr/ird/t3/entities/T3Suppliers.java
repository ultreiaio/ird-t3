/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.decorator.Decorator;

import java.util.function.Supplier;

/**
 * Some usefull suppliers.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class T3Suppliers {

    public static Supplier<String> newActivityDecorateSupplier(T3ServiceContext serviceContext, Activity activity, String text) {
        return () -> {
            DecoratorService decoratorService = serviceContext.newService(DecoratorService.class);
            Decorator<Trip> tripDecorator = decoratorService.getDecorator(serviceContext.getLocale(), Trip.class, DecoratorService.WITH_ID);
            Decorator<Activity> activityDecorator = decoratorService.getDecorator(serviceContext.getLocale(), Activity.class, DecoratorService.WITH_ID);
            String tStr = tripDecorator.toString(activity.getTrip());
            String aStr = activityDecorator.toString(activity);
            return String.format(text, tStr, aStr);
        };
    }
}
