/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.cache;

import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionTopiaDao;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategory;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.type.T3Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Deal a cache of {@link LengthWeightConversion}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class LengthWeightConversionCache {

    private static final Log log = LogFactory.getLog(LengthWeightConversionCache.class);

    private final LengthWeightConversionTopiaDao dao;
    private final Map<String, LengthWeightConversion> lengthWeightConversions;
    private final Map<String, Integer> lengthClassMap;

    public LengthWeightConversionCache(LengthWeightConversionTopiaDao dao) {
        this.dao = dao;
        this.lengthWeightConversions = new TreeMap<>();
        this.lengthClassMap = new TreeMap<>();
    }

    /**
     * Get the converter for the given parameters from the cache.
     * <p/>
     * If not found, then load it from db.
     *
     * @param species species to use
     * @param ocean   ocean to use
     * @param sex     sex to use
     * @param date    min date to use
     * @return converter found
     */
    public LengthWeightConversion getConversions(Species species, Ocean ocean, int sex, Date date) {
        String key = species.getCode() + "-" + ocean.getCode() + "-" + sex + "-" + T3Date.newDate(date);
        LengthWeightConversion result = lengthWeightConversions.get(key);
        if (result == null && !lengthWeightConversions.containsKey(key)) {
            // load it from db
            result = dao.findLengthWeightConversion(species, ocean, 0, date);
            // store it once for all in cache
            lengthWeightConversions.put(key, result);
            log.info(String.format("Cache lengthWeightConversion [%d] for %s", lengthWeightConversions.size(), key));
        }
        return result;
    }

    public <W extends WeightCategory> Map<Integer, W> getWeightCategoriesDistribution(LengthWeightConversion conversion,
                                                                                      List<W> weightCategories,
                                                                                      List<Integer> lengthClasses) {
        Map<Integer, W> result = new TreeMap<>();
        W currentWeightCategory = null;
        int currentMaxLengthClass = -1;
        Iterator<W> itr = weightCategories.iterator();
        for (Integer lengthClass : lengthClasses) {
            if (lengthClass > currentMaxLengthClass) {
                // need another weightCategory
                currentWeightCategory = null;
            }
            if (currentWeightCategory == null) {
                // get the correct weight category
                currentWeightCategory = getNextCategory(conversion, lengthClass, itr);
                currentMaxLengthClass = getSpecieHighestLengthClass(conversion, currentWeightCategory.getMax());
            }
            result.put(lengthClass, currentWeightCategory);
        }
        return result;
    }

    public int getSpecieHighestLengthClass(LengthWeightConversion conversion, Integer max) {
        String key = conversion.getTopiaId() + "-" + max/*weightCategory.getTopiaId()*/;
        Integer result = lengthClassMap.get(key);
        if (result == null) {
            // load it once for all
            result = conversion.getSpecieHighestLengthClass(max /*weightCategory.getMax()*/);
            // store it once for all
            lengthClassMap.put(key, result);
            log.info(String.format("Cache lengthClassMap [%d] for %s", lengthClassMap.size(), key));
        }
        return result;
    }

    private <W extends WeightCategory> W getNextCategory(LengthWeightConversion conversion, int lengthClass, Iterator<W> itr) {
        W result = null;
        if (itr.hasNext()) {
            result = itr.next();
            if (result.getOldCategoryCode() == 9) {
                // does not accept undefined categories
                result = getNextCategory(conversion, lengthClass, itr);
            } else {
                // get max lengthClass
                int nextLengthClass = getSpecieHighestLengthClass(conversion, result.getMax());
                if (nextLengthClass < lengthClass) {
                    // try with next category, this one is too small
                    result = getNextCategory(conversion, lengthClass, itr);
                }
            }
        }
        return result;
    }

    public List<Integer> getLengthClasses(LengthWeightConversion conversion, WeightCategoryTreatment weightCategoryTreatment, List<Integer> lengthClasses) {
        Integer min = weightCategoryTreatment.getMin();
        Integer minLengthClass = null;
        if (min != null) {
            minLengthClass = getSpecieHighestLengthClass(conversion, min);
        }
        Integer max = weightCategoryTreatment.getMax();
        Integer maxLengthClass = null;
        if (max != null) {
            maxLengthClass = getSpecieHighestLengthClass(conversion, max);
        }
        List<Integer> result = new LinkedList<>();
        for (Integer lengthClass : lengthClasses) {
            if (minLengthClass != null && lengthClass < minLengthClass || maxLengthClass != null && lengthClass > maxLengthClass) {
                continue;
            }
            result.add(lengthClass);
        }
        return result;
    }
}
