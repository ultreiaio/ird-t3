package fr.ird.t3.entities;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.user.JdbcConfiguration;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.HibernateAvailableSettings;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by tchemit on 10/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3TopiaApplicationContextBuilder {

    private static final String T3_TOPIA_CONFIG_PROPERTIES = "/t3-topia-config.properties";
    private static final String T3_TOPIA_MIGRATION_PROPERTIES = "/t3-topia-migration.properties";

    private final Path dbPath;
    private final JdbcConfiguration jdbcConfiguration;
    private boolean addMigration;
    private boolean deleteDbFile;
    private boolean checkConnexion;
    private String h2ConnectionUrlExtraOptions;
    private Version referentialVersion;
    private Path dbFile;
    private boolean loadReferential;
    private Path backupFile;

    private T3TopiaApplicationContextBuilder(Path dbPath) {
        this.dbPath = dbPath;
        this.jdbcConfiguration = null;
    }

    private T3TopiaApplicationContextBuilder(JdbcConfiguration jdbcConfiguration) {
        this.dbPath = null;
        this.jdbcConfiguration = jdbcConfiguration;
    }

    public static T3TopiaApplicationContextBuilder forH2(Path dbPath) {
        return new T3TopiaApplicationContextBuilder(Objects.requireNonNull(dbPath));
    }

    public static T3TopiaApplicationContextBuilder forH2Referential(Path dbPath, Version t3DataVersion) {
        T3TopiaApplicationContextBuilder builder = forH2(dbPath);
        if (t3DataVersion != null) {
            builder.loadReferential(t3DataVersion);
        }
        return builder;
    }

    public static T3TopiaApplicationContextBuilder forPgTest() {
        JdbcConfiguration jdbcConfiguration = new JdbcConfiguration();
        jdbcConfiguration.setUrl("jdbc:postgresql:t3");
        jdbcConfiguration.setLogin("t3-admin");
        jdbcConfiguration.setPassword("a");
        return forPg(jdbcConfiguration);
    }

    public static T3TopiaApplicationContextBuilder forPg(JdbcConfiguration jdbcConfiguration) {
        return new T3TopiaApplicationContextBuilder(Objects.requireNonNull(jdbcConfiguration));
    }

    public T3TopiaApplicationContextBuilder loadReferential(Version referentialVersion) {
        this.referentialVersion = Objects.requireNonNull(referentialVersion);
        this.loadReferential = true;
        return this;
    }

    public T3TopiaApplicationContextBuilder deleteDbFile() {
        checkIsH2("deleteDbFile");
        this.deleteDbFile = true;
        return this;
    }

    public T3TopiaApplicationContextBuilder addMigration() {
        this.addMigration = true;
        return this;
    }

    public T3TopiaApplicationContextBuilder checkConnexion() {
        checkIsPg("checkConnexion");
        this.checkConnexion = true;
        return this;
    }

    public T3TopiaApplicationContextBuilder h2ConnectionUrlExtraOptions(String h2ConnectionUrlExtraOptions) {
        checkIsH2("h2ConnectionUrlExtraOptions");
        this.h2ConnectionUrlExtraOptions = h2ConnectionUrlExtraOptions;
        return this;
    }

    public T3TopiaApplicationContextBuilder fromBackup(Path backupFile) {
        checkIsH2("fromBackup");
        this.backupFile = backupFile;
        return this;
    }

    public T3TopiaApplicationContext build() {
        Path scriptPath = new File(new File("").getAbsolutePath()).getParentFile().toPath().resolve(".mvn").resolve("cache").resolve("test");

        Properties configuration = new Properties();
        try (InputStream stream = T3TopiaApplicationContext.class.getResourceAsStream(T3_TOPIA_CONFIG_PROPERTIES)) {
            configuration.load(stream);
        } catch (IOException e) {
            throw new TopiaException("Could not load input file " + T3_TOPIA_CONFIG_PROPERTIES, e);
        }
        if (addMigration) {
            try (InputStream stream = T3TopiaApplicationContext.class.getResourceAsStream(T3_TOPIA_MIGRATION_PROPERTIES)) {
                configuration.load(stream);
            } catch (IOException e) {
                throw new TopiaException("Could not load input file " + T3_TOPIA_MIGRATION_PROPERTIES, e);
            }
        }

        if (isH2()) {
            dbFile = Objects.requireNonNull(dbPath).getParent().resolve(dbPath.toFile().getName() + ".mv.db");
            if (deleteDbFile && Files.exists(dbFile)) {
                try {
                    Files.delete(dbFile);
                } catch (IOException e) {
                    throw new IllegalStateException("Can't delete h2 database: " + dbFile, e);
                }
            }
            String jdbcUrl = "jdbc:h2:file:" + dbPath.toFile().getAbsolutePath() + ";MODE=PostgreSQL";
            if (h2ConnectionUrlExtraOptions != null) {
                jdbcUrl += h2ConnectionUrlExtraOptions;
            }
            configuration.put(HibernateAvailableSettings.URL, jdbcUrl);
            configuration.put(HibernateAvailableSettings.USER, "sa");
            configuration.put(HibernateAvailableSettings.PASS, "sa");
            if (loadReferential) {
                try {
                    File referentialDirectory = T3JdbcHelper.getScriptsDirectory(scriptPath.toFile(), "h2", referentialVersion);
                    File cacheDbFile = new File(referentialDirectory, "t3-data-h2-with-referential-" + referentialVersion + ".mv.db");
                    if (cacheDbFile.exists()) {
                        Files.copy(cacheDbFile.toPath(), dbFile);
                    } else {
                        cacheDbFile = new File(referentialDirectory, "t3-data-h2-with-referential-" + referentialVersion + ".sql");
                        if (cacheDbFile.exists()) {
                            backupFile = cacheDbFile.toPath();
                        } else {
                            throw new IllegalStateException("Could not find any referential database or script to load.");
                        }
                    }
                } catch (Exception e) {
                    throw new IllegalStateException(String.format("can't load referential: %s", e.getMessage()), e);
                }
            }
        }
        if (isPg()) {
            configuration.put(HibernateAvailableSettings.URL, jdbcConfiguration.getUrl());
            configuration.put(HibernateAvailableSettings.USER, jdbcConfiguration.getLogin());
            configuration.put(HibernateAvailableSettings.PASS, jdbcConfiguration.getPassword());

            if (checkConnexion) {
                try {
                    T3EntityHelper.checkJDBCConnection(jdbcConfiguration);
                } catch (SQLException e) {
                    throw new IllegalStateException(String.format("Can't connect to database: %s", jdbcConfiguration.getUrl()), e);
                }
            }
        }
        BeanTopiaConfiguration topiaConfiguration = new TopiaConfigurationBuilder().readProperties(configuration);
        topiaConfiguration.setValidateSchema(false);
        topiaConfiguration.setInitSchema(backupFile == null);
        T3TopiaApplicationContext topiaApplicationContext = new T3TopiaApplicationContext(topiaConfiguration);
        if (isH2()) {
            if (backupFile != null) {
                topiaApplicationContext.newJdbcHelper().executeSql(backupFile.toFile());
            }
        }
        return topiaApplicationContext;
    }

    public Path getDbPath() {
        return dbPath;
    }

    public Path getDbFile() {
        return dbFile;
    }


    private void checkIsH2(String method) {
        if (!isH2()) {
            throw new IllegalStateException(String.format("Can't use method %s since not in h2 mode", method));
        }
    }

    private void checkIsPg(String method) {
        if (!isPg()) {
            throw new IllegalStateException(String.format("Can't use method %s since not in pg mode", method));
        }
    }

    private boolean isH2() {
        return dbPath != null;
    }

    private boolean isPg() {
        return jdbcConfiguration != null;
    }

}
