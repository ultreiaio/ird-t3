/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SampleSetImpl extends SampleSetAbstract {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(SampleSetImpl.class);

    // Used for import without logbook
    private SchoolType activitySchoolType;
    // Used for import without logbook
    private Float activityLatitude;
    // Used for import without logbook
    private Float activityLongitude;
    private Integer activityQuadrant;

    @Override
    public void deleteComputedData() {
        log.debug(String.format("delete computed data from %s", getTopiaId()));
        ComputedDataHelper.deleteComputedData(this);
    }

    @Override
    public void deleteComputedDataLevel0() {
        // nothing to delete
    }

    @Override
    public void deleteComputedDataLevel1() {

        setRfMinus10(null);
        setRfPlus10(null);
        setRfTot(null);
        setPropWeightedWeight(null);
        setWeightedWeightMinus10(null);
        setWeightedWeightPlus10(null);
        setTotalSampleWeight(null);

        clearSampleSetSpeciesFrequency();
        clearSampleSetSpeciesCatWeight();
    }

    @Override
    public void deleteComputedDataLevel2() {
        // nothing to delete
    }

    @Override
    public void deleteComputedDataLevel3() {
        // nothing to delete
    }

    @Override
    public float getTotalSampleSetSpeciesFrequencyNumber(Species species) {
        float nb = 0;
        for (SampleSetSpeciesFrequency standardiseSampleSpecies : getSampleSetSpeciesFrequency()) {
            if (species.equals(standardiseSampleSpecies.getSpecies())) {
                nb += standardiseSampleSpecies.getNumber();
            }
        }
        return nb;
    }

    @Override
    public SchoolType getActivitySchoolType() {
        if (getActivity() != null) {
            return getActivity().getSchoolType();
        }
        return activitySchoolType;
    }

    @Override
    public void setActivitySchoolType(SchoolType activitySchoolType) {
        this.activitySchoolType = activitySchoolType;
    }

    @Override
    public Float getActivityLatitude() {
        if (getActivity() != null) {
            return getActivity().getLatitude();
        }
        return activityLatitude;
    }

    @Override
    public void setActivityLatitude(Float activityLatitude) {
        this.activityLatitude = activityLatitude;
    }

    @Override
    public Float getActivityLongitude() {
        if (getActivity() != null) {
            return getActivity().getLongitude();
        }
        return activityLongitude;
    }

    @Override
    public void setActivityLongitude(Float activityLongitude) {
        this.activityLongitude = activityLongitude;
    }

    @Override
    public Integer getActivityQuadrant() {
        if (getActivity() != null) {
            return getActivity().getQuadrant();
        }
        return activityQuadrant;
    }

    @Override
    public void setActivityQuadrant(Integer activityQuadrant) {
        this.activityQuadrant = activityQuadrant;
    }
}
