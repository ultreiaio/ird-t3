package fr.ird.t3.entities;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.cache.ActivityCache;
import fr.ird.t3.entities.cache.LengthWeightConversionCache;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.cache.WeightCategoryTreatmentCache;
import fr.ird.t3.entities.cache.ZoneStratumCache;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;

import java.util.Date;

public class T3TopiaPersistenceContext extends AbstractT3TopiaPersistenceContext {

    public T3TopiaPersistenceContext(AbstractTopiaPersistenceContextConstructorParameter parameter) {
        super(parameter);
    }

    public ActivityCache newActivityCache() {
        return new ActivityCache(getActivityDao());
    }

    public WeightCategoryTreatmentCache newWeightCategoryTreatmentCache() {
        return new WeightCategoryTreatmentCache(getWeightCategoryTreatmentDao());
    }

    public LengthWeightConversionCache newLengthWeightConversionSimpleCache() {
        return new LengthWeightConversionCache(getLengthWeightConversionDao());
    }

    public LengthWeightConversionWithContextCache newLengthWeightConversionWithContextCache(Ocean ocean, Date date) {
        return new LengthWeightConversionWithContextCache(getLengthWeightConversionDao(), ocean, 0, date);
    }

    public ZoneStratumCache newZoneStratumCache(ZoneStratumAwareMeta zoneMeta, String zoneVersionId) {
        return new ZoneStratumCache(this, zoneMeta, zoneVersionId);
    }
}
