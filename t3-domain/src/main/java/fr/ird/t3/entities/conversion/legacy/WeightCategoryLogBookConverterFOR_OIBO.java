/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion.legacy;

import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverter;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverterProvider;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * To convert weight categories for Indian ocean and object school type.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class WeightCategoryLogBookConverterFOR_OIBO extends AbstractWeightCategoryLogBookConverter {

    private final WeightCategoryTreatment plus10Category;

    public WeightCategoryLogBookConverterFOR_OIBO(
            WeightCategoryTreatment unknownCategory,
            WeightCategoryTreatment minus10Category,
            WeightCategoryTreatment plus10Category) {

        super(WeightCategoryLogBookConverterProvider.OI,
                WeightCategoryLogBookConverterProvider.BO,
                minus10Category, unknownCategory);
        this.plus10Category = plus10Category;
    }

    @Override
    protected Map<WeightCategoryTreatment, Map<Integer, Float>> buildDistributions() {
        Map<WeightCategoryTreatment, Map<Integer, Float>> result =
                new HashMap<>();

        // 1 + 2 + 4*(0.2) + 10
        Map<Integer, Float> distributionMinus10 = new TreeMap<>();
        distributionMinus10.put(1, 1f);
        distributionMinus10.put(2, 1f);
        distributionMinus10.put(4, 0.2f);
        distributionMinus10.put(10, 1f);

        result.put(minus10Category, distributionMinus10);

        // 3 + 4*(0.8) + 5 + 6 + 7 + 8 + 11 + 12 + 13
        Map<Integer, Float> distributionPlus10 = new TreeMap<>();
        distributionPlus10.put(3, 1f);
        distributionPlus10.put(4, 0.8f);
        distributionPlus10.put(5, 1f);
        distributionPlus10.put(6, 1f);
        distributionPlus10.put(7, 1f);
        distributionPlus10.put(8, 1f);
        distributionPlus10.put(11, 1f);
        distributionPlus10.put(12, 1f);
        distributionPlus10.put(13, 1f);

        result.put(plus10Category, distributionPlus10);
        return result;
    }

    @Override
    public final Map<WeightCategoryTreatment, Float> distribute(Species species,
                                                                Collection<ElementaryCatch> catches) {

        float totalWeight = WeightCategoryLogBookConverter.getTotalWeight(catches);
        if (totalWeight == 0) {

            // this means no catches
            return new HashMap<>();
        }

        Map<WeightCategoryTreatment, Float> result;

        switch (species.getCode()) {
            case 1:
            case 3:
            case 4:

                float unknownWeight = WeightCategoryLogBookConverter.getUnknownWeight(catches);

                result = defaultDistributeForSpecie1or3or4(catches,
                        getDistributions(),
                        unknownWeight
                );

                break;
            case 2:

                result = defaultDistributeForSpecie2(totalWeight);

                break;
            default:

                result = defaultDistributeForOtherSpecie(totalWeight);
        }
        return result;
    }
}
