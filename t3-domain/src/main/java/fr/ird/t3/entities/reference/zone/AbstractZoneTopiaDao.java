/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.TopiaException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AbstractZoneTopiaDao<E extends Zone> extends GeneratedZoneTopiaDao<E> {

    public List<ZoneVersion> findAllVersion() throws TopiaException {
        String hql = "SELECT distinct(versionId), versionLabel, versionStartDate, versionEndDate " +
                "FROM " + getTopiaEntityEnum().getImplementationFQN();
        List<Object[]> queryList = findAll(hql);
        List<ZoneVersion> result = new ArrayList<>();
        for (Object[] o : queryList) {
            ZoneVersion z = new ZoneVersion((String) o[0], (String) o[1], (Date) o[2], (Date) o[3]);
            result.add(z);
        }
        return result;
    }

    public ZoneVersion findVersionByVersionId(String versionId) throws TopiaException {
        String hql = "SELECT distinct(versionId), versionLabel, versionStartDate, versionEndDate" +
                " FROM " + getTopiaEntityEnum().getImplementationFQN() +
                " WHERE versionId = :versionId";

        HqlAndParametersBuilder<E> builder = newHqlAndParametersBuilder();
        builder.setSelectClause("SELECT distinct(versionId), versionLabel, versionStartDate, versionEndDate");
        builder.addEquals("versionId", versionId);

        List<Object[]> queryList = findAll(hql, ImmutableMap.of("versionId", versionId));
        ZoneVersion result = null;
        if (CollectionUtils.isNotEmpty(queryList)) {
            Object[] l = queryList.get(0);
            result = new ZoneVersion((String) l[0], (String) l[1], (Date) l[2], (Date) l[3]);
        }
        return result;
    }
}
