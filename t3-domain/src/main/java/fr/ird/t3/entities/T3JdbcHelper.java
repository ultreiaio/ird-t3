package fr.ird.t3.entities;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import fr.ird.t3.T3SqlScriptsImporter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.util.ZipUtil;
import org.nuiton.util.sql.SqlFileReader;
import org.nuiton.version.Version;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by tchemit on 03/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3JdbcHelper extends JdbcHelper {

    private static final Log log = LogFactory.getLog(T3JdbcHelper.class);

    private static final int BUZZER_SIZE = 100;

    T3JdbcHelper(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    public static File getScriptsDirectory(File buildRootDirectory, String classifier, Version version) throws IOException {
        File scriptsZip = Paths.get(buildRootDirectory.getAbsolutePath())
                .resolve(String.format("t3-data-%s-%s.zip", version, classifier))
                .toFile();
        File target = scriptsZip.getParentFile().toPath()
                .resolve(String.format("t3-data-%s-%s", classifier, version))
                .toFile();
        if (!Files.exists(target.toPath())) {
            log.info(String.format("Unzip: %s", scriptsZip));
            ZipUtil.uncompress(scriptsZip, scriptsZip.getParentFile());
        }
        Files.createDirectories(target.toPath());
        target.deleteOnExit();
        Preconditions.checkState(target.exists(), "Could not find script directory: " + target);
        log.info(String.format("Scripts directory: %s", target));
        return target;
    }

    public static List<File> getSqlFiles(File directory) {
        String[] list = directory.list((dir, name) -> name.endsWith(".sql"));
        ImmutableList.Builder<File> filesBuilder = ImmutableList.builder();
        if (list != null) {
            List<String> scriptsName = Arrays.asList(list);
            Collections.sort(scriptsName);
            for (String scriptName : scriptsName) {
                filesBuilder.add(new File(directory, scriptName));
            }
        }
        return filesBuilder.build();
    }

    public T3SqlScriptsImporter createScriptsImporter(File scriptDirectory) {
        return new T3SqlScriptsImporter(this, scriptDirectory);
    }

    public void executeSql(File file) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)) {
            executeSql(bufferedReader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void executeSql(InputStream file) {
        try (InputStreamReader bufferedReader = new InputStreamReader(file, StandardCharsets.UTF_8)) {
            executeSql(bufferedReader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void executeSql(Reader reader) {
        try (Connection connection = openConnection()) {
            try (Statement preparedStatement = connection.createStatement()) {
                int statementCount = 0;
                StringBuilder currentStatement = null;
                for (String sqlStatement : new SqlFileReader(reader)) {
                    if (sqlStatement.startsWith("--")) {
                        continue;
                    }
                    if (sqlStatement.trim().endsWith(";")) {
                        if (currentStatement == null) {
                            preparedStatement.addBatch(sqlStatement);
                        } else {
                            currentStatement.append("\n").append(sqlStatement);
                            preparedStatement.addBatch(currentStatement.toString());
                            currentStatement = null;
                        }
                        statementCount++;
                    } else {
                        if (currentStatement == null) {
                            currentStatement = new StringBuilder(sqlStatement);
                        } else {
                            currentStatement.append("\n").append(sqlStatement);
                        }
                    }
                    if (statementCount % BUZZER_SIZE == 0) {
                        preparedStatement.executeBatch();
                        preparedStatement.clearBatch();
                    }
                }
                preparedStatement.executeBatch();
                preparedStatement.clearBatch();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void backup(File file, boolean compress) {
        String options = "";
        if (compress) {
            options += " COMPRESSION GZIP";
        }
        runSelectOnString("SCRIPT TO '" + file.getAbsolutePath() + "'" + options);
    }
}
