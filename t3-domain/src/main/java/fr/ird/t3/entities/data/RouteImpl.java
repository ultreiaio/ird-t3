package fr.ird.t3.entities.data;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class RouteImpl extends RouteAbstract {

    private static final long serialVersionUID = 1L;

    /** @return the sum of all activities set duration in hours. */
    @Override
    public float getTotalSetsDuration() {
        float result = 0;
        if (isActivityNotEmpty()) {
            for (Activity activity : getActivity()) {
                // get the setDuration
                Float setTime = activity.getSetDuration();
                result += setTime == null ? 0f : setTime;
            }
        }
        return result / 60;
    }

    @Override
    public void deleteComputedDataLevel0() {
        setComputedFishingTimeN0(null);
        setComputedSearchTimeN0(null);
        ComputedDataHelper.deleteComputedDataLevel0(getActivity());
    }

    @Override
    public void deleteComputedData() {
        ComputedDataHelper.deleteComputedData(this);
    }

    @Override
    public void deleteComputedDataLevel1() {
        ComputedDataHelper.deleteComputedDataLevel1(getActivity());
    }

    @Override
    public void deleteComputedDataLevel2() {
        ComputedDataHelper.deleteComputedDataLevel2(getActivity());
    }

    @Override
    public void deleteComputedDataLevel3() {
        ComputedDataHelper.deleteComputedDataLevel3(getActivity());
    }
}
