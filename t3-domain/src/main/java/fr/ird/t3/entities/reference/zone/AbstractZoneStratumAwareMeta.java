/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;

import java.util.Collection;
import java.util.List;

/**
 * Abstract implementation of {@link ZoneStratumAware}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractZoneStratumAwareMeta implements ZoneStratumAwareMeta {

    private static final long serialVersionUID = 1L;

    /** Persistent type of the zone. */
    protected final Class<? extends ZoneStratumAware> type;

    /** Persistent table name of the zone. */
    private final String tableName;

    /** I18n label of the type of zone. */
    private final String i18nKey;

    AbstractZoneStratumAwareMeta(String tableName, Class<? extends ZoneStratumAware> type, String i18nKey) {
        this.tableName = tableName;
        this.type = type;
        this.i18nKey = i18nKey;
    }

    @Override
    public List<ZoneVersion> getAllZoneVersions(TopiaDaoSupplier tx) {
        AbstractZoneStratumAwareTopiaDao<?> dao = getDAO(tx);
        return dao.findAllVersion();
    }

    @Override
    public List<ZoneStratumAware> getZones(Collection<Ocean> oceans, SchoolType schoolType, ZoneVersion zoneVersion, TopiaDaoSupplier tx) {
        AbstractZoneStratumAwareTopiaDao<ZoneStratumAware> dao = getDAO(tx);
        return dao.findAllByOceanSchoolTypeAndZoneVersion(oceans, schoolType, zoneVersion);
    }

    @Override
    public ZoneVersion getZoneVersion(String zoneVersionId, TopiaDaoSupplier tx) {
        AbstractZoneStratumAwareTopiaDao<ZoneStratumAware> dao = getDAO(tx);
        return dao.findVersionByVersionId(zoneVersionId);
    }

    public Class<? extends ZoneStratumAware> getType() {
        return type;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public String getId() {
        return getTableName();
    }

    public String getI18nKey() {
        return i18nKey;
    }

    @SuppressWarnings("unchecked")
    private AbstractZoneStratumAwareTopiaDao<ZoneStratumAware> getDAO(TopiaDaoSupplier tx) {
        TopiaDao dao = tx.getDao(type);
        return (AbstractZoneStratumAwareTopiaDao<ZoneStratumAware>) dao;
    }
}
