/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.cache;

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategories;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentTopiaDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Deal a cache of {@link WeightCategoryTreatment}.
 * <p>
 * Created by tchemit on 23/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class WeightCategoryTreatmentCache {
    private static final Log log = LogFactory.getLog(WeightCategoryTreatmentCache.class);
    private final ArrayListMultimap<String, WeightCategoryTreatment> cache;
    private final WeightCategoryTreatmentTopiaDao dao;

    public WeightCategoryTreatmentCache(WeightCategoryTreatmentTopiaDao dao) {
        this.dao = dao;
        cache = ArrayListMultimap.create();
    }

    public List<WeightCategoryTreatment> forActivity(Activity activity) {
        return forOceanAndSchoolType(activity.getOcean(), activity.getSchoolType());
    }

    List<WeightCategoryTreatment> forOceanAndSchoolType(Ocean ocean, SchoolType schoolType) {
        String key = ocean.getLabel1() + "#" + schoolType.getLabel1();
        List<WeightCategoryTreatment> result;
        if (cache.containsKey(key)) {
            result = cache.get(key);
        } else {
            result = dao.forOceanEquals(ocean).addEquals(WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE, schoolType).findAll();
            WeightCategories.sort(result);
            cache.putAll(key, result);
            log.info(String.format("Cache WeightCategoryTreatment [%d] for %s (%d categories)", cache.keySet().size(), key, result.size()));
        }
        return result;
    }

    public List<WeightCategoryTreatment> forOceansAndSchoolType(Collection<Ocean> oceans, SchoolType schoolType) {
        String key = Joiner.on("#").join(oceans.stream().map(Ocean::getLabel1).collect(Collectors.toList())) + "#" + schoolType.getLabel1();
        List<WeightCategoryTreatment> result;
        if (cache.containsKey(key)) {
            result = cache.get(key);
        } else {
            result = dao.forOceanIn(oceans).addEquals(WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE, schoolType).findAll();
            WeightCategories.sort(result);
            log.info(String.format("Cache WeightCategoryTreatment [%d] for %s (%d categories)", cache.keySet().size(), key, result.size()));
            cache.putAll(key, result);
        }
        return result;
    }
}
