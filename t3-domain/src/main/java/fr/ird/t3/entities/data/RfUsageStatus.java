package fr.ird.t3.entities.data;

/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Rf±10 usage status in {@link SampleSet}.
 * <p>
 * Created on 01/02/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SampleSet#getRfMinus10UsageStatus()
 * @see SampleSet#getRfPlus10UsageStatus() ()
 * @since 2.0
 */
public enum RfUsageStatus {
    ACCEPTED,
    REJECTED_BY_CONFIGURATION,
    REJECTED_RF_NOT_DEFINED,
    REJECTED_RF_IS_ZERO,
    REJECTED_RF_TOO_HIGH,
    REJECTED_NOT_ENOUGH_SAMPLE_COUNT
}
