/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * {@link WeightCategoryTreatment} user dao operations.
 *
 * @author tchemit chemit@codelutin.com
 * @since 1.0
 */
public class AbstractWeightCategoryTreatmentTopiaDao<E extends WeightCategoryTreatment> extends GeneratedWeightCategoryTreatmentTopiaDao<E> {

    private static final String OCEAN = WeightCategoryTreatment.PROPERTY_OCEAN + "." + Ocean.PROPERTY_CODE;
    private static final String SCHOOL_TYPE = WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE + "." + SchoolType.PROPERTY_CODE;

    public static WeightCategoryTreatment getWeightCategoryTreatment(Map<WeightCategoryTreatment, Integer> weightCategories, int lfLengthClass) {
        WeightCategoryTreatment result = null;
        for (Map.Entry<WeightCategoryTreatment, Integer> e : weightCategories.entrySet()) {
            if (lfLengthClass < e.getValue()) {
                // got the good category
                result = e.getKey();
                break;
            }
        }

        if (result == null) {
            throw new IllegalStateException(
                    String.format("Could not find a weightCategoryTreatment for given lfLengthClass %d", lfLengthClass));
        }
        return result;
    }

    public WeightCategoryTreatment findByOceanSchoolTypeAndBound(int oceanCode, int schoolTypeCode, Integer min, Integer max) {
        TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> queryBuilder = newQueryBuilder()
                .addEquals(OCEAN, oceanCode)
                .addEquals(SCHOOL_TYPE, schoolTypeCode);
        if (min == null) {
            queryBuilder.addNull("min");
        } else {
            queryBuilder.addEquals("min", min);
        }
        if (max == null) {
            queryBuilder.addNull("max");
        } else {
            queryBuilder.addEquals("max", max);
        }
        return queryBuilder.findAnyOrNull();

    }

    /**
     * Obtain for each given school type in the given ocean (by his id) all available weight category.
     *
     * @param ocean       the filtering ocean
     * @param schoolTypes all the school type
     * @return the universe of weight categories found for each school type (in
     * the given ocean too)
     */
    public Multimap<SchoolType, E> getWeightCategories(Ocean ocean, Set<SchoolType> schoolTypes) {
        Multimap<SchoolType, E> result = ArrayListMultimap.create();
        for (SchoolType schoolType : schoolTypes) {
            List<E> categories = findAllByOceanAndSchoolType(ocean, schoolType);
            result.putAll(schoolType, categories);
        }
        return result;
    }

    /**
     * Obtain for given school type in the given ocean, all available weight categories.
     *
     * @param ocean      the filtering ocean
     * @param schoolType the filtering school type
     * @return the universe of weight categories found for each school type (in
     * the given ocean too)
     */
    private List<E> findAllByOceanAndSchoolType(Ocean ocean, SchoolType schoolType) {
        return forProperties(
                WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE, schoolType,
                WeightCategoryTreatment.PROPERTY_OCEAN, ocean
        ).findAll();
    }

//    public static Comparator<WeightCategoryTreatment> newComparator() {
//        return new WeightCategoryTreatmentComparator();
//    }

//    public static void sortWeightCategoryTreatments(List<WeightCategoryTreatment> list) {
//        list.sort(new WeightCategoryTreatmentComparator());
//    }

//    public void sort(List<E> weightCategoryTreatment) {
//        weightCategoryTreatment.sort(newComparator());
//    }

//    public Map<E, Integer> getWeightCategoryTreatmentLengthClass(Ocean ocean, LengthWeightConversion conversion) {
//
//        // get all categories for the given ocean
//        List<E> categories = forOceanEquals(ocean).findAll();
//
//        Map<E, Integer> result = Maps.newTreeMap(newComparator());
//
//        for (E category : categories) {
//
//            Integer lengthClass;
//            Integer max = category.getMax();
//            if (max == null) {
//
//                // this means that the category has not upper bound,
//                // just use Integer.MAX_VALUE (this will enougth I think)
//                lengthClass = Integer.MAX_VALUE;
//
//            } else {
//
//                // convert max weight to max lf length class
//                lengthClass = conversion.computeLFLengthClassFromWeight(max);
//            }
//
//            // keep for the category the max length class computed
//            result.put(category, lengthClass);
//        }
//
//        return result;
//    }

//    public static class WeightCategoryTreatmentComparator implements Comparator<WeightCategoryTreatment>, Serializable {
//
//        private static final long serialVersionUID = 1L;
//
//        private WeightCategoryTreatmentComparator() {
//        }
//
//        @Override
//        public int compare(WeightCategoryTreatment o1, WeightCategoryTreatment o2) {
//            if (o1.getMin() == null && o1.getMax() == null) {
//                // equals to o2 if only if o2.min = o2.max = null
//                if (o2.getMin() == null && o2.getMax() == null) {
//                    return 0;
//                }
//                return 1;
//            }
//            if (o2.getMin() == null && o2.getMax() == null) {
//                // equals to o1 if only if o1.min = o1.max = null
//                if (o1.getMin() == null && o1.getMax() == null) {
//                    return 0;
//                }
//                return -1;
//            }
//            if (o1.getMin() == null) {
//                return o2.getMin() == null ? 0 : -1;
//            }
//            if (o2.getMin() == null) {
//                return 1;
//            }
//            return o1.getMin().compareTo(o2.getMin());
//        }
//    }
}
