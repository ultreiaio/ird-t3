/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LengthWeightConversionImpl extends LengthWeightConversionAbstract {

    private static final Log log = LogFactory.getLog(LengthWeightConversionImpl.class);
    private static final long serialVersionUID = 1L;
    private static final Pattern COEFFICIENTS_PATTERN = Pattern.compile("(.+)=(.+)");
    /**
     * Internal math context used for round operations.
     */
    private static final MathContext MC_WEIGHT = new MathContext(3);
    /**
     * Variable used in Weight to Length relation.
     */
    private static final String VARIABLE_WEIGHT = "P";
    /**
     * Variable used in Length to Weight relation.
     */
    private static final String VARIABLE_LENGTH = "L";
    /**
     * Each thread gets his own script engine.
     */
    private static final ThreadLocal<ScriptEngine> SCRIPT_ENGINE =
            ThreadLocal.withInitial(() -> {
                ScriptEngineManager factory = new ScriptEngineManager();
                return factory.getEngineByExtension("js");
            });

    private static ScriptEngine getScriptEngine() {
        return SCRIPT_ENGINE.get();
    }

    @Override
    public int getCode() {
        // not used
        return 0;
    }

    @Override
    public String getLabel1() {
        // not used
        return null;
    }

    @Override
    public Float computeWeightFromLFLengthClass(int lengthClass) {
        Integer lfLengthClassStep = species.getLfLengthClassStep();
        float longueur = lengthClass + lfLengthClassStep / 2f;
        return computeWeight(longueur);
    }

    @Override
    public int computeLFLengthClassFromWeight(float weight) {
        float length = computeLength(weight);
        int lengthClass = (int) length;
        Integer lfLengthClassStep = species.getLfLengthClassStep();
        lengthClass = lengthClass - lengthClass % lfLengthClassStep;
        return lengthClass;
    }

    @Override
    public Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    @Override
    public Map<String, Double> getCoefficientValues() {
        Map<String, Double> result = new TreeMap<>();
        String coefficients = getCoefficients();
        if (coefficients != null) {
            for (String coefficientDef : coefficients.split(":")) {
                Matcher matcher = COEFFICIENTS_PATTERN.matcher(coefficientDef.trim());
                log.debug(String.format("constant to test = %s", coefficientDef));
                if (matcher.matches()) {
                    String key = matcher.group(1);
                    String val = matcher.group(2);
                    try {
                        Double d = Double.valueOf(val);
                        result.put(key, d);
                        log.debug(String.format("detect coefficient %s=%s", key, val));
                    } catch (NumberFormatException e) {
                        // can't get this number
                        log.warn(String.format("could not parse double %s for coefficient %s", val, key));
                    }
                }
            }
        }
        return result;
    }

    /**
     * For a given species, ocean, sex and date, get the first length class which corresponds to a weight of 10Kg or more.
     *
     * @return the first length class which when converts to weight exceeds 10Kg
     */
    @Override
    public int getSpeciePlus10KGLengthClass() {
        return getSpecieHighestLengthClass(10);
    }

    @Override
    public int getSpecieHighestLengthClass(Integer maxWeight) {
        int result;
        if (maxWeight == null) {
            // no max bound, can not compute it
            result = Integer.MAX_VALUE;
        } else {
            int lengthClassStep = species.getLfLengthClassStep();
            result = 0;
            float weight = 0;
            while (weight < maxWeight) {
                // convert the length into weight
                weight = computeWeightFromLFLengthClass(result);
                if (weight < maxWeight) {
                    // can try next length class
                    result += lengthClassStep;
                }
            }
        }
        return result;
    }

    @Override
    public Float computeWeight(float length) {
        Float weight = computeValue(getToWeightRelation(), VARIABLE_LENGTH, length);
        if (weight != null) {
            weight = roundWeight(weight);
        }
        return weight;
    }

    @Override
    public Float computeLength(float weight) {
        Double b = getCoefficientValue(LengthWeightConversionTopiaDao.COEFFICIENT_B);
        if (b == 0) {
            // limit case, can't convert length
            return null;
        }
        Float length = computeValue(getToLengthRelation(), VARIABLE_WEIGHT, weight);
        if (length != null) {
            length = roundLength(length);
        }
        return length;
    }

    private Float computeValue(String relation, String variable, float length) {
        Map<String, Double> coefficientValues = getCoefficientValues();
        ScriptEngine engine = getScriptEngine();
        Bindings bindings = engine.createBindings();
        for (Map.Entry<String, Double> entry : coefficientValues.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            bindings.put(key, value);
            log.debug(String.format("add constant %s=%s", key, value));
        }
        bindings.put(variable, length);
        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        Double o = null;
        try {
            o = (Double) engine.eval(relation);
        } catch (ScriptException e) {
            log.error(String.format("Could not compute value from %s", relation), e);
        }
        return o == null ? null : o.floatValue();
    }

    private Float roundWeight(Float number) {
        return round(number, MC_WEIGHT);
    }

    private Float roundLength(Float number) {
        return (float) number.intValue();
    }

    private Float round(Float number, MathContext mc) {
        float old = number;
        float integerPart = (int) old;
        float digit = old - (int) old;
        Float result = integerPart + new BigDecimal(digit).round(mc).floatValue();
        log.debug(String.format("round %s to %s with mc = %s", old, result, mc));
        return result;
    }
}
