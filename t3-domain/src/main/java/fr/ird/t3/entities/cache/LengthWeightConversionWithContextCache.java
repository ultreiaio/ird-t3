/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.cache;

import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionTopiaDao;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;

import java.util.Date;

/**
 * Deal a cache of {@link LengthWeightConversion} with a context (ocean, sex, date).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class LengthWeightConversionWithContextCache extends LengthWeightConversionCache {

    private final ConversionContext conversionContext;

    public LengthWeightConversionWithContextCache(LengthWeightConversionTopiaDao dao, Ocean ocean, int sex, Date date) {
        super(dao);
        this.conversionContext = new ConversionContext(ocean, sex, date);
    }

    /**
     * Get the converter for the given parameters from the cache.
     * <p/>
     * If not found, then load it from db.
     *
     * @param species species to use
     * @return converter found
     */
    public LengthWeightConversion getConversions(Species species) {
        return getConversions(species, conversionContext.getOcean(), conversionContext.getSex(), conversionContext.getDate());
    }

    private static class ConversionContext {

        private final Ocean ocean;

        private final int sex;

        private final Date date;

        private ConversionContext(Ocean ocean, int sex, Date date) {
            this.ocean = ocean;
            this.sex = sex;
            this.date = date;
        }

        Ocean getOcean() {
            return ocean;
        }

        int getSex() {
            return sex;
        }

        Date getDate() {
            return date;
        }
    }

}
