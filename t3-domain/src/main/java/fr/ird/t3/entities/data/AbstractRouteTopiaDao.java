package fr.ird.t3.entities.data;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.type.T3Point;
import fr.ird.t3.entities.type.T3PointImpl;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AbstractRouteTopiaDao<E extends Route> extends GeneratedRouteTopiaDao<E> {

    public T3Point getBarycenterForActivitiesOfADay(E route) throws TopiaException {
        TopiaSqlQuery<T3Point> query = new GetT3PointQuery<>(route);
        return topiaSqlSupport.findSingleResult(query);
    }

    private static class GetT3PointQuery<E extends Route> extends TopiaSqlQuery<T3Point> {
        private final E route;

        GetT3PointQuery(E route) {
            this.route = route;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT ST_X(record.point), ST_Y(record.point) FROM " +
                            "(" +
                            "   SELECT ST_Centroid(ST_Collect(a.the_geom)) AS point" +
                            "   FROM activity a " +
                            "   WHERE a.route = ?" +
                            ") AS record;");
            ps.setString(1, route.getTopiaId());
            return ps;
        }

        @Override
        public T3Point prepareResult(ResultSet set) throws SQLException {
            float x = set.getFloat(1);
            float y = set.getFloat(2);
            return new T3PointImpl(x, y);
        }
    }

}

