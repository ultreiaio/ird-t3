/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.T3Predicates;
import fr.ird.t3.entities.data.Trip;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * {@link Vessel} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com
 * @since 1.0
 */
public class AbstractVesselTopiaDao<E extends Vessel> extends GeneratedVesselTopiaDao<E> {

    public static <E extends Vessel> void retainsVesselSimpleTypes(Collection<E> vessels, Collection<VesselSimpleType> vesselSimpleTypes) {
        vessels.removeIf(T3Predicates.vesselUsingVesselType(vesselSimpleTypes).negate());
    }

    public static <E extends Vessel> void retainsFleetCountries(Collection<E> vessels, Collection<Country> fleetCountries) {
        vessels.removeIf(T3Predicates.vesselUsingFleetCountry(fleetCountries).negate());
    }

    public static <E extends Vessel> void retainsFlagCountries(Collection<E> vessels, Collection<Country> flagCountries) {
        vessels.removeIf(T3Predicates.vesselUsingFlagCountry(flagCountries).negate());
    }

    public static Set<Vessel> getAllVessels(Collection<Trip> allTrips) {
        return allTrips.stream().map(Trip::getVessel).collect(Collectors.toSet());
    }

    /**
     * Obtains all vessels used by all trips in the database.
     *
     * @return the set of used vessel in trips in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllUsedInTrip() throws TopiaException {
        String hql = "SELECT DISTINCT(v) FROM VesselImpl v, TripImpl t WHERE t.vessel = v.id";
        return new HashSet<>(findAll(hql));
    }

    /**
     * Obtain the set of possible vessels usable from the configuration to obtain catches of the catch stratum.
     * <p/>
     * Vessels must verify two conditions :
     * <ul>
     * <li>Be of a senneur vessel simple type</li>
     * <li>Be in the selected fleet (from the configuration)</li>
     * </ul>
     *
     * @param countries selected countries
     * @return set of possible vessels usable for activity (so trips) of the catch stratum
     * @throws TopiaException if any problem while loading data
     */
    public Set<E> getPossibleCatchVessels(Collection<Country> countries) throws TopiaException {
        List<E> vessels = forProperties(
                Vessel.PROPERTY_VESSEL_TYPE + "." +
                        VesselType.PROPERTY_VESSEL_SIMPLE_TYPE + "." +
                        VesselSimpleType.PROPERTY_CODE, 1).findAll();
        Predicate<Vessel> predicate = T3Predicates.vesselUsingFleetCountry(countries);
        return vessels.stream().filter(predicate).collect(Collectors.toSet());
    }

    /**
     * Obtain the set of possible vessels usable from the configuration to
     * obtain catches of the catch stratum.
     * <p/>
     * Vessels must verify two conditions :
     * <ul>
     * <li>Be of a senneur vessel simple type</li>
     * <li>Be in the selected fleet (from the configuration)</li>
     * </ul>
     *
     * @param sampleFleets level 2 configuration
     * @param sampleFlags  the database transaction to load data
     * @return set of possible vessels usable for activity (so trips) of the
     * catch stratum
     * @throws TopiaException if any problem while loading data
     */
    public Set<E> getPossibleSampleVessels(Collection<Country> sampleFleets,
                                           Collection<Country> sampleFlags) throws TopiaException {
        List<E> vessels = forProperties(
                Vessel.PROPERTY_VESSEL_TYPE + "." +
                        VesselType.PROPERTY_VESSEL_SIMPLE_TYPE + "." +
                        VesselSimpleType.PROPERTY_CODE, 1).findAll();
        return vessels.stream().filter(T3Predicates.vesselUsingFleetCountry(sampleFleets).and(T3Predicates.vesselUsingFlagCountry(sampleFlags))).collect(Collectors.toSet());
    }

}
