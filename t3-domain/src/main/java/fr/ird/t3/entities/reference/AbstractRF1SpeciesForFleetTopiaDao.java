/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AbstractRF1SpeciesForFleetTopiaDao<E extends RF1SpeciesForFleet> extends GeneratedRF1SpeciesForFleetTopiaDao<E> {

    public Multimap<Country, Species> getSpeciesForCountry(Collection<Country> fleets) {
        Multimap<Country, Species> result = ArrayListMultimap.create();
        for (Country country : fleets) {
            List<E> speciesForFleets = forCountryEquals(country).findAll();
            Set<Species> species = new HashSet<>();
            for (RF1SpeciesForFleet speciesForFleet : speciesForFleets) {
                species.add(speciesForFleet.getSpecies());
            }
            result.putAll(country, species);
        }
        return result;
    }
}
