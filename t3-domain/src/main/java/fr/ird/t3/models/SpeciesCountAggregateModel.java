/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.t3.entities.data.SpeciesFrequencyAware;
import fr.ird.t3.entities.reference.Species;

import java.io.Closeable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Aggregate some {@link SpeciesCountModel}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class SpeciesCountAggregateModel implements Closeable {

    protected final Map<Species, SpeciesCountModel> model;

    public SpeciesCountAggregateModel() {
        model = new HashMap<>();
    }

    public static void close(Map<?, SpeciesCountAggregateModel> map) {
        try {
            for (SpeciesCountAggregateModel model : map.values()) {
                model.close();
            }
        } finally {
            map.clear();
        }
    }

    public Set<String> getAvailableModelNames() {
        Set<String> result = new HashSet<>();
        for (SpeciesCountModel speciesCountModel : model.values()) {

            result.addAll(speciesCountModel.getModelNames());
        }
        return result;
    }

    public SpeciesCountModel getModel(Species species) {
        return model.computeIfAbsent(species, SpeciesCountModel::new);
    }

    public SpeciesCountAggregateModel extractForSpecies(Collection<Species> species) {
        SpeciesCountAggregateModel result = new SpeciesCountAggregateModel();
        for (SpeciesCountModel e : model.values()) {
            if (species.contains(e.getSpecies())) {
                result.addValues(e);
            }
        }
        return result;
    }

    public Set<Species> getAvailableSpecies() {
        return model.keySet();
    }

    public void addValues(SpeciesCountAggregateModel model) {
        Set<Species> modelAvailableSpecies = model.getAvailableSpecies();
        for (Species availableModelName : modelAvailableSpecies) {
            SpeciesCountModel myModel = getModel(availableModelName);
            SpeciesCountModel incomingModel = model.getModel(availableModelName);
            myModel.addValues(incomingModel);
        }
    }

    public void addValues(SpeciesCountModel incomingModel) {
        Species species = incomingModel.getSpecies();
        SpeciesCountModel myModel = getModel(species);
        if (myModel == null) {
            myModel = new SpeciesCountModel(species);
            model.put(species, myModel);
        }
        myModel.addValues(incomingModel);
    }

    public <E extends SpeciesFrequencyAware> void addValues(String modelName, Collection<E> data) {
        Multimap<Species, E> dataBySpecies = Multimaps.index(data, SpeciesFrequencyAware::getSpecies);
        for (Species species : dataBySpecies.keySet()) {
            SpeciesCountModel speciesModel = getModel(species);
            speciesModel.addValues(modelName, dataBySpecies.get(species));
        }
    }

    public Float getTotalCount(String modelName) {
        Float result = null;
        for (SpeciesCountModel aModel : model.values()) {
            Float totalCount = aModel.getTotalCount(modelName);
            if (totalCount != null) {
                if (result == null) {
                    result = 0f;
                }
                result += totalCount;
            }
        }
        return result;
    }

    @Override
    public void close() {
        try {
            for (SpeciesCountModel weightCompositionModel : model.values()) {
                weightCompositionModel.close();
            }
        } finally {
            model.clear();
        }
    }
}
