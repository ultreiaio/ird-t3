/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;

import java.io.Closeable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * To aggregate some {@link LengthCompositionModel} models.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class LengthCompositionAggregateModel implements Closeable {

    protected final Map<WeightCategoryTreatment, LengthCompositionModel> model;

    private final LengthCompositionModel totalModel;

    public LengthCompositionAggregateModel() {
        model = new HashMap<>();
        totalModel = new LengthCompositionModel();
    }

    public static void close(Map<?, LengthCompositionAggregateModel> map) {
        try {
            for (LengthCompositionAggregateModel model : map.values()) {
                model.close();
            }
        } finally {
            map.clear();
        }
    }

    public void addModel(LengthCompositionModel incomingModel) {
        WeightCategoryTreatment weightCategory = incomingModel.getWeightCategory();
        LengthCompositionModel m = getModel(weightCategory);
        if (m == null) {
            m = new LengthCompositionModel(weightCategory);
            // new model to store
            model.put(weightCategory, m);
        }
        // add weights to model
        m.addValues(incomingModel);
        // add also to total model
        totalModel.addValues(incomingModel);
    }

    public void addModel(WeightCategoryTreatment weightCategory, Species species, Map<Integer, Float> weights) {
        LengthCompositionModel m;
        m = getModel(weightCategory);
        if (m == null) {
            m = new LengthCompositionModel(weightCategory, species, weights);
            model.put(weightCategory, m);
        } else {
            // add weights to existing model
            m.addValues(weights);
        }
        // add also to total model
        totalModel.addValues(weights);
    }

//    public void addModel(LengthCompositionAggregateModel modelToMerge) {
//        for (LengthCompositionModel compositionModel :
//                modelToMerge.getModel().values()) {
//            addModel(compositionModel);
//        }
//    }

//    public LengthCompositionAggregateModel extractForLengthClasses(Collection<Integer> lengthClasses) {
//        LengthCompositionAggregateModel result = new LengthCompositionAggregateModel();
//        for (LengthCompositionModel e : model.values()) {
//            LengthCompositionModel newModel = e.extractForLengthClasses(lengthClasses);
//            if (newModel != null) {
//                result.addModel(newModel);
//            }
//        }
//        return result;
//    }

    public LengthCompositionModel getModel(WeightCategoryTreatment weightCategory) {
        return model.get(weightCategory);
    }

    public LengthCompositionModel getTotalModel() {
        return totalModel;
    }

    protected Map<WeightCategoryTreatment, LengthCompositionModel> getModel() {
        return model;
    }

    public Set<WeightCategoryTreatment> getWeightCategories() {
        return model.keySet();
    }

    @Override
    public void close() {
        try {
            totalModel.close();
            for (LengthCompositionModel weightCompositionModel : model.values()) {
                weightCompositionModel.close();
            }
        } finally {
            model.clear();
        }
    }
}
