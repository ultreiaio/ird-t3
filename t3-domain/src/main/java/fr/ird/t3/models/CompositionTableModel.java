/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

/**
 * Model of header to render models.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class CompositionTableModel {

    private String topSeparatorFormat;

    private String bottomSeparatorFormat;

    private String separatorFormat;

    private String separatorFormat2;

    private String separatorFormat3;

    private String categoryFormat;

    private String lineFormat;

    private String header;

    public String getTopSeparatorFormat() {
        return topSeparatorFormat;
    }

    public void setTopSeparatorFormat(String topSeparatorFormat) {
        this.topSeparatorFormat = topSeparatorFormat;
    }

    public String getBottomSeparatorFormat() {
        return bottomSeparatorFormat;
    }

    public void setBottomSeparatorFormat(String bottomSeparatorFormat) {
        this.bottomSeparatorFormat = bottomSeparatorFormat;
    }

    public String getSeparatorFormat() {
        return separatorFormat;
    }

    public void setSeparatorFormat(String separatorFormat) {
        this.separatorFormat = separatorFormat;
    }

    public void setSeparatorFormat2(String separatorFormat2) {
        this.separatorFormat2 = separatorFormat2;
    }

    public String getSeparatorFormat2() {
        return separatorFormat2;
    }

    public String getSeparatorFormat3() {
        return separatorFormat3;
    }

    public void setSeparatorFormat3(String separatorFormat3) {
        this.separatorFormat3 = separatorFormat3;
    }

    public String getCategoryFormat() {
        return categoryFormat;
    }

    public void setCategoryFormat(String categoryFormat) {
        this.categoryFormat = categoryFormat;
    }

    public String getLineFormat() {
        return lineFormat;
    }

    public void setLineFormat(String lineFormat) {
        this.lineFormat = lineFormat;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
