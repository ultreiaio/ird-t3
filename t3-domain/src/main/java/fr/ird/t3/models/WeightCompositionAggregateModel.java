/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategories;
import fr.ird.t3.entities.reference.WeightCategory;

import java.io.Closeable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * To aggregate some {@link WeightCompositionModel} models.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class WeightCompositionAggregateModel implements Closeable {

    protected final Map<WeightCategory, WeightCompositionModel> model;
    private final WeightCompositionModel totalModel;

    public WeightCompositionAggregateModel() {
        model = Maps.newTreeMap(WeightCategories.newComparator());
        totalModel = new WeightCompositionModel();
    }

    public static void close(Map<?, WeightCompositionAggregateModel> map) {
        try {
            for (WeightCompositionAggregateModel model : map.values()) {
                model.close();
            }
        } finally {
            map.clear();
        }
    }

    public void addModel(WeightCategory weightCategory, Map<Species, Float> weights) {
        WeightCompositionModel m;
        m = getModel(weightCategory);
        if (m == null) {
            m = new WeightCompositionModel(weightCategory, weights);
            model.put(weightCategory, m);
        } else {
            // add weights to existing model
            m.addWeights(weights);
        }
        // add also to total model
        totalModel.addWeights(weights);
    }

    public void addModel(WeightCompositionModel incomingModel) {
        WeightCategory weightCategory = incomingModel.getWeightCategory();
        WeightCompositionModel m = getModel(weightCategory);
        if (m == null) {
            m = new WeightCompositionModel(weightCategory);
            // new model to store
            model.put(weightCategory, m);
        }
        // add weights to model
        m.addWeights(incomingModel);
        // add also to total model
        totalModel.addWeights(incomingModel);
    }

    public void addModel(WeightCompositionAggregateModel modelToMerge) {
        for (WeightCompositionModel compositionModel : modelToMerge.getModel().values()) {
            addModel(compositionModel);
        }
    }

    public WeightCompositionAggregateModel extractForSpecies(Collection<Species> species) {
        WeightCompositionAggregateModel result = new WeightCompositionAggregateModel();
        for (WeightCompositionModel e : model.values()) {
            WeightCompositionModel newModel = e.extractForSpecies(species);
            if (newModel != null) {
                result.addModel(newModel);
            }
        }
        return result;
    }

    public WeightCompositionModel getModel(WeightCategory weightCategory) {
        return model.get(weightCategory);
    }

    public WeightCompositionModel getTotalModel() {
        return totalModel;
    }

    protected Map<WeightCategory, WeightCompositionModel> getModel() {
        return model;
    }

    public Set<WeightCategory> getWeightCategories() {
        return model.keySet();
    }

    @Override
    public void close() {
        try {
            totalModel.close();
            for (WeightCompositionModel weightCompositionModel : model.values()) {
                weightCompositionModel.close();
            }
        } finally {
            model.clear();
        }
    }
}
