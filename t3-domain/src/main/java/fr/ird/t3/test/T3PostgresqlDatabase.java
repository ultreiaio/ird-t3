package fr.ird.t3.test;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaApplicationContextBuilder;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;

/**
 * A new database created for each test.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3PostgresqlDatabase implements TestRule {

    private File testBasedir;
    private T3TopiaApplicationContext topiaApplicationContext;

    @Override
    public Statement apply(Statement base, Description description) {
        testBasedir = T3IOUtil.getTestSpecificDirectory(description.getTestClass(), description.getMethodName());
        try {
            topiaApplicationContext = T3TopiaApplicationContextBuilder.forPgTest().checkConnexion().build();
        } catch (Exception e) {
            Assume.assumeNoException("Skip test " + description + ", could not connect to database", e);
        }
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } finally {
                    if (topiaApplicationContext != null) {
                        T3EntityHelper.releaseRootContext(topiaApplicationContext);
                    }
                }
            }
        };
    }

    public T3TopiaPersistenceContext beginTransaction() throws TopiaException {
        return topiaApplicationContext.newPersistenceContext();
    }

    public File getTestBasedir() {
        return testBasedir;
    }
}
