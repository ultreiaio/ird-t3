package fr.ird.t3.test;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.T3Config;
import fr.ird.t3.T3ConfigOption;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaApplicationContextBuilder;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.mockito.Mockito;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.FileUtil;
import org.nuiton.version.Version;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A new database created for each test.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3H2Database implements TestRule, Closeable {

    private final boolean injectReferential;
    private File testBasedir;
    protected T3TopiaApplicationContext topiaApplicationContext;
    private T3Config applicationConfiguration;
    private boolean initOk;
    public T3H2Database() {
        this(false);
    }

    public T3H2Database(boolean injectReferential) {
        this.injectReferential = injectReferential;
    }

    private void starting(Description description) {
        testBasedir = T3IOUtil.getTestSpecificDirectory(description.getTestClass(), description.getMethodName());

        Properties defaultProps = new Properties();
        defaultProps.put(T3ConfigOption.DATA_DIRECTORY.getKey(), testBasedir);
        File treatmentDirectory = new File(testBasedir, "treatment");
        FileUtil.createDirectoryIfNecessary(treatmentDirectory);
        T3Config realConfiguration = new T3Config(defaultProps) {
            @Override
            public void init() {
                parse();
            }
        };
        realConfiguration.init();
        Version t3DataVersion = realConfiguration.getT3DataVersion();
        applicationConfiguration = Mockito.mock(T3Config.class);
        Mockito.when(applicationConfiguration.getDataDirectory()).thenReturn(testBasedir);
        Mockito.when(applicationConfiguration.getTreatmentWorkingDirectory()).thenReturn(treatmentDirectory);
        Mockito.when(applicationConfiguration.getT3DataVersion()).thenReturn(t3DataVersion);
        Mockito.when(applicationConfiguration.getTreatmentWorkingDirectory(Mockito.anyString(), Mockito.anyBoolean())).thenCallRealMethod();
        Mockito.when(applicationConfiguration.getApplicationVersion()).thenReturn(realConfiguration.getApplicationVersion());

        if (injectReferential) {
            topiaApplicationContext = T3TopiaApplicationContextBuilder.forH2Referential(testBasedir.toPath(), t3DataVersion).build();
        } else {
            topiaApplicationContext = T3TopiaApplicationContextBuilder.forH2(testBasedir.toPath()).build();
        }
    }

    public T3TopiaPersistenceContext beginTransaction() throws TopiaException {
        return topiaApplicationContext.newPersistenceContext();
    }

    public void loadClassPathScript(String location) {
        try (InputStream resourceAsStream = getClass().getResourceAsStream(location)) {
            topiaApplicationContext.newJdbcHelper().executeSql(resourceAsStream);
        } catch (IOException e) {
            throw new TopiaException(e);
        }
    }

    @SuppressWarnings("unused")
    public File getTestBasedir() {
        return testBasedir;
    }

    public T3Config getApplicationConfiguration() {
        return applicationConfiguration;
    }

    @Override
    public void close() {
        if (topiaApplicationContext != null) {
            T3EntityHelper.releaseRootContext(topiaApplicationContext);
        }
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    starting(description);
                    initOk=true;
                } catch (Exception e) {
                    Assume.assumeNoException("Can't start test: " + e.getMessage(), e);
                }
                try {
                    base.evaluate();
                } finally {
                    close();
                }
            }
        };
    }

    public boolean isInitOk() {
        return initOk;
    }
}
