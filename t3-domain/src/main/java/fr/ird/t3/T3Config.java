/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3;

import com.google.common.base.Charsets;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import io.ultreia.java4all.config.ApplicationConfig;
import io.ultreia.java4all.config.ArgumentsParserException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.nuiton.util.FileUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * T3 configuration
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3Config extends GeneratedT3Config {

    protected static Log log = LogFactory.getLog(T3Config.class);

    private static T3Config config;

    public T3Config(Properties defaultProps) {
        ApplicationConfig applicationConfig = get();
        applicationConfig.loadDefaultOptions(T3ConfigOption.values());
        applicationConfig.setConfigFileName("t3.conf");
        log.info(this + " is initializing...");
        if (defaultProps != null) {
            for (Map.Entry<Object, Object> entry : defaultProps.entrySet()) {
                applicationConfig.setDefaultOption(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
            }
        }
    }

    public T3Config() {
        this(null);
    }

    public static T3Config instance() {
        return config;
    }

    public static void set(T3Config config) {
        T3Config.config = config;
    }

    /**
     * To initialise the configuration.
     * <p/>
     * Will mainly parse configuration options and creates required directories.
     */
    public void init() {
        // parse configuration (with no parameters)
        parse();

        createDirectory(T3ConfigOption.DATA_DIRECTORY, "data directory");
        createDirectory(T3ConfigOption.USER_LOG_DIRECTORY, "user logs directory");
        createDirectory(T3ConfigOption.TREATMENT_WORKING_DIRECTORY, "treatment work directory");

        File log4jConfigurationFile = getLog4jConfigurationFile();
        if (!log4jConfigurationFile.exists()) {

            if (log.isInfoEnabled()) {
                log.info("Generate default log4jConfigurationFile");
            }
            try {
                CharSource charSource = Resources.asCharSource(getClass().getResource("/t3-log4j.conf"), Charsets.UTF_8);
                Files.write(log4jConfigurationFile.toPath(), charSource.readLines());
            } catch (IOException e) {
                throw new IllegalStateException("Impossible de créer un fichier de log4j", e);
            }
        }

        initLog(log4jConfigurationFile);

    }

    public void parse(String... args) {
        try {
            get().parse(args);
        } catch (ArgumentsParserException e) {
            throw new IllegalStateException("Could not parse configuration", e);
        }
    }

    private File getLog4jConfigurationFile() {
        return get().getOptionAsFile(T3ConfigOption.LOG_FILE.getKey());
    }

    public File getTreatmentWorkingDirectory(String name, boolean create) {
        File file = getTreatmentWorkingDirectory();
        file = new File(file, name);
        if (create) {

            if (!file.exists()) {
                if (log.isInfoEnabled()) {
                    log.info("Will create treatment directory " + file);
                }
                FileUtil.createDirectoryIfNecessary(file);
            }
        }
        return file;
    }

    public String getApplicationVersion() {
        return get().getOption("application.version");
    }

    public List<Integer> getLevel2DefaultSpeciesAsList() {
        return get().getOptionAsList(T3ConfigOption.LEVEL2_DEFAULT_SPECIES.getKey()).getOptionAsInt();
    }

    public List<Integer> getLevel3DefaultSpeciesAsList() {
        return get().getOptionAsList(T3ConfigOption.LEVEL3_DEFAULT_SPECIES.getKey()).getOptionAsInt();
    }

    public boolean isUpdateSchema() {
        return get().getOptionAsBoolean("updateSchema");
    }

    /**
     * Creates a directory given the configuration given key.
     *
     * @param key  the configuration option key which contains the location of
     *             the directory to create
     * @param name a name used for logs
     */
    private void createDirectory(T3ConfigOption key, String name) {
        File directory = Objects.requireNonNull(get().getOptionAsFile(key.getKey()),
                String.format("Could not find %s (key %sin your configuration file named t3.conf)", name, key));
        log.info(key + " = " + directory);
        FileUtil.createDirectoryIfNecessary(directory);
    }

    private void initLog(File logFile) {
        if (!logFile.exists()) {
            throw new IllegalStateException(String.format("Le fichier de configuration des logs (%s) n'existe pas", logFile));
        }
        log.info("Chargement du fichier de log : " + logFile);
        Properties finalLogConfigurationProperties;
        try (BufferedReader inputStream = Files.newBufferedReader(logFile.toPath(), Charsets.UTF_8)) {

            Properties logConfigurationProperties = new Properties();
            logConfigurationProperties.load(inputStream);

            finalLogConfigurationProperties = loadProperties(logConfigurationProperties);

        } catch (Exception e) {
            throw new IllegalStateException("Impossible de charger le fichier de configuration des logs", e);
        }
        LogManager.resetConfiguration();
        PropertyConfigurator.configure(finalLogConfigurationProperties);
        log = LogFactory.getLog(T3Config.class);
        log.info(String.format("Configuration des logs chargée depuis le fichier %s", logFile));
    }

    private Properties loadProperties(Properties sourceProperties) {

        Properties targetProperties = new Properties();
        for (Map.Entry<Object, Object> entry : sourceProperties.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            String newValue = get().replaceRecursiveOptions(value);
            targetProperties.setProperty(key, newValue);
        }
        return targetProperties;
    }

}
