---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
CREATE TABLE ElementaryLandingFate( topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, code INTEGER, libelle VARCHAR(255), status boolean);
CREATE UNIQUE INDEX uk_elementaryLandingFate_code ON ElementaryLandingFate(code);
ALTER TABLE ElementaryLanding ADD COLUMN elementaryLandingFate VARCHAR(255);
ALTER TABLE ElementaryLanding ADD CONSTRAINT fk_elementaryLanding_elementaryLandingFate FOREIGN KEY (elementaryLandingFate) REFERENCES ElementaryLandingFate(topiaid);
