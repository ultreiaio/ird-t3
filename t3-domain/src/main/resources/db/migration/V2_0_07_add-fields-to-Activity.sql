---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE Activity ADD COLUMN comment TEXT;
ALTER TABLE Activity ADD COLUMN windSpeed REAL;
ALTER TABLE Activity ADD COLUMN windDirection REAL;
ALTER TABLE Activity ADD COLUMN ecologicalObject INTEGER;
ALTER TABLE Activity ADD COLUMN buoyOwnership INTEGER;
ALTER TABLE Activity ADD COLUMN buoyId VARCHAR(255);
ALTER TABLE Activity ADD COLUMN buoyRelatedWeight REAL;
ALTER TABLE Activity ADD COLUMN transmittingBuoyType VARCHAR(255);
ALTER TABLE Activity ADD CONSTRAINT fk_activity_transmittingBuoyType FOREIGN KEY (transmittingBuoyType) REFERENCES transmittingBuoyType(topiaid);
ALTER TABLE Activity ADD COLUMN fpaZone VARCHAR(255);
ALTER TABLE Activity ADD CONSTRAINT fk_activity_fpaZone FOREIGN KEY (fpaZone) REFERENCES fpaZone(topiaid);
ALTER TABLE Activity ADD COLUMN objectType VARCHAR(255);
ALTER TABLE Activity ADD CONSTRAINT fk_activity_objectType FOREIGN KEY (objectType) REFERENCES objectType(topiaid);
