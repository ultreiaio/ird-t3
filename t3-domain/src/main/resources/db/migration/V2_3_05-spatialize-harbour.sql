---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
SELECT AddGeometryColumn('harbour', 'the_geom', 4326, 'POINT',2 );
CREATE INDEX idx_harbour_gist ON harbour USING GIST (the_geom);
UPDATE Harbour SET the_geom = ST_SetSRID(ST_MakePoint(longitude,latitude), 4326) WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND the_geom IS NULL;