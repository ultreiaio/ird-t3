---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#00', 1, '2018-02-22 21:43:17.612000', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#01', 1, '2018-02-22 21:43:17.676000', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#02', 1, '2018-02-22 21:43:17.677000', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#03', 1, '2018-02-22 21:43:17.677000', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#04', 1, '2018-02-22 21:43:17.680000', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#05', 1, '2018-02-22 21:43:17.681000', 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#06', 1, '2018-02-22 21:43:17.685000', 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#07', 1, '2018-02-22 21:43:17.689000', 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '1.0', '1970-01-01', null);
INSERT INTO public.weightcategorylogbookconversion (topiaid, topiaversion, topiacreatedate, ocean, schooltype, version, begindate, enddate) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#08', 1, '2018-02-22 21:43:17.690000', 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '1.0', '1970-01-01', null);

INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#00', 1, '2018-02-22 21:43:17.758000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#00', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5499777108807193": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#01', 1, '2018-02-22 21:43:17.790000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#00', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.44386731073429053": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#02', 1, '2018-02-22 21:43:17.808000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#00', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5237318856431237": {
    "3": 1.0,
    "4": 0.8,
    "6": 0.5,
    "11": 0.1,
    "12": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5499777108807192": {
    "5": 1.0,
    "6": 0.5,
    "7": 1.0,
    "8": 1.0,
    "11": 0.9,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.44386731073429053": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#03', 1, '2018-02-22 21:43:17.811000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#01', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.988038536400382": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#04', 1, '2018-02-22 21:43:17.821000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#01', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830500#0.6310700924396421": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#05', 1, '2018-02-22 21:43:17.826000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#01', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.988038536400381": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830500#0.6310700924396421": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#06', 1, '2018-02-22 21:43:17.840000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#02', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5499777108807193": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#07', 1, '2018-02-22 21:43:17.843000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#02', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.44386731073429053": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#08', 1, '2018-02-22 21:43:17.855000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#02', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5237318856431237": {
    "3": 1.0,
    "4": 0.8,
    "6": 0.5,
    "11": 0.1,
    "12": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5499777108807192": {
    "5": 1.0,
    "6": 0.5,
    "7": 1.0,
    "8": 1.0,
    "11": 0.9,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.44386731073429053": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#017', 1, '2018-02-22 21:43:17.943000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#02', 3, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830508#0.8969775036221255": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.6434841998774135": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#09', 1, '2018-02-22 21:43:17.858000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#03', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.8969775036221247": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#010', 1, '2018-02-22 21:43:17.875000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#03', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.6434841998774145": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#011', 1, '2018-02-22 21:43:17.891000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#03', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.8969775036221246": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.6434841998774145": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#012', 1, '2018-02-22 21:43:17.897000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#04', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.14752652961660063": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#013', 1, '2018-02-22 21:43:17.903000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#04', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.4807760142660965": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#014', 1, '2018-02-22 21:43:17.920000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#04', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.14752652961660062": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.4807760142660965": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#015', 1, '2018-02-22 21:43:17.929000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#05', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830508#0.8969775036221256": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#016', 1, '2018-02-22 21:43:17.939000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#05', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.6434841998774135": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#018', 1, '2018-02-22 21:43:17.951000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#06', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.8969775036221247": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#019', 1, '2018-02-22 21:43:17.956000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#06', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.6434841998774145": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#020', 1, '2018-02-22 21:43:17.961000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#06', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.8969775036221246": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.6434841998774145": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#021', 1, '2018-02-22 21:43:17.970000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#07', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.14752652961660063": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#022', 1, '2018-02-22 21:43:17.975000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#07', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830506#0.4807760142660965": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#023', 1, '2018-02-22 21:43:17.988000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#07', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.14752652961660062": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830506#0.4807760142660965": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#024', 1, '2018-02-22 21:43:17.991000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#08', 0, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830508#0.8969775036221256": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#025', 1, '2018-02-22 21:43:17.994000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#08', 1, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.6434841998774135": {}
}');
INSERT INTO public.weightcategorylogbookconversionspecies (topiaid, topiaversion, topiacreatedate, parent, parent_idx, distribution) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#026', 1, '2018-02-22 21:43:18.006000', 'fr.ird.t3.entities.reference.WeightCategoryLogBookConversion#1519311532526#08', 2, '{
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830508#0.8969775036221255": {
    "3": 1.0,
    "4": 0.8,
    "5": 1.0,
    "6": 1.0,
    "7": 1.0,
    "8": 1.0,
    "11": 1.0,
    "12": 1.0,
    "13": 1.0
  },
  "fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.6434841998774135": {
    "1": 1.0,
    "2": 1.0,
    "4": 0.2,
    "10": 1.0
  }
}');

INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#01', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#02', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#02', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#02', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#04', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#05', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#05', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#05', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#07', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#08', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#08', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#08', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#010', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#011', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#011', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#011', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#013', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#014', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#014', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#014', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#016', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#017', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#017', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#017', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#019', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#020', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#020', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#020', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#022', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#023', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#023', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#023', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#025', 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#026', 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#026', 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063');
INSERT INTO weightcategorylogbookconversionspecies_species (weightcategorylogbookconversionspecies, species) VALUES ('fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies#1519311532526#026', 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515');
