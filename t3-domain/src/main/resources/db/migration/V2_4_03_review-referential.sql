---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE schooltype RENAME libelle4 TO homeId;
ALTER TABLE vesselsizecategory RENAME gaugelibelle TO homeId;
ALTER TABLE vesselsizecategory RENAME capacitylibelle TO label1;
ALTER TABLE vesselsizecategory ADD COLUMN label2 VARCHAR(255);
ALTER TABLE vesselsizecategory ADD COLUMN label3 VARCHAR(255);
ALTER TABLE vesselsizecategory ADD COLUMN label4 VARCHAR(255);
ALTER TABLE vesselsizecategory ADD COLUMN label5 VARCHAR(255);
ALTER TABLE vesselsizecategory ADD COLUMN label6 VARCHAR(255);
ALTER TABLE vesselsizecategory ADD COLUMN label7 VARCHAR(255);
ALTER TABLE vesselsizecategory ADD COLUMN label8 VARCHAR(255);
ALTER TABLE vessel ADD COLUMN iotcId VARCHAR(255);
ALTER TABLE vessel ADD COLUMN iccatId VARCHAR(255);
