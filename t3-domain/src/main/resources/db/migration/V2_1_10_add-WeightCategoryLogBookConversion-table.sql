---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- drop TABLE IF EXISTS weightcategorylogbookconversion CASCADE ;
-- drop TABLE IF EXISTS weightcategorylogbookconversionspecies CASCADE ;
-- drop TABLE IF EXISTS weightcategorylogbookconversionspecies_species CASCADE ;

CREATE TABLE WeightCategoryLogBookConversion( topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP NOT NULL, ocean VARCHAR(255) NOT NULL, schoolType VARCHAR(255) NOT NULL, version VARCHAR(255) NOT NULL, beginDate DATE NOT NULL, endDate DATE);
CREATE UNIQUE INDEX uk_WeightCategoryLogBookConversion ON WeightCategoryLogBookConversion(version, ocean, schoolType, beginDate);
ALTER TABLE WeightCategoryLogBookConversion ADD CONSTRAINT fk_WeightCategoryLogBookConversion_ocean FOREIGN KEY (ocean) REFERENCES Ocean(topiaid);
ALTER TABLE WeightCategoryLogBookConversion ADD CONSTRAINT fk_WeightCategoryLogBookConversion_schoolType FOREIGN KEY (schoolType) REFERENCES SchoolType(topiaid);
CREATE INDEX idx_WeightCategoryLogBookConversion_ocean ON WeightCategoryLogBookConversion(ocean);
CREATE INDEX idx_WeightCategoryLogBookConversion_schoolType ON WeightCategoryLogBookConversion(schoolType);

CREATE TABLE WeightCategoryLogBookConversionSpecies( topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP NOT NULL, parent VARCHAR(255) NOT NULL, parent_idx int, distribution TEXT NOT NULL);
ALTER TABLE WeightCategoryLogBookConversionSpecies ADD CONSTRAINT fk_WeightCategoryLogBookConversionSpecies_parent FOREIGN KEY (parent) REFERENCES WeightCategoryLogBookConversion(topiaid);
CREATE INDEX idx_WeightCategoryLogBookConversion_parent ON WeightCategoryLogBookConversionSpecies(parent);

CREATE TABLE WeightCategoryLogBookConversionSpecies_Species(weightCategoryLogBookConversionSpecies VARCHAR(255) NOT NULL, species VARCHAR(255) NOT NULL);
ALTER TABLE WeightCategoryLogBookConversionSpecies_Species ADD CONSTRAINT pk_WeightCategoryLogBookConversionS PRIMARY KEY (weightCategoryLogBookConversionSpecies,species);
ALTER TABLE WeightCategoryLogBookConversionSpecies_Species ADD CONSTRAINT fk_WeightCategoryLogBookConversionS_w FOREIGN KEY (weightCategoryLogBookConversionSpecies) REFERENCES WeightCategoryLogBookConversionSpecies(topiaid);
ALTER TABLE WeightCategoryLogBookConversionSpecies_Species ADD CONSTRAINT fk_WeightCategoryLogBookConversionS_s FOREIGN KEY (species) REFERENCES Species(topiaid);
