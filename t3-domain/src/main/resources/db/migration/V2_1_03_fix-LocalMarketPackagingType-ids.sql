---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Trick (we add 1000 to code, we will subtract them later)
INSERT INTO localmarketpackagingtype (topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES ('fr.ird.t3.entities.reference.LocalMarketPackagingType#1454009635523#0.1', 0, '2016-01-28 00:00:00', 1001, 'Vrac pesé ou non (multispécifique) (sondage)', true);
INSERT INTO localmarketpackagingtype (topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES ('fr.ird.t3.entities.reference.LocalMarketPackagingType#1454009635523#0.2', 0, '2016-01-28 00:00:00', 1002, 'Pesée (monospécifique) (pas de sondage)', true);
INSERT INTO localmarketpackagingtype (topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES ('fr.ird.t3.entities.reference.LocalMarketPackagingType#1454009635523#0.3', 0, '2016-01-28 00:00:00', 1003, 'Espèce unité (monospécifique) (pas de sondage)', true);
INSERT INTO localmarketpackagingtype (topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES ('fr.ird.t3.entities.reference.LocalMarketPackagingType#1454009635523#0.4', 0, '2016-01-28 00:00:00', 1004, 'Paquet non pesé (monospécifique) (pas de sondage)', true);
UPDATE Localmarketpackaging SET localmarketpackagingtype = replace(localmarketpackagingtype,'LocalMarketPackaging', 'LocalMarketPackagingType');
DELETE FROM Localmarketpackagingtype WHERE topiaid = 'fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.1';
DELETE FROM Localmarketpackagingtype WHERE topiaid = 'fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.2';
DELETE FROM Localmarketpackagingtype WHERE topiaid = 'fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.3';
DELETE FROM Localmarketpackagingtype WHERE topiaid = 'fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.4';
UPDATE LocalmarketpackagingType SET code = code -1000;
