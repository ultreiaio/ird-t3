---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE company RENAME COLUMN libelle TO label1;
ALTER TABLE country RENAME COLUMN libelle TO label1;
ALTER TABLE schoolType RENAME COLUMN libelle TO label1;
ALTER TABLE fishingContext RENAME COLUMN libelle TO label1;
ALTER TABLE ocean RENAME COLUMN libelle TO label1;
ALTER TABLE harbour RENAME COLUMN libelle TO label1;
ALTER TABLE sampleQuality RENAME COLUMN libelle TO label1;
ALTER TABLE sampleType RENAME COLUMN libelle TO label1;
ALTER TABLE species RENAME COLUMN libelle TO label1;
ALTER TABLE species RENAME COLUMN scientificlibelle TO scientificlabel;
ALTER TABLE vessel RENAME COLUMN libelle TO label1;
ALTER TABLE vesselSimpleType RENAME COLUMN libelle TO label1;
ALTER TABLE vesselType RENAME COLUMN libelle TO label1;
ALTER TABLE vesselActivity RENAME COLUMN libelle TO label1;
ALTER TABLE weightCategoryLogbook RENAME COLUMN libelle TO label1;
ALTER TABLE weightCategoryTreatment RENAME COLUMN libelle TO label1;
ALTER TABLE weightCategoryWellPlan RENAME COLUMN libelle TO label1;
ALTER TABLE weightCategorySample RENAME COLUMN libelle TO label1;
ALTER TABLE wellDestiny RENAME COLUMN libelle TO label1;
ALTER TABLE fpaZone RENAME COLUMN libelle TO label1;
ALTER TABLE elementaryLandingFate RENAME COLUMN libelle TO label1;
ALTER TABLE transmittingBuoyType RENAME COLUMN libelle TO label1;
ALTER TABLE objectType RENAME COLUMN libelle TO label1;
ALTER TABLE localMarketPackagingType RENAME COLUMN libelle TO label1;
ALTER TABLE localMarketPackaging RENAME COLUMN libelle TO label1;
ALTER TABLE ElementaryCatchFate RENAME COLUMN libelle TO label1;
ALTER TABLE zonecwp RENAME COLUMN versionlibelle TO versionLabel;
ALTER TABLE zoneet RENAME COLUMN libelle TO label1;
ALTER TABLE zoneet RENAME COLUMN versionlibelle TO versionLabel;
ALTER TABLE zoneee RENAME COLUMN libelle TO label1;
ALTER TABLE zoneee RENAME COLUMN versionlibelle TO versionLabel;
ALTER TABLE zonefao RENAME COLUMN libelle TO label1;
ALTER TABLE zonefao RENAME COLUMN versionlibelle TO versionLabel;
ALTER TABLE country ADD COLUMN label2 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label2 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label2 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label2 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label2 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE species ADD COLUMN label2 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label2 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label2 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label2 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label2 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label2 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label2 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label2 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label2 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label2 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label2 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label2 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label2 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label2 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label2 VARCHAR(255);
ALTER TABLE country ADD COLUMN label3 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label3 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label3 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label3 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label3 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE species ADD COLUMN label3 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label3 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label3 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label3 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label3 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label3 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label3 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label3 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label3 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label3 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label3 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label3 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label3 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label3 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label3 VARCHAR(255);
ALTER TABLE country ADD COLUMN label4 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label4 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label4 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label4 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label4 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE species ADD COLUMN label4 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label4 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label4 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label4 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label4 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label4 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label4 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label4 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label4 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label4 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label4 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label4 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label4 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label4 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label4 VARCHAR(255);
ALTER TABLE country ADD COLUMN label5 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label5 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label5 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label5 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label5 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE species ADD COLUMN label5 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label5 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label5 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label5 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label5 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label5 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label5 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label5 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label5 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label5 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label5 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label5 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label5 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label5 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label5 VARCHAR(255);
ALTER TABLE country ADD COLUMN label6 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label6 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label6 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label6 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label6 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE species ADD COLUMN label6 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label6 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label6 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label6 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label6 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label6 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label6 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label6 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label6 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label6 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label6 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label6 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label6 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label6 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label6 VARCHAR(255);
ALTER TABLE country ADD COLUMN label7 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label7 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label7 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label7 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label7 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE species ADD COLUMN label7 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label7 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label7 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label7 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label7 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label7 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label7 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label7 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label7 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label7 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label7 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label7 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label7 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label7 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label7 VARCHAR(255);
ALTER TABLE country ADD COLUMN label8 VARCHAR(255);
ALTER TABLE schoolType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE fishingContext ADD COLUMN label8 VARCHAR(255);
ALTER TABLE ocean ADD COLUMN label8 VARCHAR(255);
ALTER TABLE harbour ADD COLUMN label8 VARCHAR(255);
ALTER TABLE sampleQuality ADD COLUMN label8 VARCHAR(255);
ALTER TABLE sampleType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE species ADD COLUMN label8 VARCHAR(255);
ALTER TABLE vesselSimpleType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE vesselType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE vesselActivity ADD COLUMN label8 VARCHAR(255);
ALTER TABLE weightCategoryLogbook ADD COLUMN label8 VARCHAR(255);
ALTER TABLE weightCategoryTreatment ADD COLUMN label8 VARCHAR(255);
ALTER TABLE weightCategoryWellPlan ADD COLUMN label8 VARCHAR(255);
ALTER TABLE weightCategorySample ADD COLUMN label8 VARCHAR(255);
ALTER TABLE wellDestiny ADD COLUMN label8 VARCHAR(255);
ALTER TABLE fpaZone ADD COLUMN label8 VARCHAR(255);
ALTER TABLE elementaryLandingFate ADD COLUMN label8 VARCHAR(255);
ALTER TABLE transmittingBuoyType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE objectType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE localMarketPackagingType ADD COLUMN label8 VARCHAR(255);
ALTER TABLE localMarketPackaging ADD COLUMN label8 VARCHAR(255);
ALTER TABLE ElementaryCatchFate ADD COLUMN label8 VARCHAR(255);
ALTER TABLE zoneet ADD COLUMN label8 VARCHAR(255);
ALTER TABLE zoneee ADD COLUMN label8 VARCHAR(255);
ALTER TABLE zonefao ADD COLUMN label8 VARCHAR(255);
