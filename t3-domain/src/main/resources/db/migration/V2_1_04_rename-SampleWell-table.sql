---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
CREATE TABLE sampleSet (topiaid VARCHAR(255) NOT NULL CONSTRAINT pk_sampleSet PRIMARY KEY, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, weightedweight REAL, propweightedweight REAL, weightedweightminus10 REAL, weightedweightplus10 REAL, rftot REAL, totalsampleweight REAL, rfminus10 REAL, rfplus10 REAL, activity VARCHAR(255) CONSTRAINT fk_sampleSet_activity REFERENCES activity, rfminus10usagestatus VARCHAR(255), rfplus10usagestatus VARCHAR(255), sample VARCHAR(255) CONSTRAINT fk_sampleSet_sample REFERENCES sample); CREATE INDEX idx_sampleSet ON sampleSet(topiaid); INSERT INTO SampleSet SELECT replace(topiaId,'SampleWell', 'SampleSet'), topiaversion, topiacreatedate, weightedweight, propweightedweight, weightedweightminus10, weightedweightplus10, rftot, totalsampleweight, rfminus10, rfplus10, activity, rfminus10usagestatus, rfplus10usagestatus, sample FROM sampleWell;
ALTER TABLE samplesetspeciescatweight add COLUMN sampleSet VARCHAR(255);
ALTER TABLE samplesetspeciesfrequency add COLUMN sampleSet VARCHAR(255);
UPDATE samplesetspeciescatweight SET sampleSet= replace(samplewell,'SampleWell', 'SampleSet') where samplewell is not null;
UPDATE samplesetspeciesfrequency SET sampleSet= replace(samplewell,'SampleWell', 'SampleSet') where samplewell is not null;
ALTER TABLE samplesetspeciescatweight ADD CONSTRAINT fk_SampleSetSpeciesCatWeight_SampleSet FOREIGN KEY (sampleSet) REFERENCES sampleSet(topiaid);
ALTER TABLE samplesetspeciesfrequency ADD CONSTRAINT fk_SampleSetSpeciesFrequency_SampleSet FOREIGN KEY (sampleSet) REFERENCES sampleSet(topiaid);
ALTER TABLE samplesetspeciescatweight DROP COLUMN sampleWell;
ALTER TABLE samplesetspeciesfrequency DROP COLUMN sampleWell; drop TABLE SampleWell;
