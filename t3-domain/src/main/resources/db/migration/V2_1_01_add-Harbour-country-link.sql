---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE Harbour ADD COLUMN country VARCHAR(255);
ALTER TABLE Harbour ADD CONSTRAINT fk_harbour_Country FOREIGN KEY (country) REFERENCES Country(topiaid);

UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528740#0.8780508585333283' where code = 1;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.24216094850648695' where code = 2;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.11903016892439633' where code = 3;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 4;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 5;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 6;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113028#0.11467445384123376' where code = 7;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 8;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113029#0.8621983866234215' where code = 9;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396' where code = 10;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463' where code = 11;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463' where code = 14;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463' where code = 16;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113028#0.11467445384123376' where code = 27;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463' where code = 30;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463' where code = 31;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528740#0.04603851706526396' where code = 38;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528741#0.7360505737754338' where code = 42;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528741#0.6225105985304237' where code = 43;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 44;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113033#0.5474507294589145' where code = 45;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113037#0.6515538789983367' where code = 48;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528742#0.061432903319870724' where code = 50;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528742#0.4712204602899017' where code = 54;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528742#0.4712204602899017' where code = 55;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 57;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 58;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.5504627641283989' where code = 70;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528741#0.6193885943455354' where code = 80;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 95;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 96;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 97;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 98;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 99;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 100;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 101;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113056#0.03837967322542368' where code = 102;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 103;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 104;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528743#0.7311735564355363' where code = 105;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113041#0.6156662257157167' where code = 107;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528741#0.6908104250386179' where code = 108;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528738#0.29786909155244845' where code = 109;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528739#0.021872879219458463' where code = 111;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1427896113017#0.3956845303354538' where code = 112;
UPDATE Harbour set country = 'fr.ird.t3.entities.reference.Country#1297580528741#0.6193885943455354' where code = 113;

