---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528928#0.7684791138625932';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528928#0.7462510491346072';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528931#0.7340362909206456';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528929#0.700537735794819';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528929#0.07144864561003117';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528928#0.20978380101744742';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528933#0.3131099550330918';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528929#0.1831238826180489';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528930#0.27388207197386405';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.17';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.11';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.10';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.9';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1334600123513#0.12332082367823705';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.18';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528931#0.5663867925551074';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.16';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528929#0.23557122752694926';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528928#0.715358107654525';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528928#0.00830660499375091';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528929#0.030809936588289455';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.1';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528929#0.04293415202169404';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528933#0.6009126144931943';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528931#0.40837275149448937';

UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1460000000000#0.15';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528932#0.863042372573947';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528932#0.9991925653944878';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528932#0.7711735854917587';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528933#0.7511645427969551';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528933#0.17468417015421167';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528933#0.4894751387384305';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528934#0.17822915599428157';
UPDATE Harbour SET ocean='fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396' WHERE topiaId = 'fr.ird.t3.entities.reference.Harbour#1297580528932#0.21164070994041773';

UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.09848580496276282';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.4358849181824964';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.5213952546445324';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528932#0.05217800949850426';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528932#0.09458236901561745';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1464000000000#0.00115115';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1464000000000#0.00116116';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1464000000000#0.00117117';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528935#0.5032524703497515';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.3128607487452171';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.7077375683910743';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.37146946328714225';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.1776110119020825';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528930#0.10672736119748483';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528929#0.29263430155373604';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528932#0.23227601066196246';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528934#0.5223852489420004';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528929#0.037238377699795766';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528934#0.9698949751160373';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528933#0.2600646486222412';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528934#0.35235641408050444';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1297580528934#0.45885264373047663';
UPDATE Harbour SET ocean ='fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624' WHERE topiaId ='fr.ird.t3.entities.reference.Harbour#1460000000000#0.19';
