---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
CREATE TABLE Route( topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP NOT NULL, trip VARCHAR(255) NOT NULL, trip_idx INTEGER NOT NULL, date DATE NOT NULL, computedFishingTimeN0 REAL, computedSearchTimeN0 REAL);
CREATE UNIQUE INDEX uk_route ON Route(trip, date);
ALTER TABLE Route ADD CONSTRAINT fk_route_trip FOREIGN KEY (trip) REFERENCES Trip(topiaid);
CREATE INDEX idx_route_trip ON Route(trip);

ALTER TABLE Activity ADD COLUMN route VARCHAR(255);
ALTER TABLE Activity ADD COLUMN route_idx INTEGER;
ALTER TABLE Activity ADD CONSTRAINT fk_activity_route FOREIGN KEY (route) REFERENCES Route(topiaid);
CREATE INDEX idx_activity_route ON Activity(route);
