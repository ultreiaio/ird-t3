---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
CREATE TABLE LocalMarketPackagingType(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, code INTEGER, libelle VARCHAR(255) );
CREATE UNIQUE INDEX uk_LocalMarketPackagingType_code ON LocalMarketPackagingType(code);
CREATE TABLE LocalMarketPackaging(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, code INTEGER, libelle VARCHAR(255), harbour VARCHAR(255), localMarketPackagingType VARCHAR(255), startDate DATE, endDate DATE, weight REAL, CONSTRAINT fk_LocalMarketPackaging_harbour FOREIGN KEY (harbour ) REFERENCES harbour (topiaid), CONSTRAINT fk_LocalMarketPackaging_LocalMarketPackagingType FOREIGN KEY (localMarketPackagingType) REFERENCES LocalMarketPackagingType(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketPackagingType_code ON LocalMarketPackagingType(code);
CREATE UNIQUE INDEX uk_LocalMarketPackaging_code ON LocalMarketPackaging(code);
CREATE TABLE LocalMarketSurvey(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, surveyId VARCHAR(255), trip VARCHAR(255), species VARCHAR(255), proportion REAL, CONSTRAINT fk_LocalMarketSurvey_trip FOREIGN KEY (trip) REFERENCES Trip(topiaid), CONSTRAINT fk_LocalMarketSurvey_species FOREIGN KEY (species) REFERENCES Species(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketSurvey_businessKey ON LocalMarketSurvey(trip, surveyId);
CREATE TABLE LocalMarketBatch(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, batchId VARCHAR(255), count REAL, weight REAL, origin TEXT, comment TEXT, trip VARCHAR(255), species VARCHAR(255), localMarketPackaging VARCHAR(255), CONSTRAINT fk_LocalMarketBatch_trip FOREIGN KEY (trip) REFERENCES Trip(topiaid), CONSTRAINT fk_LocalMarketBatch_species FOREIGN KEY (species) REFERENCES Species(topiaid), CONSTRAINT fk_LocalMarketBatch_localMarketPackaging FOREIGN KEY (localMarketPackaging) REFERENCES LocalMarketPackaging(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketBatch_businessKey ON LocalMarketBatch(trip, batchId);
CREATE TABLE LocalMarketSample(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, sampleId VARCHAR(255), date DATE, trip VARCHAR(255), realVessel VARCHAR(255), CONSTRAINT fk_LocalMarketSample_trip FOREIGN KEY (trip) REFERENCES Trip(topiaid), CONSTRAINT fk_LocalMarketSample_realVessel FOREIGN KEY (realVessel ) REFERENCES Vessel(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketSample_businessKey ON LocalMarketSample(trip, sampleId);
CREATE TABLE LocalMarketSampleSpecies(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, ldlfFlag INTEGER, measuredCount REAL, localMarketSample VARCHAR(255), species VARCHAR(255), CONSTRAINT fk_LocalMarketSampleSpecies_localMarketSample FOREIGN KEY (localMarketSample) REFERENCES LocalMarketSample(topiaid), CONSTRAINT fk_LocalMarketSampleSpecies_species FOREIGN KEY (species) REFERENCES Species(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketSampleSpecies_businessKey ON LocalMarketSampleSpecies(localMarketSample, species, ldlfFlag);
CREATE TABLE LocalMarketSampleSpeciesFrequency(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, lengthClass REAL, number INTEGER, localMarketSampleSpecies VARCHAR(255), CONSTRAINT fk_LocalMarketSampleSpeciesFrequency_localMarketSampleSpecies FOREIGN KEY (localMarketSampleSpecies) REFERENCES LocalMarketSampleSpecies(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketSampleSpeciesFrequency_businessKey ON LocalMarketSampleSpeciesFrequency(localMarketSampleSpecies,lengthClass);
CREATE TABLE LocalMarketSampleWell(topiaid VARCHAR(255) PRIMARY KEY NOT NULL, topiaversion BIGINT NOT NULL, topiacreatedate TIMESTAMP, wellNumber INTEGER, wellPosition INTEGER, localMarketSample VARCHAR(255), CONSTRAINT fk_LocalMarketSampleWell_localMarketSample FOREIGN KEY (localMarketSample) REFERENCES LocalMarketSample(topiaid));
CREATE UNIQUE INDEX uk_LocalMarketSampleWell_businessKey ON LocalMarketSampleWell(localMarketSample, wellNumber, wellPosition);

CREATE INDEX idx_Trip_LocalMarketSurvey ON LocalMarketSurvey(trip);
CREATE INDEX idx_Trip_LocalMarketBatch ON LocalMarketBatch(trip);
CREATE INDEX idx_Trip_LocalMarketSample ON LocalMarketSample(trip);
CREATE INDEX idx_LocalMarketSample_LocalMarketSampleSpecies ON LocalMarketSampleSpecies(localMarketSample);
CREATE INDEX idx_LocalMarketSample_LocalMarketSampleWell ON LocalMarketSampleWell(localMarketSample);
CREATE INDEX idx_LocalMarketSampleSpecies_LocalMarketSampleSpeciesFrequency ON LocalMarketSampleSpeciesFrequency(localMarketSampleSpecies);
