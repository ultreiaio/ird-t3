/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package com.web_tomorrow.utils.suntimes;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class SunTimesTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SunTimesTest.class);

    @Test
    public void getDayDuration() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(Calendar.YEAR, 2001);
        cal.set(Calendar.MONTH, 9);
        cal.set(Calendar.DAY_OF_MONTH, 11);

        Date doomDay = cal.getTime();

        float x = 40.71337f;
        float y = -74.011459f;
        double dayDuration;
        dayDuration = SunTimes.getDayDurationInMonths(doomDay, x, y);
        if (log.isInfoEnabled()) {
            log.info("Day duration of the doom day " + dayDuration);
        }

        x = 47.196668f;
        y = -1.52607f;
        cal.set(Calendar.YEAR, 2011);
        cal.set(Calendar.MONTH, 9);
        cal.set(Calendar.DAY_OF_MONTH, 27);

        Date today = cal.getTime();
        dayDuration = SunTimes.getDayDurationInMonths(today, x, y);
        if (log.isInfoEnabled()) {
            log.info("Day duration of the current day " + dayDuration);
        }
    }
}
