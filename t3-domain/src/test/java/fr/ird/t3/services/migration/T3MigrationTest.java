package fr.ird.t3.services.migration;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaApplicationContextBuilder;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverter;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverterProvider;
import fr.ird.t3.entities.conversion.legacy.AbstractWeightCategoryLogBookConverter;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OABI;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OABL;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OABO;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OIBI;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OIBL;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OIBO;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OPBI;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OPBL;
import fr.ird.t3.entities.conversion.legacy.WeightCategoryLogBookConverterFOR_OPBO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanTopiaDao;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SchoolTypeTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversion;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpecies;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpeciesImpl;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionSpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentTopiaDao;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory;
import org.nuiton.util.DateUtil;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tchemit on 20/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Ignore
public class T3MigrationTest {

    private int count = 0;
    private SpeciesTopiaDao speciesDao;
    private WeightCategoryLogBookConversionTopiaDao weightCategoryLogBookConversionDao;
    private WeightCategoryLogBookConversionSpeciesTopiaDao weightCategoryLogBookConversionSpeciesDao;

    @Test
    public void migratePostgres() {
        try (T3TopiaApplicationContext rootContext = T3TopiaApplicationContextBuilder.forPgTest().addMigration().build()) {
            Assert.assertNotNull(rootContext);
        }
    }

    @Test
    public void generateWeightCategoryLogBookConversions() {

        try (T3TopiaApplicationContext rootContext = T3TopiaApplicationContextBuilder.forPgTest().build()) {

            ((BeanTopiaConfiguration) rootContext.getConfiguration()).setTopiaIdFactory(new LegacyTopiaIdFactory() {

                @Override
                public <E extends TopiaEntity> String newTopiaId(Class<E> entityClass, TopiaEntity topiaEntity) {
                    return entityClass.getName() + "#1519311532526#0" + count++;
                }
            });

            try (T3TopiaPersistenceContext persistenceContext = rootContext.newPersistenceContext()) {

                WeightCategoryLogBookConverterProvider converterProvider = WeightCategoryLogBookConverterProvider.newLegacyInstance(persistenceContext);

                OceanTopiaDao oceanDao = persistenceContext.getOceanDao();
                speciesDao = persistenceContext.getSpeciesDao();
                SchoolTypeTopiaDao schoolTypeDao = persistenceContext.getSchoolTypeDao();
                WeightCategoryTreatmentTopiaDao weightCategoryTreatmentDao = persistenceContext.getWeightCategoryTreatmentDao();
                weightCategoryLogBookConversionDao = persistenceContext.getWeightCategoryLogBookConversionDao();
                weightCategoryLogBookConversionSpeciesDao = persistenceContext.getWeightCategoryLogBookConversionSpeciesDao();

                weightCategoryLogBookConversionDao.deleteAll(weightCategoryLogBookConversionDao.findAll());
                persistenceContext.getHibernateSupport().getHibernateSession().flush();

                Date beginDate = DateUtil.createDate(1, 1, 1970);

                Ocean oceanA = oceanDao.forCodeEquals(1).findUnique();
                Ocean oceanI = oceanDao.forCodeEquals(2).findUnique();
                Ocean oceanP = oceanDao.forCodeEquals(5).findUnique();
                SchoolType schoolTypeBo = schoolTypeDao.forCodeEquals(1).findUnique();
                SchoolType schoolTypeBl = schoolTypeDao.forCodeEquals(2).findUnique();
                SchoolType schoolTypeBi = schoolTypeDao.forCodeEquals(3).findUnique();

                String version = "1.0";

                this.count = 0;

                // OA
                WeightCategoryLogBookConversion oaBi = addConversion(version, beginDate, oceanA, schoolTypeBi);
                WeightCategoryLogBookConversion oaBo = addConversion(version, beginDate, oceanA, schoolTypeBo);
                WeightCategoryLogBookConversion oaBl = addConversion(version, beginDate, oceanA, schoolTypeBl);

                // OI
                WeightCategoryLogBookConversion oiBi = addConversion(version, beginDate, oceanI, schoolTypeBi);
                WeightCategoryLogBookConversion oiBo = addConversion(version, beginDate, oceanI, schoolTypeBo);
                WeightCategoryLogBookConversion oiBl = addConversion(version, beginDate, oceanI, schoolTypeBl);

                // OP
                WeightCategoryLogBookConversion opBi = addConversion(version, beginDate, oceanP, schoolTypeBi);
                WeightCategoryLogBookConversion opBo = addConversion(version, beginDate, oceanP, schoolTypeBo);
                WeightCategoryLogBookConversion opBl = addConversion(version, beginDate, oceanP, schoolTypeBl);

                persistenceContext.commit();

                this.count = 0;

                //OA
                {
                    WeightCategoryLogBookConverterFOR_OABI converter = getConvertor(converterProvider, oaBi, WeightCategoryLogBookConverterFOR_OABI.class);
                    addEmptyConversionSpecies(oaBi, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(oaBi, converter.getMinus10Category(), 2);
                    addConversionSpecies(oaBi, converter, 1, 3, 4);
                }
                {
                    WeightCategoryLogBookConverterFOR_OABO converter = getConvertor(converterProvider, oaBo, WeightCategoryLogBookConverterFOR_OABO.class);
                    addEmptyConversionSpecies(oaBo, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(oaBo, converter.getMinus10Category(), 2);
                    addConversionSpecies(oaBo, converter, 1, 3, 4);
                }
                {
                    WeightCategoryLogBookConverterFOR_OABL converter = getConvertor(converterProvider, oaBl, WeightCategoryLogBookConverterFOR_OABL.class);
                    addEmptyConversionSpecies(oaBl, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(oaBl, converter.getMinus10Category(), 2);
                    addConversionSpecies(oaBl, converter, 1, 3, 4);
                }
                //OI
                {
                    WeightCategoryLogBookConverterFOR_OIBI converter = getConvertor(converterProvider, oiBi, WeightCategoryLogBookConverterFOR_OIBI.class);
                    addEmptyConversionSpecies(oiBi, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(oiBi, converter.getMinus10Category(), 2);
                    addConversionSpecies(oiBi, converter, 1, 3, 4);
                }
                {
                    WeightCategoryLogBookConverterFOR_OIBO converter = getConvertor(converterProvider, oiBo, WeightCategoryLogBookConverterFOR_OIBO.class);
                    addEmptyConversionSpecies(oiBo, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(oiBo, converter.getMinus10Category(), 2);
                    addConversionSpecies(oiBo, converter, 1, 3, 4);
                }
                {
                    WeightCategoryLogBookConverterFOR_OIBL converter = getConvertor(converterProvider, oiBl, WeightCategoryLogBookConverterFOR_OIBL.class);
                    addEmptyConversionSpecies(oiBl, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(oiBl, converter.getMinus10Category(), 2);
                    addConversionSpecies(oaBl, converter, 1, 3, 4);
                }
                //OP
                {
                    WeightCategoryLogBookConverterFOR_OPBI converter = getConvertor(converterProvider, opBi, WeightCategoryLogBookConverterFOR_OPBI.class);
                    addEmptyConversionSpecies(opBi, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(opBi, converter.getMinus10Category(), 2);
                    addConversionSpecies(opBi, converter, 1, 3, 4);
                }
                {
                    WeightCategoryLogBookConverterFOR_OPBO converter = getConvertor(converterProvider, opBo, WeightCategoryLogBookConverterFOR_OPBO.class);
                    addEmptyConversionSpecies(opBo, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(opBo, converter.getMinus10Category(), 2);
                    addConversionSpecies(opBo, converter, 1, 3, 4);
                }
                {
                    WeightCategoryLogBookConverterFOR_OPBL converter = getConvertor(converterProvider, opBl, WeightCategoryLogBookConverterFOR_OPBL.class);
                    addEmptyConversionSpecies(opBl, converter.getUnkwownCategory());
                    addEmptyConversionSpecies(opBl, converter.getMinus10Category(), 2);
                    addConversionSpecies(opBl, converter, 1, 3, 4);
                }
                persistenceContext.commit();
            }
        }
    }

    private void addConversionSpecies(WeightCategoryLogBookConversion conversion, AbstractWeightCategoryLogBookConverter converter, Integer... speciesCode) {
        List<Species> species = speciesDao.forCodeIn(Stream.of(speciesCode).collect(Collectors.toSet())).findAll();
        Map<String, Map<Integer, Float>> distributionsToSave = converter.getDistributionsByIds();
        WeightCategoryLogBookConversionSpecies conversionSpecies = weightCategoryLogBookConversionSpeciesDao.createByNotNull(WeightCategoryLogBookConversionSpeciesImpl.toString(distributionsToSave), conversion);
        conversionSpecies.setSpecies(new HashSet<>(species));
        conversion.addDistributions(conversionSpecies);
    }

    private void addEmptyConversionSpecies(WeightCategoryLogBookConversion conversion, WeightCategoryTreatment weightCategoryTreatment, Integer... speciesCode) {
        List<Species> species = speciesDao.forCodeIn(Stream.of(speciesCode).collect(Collectors.toSet())).findAll();
        Map<String, Map<Integer, Float>> distributionsToSave = new LinkedHashMap<>();
        distributionsToSave.put(weightCategoryTreatment.getTopiaId(), Collections.emptyMap());
        WeightCategoryLogBookConversionSpecies conversionSpecies = weightCategoryLogBookConversionSpeciesDao.createByNotNull(WeightCategoryLogBookConversionSpeciesImpl.toString(distributionsToSave), conversion);
        conversionSpecies.setSpecies(new HashSet<>(species));
        conversion.addDistributions(conversionSpecies);
    }


    private <C extends WeightCategoryLogBookConverter> C getConvertor(WeightCategoryLogBookConverterProvider converterProvider, WeightCategoryLogBookConversion conversion, Class<C> converterType) {
        return converterType.cast(converterProvider.getConverter(conversion.getOcean(), conversion.getSchoolType()));
    }

    private WeightCategoryLogBookConversion addConversion(String version, Date beginDate, Ocean ocean, SchoolType schoolType) {
        return weightCategoryLogBookConversionDao.createByNotNull(version, beginDate, ocean, schoolType);
    }
}
