/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * tests the {@link LengthWeightConversionWithContextCache}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class LengthWeightConversionWithContextCacheTest extends AbstractDatabaseTest {

    @Before
    public void setUp() {
        db.loadClassPathScript("/db/LengthWeightConversionHelperTest.sql");
    }

    @Test
    public void testGetConversionsAtlantique() {

        T3TopiaPersistenceContext tx = beginTransaction();

        Ocean ocean = tx.getOceanDao().forCodeEquals(1).findUnique();
        Assert.assertNotNull(ocean);
        testConversionForOcean(tx, ocean);

    }

    @Test
    public void testGetConversionsIndian() {

        T3TopiaPersistenceContext tx = beginTransaction();

        Ocean ocean = tx.getOceanDao().forCodeEquals(2).findUnique();
        Assert.assertNotNull(ocean);
        testConversionForOcean(tx, ocean);

    }

    private void testConversionForOcean(T3TopiaPersistenceContext tx, Ocean ocean) throws TopiaException {
        Date date = T3Date.newDate(1, 2010).toBeginDate();

        LengthWeightConversionWithContextCache conversionHelper = tx.newLengthWeightConversionWithContextCache(ocean, date);
        Assert.assertNotNull(conversionHelper);

        SpeciesTopiaDao speciesDAO = tx.getSpeciesDao();
        List<Species> allSpecies = speciesDAO.findAll();
        for (Species species : allSpecies) {

            LengthWeightConversion conversion =
                    conversionHelper.getConversions(species);

            // only species with code in [1-6] have a convertor
            if (species.getCode() > 6) {

                // no convertor
                Assert.assertNull(conversion);
            } else {

                Assert.assertNotNull(conversion);

                // try to convert

                conversion.computeLength(1);
                conversion.computeWeight(100);

                WeightCategoryTreatmentTopiaDao weightCategoryTreatmentDAO =
                        tx.getWeightCategoryTreatmentDao();

                List<SchoolType> schoolTypes = new ArrayList<>();
                schoolTypes.add(tx.getSchoolTypeDao().forCodeEquals(1).findUnique());
                schoolTypes.add(tx.getSchoolTypeDao().forCodeEquals(2).findUnique());

                for (SchoolType schoolType : schoolTypes) {

                    List<WeightCategoryTreatment> allCategories =
                            weightCategoryTreatmentDAO.forProperties(
                                    WeightCategoryTreatment.PROPERTY_OCEAN, ocean,
                                    WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE, schoolType
                            ).findAll();

                    Assert.assertNotNull(allCategories);
                    Assert.assertFalse(allCategories.isEmpty());

                    for (WeightCategoryTreatment category : allCategories) {
                        conversionHelper.getSpecieHighestLengthClass(conversion, category.getMax());
                    }

                    Range<Integer> open = Range.open(0, 1000);
                    List<Integer> lengthClasses = Lists.newArrayList(ContiguousSet.create(open, DiscreteDomain.integers()));

                    Map<Integer, WeightCategoryTreatment> distribution =
                            conversionHelper.getWeightCategoriesDistribution(conversion, allCategories, lengthClasses);
                    Assert.assertNotNull(distribution);
                    Set<WeightCategoryTreatment> usedweithCategories = Sets.newHashSet(distribution.values());
                    // all categories must be used (except the undefined one
                    Assert.assertEquals(allCategories.size() - 1, usedweithCategories.size());
                }
            }

        }
    }
}
