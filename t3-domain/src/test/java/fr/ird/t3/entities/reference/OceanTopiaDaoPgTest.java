package fr.ird.t3.entities.reference;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.test.T3PostgresqlDatabase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

/**
 * Created by tchemit on 09/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class OceanTopiaDaoPgTest {

    private static final Log log = LogFactory.getLog(OceanTopiaDaoPgTest.class);

    @Rule
    public final T3PostgresqlDatabase db = new T3PostgresqlDatabase();

    @Test
    public void findOceanByHarbour() {
        int wrongOcean = 0;
        int fixOcean = 0;
        StringBuilder queryBuilder = new StringBuilder();
        try (T3TopiaPersistenceContext persistenceContext = db.beginTransaction()) {
            OceanTopiaDao oceanDao = persistenceContext.getOceanDao();
            for (Harbour harbour : persistenceContext.getHarbourDao().findAll()) {
                Ocean oceanByHarbour = oceanDao.findOceanByHarbour(harbour);
                if (oceanByHarbour == null) {
                    log.warn(String.format("Harbour %s [%s] can't find ocean by method.", harbour.getLabel1(), harbour.getTopiaId()));
                    continue;
                }
                if (harbour.getOcean() == null) {
                    log.warn(String.format("Harbour %s [%s] with no ocean, can use now %s [%s]", harbour.getLabel1(), harbour.getTopiaId(), oceanByHarbour.getLabel1(), oceanByHarbour.getTopiaId()));
                    queryBuilder.append(String.format("UPDATE Harbour SET ocean ='%s' WHERE topiaId ='%s';\n", oceanByHarbour.getTopiaId(), harbour.getTopiaId()));
                    fixOcean++;
                } else {
                    try {
                        Assert.assertEquals(String.format("Wrong ocean for %s", harbour.getLabel1()), harbour.getOcean().getLabel1(), oceanByHarbour.getLabel1());
                    } catch (Throwable e) {
                        log.error(e.getMessage());
                        wrongOcean++;
                    }
                }
            }
        }

        if (queryBuilder.length() > 0) {
            log.warn("You can update your database with thoses queries to fill missing harbour ocean:\n" + queryBuilder.toString());
        }
        log.info("Number of wrong oceans: " + wrongOcean);
        log.info("Number of fixed oceans: " + fixOcean);
    }
}
