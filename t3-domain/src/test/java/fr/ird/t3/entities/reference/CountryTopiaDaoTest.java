/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3PersistenceFixtures;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

/**
 * Test the user dao {@link AbstractCountryTopiaDao}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class CountryTopiaDaoTest extends AbstractDatabaseTest {

    private Country country;
    private Vessel vessel;
    private Vessel vessel2;
    private Ocean ocean;
    private T3TopiaPersistenceContext tx;
    private CountryTopiaDao dao;

    @Before
    public void setUp() {
        tx = beginTransaction();
        dao = tx.getCountryDao();
        country = T3PersistenceFixtures.newCountry(tx, 0);
        Country country2 = T3PersistenceFixtures.newCountry(tx, 1);
        vessel = T3PersistenceFixtures.newVessel(tx, country, 0);
        vessel2 = T3PersistenceFixtures.newVessel(tx, country2, 1);
        ocean = T3PersistenceFixtures.newOcean(tx, 0);
    }

    @Test
    public void findAllFleetUsedInTrip() {

        T3PersistenceFixtures.newTrip(tx, vessel);

        Set<Country> result = tx.getCountryDao().findAllFleetUsedInTrip(null);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());
    }

    @Test
    public void findAllFleetUsedInCatch() {

        Trip trip = T3PersistenceFixtures.newTrip(tx, vessel);

        // create another trip without any activity (so without also any corrected catches)
        T3PersistenceFixtures.newTrip(tx, vessel2);

        Activity activity = T3PersistenceFixtures.newActivity(tx, ocean);
        trip.addActivity(activity);

        CorrectedElementaryCatch catche = tx.getCorrectedElementaryCatchDao().create();
        activity.addCorrectedElementaryCatch(catche);

        Set<Country> result = dao.findAllFleetUsedInCatch();
        Assert.assertNotNull(result);
        //FIXME Should be one when we will fix the request...
//        Assert.assertEquals(1, result.size());
        Assert.assertEquals(2, result.size());
//        Assert.assertEquals(country, result.iterator().next());
    }

    @Test
    public void findAllFleetUsedInSample() {

        Trip trip = T3PersistenceFixtures.newTrip(tx, vessel);


        // create another trip without any activity (so without also any corrected catches)
        T3PersistenceFixtures.newTrip(tx, vessel2);

        Activity activity = T3PersistenceFixtures.newActivity(tx, ocean);
        trip.addActivity(activity);

        Set<Country> result;

        result = dao.findAllFleetUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);

        //FIXME Should be zero when we will fix the request...
//        Assert.assertEquals(0, result.size());
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());

//        SetSpeciesFrequency f =
//                tx.getSetSpeciesFrequencyDAO(tx).create(SetSpeciesFrequency.PROPERTY_ACTIVITY, activity);

        result = dao.findAllFleetUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(country, result.iterator().next());
    }

    @Test
    public void findAllFlagUsedInSample() {

        Trip trip = T3PersistenceFixtures.newTrip(tx, vessel);

        // create another trip without any activity (so withou also any corrected catches)
        T3PersistenceFixtures.newTrip(tx, vessel2);

        Activity activity = T3PersistenceFixtures.newActivity(tx, ocean);
        trip.addActivity(activity);

        Set<Country> result = dao.findAllFlagUsedInSample(ocean.getTopiaId());
        Assert.assertNotNull(result);
        //FIXME Should be zero when we will fix the request...
//        Assert.assertEquals(0, result.size());
        Assert.assertEquals(1, result.size());

//        SetSpeciesFrequency f =
//                tx.getSetSpeciesFrequencyDAO(tx).create(SetSpeciesFrequency.PROPERTY_ACTIVITY, activity);

//        result = dao.findAllFlagUsedInSample(ocean.getTopiaId());
//        Assert.assertNotNull(result);
//        Assert.assertEquals(1, result.size());
//        Assert.assertEquals(country, result.iterator().next());
    }

}
