/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * To test generated entities.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3DAOHelperTest {

    @Test
    public void testEntities() {
        T3EntityEnum[] entityEnums = T3EntityEnum.values();
        for (T3EntityEnum entityEnum : entityEnums) {
            Class<? extends TopiaEntity> contract = entityEnum.getContract();
            Class<? extends TopiaEntity> implementation = entityEnum.getImplementation();
            Assert.assertTrue(contract.isAssignableFrom(implementation));
        }
    }
}
