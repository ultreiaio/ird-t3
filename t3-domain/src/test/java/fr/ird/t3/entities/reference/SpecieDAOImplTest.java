/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3PersistenceFixtures;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Test the user dao {@link AbstractSpeciesTopiaDao}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class SpecieDAOImplTest extends AbstractDatabaseTest {

    @Test
    public void findAllSpecieUsedInCatch() {

        T3TopiaPersistenceContext tx = beginTransaction();

        SpeciesTopiaDao dao = tx.getSpeciesDao();

        Species species = T3PersistenceFixtures.newSpecies(tx, 0);
        T3PersistenceFixtures.newSpecies(tx, 1);

        Trip trip = T3PersistenceFixtures.newTrip(tx);

        // create another trip without any activity (so without also any corrected catches)
        T3PersistenceFixtures.newTrip(tx, T3PersistenceFixtures.newVessel(tx, 0));

        Activity activity = T3PersistenceFixtures.newActivity(tx, T3PersistenceFixtures.newOcean(tx));
        trip.addActivity(activity);
        activity.addCorrectedElementaryCatch(tx.getCorrectedElementaryCatchDao().create(CorrectedElementaryCatch.PROPERTY_SPECIES, species));

        Set<Species> result = dao.findAllSpeciesUsedInCatch();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(species, result.iterator().next());
    }

}
