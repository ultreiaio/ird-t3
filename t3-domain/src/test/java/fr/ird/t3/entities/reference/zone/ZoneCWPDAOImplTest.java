/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference.zone;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Tests the  {@link ZoneCWPTopiaDao}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ZoneCWPDAOImplTest extends AbstractDatabaseTest {

    public static final String VERSION_ID = "v0";

    public static final String VERSION_ID_2 = "v2";

    public static final String VERSION_LIBELLE = "libelle - v0";

    public static final String VERSION_LIBELLE_2 = "libelle - v2";

    public static final Date VERSION_START_DATE = new Date();

    public static final Date VERSION_START_DATE_2 = new Date();

    public static final Date VERSION_END_DATE_2 = new Date();

    @Test
    public void findAllVersion() {

        T3TopiaPersistenceContext tx = beginTransaction();

        ZoneCWPTopiaDao dao = tx.getZoneCWPDao();

        ZoneCWP zone1 = createZone(dao);
        ZoneCWP zone1bis = createZone(dao);
        ZoneCWP zone2 = createZone2(dao);
        ZoneCWP zone2bis = createZone2(dao);

        List<ZoneCWP> zones = Arrays.asList(zone1, zone2);

        List<ZoneVersion> result = dao.findAllVersion();

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());

        for (ZoneCWP zone : zones) {
            ZoneVersion zoneVersion = zone.toZoneVersion();
            Assert.assertTrue(result.contains(zoneVersion));
        }

    }

    @Test
    public void findVersionByVersionId() {

        T3TopiaPersistenceContext tx = beginTransaction();

        ZoneCWPTopiaDao dao = tx.getZoneCWPDao();

        ZoneCWP zone1 = createZone(dao);
        ZoneCWP zone1bis = createZone(dao);
        ZoneCWP zone2 = createZone2(dao);
        ZoneCWP zone2bis = createZone2(dao);


        ZoneVersion result;

        result = dao.findVersionByVersionId(VERSION_ID);
        Assert.assertNotNull(result);
        Assert.assertEquals(VERSION_ID, result.getVersionId());
        Assert.assertEquals(VERSION_LIBELLE, result.getVersionLabel());
        Assert.assertEquals(VERSION_START_DATE, result.getVersionStartDate());
        Assert.assertEquals(null, result.getVersionEndDate());

        result = dao.findVersionByVersionId(VERSION_ID_2);
        Assert.assertNotNull(result);
        Assert.assertEquals(VERSION_ID_2, result.getVersionId());
        Assert.assertEquals(VERSION_LIBELLE_2, result.getVersionLabel());
        Assert.assertEquals(VERSION_START_DATE_2, result.getVersionStartDate());
        Assert.assertEquals(VERSION_END_DATE_2, result.getVersionEndDate());

    }

    protected ZoneCWP createZone(ZoneCWPTopiaDao dao) throws TopiaException {
        return dao.create(ZoneImpl.PROPERTY_VERSION_ID, VERSION_ID,
                          ZoneImpl.PROPERTY_VERSION_LABEL, VERSION_LIBELLE,
                          ZoneImpl.PROPERTY_VERSION_START_DATE, VERSION_START_DATE);
    }

    protected ZoneCWP createZone2(ZoneCWPTopiaDao dao) throws TopiaException {
        return dao.create(ZoneImpl.PROPERTY_VERSION_ID, VERSION_ID_2,
                          ZoneImpl.PROPERTY_VERSION_LABEL, VERSION_LIBELLE_2,
                          ZoneImpl.PROPERTY_VERSION_START_DATE, VERSION_START_DATE_2,
                          ZoneImpl.PROPERTY_VERSION_END_DATE, VERSION_END_DATE_2);
    }

}
