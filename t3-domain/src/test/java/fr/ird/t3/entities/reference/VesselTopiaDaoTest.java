/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3PersistenceFixtures;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Test the user dao {@link AbstractVesselTopiaDao}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class VesselTopiaDaoTest extends AbstractDatabaseTest {

    @Test
    public void findAllUsedInTrip() {

        T3TopiaPersistenceContext tx = beginTransaction();

        Vessel vessel = T3PersistenceFixtures.newVessel(tx, 1);
        T3PersistenceFixtures.newVessel(tx, 2);

        Trip trip = T3PersistenceFixtures.newTrip(tx, vessel);
        Activity activity = T3PersistenceFixtures.newActivity(tx, T3PersistenceFixtures.newOcean(tx));
        trip.addActivity(activity);
        Set<Vessel> result = tx.getVesselDao().findAllUsedInTrip();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(vessel, result.iterator().next());
    }

}
