package fr.ird.t3.entities;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.VesselType;

import java.util.Date;
import java.util.Objects;

/**
 * Created by tchemit on 20/02/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3PersistenceFixtures {

    public static Activity newActivity(T3TopiaPersistenceContext tx, Ocean ocean) {
        return newActivity(tx, ocean, new Date());
    }

    public static Ocean newOcean(T3TopiaPersistenceContext tx) {
        return newOcean(tx, 0);
    }

    public static Ocean newOcean(T3TopiaPersistenceContext tx, int code) {
        return tx.getOceanDao().create(Ocean.PROPERTY_CODE, code);
    }

    public static Country newCountry(T3TopiaPersistenceContext tx, int code) {
        return tx.getCountryDao().create(Country.PROPERTY_CODE, code);
    }

    public static Activity newActivity(T3TopiaPersistenceContext tx, Ocean ocean, Date date) {
        return Objects.requireNonNull(tx.getActivityDao()).create(Activity.PROPERTY_OCEAN, Objects.requireNonNull(ocean), Activity.PROPERTY_DATE, Objects.requireNonNull(date));
    }

    public static Trip newTrip(T3TopiaPersistenceContext tx) {
        return tx.getTripDao().create();
    }

    public static Trip newTrip(T3TopiaPersistenceContext tx, Vessel vessel) {
        return tx.getTripDao().create(Trip.PROPERTY_VESSEL, vessel);
    }

    public static Trip newTrip(T3TopiaPersistenceContext tx, Harbour harbour) {
        return tx.getTripDao().create(Trip.PROPERTY_LANDING_HARBOUR, harbour);
    }

    public static Trip newTrip(T3TopiaPersistenceContext tx, int code, Date date) {
        return tx.getTripDao().create(Trip.PROPERTY_CODE, code, Trip.PROPERTY_LANDING_DATE, date);
    }

    public static Trip newTrip(T3TopiaPersistenceContext tx, int code, Date date, TripType tripType) {
        return tx.getTripDao().create(Trip.PROPERTY_CODE, code, Trip.PROPERTY_LANDING_DATE, date, Trip.PROPERTY_TRIP_TYPE, tripType);
    }

    public static Harbour newHarbour(T3TopiaPersistenceContext tx, int code) {
        return tx.getHarbourDao().create(Harbour.PROPERTY_CODE, code);
    }

    public static Species newSpecies(T3TopiaPersistenceContext tx, int code) {
        return tx.getSpeciesDao().create(Species.PROPERTY_CODE, code);
    }

    public static Vessel newVessel(T3TopiaPersistenceContext tx, int code) {
        return tx.getVesselDao().create(Vessel.PROPERTY_CODE, code);
    }

    public static VesselType newVesselType(T3TopiaPersistenceContext tx, int code, VesselSimpleType vesselSimpleType) {
        return tx.getVesselTypeDao().create(VesselType.PROPERTY_CODE, code, VesselType.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType);
    }

    public static VesselSimpleType newVesselSimpleType(T3TopiaPersistenceContext tx, int code) {
        return tx.getVesselSimpleTypeDao().create(VesselSimpleType.PROPERTY_CODE, code);
    }

    public static Vessel newVessel(T3TopiaPersistenceContext tx, Country country, int code) {
        return tx.getVesselDao().create(Vessel.PROPERTY_FLAG_COUNTRY, country, Vessel.PROPERTY_FLEET_COUNTRY, country, Vessel.PROPERTY_CODE, code);
    }

    public static Vessel newVessel(T3TopiaPersistenceContext tx, VesselType vesselType, int code) {
        return tx.getVesselDao().create(Vessel.PROPERTY_VESSEL_TYPE, vesselType, Vessel.PROPERTY_CODE, code);
    }
}
