package fr.ird.t3.entities.cache;

/*-
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.zone.ZoneETMeta;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.test.T3PostgresqlDatabase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

/**
 * Created by tchemit on 23/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ZoneStratumCacheTest {

    @ClassRule
    public static final T3PostgresqlDatabase db = new T3PostgresqlDatabase();

    private ZoneStratumCache cache;
    private T3TopiaPersistenceContext persistenceContext;

    @Before
    public void setUp() {
        persistenceContext = db.beginTransaction();
        cache = persistenceContext.newZoneStratumCache(new ZoneETMeta(), "v2011");
    }

    @Test
    public void forOceansAndSchoolType() {
        List<Ocean> oceans = persistenceContext.getOceanDao().findAll();
        List<SchoolType> schoolTypes = persistenceContext.getSchoolTypeDao().findAll();
        for (SchoolType schoolType : schoolTypes) {
            List<ZoneStratumAware> zones = cache.forOceansAndSchoolType(oceans, schoolType);
            Assert.assertNotNull(zones);
            Assert.assertFalse(zones.isEmpty());
        }
    }
}
