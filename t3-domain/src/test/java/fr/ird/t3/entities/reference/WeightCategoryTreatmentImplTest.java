/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * To test not generated methods of {@link WeightCategoryTreatmentImpl}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class WeightCategoryTreatmentImplTest extends AbstractDatabaseTest {

    @Test
    public void testGetOldCategoryCode() {
        db.loadClassPathScript("/db/WeightCategoryTreatmentImplTest.sql");

        try (T3TopiaPersistenceContext tx = beginTransaction()) {
            WeightCategoryTreatmentTopiaDao dao = tx.getWeightCategoryTreatmentDao();
            List<WeightCategoryTreatment> all = dao.findAll();
            for (WeightCategoryTreatment weightCategoryTreatment : all) {
                weightCategoryTreatment.getOldCategoryCode();
            }
        }
    }

    @Test
    public void testWeightCategoryTreatmentComparator() {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLabel1("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLabel1("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLabel1("unbound");
        catUnbound.setMin(10);
        catUnbound.setMax(null);

        List<WeightCategoryTreatment> list = new ArrayList<>();
        list.add(catUnbound);
        list.add(catInconnu);
        list.add(catBound);

        WeightCategories.sort(list);

        Assert.assertEquals(list.get(0), catBound);
        Assert.assertEquals(list.get(1), catUnbound);
        Assert.assertEquals(list.get(2), catInconnu);
    }

    @Test
    public void testWeightCategoryTreatmentComparator2() {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLabel1("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLabel1("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catBound2 = new WeightCategoryTreatmentImpl();
        catBound2.setLabel1("bound2");
        catBound2.setMin(10);
        catBound2.setMax(30);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLabel1("unbound");
        catUnbound.setMin(30);
        catUnbound.setMax(null);

        List<WeightCategoryTreatment> list = new ArrayList<>();
        list.add(catBound2);
        list.add(catUnbound);
        list.add(catInconnu);
        list.add(catBound);

        Comparator<WeightCategory> comparator =WeightCategories.newComparator();

        int compare;

        compare = comparator.compare(catBound, catBound2);
        Assert.assertTrue(compare < 0);

        compare = comparator.compare(catBound, catUnbound);
        Assert.assertTrue(compare < 0);

        compare = comparator.compare(catBound2, catUnbound);
        Assert.assertTrue(compare < 0);

        WeightCategories.sort(list);

        Assert.assertEquals(list.get(0), catBound);
        Assert.assertEquals(list.get(1), catBound2);
        Assert.assertEquals(list.get(2), catUnbound);
        Assert.assertEquals(list.get(3), catInconnu);
    }

    @Test
    public void getWeightCategoryTreatment() {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLabel1("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLabel1("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLabel1("unbound");
        catUnbound.setMin(10);
        catUnbound.setMax(null);

        Map<WeightCategoryTreatment, Integer> weightCategories =
                new LinkedHashMap<>();

        weightCategories.put(catBound, 20);
        weightCategories.put(catUnbound, Integer.MAX_VALUE);
        weightCategories.put(catInconnu, Integer.MAX_VALUE);

        WeightCategoryTreatment actual, expected;

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 0);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 19);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 20);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 100);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getWeightCategoryTreatment2() {

        WeightCategoryTreatment catInconnu = new WeightCategoryTreatmentImpl();

        catInconnu.setLabel1("inconnu");
        catInconnu.setMin(null);
        catInconnu.setMax(null);

        WeightCategoryTreatment catBound = new WeightCategoryTreatmentImpl();
        catBound.setLabel1("bound");
        catBound.setMin(0);
        catBound.setMax(10);

        WeightCategoryTreatment catBound2 = new WeightCategoryTreatmentImpl();
        catBound.setLabel1("bound2");
        catBound.setMin(10);
        catBound.setMax(30);

        WeightCategoryTreatment catUnbound = new WeightCategoryTreatmentImpl();
        catUnbound.setLabel1("unbound");
        catUnbound.setMin(30);
        catUnbound.setMax(null);

        Map<WeightCategoryTreatment, Integer> weightCategories =
                new LinkedHashMap<>();

        weightCategories.put(catBound, 20);
        weightCategories.put(catBound2, 40);
        weightCategories.put(catUnbound, Integer.MAX_VALUE);
        weightCategories.put(catInconnu, Integer.MAX_VALUE);

        WeightCategoryTreatment actual, expected;

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 0);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 19);
        expected = catBound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 20);
        expected = catBound2;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 39);
        expected = catBound2;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 40);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);

        actual = WeightCategoryTreatmentTopiaDao.getWeightCategoryTreatment(weightCategories, 100);
        expected = catUnbound;

        Assert.assertEquals(expected, actual);
    }
}
