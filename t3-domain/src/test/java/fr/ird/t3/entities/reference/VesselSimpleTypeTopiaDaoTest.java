/*
 * #%L
 * T3 :: Domain
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3PersistenceFixtures;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Test the user dao {@link AbstractVesselSimpleTypeTopiaDao}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class VesselSimpleTypeTopiaDaoTest extends AbstractDatabaseTest {

    @Test
    public void findAllUsedInTrip() {

        T3TopiaPersistenceContext tx = beginTransaction();

        VesselSimpleType vesselSimpleType = T3PersistenceFixtures.newVesselSimpleType(tx, 1);
        VesselSimpleType vesselSimpleType2 = T3PersistenceFixtures.newVesselSimpleType(tx, 2);

        VesselType vesselType = T3PersistenceFixtures.newVesselType(tx, 1, vesselSimpleType);
        VesselType vesselType2 = T3PersistenceFixtures.newVesselType(tx, 2, vesselSimpleType2);

        Vessel vessel = T3PersistenceFixtures.newVessel(tx, vesselType, 1);
        T3PersistenceFixtures.newVessel(tx, vesselType2, 2);

        T3PersistenceFixtures.newTrip(tx, vessel);
        Set<VesselSimpleType> result = tx.getVesselSimpleTypeDao().findAllUsedInTrip(null);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(vesselSimpleType, result.iterator().next());
    }

}
