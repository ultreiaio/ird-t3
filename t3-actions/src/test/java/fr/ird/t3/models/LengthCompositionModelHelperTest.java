/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import fr.ird.t3.FakeT3ServiceContext;
import fr.ird.t3.T3ActionFixtures;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * To test {@link LengthCompositionModelHelper}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class LengthCompositionModelHelperTest {

    private static final Log log = LogFactory.getLog(LengthCompositionModelHelperTest.class);

    @Rule
    public final FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected final T3ActionFixtures fixtures = new T3ActionFixtures();

    @Test
    public void testDecorateModel() {

        Species species1 = fixtures.species1();
        Species species2 = fixtures.species2();

        Map<Species, LengthCompositionAggregateModel> models = new HashMap<>();

        LengthCompositionAggregateModel model;
        LengthCompositionModel model1;
        LengthCompositionModel model2;
        LengthCompositionModel model3;
        Map<Integer, Float> lengths;

        model = new LengthCompositionAggregateModel();
        model1 = new LengthCompositionModel(fixtures.weightCategoryTreatment1());
        lengths = new HashMap<>();
        lengths.put(1, 1.f);
        lengths.put(2, 2.f);
        model1.addValues(lengths);
        model.addModel(model1);

        model2 = new LengthCompositionModel(fixtures.weightCategoryTreatment2());
        lengths = new HashMap<>();
        lengths.put(3, 2.f);
        lengths.put(4, 9.f);
        model2.addValues(lengths);
        model.addModel(model2);

        models.put(species1, model);

        model = new LengthCompositionAggregateModel();
        model1 = new LengthCompositionModel(fixtures.weightCategoryTreatment1());
        lengths = new HashMap<>();
        lengths.put(1, 11.f);
        lengths.put(2, 2.f);
        model1.addValues(lengths);
        model.addModel(model1);

        model2 = new LengthCompositionModel(fixtures.weightCategoryTreatment2());
        lengths = new HashMap<>();
        lengths.put(4, 1.f);
        lengths.put(5, 22.f);
        model2.addValues(lengths);
        model.addModel(model2);

        model3 = new LengthCompositionModel(fixtures.weightCategoryTreatment3());
        lengths = new HashMap<>();
        lengths.put(8, 61.f);
        lengths.put(9, 2.f);
        model3.addValues(lengths);
        model.addModel(model3);

        models.put(species2, model);

        DecoratorService decoratorService = serviceContext.newService(DecoratorService.class);

        String s = LengthCompositionModelHelper.decorateModel(decoratorService, "Mon titre", "Value titre", models);
        if (log.isInfoEnabled()) {
            log.info("Result :\n" + s);
        }
    }
}
