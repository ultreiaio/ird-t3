/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import fr.ird.t3.FakeT3ServiceContext;
import fr.ird.t3.T3ActionFixtures;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.junit.Test;

/**
 * To test {@link SpeciesCountModelHelper}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class SpeciesCountModelHelperTest {

    private static final Log log = LogFactory.getLog(SpeciesCountModelHelperTest.class);

    @Rule
    public final FakeT3ServiceContext serviceContext = new FakeT3ServiceContext();

    protected final T3ActionFixtures fixtures = new T3ActionFixtures();

    @Test
    public void testDecorateModel() {

        Species species1 = fixtures.species1();
        Species species2 = fixtures.species2();
        Species species3 = fixtures.species3();

        SpeciesCountAggregateModel model = new SpeciesCountAggregateModel();

        SpeciesCountModel speciesModel;

        speciesModel = new SpeciesCountModel(species1);
        speciesModel.addValue("beforeN3 Count", 1, 1f);
        speciesModel.addValue("beforeN3 Count", 2, 2f);
        speciesModel.addValue("beforeN3 Count", 10, 3f);
        speciesModel.addValue("beforeN3 Count", 20, 4f);
        speciesModel.addValue("afterN3 Count", 1, 2f);
        speciesModel.addValue("afterN3 Count", 2, 4f);
        speciesModel.addValue("afterN3 Count", 10, 6f);
        speciesModel.addValue("afterN3 Count", 20, 8f);
        model.addValues(speciesModel);

        speciesModel = new SpeciesCountModel(species2);
        speciesModel.addValue("beforeN3 Count", 31, 1f);
        speciesModel.addValue("beforeN3 Count", 2, 2f);
        speciesModel.addValue("beforeN3 Count", 10, 3f);
        speciesModel.addValue("beforeN3 Count", 20, 4f);
        speciesModel.addValue("afterN3 Count", 32, 2f);
        speciesModel.addValue("afterN3 Count", 2, 4f);
        speciesModel.addValue("afterN3 Count", 10, 6f);
        speciesModel.addValue("afterN3 Count", 20, 8f);
        model.addValues(speciesModel);

        speciesModel = new SpeciesCountModel(species3);
        speciesModel.addValue("beforeN3 Count", 10, 1f);
        speciesModel.addValue("beforeN3 Count", 22, 2f);
        speciesModel.addValue("beforeN3 Count", 10, 3f);
        speciesModel.addValue("beforeN3 Count", 20, 4f);
        speciesModel.addValue("afterN3 Count", 15, 2f);
        speciesModel.addValue("afterN3 Count", 24, 4f);
        speciesModel.addValue("afterN3 Count", 10, 6f);
        speciesModel.addValue("afterN3 Count", 20, 8f);
        model.addValues(speciesModel);

        String result = SpeciesCountModelHelper.decorateModel(
                serviceContext.newService(DecoratorService.class),
                "Mon titre",
                model
        );

        if (log.isInfoEnabled()) {
            log.info("Result :\n" + result);
        }
    }
}
