/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.fake.v0;

import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

public class T3OutputProviderFakeImpl implements T3OutputProvider<T3OutputOperationFakeImpl, T3OutputConfiguration> {

    private static final long serialVersionUID = 1L;

    public static final Version VERSION = Versions.valueOf("0");

    public static final String NAME = "Fake";

    public static final String ID = NAME + "__" + VERSION;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Version getVersion() {
        return VERSION;
    }

    @Override
    public String getOutputType() {
        return "SQL";
    }

    @Override
    public String getLibelle() {
        return getName() + " - v." + getVersion() + " (" + getOutputType() + ")";
    }

    @Override
    public T3OutputFakeImpl newInstance(T3OutputConfiguration configuration,
                                        T3Messager messager,
                                        T3ServiceContext serviceContext) {
        T3OutputFakeImpl output = new T3OutputFakeImpl(configuration, messager);
        output.setServiceContext(serviceContext);
        return output;
    }

    @Override
    public T3OutputOperationFakeImpl[] getOperations() {
        return T3OutputOperationFakeImpl.values();
    }

    @Override
    public T3OutputOperationFakeImpl getOperation(String operationId) {
        for (T3OutputOperationFakeImpl operation : getOperations()) {
            if (operationId.equals(operation.getId())) {
                return operation;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof T3OutputProvider)) return false;

        T3OutputProviderFakeImpl that = (T3OutputProviderFakeImpl) o;

        return ID.equals(that.getId());
    }

    @Override
    public int hashCode() {
        return ID.hashCode();
    }

}
