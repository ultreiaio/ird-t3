/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.fake.v0;

import com.google.common.collect.ImmutableSet;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.io.input.access.AbstractT3InputMSAccess;
import fr.ird.t3.io.input.access.T3AccessDataSource;
import fr.ird.t3.io.input.access.T3DataEntityVisitor;

import java.io.File;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since TODO
 */
public class T3InputFake0 extends AbstractT3InputMSAccess {

    @Override
    public T3AccessDataSource newDataSource(File inputFile) {
        return new T3AccessDataSource(inputFile, null);

    }

    @Override
    public T3DataEntityVisitor newDataVisitor(T3AccessDataSource dataSource,
                                              ReferenceEntityMap safeReferences) {
        return null;
    }

    @Override
    public ImmutableSet<T3EntityEnum> getReferenceTypes() {
        return null;
    }

    @Override
    public ImmutableSet<T3EntityEnum> getDataTypes() {
        return null;
    }
}
