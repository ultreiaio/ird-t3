/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.fake.v0;

import fr.ird.t3.io.output.T3OutputOperation;

import java.util.Locale;

public enum T3OutputOperationFakeImpl implements T3OutputOperation {

    /** To export trips and elementary landings. */
    TRIP_AND_LANDING,
    /** to export activities and catches. */
    ACTIVITY_AND_CATCHES,
    /** To export samples. */
    SAMPLE;

    @Override
    public String getId() {
        return name();
    }

    @Override
    public String getLibelle(Locale locale) {
        return name();
    }

}
