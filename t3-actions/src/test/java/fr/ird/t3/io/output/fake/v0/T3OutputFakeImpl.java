/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.fake.v0;

import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.output.T3Output;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.services.T3ServiceSupport;

import java.util.List;
import java.util.Locale;

public class T3OutputFakeImpl extends T3ServiceSupport implements T3Output<T3OutputOperationFakeImpl, T3OutputConfiguration> {

    protected final T3OutputConfiguration configuration;
    private final T3Messager messager;
    protected Locale locale;

    T3OutputFakeImpl(T3OutputConfiguration configuration, T3Messager messager) {
        this.configuration = configuration;
        this.messager = messager;
    }

    @Override
    public T3OutputConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public String executeOperation(List<Trip> trips, T3OutputOperationFakeImpl operation) {
        return "Summary";
    }

    @Override
    public T3Messager getMessager() {
        return messager;
    }
}
