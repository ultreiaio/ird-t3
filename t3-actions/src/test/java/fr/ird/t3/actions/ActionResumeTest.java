package fr.ird.t3.actions;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.FakeT3ServiceContext;
import fr.ird.t3.T3ActionFixtures;
import fr.ird.t3.actions.admin.DeleteTripAction;
import fr.ird.t3.actions.admin.DeleteTripConfiguration;
import fr.ird.t3.actions.data.level0.ComputeRF1Action;
import fr.ird.t3.actions.data.level0.ComputeRF1Configuration;
import fr.ird.t3.actions.data.level0.ComputeRF2Action;
import fr.ird.t3.actions.data.level0.ComputeRF2Configuration;
import fr.ird.t3.actions.data.level0.ComputeSetDurationAndPositiveSetCountAction;
import fr.ird.t3.actions.data.level0.ComputeSetDurationAndPositiveSetCountConfiguration;
import fr.ird.t3.actions.data.level0.ComputeTripEffortsAction;
import fr.ird.t3.actions.data.level0.ComputeTripEffortsConfiguration;
import fr.ird.t3.actions.data.level0.ComputeWellPlanWeightCategoriesProportionsAction;
import fr.ird.t3.actions.data.level0.ComputeWellPlanWeightCategoriesProportionsConfiguration;
import fr.ird.t3.actions.data.level0.ConvertCatchesWeightCategoriesAction;
import fr.ird.t3.actions.data.level0.ConvertCatchesWeightCategoriesConfiguration;
import fr.ird.t3.actions.data.level1.AbstractLevel1Action;
import fr.ird.t3.actions.data.level1.ComputeWeightOfCategoriesForSetAction;
import fr.ird.t3.actions.data.level1.ConvertSampleSetSpeciesFrequencyToWeightAction;
import fr.ird.t3.actions.data.level1.ConvertSetSpeciesFrequencyToWeightAction;
import fr.ird.t3.actions.data.level1.ExtrapolateSampleCountedAndMeasuredAction;
import fr.ird.t3.actions.data.level1.ExtrapolateSampleWeightToSetAction;
import fr.ird.t3.actions.data.level1.Level1Configuration;
import fr.ird.t3.actions.data.level1.RedistributeSampleNumberToSetAction;
import fr.ird.t3.actions.data.level1.StandardizeSampleMeasuresAction;
import fr.ird.t3.actions.data.level2.Level2InputContext;
import fr.ird.t3.actions.data.level2.Level2OutputContext;
import fr.ird.t3.actions.data.level2.L2StratumResult;
import fr.ird.t3.actions.data.level2.Level2Action;
import fr.ird.t3.actions.data.level2.Level2Configuration;
import fr.ird.t3.actions.data.level3.Level3InputContext;
import fr.ird.t3.actions.data.level3.Level3OutputContext;
import fr.ird.t3.actions.data.level3.L3StratumResult;
import fr.ird.t3.actions.data.level3.Level3Action;
import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceAction;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceConfiguration;
import fr.ird.t3.actions.io.input.ImportInputSourceAction;
import fr.ird.t3.actions.io.input.ImportInputSourceConfiguration;
import fr.ird.t3.actions.io.output.ExportAction;
import fr.ird.t3.actions.io.output.ExportConfiguration;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.cache.ZoneStratumCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityImpl;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleImpl;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.SampleSetImpl;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequencyImpl;
import fr.ird.t3.entities.data.StandardiseSampleSpecies;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequency;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequencyImpl;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesImpl;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripImpl;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesImpl;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.io.output.T3OutputOperation;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.io.output.fake.v0.T3OutputOperationFakeImpl;
import fr.ird.t3.io.output.fake.v0.T3OutputProviderFakeImpl;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.FreeMarkerService;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3OutputService;
import fr.ird.t3.services.T3ServiceFactory;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Version;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;

/**
 * Created by tchemit on 11/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@RunWith(Parameterized.class)
public class ActionResumeTest<C extends T3ActionConfiguration, A extends T3Action<C>> {
    @ClassRule
    public static final FakeT3ServiceContext serviceContext = new FakeT3ServiceContext(true);
    private static final Log log = LogFactory.getLog(ActionResumeTest.class);
    private static final Consumer<Level1Configuration> level1Configuration = configuration -> {
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
    };
    private static T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();
    private static T3ActionFixtures fixtures = serviceFactory.newService(T3ActionFixtures.class, serviceContext);
    private static final Consumer<AbstractLevel1Action> level1Action = action -> {
        // set step in configuration
        action.getConfiguration().setStep(action.getStep());

        Multimap<Trip, Sample> samplesByTrip = ArrayListMultimap.create();
        Vessel v = fixtures.vessel1();
        Vessel v2 = fixtures.vessel2();

        Species sp = new SpeciesImpl();
        sp.setTopiaId("topiaid:1");
        sp.setLabel1("Species 1");
        sp.setCode3L("code3L 1");

        Species sp2 = new SpeciesImpl();
        sp2.setTopiaId("topiaid:2");
        sp2.setLabel1("Species 2");
        sp2.setCode3L("code3L 2");

        Sample s = new SampleImpl();
        s.setTopiaId("topiaid:1");
        s.setSampleNumber(1);

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleSet sw = new SampleSetImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s.addSampleSet(sw);

            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:1");
            sassf.setSpecies(sp);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:2");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleSet sw = new SampleSetImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s.addSampleSet(sw);
            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:3");
            sassf.setSpecies(sp);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:4");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }
        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:1");
            sss.setSpecies(sp);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:1");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:2");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s.addStandardiseSampleSpecies(sss);
        }

        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:3");
            sss.setSpecies(sp2);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:3");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:4");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s.addStandardiseSampleSpecies(sss);
        }

        Sample s2 = new SampleImpl();
        s2.setTopiaId("topiaid:2");
        s2.setSampleNumber(2);

        Sample s3 = new SampleImpl();
        s3.setTopiaId("topiaid:3");
        s3.setSampleNumber(3);
        Sample s4 = new SampleImpl();
        s4.setTopiaId("topiaid:4");
        s4.setSampleNumber(4);

        Trip t = new TripImpl();
        t.setTopiaId("topiaid:1");
        t.setLandingDate(new Date());
        t.setVessel(v);

        t.addSample(s);
        t.addSample(s2);

        samplesByTrip.put(t, s);

        Trip t2 = new TripImpl();
        t2.setTopiaId("topiaid:2");
        t2.setLandingDate(new Date());
        t2.setVessel(v2);
        t2.addSample(s4);
        t2.addSample(s3);

        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:3");
            sss.setSpecies(sp);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:5");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:6");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s4.addStandardiseSampleSpecies(sss);


        }
        {
            StandardiseSampleSpecies sss = new StandardiseSampleSpeciesImpl();
            sss.setTopiaId("topiaid:4");
            sss.setSpecies(sp2);

            StandardiseSampleSpeciesFrequency sssf = new StandardiseSampleSpeciesFrequencyImpl();
            sssf.setTopiaId("topiaid:7");
            sssf.setLfLengthClass(10);
            sssf.setNumber(10);
            sss.addStandardiseSampleSpeciesFrequency(sssf);
            StandardiseSampleSpeciesFrequency sssf2 = new StandardiseSampleSpeciesFrequencyImpl();
            sssf2.setTopiaId("topiaid:8");
            sssf2.setLfLengthClass(20);
            sssf2.setNumber(20);
            sss.addStandardiseSampleSpeciesFrequency(sssf2);

            s4.addStandardiseSampleSpecies(sss);
        }

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleSet sw = new SampleSetImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s4.addSampleSet(sw);

            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:1");
            sassf.setSpecies(sp2);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:2");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }

        {
            Activity a = new ActivityImpl();
            a.setTopiaId("topiaid:1");
            a.setDate(new Date());
            SampleSet sw = new SampleSetImpl();
            sw.setTopiaId("topiaid:1");
            sw.setActivity(a);
            s4.addSampleSet(sw);
            SampleSetSpeciesFrequency sassf = new SampleSetSpeciesFrequencyImpl();
            sassf.setTopiaId("topiaid:3");
            sassf.setSpecies(sp2);
            sassf.setNumber(10f);
            sassf.setLfLengthClass(10);
            sw.addSampleSetSpeciesFrequency(sassf);

            SampleSetSpeciesFrequency sassf2 = new SampleSetSpeciesFrequencyImpl();
            sassf2.setTopiaId("topiaid:4");
            sassf2.setSpecies(sp);
            sassf2.setNumber(20f);
            sassf2.setLfLengthClass(20);
            sw.addSampleSetSpeciesFrequency(sassf2);
        }
        samplesByTrip.put(t2, s4);

        action.setSamplesByTrip(samplesByTrip);
        Multimap<Trip, Sample> notTreatedSamples =
                action.buildSamplesNotTreated(samplesByTrip);
        action.setNotTreatedSamplesByTrip(notTreatedSamples);
    };
    private static final Consumer<Map<String, Object>> level1Parameters = parameters -> {
        Map<String, String> oceans = new TreeMap<>();
        putInMap(oceans, fixtures.oceanAtlantic());
        parameters.put("oceans", oceans);

        Map<String, String> vesselSimpleTypes = new TreeMap<>();
        putInMap(vesselSimpleTypes,
                fixtures.vesselSimpleTypeCanneur(),
                fixtures.vesselSimpleTypeSenneur());
        parameters.put("vesselSimpleTypes", vesselSimpleTypes);

        Map<String, String> fleets = new TreeMap<>();
        putInMap(fleets, fixtures.frenchCountry(), fixtures.spanishCountry());
        parameters.put("fleets", fleets);

        Map<String, String> sampleQualities = new TreeMap<>();
        putInMap(sampleQualities,
                fixtures.sampleQuality1(),
                fixtures.sampleQuality2());
        parameters.put("sampleQualities", sampleQualities);

        Map<String, String> sampleTypes = new TreeMap<>();
        putInMap(sampleTypes, fixtures.sampleType1(), fixtures.sampleType2());
        parameters.put("sampleTypes", sampleTypes);

        parameters.put("speciesDecorator", getDecorator(Species.class));
        parameters.put("tripDecorator", getDecorator(Trip.class));
        parameters.put("activityDecorator", getDecorator(Activity.class));
        parameters.put("statics", new BeansWrapperBuilder(new Version("2.3.0")).build().getStaticModels());
    };
    private static Map<String, String> species;
    private final Class<C> configurationType;
    private final Class<A> actionType;
    private final Consumer<C> configurationExtra;
    private final Consumer<A> actionExtra;
    private final Consumer<T3ActionContext<C>> actionContextExtra;
    private final Consumer<Map<String, Object>> parametersExtra;

    public ActionResumeTest(Class<C> configurationType, Class<A> actionType, Consumer<C> configurationExtra, Consumer<A> actionExtra, Consumer<Map<String, Object>> parametersExtra, Consumer<T3ActionContext<C>> actionContextExtra) {
        this.configurationType = configurationType;
        this.actionType = actionType;
        this.configurationExtra = configurationExtra;
        this.actionExtra = actionExtra;
        this.parametersExtra = parametersExtra;
        this.actionContextExtra = actionContextExtra;
        species = null;
    }

    @SuppressWarnings("Duplicates")
    @Parameterized.Parameters
    public static List<Object[]> data() {
        return ImmutableList.<Object[]>builder()
                .add(new Object[]{
                        ComputeRF1Configuration.class,
                        ComputeRF1Action.class,
                        (Consumer<ComputeRF1Configuration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                            configuration.setMinimumRate(0.8f);
                            configuration.setMaximumRate(1.5f);
                        },
                        (Consumer<ComputeRF1Action>) action -> {
                            action.setNbVessels(10);
                            action.setNbAcceptedTrips(5);
                            action.setNbRejectedTrips(5);
                            action.setNbCompleteAcceptedTrips(2);
                            action.setNbCompleteAcceptedTripsWithBadRF1(1);
                            action.setNbTrips(10);
                            action.setTotalCatchWeightRF1(100);
                            action.setTotalLandingWeight(102);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> vesselSimpleTypes = new TreeMap<>();
                            putInMap(vesselSimpleTypes, fixtures.vesselSimpleTypeCanneur(), fixtures.vesselSimpleTypeSenneur());
                            parameters.put("vesselSimpleTypes", vesselSimpleTypes);
                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry());
                            parameters.put("fleets", fleets);
                        }, null
                })
                .add(new Object[]{
                        ComputeRF2Configuration.class,
                        ComputeRF2Action.class,
                        (Consumer<ComputeRF2Configuration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                            configuration.setLandingHarbourIds(Collections.singletonList("1"));
                            configuration.setVesselSimpleTypeIds(Arrays.asList(fixtures.vesselSimpleTypeCanneur().getTopiaId(),
                                    fixtures.vesselSimpleTypeSenneur().getTopiaId()));
                            configuration.setFleetIds(Collections.singletonList(fixtures.frenchCountry().getTopiaId()));
                        },
                        (Consumer<ComputeRF2Action>) action -> {
                            action.setNbStratum(100);
                            action.setNbTrips(10);
                            action.setNbTripsWithRF2(5);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> landingHarbours = new TreeMap<>();
                            landingHarbours.put("1", "Saint Nazaire");
                            parameters.put("landingHarbours", landingHarbours);

                            Map<String, String> vesselSimpleTypes = new TreeMap<>();
                            putInMap(vesselSimpleTypes, fixtures.vesselSimpleTypeCanneur(), fixtures.vesselSimpleTypeSenneur());
                            parameters.put("vesselSimpleTypes", vesselSimpleTypes);

                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("fleets", fleets);
                        }, null
                })
                .add(new Object[]{
                        ComputeRF2Configuration.class,
                        ComputeRF2Action.class,
                        (Consumer<ComputeRF2Configuration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                            configuration.setLandingHarbourIds(Collections.emptyList());
                            configuration.setVesselSimpleTypeIds(Collections.emptyList());
                            configuration.setFleetIds(Collections.emptyList());
                        },
                        (Consumer<ComputeRF2Action>) action -> {
                            action.setNbStratum(100);
                            action.setNbTrips(10);
                            action.setNbTripsWithRF2(5);
                        },
                        null, null
                })
                .add(new Object[]{
                        ComputeSetDurationAndPositiveSetCountConfiguration.class,
                        ComputeSetDurationAndPositiveSetCountAction.class,
                        (Consumer<ComputeSetDurationAndPositiveSetCountConfiguration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                        },
                        (Consumer<ComputeSetDurationAndPositiveSetCountAction>) action -> {
                            action.setNbVessels(5);
                            action.setNbTrips(10);
                            action.setNbActivities(100);
                            action.setNbPositiveActivities(80);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> vesselSimpleTypes = new TreeMap<>();
                            putInMap(vesselSimpleTypes, fixtures.vesselSimpleTypeCanneur(), fixtures.vesselSimpleTypeSenneur());
                            parameters.put("vesselSimpleTypes", vesselSimpleTypes);
                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry());
                            parameters.put("fleets", fleets);
                        }, null
                })
                .add(new Object[]{
                        ComputeTripEffortsConfiguration.class,
                        ComputeTripEffortsAction.class,
                        (Consumer<ComputeTripEffortsConfiguration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                        },
                        (Consumer<ComputeTripEffortsAction>) action -> {
                            action.setNbTrips(10);
                            action.setTotalFishingTimeN0(10);
                            action.setTotalSearchTimeN0(20);
                            action.setTotalTimeAtSeaN0(30);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> vesselSimpleTypes = new TreeMap<>();
                            putInMap(vesselSimpleTypes, fixtures.vesselSimpleTypeCanneur(), fixtures.vesselSimpleTypeSenneur());
                            parameters.put("vesselSimpleTypes", vesselSimpleTypes);
                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry());
                            parameters.put("fleets", fleets);
                        }, null
                })
                .add(new Object[]{
                        ComputeWellPlanWeightCategoriesProportionsConfiguration.class,
                        ComputeWellPlanWeightCategoriesProportionsAction.class,
                        (Consumer<ComputeWellPlanWeightCategoriesProportionsConfiguration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                        },
                        (Consumer<ComputeWellPlanWeightCategoriesProportionsAction>) action -> {
                            action.setNbVessels(10);
                            action.setNbWell(5);
                            action.setNbWellWithPlan(10);
                            action.setNbWellWithNoPlan(5);
                            action.setNbTrips(10);
                            action.setTotalWellPlanWeight(100.20f);
                            action.setTotalWellSetAllSpeciesWeight(100.20f);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> vesselSimpleTypes = new TreeMap<>();
                            putInMap(vesselSimpleTypes, fixtures.vesselSimpleTypeCanneur(), fixtures.vesselSimpleTypeSenneur());
                            parameters.put("vesselSimpleTypes", vesselSimpleTypes);
                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry());
                            parameters.put("fleets", fleets);
                        }, null
                })
                .add(new Object[]{
                        ConvertCatchesWeightCategoriesConfiguration.class,
                        ConvertCatchesWeightCategoriesAction.class,
                        (Consumer<ConvertCatchesWeightCategoriesConfiguration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                        },
                        (Consumer<ConvertCatchesWeightCategoriesAction>) action -> {
                            action.setNbTrips(10);

                            Map<Species, ConvertCatchesWeightCategoriesAction.CatchWeightResult> map = new HashMap<>();
                            Species species = new SpeciesImpl();
                            species.setLabel1("Albaroce");
                            species.setCode3L("ABT");
                            ConvertCatchesWeightCategoriesAction.CatchWeightResult result = new ConvertCatchesWeightCategoriesAction.CatchWeightResult();
                            result.setLogBookTotalWeight(10);
                            result.setTreatmentTotalWeight(10.2f);
                            map.put(species, result);
                            action.setSpecieWeightResult(map);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> vesselSimpleTypes = new TreeMap<>();
                            putInMap(vesselSimpleTypes, fixtures.vesselSimpleTypeCanneur(), fixtures.vesselSimpleTypeSenneur());
                            parameters.put("vesselSimpleTypes", vesselSimpleTypes);
                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry());
                            parameters.put("fleets", fleets);
                            parameters.put("speciesDecorator", getDecorator(Species.class));
                        }, null
                })
                .add(new Object[]{
                        AnalyzeInputSourceConfiguration.class,
                        AnalyzeInputSourceAction.class,
                        (Consumer<AnalyzeInputSourceConfiguration>) configuration -> {
                            configuration.setInputFile(new File("inputSource"));
                            configuration.setUseWells(true);
                            configuration.setTripType(TripType.STANDARD);
                            configuration.setInputProvider(serviceContext.newService(T3InputService.class).getProviders()[0]);
                            configuration.setCanCreateVessel(true);
                            configuration.setCreateVirtualVessel(true);
                        },
                        (Consumer<AnalyzeInputSourceAction>) action -> {
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            List<Trip> safeTripList = new ArrayList<>();

                            safeTripList.add(fixtures.trip1());
                            safeTripList.add(fixtures.trip2());

                            List<Trip> unsafeTripList = new ArrayList<>();

                            unsafeTripList.add(fixtures.trip3());
                            unsafeTripList.add(fixtures.trip4());

                            Map<Trip, Trip> tripToReplaceList = new HashMap<>();

                            tripToReplaceList.put(safeTripList.get(0), fixtures.trip5());
                            tripToReplaceList.put(safeTripList.get(1), fixtures.trip6());

                            parameters.put(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, safeTripList);
                            parameters.put(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, unsafeTripList);
                            parameters.put(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE, tripToReplaceList);

                            parameters.put("tripDecorator", getDecorator(Trip.class));
                            parameters.put("tripDecorator2", getDecorator(Trip.class, DecoratorService.WITH_ID));
                        },
                        (Consumer<T3ActionContext<AnalyzeInputSourceConfiguration>>) actionContext -> {
                            List<Trip> safeTripList = new ArrayList<>();

                            safeTripList.add(fixtures.trip1());
                            safeTripList.add(fixtures.trip2());

                            Map<Trip, Trip> tripToReplaceList = new HashMap<>();

                            tripToReplaceList.put(safeTripList.get(0), fixtures.trip5());
                            tripToReplaceList.put(safeTripList.get(1), fixtures.trip6());

                            actionContext.putResult(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE, tripToReplaceList);
                        }
                })
                .add(new Object[]{
                        ImportInputSourceConfiguration.class,
                        ImportInputSourceAction.class,
                        (Consumer<ImportInputSourceConfiguration>) configuration -> {
                            configuration.setInputFile(new File("inputSource"));
                            configuration.setUseWells(true);
                            configuration.setTripType(TripType.STANDARD);
                            configuration.setInputProvider(serviceContext.newService(T3InputService.class).getProviders()[0]);
                            configuration.setCanCreateVessel(true);
                            configuration.setCreateVirtualVessel(true);

                            Set<Trip> safeTripList = new HashSet<>();

                            safeTripList.add(fixtures.trip1());
                            safeTripList.add(fixtures.trip1());

                            Set<Trip> unsafeTripList = new HashSet<>();
                            unsafeTripList.add(fixtures.trip3());
                            unsafeTripList.add(fixtures.trip3());

                            configuration.setTripsToImport(safeTripList);
                            configuration.setTripsToDelete(unsafeTripList);
                        },
                        (Consumer<ImportInputSourceAction>) action -> {

                        },
                        (Consumer<Map<String, Object>>) parameters -> parameters.put("tripDecorator", getDecorator(Trip.class, DecoratorService.WITH_ID)), null
                })
                .add(new Object[]{
                        ExportConfiguration.class,
                        ExportAction.class,
                        (Consumer<ExportConfiguration>) configuration -> {

                            T3OutputService t3OutputService = serviceContext.newService(T3OutputService.class);
                            T3OutputProvider outputProvider = t3OutputService.getProvider(T3OutputProviderFakeImpl.ID);

                            //noinspection unchecked
                            configuration.setOutputProvider(outputProvider);
                            List<String> operationIds = new ArrayList<>();
                            for (T3OutputOperation operation : outputProvider.getOperations()) {
                                operationIds.add(operation.getId());
                            }
                            configuration.setOperationIds(operationIds);
                            configuration.setBeginDate(T3Date.newDate(10, 2011));
                            configuration.setEndDate(T3Date.newDate(10, 2012));

                            configuration.setOceanId(String.valueOf(fixtures.oceanAtlantic().getCode()));
                            configuration.setFleetId(String.valueOf(fixtures.frenchCountry().getCode()));

                            configuration.setUrl("jdbc://localhost/Fake");
                            configuration.setLogin("login");
                            configuration.setPassword("***");
                        },
                        (Consumer<ExportAction>) action -> {
                            Map<T3OutputOperation, String> operationSummaries = new LinkedHashMap<>();
                            operationSummaries.put(T3OutputOperationFakeImpl.TRIP_AND_LANDING, "summary for export of trip and landing...");
                            operationSummaries.put(T3OutputOperationFakeImpl.ACTIVITY_AND_CATCHES, "summary for export of activities and catches...");
                            operationSummaries.put(T3OutputOperationFakeImpl.SAMPLE, "summary for export of samples...");
                            action.setOperationSummaries(operationSummaries);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> oceans = new TreeMap<>();

                            putInMap(oceans, fixtures.oceanAtlantic());
                            parameters.put("oceans", oceans);

                            Map<String, String> fleets = new TreeMap<>();
                            putInMap(fleets, fixtures.frenchCountry());
                            parameters.put("fleets", fleets);
                        }, null
                })
                .add(new Object[]{
                        DeleteTripConfiguration.class,
                        DeleteTripAction.class,
                        (Consumer<DeleteTripConfiguration>) configuration -> {
                            configuration.setDeleteTrip(true);
                            Trip trip1 = fixtures.trip1();
                            Trip trip2 = fixtures.trip2();
                            Trip trip3 = fixtures.trip3();
                            configuration.setTripIds(Arrays.asList(trip1.getTopiaId(), trip2.getTopiaId(), trip3.getTopiaId()));
                        },
                        (Consumer<DeleteTripAction>) action -> {
                            action.setNbTrips(3);
                            action.setNbActivities(30);
                        },
                        null, null
                })
                .add(new Object[]{
                        Level2Configuration.class,
                        Level2Action.class,
                        (Consumer<Level2Configuration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                            configuration.setStratumMinimumSampleCountObjectSchoolType(10);
                            configuration.setStratumMinimumSampleCountFreeSchoolType(20);
                            configuration.setTimeStep(4);
                            configuration.setStratumWeightRatio(12.5f);
                        },
                        (Consumer<Level2Action>) action -> {
                            try {
                                Level2InputContext inputContext = new Level2InputContext(action) {
                                    @Override
                                    public List<ZoneStratumAware> getZones(SchoolType schoolType) {
                                        return Collections.emptyList();
                                    }

                                    @Override
                                    protected ZoneStratumCache createZoneCache(T3TopiaPersistenceContext persistenceContext, ZoneStratumAwareMeta zoneMeta, String zoneVersionId) {
                                        return null;
                                    }

                                    @Override
                                    protected ZoneStratumAwareMeta createZoneMeta(String zoneTypeId) {
                                        return null;
                                    }
                                };
                                Level2OutputContext outputContext = new Level2OutputContext(action, inputContext);
                                action.setOutputContext(outputContext);
                            } catch (Exception e) {
                                throw new IllegalStateException(String.format("Can't prepare action: %s", action), e);
                            }
                            Level2OutputContext outputContext = action.getOutputContext();
                            L2StratumResult stratumResult = new L2StratumResult(null, "Mon libelle");
                            stratumResult.setNbActivities(10);
                            stratumResult.setNbActivities(5);
                            outputContext.addStratumResult(stratumResult);
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> oceans = new TreeMap<>();
                            putInMap(oceans, fixtures.oceanAtlantic());
                            parameters.put("oceans", oceans);

                            Map<String, String> catchFleets = new TreeMap<>();
                            putInMap(catchFleets, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("catchFleets", catchFleets);

                            Map<String, String> sampleFleets = new TreeMap<>();
                            putInMap(sampleFleets, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("sampleFleets", sampleFleets);

                            Map<String, String> sampleFlags = new TreeMap<>();
                            putInMap(sampleFlags, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("sampleFlags", sampleFlags);

                            Map<String, String> species = new TreeMap<>();
                            putInMap(species, fixtures.species1(), fixtures.species2());
                            parameters.put("species", species);

                            Map<String, String> zoneTypes = new TreeMap<>();
                            zoneTypes.put("1", "zoneType 1");
                            zoneTypes.put("2", "zoneType 2");
                            parameters.put("zoneTypes", zoneTypes);

                            Map<String, String> zoneVersions = new TreeMap<>();
                            ZoneVersion zoneVersion1 = fixtures.zoneVersion1();
                            ZoneVersion zoneVersion2 = fixtures.zoneVersion1();
                            zoneVersions.put(zoneVersion1.getVersionId(), zoneVersion1.getVersionLabel());
                            zoneVersions.put(zoneVersion2.getVersionId(), zoneVersion2.getVersionLabel());
                            parameters.put("zoneVersions", zoneVersions);

                        }, null
                })
                .add(new Object[]{
                        Level3Configuration.class,
                        Level3Action.class,
                        (Consumer<Level3Configuration>) configuration -> {
                            configuration.setBeginDate(T3Date.newDate(1, 2011));
                            configuration.setEndDate(T3Date.newDate(1, 2012));
                            configuration.setTimeStep(4);
                            configuration.setStratumWeightRatio(12.5f);
                            Map<String, StratumMinimumSampleCount> ratios = new HashMap<>();
                            for (String s : getSpecies().keySet()) {
                                StratumMinimumSampleCount r = new StratumMinimumSampleCount();
                                r.setMinimumCountForFreeSchool(10);
                                r.setMinimumCountForObjectSchool(20);
                                ratios.put(s, r);
                            }
                            configuration.setStratumMinimumSampleCount(ratios);
                        },
                        (Consumer<Level3Action>) action -> {
                            Level3InputContext inputContext = new Level3InputContext(action) {
                                @Override
                                public List<ZoneStratumAware> getZones(SchoolType schoolType) {
                                    return Collections.emptyList();
                                }

                                @Override
                                protected ZoneStratumCache createZoneCache(T3TopiaPersistenceContext persistenceContext, ZoneStratumAwareMeta zoneMeta, String zoneVersionId) {
                                    return null;
                                }

                                @Override
                                protected ZoneStratumAwareMeta createZoneMeta(String zoneTypeId) {
                                    return null;
                                }
                            };
                            try {
                                Level3OutputContext outputContext = new Level3OutputContext(action, inputContext);
                                action.setOutputContext(outputContext);
                            } catch (Exception e) {
                                throw new IllegalStateException(String.format("Can't prepare action: %s", action), e);
                            }
                            Level3OutputContext outputContext = action.getOutputContext();

                            L3StratumResult stratumResult = new L3StratumResult(null, "Mon libelle");
                            stratumResult.setNbActivities(10);
                            stratumResult.setNbActivities(5);
                            outputContext.addStratumResult(stratumResult);

                            Activity activity = fixtures.activityLevel3();
                            action.mergeActivityTotalFishesCount(activity, stratumResult.getTotalFishesCount());
                            inputContext.setSpecies(Sets.newHashSet(fixtures.species1(), fixtures.species2()));
                            outputContext.getTotalFishesCount().addValues(stratumResult.getTotalFishesCount());
                        },
                        (Consumer<Map<String, Object>>) parameters -> {
                            Map<String, String> oceans = new TreeMap<>();
                            putInMap(oceans, fixtures.oceanAtlantic());
                            parameters.put("oceans", oceans);

                            Map<String, String> catchFleets = new TreeMap<>();
                            putInMap(catchFleets, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("catchFleets", catchFleets);

                            Map<String, String> sampleFleets = new TreeMap<>();
                            putInMap(sampleFleets, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("sampleFleets", sampleFleets);

                            Map<String, String> sampleFlags = new TreeMap<>();
                            putInMap(sampleFlags, fixtures.frenchCountry(), fixtures.spanishCountry());
                            parameters.put("sampleFlags", sampleFlags);

                            parameters.put("species", species);

                            Map<String, String> zoneTypes = new TreeMap<>();
                            zoneTypes.put("1", "zoneType 1");
                            zoneTypes.put("2", "zoneType 2");
                            parameters.put("zoneTypes", zoneTypes);

                            Map<String, String> zoneVersions = new TreeMap<>();
                            ZoneVersion zoneVersion1 = fixtures.zoneVersion1();
                            ZoneVersion zoneVersion2 = fixtures.zoneVersion2();
                            zoneVersions.put(zoneVersion1.getVersionId(), zoneVersion1.getVersionLabel());
                            zoneVersions.put(zoneVersion2.getVersionId(), zoneVersion2.getVersionLabel());
                            parameters.put("zoneVersions", zoneVersions);

                        }, null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        ComputeWeightOfCategoriesForSetAction.class,
                        level1Configuration,
                        (Consumer<ComputeWeightOfCategoriesForSetAction>) action -> {
                            level1Action.accept(action);
                            action.setNbSampleWithoutWell(1);
                        },
                        level1Parameters,
                        null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        ConvertSampleSetSpeciesFrequencyToWeightAction.class,
                        level1Configuration,
                        (Consumer<ConvertSampleSetSpeciesFrequencyToWeightAction>) level1Action::accept,
                        level1Parameters,
                        null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        ConvertSetSpeciesFrequencyToWeightAction.class,
                        level1Configuration,
                        (Consumer<ConvertSetSpeciesFrequencyToWeightAction>) action -> {
                            level1Action.accept(action);
                            action.setNbTreatedSets(10);
                            action.setNbTreatedFishesInSamples(103);
                            action.setNbCreatedFishesInSetSpeciesFrequency(102);
                        },
                        level1Parameters,
                        null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        ExtrapolateSampleCountedAndMeasuredAction.class,
                        level1Configuration,
                        (Consumer<ExtrapolateSampleCountedAndMeasuredAction>) action -> {
                            level1Action.accept(action);
                            List<ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel> speciesModel = new ArrayList<>();
                            Species s = new SpeciesImpl();
                            s.setTopiaId("topiaid:1");
                            s.setLabel1("species 1");
                            s.setCode3L("code3L 1");
                            ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel sM = new ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel(s);
                            sM.addTotalCount(10);
                            sM.addMeasuredCount(5);
                            speciesModel.add(sM);
                            s = new SpeciesImpl();
                            s.setTopiaId("topiaid:2");
                            s.setLabel1("species 2");
                            s.setCode3L("code3L 2");
                            sM = new ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel(s);
                            sM.addTotalCount(20);
                            sM.addMeasuredCount(15);
                            speciesModel.add(sM);
                            action.putResult(ExtrapolateSampleCountedAndMeasuredAction.RESULT_SPECIES_MODEL, speciesModel);
                        },
                        level1Parameters,
                        null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        ExtrapolateSampleWeightToSetAction.class,
                        level1Configuration,
                        (Consumer<ExtrapolateSampleWeightToSetAction>) action -> {
                            level1Action.accept(action);
                            action.setNbTreatedSets(10);
                            action.setNbTreatedFishesInSamples(103);
                            action.setNbCreatedFishesInSetSpeciesFrequency(102);
                        },
                        level1Parameters,
                        null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        RedistributeSampleNumberToSetAction.class,
                        level1Configuration,
                        level1Action,
                        level1Parameters,
                        null
                })
                .add(new Object[]{
                        Level1Configuration.class,
                        StandardizeSampleMeasuresAction.class,
                        level1Configuration,
                        (Consumer<StandardizeSampleMeasuresAction>) action -> {
                            level1Action.accept(action);
                            List<StandardizeSampleMeasuresAction.StandardizeSpeciesCountModel> speciesModel = new ArrayList<>();
                            Species s = new SpeciesImpl();
                            s.setTopiaId("topiaid:1");
                            s.setLabel1("species 1");
                            s.setCode3L("code3L 1");
                            StandardizeSampleMeasuresAction.StandardizeSpeciesCountModel sM = new StandardizeSampleMeasuresAction.StandardizeSpeciesCountModel(s);
                            sM.addOldCount(10);
                            sM.addNewCount(5);
                            speciesModel.add(sM);
                            s = new SpeciesImpl();
                            s.setTopiaId("topiaid:2");
                            s.setLabel1("species 2");
                            s.setCode3L("code3L 2");
                            sM = new StandardizeSampleMeasuresAction.StandardizeSpeciesCountModel(s);
                            sM.addOldCount(20);
                            sM.addNewCount(15);
                            speciesModel.add(sM);
                            action.putResult(ExtrapolateSampleCountedAndMeasuredAction.RESULT_SPECIES_MODEL, speciesModel);
                        },
                        level1Parameters,
                        null
                })

                .build();
    }

    private static void putInMap(Map<String, String> map, T3ReferenceEntity... entities) {
        for (T3ReferenceEntity entity : entities) {
            map.put(String.valueOf(entity.getCode()), entity.getLabel1());
        }
    }

    private static <E> Decorator<E> getDecorator(Class<E> beanType) {
        return getDecorator(beanType, null);
    }

    private static <E> Decorator<E> getDecorator(Class<E> beanType, String context) {
        DecoratorService decoratorService = serviceContext.newService(DecoratorService.class);
        Locale locale = serviceContext.getLocale();
        return decoratorService.getDecorator(locale, beanType, context);
    }

    private static Map<String, String> getSpecies() {
        if (species == null) {
            species = new TreeMap<>();
            putInMap(species, fixtures.species1(), fixtures.species2());
        }
        return species;
    }

    @Test
    public void testRender() throws Exception {

        C configuration = configurationType.newInstance();
        if (configurationExtra != null) {
            configurationExtra.accept(configuration);
        }
        T3ActionContext<C> actionContext = serviceFactory.newT3ActionContext(configuration, serviceContext);
        A action = serviceFactory.newT3Action(actionType, actionContext);
        if (actionExtra != null) {
            actionExtra.accept(action);
        }
        if (actionContextExtra != null) {
            actionContextExtra.accept(actionContext);
        }
        doRender(action, prepareAction(action, Locale.FRENCH, parametersExtra));
        doRender(action, prepareAction(action, Locale.UK, parametersExtra));
    }

    private void doRender(A action, Map<String, Object> parameters) throws Exception {
        String templateName = action.getRenderTemplateName();
        FreeMarkerService freeMarkerService = serviceContext.newService(FreeMarkerService.class);
        String result = freeMarkerService.renderTemplate(templateName, serviceContext.getLocale(), parameters);
        Assert.assertNotNull(result);
        log.debug(result);
    }

    private Map<String, Object> prepareAction(A action, Locale locale, Consumer<Map<String, Object>> extraParameters) {
        action.addErrorMessage("Une erreur :(");
        action.addWarningMessage("Un warning -:(");
        action.addInfoMessage("Une info :)");
        serviceContext.setLocale(locale);
        action.locale = serviceContext.getLocale();
        Map<String, Object> parameters = new TreeMap<>();
        parameters.put("startDate", new Date());
        parameters.put("endDate", new Date());
        parameters.put("action", action);
        parameters.put("configuration", action.getConfiguration());
        parameters.put("showError", true);
        parameters.put("showWarning", true);
        parameters.put("showInfo", true);
        parameters.put("statics", new BeansWrapperBuilder(new Version("2.3.0")).build().getStaticModels());
        if (extraParameters != null) {
            extraParameters.accept(parameters);
        }
        return parameters;
    }
}
