package fr.ird.t3.actions.data.level3;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.stratum.LevelOutputContext;
import fr.ird.t3.models.SpeciesCountAggregateModel;
import fr.ird.t3.models.SpeciesCountModelHelper;

import static org.nuiton.i18n.I18n.l;

/**
 * To store all output data of {@link Level3Action}.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Level3OutputContext extends LevelOutputContext<Level3Configuration, Level3Action, Level3InputContext, L3StratumResult> {
    /**
     * Global total fishes count for all strata.
     * <p/>
     * We store two count here :
     * <ul>
     * <li>{@link Level3Action#BEFORE_LEVEL3} : total fishes count before level 3</li>
     * <li>{@link Level3Action#AFTER_LEVEL3} : total fishes count after level 3</li>
     * </ul>
     *
     * @since 1.3.1
     */
    private final SpeciesCountAggregateModel totalFishesCount;

    public Level3OutputContext(Level3Action action, Level3InputContext inputContext) {
        super(action, inputContext);
        this.totalFishesCount = new SpeciesCountAggregateModel();
    }

    public void mergeResult(L3StratumResult stratumResult, SpeciesCountAggregateModel totalFishesCountModel) {
        totalFishesCount.addValues(totalFishesCountModel);
        mergeResult(stratumResult);
    }

    public SpeciesCountAggregateModel getTotalFishesCount() {
        return totalFishesCount;
    }

    @SuppressWarnings("unused")
    public String getTotalFishesCountResume() {
        return SpeciesCountModelHelper.decorateModel(
                getDecoratorService(),
                l(getLocale(), "t3.level3.nbFishesResume.title"),
                totalFishesCount.extractForSpecies(getInputContext().getSpecies()),
                Level3Action.BEFORE_LEVEL3, Level3Action.AFTER_LEVEL3);
    }

}
