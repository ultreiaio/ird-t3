package fr.ird.t3.actions.data.level1;

/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.CountAndWeight;
import fr.ird.t3.entities.cache.LengthWeightConversionCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeight;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeightTopiaDao;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategories;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.entities.reference.WeightCategorySampleTopiaDao;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.nuiton.util.TimeLog;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.l;

/**
 * Converts {@link SampleSetSpeciesFrequency} to {@link SampleSetSpeciesCatWeight}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.2
 */
public class ConvertSampleSetSpeciesFrequencyToWeightAction extends AbstractLevel1Action {

    private final Map<String, List<WeightCategorySample>> weightCategoryCache;
    @InjectDAO(entityType = SampleSetSpeciesCatWeight.class)
    private SampleSetSpeciesCatWeightTopiaDao sampleSetSpeciesCatWeightDAO;
    @InjectDAO(entityType = WeightCategorySample.class)
    private WeightCategorySampleTopiaDao weightCategorySampleDAO;
    private LengthWeightConversionCache conversionHelper;

    public ConvertSampleSetSpeciesFrequencyToWeightAction() {
        super(Level1Step.CONVERT_SAMPLE_SET_SPECIES_FREQUENCY_TO_WEIGHT);
        weightCategoryCache = new TreeMap<>();
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        conversionHelper = getT3TopiaPersistenceContext().get().newLengthWeightConversionSimpleCache();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() {
        Set<Trip> trips = samplesByTrip.keySet();
        setNbSteps(samplesByTrip.size() + trips.size());
        for (Trip trip : trips) {
            Collection<Sample> samples = samplesByTrip.get(trip);
            logTreatedAndNotSamplesforATrip(trip, samples);
            long s0 = TimeLog.getTime();
            doExecuteTrip(trip, samples);
            String tripStr = decorate(trip, DecoratorService.WITH_ID);
            getTimeLog().log(s0, String.format("treat trip %s", tripStr));
            // flush transaction otherwise too much data in memory
            flushTransaction(String.format("Flush transaction for %s", tripStr));
        }
        return true;
    }

    private void doExecuteTrip(Trip trip, Collection<Sample> samples) {
        incrementsProgression();
        // extrapolate for each sample set number to set
        for (Sample sample : samples) {
            long s0 = TimeLog.getTime();
            incrementsProgression();
            String sampleStr = l(locale, "t3.level1.convertSampleSetSpeciesFrequencyToWeight.sampleStr",
                    decorate(trip), sample.getSampleNumber());
            for (SampleSet sampleSet : sample.getSampleSet()) {
                long s1 = TimeLog.getTime();
                // found a new activity to convert
                convertFrequenciesToCatWeight(sampleSet);
                getTimeLog().log(s1, String.format("convertSampleSetSpeciesFrequencyToWeight for %d sets.", sample.sizeSampleSet()));
            }
            // mark sample as treated for this step of level 1 treatment
            markAsTreated(sample);
            getTimeLog().log(s0, String.format("treat sample %s", sampleStr));
        }
        // mar trip as treated for this level 1 step
        markAsTreated(trip);
    }

    private List<WeightCategorySample> getWeightCategories(SampleSet sampleSet) {
        Activity activity = sampleSet.getActivity();
        Ocean ocean = activity.getOcean();
        SchoolType schoolType = activity.getSchoolType();
        String key = ocean.getTopiaId() + "-" + schoolType.getTopiaId();
        List<WeightCategorySample> result = weightCategoryCache.get(key);
        if (result == null) {
            result = weightCategorySampleDAO.forOceanEquals(ocean).findAll();
            WeightCategories.sort(result);
            weightCategoryCache.put(key, result);
        }
        return Objects.requireNonNull(result, "Could not find any weight categories for ocean " + ocean.getLabel1());
    }

    private void convertFrequenciesToCatWeight(SampleSet sampleSet) {
        Collection<SampleSetSpeciesFrequency> frequencies = sampleSet.getSampleSetSpeciesFrequency();
        Activity activity = sampleSet.getActivity();
        Date date = activity.getDate();
        Ocean activityOcean = activity.getOcean();
        // group frequencies by species
        Multimap<Species, SampleSetSpeciesFrequency> frequenciesBySpecies = SpeciesTopiaDao.groupBySpecies(frequencies);
        Multimap<Species, Integer> lengthClassesBySpecies = ArrayListMultimap.create();
        // collect all length classes by species
        SampleSetSpeciesFrequencyTopiaDao.collectLengthClasses(frequenciesBySpecies, lengthClassesBySpecies);
        // get all available weight categories
        List<WeightCategorySample> weightCategories = getWeightCategories(sampleSet);
        for (Species species : frequenciesBySpecies.keySet()) {
            // get conversion for species
            LengthWeightConversion conversion = conversionHelper.getConversions(species, activityOcean, 0, date);
            Collection<SampleSetSpeciesFrequency> frequenciesForSpecies = frequenciesBySpecies.get(species);
            List<Integer> lengthClasses = Lists.newArrayList(lengthClassesBySpecies.get(species));
            Collections.sort(lengthClasses);
            Map<WeightCategorySample, CountAndWeight> countAndWeightsByCategories =
                    convertForSpecies(weightCategories, conversion, frequenciesForSpecies, lengthClasses);
            // now we can build for the given activity and species the entries in SetWeight
            fill(sampleSet, species, countAndWeightsByCategories);
        }
    }

    private Map<WeightCategorySample, CountAndWeight> convertForSpecies(List<WeightCategorySample> weightCategories,
                                                                        LengthWeightConversion conversion,
                                                                        Collection<SampleSetSpeciesFrequency> frequencies,
                                                                        List<Integer> lengthClasses) {
        Map<Integer, WeightCategorySample> weightCategoryMap =
                conversionHelper.getWeightCategoriesDistribution(conversion, weightCategories, lengthClasses);
        Map<WeightCategorySample, CountAndWeight> result = new HashMap<>();
        T3IOUtil.fillMapWithDefaultValue(result, weightCategoryMap.values(), CountAndWeight::new);
        for (SampleSetSpeciesFrequency setSize : frequencies) {
            // get length class
            int lfLengthClass = setSize.getLfLengthClass();
            // find the correct category for this length class
            WeightCategorySample weightCategory = weightCategoryMap.get(lfLengthClass);
            CountAndWeight countAndWeight = result.get(weightCategory);
            Float number = setSize.getNumber();
            countAndWeight.addCount(number);
            // convert to weight from lf length class
            countAndWeight.addWeight(number * conversion.computeWeightFromLFLengthClass(lfLengthClass));
        }
        return result;
    }

    private void fill(SampleSet sampleSet, Species species, Map<WeightCategorySample, CountAndWeight> countAndWeightsByCategories) {

        for (Map.Entry<WeightCategorySample, CountAndWeight> e2 : countAndWeightsByCategories.entrySet()) {
            WeightCategorySample categoryTreatment = e2.getKey();
            CountAndWeight countAndWeight = e2.getValue();
            float count = countAndWeight.getCount();
            // weight must be stored in tons but are in kilograms
            Float weight = countAndWeight.getWeight() / 1000;
            SampleSetSpeciesCatWeight setSpeciesCatWeight = sampleSetSpeciesCatWeightDAO.create(
                    SampleSetSpeciesCatWeight.PROPERTY_SPECIES, species,
                    SampleSetSpeciesCatWeight.PROPERTY_WEIGHT_CATEGORY_SAMPLE, categoryTreatment,
                    SampleSetSpeciesCatWeight.PROPERTY_WEIGHT, weight,
                    SampleSetSpeciesCatWeight.PROPERTY_COUNT, count);
            sampleSet.addSampleSetSpeciesCatWeight(setSpeciesCatWeight);
        }
    }
}
