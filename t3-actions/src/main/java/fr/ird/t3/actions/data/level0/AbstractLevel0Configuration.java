/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.type.T3Date;

import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Abstract common configuration for all actions of the level 0.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractLevel0Configuration implements T3ActionConfiguration {

    private static final long serialVersionUID = -8845009278361356281L;

    /** All usable vessel simple types. */
    protected List<VesselSimpleType> vesselSimpleTypes;

    /** All usable fleets. */
    protected List<Country> fleets;

    /** Selected vessel simple types ids. */
    protected List<String> vesselSimpleTypeIds;

    /** Selected fleet ids. */
    protected List<String> fleetIds;

    /** begin date to use. */
    protected T3Date beginDate;

    /** end date to use. */
    protected T3Date endDate;

    /** min begin date to use. */
    protected T3Date minDate;

    /** max end date to use. */
    protected T3Date maxDate;

    protected final Level0Step step;

    protected AbstractLevel0Configuration(Level0Step step) {
        this.step = step;
    }

    public List<VesselSimpleType> getVesselSimpleTypes() {
        return vesselSimpleTypes;
    }

    public void setVesselSimpleTypes(List<VesselSimpleType> vesselSimpleTypes) {
        this.vesselSimpleTypes = vesselSimpleTypes;
    }

    public List<Country> getFleets() {
        return fleets;
    }

    public void setFleets(List<Country> fleets) {
        this.fleets = fleets;
    }

    public List<String> getVesselSimpleTypeIds() {
        return vesselSimpleTypeIds;
    }

    public void setVesselSimpleTypeIds(List<String> vesselSimpleTypeIds) {
        this.vesselSimpleTypeIds = vesselSimpleTypeIds;
    }

    public List<String> getFleetIds() {
        return fleetIds;
    }

    public void setFleetIds(List<String> fleetIds) {
        this.fleetIds = fleetIds;
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(T3Date beginDate) {
        this.beginDate = beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public void setEndDate(T3Date endDate) {
        this.endDate = endDate;
    }

    public T3Date getMinDate() {
        return minDate;
    }

    public void setMinDate(T3Date minDate) {
        this.minDate = minDate;
    }

    public T3Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(T3Date maxDate) {
        this.maxDate = maxDate;
    }

    @Override
    public final String getName(Locale locale) {
        String stepName = l(locale, step.getI18nKey());
        return l(locale, "t3.level0.action", stepName);
    }
}
