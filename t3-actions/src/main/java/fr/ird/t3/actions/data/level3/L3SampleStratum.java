/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.actions.stratum.SampleStratum;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Suppliers;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.data.AbstractSetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.LengthCompositionAggregateModel;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Sample stratum for level 3 treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class L3SampleStratum extends SampleStratum<Level3Configuration, Level3Action, L3StratumConfiguration, L3SampleStratum> {

    private static final Log log = LogFactory.getLog(L3SampleStratum.class);

    /**
     * Total weight of the catch stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private final float catchStratumTotalWeight;
    /**
     * Keep the count of fishes for all species of the stratum.
     *
     * @since 1.3.1
     */
    private final Map<Species, Float> speciesCount;
    /**
     * Contains for each species his composition to apply if a set does not
     * have any sample.
     *
     * @since 1.3.1
     */
    private final Map<Species, LengthCompositionAggregateModel> compositionModel;
    /**
     * Contains for all activities of the stratum their setspeciesFrequencies
     * grouped by species.
     *
     * @since 1.3.1
     */
    private final Multimap<Species, SetSpeciesFrequency> allSetSpeciesFrequenciesBySpecies;
    /**
     * Conversion helper.
     *
     * @since 1.3.1
     */
    private final LengthWeightConversionWithContextCache conversionHelper;
    /**
     * Total weight of the sample stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private float sampleStratumTotalWeight;

    L3SampleStratum(L3StratumConfiguration configuration,
                    Collection<Species> speciesToFix,
                    float catchStratumTotalWeight,
                    LengthWeightConversionWithContextCache conversionHelper) {
        super(configuration, speciesToFix);
        this.catchStratumTotalWeight = catchStratumTotalWeight;
        this.conversionHelper = conversionHelper;
        this.speciesCount = new HashMap<>();
        this.compositionModel = new HashMap<>();
        this.allSetSpeciesFrequenciesBySpecies = ArrayListMultimap.create();
    }

    @Override
    public void close() throws IOException {
        super.close();
        LengthCompositionAggregateModel.close(compositionModel);
        speciesCount.clear();
        compositionModel.clear();
        allSetSpeciesFrequenciesBySpecies.clear();
    }

    @Override
    protected L3SampleStratumLoader newLoader() {
        int oceanCode = getConfiguration().getZone().getOcean().getCode();
        L3SampleStratumLoader result;
        switch (oceanCode) {
            case 1:
                result = new L3SampleStratumLoaderAtlantic(this);
                break;
            case 2:
                result = new L3SampleStratumLoaderIndian(this);
                break;
            default:
                throw new IllegalStateException("Not implemented for ocean with code " + oceanCode);
        }
        return result;
    }

    @Override
    public void mergeNewActivities(T3ServiceContext serviceContext, Set<Activity> activities) {
        for (Activity activity : activities) {
            // add total weight of samples of the activity (only for selected species)
            float totalWeight = T3EntityHelper.getTotal(
                    activity.getSetSpeciesCatWeight(),
                    SetSpeciesCatWeight::getWeight,
                    getConfiguration().getSpeciesToFixFilter(),
                    T3Suppliers.newActivityDecorateSupplier(
                            serviceContext,
                            activity,
                            "Found a trip %s - activity %s with a null SetSpeciesCatWeight#weight"));
            sampleStratumTotalWeight += totalWeight;
            // split activity frequencies by species
            Multimap<Species, SetSpeciesFrequency> setSpeciesFrequenciesBySpecies =
                    Multimaps.index(activity.getSetSpeciesFrequency(), SetSpeciesFrequency::getSpecies);
            // get species found (and only the one to use)
            Set<Species> speciesFound = new HashSet<>(setSpeciesFrequenciesBySpecies.keySet());
            speciesFound.retainAll(getSpeciesToFix());
            // add missing empty model
            T3IOUtil.fillMapWithDefaultValue(speciesCount, speciesFound, () -> 0f);
            // store it in stratum (to be reused to compute composition model)
            // for each species compute his total count for any length class (used by level substitution)
            for (Species species : speciesFound) {
                Collection<SetSpeciesFrequency> frequencies = setSpeciesFrequenciesBySpecies.get(species);
                allSetSpeciesFrequenciesBySpecies.putAll(species, frequencies);
                float newCount = AbstractSetSpeciesFrequencyTopiaDao.collectSimpleSampleCount(frequencies);
                Float oldCount = speciesCount.get(species);
                speciesCount.put(species, oldCount + newCount);
            }
        }
        addMergedActivitiesCount(activities.size());
        log.info(String.format("sampleStratumTotalWeight = %s", getSampleStratumTotalWeight()));
    }

    @Override
    public void init(T3ServiceContext serviceContext, List<WeightCategoryTreatment> weightCategories, Level3Action messager) throws Exception {
        super.init(serviceContext, weightCategories, messager);
        // compute the global compositionModel
        for (Species species : allSetSpeciesFrequenciesBySpecies.keySet()) {
            LengthWeightConversion conversions = conversionHelper.getConversions(species);
            Collection<SetSpeciesFrequency> setSpeciesFrequencies = allSetSpeciesFrequenciesBySpecies.get(species);
            // collect counts
            Map<Integer, Float> counts = SetSpeciesFrequencyTopiaDao.collectSampleCount(setSpeciesFrequencies);
            // get all length classes
            List<Integer> lengthClasses = new ArrayList<>(counts.keySet());
            // sort them
            Collections.sort(lengthClasses);
            // distribute then to the weightCategories
            Map<Integer, WeightCategoryTreatment> weightCategoriesDistribution = conversionHelper.getWeightCategoriesDistribution(conversions, weightCategories, lengthClasses);
            LengthCompositionAggregateModel model = new LengthCompositionAggregateModel();
            compositionModel.put(species, model);
            // compute
            for (WeightCategoryTreatment weightCategory : weightCategories) {
                // compute weights
                Map<Integer, Float> weights = new HashMap<>();
                for (Map.Entry<Integer, WeightCategoryTreatment> entry : weightCategoriesDistribution.entrySet()) {
                    if (weightCategory.equals(entry.getValue())) {
                        // length class
                        int lengthClass = entry.getKey();
                        // fishes count
                        float count = counts.get(lengthClass);
                        // unit fish weight (from length class)
                        float unitWeight = conversions.computeWeightFromLFLengthClass(lengthClass);
                        // total weight for this length class
                        float totalWeight = count * unitWeight;
                        // keep it
                        weights.put(lengthClass, totalWeight);
                    }
                }
                model.addModel(weightCategory, species, weights);
            }
        }
    }

    public float getCatchStratumTotalWeight() {
        return catchStratumTotalWeight;
    }

    public float getSampleStratumTotalWeight() {
        return sampleStratumTotalWeight;
    }

    public float getSpeciesTotalCount(Species species) {
        checkInitMethodInvoked(speciesCount);
        return speciesCount.get(species);
    }

    public Collection<Species> getSampleSpecies() {
        checkInitMethodInvoked(speciesCount);
        return speciesCount.keySet();
    }

    public Map<Species, LengthCompositionAggregateModel> getCompositionModel() {
        checkInitMethodInvoked(compositionModel);
        return compositionModel;
    }

    @Override
    public String logSampleStratumLevel(int substitutionLevel, Level3Action messager) {
        Locale l = messager.getLocale();
        String title = l(l, "t3.level3.sampleStratum.resume.for.level",
                substitutionLevel,
                // Do not use #getNbActivites() since it is perharp not yet setted.
                // Use this internal state instead of the #getNbActivities() since activites can still not be setted in the stratum
                // Fix http://forge.codelutin.com/issues/1907
                getNbMergedActivities(),
                getSampleStratumTotalWeight());

        Decorator<Species> decorator = messager.getDecoratorService().getDecorator(l, Species.class, null);
        Set<Species> speciesToFix = getSpeciesToFix();
        StringBuilder sb = new StringBuilder(title);
        for (Map.Entry<Species, Float> entry : speciesCount.entrySet()) {
            Species species = entry.getKey();
            if (speciesToFix.contains(species)) {
                sb.append(l(l, "t3.level3.sampleStratum.resume.for.level.and.species", decorator.toString(species), entry.getValue()));
            }
        }
        return sb.toString();
    }

}
