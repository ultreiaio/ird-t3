/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input.test;

import fr.ird.t3.entities.data.TripType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Configuration of a msaccess test.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class MSAccessTestConfiguration {

    private static final String DB_PATH = "..:.mvn:cache:avdth";
    private static final Log log = LogFactory.getLog(MSAccessTestConfiguration.class);
    private static File basedir;
    private final Pattern testNamePattern;
    private final String dbPattern;
    private final String providerId;
    public File accessFile;
    public boolean execute;
    public String dbName;
    private boolean executeOnce;
    private Integer exactYear;
    private Integer lastYear;
    private Boolean executeAll;
    private File dbBasedir;
    private boolean useWells;
    private boolean logWarnings;
    private TripType tripType;

    public MSAccessTestConfiguration(String testNamePattern, String dbPattern, String providerId) {
        this.testNamePattern = Pattern.compile(testNamePattern);
        this.dbPattern = dbPattern;
        this.providerId = providerId;
    }

    private static File getBasedir() {
        if (basedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            basedir = new File(tmp);
        }
        return basedir;
    }

    public boolean beforeClass() {
        String[] split = DB_PATH.split(":");
        dbBasedir = getBasedir();
        for (String s1 : split) {
            dbBasedir = new File(dbBasedir, s1);
        }
        if (!dbBasedir.exists()) {
            log.warn(String.format("Db directory [%s] does not exist, will skip test.", dbBasedir));
            return false;
        }
        log.info(String.format("Db directory [%s] detected, will do tests.", dbBasedir));
        String s = System.getenv("executeAll");
        if (!StringUtils.isEmpty(s)) {
            executeAll = Boolean.valueOf(s);
        } else {
            executeAll = false;
        }
        executeAll = true;
        s = System.getenv("lastYear");
        if (!StringUtils.isEmpty(s) && !"null".equals(s)) {
            lastYear = Integer.valueOf(s);
        }
        s = System.getenv("exactYear");
        if (!StringUtils.isEmpty(s) && !"null".equals(s)) {
            exactYear = Integer.valueOf(s);
        }
        s = System.getenv("useWells");
        if (!StringUtils.isEmpty(s)) {
            useWells = Boolean.valueOf(s);
        } else {
            useWells = false;
        }
        return true;
    }

    public boolean setup(String testName) {
        execute = false;
        Matcher matcher = testNamePattern.matcher(testName);
        if (!matcher.find()) {
            throw new IllegalStateException(
                    String.format("Test name [%s] is not valid and should respect pattern %s", testName, testNamePattern));
        }
        String ocean = matcher.group(1);
        String group = matcher.group(2);
        Integer year = Integer.valueOf(group);
        if (exactYear != null && !year.equals(exactYear)) {
            return false;
        }
        if (exactYear == null && lastYear != null && year > lastYear) {
            return false;
        }
        if (lastYear == null && exactYear == null && executeOnce && !executeAll) {
            log.debug(String.format("Skip test %s executeAll flag is off", testName));
            return false;
        }
        dbName = String.format(dbPattern, ocean, group);
        accessFile = new File(new File(dbBasedir, ocean), dbName);
        boolean exists = accessFile.exists();
        if (!exists) {
            log.warn(String.format("Could not find access file %s", accessFile));
            return false;
        }
        log.debug(String.format("DbName = %s", dbName));
        execute = true;
        return true;
    }

    public boolean doTest(String testName) {
        if (!execute) {
            log.info(String.format("Will not execute test [%s]", testName));
            return false;
        }
        executeOnce = true;
        return true;
    }

    public File getAccessFile() {
        return accessFile;
    }

    public boolean isExecute() {
        return execute;
    }

    public String getDbName() {
        return dbName;
    }

    public boolean isExecuteOnce() {
        return executeOnce;
    }

    public Integer getExactYear() {
        return exactYear;
    }

    public Integer getLastYear() {
        return lastYear;
    }

    public Boolean getExecuteAll() {
        return executeAll;
    }

    public String getDbPattern() {
        return dbPattern;
    }

    public File getDbBasedir() {
        return dbBasedir;
    }

    public boolean isUseWells() {
        return useWells;
    }

    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    public String getProviderId() {
        return providerId;
    }

    public boolean isLogWarnings() {
        return logWarnings;
    }

    public void setLogWarnings(boolean logWarnings) {
        this.logWarnings = logWarnings;
    }

    public boolean setup(OceanFixtures fixtures) {
        String[] split = DB_PATH.split(":");
        dbBasedir = getBasedir();
        for (String s1 : split) {
            dbBasedir = new File(dbBasedir, s1);
        }
        if (!dbBasedir.exists()) {
            log.warn(String.format("Db directory [%s] does not exist, will skip test.", dbBasedir));
            return false;
        }
        log.info(String.format("Db directory [%s] detected, will do tests.", dbBasedir));

        String[] fixtureName = fixtures.toString().split("_");
        String ocean = fixtureName[0];
        String group = fixtureName[1];
        dbName = String.format(dbPattern, ocean, group);
        accessFile = new File(new File(dbBasedir, ocean), dbName);
        boolean exists = accessFile.exists();
        if (!exists) {
            log.warn(String.format("Could not find access file %s", accessFile));
            return false;
        }
        log.debug(String.format("DbName = %s", dbName));

        execute = true;
        return true;
    }

    public void setTripType(TripType tripType) {
        this.tripType = tripType;
    }

    public TripType getTripType() {
        return tripType;
    }
}
