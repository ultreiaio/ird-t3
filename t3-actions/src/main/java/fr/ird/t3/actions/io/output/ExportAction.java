/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.output;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselTopiaDao;
import fr.ird.t3.io.output.T3Output;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.io.output.T3OutputOperation;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntityById;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * To export t3+ data using {@link T3Output} mecanism.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ExportAction extends T3Action<ExportConfiguration> {

    private static final Log log = LogFactory.getLog(ExportAction.class);
    protected Map<T3OutputOperation, String> operationSummaries;
    @InjectEntityById(entityType = Country.class)
    protected Country fleet;
    @InjectEntityById(entityType = Ocean.class)
    protected Ocean ocean;
    @InjectDAO(entityType = Trip.class)
    protected TripTopiaDao tripDAO;
    protected List<Trip> trips;
    private T3Output<T3OutputOperation, T3OutputConfiguration> outputPilot;

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();

        // get output provider
        ExportConfiguration configuration = getConfiguration();

        T3OutputProvider<T3OutputOperation, T3OutputConfiguration> outputProvider = configuration.getOutputProvider();

        // get output configuration
        T3OutputConfiguration outputConfiguration = configuration.getOutputConfiguration();

        // instantiate the output pilot from the provider
        T3Output<T3OutputOperation, T3OutputConfiguration> pilot = outputProvider.newInstance(outputConfiguration, this, serviceContext);

        // inject in output pilot
        newService(IOCService.class).injectExcept(pilot);

        setOutputPilot(pilot);

        trips = buildTrips();
    }

    @Override
    protected void deletePreviousData() {
        //TODO Remove data
    }

    @Override
    protected boolean executeAction() throws Exception {

        operationSummaries = new LinkedHashMap<>();

        T3OutputProvider<T3OutputOperation, T3OutputConfiguration> outputProvider = getConfiguration().getOutputProvider();

        for (String operationId : getConfiguration().getOperationIds()) {

            T3OutputOperation operation = outputProvider.getOperation(operationId);

            // execute operation and obtain his summary in return
            String summary = outputPilot.executeOperation(trips, operation);

            // keep the summary for action resume
            operationSummaries.put(operation, summary);
        }

        // no need commit in T3+ database
        return false;
    }

    @SuppressWarnings("unused")
    public String getOperationSummary(T3OutputOperation operation) {
        return operationSummaries.get(operation);
    }

    private void setOutputPilot(T3Output<T3OutputOperation, T3OutputConfiguration> outputPilot) {
        this.outputPilot = outputPilot;
    }

    private List<Trip> buildTrips() {

        List<Trip> result = tripDAO.findAllBetweenLandingDate(getConfiguration().getBeginDate(), getConfiguration().getEndDate(), false, false);

        // get all vessels used in trips
        Set<Vessel> vessels = VesselTopiaDao.getAllVessels(result);

        if (log.isInfoEnabled()) {
            log.info(String.format("All vessels used by any trip : %d", vessels.size()));
        }

        // keep only given fleets
        VesselTopiaDao.retainsFleetCountries(vessels, Collections.singletonList(fleet));

        if (log.isInfoEnabled()) {
            log.info(String.format("Usable vessels (retains fleets) : %d", vessels.size()));
        }

        // keep trips only for given vessels
        TripTopiaDao.retainsVessels(result, vessels);

        if (log.isInfoEnabled()) {
            log.info(String.format("Usable trips (retains vessels) : %d", result.size()));
        }
        // keep trips only for given ocean
        TripTopiaDao.retainsOceans(result, Collections.singletonList(ocean));

        return result;
    }

    public void setOperationSummaries(Map<T3OutputOperation, String> operationSummaries) {
        this.operationSummaries = operationSummaries;
    }
}
