package fr.ird.t3.actions.data.level3;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.stratum.LevelInputContext;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.reference.SchoolType;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * A context to get all input data for the execution of action.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Level3InputContext extends LevelInputContext<Level3Configuration, Level3Action> {
    /**
     * Cache of activity with multi-zones.
     */
    private final Map<String, Map<String, String>> activityWithMultiZones;
    /**
     * Cache of stratum minimum sample count by school type.
     */
    private final Map<SchoolType, Map<String, Integer>> stratumMinimumSampleCountBySchoolType;

    protected Level3InputContext(Level3Action action) {
        super(action);
        this.activityWithMultiZones = new TreeMap<>();
        // prepare for each school type all minimum sample count by species
        Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount = action.getConfiguration().getStratumMinimumSampleCount();
        Map<SchoolType, Map<String, Integer>> minimumSampleCountBySchoolType = new HashMap<>();
        for (SchoolType schoolType : getSchoolTypes()) {
            Map<String, Integer> bySpecies = new TreeMap<>();
            minimumSampleCountBySchoolType.put(schoolType, bySpecies);
            int code = schoolType.getCode();
            switch (code) {
                case 1:
                    // Object school type
                    for (Map.Entry<String, StratumMinimumSampleCount> e : stratumMinimumSampleCount.entrySet()) {
                        bySpecies.put(e.getKey(), e.getValue().getMinimumCountForObjectSchool());
                    }
                    break;
                case 2:
                    // Free school type
                    for (Map.Entry<String, StratumMinimumSampleCount> e : stratumMinimumSampleCount.entrySet()) {
                        bySpecies.put(e.getKey(), e.getValue().getMinimumCountForFreeSchool());
                    }
                    break;
                default:
                    throw new IllegalStateException(String.format("Can not use the school type %d", code));
            }
        }
        this.stratumMinimumSampleCountBySchoolType = minimumSampleCountBySchoolType;
    }

    public Map<String, Integer> getStratumMinimumSampleCount(SchoolType schoolType) {
        return stratumMinimumSampleCountBySchoolType.get(schoolType);
    }

    public Map<String, String> getActivityWithMultiZones(Activity activity) {
        return activityWithMultiZones.computeIfAbsent(activity.getTopiaId(), k -> new HashMap<>());
    }

    public void clear() {
        super.clear();
        activityWithMultiZones.clear();
    }

}
