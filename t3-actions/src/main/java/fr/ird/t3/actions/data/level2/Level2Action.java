/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.collect.Multimap;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.stratum.SchoolTypeIndeterminate;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.CorrectedElementaryCatchTopiaDao;
import fr.ird.t3.entities.reference.SchoolTypeTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.models.WeightCompositionModel;
import fr.ird.t3.models.WeightCompositionModelHelper;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Level 2 treatment action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class Level2Action extends T3Action<Level2Configuration> {

    private static final Log log = LogFactory.getLog(Level2Action.class);
    /**
     * {@link Activity} dao.
     */
    @InjectDAO(entityType = Activity.class)
    private ActivityTopiaDao activityDAO;
    /**
     * {@link CorrectedElementaryCatch} dao.
     */
    @InjectDAO(entityType = CorrectedElementaryCatch.class)
    private CorrectedElementaryCatchTopiaDao correctedElementaryCatchDAO;
    /**
     * Input data context.
     */
    private Level2InputContext inputContext;
    /**
     * Output data context.
     */
    private Level2OutputContext outputContext;

    @Override
    public void prepareAction() throws Exception {
        super.prepareAction();
        inputContext = new Level2InputContext(this);
        outputContext = new Level2OutputContext(this, inputContext);
        setNbSteps(3 * inputContext.getNbStratum());
    }

    @Override
    protected void deletePreviousData() {
        // data will be removed from selected activities
    }

    @Override
    protected void finalizeAction() throws TopiaException, IOException {
        inputContext.clear();
        super.finalizeAction();
    }

    @Override
    protected boolean executeAction() throws Exception {

        Iterator<L2StratumConfiguration> stratumConfigurationIterator;
        if (getConfiguration().isUseWeightCategoriesInStratum()) {
            stratumConfigurationIterator = new L2StratumConfigurationIteratorWithCategories(inputContext);
        } else {
            stratumConfigurationIterator = new L2StratumConfigurationIteratorWithoutCategories(inputContext);
        }
        while (stratumConfigurationIterator.hasNext()) {
            L2StratumConfiguration stratumConfiguration = stratumConfigurationIterator.next();
            try {
                L2StratumResult result = doExecuteStratum(stratumConfiguration);
                outputContext.addStratumResult(result);
            } finally {
                flushTransaction("After stratum " + stratumConfiguration.getStratumIndex());
            }
        }
        return true;
    }

    private L2StratumResult doExecuteStratum(L2StratumConfiguration stratumConfiguration) throws Exception {
        Collection<Species> species = inputContext.getSpecies();
        List<WeightCategoryTreatment> weightCategories = stratumConfiguration.getWeightCategoryTreatments();
        int stratumIndex = stratumConfiguration.getStratumIndex();
        int nbStratum = inputContext.getNbStratum();
        incrementsProgression();
        String stratumPrefix;
        if (getConfiguration().isUseWeightCategoriesInStratum()) {
            stratumPrefix = l(locale, "t3.level2.stratumLabelWithCategory",
                    stratumIndex, nbStratum,
                    decorate(stratumConfiguration.getSchoolType()),
                    decorate(stratumConfiguration.getZone()),
                    decorate(stratumConfiguration.getWeightCategoryTreatment()),
                    stratumConfiguration.getBeginDate(),
                    stratumConfiguration.getEndDate());
        } else {
            stratumPrefix = l(locale, "t3.level2.stratumLabel",
                    stratumIndex, nbStratum,
                    decorate(stratumConfiguration.getSchoolType()),
                    decorate(stratumConfiguration.getZone()),
                    stratumConfiguration.getBeginDate(),
                    stratumConfiguration.getEndDate());
        }
        L2StratumResult result = new L2StratumResult(stratumConfiguration, stratumPrefix);
        String message = l(locale, "t3.level2.message.start.stratum", stratumPrefix);
        log.info(message);
        if (SchoolTypeIndeterminate.IGNORE == getConfiguration().getSchoolTypeIndeterminate()) {
            // Just report for all activities on School type indeterminate catch weight
            Set<String> activityIds = activityDAO.findAllActivityIdsForCatchStratum(
                    stratumConfiguration.getZoneTableName(),
                    stratumConfiguration.getZone().getTopiaId(),
                    Collections.singleton(SchoolTypeTopiaDao.SCHOOL_TYPE_INDETERMINATE_ID),
                    stratumConfiguration.getBeginDate(),
                    stratumConfiguration.getEndDate()).keySet();
            inputContext.loadActivities(activityIds);
            for (String activityId : activityIds) {
                for (CorrectedElementaryCatch correctedElementaryCatch : inputContext.getActivityCache().forId(activityId).getCorrectedElementaryCatch()) {
                    correctedElementaryCatch.setCorrectedCatchWeight(correctedElementaryCatch.getCatchWeight());
                }
            }
        }
        addInfoMessage("==============================================================================================");
        addInfoMessage(message);
        addInfoMessage("==============================================================================================");
        // compute the catch stratum
        try (L2CatchStratum catchStratum = L2CatchStratum.newCatchStratum(stratumConfiguration, weightCategories, species, this)) {
            incrementsProgression();
            if (catchStratum == null) {
                // no catch in this stratum
                // mark as a 0 substitution level
                result.setSubstitutionLevel(0);
                incrementsProgression();
            } else {

                // compute sample stratum
                try (L2SampleStratum sampleStratum = L2SampleStratum.newSampleStratum(stratumConfiguration, catchStratum, weightCategories, species, this)) {
                    incrementsProgression();
                    // get the substitution level for the sample stratum
                    Integer level = sampleStratum.getSubstitutionLevel();
                    log.info("Sample stratum substitution level " + level);
                    if (level == -1) {
                        // sample stratum could not be found
                        // log sample stratum new composition
                        message = l(getLocale(), "t3.level2.warning.missing.data.for.stratum", result.getLabel());
                        log.warn(message);
                        addWarningMessage(message);
                        // mark it as 999 level to make it appear at top of a reverse list
                        result.setSubstitutionLevel(999);
                        result.addNbActivities(catchStratum.getNbActivities());
                        result.addNbActivitiesWithSample(catchStratum.getNbActivitiesWithSample());
                        // Fill back  CorrectedElementaryCatch.CatchWeight to CorrectedElementaryCatch.CorrectedCatchWeight
                        int activityIndex = 1;
                        int nbActivities = catchStratum.getNbActivities();
                        for (Map.Entry<Activity, Integer> e : catchStratum) {
                            Activity activity = e.getKey();
//                                int nbZones = e.getValue();
                            // is activity was already treated ?
                            boolean newActivity = outputContext.addActivityId(activity.getTopiaId());
                            doExecuteActivityInCatchStratumWithoutSampleStratum(catchStratum,
                                    activity,
                                    activityIndex++,
                                    nbActivities,
                                    newActivity);
                            // keep the stratum substitution level in the activity
                            stratumConfiguration.addToActivity(activity, Activity::getStratumLevelN2, Activity::setStratumLevelN2, 999);
                            activity.setUseMeanStratumCompositionN2(false);
                        }
                    } else {
                        // can use this sampleStratum
                        int activityIndex = 1;
                        int nbActivities = catchStratum.getNbActivities();
                        for (Map.Entry<Activity, Integer> e : catchStratum) {
                            Activity activity = e.getKey();
                            int nbZones = e.getValue();
                            // is activity was already treated ?
                            boolean newActivity = outputContext.addActivityId(activity.getTopiaId());
                            doExecuteActivityInCatchStratum(catchStratum,
                                    sampleStratum,
                                    activity,
                                    nbZones,
                                    activityIndex++,
                                    nbActivities,
                                    newActivity);
                            // keep the stratum substitution level in the activity
                            stratumConfiguration.addToActivity(activity, Activity::getStratumLevelN2, Activity::setStratumLevelN2, sampleStratum.getSubstitutionLevel());
                            activity.setUseMeanStratumCompositionN2(true);
                        }
                        // usable catch stratum
                        // --------------------------------------------------
                        // --- Report results
                        // --------------------------------------------------
                        result.setSubstitutionLevel(sampleStratum.getSubstitutionLevel());
                        result.addNbActivities(catchStratum.getNbActivities());
                        result.addNbActivitiesWithSample(catchStratum.getNbActivitiesWithSample());
                        // merge input and output model from catch stratum models
                        outputContext.mergeResult(catchStratum, result);
//                        catchStratum.mergeGlobalCompositionModels(inputCatchModelForAllSpecies, outputCatchModelForAllSpecies);
//                        totalCatchWeight += catchStratum.getTotalCatchWeightForAllSpecies();
//                        totalCatchWeightForSpeciesFoFix += catchStratum.getTotalCatchWeightForSpeciesToFix();
//                        totalCatchActivities += result.getNbActivities();
//                        totalCatchActivitiesWithSample += result.getNbActivitiesWithSample();
                    }
                }
            }
        }

        return result;
    }

    private void doExecuteActivityInCatchStratumWithoutSampleStratum(L2CatchStratum catchStratum,
                                                                     Activity activity,
                                                                     int activityIndex,
                                                                     int nbActivities,
                                                                     boolean deleteOldData) {

        String activityStr = String.format("%s (%s)", decorate(activity), decorate(activity.getTrip(), DecoratorService.WITH_ID));
        String message = l(locale, "t3.level2.message.start.activity", activityIndex, nbActivities, activityStr, 1);
        log.info(message);
        addInfoMessage(message);
        if (deleteOldData) {
            // delete old data for this activity
            log.info(String.format("Delete previous level2 data of %s", activityStr));
            activity.deleteComputedDataLevel2();
        }
        Collection<Species> species = inputContext.getSpecies();
        // build a model for getting total of each weight category
        try (WeightCompositionAggregateModel catchWeightModelForAllSpecies = catchStratum.catchWeightModelForAllSpecies(activity, 1)) {
            try (WeightCompositionAggregateModel catchWeightModelForSpeciesToFix = catchWeightModelForAllSpecies.extractForSpecies(species)) {
                // log catches weight
                message = logCatchWeight(getDecoratorService(), catchWeightModelForAllSpecies, catchWeightModelForSpeciesToFix);
                log.info(message);
                addInfoMessage(message);
            }
            try (WeightCompositionAggregateModel sampleWeightModelForAllSpecies = catchStratum.sampleWeightModelForAllSpecies(activity)) {
                try (WeightCompositionAggregateModel sampleWeightModelForSpeciesToFix = sampleWeightModelForAllSpecies.extractForSpecies(species)) {
                    String activityResume = logActivityCatchStratum(sampleWeightModelForAllSpecies, sampleWeightModelForSpeciesToFix, getDecoratorService());
                    message = l(locale, "t3.level2.message.activity.with.sample.useOwn.composition", activityResume);
                    log.info(message);
                    addInfoMessage(message);
                }
            }
            // apply composition model to activity catches
            applySampleSpecificCompositionWithoutSampleStratum(catchStratum, activity);
        }
    }

    private void applySampleSpecificCompositionWithoutSampleStratum(L2CatchStratum catchStratum, Activity activity) {
        try (WeightCompositionAggregateModel correctedCatchWeightModel = new WeightCompositionAggregateModel()) {
            // Get all corrected catch group by weight category
            Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> correctedElementaryCatches =
                    catchStratum.getCorrectedElementaryCatchByCategory(activity);
            for (Map.Entry<WeightCategoryTreatment, Collection<CorrectedElementaryCatch>> entry : correctedElementaryCatches.asMap().entrySet()) {
                WeightCategoryTreatment weightCategory = entry.getKey();
                Map<Species, Float> weights = new HashMap<>();
                // Apply on all existing corrected catch weight
                for (CorrectedElementaryCatch aCatch : entry.getValue()) {
                    Species speciesToUse = aCatch.getSpecies();
                    // the corrected catch weight to add to the row
                    float correctedCatchWeight = aCatch.getCatchWeight();
                    // hold added value (for logs)
                    weights.put(speciesToUse, correctedCatchWeight);
                    aCatch.setCorrectedCatchWeight(correctedCatchWeight);
                    aCatch.setCorrectedFlag(false);
                }
                // add corrected weights used for this category
                correctedCatchWeightModel.addModel(weightCategory, weights);
            }
            // log corrected catches weight
            String message = logCorrectedCatchWeight(correctedCatchWeightModel, getDecoratorService());
            log.info(message);
            addInfoMessage(message);
            // add corrected catch weight done for this activity in stratum output model
            catchStratum.addActivityOutputModel(correctedCatchWeightModel);
        }
    }

    private void doExecuteActivityInCatchStratum(L2CatchStratum catchStratum,
                                                 L2SampleStratum sampleStratum,
                                                 Activity activity,
                                                 int nbZones,
                                                 int activityIndex,
                                                 int nbActivities,
                                                 boolean deleteOldData) {
        String activityStr = String.format("%s (%s)", decorate(activity), decorate(activity.getTrip(), DecoratorService.WITH_ID));
        String message = l(locale, "t3.level2.message.start.activity", activityIndex, nbActivities, activityStr, nbZones);
        log.info(message);
        addInfoMessage(message);
        if (deleteOldData) {
            // delete old data for this activity
            log.info(String.format("Delete previous level2 data of %s", activityStr));
            activity.deleteComputedDataLevel2();
        }
        Collection<Species> species = inputContext.getSpecies();
        // build a model for getting total of each weight category
        try (WeightCompositionAggregateModel catchWeightModelForAllSpecies = catchStratum.catchWeightModelForAllSpecies(activity, nbZones)) {
            try (WeightCompositionAggregateModel catchWeightModelForSpeciesToFix = catchWeightModelForAllSpecies.extractForSpecies(species)) {
                // log catches weight
                message = logCatchWeight(getDecoratorService(), catchWeightModelForAllSpecies, catchWeightModelForSpeciesToFix);
                log.info(message);
                addInfoMessage(message);
                // obtain the sample composition model to use for the activity (can come from catch if it has some samples, otherwise use the catch stratum one)
                boolean activityWithSample = activity.isSetSpeciesCatWeightNotEmpty();
                boolean useAllSamplesOfStratum = getConfiguration().isUseAllSamplesOfStratum();
                WeightCompositionAggregateModel sampleWeightModelForSpeciesToFix;
                boolean useSampleStratumComposition = activityWithSample && !useAllSamplesOfStratum;
                if (useSampleStratumComposition) {
                    // activity has some samples, compute his specific composition model
                    // and configuration does not ask to treat such catches with all stratum sample
                    try (WeightCompositionAggregateModel sampleWeightModelForAllSpecies = catchStratum.sampleWeightModelForAllSpecies(activity)) {
                        sampleWeightModelForSpeciesToFix = sampleWeightModelForAllSpecies.extractForSpecies(species);
                        String activityResume = logActivityCatchStratum(sampleWeightModelForAllSpecies, sampleWeightModelForSpeciesToFix, getDecoratorService());
                        message = l(locale, "t3.level2.message.activity.with.sample.useOwn.composition", activityResume);
                        log.info(message);
                        addInfoMessage(message);
                    }
                } else {
                    // activity with no sample, use sample stratum composition or configuration ask to always use stratum sample
                    sampleWeightModelForSpeciesToFix = sampleStratum.getModelsForSpeciesToFix();
                    if (activityWithSample) {
                        message = l(locale, "t3.level2.message.activity.with.sample.useSampleStratum.composition", activityStr);
                    } else {
                        message = l(locale, "t3.level2.message.activity.with.no.sample.useSampleStratum.composition", activityStr);
                    }
                    log.info(message);
                    addInfoMessage(message);
                }

                // apply composition model to activity catches
                applySampleSpecificComposition(catchStratum, activity, nbZones, sampleWeightModelForSpeciesToFix, catchWeightModelForSpeciesToFix);
                if (useSampleStratumComposition) {
                    sampleWeightModelForSpeciesToFix.close();
                }
            }
        }
    }

    private void applySampleSpecificComposition(L2CatchStratum catchStratum,
                                                Activity activity,
                                                int nbZones,
                                                WeightCompositionAggregateModel sampleCompositionModel,
                                                WeightCompositionAggregateModel correctedCatchesForSpeciesToFix) {
        try (WeightCompositionAggregateModel correctedCatchWeightModel = new WeightCompositionAggregateModel()) {
            // Get all corrected catch group by weight category
            Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> correctedElementaryCatches = catchStratum.getCorrectedElementaryCatchByCategory(activity);
            for (Map.Entry<WeightCategoryTreatment, Collection<CorrectedElementaryCatch>> entry : correctedElementaryCatches.asMap().entrySet()) {
                WeightCategoryTreatment weightCategoryTreatment = entry.getKey();
                Map<Species, Float> weights = new HashMap<>();
                // get composition model to use
                WeightCompositionModel model = sampleCompositionModel.getModel(weightCategoryTreatment);
                // set of species to fix
                Set<Species> speciesToFix;
                // set of species still to fix
                Set<Species> speciesStillToFix;
                // get total weight for species to fix from correctedCatches
                float totalWeight;
                if (model == null) {
                    // this means there is no specific composition for this weight category to apply for species to fix
                    // says there is no species to fix (so all catches will be copied to correctedCatchWeight (with no fixedFlag)
                    speciesToFix = speciesStillToFix = Collections.emptySet();
                    totalWeight = 0f;
                } else {
                    // found a specific composition to apply for this weight category
                    speciesToFix = model.getSpecies();
                    speciesStillToFix = new HashSet<>(speciesToFix);
                    totalWeight = correctedCatchesForSpeciesToFix.getModel(weightCategoryTreatment).getTotalWeight();
                }
                // divide the total weight to used by the number of zones of this activity
                totalWeight = totalWeight / nbZones;
                // Apply on all existing corrected catch weight
                for (CorrectedElementaryCatch aCatch : entry.getValue()) {
                    Species speciesToUse = Objects.requireNonNull(aCatch.getSpecies());
                    // flag (was fixed or not)
                    boolean toFix;
                    // the corrected catch weight to add to the row
                    float correctedCatchWeight;
                    if (model != null && speciesToFix.contains(speciesToUse)) {
                        // species to fix
                        toFix = true;
                        float weightRate = model.getWeightRate(speciesToUse);
                        // new corrected catch weight
                        correctedCatchWeight = totalWeight * weightRate;
                        // species no more to treat
                        speciesStillToFix.remove(speciesToUse);
                    } else {
                        // nothing to fix, just propagate old value
                        toFix = false;
                        correctedCatchWeight = aCatch.getCatchWeight();
                    }
                    // hold added value (for logs)
                    weights.put(speciesToUse, correctedCatchWeight);
                    // Add to old value (if any)
                    Float oldCorrectedCatchWeight = aCatch.getCorrectedCatchWeight();
                    if (oldCorrectedCatchWeight == null) {
                        oldCorrectedCatchWeight = 0f;
                    }
                    aCatch.setCorrectedCatchWeight(oldCorrectedCatchWeight + correctedCatchWeight);
                    aCatch.setCorrectedFlag(toFix);
                }
                // Creates new corrected catch weight records for all species still to fix
                // Means they are in samples but not in catches
                for (Species speciesToUse : speciesStillToFix) {
                    // weight rate of the species
                    float weightRate = model == null ? 1f : model.getWeightRate(speciesToUse);
                    // corrected catch weight
                    float correctedCatchWeight = totalWeight * weightRate;
                    // create new record
                    CorrectedElementaryCatch aCatch = correctedElementaryCatchDAO.create();
                    aCatch.setSpecies(speciesToUse);
                    aCatch.setWeightCategoryTreatment(weightCategoryTreatment);
                    aCatch.setCatchWeight(0f);
                    aCatch.setCorrectedFlag(true);
                    aCatch.setCorrectedCatchWeight(correctedCatchWeight);
                    // hold added value (for logs)
                    weights.put(speciesToUse, correctedCatchWeight);
                    // add it to activity
                    activity.addCorrectedElementaryCatch(aCatch);
                }
                // add corrected weights used for this category
                correctedCatchWeightModel.addModel(weightCategoryTreatment, weights);
            }
            // log corrected catches weight
            String message = logCorrectedCatchWeight(correctedCatchWeightModel, getDecoratorService());
            log.info(message);
            addInfoMessage(message);
            // add corrected catch weight done for this activity in stratum output model
            catchStratum.addActivityOutputModel(correctedCatchWeightModel);
        }
    }

    private String logCatchWeight(DecoratorService decoratorService,
                                  WeightCompositionAggregateModel modelForAllSpecies,
                                  WeightCompositionAggregateModel modelForSpeciesToFix) {
        String title = l(locale, "t3.level2.message.activityCatchWeight.resume",
                modelForAllSpecies.getTotalModel().getTotalWeight(),
                modelForSpeciesToFix.getTotalModel().getTotalWeight());
        return WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                modelForAllSpecies,
                modelForSpeciesToFix);
    }

    private String logCorrectedCatchWeight(WeightCompositionAggregateModel correctedCatchWeightModel, DecoratorService decoratorService) {
        Collection<Species> species = inputContext.getSpecies();
        WeightCompositionAggregateModel modelForSpeciesToFix = correctedCatchWeightModel.extractForSpecies(species);
        String title = l(locale, "t3.level2.message.activityCorrectedCatchWeight.resume",
                correctedCatchWeightModel.getTotalModel().getTotalWeight(),
                modelForSpeciesToFix.getTotalModel().getTotalWeight());
        return WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                correctedCatchWeightModel,
                modelForSpeciesToFix);
    }

    private String logActivityCatchStratum(WeightCompositionAggregateModel modelForAllSpecies,
                                           WeightCompositionAggregateModel modelForSpeciesToFix,
                                           DecoratorService decoratorService) {
        String title = l(locale, "t3.level2.message.activityComposition.resume",
                modelForAllSpecies.getTotalModel().getTotalWeight(),
                modelForSpeciesToFix.getTotalModel().getTotalWeight());
        return WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                modelForAllSpecies,
                modelForSpeciesToFix);
    }

    public Level2OutputContext getOutputContext() {
        return outputContext;
    }

    public void setOutputContext(Level2OutputContext outputContext) {
        this.outputContext = outputContext;
    }

    T3ServiceContext getServiceContext() {
        return serviceContext;
    }


}
