/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.base.Preconditions;
import fr.ird.t3.actions.stratum.SampleStratumLoader;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Base level 2 sample stratum loader.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public abstract class L2SampleStratumLoader extends SampleStratumLoader<Level2Configuration, Level2Action, L2StratumConfiguration, L2SampleStratum> {

    private static final Log log = LogFactory.getLog(L2SampleStratumLoader.class);
    /**
     * The minimum sample total weight to obtain the correct substitution level.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration and catch stratum total weight.
     *
     * @since 1.4
     */
    private final float minimumSampleTotalWeight;
    /**
     * The minimum sample count required to obtain the correct substitution level.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration.
     */
    private final int minimumSampleCount;
    /**
     * {@link Activity} dao.
     */
    @InjectDAO(entityType = Activity.class)
    private ActivityTopiaDao activityDAO;

    L2SampleStratumLoader(L2SampleStratum sampleStratum) {
        super(sampleStratum);
        Level2Configuration configuration = sampleStratum.getLevelConfiguration();
        // compute minimumSampleTotalWeight
        float catchStratumTotalWeight = sampleStratum.getCatchStratumTotalWeight();
        float maximumWeightRatio = configuration.getStratumWeightRatio();
        minimumSampleTotalWeight = catchStratumTotalWeight / maximumWeightRatio;
        SchoolType schoolType = sampleStratum.getConfiguration().getSchoolType();
        int code = schoolType.getCode();
        switch (code) {
            case 1:
                // object School type
                minimumSampleCount = configuration.getStratumMinimumSampleCountObjectSchoolType();
                break;
            case 2:
                // free school type
                minimumSampleCount = configuration.getStratumMinimumSampleCountFreeSchoolType();
                break;
            default:
                throw new IllegalStateException("Can not deal with school type with a code = " + code);
        }
    }

    @Override
    protected Set<String> findActivityIds(String schoolTypeId, T3Date beginDate, T3Date endDate, String... zoneIds) {
        StratumConfiguration<Level2Configuration, Level2Action> configuration = getSampleStratum().getConfiguration();
        Set<String> result = new HashSet<>();
        for (String zoneId : zoneIds) {
            // on commence par récupérer les activités :
            // - dans la zone ET
            // - avec le bon type de banc
            // - dans la bonne période de temps
            List<String> activityIds = activityDAO.findAllActivityIdsForSampleStratum(
                    configuration.getZoneTableName(),
                    zoneId,
                    configuration.getSchoolTypeIds(),
                    beginDate,
                    endDate);
            result.addAll(activityIds);
        }
        return result;
    }

    @Override
    public boolean isStratumOk() {
        // test if we have enough sample weight
        L2SampleStratum sampleStratum = getSampleStratum();
        float sampleStratumTotalWeight = sampleStratum.getSampleStratumTotalWeight();
        if (sampleStratumTotalWeight < minimumSampleTotalWeight) {
            // need more sample weight
            log.info(String.format("Missing some weight (got: %s, but required minimum: %s).", sampleStratumTotalWeight, minimumSampleTotalWeight));
            return false;
        }
        // test if we have enough sample count
        int sampleStratumTotalCount = sampleStratum.getSampleStratumTotalCount();
        if (sampleStratumTotalCount < minimumSampleCount) {
            // need more fishes count
            log.info(String.format("Missing fishes count (got: %d, but required minimum: %d).", sampleStratumTotalCount, minimumSampleCount));
            return false;
        }
        // ok the stratum is fine
        return true;
    }
}
