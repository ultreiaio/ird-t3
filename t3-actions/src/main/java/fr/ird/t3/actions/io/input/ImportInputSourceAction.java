/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.T3DataEntity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanTopiaDao;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselTopiaDao;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import static org.nuiton.i18n.I18n.l;

/**
 * Action to import incoming trips (which coming from input source) into a
 * real T3 database.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ImportInputSourceAction extends T3Action<ImportInputSourceConfiguration> {

    private static final Log log = LogFactory.getLog(ImportInputSourceAction.class);
    @InjectDAO(entityType = Trip.class)
    private TripTopiaDao tripDAO;
    @InjectDAO(entityType = Vessel.class)
    private VesselTopiaDao vesselDAO;
    @InjectDAO(entityType = Ocean.class)
    private OceanTopiaDao oceanDAO;
    private Set<Trip> tripToDelete;
    private Set<Trip> trips;

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        trips = getConfiguration().getTripsToImport();
        tripToDelete = getConfiguration().getTripsToDelete();
    }

    @Override
    protected void deletePreviousData() {
        //TODO Remove data
    }

    @Override
    protected boolean executeAction() {
        if (CollectionUtils.isEmpty(trips)) {
            // no trips at all to import, no commit to do, skip the action
            return false;
        }
        int nbSteps = 0;
        if (CollectionUtils.isNotEmpty(trips)) {
            nbSteps += trips.size();
        }
        if (CollectionUtils.isNotEmpty(tripToDelete)) {
            nbSteps += tripToDelete.size();
        }
        setNbSteps(nbSteps);
        log.info(String.format("Nb steps : %d", getNbSteps()));
        if (CollectionUtils.isNotEmpty(tripToDelete)) {
            // delete trips which will be replaced
            for (Trip trip : tripToDelete) {
                incrementsProgression();
                String tripLabel = trip.getVessel().getLabel1() + " - " + trip.getLandingDate();
                log.info(String.format("Delete old trip %s", tripLabel));
                // get trip to delete from this session
                Trip aTripToDelete = tripDAO.forTopiaIdEquals(trip.getTopiaId()).findUnique();
                tripDAO.delete(aTripToDelete);
                String message = l(locale, "t3.message.trip.was.deleted", tripLabel);
                addInfoMessage(message);
            }
        }
        boolean logbookMissing = TripType.LOGBOOKMISSING == getConfiguration().getTripType();
        boolean createVirtualVessel = getConfiguration().isCreateVirtualVessel();
        BuildVisitor builder = new BuildVisitor(getActionContext().getApplicationContext(), getT3TopiaPersistenceContext().get());
        for (Trip trip : trips) {
            incrementsProgression();
            Vessel vessel = trip.getVessel();
            String tripLabel = vessel.getLabel1() + " - " + trip.getLandingDate();
            log.info(String.format("Import trip %s", tripLabel));
            if (!vesselDAO.forCodeEquals(vessel.getCode()).exists()) {
                // will create the vessel
                String message = l(locale, "t3.message.create.new.vessel", decorate(vessel));
                log.info(message);
                addInfoMessage(message);
                vessel.setVesselVirtual(createVirtualVessel);
                Vessel createdVessel = vesselDAO.create(vessel);
                trip.setVessel(createdVessel);
            }
            builder.acceptEntity(trip);
            String message = l(locale, "t3.message.trip.was.imported", tripLabel);
            addInfoMessage(message);

            if (logbookMissing) {
                fillActivityOcean(builder.activitiesWithNoOcean);
            }
            // clean the builder after each entity (since each entity is
            // standalone and does not share data with other trips)
            builder.clear();
        }
        // some trips were imported must commit them
        return true;
    }

    private void fillActivityOcean(Set<Activity> ids) {

        // Need to flush as we are going to query new data
        getT3TopiaPersistenceContext().get().getHibernateSupport().getHibernateSession().flush();
        for (Activity activity : ids) {
            Ocean ocean = oceanDAO.findOceanByActivity(activity);
            activity.setOcean(ocean);
        }
    }

    protected static class BuildVisitor implements TopiaEntityVisitor {

        protected final Set<String> ids = new HashSet<>();
        final Set<Activity> activitiesWithNoOcean = new HashSet<>();
        final T3TopiaPersistenceContext tx;
        final T3TopiaApplicationContext rootTx;
        final Stack<TopiaEntity> stack = new Stack<>();
        protected TopiaEntity current;

        BuildVisitor(T3TopiaApplicationContext rootTx, T3TopiaPersistenceContext tx) {
            this.tx = tx;
            this.rootTx = rootTx;
        }

        void acceptEntity(TopiaEntity entity) {
            if (StringUtils.isEmpty(entity.getTopiaId()) || !ids.contains(entity.getTopiaId())) {
                // first time we want to accept the entity
                try {
                    entity.accept(this);
                } catch (TopiaException e) {
                    throw new IllegalStateException(String.format("Could not visit entity %s", entity), e);
                }
            } else {
                log.debug(String.format("Entity already visited %s", entity.getTopiaId()));
            }
        }

        @Override
        public void start(TopiaEntity entity) {
            String id = entity.getTopiaId();
            if (StringUtils.isEmpty(id)) {
                Class<? extends TopiaEntity> type = rootTx.getContractClass(entity.getClass());
                // generate a new id for the given entity
                id = rootTx.getConfiguration().getTopiaIdFactory().newTopiaId(type, "" + System.nanoTime());
                entity.setTopiaId(id);
            }
            // register the entity (to not come back to it twice)
            ids.add(id);
            entity.setTopiaCreateDate(new Date());
            log.debug(String.format("start entity %s", id));
            stack.push(entity);
        }

        @Override
        public void end(TopiaEntity entity) {
            String id = entity.getTopiaId();
            log.debug(String.format("end  entity %s", id));
            // can now persist the entity
            try {
                @SuppressWarnings("unchecked")
                TopiaDao<TopiaEntity> dao = tx.getDao(T3EntityEnum.getContractClass((Class) entity.getClass()));
                dao.update(entity);
                if (entity instanceof Activity) {
                    Activity activity = (Activity) entity;
                    if (activity.getOcean() == null) {
                        activitiesWithNoOcean.add(activity);
                    }
                }
            } catch (TopiaException e) {
                throw new IllegalStateException("Could not get dao for entity " + entity, e);
            }
            stack.pop();
            if (!stack.isEmpty()) {
                // still some entity at the top
                current = stack.peek();
            }
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
            if (value != null && T3DataEntity.class.isAssignableFrom(type)) {
                // ok try to accept this entity
                acceptEntity((TopiaEntity) value);
            }
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
            if (value != null && T3DataEntity.class.isAssignableFrom(type)) {
                Iterable<?> col = (Iterable<?>) value;
                for (Object t3DataEntity : col) {
                    acceptEntity((TopiaEntity) t3DataEntity);
                }
            }
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
        }

        @Override
        public void clear() {
            ids.clear();
            activitiesWithNoOcean.clear();
        }
    }

}
