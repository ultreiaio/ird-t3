/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import fr.ird.t3.actions.stratum.CatchStratum;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Suppliers;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.T3ServiceContext;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Define a catch stratum for a given stratum of a level 2 treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class L3CatchStratum extends CatchStratum<Level3Configuration, Level3Action, L3StratumConfiguration> {

    /**
     * Keep the total weight of all the catches in the stratum.
     * <p/>
     * Used by sampleStratum for quality tests.
     *
     * @since 1.3.1
     */
    private float totalCatchWeight;

    L3CatchStratum(L3StratumConfiguration stratumConfiguration, Collection<Species> speciesToFix) {
        super(stratumConfiguration, speciesToFix);
    }

    @Override
    protected L3CatchStratumLoader newLoader() {
        return new L3CatchStratumLoader();
    }

    @Override
    public void init(T3ServiceContext serviceContext, List<WeightCategoryTreatment> weightCategories, Level3Action messager) throws Exception {
        // load catch stratum
        super.init(serviceContext, weightCategories, messager);
        for (Map.Entry<Activity, Integer> e : this) {
            Activity activity = e.getKey();
            int nbZones = e.getValue();
            // compute total catch weight of the activity (only for species to fix)
            totalCatchWeight +=
                    T3EntityHelper.getTotal(
                            getCorrectedElementaryCatch(activity),
                            CorrectedElementaryCatch::getCorrectedCatchWeight,
                            getConfiguration().getSpeciesToFixFilter(),
                            T3Suppliers.newActivityDecorateSupplier(
                                    serviceContext,
                                    activity,
                                    "Found a trip %s - activity %s with a null CorrectedElementaryCatch#correctedCatchWeight")
                    ) / nbZones;
        }
    }

    /**
     * Obtain total catch weight of selected catches for this stratum.
     *
     * @return total catch weight of selected catches for this stratum.
     */
    public float getTotalCatchWeight() {
        return totalCatchWeight;
    }

}
