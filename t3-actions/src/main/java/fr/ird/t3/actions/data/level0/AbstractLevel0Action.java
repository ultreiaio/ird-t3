/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Route;
import fr.ird.t3.entities.data.RouteTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.VesselTopiaDao;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.Set;

/**
 * Abstract level 0 action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractLevel0Action<C extends AbstractLevel0Configuration> extends T3Action<C> {

    //    public static final String RESULT_TRIP_LIST_DONE = "tripListDone";
//    public static final String RESULT_TRIP_LIST_REDONE = "tripListReDone";
    private static final Log log = LogFactory.getLog(AbstractLevel0Action.class);
    private final Level0Step step;
    private final Set<Level0Step> higherSteps;
    @InjectDAO(entityType = Trip.class)
    TripTopiaDao tripDAO;
    @InjectDAO(entityType = Route.class)
    RouteTopiaDao routeDAO;
    @InjectEntitiesById(entityType = Country.class)
    List<Country> fleets;
    @InjectEntitiesById(entityType = VesselSimpleType.class)
    List<VesselSimpleType> vesselSimpleTypes;
    List<Trip> trips;
    private Set<Vessel> vessels;
    private int nbTrips;
    private int nbVessels;

    public AbstractLevel0Action(Level0Step step) {
        this.step = step;
        higherSteps = Level0Step.allAfter(step);
    }

    public Level0Step getStep() {
        return step;
    }

    public final void setTrips(List<Trip> trips) {
        this.trips = trips;
        nbTrips = trips.size();
    }

    public final int getNbTrips() {
        return nbTrips;
    }

    public final int getNbVessels() {
        return nbVessels;
    }

    public void setVessels(Set<Vessel> vessels) {
        this.vessels = vessels;
        nbVessels = vessels.size();
    }

//    protected List<Trip> getUsableTrips() throws TopiaException {
//        return getUsableTrips(null);
//    }

    List<Trip> getUsableTrips(List<Harbour> landingHarbours, boolean rejectComputationStatus) {

        C configuration = getConfiguration();

        List<Trip> tripList = tripDAO.findAllBetweenLandingDate(configuration.getBeginDate(), configuration.getEndDate(), false, rejectComputationStatus);
        log.info(String.format("All trips between %s and %s : %d", configuration.getBeginDate(), configuration.getEndDate(), tripList.size()));

        // get all vessels used in trips
        Set<Vessel> vesselSet = VesselTopiaDao.getAllVessels(tripList);
        setVessels(vesselSet);
        log.info(String.format("All vessels used by any trip : %d", getNbVessels()));

        // keep only given vessel simple types
        VesselTopiaDao.retainsVesselSimpleTypes(vesselSet, vesselSimpleTypes);
        log.info(String.format("Usable vessels (retains vessel simple types) : %d", getNbVessels()));

        // keep only given fleets
        VesselTopiaDao.retainsFleetCountries(vesselSet, fleets);
        log.info(String.format("Usable vessels (retains fleets) : %d", getNbVessels()));

        // keep trips only for given vessels
        TripTopiaDao.retainsVessels(tripList, vesselSet);
        log.info(String.format("Usable trips (retains vessels) : %d", tripList.size()));

        if (landingHarbours != null) {
            // keep trips only for given landing harbours
            TripTopiaDao.retainsLandingHarbours(tripList, landingHarbours);
            log.info(String.format("Usable trips (retains landing harbours) : %d", tripList.size()));
        }
        // sort trips on landing date (once for all)
        TripTopiaDao.sortTrips(tripList);
        return tripList;
    }

    void markTripAsTreated(Trip trip) {

        // let's validate this step
        step.setTripState(trip, true);

        // and invalidate all other following steps
        Level0Step.invalidate(trip, higherSteps);
    }

    public void setNbVessels(int nbVessels) {
        this.nbVessels = nbVessels;
    }

    public void setNbTrips(int nbTrips) {
        this.nbTrips = nbTrips;
    }
}
