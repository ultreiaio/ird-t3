/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import fr.ird.t3.actions.stratum.LevelConfigurationWithStratum;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;

import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Define the global configuration of a level 3 treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class Level3Configuration extends LevelConfigurationWithStratum {

    private static final long serialVersionUID = 1L;

    /** Minimum count of sample usable for each species. */
    private Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount;

    /**
     * For catches with samples, use all samples of the stratum, otherwise keep intact their samples.
     *
     * @since 1.6.1
     */
    private boolean useAllSamplesOfStratum;

    public Map<String, StratumMinimumSampleCount> getStratumMinimumSampleCount() {
        return stratumMinimumSampleCount;
    }

    public void setStratumMinimumSampleCount(Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount) {
        this.stratumMinimumSampleCount = stratumMinimumSampleCount;
    }

    public boolean isUseAllSamplesOfStratum() {
        return useAllSamplesOfStratum;
    }

    public void setUseAllSamplesOfStratum(boolean useAllSamplesOfStratum) {
        this.useAllSamplesOfStratum = useAllSamplesOfStratum;
    }

    @Override
    public String getName(Locale locale) {
        return l(locale, "t3.level3.action");
    }
}
