/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSpecies;
import fr.ird.t3.entities.data.SampleSpeciesFrequency;
import fr.ird.t3.entities.data.StandardiseSampleSpecies;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequency;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesLengthStep;
import fr.ird.t3.entities.reference.SpeciesLengthStepTopiaDao;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.l;

/**
 * Standardize samples measures : this is done in two step :
 * <ul>
 * <li>converts all ld1 measures in lf length classes</li>
 * <li>recode lf length classes according to the step of them defined in {@link Species#getLfLengthClassStep()}</li>
 * </ul>
 * Results are stored in {@link StandardiseSampleSpecies}
 * and {@link StandardiseSampleSpeciesFrequency}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StandardizeSampleMeasuresAction extends AbstractLevel1Action {

    private static final String RESULT_SPECIES_MODEL = "speciesModel";
    private static final Log log = LogFactory.getLog(StandardizeSampleMeasuresAction.class);
    @InjectDAO(entityType = StandardiseSampleSpecies.class)
    private StandardiseSampleSpeciesTopiaDao standardiseSampleSpeciesDAO;
    @InjectDAO(entityType = StandardiseSampleSpeciesFrequency.class)
    private StandardiseSampleSpeciesFrequencyTopiaDao standardiseSampleSpeciesFrequencyDAO;
    @InjectDAO(entityType = SpeciesLengthStep.class)
    private SpeciesLengthStepTopiaDao speciesLengthStepDAO;

    public StandardizeSampleMeasuresAction() {
        super(Level1Step.STANDARDIZE_SAMPLE_MEASURE);
    }

    public List<StandardizeSpeciesCountModel> getResultSpeciesModel() {
        return getResultAsList(RESULT_SPECIES_MODEL, StandardizeSpeciesCountModel.class);
    }

    public StandardizeSpeciesCountModel getTotalSpeciesModel() {
        StandardizeSpeciesCountModel totalResult = new StandardizeSpeciesCountModel(null);
        for (StandardizeSpeciesCountModel m : getResultSpeciesModel()) {
            totalResult.addOldCount(m.getOldCount());
            totalResult.addNewCount(m.getNewCount());
        }
        return totalResult;
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() {
        Map<Species, StandardizeSpeciesCountModel> model = new HashMap<>();
        setNbSteps(samplesByTrip.size());
        for (Trip trip : samplesByTrip.keySet()) {
            Collection<Sample> samples = samplesByTrip.get(trip);
            logTreatedAndNotSamplesforATrip(trip, samples);
            for (Sample sample : samples) {
                doExecuteSample(trip, sample, model);
                // mark sample as treated for this step of level 1 treatment
                markAsTreated(sample);
            }
            // mar trip as treated for this level 1 step
            markAsTreated(trip);
        }
        // store final result in action context
        putResult(RESULT_SPECIES_MODEL, Lists.newArrayList(model.values()));
        model.clear();
        return true;
    }

    private void doExecuteSample(Trip trip, Sample sample, Map<Species, StandardizeSpeciesCountModel> model) {
        incrementsProgression();
        String tripStr = decorate(trip);
        addInfoMessage(l(locale, "t3.level1.standardizeSampleMeasures.treat.sample", tripStr, sample.getSampleNumber()));
        if (!sample.isStandardiseSampleSpeciesEmpty()) {
            // was already computed, remove previous data
            addInfoMessage(
                    l(locale, "t3.level1.standardizeSampleMeasures.treat.remove.previously.treated.data",
                            sample.sizeStandardiseSampleSpecies()));
            sample.clearStandardiseSampleSpecies();
        }
        if (sample.isSampleSpeciesEmpty()) {
            // no sample species, nothing to do
            return;
        }
        // split sample species by species
        Multimap<Species, SampleSpecies> sampleSpeciesBySpecies = SpeciesTopiaDao.groupBySpecies(sample.getSampleSpecies());
        for (Species species : sampleSpeciesBySpecies.keySet()) {
            // get the species count model for this species
            StandardizeSpeciesCountModel speciesCountModel = model.computeIfAbsent(species, StandardizeSpeciesCountModel::new);
            // get sample species for this species
            Collection<SampleSpecies> sampleSpecies = sampleSpeciesBySpecies.get(species);
            //FIXME I replace ocean by trip.departureHarbour.ocean, check this is ok (See #260)
            // obtain once for all the length classes for this species on this ocean
            Multimap<Integer, SpeciesLengthStep> proportionsForSpecies =
                    speciesLengthStepDAO.findAllByOceanAndSpeciesGroupByLd1Class(trip.getDepartureHarbour().getOcean(), species);
            doExecuteSampleSpecies(species, sampleSpecies, sample, speciesCountModel, proportionsForSpecies);
        }
    }

    private void doExecuteSampleSpecies(Species species,
                                        Collection<SampleSpecies> sampleSpecies,
                                        Sample sample,
                                        StandardizeSpeciesCountModel speciesCountModel,
                                        Multimap<Integer, SpeciesLengthStep> proportionsForSpecies) {
        float oldCount = speciesCountModel.computedOldCount(sampleSpecies);
        // each species will give exactly one entry in StandardiseSampleSpecies table
        StandardiseSampleSpecies standardiseSampleSpecies = standardiseSampleSpeciesDAO.create(StandardiseSampleSpecies.PROPERTY_SPECIES, species);
        // contains for each lfLengthClass, the number of fishes
        Map<Integer, Float> frequencies = new TreeMap<>();
        float measuredCount = 0;
        float totalCount = 0;
        for (SampleSpecies sampleSpecie : sampleSpecies) {
            measuredCount += sampleSpecie.getMeasuredCount();
            totalCount += sampleSpecie.getTotalCount();
            collectSampleSpeciesFrequencies(proportionsForSpecies, sampleSpecie, frequencies);
        }
        // set the total measured count computed
        standardiseSampleSpecies.setMeasuredCount(measuredCount);
        // set the total total count computed
        standardiseSampleSpecies.setTotalCount(totalCount);
        Map<Integer, Float> finalFrequencies;
        // historical data are all coded with a 1cm length class step
        Integer lfLengthClassStep = species.getLfLengthClassStep();
        if (lfLengthClassStep != 1) {
            // must recode length classes
            finalFrequencies = new TreeMap<>();
            for (Map.Entry<Integer, Float> ee : frequencies.entrySet()) {
                Integer lfLengthClass = ee.getKey();
                Float numberExtrapolated = ee.getValue();
                int finalLengthClass = lfLengthClass - lfLengthClass % lfLengthClassStep;
                Float finalNumber = finalFrequencies.get(finalLengthClass);
                if (finalNumber == null) {
                    finalNumber = 0.f;
                }
                finalNumber += numberExtrapolated;
                finalFrequencies.put(finalLengthClass, finalNumber);
            }
        } else {
            finalFrequencies = frequencies;
        }
        float newCount = 0;
        // for all final frequencies (for recoded length classes), we obtain a entry in StandardiseSampleSpeciesFrequency
        for (Map.Entry<Integer, Float> ee : finalFrequencies.entrySet()) {
            Integer lfLengthClass = ee.getKey();
            Float numberExtrapolated = ee.getValue();
            StandardiseSampleSpeciesFrequency standardiseSampleSpeciesFrequency = standardiseSampleSpeciesFrequencyDAO.create(
                    StandardiseSampleSpeciesFrequency.PROPERTY_LF_LENGTH_CLASS, lfLengthClass,
                    StandardiseSampleSpeciesFrequency.PROPERTY_NUMBER, numberExtrapolated);
            standardiseSampleSpecies.addStandardiseSampleSpeciesFrequency(standardiseSampleSpeciesFrequency);
            newCount += numberExtrapolated;
        }
        speciesCountModel.addNewCount(newCount);
        // attach to sample new data
        sample.addStandardiseSampleSpecies(standardiseSampleSpecies);
        addInfoMessage(l(locale, "t3.level1.standardizeSampleMeasures.treat.resume.for.species",
                decorate(species), oldCount, newCount));
    }

    private void collectSampleSpeciesFrequencies(Multimap<Integer, SpeciesLengthStep> proportionsForSpecies,
                                                 SampleSpecies sampleSpecies, Map<Integer, Float> frequencies) {
        if (sampleSpecies.isSampleSpeciesFrequencyEmpty()) {
            // no frequency (should never happen)...
            return;
        }
        if (sampleSpecies.isLd1LengthClass()) {
            // need a conversion
            Species species = sampleSpecies.getSpecies();
            for (SampleSpeciesFrequency sampleSpeciesFrequency : sampleSpecies.getSampleSpeciesFrequency()) {
                // ld1 length class is in cm in sample, but in milimeter in SpeciesLengthStep table
                int ld1LengthClass = sampleSpeciesFrequency.getLengthClass() * 10;
                Collection<SpeciesLengthStep> proportions = proportionsForSpecies.get(ld1LengthClass);
                if (CollectionUtils.isEmpty(proportions)) {
                    //TODO This should be an error ?
                    String message = l(locale, "t3.level1.standardizeSampleMeasures.warning.lfConversion.not.found",
                            decorate(species),
                            ld1LengthClass);
                    log.warn(message);
                    addWarningMessage(message);
                    continue;
                }
                float number = sampleSpeciesFrequency.getNumberExtrapolated();
                for (SpeciesLengthStep proportion : proportions) {
                    addLFFrequency(
                            proportion.getLfClass(),
                            // ratios are in integer (0 to 100), but we need the ratio
                            number * proportion.getRatio() / 100,
                            frequencies);
                }
            }
        } else {
            // just add lf frequencies
            for (SampleSpeciesFrequency sampleSpecyFrequency : sampleSpecies.getSampleSpeciesFrequency()) {
                addLFFrequency(sampleSpecyFrequency.getLengthClass(), sampleSpecyFrequency.getNumberExtrapolated(), frequencies);
            }
        }
    }

    private void addLFFrequency(int lengthClass, float number, Map<Integer, Float> frequencies) {
        Float standardiseSampleSpecieFrequency = frequencies.computeIfAbsent(lengthClass, k -> 0.f);
        standardiseSampleSpecieFrequency += number;
        frequencies.put(lengthClass, standardiseSampleSpecieFrequency);
    }

    public static class StandardizeSpeciesCountModel {

        protected final Species species;

        float oldCount;

        float newCount;

        public StandardizeSpeciesCountModel(Species species) {
            this.species = species;
        }

        public Species getSpecies() {
            return species;
        }

        public float getOldCount() {
            return oldCount;
        }

        public float getNewCount() {
            return newCount;
        }

        public void addOldCount(float oldCount) {
            this.oldCount += oldCount;
        }

        public void addNewCount(float newCount) {
            this.newCount += newCount;
        }

        float computedOldCount(Collection<SampleSpecies> sampleSpecies) {
            float result = 0;
            for (SampleSpecies sampleSpecy : sampleSpecies) {
                if (!sampleSpecy.isSampleSpeciesFrequencyEmpty()) {
                    for (SampleSpeciesFrequency sampleSpeciesFrequency : sampleSpecy.getSampleSpeciesFrequency()) {
                        result += sampleSpeciesFrequency.getNumberExtrapolated();
                    }
                }
            }
            float intResult = result;
            addOldCount(intResult);
            return intResult;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof StandardizeSpeciesCountModel)) {
                return false;
            }
            StandardizeSpeciesCountModel that = (StandardizeSpeciesCountModel) o;
            return species.equals(that.species);
        }

        @Override
        public int hashCode() {
            return species.hashCode();
        }

    }
}
