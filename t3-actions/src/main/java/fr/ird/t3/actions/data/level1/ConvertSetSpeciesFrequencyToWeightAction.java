/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.CountAndWeight;
import fr.ird.t3.entities.cache.LengthWeightConversionCache;
import fr.ird.t3.entities.cache.WeightCategoryTreatmentCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.SetSpeciesCatWeightTopiaDao;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.nuiton.util.TimeLog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Converts {@link SetSpeciesFrequency} to {@link SetSpeciesCatWeight}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3.1
 */
public class ConvertSetSpeciesFrequencyToWeightAction extends AbstractLevel1Action {

    @InjectDAO(entityType = SetSpeciesCatWeight.class)
    private SetSpeciesCatWeightTopiaDao setSpeciesCatWeightDAO;
    private WeightCategoryTreatmentCache weightCategoryCache;
    private LengthWeightConversionCache conversionHelper;

    private int nbTreatedSets;
    private float nbTreatedFishesInSamples;
    private float nbCreatedFishesInSetSpeciesFrequency;

    public ConvertSetSpeciesFrequencyToWeightAction() {
        super(Level1Step.CONVERT_SET_SPECIES_FREQUENCY_TO_WEIGHT);
    }

    @SuppressWarnings("unused")
    public int getNbTreatedSets() {
        return nbTreatedSets;
    }

    public void setNbTreatedSets(int nbTreatedSets) {
        this.nbTreatedSets = nbTreatedSets;
    }

    @SuppressWarnings("unused")
    public float getNbTreatedFishesInSamples() {
        return nbTreatedFishesInSamples;
    }

    public void setNbTreatedFishesInSamples(int nbTreatedFishesInSamples) {
        this.nbTreatedFishesInSamples = nbTreatedFishesInSamples;
    }

    @SuppressWarnings("unused")
    public float getNbCreatedFishesInSetSpeciesFrequency() {
        return nbCreatedFishesInSetSpeciesFrequency;
    }

    public void setNbCreatedFishesInSetSpeciesFrequency(int nbCreatedFishesInSetSpeciesFrequency) {
        this.nbCreatedFishesInSetSpeciesFrequency = nbCreatedFishesInSetSpeciesFrequency;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        conversionHelper = getT3TopiaPersistenceContext().get().newLengthWeightConversionSimpleCache();
        weightCategoryCache = getT3TopiaPersistenceContext().get().newWeightCategoryTreatmentCache();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() {
        Set<Trip> trips = samplesByTrip.keySet();
        setNbSteps(samplesByTrip.size() + trips.size());
        for (Trip trip : trips) {
            Collection<Sample> samples = samplesByTrip.get(trip);
            logTreatedAndNotSamplesforATrip(trip, samples);
            long s0 = TimeLog.getTime();
            doExecuteTrip(trip, samples);
            String tripStr = decorate(trip, DecoratorService.WITH_ID);
            getTimeLog().log(s0, "treat trip " + tripStr);
            // flush transaction otherwise too much data in memory
            flushTransaction("Flush transaction for " + tripStr);
        }
        return true;
    }

    private void doExecuteTrip(Trip trip, Collection<Sample> samples) {
        incrementsProgression();
        Set<Activity> tripActivities = new HashSet<>();
        // extrapolate for each sample set number to set
        for (Sample sample : samples) {
            long s0 = TimeLog.getTime();
            incrementsProgression();
            String sampleStr = l(locale, "t3.level1.convertSetSpeciesFrequencyToWeight.sampleStr",
                    decorate(trip), sample.getSampleNumber());
            float nb = sample.getTotalStandardiseSampleSpeciesFrequencyNumber();
            addInfoMessage(l(locale, "t3.level1.convertSetSpeciesFrequencyToWeight.sample.nbFishes", sampleStr, nb));
            nbTreatedFishesInSamples += nb;
            for (SampleSet sampleSet : sample.getSampleSet()) {
                Activity activity = sampleSet.getActivity();
                boolean added = tripActivities.add(activity);
                if (added) {
                    long s1 = TimeLog.getTime();
                    // found a new activity to convert
                    convertFrequenciesToCatWeight(activity);
                    getTimeLog().log(s1, String.format("convertSetSpeciesFrequencyToWeight for %d sets.", tripActivities.size()));
                }
            }
            // mark sample as treated for this step of level 1 treatment
            markAsTreated(sample);
            getTimeLog().log(s0, String.format("treat sample %s", sampleStr));
        }
        nbTreatedSets += tripActivities.size();
        // mar trip as treated for this level 1 step
        markAsTreated(trip);
    }

    private void convertFrequenciesToCatWeight(Activity activity) {
        Collection<SetSpeciesFrequency> frequencies = activity.getSetSpeciesFrequency();
        Date date = activity.getDate();
        Ocean activityOcean = activity.getOcean();
        // group frequencies by species
        Multimap<Species, SetSpeciesFrequency> setSpeciesFrequencyBySpecies = SpeciesTopiaDao.groupBySpecies(frequencies);
        Multimap<Species, Integer> lengthClassesBySpecies = ArrayListMultimap.create();
        // collect all length classes by species
        SetSpeciesFrequencyTopiaDao.collectLengthClasses(setSpeciesFrequencyBySpecies, lengthClassesBySpecies);
        // get all available weight categories
        List<WeightCategoryTreatment> weightCategories = weightCategoryCache.forActivity(activity);
        for (Map.Entry<Species, Collection<SetSpeciesFrequency>> entry : setSpeciesFrequencyBySpecies.asMap().entrySet()) {
            Species species = entry.getKey();
            LengthWeightConversion conversion = conversionHelper.getConversions(species, activityOcean, 0, date);
            Collection<SetSpeciesFrequency> setSizes = entry.getValue();
            List<Integer> lengthClasses = new ArrayList<>(lengthClassesBySpecies.get(species));
            Collections.sort(lengthClasses);
            Map<WeightCategoryTreatment, CountAndWeight> countAndWeightsByCategories =
                    convertForSpecies(weightCategories, conversion, setSizes, lengthClasses);
            // now we can build for the given activity and species the entries in SetWeight
            fill(activity, species, countAndWeightsByCategories);
        }
    }

    private Map<WeightCategoryTreatment, CountAndWeight> convertForSpecies(List<WeightCategoryTreatment> weightCategories,
                                                                           LengthWeightConversion conversion,
                                                                           Collection<SetSpeciesFrequency> setSizes,
                                                                           List<Integer> lengthClasses) {
        Map<Integer, WeightCategoryTreatment> weightCategoryMap = conversionHelper.getWeightCategoriesDistribution(
                conversion, weightCategories, lengthClasses);
        Map<WeightCategoryTreatment, CountAndWeight> result = new HashMap<>();
        T3IOUtil.fillMapWithDefaultValue(result, weightCategoryMap.values(), CountAndWeight::new);
        for (SetSpeciesFrequency setSize : setSizes) {
            // get length class
            int lfLengthClass = setSize.getLfLengthClass();
            // find the correct category for this length class
            WeightCategoryTreatment weightCategory = weightCategoryMap.get(lfLengthClass);
            CountAndWeight countAndWeight = result.get(weightCategory);
            float number = setSize.getNumber();
            countAndWeight.addCount(number);
            // convert to weight from lf length class
            countAndWeight.addWeight(number * conversion.computeWeightFromLFLengthClass(lfLengthClass));
        }
        return result;
    }

    private void fill(Activity activity, Species species, Map<WeightCategoryTreatment, CountAndWeight> countAndWeightsByCategories) {
        for (Map.Entry<WeightCategoryTreatment, CountAndWeight> e2 : countAndWeightsByCategories.entrySet()) {
            WeightCategoryTreatment weightCategoryTreatment = e2.getKey();
            CountAndWeight countAndWeight = e2.getValue();
            float count = countAndWeight.getCount();
            // weight must be stored in tons but are in kilograms
            Float weight = countAndWeight.getWeight() / 1000;
            SetSpeciesCatWeight setSpeciesCatWeight = setSpeciesCatWeightDAO.create(
                    SetSpeciesCatWeight.PROPERTY_SPECIES, species,
                    SetSpeciesCatWeight.PROPERTY_WEIGHT_CATEGORY_TREATMENT, weightCategoryTreatment,
                    SetSpeciesCatWeight.PROPERTY_WEIGHT, weight,
                    SetSpeciesCatWeight.PROPERTY_COUNT, count);
            activity.addSetSpeciesCatWeight(setSpeciesCatWeight);
        }
    }
}
