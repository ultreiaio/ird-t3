/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import fr.ird.t3.actions.stratum.SampleStratum;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategory;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.models.WeightCompositionModelHelper;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Sample stratum for level 2 treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class L2SampleStratum extends SampleStratum<Level2Configuration, Level2Action, L2StratumConfiguration, L2SampleStratum> {

    private static final Log log = LogFactory.getLog(L2SampleStratum.class);
    /**
     * Total weight of the catch stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private final float catchStratumTotalWeight;
    /** Weight composition for all species found in catches. */
    private final WeightCompositionAggregateModel modelsForAllSpecies;
    /** Weight composition model for only species to fix. */
    private WeightCompositionAggregateModel modelsForSpeciesToFix;

    /**
     * Current total count of sample.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private int sampleStratumTotalCount;

    /**
     * Current sample total weight of the stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private float sampleStratumTotalWeight;

    private L2SampleStratum(L2StratumConfiguration configuration, L2CatchStratum catchStratum, Collection<Species> speciesToFix) {
        super(configuration, speciesToFix);
        this.catchStratumTotalWeight = catchStratum.getTotalCatchWeightForSpeciesToFix();
        this.modelsForAllSpecies = new WeightCompositionAggregateModel();
    }

    static L2SampleStratum newSampleStratum(L2StratumConfiguration stratumConfiguration,
                                            L2CatchStratum catchStratum,
                                            List<WeightCategoryTreatment> weightCategories,
                                            Collection<Species> species,
                                            Level2Action action) throws Exception {
        L2SampleStratum sampleStratum = new L2SampleStratum(stratumConfiguration, catchStratum, species);
        sampleStratum.init(action.getServiceContext(), weightCategories, action);
        return sampleStratum;
    }

    @Override
    public void close() throws IOException {
        super.close();
        modelsForAllSpecies.close();
        modelsForSpeciesToFix.close();
    }

    @Override
    protected L2SampleStratumLoader newLoader() {
        int oceanCode = getConfiguration().getZone().getOcean().getCode();
        switch (oceanCode) {
            case 1:
                return new L2SampleStratumLoaderAtlantic(this);
            case 2:
                return new L2SampleStratumLoaderIndian(this);
            default:
                throw new IllegalStateException("Not implemented for ocean with code " + oceanCode);
        }
    }

    public WeightCompositionAggregateModel getModelsForSpeciesToFix() {
        return modelsForSpeciesToFix;
    }

    public float getCatchStratumTotalWeight() {
        return catchStratumTotalWeight;
    }

    public int getSampleStratumTotalCount() {
        return sampleStratumTotalCount;
    }

    public float getSampleStratumTotalWeight() {
        return sampleStratumTotalWeight;
    }

    @Override
    protected void mergeNewActivities(T3ServiceContext serviceContext, Set<Activity> activities) {
        Set<Species> species = getSpeciesToFix();
        // all weights (for each species) for all weight categories found
        Map<WeightCategoryTreatment, Map<Species, Float>> weights = new HashMap<>();
        for (Activity activity : activities) {
            // split cat weight by weight category
            fillWeightsFromSamplesWeight(activity, weights, null);
            // obtain the set species frequencies for the current activity
            Collection<SetSpeciesFrequency> setSpeciesFrequencies = getSetSpeciesFrequency(activity);
            // compute sample count for this activity
            int newCount = computeSampleCount(setSpeciesFrequencies, species);
            // merge it with final total count
            sampleStratumTotalCount += newCount;
        }
        // add all weights to model
        for (Map.Entry<WeightCategoryTreatment, Map<Species, Float>> e : weights.entrySet()) {
            Map<Species, Float> speciesFloatMap = e.getValue();
            WeightCategory weightCategorySample = e.getKey();
            modelsForAllSpecies.addModel(weightCategorySample, speciesFloatMap);
        }
        // recompute the weight model for species to fix
        modelsForSpeciesToFix = modelsForAllSpecies.extractForSpecies(species);
        // recompute the total sample weight (for species to fix)
        sampleStratumTotalWeight = modelsForSpeciesToFix.getTotalModel().getTotalWeight();
        addMergedActivitiesCount(activities.size());
        log.info(String.format("sampleStratumTotalCount  = %d / sampleStratumTotalWeight = %f", getSampleStratumTotalCount(), getSampleStratumTotalWeight()));
    }

    @Override
    public String logSampleStratumLevel(int substitutionLevel, Level2Action messager) {
        Locale l = messager.getLocale();
        String title = l(l, "t3.level2.sampleStratum.resume.for.level",
                substitutionLevel,
                // Do not use #getNbActivites() since it is perhaps not yet set.
                // Use this internal state instead of the #getNbActivities() since activities can still not be set in the stratum
                // Fix http://forge.codelutin.com/issues/1907
                getNbMergedActivities(),
                getSampleStratumTotalCount(),
                getSampleStratumTotalWeight());
        return WeightCompositionModelHelper.decorateModel(
                messager.getDecoratorService(),
                title,
                modelsForAllSpecies,
                modelsForSpeciesToFix);
    }

    private int computeSampleCount(Collection<SetSpeciesFrequency> setSpeciesFrequencies, Collection<Species> speciesToFix) {
        int newCount = 0;
        for (SetSpeciesFrequency setSpeciesFrequency : setSpeciesFrequencies) {
            Species species = setSpeciesFrequency.getSpecies();
            if (speciesToFix.contains(species)) {
                newCount += setSpeciesFrequency.getNumber();
            }
        }
        return newCount;
    }
}
