/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.web_tomorrow.utils.suntimes.SunTimes;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Route;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.VesselActivity;
import fr.ird.t3.entities.type.T3Point;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.nuiton.i18n.I18n.l;

/**
 * Compute for each given trip the efforts of it.
 * <p/>
 * <strong>Note:</strong> to make this action, all trips must have a computed
 * rf2.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ComputeTripEffortsAction extends AbstractLevel0Action<ComputeTripEffortsConfiguration> {

    /**
     * Les codes des activités surlequel on ne calcule pas d'effort de pêche.
     *
     * @since 2.0
     */
    private static final ImmutableSet<Integer> VESSEL_ACTIVITY_CODES_TO_SKIP = ImmutableSet.of(
            4,  // route sans veille
            7,  // avarie
            8,  // à la cape
            10, // en attente
            15  // au port
    );
    private static final Log log = LogFactory.getLog(ComputeTripEffortsAction.class);
    private float totalTimeAtSeaN0;
    private float totalFishingTimeN0;
    private float totalSearchTimeN0;

    public ComputeTripEffortsAction() {
        super(Level0Step.COMPUTE_TRIP_EFFORTS);
    }

    @SuppressWarnings("unused")
    public float getTotalTimeAtSeaN0() {
        return totalTimeAtSeaN0;
    }

    public void setTotalTimeAtSeaN0(int totalTimeAtSeaN0) {
        this.totalTimeAtSeaN0 = totalTimeAtSeaN0;
    }

    @SuppressWarnings("unused")
    public float getTotalFishingTimeN0() {
        return totalFishingTimeN0;
    }

    public void setTotalFishingTimeN0(int totalFishingTimeN0) {
        this.totalFishingTimeN0 = totalFishingTimeN0;
    }

    @SuppressWarnings("unused")
    public float getTotalSearchTimeN0() {
        return totalSearchTimeN0;
    }

    public void setTotalSearchTimeN0(int totalSearchTimeN0) {
        this.totalSearchTimeN0 = totalSearchTimeN0;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() {
        boolean result = false;
        if (CollectionUtils.isNotEmpty(trips)) {
            int nbSteps = trips.stream().mapToInt(Trip::sizeRoute).sum();
            setNbSteps(2 * nbSteps);
            // do action for each data using the prepared action context
            for (Trip trip : trips) {
                if (trip.isRouteEmpty()) {
                    markTripAsTreated(trip);
                    continue;
                }
                result |= executeForTrip(trip);
            }
        }
        return result;
    }

    private boolean executeForTrip(Trip trip) throws TopiaException {
        List<Route> routes = (List<Route>) trip.getRoute();
        Route firstRoute = Objects.requireNonNull(routes.get(0));
        Route lastRoute = Objects.requireNonNull(Iterables.getLast(routes));
        for (Route route : routes) {
            totalTimeAtSeaN0 += 24;
            // compute fishing time
            Float fishingTime = computeFishingTime(firstRoute, lastRoute, route);
            route.setComputedFishingTimeN0(fishingTime);
            incrementsProgression();
            if (fishingTime == null) {
                route.setComputedSearchTimeN0(null);
                incrementsProgression();
                continue;
            }
            totalFishingTimeN0 += fishingTime;
            // compute search time
            float searchTime = computeTripSearchTime(route, fishingTime);
            totalSearchTimeN0 += searchTime;
            route.setComputedSearchTimeN0(searchTime);
            incrementsProgression();
            String message = l(locale, "t3.level0.computeTripEffort",
                    decorate(route), route.getComputedFishingTimeN0(), route.getComputedSearchTimeN0());
            addInfoMessage(message);
            log.info(message);
        }
        markTripAsTreated(trip);
        return true;
    }

    private Float computeFishingTime(Route firstRoute, Route lastRoute, Route route) throws TopiaException {
        Collection<Activity> activities = route.getActivity();
        if (Objects.equals(firstRoute, route) || Objects.equals(lastRoute, route)) {
            return computeSetFishingTimeForFirstOrLayDay(activities);
        }
        Date date = route.getDate();
        if (!canComputeSetFishingTime(activities)) {
            String message = l(locale, "t3.level0.computeTripEffortSkipForDay", decorate(route));
            addInfoMessage(message);
            log.info(message);
            return null;
        }
        T3Point point;
        if (activities.size() == 1) {
            // only one activity for this day,
            Activity activity = activities.iterator().next();
            point = activity.toPoint();
            log.debug(String.format("Day [%s] , single activity,  point : %s", date, point));
        } else {
            // compute barycenter for all activities of the day
            point = routeDAO.getBarycenterForActivitiesOfADay(route);
            log.debug(String.format("Day [%s] Computed barycenter point %s", date, point));
        }
        // compute day duration for this point and day (in minutes)
        double duration = SunTimes.getDayDurationInMonths(date, point.getX(), point.getY());
        log.info(String.format("Day [%s] duration = %s", date, duration));
        return (float) duration;
    }

    private float computeTripSearchTime(Route route, float fishingTime) {
        // compute sum of catches time
        float catchesTime = route.getTotalSetsDuration();
        log.debug(String.format("Total catches time = %s", catchesTime));
        float result = fishingTime - catchesTime;
        log.debug(String.format("%s - trip search time = %s", decorate(route), result));
        return result;
    }

    private float computeSetFishingTimeForFirstOrLayDay(Collection<Activity> activities) {
        float result = 0f;
        for (Activity activity : activities) {
            result += activity.getFishingTime();
        }
        return result;
    }

    private boolean canComputeSetFishingTime(Collection<Activity> activities) {
        boolean result = false;
        for (Activity activity : activities) {
            VesselActivity vesselActivity = activity.getVesselActivity();
            if (!VESSEL_ACTIVITY_CODES_TO_SKIP.contains(vesselActivity.getCode())) {
                result = true;
                break;
            }
        }
        return result;
    }

}
