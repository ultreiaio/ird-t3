package fr.ird.t3.actions.data.level3;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.type.T3Date;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class L3StratumConfiguration extends StratumConfiguration<Level3Configuration, Level3Action> {

    private final Map<String, Integer> stratumMinimumSampleCount;

    L3StratumConfiguration(Level3InputContext inputContext,
                           SchoolType schoolType,
                           ZoneStratumAware zone,
                           WeightCategoryTreatment weightCategoryTreatment,
                           T3Date beginDate,
                           int stratumIndex,
                           Collection<ZoneStratumAware> zones,
                           List<WeightCategoryTreatment> weightCategoryTreatments,
                           Map<String, Integer> stratumMinimumSampleCount) {
        super(
                inputContext,
                zone,
                schoolType,
                weightCategoryTreatment,
                beginDate,
                beginDate.incrementsMonths(inputContext.getConfiguration().getTimeStep() - 1),
                zones,
                stratumMinimumSampleCount,
                weightCategoryTreatments,
                stratumIndex);
        this.stratumMinimumSampleCount = stratumMinimumSampleCount;
    }

    public Map<String, Integer> getStratumMinimumSampleCount() {
        return stratumMinimumSampleCount;
    }
}
