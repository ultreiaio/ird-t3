/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.CompleteTrip;
import fr.ird.t3.entities.data.RaisingFactor2;
import fr.ird.t3.entities.data.RaisingFactor2TopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryTopiaDao;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.HarbourTopiaDao;
import fr.ird.t3.entities.reference.RF1SpeciesForFleet;
import fr.ird.t3.entities.reference.RF1SpeciesForFleetTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.VesselSimpleTypeTopiaDao;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Action to compute Raising Factor 2 on each trip and fill the
 * {@link RaisingFactor2} table.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ComputeRF2Action extends AbstractLevel0Action<ComputeRF2Configuration> {

    private static final Log log = LogFactory.getLog(ComputeRF2Action.class);
    @InjectEntitiesById(entityType = Harbour.class)
    private List<Harbour> landingHarbours;
    private int nbStratum;
    private int nbTripsWithRF2;
    @InjectDAO(entityType = RaisingFactor2.class)
    private RaisingFactor2TopiaDao raisingFactor2DAO;
    @InjectDAO(entityType = Harbour.class)
    private HarbourTopiaDao harbourDAO;
    @InjectDAO(entityType = Country.class)
    private CountryTopiaDao countryDAO;
    @InjectDAO(entityType = VesselSimpleType.class)
    private VesselSimpleTypeTopiaDao vesselSimpleTypeDAO;
    @InjectDAO(entityType = RF1SpeciesForFleet.class)
    private RF1SpeciesForFleetTopiaDao rF1SpecieForFleetDAO;
    /** usable species group by country */
    private Multimap<Country, Species> speciesByCountry;
    /** usable complete trips group by vessel */
    private ListMultimap<Vessel, CompleteTrip> completeTripsByVessel;

    public ComputeRF2Action() {
        super(Level0Step.COMPUTE_RF2);
    }

    @SuppressWarnings("unused")
    public int getNbStratum() {
        return nbStratum;
    }

    public void setNbStratum(int nbStratum) {
        this.nbStratum = nbStratum;
    }

    @SuppressWarnings("unused")
    public int getNbTripsWithRF2() {
        return nbTripsWithRF2;
    }

    public void setNbTripsWithRF2(int nbTripsWithRF2) {
        this.nbTripsWithRF2 = nbTripsWithRF2;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        boolean configurationEmpty = getConfiguration().isConfigurationEmpty();
        if (configurationEmpty) {
            // use all landingHarbours
            landingHarbours = harbourDAO.findAll();
            // use all vesselSimpleTypes
            vesselSimpleTypes = vesselSimpleTypeDAO.findAll();
            // use all fleets
            fleets = countryDAO.findAll();
        }
        // get all species usable for rf1 for all selected fleet countries
        this.speciesByCountry = rF1SpecieForFleetDAO.getSpeciesForCountry(fleets);
        List<Trip> tripList = getUsableTrips(landingHarbours, true);
        log.info("Trip count: " + tripList.size());
        setTrips(tripList);
        completeTripsByVessel = ArrayListMultimap.create();
        // get all trips group by the vessel
        ListMultimap<Vessel, Trip> tripsByVessel = TripTopiaDao.groupByVessel(tripList);
        log.info("Trip vessel count: " + tripsByVessel.keySet().size());

        // compute for each vessel list of complete trip
        for (Map.Entry<Vessel, Collection<Trip>> entry : tripsByVessel.asMap().entrySet()) {
            Vessel vessel = entry.getKey();
            List<Trip> tripsForVessel = new ArrayList<>(entry.getValue());
            log.info("Trip vessel [" + vessel.getLabel1() + "] count: " + tripsForVessel.size());
            TripTopiaDao.sortTrips(tripsForVessel);
            // get all complete trips
            List<CompleteTrip> completeTrips = tripDAO.toCompleteTrip(tripsForVessel);
            log.info("Complete trip vessel [" + vessel.getLabel1() + "] count: " + completeTrips.size());
            completeTripsByVessel.putAll(vessel, completeTrips);
        }

    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() {
        boolean result = false;
        if (CollectionUtils.isNotEmpty(trips)) {
            setNbSteps(landingHarbours.size() * fleets.size() * vesselSimpleTypes.size());

            for (Harbour harbour : landingHarbours) {
                String harbourStr = harbour.getLabel1();
                for (Country fleet : fleets) {
                    String countryStr = fleet.getLabel1();

                    Collection<Species> species = speciesByCountry.get(fleet);

                    boolean computeRf2 = !getConfiguration().isConfigurationEmpty();

                    if (computeRf2 && species.isEmpty()) {
                        String message = l(locale, "t3.level0.computeRF2.skip.missing.species", harbourStr, countryStr);
                        log.info(message);
                        addInfoMessage(message);
                        computeRf2 = false;
                    }
                    for (VesselSimpleType vesselSimpleType : vesselSimpleTypes) {
                        incrementsProgression();
                        // get all trip usable for this stratum
                        Collection<Vessel> stratumVessels = getStratumVessel(fleet, vesselSimpleType);
                        String vesselSimpleTypeStr = vesselSimpleType.getLabel1();
                        // obtain all trips for harbour / fleet / vesselSimpleType
                        List<CompleteTrip> completeTrips = getStratumCompleteTrips(stratumVessels, harbour);
                        log.info(String.format("For [%s/%s/%s] nb trips = %d", harbourStr, countryStr, vesselSimpleTypeStr, completeTrips.size()));
                        if (completeTrips.isEmpty()) {
                            // no trip for this stratum
                            continue;
                        }
                        Multimap<T3Date, CompleteTrip> tripsByMonth = TripTopiaDao.splitTripsByMonth(completeTrips);
                        log.info(String.format("found %d months.", tripsByMonth.keySet().size()));
                        for (Map.Entry<T3Date, Collection<CompleteTrip>> entry : tripsByMonth.asMap().entrySet()) {
                            T3Date month = entry.getKey();
                            Collection<CompleteTrip> stratumTrips = entry.getValue();
                            nbStratum++;
                            boolean computeThisStratum = computeRf2;
                            if (computeRf2) {
                                // check that at least on trip miss logbook
                                computeThisStratum = false;
                                for (CompleteTrip completeTrip : stratumTrips) {
                                    for (Trip trip : completeTrip) {
                                        if (trip.getLogbookAvailability() == 0) {
                                            // no logbook on this
                                            computeThisStratum = true;
                                            String message = l(locale, "t3.level0.computeRF2.found.missing.logbook", harbourStr, countryStr, vesselSimpleTypeStr, month, nbStratum, decorate(trip));
                                            log.info(message);
                                            addInfoMessage(message);
                                        }
                                    }
                                }
                                if (!computeThisStratum) {
                                    String message = l(locale, "t3.level0.computeRF2.skip.missing.logbook", harbourStr, countryStr, vesselSimpleTypeStr, month, nbStratum);
                                    log.info(message);
                                    addInfoMessage(message);
                                }
                            }
                            String message = l(locale, "t3.level0.computeRF2.nbTrips.for.stratum",
                                    harbourStr,
                                    countryStr,
                                    vesselSimpleTypeStr,
                                    month,
                                    nbStratum,
                                    stratumTrips.size());
                            log.info(message);
                            addInfoMessage(message);
                            executeForStratum(harbour, fleet, vesselSimpleType, month, species, stratumTrips, computeThisStratum);
                        }
                    }
                }
            }
            result = true;
        }
        return result;
    }

    private Collection<Vessel> getStratumVessel(Country fleet, VesselSimpleType vesselSimpleType) {
        Set<Vessel> result = new HashSet<>();
        for (Vessel vessel : completeTripsByVessel.keys()) {
            if (vesselSimpleType.equals(vessel.getVesselType().getVesselSimpleType()) &&
                    fleet.equals(vessel.getFleetCountry())) {
                result.add(vessel);
            }
        }
        return result;
    }

    private List<CompleteTrip> getStratumCompleteTrips(Collection<Vessel> vessels, Harbour harbour) {
        List<CompleteTrip> result = new ArrayList<>();
        for (Vessel vessel : vessels) {
            // get all complete trips
            List<CompleteTrip> completeTrips = completeTripsByVessel.get(vessel);
            Iterator<CompleteTrip> itr = completeTrips.iterator();
            while (itr.hasNext()) {
                CompleteTrip completeTrip = itr.next();
                Trip trip = completeTrip.getLandingTrip();
                if (harbour.equals(trip.getLandingHarbour())) {
                    // keep it for this stratum
                    result.add(completeTrip);
                    // complete trip no more usable for another stratum
                    itr.remove();
                }
            }
        }
        return result;
    }

    private void executeForStratum(Harbour harbour,
                                   Country fleet,
                                   VesselSimpleType vesselSimpleType,
                                   T3Date month,
                                   Collection<Species> species,
                                   Collection<CompleteTrip> trips,
                                   boolean computeRf2) throws TopiaException {

        Date date = month.toBeginDate();
        // get rf2 (if already exists)
        RaisingFactor2 raisingFactor = raisingFactor2DAO.forProperties(
                RaisingFactor2.PROPERTY_COUNTRY, fleet,
                RaisingFactor2.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType,
                RaisingFactor2.PROPERTY_HARBOUR, harbour,
                RaisingFactor2.PROPERTY_MONTH, date
        ).findAnyOrNull();
        boolean rFCreated = false;
        if (raisingFactor == null) {
            // creates it, was not already found
            raisingFactor = raisingFactor2DAO.create(
                    RaisingFactor2.PROPERTY_COUNTRY, fleet,
                    RaisingFactor2.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType,
                    RaisingFactor2.PROPERTY_HARBOUR, harbour,
                    RaisingFactor2.PROPERTY_MONTH, date);
            rFCreated = true;
        }
        // do the rf2 stratum computation
        float rf2 = computeRf2 ? computeRF2ForStratum(trips, species) : 1f;
        if (computeRf2) {
            addInfoMessage(l(locale, "t3.level0.computeRF2.computed.rf2.for.stratum", rf2));
        }
        // assign rf2 value to the entity
        raisingFactor.setRaisingFactorValue(rf2);
        if (!rFCreated) {
            // must update it
            raisingFactor2DAO.update(raisingFactor);
        }
        // set rf2 to all trips
        for (CompleteTrip trip : trips) {
            String tripStr = decorate(trip);
            if (rf2 != 1) {
                nbTripsWithRF2++;
            }
            String message = l(locale, "t3.level0.computeRF1.resume.rf2.for.trip", tripStr, rf2);
            log.info(message);
            addInfoMessage(message);
            trip.applyRf2(rf2);
            markTripAsTreated(trip);
        }
    }

    private float computeRF2ForStratum(Collection<CompleteTrip> trips, Collection<Species> species) {
        float totalLandingWeight = 0f;
        float totalCatchWeight = 0f;
        for (CompleteTrip trip : trips) {
            float tripLandingWeight = trip.getElementaryLandingTotalWeight(species);
            totalLandingWeight += tripLandingWeight;
            String tripStr = decorate(trip, DecoratorService.WITH_ID);
            if (tripLandingWeight > 0) {
                // there is some landing
                float tripCatchWeight;
                if (trip.getRf1() == null) {
                    tripCatchWeight = trip.getElementaryCatchTotalWeight(species);
                } else {
                    tripCatchWeight = trip.getElementaryCatchTotalWeightRf1(species);
                }
                totalCatchWeight += tripCatchWeight;
                addInfoMessage(l(locale, "t3.level0.computeRF2.resume.rf1.for.trip", tripStr, tripCatchWeight, tripLandingWeight));
            } else {
                // this trip does not have any landing catches for given species
                addWarningMessage(l(locale, "t3.level0.computeRF2.resume.skip.for.trip", tripStr));
            }
        }
        addInfoMessage(l(locale, "t3.level0.computeRF2.resume.total.rf1", totalCatchWeight, totalLandingWeight));
        float rf2 = 1;
        if (totalCatchWeight > 0) {
            rf2 = totalLandingWeight / totalCatchWeight;
        }
        log.debug(String.format("Computed rf2 %s", rf2));
        return rf2;
    }

    private void markTripAsTreated(CompleteTrip trip) {
        for (Trip trip1 : trip) {
            markTripAsTreated(trip1);
        }
    }

}
