/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.data.WellPlan;
import fr.ird.t3.entities.data.WellSetAllSpecies;
import fr.ird.t3.entities.data.WellSetAllSpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryWellPlan;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Compute well plan weight categories proportions for all well of selected
 * trips.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class ComputeWellPlanWeightCategoriesProportionsAction extends AbstractLevel0Action<ComputeWellPlanWeightCategoriesProportionsConfiguration> {

    private float totalWellPlanWeight;
    private float totalWellSetAllSpeciesWeight;
    private int nbWell;
    private int nbWellWithPlan;
    private int nbWellWithNoPlan;
    @InjectDAO(entityType = WellSetAllSpecies.class)
    private WellSetAllSpeciesTopiaDao wellSetAllSpeciesDAO;

    public ComputeWellPlanWeightCategoriesProportionsAction() {
        super(Level0Step.COMPUTE_WELL_PLAN_WEIGHT_CATEGORIES_PROPORTIONS);
    }
    @SuppressWarnings("unused")
    public int getNbWell() {
        return nbWell;
    }

    public void setNbWell(int nbWell) {
        this.nbWell = nbWell;
    }
    @SuppressWarnings("unused")
    public int getNbWellWithPlan() {
        return nbWellWithPlan;
    }

    public void setNbWellWithPlan(int nbWellWithPlan) {
        this.nbWellWithPlan = nbWellWithPlan;
    }
    @SuppressWarnings("unused")
    public int getNbWellWithNoPlan() {
        return nbWellWithNoPlan;
    }

    public void setNbWellWithNoPlan(int nbWellWithNoPlan) {
        this.nbWellWithNoPlan = nbWellWithNoPlan;
    }
    @SuppressWarnings("unused")
    public float getTotalWellPlanWeight() {
        return totalWellPlanWeight;
    }

    public void setTotalWellPlanWeight(float totalWellPlanWeight) {
        this.totalWellPlanWeight = totalWellPlanWeight;
    }
    @SuppressWarnings("unused")
    public float getTotalWellSetAllSpeciesWeight() {
        return totalWellSetAllSpeciesWeight;
    }

    public void setTotalWellSetAllSpeciesWeight(float totalWellSetAllSpeciesWeight) {
        this.totalWellSetAllSpeciesWeight = totalWellSetAllSpeciesWeight;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() {
        boolean result = false;
        if (CollectionUtils.isNotEmpty(trips)) {
            setNbSteps(trips.size());
            // do action for each data using the prepared action context
            Set<Well> wellTreated = new HashSet<>();
            for (Trip trip : trips) {
                result |= executeForTrip(trip, wellTreated);
            }
        }
        return result;
    }

    private boolean executeForTrip(Trip trip, Set<Well> wellTreated) {
        if (trip.isWellNotEmpty()) {
            for (Well well : trip.getWell()) {
                if (wellTreated.add(well)) {
                    // not already treated well
                    nbWell++;
                    if (well.isWellPlanEmpty()) {
                        // no well plan
                        nbWellWithNoPlan++;
                    } else {
                        nbWellWithPlan++;
                        // with well plan
                        treatWell(trip, well);
                    }
                }
            }
        }
        incrementsProgression();
        markTripAsTreated(trip);
        return true;
    }

    private void treatWell(Trip trip, Well well) {
        // split well Plan by their activity
        Multimap<Activity, WellPlan> plansByActivity = ActivityTopiaDao.groupByActivity(well.getWellPlan());
        // for each activity remove the species information from his wellPlans
        float wellTotalWeight = 0.0f;
        for (Activity activity : plansByActivity.keySet()) {
            Collection<WellPlan> wellPlans = plansByActivity.get(activity);
            Map<WeightCategoryWellPlan, Float> totalWeightByCategory = new HashMap<>();
            float totalWeight = 0f;
            // compute total weight for each weight category
            for (WellPlan wellPlan : wellPlans) {
                WeightCategoryWellPlan category = wellPlan.getWeightCategoryWellPlan();
                Float categoryTotalWeight = totalWeightByCategory.get(category);
                if (categoryTotalWeight == null) {
                    categoryTotalWeight = 0f;
                }
                float weight = wellPlan.getWeight();
                categoryTotalWeight += weight;
                totalWeight += weight;
                totalWeightByCategory.put(category, categoryTotalWeight);
            }
            wellTotalWeight += totalWeight;
            // create now all WellSetAllSpecies for the given activity (one for each weight category found for this well plan)
            for (Map.Entry<WeightCategoryWellPlan, Float> e2 :
                    totalWeightByCategory.entrySet()) {
                WeightCategoryWellPlan category = e2.getKey();
                Float weight = e2.getValue();
                Float propWeight = weight / totalWeight;
                WellSetAllSpecies wellSetAllSpecies = wellSetAllSpeciesDAO.create(
                        WellSetAllSpecies.PROPERTY_WEIGHT_CATEGORY_WELL_PLAN, category,
                        WellSetAllSpecies.PROPERTY_ACTIVITY, activity,
                        WellSetAllSpecies.PROPERTY_WEIGHT, weight,
                        WellSetAllSpecies.PROPERTY_PROP_WEIGHT, propWeight
                );
                well.addWellSetAllSpecies(wellSetAllSpecies);
                totalWellSetAllSpeciesWeight += weight;
            }
            totalWeightByCategory.clear();
        }
        totalWellPlanWeight += wellTotalWeight;
        Collection<WellSetAllSpecies> wellSetAllSpecies = well.getWellSetAllSpecies();
        String message = l(locale, "t3.level0.computeWellPlanWeightCategoriesProportions.well.withWellPlan",
                decorate(trip) + " - Well " + decorate(well), wellSetAllSpecies.size(), wellTotalWeight);
        addInfoMessage(message);
        for (WellSetAllSpecies aWellSetAllSpecies : wellSetAllSpecies) {
            WeightCategoryWellPlan weightCategory = aWellSetAllSpecies.getWeightCategoryWellPlan();
            message = l(locale, "t3.level0.computeWellPlanWeightCategoriesProportions.resume.for.weightCategory",
                    weightCategory.getLabel1(),
                    aWellSetAllSpecies.getWeight(),
                    aWellSetAllSpecies.getPropWeight(),
                    decorate(aWellSetAllSpecies.getActivity()));
            addInfoMessage(message);
        }
    }

}
