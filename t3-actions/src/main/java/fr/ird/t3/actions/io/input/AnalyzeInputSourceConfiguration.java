/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.io.input.T3InputConfiguration;
import fr.ird.t3.io.input.T3InputProvider;

import java.io.File;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration for action {@link AnalyzeInputSourceAction}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AnalyzeInputSourceConfiguration implements InputSourceConfiguration {

    private static final long serialVersionUID = 1L;
    protected boolean useWells;
    protected boolean canCreateVessel;
    protected boolean createVirtualVessel;
    protected T3InputProvider inputProvider;
    protected File inputFile;
    /**
     * Flag to load only samples only trips
     *
     * @see TripType#SAMPLEONLY
     */
    private boolean useSamplesOnly;
    /**
     * Flag to authorize virtual activity creation (when logbooks are missing).
     * <p>
     * By default, let's always authorize it.
     * <p>
     * <b>Note:</b> This option is always at false if {@link #useSamplesOnly} is on.
     *
     * @see TripType#LOGBOOKMISSING
     * @see TripType#STANDARD
     */
    private boolean canCreateVirtualActivity;
    private TripType tripType;

    public static AnalyzeInputSourceConfiguration newConfiguration(
            T3InputProvider inputProvider,
            File inputFile,
            boolean useWells,
            boolean canCreateVessel,
            boolean createVirtualVessel,
            boolean useSamplesOnly,
            boolean canCreateVirtualActivity) {
        AnalyzeInputSourceConfiguration result = new AnalyzeInputSourceConfiguration();
        result.setInputFile(inputFile);
        result.setInputProvider(inputProvider);
        result.setUseWells(useWells);
        result.setTripType(TripType.getTripType(useSamplesOnly, canCreateVirtualActivity));
        result.setCanCreateVessel(canCreateVessel);
        result.setCreateVirtualVessel(createVirtualVessel);
        result.setCanCreateVirtualActivity(canCreateVirtualActivity);
        result.setUseSamplesOnly(useSamplesOnly);
        return result;
    }

    @Override
    public boolean isUseWells() {
        return useWells;
    }

    @Override
    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    @Override
    public TripType getTripType() {
        return tripType;
    }

    @Override
    public void setTripType(TripType tripType) {
        this.tripType = tripType;
    }

    @Override
    public boolean isCanCreateVessel() {
        return canCreateVessel;
    }

    @Override
    public void setCanCreateVessel(boolean canCreateVessel) {
        this.canCreateVessel = canCreateVessel;
    }

    @Override
    public boolean isCreateVirtualVessel() {
        return createVirtualVessel;
    }

    @Override
    public void setCreateVirtualVessel(boolean createVirtualVessel) {
        this.createVirtualVessel = createVirtualVessel;
    }

    @Override
    public T3InputProvider getInputProvider() {
        return inputProvider;
    }

    @Override
    public void setInputProvider(T3InputProvider inputProvider) {
        this.inputProvider = inputProvider;
    }

    @Override
    public File getInputFile() {
        return inputFile;
    }

    @Override
    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    @Override
    public String getName(Locale locale) {
        return l(locale, "t3.input.AnalyzeInputSource");
    }

    @Override
    public boolean isUseSamplesOnly() {
        return useSamplesOnly;
    }

    @Override
    public void setUseSamplesOnly(boolean useSamplesOnly) {
        this.useSamplesOnly = useSamplesOnly;
    }

    @Override
    public boolean isCanCreateVirtualActivity() {
        return canCreateVirtualActivity;
    }

    @Override
    public void setCanCreateVirtualActivity(boolean canCreateVirtualActivity) {
        this.canCreateVirtualActivity = canCreateVirtualActivity;
    }

    T3InputConfiguration toInputConfiguration() {
        T3InputConfiguration result = new T3InputConfiguration();
        result.setCanCreateVessel(canCreateVessel);
        result.setCreateVirtualVessel(createVirtualVessel);
        result.setTripType(tripType);
        result.setUseWells(useWells);
        result.setCanCreateVirtualActivity(canCreateVirtualActivity);
        result.setUseSamplesOnly(useSamplesOnly);
        result.setInputFile(inputFile);
        return result;
    }
}
