/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import java.util.List;

/**
 * Configuration of a convertion of weight categories action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ConvertCatchesWeightCategoriesAction
 * @since 1.0
 */
public class ConvertCatchesWeightCategoriesConfiguration extends AbstractLevel0Configuration {

    public static final String LEGACY_VERSION = "legacy";
    private static final long serialVersionUID = 1L;
    /**
     * All converter available version.
     */
    private List<String> versions;

    /**
     * Version of converter to use.
     */
    private String version;

    public ConvertCatchesWeightCategoriesConfiguration() {
        super(Level0Step.CONVERT_CATCHES_WEIGHT_CATEGORIES);
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<String> getVersions() {
        return versions;
    }

    public void setVersions(List<String> versions) {
        this.versions = versions;
    }

    @SuppressWarnings("unused")
    public boolean isConfigurationEmpty() {
        return version == null || versions.isEmpty();

    }
}
