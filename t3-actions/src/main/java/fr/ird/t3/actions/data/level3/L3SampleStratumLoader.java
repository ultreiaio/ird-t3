/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import fr.ird.t3.actions.stratum.SampleStratumLoader;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Base level 2 sample stratum loader.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public abstract class L3SampleStratumLoader extends SampleStratumLoader<Level3Configuration, Level3Action, L3StratumConfiguration, L3SampleStratum> {

    private static final Log log = LogFactory.getLog(L3SampleStratumLoader.class);

    /**
     * The minimum sample total weight to obtain the correct substitution level.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration
     * and catch stratum total weight.
     *
     * @since 1.4
     */
    private final float minimumSampleTotalWeight;

    /**
     * The minimum sample count required to obtain the correct
     * substitution level for each species to fix.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration.
     */
    private final Map<String, Integer> minimumSampleCount;

    @InjectDAO(entityType = Activity.class)
    private ActivityTopiaDao activityDAO;

    L3SampleStratumLoader(L3SampleStratum sampleStratum) {
        super(sampleStratum);
        Level3Configuration configuration = sampleStratum.getLevelConfiguration();
        // compute minimumSampleTotalWeight
        float catchStratumTotalWeight = sampleStratum.getCatchStratumTotalWeight();
        float maximumWeightRatio = configuration.getStratumWeightRatio();
        minimumSampleTotalWeight = catchStratumTotalWeight / maximumWeightRatio;
        minimumSampleCount = sampleStratum.getConfiguration().getStratumMinimumCountBySpecie();
    }

    @Override
    protected Set<String> findActivityIds(String schoolTypeId, T3Date beginDate, T3Date endDate, String... zoneIds) {
        Set<String> result = new HashSet<>();
        StratumConfiguration<Level3Configuration, Level3Action> configuration = getSampleStratum().getConfiguration();
        for (String zoneId : zoneIds) {
            // on commence par récupérer les activités :
            // - dans la zone ET
            // - avec le bon type de banc
            // - dans la bonne période de temps
            List<String> activityIds = activityDAO.findAllActivityIdsForSampleStratum(
                    configuration.getZoneTableName(),
                    zoneId,
                    schoolTypeId == null ? null : configuration.getSchoolTypeIds(),
                    beginDate,
                    endDate);
            result.addAll(activityIds);
        }
        return result;
    }

    @Override
    public boolean isStratumOk() {
        L3SampleStratum stratum = getSampleStratum();
        // test if we have enough sample weight
        float sampleStratumTotalWeight = stratum.getSampleStratumTotalWeight();
        boolean minimumSampleTotalWeightOk = sampleStratumTotalWeight >= minimumSampleTotalWeight;
        if (!minimumSampleTotalWeightOk) {
            // need more sample weight
            log.info(String.format("Missing some weight (got: %s, but required minimum: %s).", sampleStratumTotalWeight, minimumSampleTotalWeight));
        }
        // get all species to fix
        Collection<Species> species = new HashSet<>(stratum.getSpeciesToFix());
        // retains only the one found in sample
        species.retainAll(stratum.getSampleSpecies());
        // test if we have enough sample count for each species to fix
        boolean minimumSampleCountOk = true;
        for (Species aSpecies : species) {
            int minCount = minimumSampleCount.get(aSpecies.getTopiaId());
            float sampleCount = stratum.getSpeciesTotalCount(aSpecies);
            if (sampleCount < minCount) {
                minimumSampleCountOk = false;
                // need more fishes count
                log.info(String.format("Missing fishes count for species %s (got: %s, but required minimum: %d ).", aSpecies.getLabel1(), sampleCount, minCount));
            }
        }
        // at least one test must be ok
        // see http://forge.codelutin.com/issues/3229
        return minimumSampleTotalWeightOk || minimumSampleCountOk;
    }
}
