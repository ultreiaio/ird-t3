/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import static org.nuiton.i18n.I18n.n;

/**
 * Configuration of a compute RF1 action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ComputeRF1Action
 * @since 1.0
 */
public class ComputeRF1Configuration extends AbstractLevel0Configuration {

    private static final long serialVersionUID = 1L;

    static {
        n("t3.common.rf1.minimumRate.acceptable");
        n("t3.common.rf1.maximumRate.acceptable");
    }

    /** Minimum rf1 rate acceptable. */
    private float minimumRate;
    /** Maximum rf1 rate acceptable. */
    private float maximumRate;
    /** Flag to use or not legacy behaviour when you treat trips without landings. */
    private boolean useLegacyBehaviourForTripWithoutLanding;

    public ComputeRF1Configuration() {
        super(Level0Step.COMPUTE_RF1);
    }

    public float getMinimumRate() {
        return minimumRate;
    }

    public void setMinimumRate(float minimumRate) {
        this.minimumRate = minimumRate;
    }

    public float getMaximumRate() {
        return maximumRate;
    }

    public void setMaximumRate(float maximumRate) {
        this.maximumRate = maximumRate;
    }

    public boolean isUseLegacyBehaviourForTripWithoutLanding() {
        return useLegacyBehaviourForTripWithoutLanding;
    }

    public void setUseLegacyBehaviourForTripWithoutLanding(boolean useLegacyBehaviourForTripWithoutLanding) {
        this.useLegacyBehaviourForTripWithoutLanding = useLegacyBehaviourForTripWithoutLanding;
    }
}
