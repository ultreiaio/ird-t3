/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input.test;

import fr.ird.t3.T3Config;
import fr.ird.t3.T3ConfigOption;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaApplicationContextBuilder;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.user.T3Users;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.mockito.Mockito;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.FileUtil;
import org.nuiton.version.Version;

import java.io.Closeable;
import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class FakeT3AvdthServiceContext implements TestRule, T3ServiceContext, Closeable {

    private static final Log log = LogFactory.getLog(FakeT3AvdthServiceContext.class);

    private final T3ServiceFactory serviceFactory = new T3ServiceFactory();
    private final boolean injectReferential;
    protected T3TopiaPersistenceContext transaction;
    private T3TopiaApplicationContext rootContext;
    private File testDir;
    private T3Config applicationConfiguration;
    private Locale locale;
    private String testName;
    private boolean initOk;
    private MSAccessTestConfiguration msConfig;

    FakeT3AvdthServiceContext(boolean injectReferential, MSAccessTestConfiguration msConfig) {
        this.injectReferential = injectReferential;
        this.msConfig = msConfig;
        setLocale(Locale.FRANCE);
    }

    @Override
    public T3Users getT3Users() {
        return null;
    }

    public boolean isInitOk() {
        return initOk;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    starting(description);
                    Assume.assumeTrue("Can't start test.", initOk);
                } catch (Exception e) {
                    Assume.assumeNoException("Can't start test: " + e.getMessage(), e);
                }
                try {
                    base.evaluate();
                } finally {
                    close();
                }
            }
        };
    }

    /** May be used in test to get a fresh transaction. */
    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return () -> transaction == null ? transaction = newTransaction() : transaction;
    }

    @Override
    public T3TopiaApplicationContext getApplicationContext() {
        return rootContext;
    }

    private T3TopiaPersistenceContext newTransaction() throws TopiaException {
        return rootContext.newPersistenceContext();
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public T3Config getApplicationConfiguration() {
        return applicationConfiguration;
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

    @Override
    public void setTransaction(T3TopiaPersistenceContext transaction) {
        this.transaction = transaction;
    }

    @Override
    public Date getCurrentDate() {
        return new Date();
    }

    public File getTestDir() {
        return testDir;
    }

    public String getTestName() {
        return testName;
    }

    private void starting(Description description) {
        testName = description.getMethodName();
        testDir = T3IOUtil.getTestSpecificDirectory(description.getTestClass(), description.getMethodName());
        log.info(String.format("Test dir = %s", testDir));
        Properties defaultProps = new Properties();
        defaultProps.put(T3ConfigOption.DATA_DIRECTORY.getKey(), testDir);
        File treatmentDirectory = new File(testDir, "treatment");
        FileUtil.createDirectoryIfNecessary(treatmentDirectory);
        T3Config realConfiguration = new T3Config(defaultProps) {
            @Override
            public void init() {
                parse();
            }
        };
        realConfiguration.init();
        Version t3DataVersion = realConfiguration.getT3DataVersion();
        applicationConfiguration = Mockito.mock(T3Config.class);
        Mockito.when(applicationConfiguration.getDataDirectory()).thenReturn(testDir);
        Mockito.when(applicationConfiguration.getTreatmentWorkingDirectory()).thenReturn(treatmentDirectory);
        Mockito.when(applicationConfiguration.getT3DataVersion()).thenReturn(t3DataVersion);
        Mockito.when(applicationConfiguration.getTreatmentWorkingDirectory(Mockito.anyString(), Mockito.anyBoolean())).thenCallRealMethod();
        Mockito.when(applicationConfiguration.getApplicationVersion()).thenReturn(realConfiguration.getApplicationVersion());
        if (msConfig != null) {
            boolean b = msConfig.beforeClass() && msConfig.setup(testName);
            if (!b) {
                initOk = false;
                return;
            }
        }
        rootContext = T3TopiaApplicationContextBuilder.forH2Referential(testDir.toPath(), injectReferential ? t3DataVersion : null).build();
        initOk=true;
    }

    @Override
    public void close() {
        if (rootContext != null && rootContext.isOpened()) {
            rootContext.close();
        }
    }

    public MSAccessTestConfiguration getMsConfig() {
        return msConfig;
    }
}
