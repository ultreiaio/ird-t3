/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input.test;

import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceAction;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceConfiguration;
import fr.ird.t3.actions.io.input.ImportInputSourceAction;
import fr.ird.t3.actions.io.input.ImportInputSourceConfiguration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Tests the action {@link ImportInputSourceAction}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class ImportInputSourceActionTestSupport {

    private static final Log log = LogFactory.getLog(ImportInputSourceActionTestSupport.class);

    @Rule
    public final FakeT3AvdthServiceContext serviceContext = new FakeT3AvdthServiceContext(true, createConfiguration());
    protected T3InputProvider inputProvider;
    protected File target;

    protected abstract MSAccessTestConfiguration createConfiguration();

    @Before
    public void setUp() throws IOException {
        boolean initOk = serviceContext.isInitOk();
        Assume.assumeTrue("Could not init db", initOk);
        MSAccessTestConfiguration msConfig = serviceContext.getMsConfig();
        String dbName = msConfig.dbName;
        log.debug("Do test for db " + dbName);
        File workingDirectory = serviceContext.getApplicationConfiguration().getTreatmentWorkingDirectory(dbName, true);
        // push in treatment directory the base to import
        target = new File(workingDirectory, dbName);
        log.debug(String.format("Will copy msAccess from %s to %s", msConfig.accessFile, target));
        FileUtils.copyFile(msConfig.getAccessFile(), target);
        inputProvider = serviceContext.newService(T3InputService.class).getProvider(msConfig.getProviderId());
    }

    @After
    public void tearDown() {
        serviceContext.close();
    }

    protected void testExecute(OceanFixtures fixture) throws Exception {
        testExecute(fixture, TripType.STANDARD);
    }

    protected void testExecute(OceanFixtures fixture, TripType tripType) throws Exception {
        testExecute(fixture.nbSafeWithoutWell(), fixture.nbUnsafeWithoutWell(), tripType);
    }

    public void testExecute(int nbSafe, int nbUnsafe, TripType tripType) throws Exception {

        MSAccessTestConfiguration msConfig = serviceContext.getMsConfig();
        if (msConfig.doTest(serviceContext.getTestName())) {
            AnalyzeInputSourceConfiguration analyzeActionConfiguration = AnalyzeInputSourceConfiguration.newConfiguration(
                    inputProvider,
                    target,
                    msConfig.isUseWells(),
                    false,
                    false,
                    tripType != null && tripType == TripType.SAMPLEONLY,
                    tripType == null || tripType == TripType.LOGBOOKMISSING);
            T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();
            T3ActionContext<AnalyzeInputSourceConfiguration> analyzeContext =
                    serviceFactory.newT3ActionContext(analyzeActionConfiguration, serviceContext);

            AnalyzeInputSourceAction analyzeAction = serviceFactory.newT3Action(AnalyzeInputSourceAction.class, analyzeContext);

            Assert.assertNotNull(analyzeAction);
            analyzeAction.run();

            Set<Trip> safeTrips = analyzeAction.getResultAsSet(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, Trip.class);
            Assert.assertNotNull(safeTrips);
            Assert.assertEquals(nbSafe, safeTrips.size());

            Set<Trip> unsafeTrips = analyzeAction.getResultAsSet(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, Trip.class);
            Assert.assertNotNull(unsafeTrips);
            Assert.assertEquals(nbUnsafe, unsafeTrips.size());
            log.info(String.format("[%s] safe : %d - unsafe : %d", msConfig.dbName, safeTrips.size(), unsafeTrips.size()));

            ImportInputSourceConfiguration importActionConfiguration = ImportInputSourceConfiguration.newConfiguration(analyzeActionConfiguration);
            importActionConfiguration.setTripsToImport(safeTrips);

            T3ActionContext<ImportInputSourceConfiguration> importContext = serviceFactory.newT3ActionContext(importActionConfiguration, serviceContext);
            // input db is safe, import it in h2 db
            long oldNbTrips;
            long newNbTrips;
            importActionConfiguration.setTripsToImport(safeTrips);
            oldNbTrips = serviceContext.getT3TopiaPersistenceContext().get().getTripDao().count();
            ImportInputSourceAction importAction = serviceFactory.newT3Action(ImportInputSourceAction.class, importContext);
            importAction.run();
            newNbTrips = serviceContext.getT3TopiaPersistenceContext().get().getTripDao().count();
            Assert.assertEquals(oldNbTrips + safeTrips.size(), newNbTrips);
        }
    }
}
