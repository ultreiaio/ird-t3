/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import com.google.common.collect.Multimap;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.T3EntityMap;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.AbstractTripTopiaDao;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.io.input.MissingForeignKey;
import fr.ird.t3.io.input.MissingForeignKeyInT3;
import fr.ird.t3.io.input.T3Input;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.io.input.access.LoadingTripHitModel;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.nuiton.i18n.I18n.l;

/**
 * Action to analyze an incoming input source.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AnalyzeInputSourceAction extends T3Action<AnalyzeInputSourceConfiguration> {

    public static final String RESULT_SAFE_TRIPS = "safeTrips";
    public static final String RESULT_UNSAFE_TRIPS = "unsafeTrips";
    public static final String RESULT_TRIPS_TO_REPLACE = "tripsToReplace";
    private static final Predicate<MissingForeignKey> WELL_PLAN_TO_ACTIVITY = foreignKey -> foreignKey.getSourceType() == T3EntityEnum.WellPlan && foreignKey.getTargetType() == T3EntityEnum.Activity;
    private static final Predicate<MissingForeignKey> SAMPLE_TO_WELL = foreignKey -> foreignKey.getSourceType() == T3EntityEnum.Sample && foreignKey.getTargetType() == T3EntityEnum.Well;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private static final Log log = LogFactory.getLog(AnalyzeInputSourceAction.class);
    @InjectDAO(entityType = Trip.class)
    protected TripTopiaDao tripDAO;
    private T3Input inputPilot;
    private boolean safe;

    @SuppressWarnings("unused")
    public Trip getTripToReplace(Trip trip) {
        Map<Trip, Trip> resultAsMap = getResultAsMap(RESULT_TRIPS_TO_REPLACE);
        Trip newTrip = null;
        if (resultAsMap != null) {
            newTrip = resultAsMap.get(trip);
        }
        return newTrip;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        File inputFile = getConfiguration().getInputFile();
        T3InputProvider inputProvider = getConfiguration().getInputProvider();
        // instantiate the input pilot to use
        T3Input pilot = inputProvider.newInstance(inputFile);
        setInputPilot(pilot);
        safe = true;
        try {
            // init pilot
            inputPilot.init(getConfiguration().toInputConfiguration());
            String[] analyzeErrors = inputPilot.getAnalyzeErrors();
            if (analyzeErrors != null && analyzeErrors.length > 0) {
                safe = false;
                // some errors on db
                StringBuilder sb = new StringBuilder(l(locale, "t3.import.error.on.analyze", analyzeErrors.length));
                for (String analyzeError : analyzeErrors) {
                    sb.append('\n').append(analyzeError);
                }
                addErrorMessage(sb.toString());
            }
            String[] analyzeWarnings = inputPilot.getAnalyzeWarnings();
            if (analyzeWarnings != null && analyzeWarnings.length > 0) {
                // some warnings on db
                StringBuilder sb = new StringBuilder(l(locale, "t3.import.warning.on.analyze", analyzeWarnings.length));
                for (String analyzeWarning : analyzeWarnings) {
                    sb.append('\n').append(analyzeWarning);
                }
                addWarningMessage(sb.toString());
            }
            // obtain the safe references from t3 database
            ReferenceEntityMap references = getSafeReferences(getT3TopiaPersistenceContext().get());
            // inject the safe references in the input
            inputPilot.setSafeReferences(references);
            int nbTrips = inputPilot.getNbTrips();
            setNbSteps(nbTrips);
            log.info(String.format("Real nb steps : %d", getNbSteps()));
        } catch (Exception e) {
            log.error("Error while init input pilot", e);
            safe = false;
            // something was wrong in pilot
            inputPilot.destroy();
        }
    }

    @Override
    protected void deletePreviousData() {
        // nothing to remove from anywhere...
    }

    @Override
    protected boolean executeAction() {
        if (!safe) {
            putResult(RESULT_SAFE_TRIPS, new HashSet<>());
            putResult(RESULT_UNSAFE_TRIPS, new HashSet<>());
            putResult(RESULT_TRIPS_TO_REPLACE, new HashMap<>());
            return false;
        }
        Map<Trip, T3EntityMap> tripsLoaded = null;
        try {
            // load fully trips from the input
            LoadingTripHitModel hitModel = new LoadingTripHitModel();
            hitModel.addPropertyChangeListener(LoadingTripHitModel.NB_TRIP_LOADED, evt -> incrementsProgression());
            TripType tripType = getConfiguration().getTripType();
            boolean samplesOnly = tripType == TripType.SAMPLEONLY;
            tripsLoaded = inputPilot.loadTrips(hitModel);
            boolean createVessel = getConfiguration().isCanCreateVessel();
            Map<Integer, Vessel> vessels = null;
            if (createVessel) {
                // load vessel from data source
                vessels = inputPilot.getNewVessels();
            }
            Set<Trip> safeTrips = new HashSet<>();
            Set<Trip> unsafeTrips = new HashSet<>();
            for (Map.Entry<Trip, T3EntityMap> e : tripsLoaded.entrySet()) {
                // trip to check
                Trip trip = e.getKey();
                // universe of used entities by this trip
                T3EntityMap entitiesUsed = e.getValue();
                if (samplesOnly && !entitiesUsed.containsKey(T3EntityEnum.Sample)) {
                    // trip with no sample in samplesOnly mode do not accept it
                    addWarningMessage(
                            l(locale, "t3.message.reject.trip.with.no.sample.in.samplesOnly.mode", decorate(trip)));
                    continue;
                }
                // all missing foreign keys
                List<MissingForeignKey> missingForeignKeys = inputPilot.getMissingForeignKeys(trip);
                if (createVessel) {
                    // remove check on vessel at this point will be checked by other way
                    entitiesUsed.remove(T3EntityEnum.Vessel);
                }
                // check the trip
                boolean tripSafe = checkTrip(trip, entitiesUsed, missingForeignKeys, vessels);
                if (tripSafe) {
                    // everything is fine for this trip, can be safely used
                    safeTrips.add(trip);
                } else {
                    // there is some errors on this trip, can not be safely used in T3
                    unsafeTrips.add(trip);
                    String message = l(locale, "t3.message.importable.trip.is.unsafe", decorate(trip));
                    addErrorMessage(message);
                }
            }

            if (CollectionUtils.isEmpty(unsafeTrips)) {
                // compute universe of trip to replace
                Map<Trip, Trip> tripsToReplace = getTripsToReplace(safeTrips);
                putResult(RESULT_TRIPS_TO_REPLACE, tripsToReplace);
                for (Trip trip : safeTrips) {
                    String message;
                    if (tripsToReplace.containsValue(trip)) {
                        // trip to replace
                        message = l(locale, "t3.message.importable.trip.exists", decorate(trip));
                    } else {
                        // new trip
                        message = l(locale, "t3.message.importable.trip.is.safe", decorate(trip));
                    }
                    addInfoMessage(message);
                }
            } else {
                putResult(RESULT_TRIPS_TO_REPLACE, Collections.emptyMap());
                for (Trip trip : safeTrips) {
                    String message = l(locale, "t3.message.importable.trip.is.safe", decorate(trip));
                    addInfoMessage(message);
                }
            }
            putResult(RESULT_SAFE_TRIPS, safeTrips);
            putResult(RESULT_UNSAFE_TRIPS, unsafeTrips);
            // while analyzing db, nothing to commit in db
            return false;
        } finally {
            if (tripsLoaded != null) {
                tripsLoaded.clear();
            }
            inputPilot.destroy();
        }
    }

    private boolean checkTrip(Trip trip, T3EntityMap entitiesUsed, List<MissingForeignKey> missingForeignKeys, Map<Integer, Vessel> newVessels) {
        boolean useWells = getConfiguration().isUseWells();
        boolean tripSafe;
        String tripStr = decorate(trip);
        Function<MissingForeignKey, String> missingFkToMessage = new MissingForeignKeyToMessage(tripStr);
        List<Well> wells = entitiesUsed.get(Well.class);
        boolean withWell = CollectionUtils.isNotEmpty(wells);
        if (!withWell) {
            // no well for this trip can remove all missing fk for wells
            addWarningMessage(l(locale, "t3.import.warning.no.wellPlan", decorate(trip)));
            // remove missing keys Sample->Well since there is no well
            removeWellMissingForeignKeys(missingForeignKeys, false, SAMPLE_TO_WELL, missingFkToMessage);
            // remove missing keys WellPlan->Activity since there is no well
            removeWellMissingForeignKeys(missingForeignKeys, true, WELL_PLAN_TO_ACTIVITY, missingFkToMessage);
        } else {
            // there is some wells for this trip
            if (!useWells) {
                withWell = removeWells(trip, wells, missingForeignKeys, missingFkToMessage);
            }
        }
        // check new foreign keys are filled
        tripSafe = checkForeignKeys(trip, entitiesUsed, withWell, missingForeignKeys, missingFkToMessage);
        tripSafe = checkNullReferences(entitiesUsed) & tripSafe;
        if (tripSafe && newVessels != null) {
            // check that vessel is ok
            Vessel vessel = trip.getVessel();
            int code = vessel.getCode();
            if (newVessels.containsKey(code)) {
                // use the vessel from db - check flag, fleet and vesselType exists
                if (vessel.getFlagCountry() == null) {
                    addErrorMessage(l(locale, "t3.import.error.vessel.with.noFlagCountry", code));
                    tripSafe = false;
                }
                if (vessel.getFleetCountry() == null) {
                    addErrorMessage(l(locale, "t3.import.error.vessel.with.noFleetCountry", code));
                    tripSafe = false;
                }
                if (vessel.getVesselType() == null) {
                    addErrorMessage(l(locale, "t3.import.error.vessel.with.noVesselType", code));
                    tripSafe = false;
                }
                // use this vessel for the trip
                addInfoMessage(l(locale, "t3.import.message.use.new.vessel.for.trip", decorate(vessel), decorate(trip)));
            }
        }
        String tripTypeNotSafeMessage = inputPilot.getTripTypeNotSafeMessage(trip);
        if (tripTypeNotSafeMessage != null) {
            tripSafe = false;
            addErrorMessage(l(locale, "t3.import.error.bad.tripType", decorate(trip), tripTypeNotSafeMessage));
        }
        //TODO Use this when Will have exact list of entities to exclude...
        // check we are not using bad references
//        tripSafe &= checkReferences(trip, entitiesUsed);
        return tripSafe;
    }

    private boolean removeWells(Trip trip, Collection<Well> wells, List<MissingForeignKey> missingForeignKeys,
                                Function<MissingForeignKey, String> missingFKToMessage) {
        boolean withWell = true;
        // remove missing WellPlan to Activity missing FK
        boolean missWellPlanToActivityFK = removeWellMissingForeignKeys(missingForeignKeys, true, WELL_PLAN_TO_ACTIVITY, missingFKToMessage);
        // remove missing Sample to Well missing FK
        boolean missSampleToWellFK = removeWellMissingForeignKeys(missingForeignKeys, true, SAMPLE_TO_WELL, missingFKToMessage);
        if (missWellPlanToActivityFK) {
            // must remove all WellPlan from Well with such problem
            for (Well well : wells) {
                if (!well.isWellPlanEmpty()) {
                    well.getWellPlan().removeIf(wellPlan -> wellPlan.getActivity() == null);
                }
            }
        }
        if (missWellPlanToActivityFK || missSampleToWellFK) {
            // well can not be used at all for sample remove for the trip all links between sample and well
            if (!trip.isSampleEmpty()) {
                for (Sample sample : trip.getSample()) {
                    sample.setWell(null);
                }
            }
            // let's say we do not use any longer wells
            withWell = false;
        }
        return withWell;
    }

    private boolean removeWellMissingForeignKeys(List<MissingForeignKey> missingForeignKeys,
                                                 boolean addWarning,
                                                 Predicate<MissingForeignKey> fKFilter,
                                                 Function<MissingForeignKey, String> f) {
        boolean found = false;
        if (CollectionUtils.isNotEmpty(missingForeignKeys)) {
            Iterator<MissingForeignKey> itr = missingForeignKeys.iterator();
            while (itr.hasNext()) {
                MissingForeignKey foreignKey = itr.next();
                if (fKFilter.test(foreignKey)) {
                    String message = f.apply(foreignKey);
                    if (addWarning) {
                        log.warn(message);
                        addWarningMessage(message);
                    } else {
                        log.debug(message);
                    }
                    itr.remove();
                    found = true;
                }
            }
        }
        return found;
    }

    private boolean checkNullReferences(T3EntityMap entitiesUsed) {
        boolean result = true;
        for (T3EntityEnum anEnum : inputPilot.getReferenceTypes()) {
            List<? extends TopiaEntity> list = entitiesUsed.get(anEnum);
            result = checkNullReferences(anEnum, list) & result;
        }
        return result;
    }

    private boolean checkNullReferences(T3EntityEnum anEnum, List<? extends TopiaEntity> list) {
        boolean result = true;
        if (CollectionUtils.isNotEmpty(list)) {
            for (TopiaEntity entity : list) {
                if (entity.getTopiaId() == null) {
                    // no topia id of reference : this is a missing ref not safe
                    String message = l(locale, "t3.import.error.reference.not.found", anEnum, ((T3ReferenceEntity) entity).getCode());
                    log.warn(message);
                    addErrorMessage(message);
                    result = false;
                }
            }
        }
        return result;
    }

    private boolean checkForeignKeys(Trip trip,
                                     T3EntityMap entitiesUsed,
                                     boolean withWell,
                                     List<MissingForeignKey> missingForeignKeys,
                                     Function<MissingForeignKey, String> missingFkToMessage) {
        String tripStr = decorate(trip);
        log.debug(String.format("Check foreign keys for trip %s", tripStr));
        boolean result = true;

        // check all added foreign keys

        if (!withWell) {
            // must check that all Sample used in the trip have replacement values to usage of well plan
            List<Sample> samples = entitiesUsed.get(Sample.class);
            if (CollectionUtils.isNotEmpty(samples)) {
                for (Sample sample : samples) {
                    float replaceCheck = sample.getGlobalWeight() + sample.getMinus10Weight() + sample.getPlus10Weight();
                    if (replaceCheck == 0) {
                        // can not allowed to use this sample (so this trip)
                        // since without this information can not do anything in
                        // sample standardisation (treatment level 1)
                        String message =
                                l(locale, "t3.import.error.sample.with.noWellPlanReplacementValue",
                                        decorateSample(trip, sample));
                        log.warn(message);
                        addErrorMessage(message);
                        result = false;
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(missingForeignKeys)) {
            // there is some missing foreign keys, if they appear here it means only that it is an error...
            for (MissingForeignKey foreignKey : missingForeignKeys) {
                String message = missingFkToMessage.apply(foreignKey);
                log.warn(message);
                addErrorMessage(message);
                result = false;
            }
        }
        return result;
    }

//    protected boolean checkReferences(Trip trip, T3EntityMap entitiesUsed) {
//        log.info(String.format("Check references for trip %s", trip));
//        boolean result = true;
//        for (T3EntityEnum type : inputPilot.getReferenceTypes()) {
//            List<? extends TopiaEntity> list = entitiesUsed.get(type);
//            if (list == null) {
//                // not used
//                continue;
//            }
//            for (TopiaEntity entity : list) {
//                boolean safe = checkReference(trip, (T3ReferenceEntity) entity);
//                result &= safe;
//            }
//        }
//        return result;
//    }

//    private boolean checkReference(Trip trip, T3ReferenceEntity entity) {
//        int code = entity.getCode();
//        boolean safe = true;
//        if (code == 9 || code == 99 || code == 999) {
//            // bad code
//            String message = l(locale, "t3.import.error.badReferenceCode", decorate(trip), T3EntityEnum.valueOf(entity), code);
//            log.warn(message);
//            addErrorMessage(message);
//            safe = false;
//        }
//        return safe;
//    }

    @SuppressWarnings({"unchecked"})
    private ReferenceEntityMap getSafeReferences(T3TopiaPersistenceContext tx) {
        ReferenceEntityMap references = new ReferenceEntityMap();
        for (T3EntityEnum type : inputPilot.getReferenceTypes()) {
            // obtain references from t3 database
            Class<T3ReferenceEntity> contract = (Class<T3ReferenceEntity>) type.getContract();
            List safeRefs = tx.getDao(contract).findAll();
            references.put(type, safeRefs);
        }
        return references;
    }

    private Map<Trip, Trip> getTripsToReplace(Set<Trip> safeTrips) {
        List<Trip> allTrips = tripDAO.findAll();
        Multimap<Vessel, Trip> tripByVessel = AbstractTripTopiaDao.groupByVessel(allTrips);
        Map<Trip, Trip> result = new HashMap<>();
        for (Trip newTrip : safeTrips) {
            // old trip will not be empty if there is already a matching trip
            Trip oldTrip = null;
            // vessel + landing date is a natural id of trip
            Vessel vessel = newTrip.getVessel();
            Collection<Trip> trips = tripByVessel.get(vessel);
            if (CollectionUtils.isNotEmpty(trips)) {
                // find if there is a trip with the good landing date
                Date landingDate = newTrip.getLandingDate();
                for (Trip trip : trips) {
                    if (landingDate.equals(trip.getLandingDate())) {
                        // found it!
                        oldTrip = trip;
                        break;
                    }
                }
            }
            if (oldTrip != null) {
                // old trip will be replaced by new one
                result.put(oldTrip, newTrip);
            }
        }
        return result;
    }

    private String decorateSample(Trip trip, Sample sample) {
        return decorate(trip) + " - " + sample.getSampleNumber();
    }

    private void setInputPilot(T3Input inputPilot) {
        this.inputPilot = inputPilot;
    }

    class MissingForeignKeyToMessage implements Function<MissingForeignKey, String> {
        private final String tripStr;

        MissingForeignKeyToMessage(String tripStr) {
            this.tripStr = tripStr;
        }

        @Override
        public String apply(MissingForeignKey input) {
            String message;
            if (input instanceof MissingForeignKeyInT3) {
                message = l(locale, "t3.import.error.missingForeignKeyInT3",
                        tripStr,
                        input.getTargetType(),
                        getPKey(input.getTargetType(), input.getTargetPKey()));
            } else {
                message = l(locale, "t3.import.error.missingForeignKey",
                        tripStr,
                        input.getSourceType(),
                        getPKey(input.getSourceType(), input.getSourcePKey()),
                        input.getTargetType(),
                        getPKey(input.getTargetType(), input.getTargetPKey()));
            }
            return message;
        }

        String getPKey(T3EntityEnum type, Object[] pKeys) {
            Iterator<String> pKeyNamesIterator = inputPilot.getPKeyNames(type).iterator();
            List<String> result = new ArrayList<>(pKeys.length);
            for (Object pKey : pKeys) {
                String str;
                if (pKey instanceof Date) {
                    Date date = (Date) pKey;
                    str = DATE_FORMAT.format(date);
                } else {
                    str = String.valueOf(pKey);
                }
                String propertyName = pKeyNamesIterator.next();
                result.add(String.format("%s = %s", propertyName, str));
            }
            return result.toString();

        }
    }

}
