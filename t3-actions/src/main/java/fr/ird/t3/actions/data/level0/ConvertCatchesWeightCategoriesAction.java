/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Multimap;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverter;
import fr.ird.t3.entities.conversion.WeightCategoryLogBookConverterProvider;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.CorrectedElementaryCatchTopiaDao;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryLogBook;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Action to redistribute the elementaries catches using now as weight categories
 * the {@link WeightCategoryTreatment} instead of the
 * {@link WeightCategoryLogBook}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ConvertCatchesWeightCategoriesAction extends AbstractLevel0Action<ConvertCatchesWeightCategoriesConfiguration> {

    private static final Log log = LogFactory.getLog(ConvertCatchesWeightCategoriesAction.class);
    private Map<Species, CatchWeightResult> specieWeightResult;
    @InjectDAO(entityType = CorrectedElementaryCatch.class)
    private CorrectedElementaryCatchTopiaDao correctedElementaryCatchDAO;
    private WeightCategoryLogBookConverterProvider converterProvider;

    public ConvertCatchesWeightCategoriesAction() {
        super(Level0Step.CONVERT_CATCHES_WEIGHT_CATEGORIES);
    }

    @SuppressWarnings("unused")
    public Map<Species, CatchWeightResult> getSpecieWeightResult() {
        return specieWeightResult;
    }

    public void setSpecieWeightResult(Map<Species, CatchWeightResult> specieWeightResult) {
        this.specieWeightResult = specieWeightResult;
    }

    @SuppressWarnings("unused")
    public CatchWeightResult getWeightResult(Species species) {
        return specieWeightResult.get(species);
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        String version = getConfiguration().getVersion();
        if (ConvertCatchesWeightCategoriesConfiguration.LEGACY_VERSION.equals(version)) {
            this.converterProvider = WeightCategoryLogBookConverterProvider.newLegacyInstance(getT3TopiaPersistenceContext().get());
        } else {
            this.converterProvider = WeightCategoryLogBookConverterProvider.newDefaultInstance(getT3TopiaPersistenceContext().get(), version);
        }
        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);
        specieWeightResult = new HashMap<>();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() {
        boolean result = false;
        if (CollectionUtils.isNotEmpty(trips)) {
            setNbSteps(trips.size());
            for (Trip trip : trips) {
                result |= executeForTrip(trip);
            }
        }
        return result;
    }

    private boolean executeForTrip(Trip trip) throws TopiaException {
        incrementsProgression();
        String tripStr = decorate(trip);
        String message = l(locale, "t3.level0.convertCatchesWeightCategories.treat.trip", tripStr);
        log.info(message);
        addInfoMessage(message);
        Map<Species, CatchWeightResult> tripResult = new HashMap<>();
        if (trip.isActivityNotEmpty()) {
            // must remove previous corrected elementary catches
            for (Activity activity : trip.getActivity()) {
                activity.clearCorrectedElementaryCatch();
            }
            for (Activity activity : trip.getActivity()) {
                if (activity.isElementaryCatchEmpty()) {
                    // no catches
                    continue;
                }
                log.info(String.format("Treat activity %s with %d", activity.getDate(), activity.sizeElementaryCatch()));
                Ocean currentOcean = activity.getOcean();
                SchoolType schoolType = activity.getSchoolType();
                // get converter for this ocean and school type
                WeightCategoryLogBookConverter converter = converterProvider.getConverter(currentOcean, schoolType);
                if (converter == null) {
                    // can not redistribute if no weight categories found
                    message = l(locale, "t3.level0.convertCatchesWeightCategories.warning.no.converter.found",
                            currentOcean.getLabel1(), schoolType.getLabel1());
                    log.warn(message);
                    addWarningMessage(message);
                    continue;
                }
                // split catches by species
                Multimap<Species, ElementaryCatch> catchesBySpecie = SpeciesTopiaDao.groupBySpecies(activity.getElementaryCatch());
                for (Species species : catchesBySpecie.keySet()) {
                    CatchWeightResult catchWeightResult = getCatchWeightResult(tripResult, species);
                    Collection<ElementaryCatch> catches = catchesBySpecie.get(species);
                    catchWeightResult.addLogBookWeight(catches);
                    log.info(String.format("Treat species %d with %d catches", species.getCode(), catches.size()));
                    // converts to weight category treatment for this species
                    Map<WeightCategoryTreatment, Float> distribution = converter.distribute(species, catches);
                    if (MapUtils.isEmpty(distribution)) {
                        // nothing to create
                        continue;
                    }
                    Collection<CorrectedElementaryCatch> newCatches = new ArrayList<>();
                    // create new catches
                    for (Map.Entry<WeightCategoryTreatment, Float> e2 : distribution.entrySet()) {
                        WeightCategoryTreatment weightCategoryTreatment = e2.getKey();
                        Float weight = e2.getValue();
                        CorrectedElementaryCatch correctedElementaryCatch =
                                correctedElementaryCatchDAO.create(
                                        CorrectedElementaryCatch.PROPERTY_WEIGHT_CATEGORY_TREATMENT, weightCategoryTreatment,
                                        CorrectedElementaryCatch.PROPERTY_SPECIES, species,
                                        CorrectedElementaryCatch.PROPERTY_CATCH_WEIGHT, weight);
                        newCatches.add(correctedElementaryCatch);
                    }
                    catchWeightResult.addTreatmentWeight(newCatches);
                    activity.addAllCorrectedElementaryCatch(newCatches);
                }
            }
        }
        addInfoMessage(l(locale, "t3.level0.convertCatchesWeightCategories.resume.for.trip", tripStr, tripResult.size()));
        for (Map.Entry<Species, CatchWeightResult> e : tripResult.entrySet()) {
            Species species = e.getKey();
            CatchWeightResult r = e.getValue();
            CatchWeightResult globalCatchWeightResult = getCatchWeightResult(specieWeightResult, species);
            globalCatchWeightResult.addResult(r);
            addInfoMessage(l(locale, "t3.level0.convertCatchesWeightCategories.resume.for.species",
                    decorate(species), r.getLogBookTotalWeight(), r.getTreatmentTotalWeight()));
        }
        // will need rf1 to be reapplied on the trip
        markTripAsTreated(trip);
        return true;
    }

    private CatchWeightResult getCatchWeightResult(Map<Species, CatchWeightResult> specieWeightResult, Species species) {
        return specieWeightResult.computeIfAbsent(species, k -> new CatchWeightResult());
    }

    public static class CatchWeightResult {

        float logBookTotalWeight;
        float treatmentTotalWeight;

        @SuppressWarnings("WeakerAccess")
        public float getLogBookTotalWeight() {
            return logBookTotalWeight;
        }

        public void setLogBookTotalWeight(int logBookTotalWeight) {
            this.logBookTotalWeight = logBookTotalWeight;
        }

        @SuppressWarnings("WeakerAccess")
        public float getTreatmentTotalWeight() {
            return treatmentTotalWeight;
        }

        public void setTreatmentTotalWeight(float treatmentTotalWeight) {
            this.treatmentTotalWeight = treatmentTotalWeight;
        }

        void addLogBookWeight(Collection<ElementaryCatch> catches) {
            for (ElementaryCatch aCatch : catches) {
                logBookTotalWeight += aCatch.getCatchWeightRf2();
            }
        }

        void addTreatmentWeight(Collection<CorrectedElementaryCatch> catches) {
            for (CorrectedElementaryCatch aCatch : catches) {
                treatmentTotalWeight += aCatch.getCatchWeight();
            }
        }

        void addResult(CatchWeightResult catchWeightResult) {
            logBookTotalWeight += catchWeightResult.getLogBookTotalWeight();
            treatmentTotalWeight += catchWeightResult.getTreatmentTotalWeight();
        }
    }
}
