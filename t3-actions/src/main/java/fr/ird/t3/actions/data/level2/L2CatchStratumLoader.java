/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import fr.ird.t3.actions.stratum.CatchStratumLoader;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.services.ioc.InjectDAO;

import java.util.Map;

/**
 * To load a {@link L2CatchStratum} for a given stratum configuration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class L2CatchStratumLoader extends CatchStratumLoader<Level2Configuration, Level2Action, L2StratumConfiguration> {

    @InjectDAO(entityType = Activity.class)
    private ActivityTopiaDao activityDAO;

    @Override
    public Map<Activity, Integer> loadData(L2StratumConfiguration configuration) {
        // on commence par récupérer les activités :
        // - dans la zone
        // - avec le bon type de banc
        // - dans la bonne période de temps
        // - expertFlag <> 0
        // - avec des captures
        // - dont la marée n'est pas samplesOnly
        // - dont la marée est complète
        Map<String, Integer> activityIds = activityDAO.findAllActivityIdsForCatchStratum(
                configuration.getZoneTableName(),
                configuration.getZone().getTopiaId(),
                configuration.getSchoolTypeIds(),
                configuration.getBeginDate(),
                configuration.getEndDate());
        // ensuite pour chaque activité on récupère sa marée et on conserve
        // l'activité uniquement ssi :
        // - maree.bateau de type senneur OK
        // - maree.bateau dans la bonne flotte OK
        return filterActivities(configuration, activityIds);
    }

    @Override
    protected void onActivityFound(Activity activity) {
        if (activity.isCorrectedElementaryCatchNotEmpty()) {
            for (CorrectedElementaryCatch correctedElementaryCatch : activity.getCorrectedElementaryCatch()) {
                correctedElementaryCatch.setCorrectedCatchWeight(correctedElementaryCatch.getCatchWeight());
            }
        }
    }
}