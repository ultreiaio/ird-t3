/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.data.N1ResultState;
import java.util.Collection;
import java.util.EnumSet;


import static org.nuiton.i18n.I18n.n;

/**
 * An enumeration to define all steps of the level 1.
 * <p/>
 * The constants are ordered in theire natural order to be executed.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public enum Level1Step {

    /**
     * Extrapolate Sample counted and measured.
     *
     * @see ExtrapolateSampleCountedAndMeasuredAction
     */
    EXTRAPOLATE_SAMPLE_COUNTED_AND_MEASURED(
            n("t3.level1.step.extrapolateSampleCountedAndMeasured")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isExtrapolateSampleCountedAndMeasured();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setExtrapolateSampleCountedAndMeasured(newState);
        }
    },

    /**
     * Standardize sample measured.
     *
     * @see StandardizeSampleMeasuresAction
     */
    STANDARDIZE_SAMPLE_MEASURE(
            n("t3.level1.step.standardizeSampleMeasure")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isStandardizeSampleMeasures();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setStandardizeSampleMeasures(newState);
        }
    },
    /**
     * Compute weight of categories for set.
     *
     * @see ComputeWeightOfCategoriesForSetAction
     */
    COMPUTE_WEIGHT_OF_CATEGORIES_FOR_SET(
            n("t3.level1.step.computeWeightOfCategoriesForSet")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isComputeWeightOfCategoriesForSet();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setComputeWeightOfCategoriesForSet(newState);
        }
    },
    /**
     * Redistribute sample set frequencies to set.
     *
     * @see RedistributeSampleNumberToSetAction
     */
    REDISTRIBUTE_SAMPLE_SET_TO_SET(
            n("t3.level1.step.redistributeSampleNumberToSet")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isRedistributeSampleNumberToSet();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setRedistributeSampleNumberToSet(newState);
        }
    },

    /**
     * Converts sample set species frequencies to weight.
     *
     * @see ConvertSampleSetSpeciesFrequencyToWeightAction
     */
    CONVERT_SAMPLE_SET_SPECIES_FREQUENCY_TO_WEIGHT(
            n("t3.level1.step.convertSampleSetSpeciesFrequencyToWeight")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isConvertSampleSetSpeciesFrequencyToWeight();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setConvertSampleSetSpeciesFrequencyToWeight(newState);
        }
    },
    /**
     * Extrapolate sample weight to set.
     *
     * @see ExtrapolateSampleWeightToSetAction
     */
    EXTRAPOLATE_SAMPLE_WEIGHT_TO_SET(
            n("t3.level1.step.extrapolateSampleWeightToSet")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isExtrapolateSampleWeightToSet();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setExtrapolateSampleWeightToSet(newState);
        }
    },
    /**
     * Converts set species frequencies to weight.
     *
     * @see ConvertSetSpeciesFrequencyToWeightAction
     */
    CONVERT_SET_SPECIES_FREQUENCY_TO_WEIGHT(
            n("t3.level1.step.convertSetSpeciesFrequencyToWeight")) {
        @Override
        public boolean getState(N1ResultState entity) {
            return entity.isConvertSetSpeciesFrequencyToWeight();
        }

        @Override
        public void setState(N1ResultState entity, boolean newState) {
            entity.setConvertSetSpeciesFrequencyToWeight(newState);
        }
    };

    private final String i18nKey;

    Level1Step(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public String getI18nKey() {
        return i18nKey;
    }

    /**
     * Gets the executed state value for the given entity.
     *
     * @param entity the entity to test
     * @return the executed state value for the given entity
     */
    public abstract boolean getState(N1ResultState entity);

    /**
     * Sets the executed state value for the given entity.
     *
     * @param entity   the entity to change
     * @param newState the new state value
     */
    public abstract void setState(N1ResultState entity, boolean newState);

    public static EnumSet<Level1Step> allBefore(Level1Step step) {
        return T3EntityHelper.allBefore(Level1Step.class, step, values());
    }

    public static EnumSet<Level1Step> allAfter(Level1Step step) {
        return T3EntityHelper.allAfter(Level1Step.class, step, values());
    }

    public static void invalidate(N1ResultState trip, Collection<Level1Step> steps) {
        for (Level1Step step : steps) {
            step.setState(trip, false);
        }
    }
}
