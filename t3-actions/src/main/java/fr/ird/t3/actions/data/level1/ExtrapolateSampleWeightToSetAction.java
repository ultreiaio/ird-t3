/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.Multimap;
import fr.ird.t3.entities.cache.LengthWeightConversionCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.RfUsageStatus;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeight;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.WeightCategories;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.TimeLog;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Extrapolate weight from sample to their owing set.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ExtrapolateSampleWeightToSetAction extends AbstractLevel1Action {

    private static final Log log = LogFactory.getLog(ExtrapolateSampleWeightToSetAction.class);
    private int nbTreatedSets;
    private float nbTreatedFishesInSamples;
    private float nbCreatedFishesInSetSpeciesFrequency;
    @InjectDAO(entityType = SetSpeciesFrequency.class)
    private SetSpeciesFrequencyTopiaDao setSpeciesFrequencyDAO;
    private LengthWeightConversionCache conversionHelper;

    public ExtrapolateSampleWeightToSetAction() {
        super(Level1Step.EXTRAPOLATE_SAMPLE_WEIGHT_TO_SET);
    }

    @SuppressWarnings("unused")
    public int getNbTreatedSets() {
        return nbTreatedSets;
    }

    public void setNbTreatedSets(int nbTreatedSets) {
        this.nbTreatedSets = nbTreatedSets;
    }

    @SuppressWarnings("unused")
    public float getNbTreatedFishesInSamples() {
        return nbTreatedFishesInSamples;
    }

    public void setNbTreatedFishesInSamples(int nbTreatedFishesInSamples) {
        this.nbTreatedFishesInSamples = nbTreatedFishesInSamples;
    }

    @SuppressWarnings("unused")
    public float getNbCreatedFishesInSetSpeciesFrequency() {
        return nbCreatedFishesInSetSpeciesFrequency;
    }

    public void setNbCreatedFishesInSetSpeciesFrequency(int nbCreatedFishesInSetSpeciesFrequency) {
        this.nbCreatedFishesInSetSpeciesFrequency = nbCreatedFishesInSetSpeciesFrequency;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        conversionHelper = getT3TopiaPersistenceContext().get().newLengthWeightConversionSimpleCache();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() {
        Set<Trip> trips = samplesByTrip.keySet();
        setNbSteps(samplesByTrip.size() + trips.size());
        for (Trip trip : trips) {
            Collection<Sample> samples = samplesByTrip.get(trip);
            logTreatedAndNotSamplesforATrip(trip, samples);
            long s0 = TimeLog.getTime();
            doExecuteTrip(trip, samples);
            String tripStr = decorate(trip, DecoratorService.WITH_ID);
            getTimeLog().log(s0, "treat trip", tripStr);
            // flush transaction otherwise too much data in memory
            flushTransaction(tripStr);
        }
        return true;
    }

    private void doExecuteTrip(Trip trip, Collection<Sample> samples) {
        incrementsProgression();
        Set<Activity> tripActivities = new HashSet<>();
        // extrapolate for each sample set number to set
        for (Sample sample : samples) {
            long s0 = TimeLog.getTime();
            incrementsProgression();
            String sampleStr = l(locale, "t3.level1.extrapolateSampleWeightToSet.sampleStr", decorate(trip), sample.getSampleNumber());
            float nb = sample.getTotalStandardiseSampleSpeciesFrequencyNumber();
            addInfoMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.sample.nbFishes", sampleStr, nb));
            nbTreatedFishesInSamples += nb;
            Set<Species> species = SpeciesTopiaDao.getAllSpeciesFromSampleSpecies(samples);
            for (SampleSet sampleSet : sample.getSampleSet()) {
                Activity activity = sampleSet.getActivity();
                Ocean ocean = activity.getOcean();
                // get the length class +10kg limit
                Map<Species, Integer> limitLengthClassBySpecie = getThresholdPlus10ForSpecies(species, ocean, activity.getRoute().getDate());
                boolean added = tripActivities.add(activity);
                if (added) {
                    // remove all stuff from activity
                    activity.clearSetSpeciesFrequency();
                    activity.clearSetSpeciesCatWeight();
                }
                doExecuteSampleSet(activity, sampleSet, sampleStr, limitLengthClassBySpecie);
            }
            // mark sample as treated for this step of level 1 treatment
            markAsTreated(sample);
            getTimeLog().log(s0, "treat sample ", sampleStr);
        }
        nbTreatedSets += tripActivities.size();
        // mar trip as treated for this level 1 step
        markAsTreated(trip);
    }

    private void doExecuteSampleSet(Activity activity, SampleSet sampleSet, String sampleStr, Map<Species, Integer> limitLengthClassBySpecie) {
        String sampleWellStr = l(locale, "t3.level1.extrapolateSampleWeightToSet.sampleWellStr", sampleStr, activity.getDate());
        //compute sample well sample weight
        SampleWellSampleWeight sampleWellSampleWeight = computeSampleWellSampleWeight(sampleWellStr, sampleSet);
        // compute sample well set weight
        SampleWellSetWeight sampleWellSetWeight = computeSampleWellSetWeight(sampleWellStr, sampleSet);
        boolean useRfMinus10AndRfPlus10 = getConfiguration().isUseRfMinus10AndRfPlus10();
        // compute the rf context
        RFContext rfContext = computeRFContext(sampleWellStr, sampleWellSetWeight, sampleWellSampleWeight, useRfMinus10AndRfPlus10);
        // keep rf data + status in the sample well
        sampleSet.setRfMinus10(rfContext.getRfMinus10());
        sampleSet.setRfMinus10UsageStatus(rfContext.getRfMinus10Status());
        sampleSet.setRfPlus10(rfContext.getRfPlus10());
        sampleSet.setRfPlus10UsageStatus(rfContext.getRfPlus10Status());
        /*
        0 : accepté rf-10
        1 : rejeté car non utilisation des rf-10 / rf+10
        2 : rejeté car rf-10 non présent (non défini)
        3 : rejeté car rf-10 vaut 0
        4 : rejeté car rf-10 trop grand (dépasse 500)
        5 : rejeté car rf-10 trop peu de poisson dans l'échantillon
         */
        sampleSet.setRfTot(rfContext.getRfTot());
        // fill the SetSpecieFrequency table
        fillSetSpeciesFrequency(activity, limitLengthClassBySpecie, sampleSet, rfContext);
    }

    /**
     * Given a sample well, compute the {@link SampleWellSampleWeight}, says :
     * <ul>
     * <li>his total weight</<li>
     * <li>his weight of -10Kg category</<li>
     * <li>his weight of +10Kg category</<li>
     * </ul>
     *
     * @param sampleWellStr decoration of a sample well
     * @param sampleSet     sampleSet of fishes
     * @return the computed weight of the sample
     * @see SampleWellSampleWeight
     */
    private SampleWellSampleWeight computeSampleWellSampleWeight(String sampleWellStr, SampleSet sampleSet) {
        float pdechm = 0;
        float pdechp = 0;
        float plus10Number = 0;
        float minus10Number = 0;
        for (SampleSetSpeciesCatWeight sampleSetSpeciesCatWeight : sampleSet.getSampleSetSpeciesCatWeight()) {
            WeightCategorySample weightCategorySample = sampleSetSpeciesCatWeight.getWeightCategorySample();
            if (WeightCategories.isMinus10Category(weightCategorySample)) {
                pdechm += sampleSetSpeciesCatWeight.getWeight();
                minus10Number += sampleSetSpeciesCatWeight.getCount();
            } else if (WeightCategories.isPlus10Category(weightCategorySample)) {
                pdechp += sampleSetSpeciesCatWeight.getWeight();
                plus10Number += sampleSetSpeciesCatWeight.getCount();
            } else {
                throw new IllegalStateException("weightCategorySample must plus -10Kg, or +10Kg, but was: " + weightCategorySample);
            }
        }
        SampleWellSampleWeight result = SampleWellSampleWeight.create(pdechm, pdechp, minus10Number, plus10Number);
        String message = l(locale, "t3.level1.extrapolateSampleWeightToSet.resume.for.sampleSet.sampleWellSampleWeight",
                sampleWellStr,
                result.getMinus10Weight(),
                result.getPlus10Weight(),
                result.getTotalWeight(),
                result.getMinus10Number(),
                result.getPlus10Number());
        addInfoMessage(message);
        log.debug(message);
        return result;
    }

    /**
     * Given a sample well, compute the {@link SampleWellSetWeight}, says:
     * <ul>
     * <li>his total weight</<li>
     * <li>his weight of -10Kg category</<li>
     * <li>his weight of +10Kg category</<li>
     * </ul>
     *
     * @param sampleWellStr decoration of a sample well
     * @param sampleSet     sampleSet of fishes a more +10Kg weight for each species
     * @return the computed weight of the sample
     * @see SampleWellSampleWeight
     */
    private SampleWellSetWeight computeSampleWellSetWeight(String sampleWellStr, SampleSet sampleSet) {
        float pondt = sampleSet.getWeightedWeight();
        Float pondm = sampleSet.getWeightedWeightMinus10();
        Float pondp = sampleSet.getWeightedWeightPlus10();
        SampleWellSetWeight result = SampleWellSetWeight.create(pondt, pondm, pondp);
        String message = l(locale, "t3.level1.extrapolateSampleWeightToSet.resume.for.sampleSet.sampleWellSetWeight",
                sampleWellStr,
                result.getMinus10Weight(),
                result.getPlus10Weight(),
                result.getTotalWeight());
        addInfoMessage(message);
        log.debug(message);
        return result;
    }

    /**
     * Given the {@code sampleSet} compute his {@code rf context}.
     *
     * @param sampleWellStr           sampleSet decorate value
     * @param setWeight               the sampleSet weight computed
     * @param sampleWeight            the sampleSet weight computed
     * @param useRfMinus10AndRfPlus10 flag to try to use rf-10 and rf+10
     * @return the computed rf context for the given sample set
     */
    private RFContext computeRFContext(String sampleWellStr, SampleWellSetWeight setWeight, SampleWellSampleWeight sampleWeight, boolean useRfMinus10AndRfPlus10) {
        float pondt = setWeight.getTotalWeight();
        Float pondp = setWeight.getPlus10Weight();
        Float pondm = setWeight.getMinus10Weight();
        // compute rftot
        float pdecht = sampleWeight.getTotalWeight();
        float rftot = pondt / pdecht;
        // compute rfMinus10
        Float pdechm = sampleWeight.getMinus10Weight();
        Float rfMinus10 = null;
        if (pdechm != null && pdechm > 0 && pondm != null) {
            rfMinus10 = pondm / pdechm;
        }
        // compute rfPlus10
        Float pdechp = sampleWeight.getPlus10Weight();
        Float rfPlus10 = null;
        if (pdechp != null && pdechp > 0 && pondp != null) {
            rfPlus10 = pondp / pdechp;
        }
        if (rftot > getConfiguration().getRfTotMax()) {
            addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rftot.too.high",
                    sampleWellStr, rftot));
        }
        RfUsageStatus rfMinus10Status = RfUsageStatus.ACCEPTED;
        RfUsageStatus rfPlus10Status = RfUsageStatus.ACCEPTED;
        if (!useRfMinus10AndRfPlus10) {
            rfMinus10Status = rfPlus10Status = RfUsageStatus.REJECTED_BY_CONFIGURATION;
        } else {
            if (rfMinus10 == null) {
                addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.notDefined",
                        sampleWellStr, rftot));
                rfMinus10Status = RfUsageStatus.REJECTED_RF_NOT_DEFINED;
            } else {
                int rfMinus10MinNumber = getConfiguration().getRfMinus10MinNumber();
                Float sampletWeightMinus10Number = sampleWeight.getMinus10Number();
                if (Math.abs(rfMinus10) < 0.001f) {
                    addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.too.low",
                            sampleWellStr, rfMinus10, rftot));
                    rfMinus10Status = RfUsageStatus.REJECTED_RF_IS_ZERO;
                } else if (rfMinus10 > getConfiguration().getRfMinus10Max()) {
                    addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.too.high",
                            sampleWellStr, rfMinus10, rftot));
                    rfMinus10Status = RfUsageStatus.REJECTED_RF_TOO_HIGH;
                } else if (sampletWeightMinus10Number < rfMinus10MinNumber) {
                    addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfMinus10.no.enough.sample.count",
                            sampleWellStr, rfPlus10, rftot, sampletWeightMinus10Number, rfMinus10MinNumber));
                    rfMinus10Status = RfUsageStatus.REJECTED_NOT_ENOUGH_SAMPLE_COUNT;
                }
            }
            if (rfPlus10 == null) {
                addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.notDefined",
                        sampleWellStr, rftot));
                rfPlus10Status = RfUsageStatus.REJECTED_RF_NOT_DEFINED;
            } else {
                int rfPlus10MinNumber = getConfiguration().getRfPlus10MinNumber();
                Float sampletWeightPlus10Number = sampleWeight.getPlus10Number();
                if (Math.abs(rfPlus10) < 0.001f) {
                    addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.too.low",
                            sampleWellStr, rfPlus10, rftot));
                    rfPlus10Status = RfUsageStatus.REJECTED_RF_IS_ZERO;
                } else if (rfPlus10 > getConfiguration().getRfPlus10Max()) {
                    addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.too.high",
                            sampleWellStr, rfPlus10, rftot));
                    rfPlus10Status = RfUsageStatus.REJECTED_RF_TOO_HIGH;
                } else if (sampletWeightPlus10Number < rfPlus10MinNumber) {
                    addWarningMessage(l(locale, "t3.level1.extrapolateSampleWeightToSet.warning.rfPlus10.no.enough.sample.count",
                            sampleWellStr, rfPlus10, rftot, sampletWeightPlus10Number, rfPlus10MinNumber));
                    rfPlus10Status = RfUsageStatus.REJECTED_NOT_ENOUGH_SAMPLE_COUNT;
                }
            }
        }
        RFContext rfContext = RFContext.create(rftot, rfMinus10, rfPlus10, rfMinus10Status, rfPlus10Status);
        String message = l(locale, "t3.level1.extrapolateSampleWeightToSet.resume.for.sample.rfContext",
                sampleWellStr, rfContext.getRfTot(), rfContext.getRfMinus10(), rfContext.getRfPlus10());
        addInfoMessage(message);
        log.debug(message);
        return rfContext;
    }

    private void fillSetSpeciesFrequency(Activity activity, Map<Species, Integer> limitLengthClassBySpecies, SampleSet sampleSet, RFContext rfContext) {
        // group sample set frequencies by species
        Multimap<Species, SampleSetSpeciesFrequency> sampleSetSpeciesFrequencyBySpecies =
                SpeciesTopiaDao.groupBySpecies(sampleSet.getSampleSetSpeciesFrequency());
        for (Species species : sampleSetSpeciesFrequencyBySpecies.keySet()) {
            Integer threshold = limitLengthClassBySpecies.get(species);
            Map<Integer, SetSpeciesFrequency> setSpeciesFrequencyMap =
                    setSpeciesFrequencyDAO.findAllByActivityAndSpeciesOrderedByLengthClass(activity, species);
            for (SampleSetSpeciesFrequency sampleSetSpeciesFrequency : sampleSetSpeciesFrequencyBySpecies.get(species)) {
                Float number = sampleSetSpeciesFrequency.getNumber();
                int lfLengthClass = sampleSetSpeciesFrequency.getLfLengthClass();
                float rf = rfContext.computeRf(lfLengthClass, threshold);
                float value = rf * number;
                SetSpeciesFrequency setSpeciesFrequency = setSpeciesFrequencyMap.get(lfLengthClass);
                if (setSpeciesFrequency == null) {
                    // new entry
                    setSpeciesFrequency = setSpeciesFrequencyDAO.create(
                            SetSpeciesFrequency.PROPERTY_SPECIES, species,
                            SetSpeciesFrequency.PROPERTY_LF_LENGTH_CLASS, lfLengthClass,
                            SetSpeciesFrequency.PROPERTY_NUMBER, value);
                    activity.addSetSpeciesFrequency(setSpeciesFrequency);
                    setSpeciesFrequencyMap.put(lfLengthClass, setSpeciesFrequency);
                    nbCreatedFishesInSetSpeciesFrequency += value;
                } else {
                    // sum value
                    setSpeciesFrequency.setNumber(setSpeciesFrequency.getNumber() + value);
                }
            }
        }
    }

    /**
     * Obtain for each species used in one of the given sample the first length class which represents a +10Kg weight.
     *
     * @param species species to scan
     * @param ocean   where (used to get conversion)
     * @param date    when (used to get conversion)
     * @return the universe of length class limits computed indexed by species
     */
    private Map<Species, Integer> getThresholdPlus10ForSpecies(Set<Species> species, Ocean ocean, Date date) {
        // for each of those species, found the length class which matches a weight > 10Kg
        Map<Species, Integer> result = new HashMap<>();
        for (Species specie : species) {
            LengthWeightConversion conversion = conversionHelper.getConversions(specie, ocean, 0, date);
            if (conversion != null) {
                // only add conversion for species if found
                int lengthClass = conversionHelper.getSpecieHighestLengthClass(conversion, 10);
                result.put(specie, lengthClass);
            }
        }
        if (log.isDebugEnabled()) {
            for (Map.Entry<Species, Integer> entry : result.entrySet()) {
                log.debug(String.format("Species %d - limit 10Kg length class : %d", entry.getKey().getCode(), entry.getValue()));
            }
        }
        return result;
    }

    /**
     * This object contains the sample weights (-10,+10 and total) for sample well.
     * <ul>
     * <li>{@link #getMinus10Number()} is legacy {@code pdechm}</li>
     * <li>{@link #getPlus10Number()}} is legacy {@code pdechp}</li>
     * <li>{@link #getTotalWeight()} is legacy {@code pdecht}</li>
     * </ul>
     * We keep also here the number of fishes (-10,+10) for the sample well.
     */
    protected static class SampleWellSampleWeight {

        /** sum of fishes weight for length class < +10Kg. */
        Float minus10Weight;
        /** sum of fishes weight for length class > +10Kg. */
        Float plus10Weight;
        /** Number of fishes for length class > -10Kg. */
        private Float plus10Number;
        /** Number of fishes for length class < -10Kg. */
        private Float minus10Number;

        protected static SampleWellSampleWeight create(Float minus10Weight, Float plus10Weight, Float minus10Number, Float plus10Number) {
            SampleWellSampleWeight result = new SampleWellSampleWeight();
            result.minus10Number = minus10Number;
            result.plus10Number = plus10Number;
            result.minus10Weight = minus10Weight;
            result.plus10Weight = plus10Weight;
            return result;
        }

        Float getTotalWeight() {
            return minus10Weight + plus10Weight;
        }

        Float getMinus10Weight() {
            return minus10Weight;
        }

        Float getPlus10Weight() {
            return plus10Weight;
        }

        Float getPlus10Number() {
            return plus10Number;
        }

        Float getMinus10Number() {
            return minus10Number;
        }
    }

    /**
     * This object contains the set weights (-10,+10 and total) for sample well.
     * <ul>
     * <li>{@link #getMinus10Weight()} is legacy {@code pondm}</li>
     * <li>{@link #getPlus10Weight()}} is legacy {@code pondp}</li>
     * <li>{@link #getTotalWeight()} is legacy {@code pondt}</li>
     * </ul>
     */
    protected static class SampleWellSetWeight {

        /** sum of fishes weight for length class < +10Kg. */
        Float totalWeight;
        /** sum of fishes weight for length class < +10Kg. */
        Float minus10Weight;
        /** sum of fishes weight for length class > +10Kg. */
        Float plus10Weight;

        protected static SampleWellSetWeight create(Float totalWeight, Float minus10Weight, Float plus10Weight) {
            SampleWellSetWeight result = new SampleWellSetWeight();
            result.totalWeight = totalWeight;
            result.minus10Weight = minus10Weight;
            result.plus10Weight = plus10Weight;
            return result;
        }

        Float getTotalWeight() {
            return totalWeight;
        }

        Float getMinus10Weight() {
            return minus10Weight;
        }

        Float getPlus10Weight() {
            return plus10Weight;
        }
    }

    protected static class RFContext {

        /**
         * Rf tot value.
         */
        Float rfTot;
        /**
         * Rf minus 10 value.
         */
        Float rfMinus10;
        /**
         * Rf plus 10 value.
         */
        Float rfPlus10;
        /**
         * Rf minus 10 usage status.
         */
        RfUsageStatus rfMinus10Status;
        /**
         * Rf plus 10 usage status.
         */
        RfUsageStatus rrPlus10Status;
        /**
         * Try to use rfMinus10/RfPlus10 or always use rfTot
         */
        boolean useRfMinus10OrRfPlus10;

        public static RFContext create(Float rfTot,
                                       Float rfm10,
                                       Float rfp10,
                                       RfUsageStatus rfMinus10Status,
                                       RfUsageStatus rrPlus10Status) {
            RFContext result = new RFContext();
            result.rfTot = rfTot;
            result.rfMinus10 = rfm10;
            result.rfPlus10 = rfp10;
            result.rfMinus10Status = rfMinus10Status;
            result.rrPlus10Status = rrPlus10Status;
            result.useRfMinus10OrRfPlus10 = rfMinus10Status == RfUsageStatus.ACCEPTED && rrPlus10Status == RfUsageStatus.ACCEPTED;
            return result;
        }

        Float getRfTot() {
            return rfTot;
        }

        Float getRfMinus10() {
            return rfMinus10;
        }

        Float getRfPlus10() {
            return rfPlus10;
        }

        RfUsageStatus getRfMinus10Status() {
            return rfMinus10Status;
        }

        RfUsageStatus getRfPlus10Status() {
            return rrPlus10Status;
        }

        Float computeRf(int lfLengthClass, int plus10LengthClass) {
            Float result;
            if (useRfMinus10OrRfPlus10) {
                if (lfLengthClass <= plus10LengthClass) {
                    // use rf-10
                    result = rfMinus10;
                } else {
                    // use rf+10
                    result = rfPlus10;
                }
            } else {
                // use only the rfTot
                result = rfTot;
            }
            return result;
        }
    }
}
