/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SetDuration;
import fr.ird.t3.entities.reference.SetDurationTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.TimeLog;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.l;

/**
 * Compute for the all activities of selected trips, the set duration and the positive set count.
 * <p/>
 * <strong>Note:</strong> The rf2 must have been computed on each trip.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ComputeSetDurationAndPositiveSetCountAction extends AbstractLevel0Action<ComputeSetDurationAndPositiveSetCountConfiguration> {

    private static final Log log = LogFactory.getLog(ComputeSetDurationAndPositiveSetCountAction.class);
    @InjectDAO(entityType = SetDuration.class)
    private SetDurationTopiaDao setDurationDAO;
    /** Count of treated activities. */
    private int nbActivities;
    /** Count of positive activities found. */
    private int nbPositiveActivities;
    /** Count of set (sum(a.setCount) on each activity trip). */
    private int nbSet;
    /**
     * Cache of setDuration (improve a lot performance!).
     *
     * @since 1.3
     */
    private Map<String, SetDuration> setDurations;

    public ComputeSetDurationAndPositiveSetCountAction() {
        super(Level0Step.COMPUTE_SET_DURATION_AND_POSITIVE_SET_COUNT);
    }

    @SuppressWarnings("unused")
    public int getNbSet() {
        return nbSet;
    }

    @SuppressWarnings("unused")
    public int getNbPositiveActivities() {
        return nbPositiveActivities;
    }

    public void setNbPositiveActivities(int nbPositiveActivities) {
        this.nbPositiveActivities = nbPositiveActivities;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);
        for (Trip trip : tripList) {
            nbActivities += trip.sizeActivity();
        }
        setDurations = new TreeMap<>();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() {
        boolean result = false;
        if (CollectionUtils.isNotEmpty(trips)) {
            setNbSteps(2 * nbActivities);
            for (Trip trip : trips) {
                result |= executeForTrip(trip);
            }
        }
        return result;
    }

    private boolean executeForTrip(Trip trip) throws TopiaException {
        long s0 = TimeLog.getTime();
        String tripStr = decorate(trip, DecoratorService.WITH_ID);
        if (trip.isActivityNotEmpty()) {
            int year = T3Functions.TRIP_TO_LANDING_YEAR.apply(trip);
            Set<Species> species = trip.getElementaryCatchSpecies();
            for (Activity activity : trip.getActivity()) {
                String activityStr = String.format("Activity %s - %s", tripStr, decorate(activity));
                int setCount = activity.getSetCount();
                nbSet += setCount;
                float totalCatchesWeight = activity.getElementaryCatchTotalWeightRf2(species);
                log.debug(String.format("Total catches %s for %s", totalCatchesWeight, activityStr));
                int positiveSetCount = computePositiveSetCount(totalCatchesWeight > 0, activity);
                activity.setPositiveSetCount(positiveSetCount);
                if (positiveSetCount > 0) {
                    nbPositiveActivities++;
                }
                incrementsProgression();
                float subCatch = totalCatchesWeight;
                if (positiveSetCount > 0) {
                    subCatch = subCatch / activity.getPositiveSetCount();
                }
                int negativeSetCount = setCount - positiveSetCount;
                Float setDuration = computeSetDuration(year, subCatch, activity, negativeSetCount);
                activity.setSetDuration(setDuration);
                incrementsProgression();
                String message = l(locale, "t3.level0.computeActivitySetDurationAndPositiveSetCount",
                        activityStr, setDuration, positiveSetCount, negativeSetCount);
                addInfoMessage(message);
                log.debug(message);
            }
        }
        markTripAsTreated(trip);
        getTimeLog().log(s0, "executeForTrip for " + tripStr);
        // always commit since trip is always modified
        return true;
    }

    private int computePositiveSetCount(boolean withCatches, Activity activity) {
        if (withCatches) {
            // ok some catches found on activity, it means positive set
            return activity.getSetCount();
        }
        return 0;
    }

    private Float computeSetDuration(int year, float subCatch, Activity activity, int negativeSetCount) throws TopiaException {
        // get the correct setDuration
        SetDuration setDuration = getSetDuration(activity, year);
        if (setDuration == null) {
            log.debug("No setDuration found, use 0 value.");
            return 0f;
        }
        float nullSetDuration = negativeSetCount * setDuration.getNullSetValue();
        if (activity.isSetNull()) {
            log.debug(String.format("Use nullValue of set duration = %s", nullSetDuration));
            return nullSetDuration;
        }
        if (activity.isWithSetDuration()) {
            float subDuration = (setDuration.getParameterB() + setDuration.getParameterA() * subCatch);
            float positiveSetDuration = activity.getPositiveSetCount() * subDuration;
            log.debug(String.format("Computed positive set duration = %s", positiveSetDuration));
            return nullSetDuration + positiveSetDuration;
        }
        // no setDuration
        log.debug("Use null set duration (set is null) ");
        return null;
    }

    private SetDuration getSetDuration(Activity activity, int year) throws TopiaException {
        Ocean ocean = activity.getOcean();
        Country fleetCountry = activity.getTrip().getVessel().getFleetCountry();
        SchoolType schoolType = activity.getSchoolType();
        String key = ocean.getCode() + ":" + fleetCountry.getCode() + ":" + schoolType.getCode() + ":" + year;
        SetDuration result = setDurations.get(key);
        if (result == null) {
            // no result found in cache
            if (!setDurations.containsKey(key)) {
                // try to load it
                result = setDurationDAO.findByActivityAndYear(activity, year);
                if (result == null) {
                    addWarningMessage(
                            l(getLocale(), "t3.level0.computeSetDurationAndPositiveSetCount.error.noSetDurationFound",
                                    decorate(ocean),
                                    decorate(fleetCountry),
                                    decorate(schoolType),
                                    year));
                }
            }
            // store it once for all (even if result is null)
            setDurations.put(key, result);
        }
        return result;
    }

    public void setNbActivities(int nbActivities) {
        this.nbActivities = nbActivities;
    }
}
