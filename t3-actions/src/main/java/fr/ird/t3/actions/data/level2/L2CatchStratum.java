/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import fr.ird.t3.actions.stratum.CatchStratum;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.models.WeightCompositionModel;
import fr.ird.t3.models.WeightCompositionModelHelper;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Define a catch stratum for a given stratum of a level 2 treatment.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class L2CatchStratum extends CatchStratum<Level2Configuration, Level2Action, L2StratumConfiguration> {

    private static final Log log = LogFactory.getLog(L2CatchStratum.class);
    /**
     * All species used by all weight categories found in all catches for this stratum.
     */
    private final SetMultimap<WeightCategoryTreatment, Species> weightCategoriesForSpecies;

    /**
     * Composition spécifique en entrée pour toutes les espèces trouvées
     * de la strate captures basée sur les
     * {@link CorrectedElementaryCatch#getCatchWeight()}.
     * <p/>
     * <strong>Note:</strong> uniquement utilisés dans les indicateurs mais pas dans les calculs.
     */
    private final WeightCompositionAggregateModel inputModelForAllSpecies;
    /**
     * Composition spécifique en sortie pour toutes les espèces trouvées
     * de la strate captures. Il s'agit ici de ne conserver uniquement que les
     * {@link CorrectedElementaryCatch#getCorrectedCatchWeight()} qui ont été
     * ajoutés sur cette strate capture.On ne peut plus ici utilisé tout le
     * poids de correctedCatWeight car les calées peuvent être traitées dans
     * plusieurs strates (donc des poinds pourraient être alors dupliqué dans
     * le résultat final).
     * <p/>
     * Ainsi en sommant cette variable pour toutes les startes captures, on
     * obtient bien tout ce qui a été corrigé (ou reporté) dans toutes les
     * captures du niveau 2.
     * <p/>
     * Une conséquence est qu'au niveau strate capture, on ne peut plus faire
     * de comparaison entre le modèle en entré et celui en sortie...
     * <p/>
     * <strong>Note:</strong> uniquement utilisés dans les indicateurs mais pas dans les calculs.
     */
    private final WeightCompositionAggregateModel outputModelForAllSpecies;
    /**
     * Composition spécifique en entrée pour les espèces à corriger de la strate capture basée sur les
     * {@link CorrectedElementaryCatch#getCatchWeight()}.
     * <p/>
     * <strong>Note:</strong> uniquement utilisés dans les indicateurs mais pas dans les calculs sauf pour
     * obtenir le poids total de la strate.
     */
    private WeightCompositionAggregateModel inputModelForSpeciesToFix;

    private L2CatchStratum(L2StratumConfiguration stratumConfiguration, Collection<Species> speciesToFix) {
        super(stratumConfiguration, speciesToFix);
        weightCategoriesForSpecies = HashMultimap.create();
        inputModelForAllSpecies = new WeightCompositionAggregateModel();
        outputModelForAllSpecies = new WeightCompositionAggregateModel();
    }

    static L2CatchStratum newCatchStratum(L2StratumConfiguration stratumConfiguration,
                                          List<WeightCategoryTreatment> weightCategories,
                                          Collection<Species> species,
                                          Level2Action action) throws Exception {
        L2CatchStratum catchStratum = new L2CatchStratum(stratumConfiguration, species);
        T3ServiceContext serviceContext = action.getServiceContext();
        catchStratum.init(serviceContext, weightCategories, action);
        // get the total weight of the catch stratum
        float catchStratumWeight = catchStratum.getTotalCatchWeightForSpeciesToFix();
        if (catchStratumWeight == 0) {
            // no catch in this stratum, skip it
            String message = l(serviceContext.getLocale(), "t3.level2.message.noCatch.in.stratum");
            log.info(message);
            action.addInfoMessage(message);
            // let's nullify the catch stratum (make it no more available)
            catchStratum = null;
        } else {
            // log it
            String message = catchStratum.logCatchStratum(action.getDecoratorService());
            log.info(message);
            action.addInfoMessage(message);
        }
        return catchStratum;
    }


    @Override
    public void close() throws IOException {
        super.close();
        inputModelForAllSpecies.close();
        inputModelForSpeciesToFix.close();
        outputModelForAllSpecies.close();
        weightCategoriesForSpecies.clear();
    }

    @Override
    public void init(T3ServiceContext serviceContext, List<WeightCategoryTreatment> weightCategories, Level2Action messager) throws Exception {
        // load catch stratum
        super.init(serviceContext, weightCategories, messager);
        // get all weight categories and species for all found catches compute also the input model for this catch stratum
        for (Map.Entry<Activity, Integer> e : this) {
            Activity activity = e.getKey();
            Integer nbZones = e.getValue();
            Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> correctedElementaryCatchesByCategory = getCorrectedElementaryCatchByCategory(activity);
            for (Map.Entry<WeightCategoryTreatment, Collection<CorrectedElementaryCatch>> entry : correctedElementaryCatchesByCategory.asMap().entrySet()) {
                WeightCategoryTreatment weightCategory = entry.getKey();
                Collection<CorrectedElementaryCatch> catches = entry.getValue();
                fillWeights(weightCategory, catches, null, CorrectedElementaryCatch::getCatchWeight, inputModelForAllSpecies, nbZones);
                for (CorrectedElementaryCatch aCatch : catches) {
                    weightCategoriesForSpecies.put(aCatch.getWeightCategoryTreatment(), aCatch.getSpecies());
                }
            }
        }
        inputModelForSpeciesToFix = inputModelForAllSpecies.extractForSpecies(getSpeciesToFix());
    }


    /**
     * Get all used weight category in catches.
     *
     * @return all used weight categories in catches.
     */
    @SuppressWarnings("unused")
    public Set<WeightCategoryTreatment> getAllWeightCategories() {
        checkInitMethodInvoked(weightCategoriesForSpecies);
        return weightCategoriesForSpecies.keySet();
    }

    /**
     * Obtain total catch weight of selected catches for this stratum.
     *
     * @return total catch weight of selected catches for this stratum.
     */
    public float getTotalCatchWeightForAllSpecies() {
        WeightCompositionModel totalModel = inputModelForAllSpecies.getTotalModel();
        return totalModel.getTotalWeight();
    }

    /**
     * Obtain total catch weight of selected catches for this stratum.
     *
     * @return total catch weight of selected catches for this stratum.
     */
    public float getTotalCatchWeightForSpeciesToFix() {
        WeightCompositionModel totalModel = inputModelForSpeciesToFix.getTotalModel();
        return totalModel.getTotalWeight();
    }

    private String logCatchStratum(DecoratorService decoratorService) {
        Locale locale = decoratorService.getLocale();
        String title = l(locale, "t3.level2.message.catchStratum.resume",
                getNbActivities(),
                getTotalCatchWeightForAllSpecies(),
                getTotalCatchWeightForSpeciesToFix());
        return WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                inputModelForAllSpecies,
                inputModelForSpeciesToFix);
    }

    public void addActivityOutputModel(WeightCompositionAggregateModel outputModel) {
        outputModelForAllSpecies.addModel(outputModel);
    }

    public void mergeGlobalCompositionModels(WeightCompositionAggregateModel inputModel, WeightCompositionAggregateModel outputModel) {
        inputModel.addModel(inputModelForAllSpecies);
        outputModel.addModel(outputModelForAllSpecies);
    }

    @Override
    protected L2CatchStratumLoader newLoader() {
        return new L2CatchStratumLoader();
    }

    WeightCompositionAggregateModel catchWeightModelForAllSpecies(Activity activity, int nbZones) {
        WeightCompositionAggregateModel catchWeightModelForAllSpecies = new WeightCompositionAggregateModel();
        fillWeightsFromCatchesWeight(activity, catchWeightModelForAllSpecies, nbZones);
        return catchWeightModelForAllSpecies;
    }

    WeightCompositionAggregateModel sampleWeightModelForAllSpecies(Activity activity) {
        WeightCompositionAggregateModel sampleWeightModelFroAllSpecies = new WeightCompositionAggregateModel();
        fillWeightsFromSamplesWeight(activity, null, sampleWeightModelFroAllSpecies);
        return sampleWeightModelFroAllSpecies;
    }

}
