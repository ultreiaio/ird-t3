package fr.ird.t3.actions.data.level2;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.stratum.LevelOutputContext;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.models.WeightCompositionModelHelper;

import java.util.Collection;

import static org.nuiton.i18n.I18n.l;

/**
 * To store all output data of {@link Level2Action}.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Level2OutputContext extends LevelOutputContext<Level2Configuration, Level2Action, Level2InputContext, L2StratumResult> {

    /**
     * Input Weight composition (based on {@link CorrectedElementaryCatch#getCatchWeight()}) for all species found in catches.
     */
    private final WeightCompositionAggregateModel inputCatchModelForAllSpecies;
    /**
     * Output Weight composition (based on {@link CorrectedElementaryCatch#getCorrectedCatchWeight()}) for all species found in catches.
     */
    private final WeightCompositionAggregateModel outputCatchModelForAllSpecies;
    private long totalCatchWeightForSpeciesFoFix;
    private long totalCatchWeight;

    public Level2OutputContext(Level2Action action, Level2InputContext inputContext) {
        super(action, inputContext);
        this.inputCatchModelForAllSpecies = new WeightCompositionAggregateModel();
        this.outputCatchModelForAllSpecies = new WeightCompositionAggregateModel();
    }

    @SuppressWarnings("unused")
    public long getTotalCatchWeightForSpeciesFoFix() {
        return totalCatchWeightForSpeciesFoFix;
    }

    @SuppressWarnings("unused")
    public long getTotalCatchWeight() {
        return totalCatchWeight;
    }

    public void mergeResult(L2CatchStratum catchStratum, L2StratumResult stratumResult) {
        catchStratum.mergeGlobalCompositionModels(inputCatchModelForAllSpecies, outputCatchModelForAllSpecies);
        totalCatchWeight += catchStratum.getTotalCatchWeightForAllSpecies();
        totalCatchWeightForSpeciesFoFix += catchStratum.getTotalCatchWeightForSpeciesToFix();
        mergeResult(stratumResult);
    }

    @SuppressWarnings("unused")
    public WeightCompositionAggregateModel getInputCatchModelForAllSpecies() {
        return inputCatchModelForAllSpecies;
    }

    @SuppressWarnings("unused")
    public WeightCompositionAggregateModel getOutputCatchModelForAllSpecies() {
        return outputCatchModelForAllSpecies;
    }

    @SuppressWarnings("unused")
    public String getInputCatchStratumLog() {
        Collection<Species> species = getSpecies();
        WeightCompositionAggregateModel inputCatchModelForSpeciesToFix = inputCatchModelForAllSpecies.extractForSpecies(species);
        String title = l(getLocale(), "t3.level2.message.stratumInputGlobalComposition.resume");
        return WeightCompositionModelHelper.decorateModel(
                getDecoratorService(),
                title,
                inputCatchModelForAllSpecies,
                inputCatchModelForSpeciesToFix);
    }

    @SuppressWarnings("unused")
    public String getOutputCatchStratumLog() {
        Collection<Species> species = getSpecies();
        WeightCompositionAggregateModel outputCatchModelForSpeciesToFix = outputCatchModelForAllSpecies.extractForSpecies(species);
        String title = l(getLocale(), "t3.level2.message.stratumOutputGlobalComposition.resume");
        return WeightCompositionModelHelper.decorateModel(
                getDecoratorService(),
                title,
                outputCatchModelForAllSpecies,
                outputCatchModelForSpeciesToFix);
    }
}
