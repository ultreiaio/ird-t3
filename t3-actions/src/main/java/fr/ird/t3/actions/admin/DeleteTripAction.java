/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.admin;

import com.google.common.base.Preconditions;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;

import static org.nuiton.i18n.I18n.l;

/**
 * Action to delete trips and or trip computed data.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class DeleteTripAction extends T3Action<DeleteTripConfiguration> {

    private static final Log log = LogFactory.getLog(DeleteTripAction.class);
    private int nbTrips;
    private int nbActivities;
    @InjectDAO(entityType = Trip.class)
    private TripTopiaDao tripDAO;
    private Decorator<Trip> tripDecorator;

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        tripDecorator = getDecoratorService().getDecorator(locale, Trip.class, DecoratorService.WITH_ID);
    }

    @Override
    protected void deletePreviousData() {
        // nothing to do here
    }

    @Override
    protected boolean executeAction() {
        Collection<String> ids = getConfiguration().getTripIds();
        boolean result = false;
        if (CollectionUtils.isNotEmpty(ids)) {
            setNbSteps(ids.size());
            if (getConfiguration().isDeleteTrip()) {
                deleteTrips(ids);
            } else {
                deleteTripsComputedData(ids);
            }
            result = true;
        }
        return result;
    }

    public int getNbTrips() {
        return nbTrips;
    }

    public void setNbTrips(int nbTrips) {
        this.nbTrips = nbTrips;
    }

    public int getNbActivities() {
        return nbActivities;
    }

    public void setNbActivities(int nbActivities) {
        this.nbActivities = nbActivities;
    }

    private void deleteTrips(Collection<String> tripIds) {
        for (String tripId : tripIds) {
            Trip trip = getTripId(tripId);
            String message = l(locale, "t3.admin.deleteTrip", tripDecorator.toString(trip));
            log.info(message);
            addInfoMessage(message);
            // then delete trips
            tripDAO.delete(trip);
        }
        getT3TopiaPersistenceContext().get().commit();
    }

    private void deleteTripsComputedData(Collection<String> tripIds) {
        for (String tripId : tripIds) {
            Trip trip = getTripId(tripId);
            String message = l(locale, "t3.admin.deleteTripDatas", tripDecorator.toString(trip));
            addInfoMessage(message);
            log.info(message);
            // then delete all computed data attached to trips
            trip.deleteComputedData();
        }
        getT3TopiaPersistenceContext().get().commit();
    }

    private Trip getTripId(String tripId) throws TopiaException {
        incrementsProgression();
        Trip trip = tripDAO.forTopiaIdEquals(tripId).findAnyOrNull();
        Preconditions.checkNotNull(trip, "Could not find with id " + tripId);
        nbActivities += trip.sizeActivity();
        nbTrips++;
        return trip;
    }
}
