package fr.ird.t3.actions.data.level2;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.stratum.LevelInputContext;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;

import java.util.Collection;

/**
 * A context to get all input data for the execution of action.
 * <p>
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Level2InputContext extends LevelInputContext<Level2Configuration, Level2Action> {

    protected Level2InputContext(Level2Action action) {
        super(action);
    }

    public Collection<CorrectedElementaryCatch> getCorrectedElementaryCatch(String activityId) {
        return getActivityCache().forId(activityId).getCorrectedElementaryCatch();
    }

}
