/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSpecies;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.nuiton.i18n.I18n.l;

/**
 * Extrapolate sample fishes numbers from counted one.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ExtrapolateSampleCountedAndMeasuredAction extends AbstractLevel1Action {

    public static final String RESULT_SPECIES_MODEL = "speciesModel";

    public ExtrapolateSampleCountedAndMeasuredAction() {
        super(Level1Step.EXTRAPOLATE_SAMPLE_COUNTED_AND_MEASURED);
    }

    public List<SpeciesCountAndMeasuredModel> getResultSpeciesModel() {
        return getResultAsList(RESULT_SPECIES_MODEL, SpeciesCountAndMeasuredModel.class);
    }

    public SpeciesCountAndMeasuredModel getTotalSpeciesModel() {
        SpeciesCountAndMeasuredModel totalResult = new SpeciesCountAndMeasuredModel(null);
        for (SpeciesCountAndMeasuredModel m : getResultSpeciesModel()) {
            totalResult.addTotalCount(m.getTotalCount());
            totalResult.addMeasuredCount(m.getMeasuredCount());
        }
        return totalResult;
    }

    @Override
    protected void deletePreviousData() {

        for (Trip trip : getSamplesByTrip().keySet()) {

            // remove level 1 data
            trip.deleteComputedDataLevel1();

            // remove level 2 data
            trip.deleteComputedDataLevel2();

            // remove level 3 data
            trip.deleteComputedDataLevel3();
        }
    }

    @Override
    protected boolean executeAction() {

        Map<Species, SpeciesCountAndMeasuredModel> model = new HashMap<>();

        setNbSteps(samplesByTrip.size());

        for (Trip trip : samplesByTrip.keySet()) {

            Collection<Sample> samples = samplesByTrip.get(trip);

            logTreatedAndNotSamplesforATrip(trip, samples);

            for (Sample sample : samples) {
                doExecuteSample(sample, model);

                // mark sample as treated for this step of level 1 treatment
                markAsTreated(sample);
            }

            // mar trip as treated for this level 1 step
            markAsTreated(trip);
        }

        // store final result in action context
        putResult(RESULT_SPECIES_MODEL, Lists.newArrayList(model.values()));
        model.clear();
        return true;
    }

    private void doExecuteSample(Sample sample, Map<Species, SpeciesCountAndMeasuredModel> model) {
        incrementsProgression();
        addInfoMessage(
                l(locale, "t3.level1.extrapolateSampleCountedAndMeasured.treat.sample",
                        sample.getSampleNumber()));
        if (sample.isSampleSpeciesEmpty()) {
            // no sample species, nothing to do...
            return;
        }
        Multimap<Species, SampleSpecies> sampleSpeciesBySpecies = SpeciesTopiaDao.groupBySpecies(sample.getSampleSpecies());
        for (Species species : sampleSpeciesBySpecies.keySet()) {
            SpeciesCountAndMeasuredModel speciesCountModel = model.computeIfAbsent(species, SpeciesCountAndMeasuredModel::new);
            Collection<SampleSpecies> sampleSpecies = sampleSpeciesBySpecies.get(species);
            // obtain total count for this species
            // obtain measured count for this species
            float totalCount = 0;
            float measuredCount = 0;
            for (SampleSpecies aSampleSpecies : sampleSpecies) {
                totalCount += aSampleSpecies.getTotalCount();
                measuredCount += aSampleSpecies.getTotalSampleSpeciesFrequencyNumber();
            }
            float rf0 = totalCount / measuredCount;
            speciesCountModel.addTotalCount(totalCount);
            speciesCountModel.addMeasuredCount(measuredCount);
            addInfoMessage(
                    l(locale, "t3.level1.extrapolateSampleCountedAndMeasured.resume.for.species",
                            species.getLabel1(), totalCount, measuredCount, rf0));
            // apply rf0 on each sample
            for (SampleSpecies sampleSpecie : sampleSpecies) {
                sampleSpecie.applyRf0(rf0);
            }
        }
    }

    public static class SpeciesCountAndMeasuredModel {

        protected final Species species;
        float totalCount;
        float measuredCount;

        public SpeciesCountAndMeasuredModel(Species species) {
            this.species = species;
        }

        public Species getSpecies() {
            return species;
        }

        public float getTotalCount() {
            return totalCount;
        }

        public float getMeasuredCount() {
            return measuredCount;
        }

        public float getRf0() {
            return totalCount / measuredCount;
        }

        public void addTotalCount(float count) {
            totalCount += count;
        }

        public void addMeasuredCount(float count) {
            measuredCount += count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SpeciesCountAndMeasuredModel that = (SpeciesCountAndMeasuredModel) o;
            return Objects.equals(species, that.species);
        }

        @Override
        public int hashCode() {
            return Objects.hash(species);
        }
    }
}
