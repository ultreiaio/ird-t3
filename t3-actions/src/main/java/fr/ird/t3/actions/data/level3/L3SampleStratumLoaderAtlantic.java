/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.type.T3Date;

import java.util.Set;

/**
 * Level 2 Sample stratum loader for atlantic ocean.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class L3SampleStratumLoaderAtlantic extends L3SampleStratumLoader {

    L3SampleStratumLoaderAtlantic(L3SampleStratum sampleStratum) {
        super(sampleStratum);
    }

    @Override
    protected Set<String> findActivityIds(int level) {
        StratumConfiguration<Level3Configuration, Level3Action> configuration = getSampleStratum().getConfiguration();
        String schoolTypeId = configuration.getSchoolType().getTopiaId();
        String zoneId = configuration.getZone().getTopiaId();
        T3Date beginDate = configuration.getBeginDate();
        T3Date endDate = configuration.getEndDate();
        int currentYear = beginDate.getYear();
        int timeStep = getTimeStep();
        switch (level) {
            case 1:
                // same stratum as catch stratum
                return findActivityIds(schoolTypeId, beginDate, endDate, zoneId);
            case 2:
                // same stratum previous year
                return findActivityIds(
                        schoolTypeId,
                        beginDate.decrementsMonths(12),
                        endDate.decrementsMonths(12),
                        zoneId);
            case 3:
                // same stratum previous time step
                return findActivityIds(
                        schoolTypeId,
                        beginDate.decrementsMonths(timeStep),
                        endDate.decrementsMonths(timeStep),
                        zoneId);
            case 4:
                // same stratum next time step
                return findActivityIds(
                        schoolTypeId,
                        beginDate.incrementsMonths(timeStep),
                        endDate.incrementsMonths(timeStep),
                        zoneId);
            case 5:
                // same stratum all the current year
                return findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear),
                        T3Date.newDate(12, currentYear),
                        zoneId);
            case 6:
                // same stratum for all previous year
                return findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear - 1),
                        T3Date.newDate(12, currentYear - 1),
                        zoneId);
            case 7:
                // same stratum  for all zones in all current year
                return findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear),
                        T3Date.newDate(12, currentYear),
                        getSampleStratum().getConfiguration().getZoneIds());
            case 8:
                // same stratum for all zones in all previous year
                return findActivityIds(
                        schoolTypeId,
                        T3Date.newDate(1, currentYear - 1),
                        T3Date.newDate(12, currentYear - 1),
                        getSampleStratum().getConfiguration().getZoneIds());
            case 9:
                // same stratum for all zones in all current and previous year for all school types
                return findActivityIds(
                        null,
                        T3Date.newDate(1, currentYear - 1),
                        T3Date.newDate(12, currentYear),
                        getSampleStratum().getConfiguration().getZoneIds());
            default:
                // null means nothing more to do
                return null;
        }
    }
}
