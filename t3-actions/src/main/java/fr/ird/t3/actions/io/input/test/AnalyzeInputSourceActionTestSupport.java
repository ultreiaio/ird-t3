/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input.test;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceAction;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceConfiguration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AnalyzeInputSourceActionTestSupport {

    private static final Log log = LogFactory.getLog(AnalyzeInputSourceActionTestSupport.class);
    @Rule
    public final FakeT3AvdthServiceContext serviceContext = new FakeT3AvdthServiceContext(true, createConfiguration());
    private BufferedWriter outputWriter;
    private File target;
    private T3InputProvider inputProvider;

    protected abstract MSAccessTestConfiguration createConfiguration();

    @Before
    public void setUp() throws IOException {
        boolean initOk = serviceContext.isInitOk();
        Assume.assumeTrue("Could not init db", initOk);
        MSAccessTestConfiguration msConfig = serviceContext.getMsConfig();
        String dbName = msConfig.dbName;
        log.debug(String.format("Do test for db %s", dbName));
        File outputFile = new File(serviceContext.getTestDir(), "result.txt");
        log.info(String.format("Will save result in file : %s", outputFile));
        outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
        File workingDirectory = serviceContext.getApplicationConfiguration().getTreatmentWorkingDirectory("yo", true);
        // push in treatment directory the base to import
        target = new File(workingDirectory, dbName);
        log.debug(String.format("Will copy msAccess from %s to %s", msConfig.accessFile, target));
        FileUtils.copyFile(msConfig.accessFile, target);
        T3InputService inputService = serviceContext.newService(T3InputService.class);
        inputProvider = inputService.getProvider(msConfig.getProviderId());
    }

    @After
    public void tearDown() throws Exception {
        serviceContext.close();
        if (outputWriter != null) {
            outputWriter.flush();
            outputWriter.close();
        }
    }

    protected void testExecute(OceanFixtures fixture) throws Exception {
        testExecute(fixture, TripType.STANDARD);
    }

    protected void testExecute(OceanFixtures fixture, TripType tripType) throws Exception {
        testExecute(
                fixture.nbSafe(),
                fixture.nbUnsafe(),
                fixture.nbSafeWithoutWell(),
                fixture.nbUnsafeWithoutWell(),
                tripType);
    }

    public FakeT3AvdthServiceContext getServiceContext() {
        return serviceContext;
    }

    public MSAccessTestConfiguration getMsConfig() {
        return serviceContext.getMsConfig();
    }

    public BufferedWriter getOutputWriter() {
        return outputWriter;
    }

    public File getTarget() {
        return target;
    }

    public T3InputProvider getInputProvider() {
        return inputProvider;
    }

    protected void testExecute(int nbSafe,
                               int nbUnsafe,
                               int nbSafeWithoutWell,
                               int nbUnsafeWithoutWell,
                               TripType tripType) throws Exception {

        MSAccessTestConfiguration msConfig = getMsConfig();
        FakeT3AvdthServiceContext serviceContext = getServiceContext();
        if (msConfig.doTest(serviceContext.getTestName())) {
            T3ServiceFactory serviceFactory = serviceContext.getServiceFactory();
            AnalyzeInputSourceConfiguration actionConfiguration = AnalyzeInputSourceConfiguration.newConfiguration(
                    inputProvider,
                    target,
                    msConfig.isUseWells(),
                    false,
                    false,
                    tripType != null && tripType == TripType.SAMPLEONLY,
                    tripType == null || tripType == TripType.LOGBOOKMISSING);
            T3ActionContext<AnalyzeInputSourceConfiguration> context = serviceFactory.newT3ActionContext(actionConfiguration, serviceContext);

            BufferedWriter outputWriter = getOutputWriter();
            outputWriter.write("----------------------------------------------------\n");
            outputWriter.write(msConfig.getAccessFile() + "\n");
            T3Action<AnalyzeInputSourceConfiguration> action;

            action = serviceFactory.newT3Action(AnalyzeInputSourceAction.class, context);
            Assert.assertNotNull(action);
            action.run();

            Set<Trip> safeTrips = action.getResultAsSet(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, Trip.class);
            Assert.assertNotNull(safeTrips);
            outputWriter.write(String.format("found %d   safe trip(s).\n", safeTrips.size()));
            Set<Trip> unsafeTrips = action.getResultAsSet(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, Trip.class);
            Assert.assertNotNull(unsafeTrips);
            outputWriter.write(String.format("found %d unsafe trip(s).\n", unsafeTrips.size()));
            List<String> messages;
            if (log.isWarnEnabled()) {
                messages = action.getWarnMessages();
                if (CollectionUtils.isNotEmpty(messages)) {
                    for (String message : messages) {
                        outputWriter.write(String.format("[WARNING] %s\n", message));
                    }
                }
            }
            if (log.isErrorEnabled()) {
                messages = action.getErrorMessages();
                if (CollectionUtils.isNotEmpty(messages)) {
                    for (String message : messages) {
                        outputWriter.write(String.format("[ERROR] %s\n", message));
                    }
                }
            }
            log.info(String.format("\n[%s] safe : %d - unsafe : %d\n", msConfig.getDbName(), safeTrips.size(), unsafeTrips.size()));
            if (msConfig.isUseWells()) {
                Assert.assertEquals(nbSafe, safeTrips.size());
                Assert.assertEquals(nbUnsafe, unsafeTrips.size());
            } else {
                Assert.assertEquals(nbSafeWithoutWell, safeTrips.size());
                Assert.assertEquals(nbUnsafeWithoutWell, unsafeTrips.size());
            }
        }
    }

}
