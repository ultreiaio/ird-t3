/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityTopiaDao;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.ExtrapolatedAllSetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.LengthCompositionAggregateModel;
import fr.ird.t3.models.LengthCompositionModel;
import fr.ird.t3.models.LengthCompositionModelHelper;
import fr.ird.t3.models.SpeciesCountAggregateModel;
import fr.ird.t3.models.SpeciesCountModel;
import fr.ird.t3.models.SpeciesCountModelHelper;
import fr.ird.t3.services.DecoratorService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.util.TimeLog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;

/**
 * Level 3 treatment action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class Level3Action extends T3Action<Level3Configuration> {

    static final String BEFORE_LEVEL3 = n("t3.level3.nbFishesBeforeLevel3");
    static final String AFTER_LEVEL3 = n("t3.level3.nbFishesAfterLevel3");
    private static final String INSERT_QUERY =
            "INSERT INTO ExtrapolatedAllSetSpeciesFrequency(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, ACTIVITY, SPECIES, LFLENGTHCLASS, NUMBER) VALUES ('%s', 0, TIMESTAMP '%s 00:00:00.0', '%s','%s', %s, %s);\n";
    private static final String UPDATE_QUERY =
            "UPDATE ExtrapolatedAllSetSpeciesFrequency SET NUMBER = NUMBER + %s WHERE TOPIAID = '%s';\n";
    private static final Log log = LogFactory.getLog(Level3Action.class);

    private String topiaCreateDate;
    private Level3InputContext inputContext;
    private Level3OutputContext outputContext;
    private int nbQuery;
    private StringBuilder queryBuffer = new StringBuilder();

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        topiaCreateDate = new SimpleDateFormat("yyyy-MM-dd").format(serviceContext.getCurrentDate());
        inputContext = new Level3InputContext(this);
        outputContext = new Level3OutputContext(this, inputContext);
        setNbSteps(3 * inputContext.getNbStratum());
    }

    @Override
    protected void deletePreviousData() {
        // data will be removed from selected activities
    }

    @Override
    protected void finalizeAction() throws IOException {
        inputContext.clear();
        super.finalizeAction();
    }

    @Override
    protected boolean executeAction() throws Exception {
        Iterator<L3StratumConfiguration> stratumConfigurationIterator;
        if (getConfiguration().isUseWeightCategoriesInStratum()) {
            stratumConfigurationIterator = new L3StratumConfigurationIteratorWithCategories(inputContext);
        } else {
            stratumConfigurationIterator = new L3StratumConfigurationIteratorWithoutCategories(inputContext);
        }
        while (stratumConfigurationIterator.hasNext()) {
            L3StratumConfiguration stratumConfiguration = stratumConfigurationIterator.next();
            // clear query buffer
            queryBuffer = new StringBuilder();
            nbQuery = 0;
            try {
                L3StratumResult result = doExecuteStratum(stratumConfiguration);
                outputContext.addStratumResult(result);
            } finally {
                // first flush delete previous level 3 data
                flushTransaction("Flush delete level 3 data.");
                // flush then new data
                long s0 = TimeLog.getTime();
                if (nbQuery > 0) {
                    log.info(String.format("Will flush %d queries.", nbQuery));
                    getT3TopiaPersistenceContext().get().getSqlSupport().executeSql(queryBuffer.toString());
                }
                getTimeLog().log(s0, "Execute queries");
            }
        }
        return true;
    }

    private L3StratumResult doExecuteStratum(L3StratumConfiguration stratumConfiguration) throws Exception {

        Collection<Species> species = inputContext.getSpecies();
        List<WeightCategoryTreatment> weightCategories = stratumConfiguration.getWeightCategoryTreatments();
        int stratumIndex = stratumConfiguration.getStratumIndex();
        int nbStratum = inputContext.getNbStratum();

        incrementsProgression();
        String stratumPrefix;
        if (getConfiguration().isUseWeightCategoriesInStratum()) {
            stratumPrefix = l(locale, "t3.level3.stratumLabelWithCategory",
                    stratumIndex, nbStratum,
                    decorate(stratumConfiguration.getSchoolType()),
                    decorate(stratumConfiguration.getZone()),
                    decorate(stratumConfiguration.getWeightCategoryTreatment()),
                    stratumConfiguration.getBeginDate(),
                    stratumConfiguration.getEndDate());
        } else {
            stratumPrefix = l(locale, "t3.level3.stratumLabel",
                    stratumIndex, nbStratum,
                    decorate(stratumConfiguration.getSchoolType()),
                    decorate(stratumConfiguration.getZone()),
                    stratumConfiguration.getBeginDate(),
                    stratumConfiguration.getEndDate());
        }
        L3StratumResult result = new L3StratumResult(stratumConfiguration, stratumPrefix);
        String message = l(locale, "t3.level3.message.start.stratum", stratumPrefix);
        log.info(message);
        addInfoMessage("==============================================================================================");
        addInfoMessage(message);
        addInfoMessage("==============================================================================================");
        //FIXME-261 Is there anything to do ?
//        if (SchoolTypeIndeterminate.IGNORE == getConfiguration().getSchoolTypeIndeterminate()) {
//            // Just report for all activities on School type indeterminate catch weight
//            Set<String> activityIds = activityDAO.findAllActivityIdsForCatchStratum(
//                    stratumConfiguration.getZoneTableName(),
//                    stratumConfiguration.getZone().getTopiaId(),
//                    Collections.singleton(SchoolTypeTopiaDao.SCHOOL_TYPE_INDETERMINATE_ID),
//                    stratumConfiguration.getBeginDate(),
//                    stratumConfiguration.getEndDate()).keySet();
//            for (String activityId : activityIds) {
//                for (CorrectedElementaryCatch correctedElementaryCatch : activityCache.get(activityId).getCorrectedElementaryCatch()) {
//                    correctedElementaryCatch.setCorrectedCatchWeight(correctedElementaryCatch.getCatchWeight());
//                }
//            }
//        }
        // get the catch stratum
        try (L3CatchStratum catchStratum = newCatchStratum(stratumConfiguration)) {
            incrementsProgression();
            if (catchStratum == null) {
                // no catch in this stratum - mark as a 0 substitution level
                result.setSubstitutionLevel(0);
                incrementsProgression();
            } else {
                // compute sample stratum
                try (L3SampleStratum sampleStratum = newSampleStratum(stratumConfiguration, catchStratum.getTotalCatchWeight())) {
                    // get the substitution level for the sample stratum
                    Integer level = sampleStratum.getSubstitutionLevel();
                    log.debug(String.format("Sample stratum substitution level %d", level));
                    incrementsProgression();
                    if (level == -1) {
                        // sample stratum could not be found - log sample stratum new composition
                        message = l(locale, "t3.level3.warning.missing.data.for.stratum", result.getLabel());
                        log.warn(message);
                        addWarningMessage(message);
                        // mark it as 999 level to make it appear at top of a reverse list
                        result.setSubstitutionLevel(999);
                        result.addNbActivities(catchStratum.getNbActivities());
                        result.addNbActivitiesWithSample(catchStratum.getNbActivitiesWithSample());
//                        FIXME-269 Should we fill at least stratumLevelN3?
                    } else {
                        // can use this sampleStratum
                        message = l(locale, "t3.level3.message.sampleStratum.resume",
                                level,
                                sampleStratum.getNbMergedActivities(),
                                sampleStratum.getSampleStratumTotalWeight());
                        message = LengthCompositionModelHelper.decorateModel(
                                getDecoratorService(),
                                message,
                                l(locale, "t3.level3.fishingCountAndRate"),
                                sampleStratum.getCompositionModel());
                        log.info(message);
                        addInfoMessage(message);
                        int activityIndex = 1;
                        int nbActivities = catchStratum.getNbActivities();
                        for (Map.Entry<Activity, Integer> e : catchStratum) {
                            Activity activity = e.getKey();
                            int nbZones = e.getValue();
                            // is activity was already treated ?
                            boolean newActivity = outputContext.addActivityId(activity.getTopiaId());
//                            if (!newActivity) {
//                                Preconditions.checkState(nbZones > 1, "Only a multi-zone activity can be seen more tha once!, problem with activity: " + activity.getTopiaId());
//                            }
                            doExecuteActivityInCatchStratum(
                                    catchStratum,
                                    sampleStratum,
                                    activity,
                                    nbZones,
                                    activityIndex++,
                                    weightCategories,
                                    nbActivities,
                                    newActivity,
                                    result.getTotalFishesCount());
                            // keep the stratum substitution level in the activity
                            stratumConfiguration.addToActivity(activity, Activity::getStratumLevelN3, Activity::setStratumLevelN3, level);
                            activity.setUseMeanStratumCompositionN3(true);
                        }
                        // --------------------------------------------------
                        // --- Report results
                        // --------------------------------------------------
                        result.setSubstitutionLevel(level);
                        result.addNbActivities(catchStratum.getNbActivities());
                        result.addNbActivitiesWithSample(catchStratum.getNbActivitiesWithSample());
                        // merge input and output model from catch stratum models
                        // compute the nb fishes before n3 for all activities of stratum catches
                        // compute the nb fishes after n3 for all activities of stratum catches
                        SpeciesCountAggregateModel totalFishesCountModel = result.getTotalFishesCount();
                        for (Map.Entry<Activity, Integer> e : catchStratum) {
                            mergeActivityTotalFishesCount(e.getKey(), totalFishesCountModel);
                        }
                        // log fishes count of the stratum
                        message = SpeciesCountModelHelper.decorateModel(
                                getDecoratorService(),
                                l(locale, "t3.level3.catchStratum.nbFishesResume.title"),
                                totalFishesCountModel.extractForSpecies(species),
                                BEFORE_LEVEL3,
                                AFTER_LEVEL3);
                        log.info(message);
                        addInfoMessage(message);
                        // merge input and output model from catch stratum models
                        outputContext.mergeResult(result, totalFishesCountModel);
                    }
                }
            }
            return result;
        }
    }

    public void mergeActivityTotalFishesCount(Activity activity, SpeciesCountAggregateModel totalFishesCountModel) {
        totalFishesCountModel.addValues(BEFORE_LEVEL3, activity.getSetSpeciesFrequency());
        totalFishesCountModel.addValues(AFTER_LEVEL3, activity.getExtrapolatedAllSetSpeciesFrequency());
    }

    private void doExecuteActivityInCatchStratum(L3CatchStratum catchStratum,
                                                 L3SampleStratum sampleStratum,
                                                 Activity activity,
                                                 int nbZones,
                                                 int activityIndex,
                                                 List<WeightCategoryTreatment> weightCategories,
                                                 int nbActivities,
                                                 boolean deleteOldData,
                                                 SpeciesCountAggregateModel totalFishesCount) throws TopiaException {
        String activityStr = String.format("%s (%s)", decorate(activity), decorate(activity.getTrip(), DecoratorService.WITH_ID));
        String message = l(locale, "t3.level3.message.start.activity", activityIndex, nbActivities, activityStr, nbZones);
        log.info(message);
        addInfoMessage(message);
        // to deal with multiple-zone activities (must keep a track of what was already done)
        boolean activityMultiZone = nbZones > 1;
        Map<String, String> previousValues = inputContext.getActivityWithMultiZones(activity);
//        if (activityMultiZone) {
//            previousValues = inputContext.getActivityWithMultiZones(activity);
//        } else {
//            previousValues = null;
//        }
        if (deleteOldData) {
            // delete old data for this activity
            log.debug(String.format("Delete previous level3 data of %s :: %d", activity.getTopiaId(), activity.sizeExtrapolatedAllSetSpeciesFrequency()));
            activity.deleteComputedDataLevel3();
        }
        boolean activityWithSample = activity.isSetSpeciesCatWeightNotEmpty();
        boolean useAllSamplesOfStratum = getConfiguration().isUseAllSamplesOfStratum();
        if (activityWithSample && !useAllSamplesOfStratum) {
            // need only to clone back data from SetSpeciesFrequency to new table
            message = l(locale, "t3.level3.message.activity.with.sample.useOwn.composition");
            log.debug(message);
            addInfoMessage(message);
            for (SetSpeciesFrequency setSpeciesFrequency : activity.getSetSpeciesFrequency()) {
                Species aSpecies = setSpeciesFrequency.getSpecies();
                SpeciesCountModel speciesCountModel = totalFishesCount.getModel(aSpecies);
                addInsertOrUpdateQuery(
                        activity,
                        previousValues,
                        aSpecies,
                        setSpeciesFrequency.getLfLengthClass(),
                        setSpeciesFrequency.getNumber() / nbZones,
                        speciesCountModel);
            }
        } else {
            if (activityWithSample) {
                message = l(locale, "t3.level3.message.activity.with.sample.useSampleStratum.composition", activityStr);
            } else {
                message = l(locale, "t3.level3.message.activity.with.no.sample.useSampleStratum.composition", activityStr);
            }
            log.debug(message);
            addInfoMessage(message);
            Map<Species, LengthCompositionAggregateModel> compositionModels = sampleStratum.getCompositionModel();
            // get all species found in catches
            Collection<CorrectedElementaryCatch> correctedElementaryCatch = catchStratum.getCorrectedElementaryCatch(activity);
            Set<Species> catchSpecies = new HashSet<>(ActivityTopiaDao.getSpecies(correctedElementaryCatch));
            // and keep only the one to fix
            catchSpecies.retainAll(inputContext.getSpecies());
            // compute a linear representation of corrected catches (by species and weight category)
            Map<String, CorrectedElementaryCatch> linearCorrectedCatches = new TreeMap<>();
            for (CorrectedElementaryCatch aCatch : correctedElementaryCatch) {
                String key = aCatch.getWeightCategoryTreatment().getTopiaId() + aCatch.getSpecies().getTopiaId();
                linearCorrectedCatches.put(key, aCatch);
            }
            // Generate for each species to fix
            for (Species aSpecies : catchSpecies) {
                // get all compositions for this species
                LengthCompositionAggregateModel compositionModel = compositionModels.get(aSpecies);
                // total count result model
                SpeciesCountModel speciesCountModel = totalFishesCount.getModel(aSpecies);
                // check this species exists in composition
                if (compositionModel == null) {
                    // missing a species in sample, can not fix this species
                    message = l(locale, "t3.level3.warning.missing.sample.species", activityStr, decorate(aSpecies));
                    log.warn(message);
                    addWarningMessage(message);
                    continue;
                }
                LengthWeightConversion conversions = catchStratum.getConfiguration().getConversionHelper().getConversions(aSpecies);
                float totalWeight = 0f;
                for (WeightCategoryTreatment weightCategory : weightCategories) {
                    // get total catch weight for this species and weight category
                    String key = weightCategory.getTopiaId() + aSpecies.getTopiaId();
                    CorrectedElementaryCatch aCatch = linearCorrectedCatches.get(key);
                    if (aCatch != null) {
                        totalWeight += aCatch.getCorrectedCatchWeight();
                    }
                }
                totalWeight = 1000 * totalWeight / nbZones;
                // get composition for all weight categories
                LengthCompositionModel model = compositionModel.getTotalModel();
                generateFrequencies(activity, aSpecies, previousValues, model, totalWeight, speciesCountModel, conversions);
            }
        }
    }

    private void generateFrequencies(Activity activity,
                                     Species aSpecies,
                                     Map<String, String> previousValues,
                                     LengthCompositionModel model,
                                     float totalWeight,
                                     SpeciesCountModel speciesCountModel,
                                     LengthWeightConversion conversions) {
        // get all length classes involved
        Set<Integer> lengthClasses = model.getLengthClasses();
        for (Integer lengthClass : lengthClasses) {
            // rate of this length class beyond the weight category
            float rate = model.getRate(lengthClass);
            // weight for this length class
            float weight = totalWeight * rate;
            // weight of a fish for this length class
            float unitWeight = conversions.computeWeightFromLFLengthClass(lengthClass);
            // number of fishes for this length class
            float number = weight / unitWeight;
            // create the frequency
            addInsertOrUpdateQuery(activity, previousValues, aSpecies, lengthClass, number, speciesCountModel);
        }
    }

    private L3CatchStratum newCatchStratum(L3StratumConfiguration stratumConfiguration) throws Exception {
        L3CatchStratum catchStratum = new L3CatchStratum(stratumConfiguration, inputContext.getSpecies());
        catchStratum.init(serviceContext, stratumConfiguration.getWeightCategoryTreatments(), this);
        // get the total weight of the catch stratum
        float catchStratumWeight = catchStratum.getTotalCatchWeight();
        if (catchStratumWeight == 0) {
            // no catch in this stratum, skip it
            String message = l(locale, "t3.level3.message.noCatch.in.stratum");
            log.info(message);
            addInfoMessage(message);
            // let's nullify the catch stratum (make it no more available)
            catchStratum = null;
        } else {
            // log it
            String message = l(locale, "t3.level3.message.catchStratum.resume", catchStratum.getNbActivities(), catchStratum.getTotalCatchWeight());
            log.info(message);
            addInfoMessage(message);
        }
        return catchStratum;
    }

    private L3SampleStratum newSampleStratum(L3StratumConfiguration stratumConfiguration, float totalCatchWeight) throws Exception {
        L3SampleStratum sampleStratum =
                new L3SampleStratum(stratumConfiguration,
                        inputContext.getSpecies(),
                        totalCatchWeight,
                        stratumConfiguration.getConversionHelper()
                );
        sampleStratum.init(serviceContext, stratumConfiguration.getWeightCategoryTreatments(), this);
        return sampleStratum;
    }

    private void addInsertOrUpdateQuery(Activity activity, Map<String, String> previousValues, Species species, int lengthClass, float number, SpeciesCountModel speciesCountModel) {
        String key = species.getTopiaId() + "__" + lengthClass;
        String oldId = previousValues == null ? null : previousValues.get(key);
        String query;
        if (oldId == null) {
            // insert query
            TopiaIdFactory topiaIdFactory = getActionContext().getApplicationContext().getConfiguration().getTopiaIdFactory();
            String newId = topiaIdFactory.newTopiaId(ExtrapolatedAllSetSpeciesFrequency.class, "" + System.nanoTime());
            query = String.format(
                    INSERT_QUERY,
                    newId,
                    topiaCreateDate,
                    activity.getTopiaId(),
                    species.getTopiaId(),
                    lengthClass,
                    number);
            if (previousValues != null) {
                // keep it in multi-zone
                previousValues.put(key, newId);
            }
        } else {
            // update query
            query = String.format(UPDATE_QUERY, number, oldId);
        }
        // add query
        queryBuffer.append(query);
        nbQuery++;
        // add to total result count
        speciesCountModel.addValue(AFTER_LEVEL3, lengthClass, number);
    }

    public Level3OutputContext getOutputContext() {
        return outputContext;
    }

    public void setOutputContext(Level3OutputContext outputContext) {
        this.outputContext = outputContext;
    }
}

