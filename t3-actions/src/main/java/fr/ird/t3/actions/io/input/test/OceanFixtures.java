package fr.ird.t3.actions.io.input.test;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Created by tchemit on 10/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface OceanFixtures {

    default int nbSafe(boolean useWells) {
        return useWells ? nbSafe() : nbSafeWithoutWell();
    }

    default int nbUnsafe(boolean useWells) {
        return useWells ? nbUnsafe() : nbUnsafeWithoutWell();
    }

    int nbSafe();

    int nbUnsafe();

    int nbSafeWithoutWell();

    int nbUnsafeWithoutWell();
}
