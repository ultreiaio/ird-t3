/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.data.Trip;

import java.util.Collection;
import java.util.EnumSet;


import static org.nuiton.i18n.I18n.n;

/**
 * An enumeration to define all steps of the level 0.
 * <p/>
 * The constants are ordered in theire natural order to be executed.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public enum Level0Step {

    /**
     * Compute rf1.
     *
     * @see ComputeRF1Action
     */
    COMPUTE_RF1(n("t3.level0.step.computeRf1")) {
        @Override
        public boolean getTripState(Trip trip) {
            return trip.isRf1Computed();
        }

        @Override
        public void setTripState(Trip trip, boolean newState) {
            trip.setRf1Computed(newState);
        }
    },

    /**
     * Compute rf2.
     *
     * @see ComputeRF2Action
     */
    COMPUTE_RF2(n("t3.level0.step.computeRf2")) {
        @Override
        public boolean getTripState(Trip trip) {
            return trip.isRf2Computed();
        }

        @Override
        public void setTripState(Trip trip, boolean newState) {
            trip.setRf2Computed(newState);
        }
    },

    /**
     * Standardize sample measured.
     *
     * @see ConvertCatchesWeightCategoriesAction
     */
    CONVERT_CATCHES_WEIGHT_CATEGORIES(
            n("t3.level0.step.convertCatchesWeightCategories")) {
        @Override
        public boolean getTripState(Trip trip) {
            return trip.isCatchesWeightCategorieConverted();
        }

        @Override
        public void setTripState(Trip trip, boolean newState) {
            trip.setCatchesWeightCategorieConverted(newState);
        }
    },
    /**
     * Compute weight of categories for set.
     *
     * @see ComputeSetDurationAndPositiveSetCountAction
     */
    COMPUTE_SET_DURATION_AND_POSITIVE_SET_COUNT(
            n("t3.level0.step.computeSetDurationAndPositiveSetCount")) {
        @Override
        public boolean getTripState(Trip trip) {
            return trip.isSetDurationAndPositiveCountComputed();
        }

        @Override
        public void setTripState(Trip trip, boolean newState) {
            trip.setSetDurationAndPositiveCountComputed(newState);
        }
    },
    /**
     * Redistribute sample set frequencies to set.
     *
     * @see ComputeTripEffortsAction
     */
    COMPUTE_TRIP_EFFORTS(n("t3.level0.step.computeTripEfforts")) {
        @Override
        public boolean getTripState(Trip trip) {
            return trip.isEffortComputed();
        }

        @Override
        public void setTripState(Trip trip, boolean newState) {
            trip.setEffortComputed(newState);
        }
    },
    /**
     * Compute for each well of the trip the wellSetAllSpecies..
     *
     * @see ComputeWellPlanWeightCategoriesProportionsAction
     */
    COMPUTE_WELL_PLAN_WEIGHT_CATEGORIES_PROPORTIONS(n("t3.level0.step.computeWellPlanWeightCategoriesProportions")) {
        @Override
        public boolean getTripState(Trip trip) {
            return trip.isWellPlanWeightCategoriesComputed();
        }

        @Override
        public void setTripState(Trip trip, boolean newState) {
            trip.setWellPlanWeightCategoriesComputed(newState);
        }
    };

    private final String i18nKey;

    Level0Step(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public String getI18nKey() {
        return i18nKey;
    }

    /**
     * Gets the executed state value for the given trip
     *
     * @param trip the trip to test
     * @return the executed state value for the given trip
     */
    public abstract boolean getTripState(Trip trip);

    /**
     * Sets the executed state value for the given trip.
     *
     * @param trip     the trip to change
     * @param newState the new state value
     */
    public abstract void setTripState(Trip trip, boolean newState);

    public static EnumSet<Level0Step> allBefore(Level0Step step) {
        return T3EntityHelper.allBefore(Level0Step.class, step, values());
    }

    public static EnumSet<Level0Step> allAfter(Level0Step step) {
        return T3EntityHelper.allAfter(Level0Step.class, step, values());
    }

    public static void invalidate(Trip trip, Collection<Level0Step> steps) {
        for (Level0Step step : steps) {
            step.setTripState(trip, false);
        }
    }
}
