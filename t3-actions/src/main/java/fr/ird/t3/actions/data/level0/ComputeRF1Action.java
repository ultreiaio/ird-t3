/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.CompleteTrip;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.RF1SpeciesForFleet;
import fr.ird.t3.entities.reference.RF1SpeciesForFleetTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Action to compute Raising Factor 1 on each trip.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ComputeRF1Action extends AbstractLevel0Action<ComputeRF1Configuration> {

    private static final Log log = LogFactory.getLog(ComputeRF1Action.class);
    /** total catch weight for trips with rf1 != null. */
    private float totalCatchWeightRF1;
    /** total landing weight for trips with rf1 != null. */
    private float totalLandingWeight;
    /** Number of trips rejected (with no logbook). */
    private int nbRejectedTrips;
    /** Number of accepted trips (says with a rf1 computation). */
    private int nbAcceptedTrips;
    /** Number of accepted complete trips (says with a rf1 computation). */
    private int nbCompleteAcceptedTrips;
    /** Number of complete accepted trips (says with a rf1 computation, but not in defined bound). */
    private int nbCompleteAcceptedTripsWithBadRF1;
    @InjectDAO(entityType = RF1SpeciesForFleet.class)
    private RF1SpeciesForFleetTopiaDao rF1SpecieForFleetDAO;
    /** usable trips group by vessel */
    private Multimap<Vessel, Trip> tripsByVessel;
    /** usable species group by country */
    private Multimap<Country, Species> speciesByCountry;
    /** Number of complete trips. */
    private int nbCompleteTrips;
    /** Number of complete trips rejected (with no logbook). */
    private int nbCompleteRejectedTrips;

    public ComputeRF1Action() {
        super(Level0Step.COMPUTE_RF1);
    }

    @SuppressWarnings("unused")
    public int getNbCompleteAcceptedTrips() {
        return nbCompleteAcceptedTrips;
    }

    public void setNbCompleteAcceptedTrips(int nbCompleteAcceptedTrips) {
        this.nbCompleteAcceptedTrips = nbCompleteAcceptedTrips;
    }

    @SuppressWarnings("unused")
    public int getNbCompleteAcceptedTripsWithBadRF1() {
        return nbCompleteAcceptedTripsWithBadRF1;
    }

    public void setNbCompleteAcceptedTripsWithBadRF1(int nbCompleteAcceptedTripsWithBadRF1) {
        this.nbCompleteAcceptedTripsWithBadRF1 = nbCompleteAcceptedTripsWithBadRF1;
    }

    @SuppressWarnings("unused")
    public float getTotalCatchWeightRF1() {
        return totalCatchWeightRF1;
    }

    public void setTotalCatchWeightRF1(int totalCatchWeightRF1) {
        this.totalCatchWeightRF1 = totalCatchWeightRF1;
    }

    @SuppressWarnings("unused")
    public float getTotalLandingWeight() {
        return totalLandingWeight;
    }

    public void setTotalLandingWeight(int totalLandingWeight) {
        this.totalLandingWeight = totalLandingWeight;
    }

    @SuppressWarnings("unused")
    public int getNbRejectedTrips() {
        return nbRejectedTrips;
    }

    public void setNbRejectedTrips(int nbRejectedTrips) {
        this.nbRejectedTrips = nbRejectedTrips;
    }

    @SuppressWarnings("unused")
    public int getNbCompleteRejectedTrips() {
        return nbCompleteRejectedTrips;
    }

    @SuppressWarnings("unused")
    public int getNbAcceptedTrips() {
        return nbAcceptedTrips;
    }

    public void setNbAcceptedTrips(int nbAcceptedTrips) {
        this.nbAcceptedTrips = nbAcceptedTrips;
    }

    @SuppressWarnings("unused")
    public int getNbCompleteTrips() {
        return nbCompleteTrips;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();
        // get all species usable for rf1 for all selected fleet countries
        Multimap<Country, Species> result = rF1SpecieForFleetDAO.getSpeciesForCountry(fleets);
        setSpeciesByCountry(result);
        List<Trip> tripList = getUsableTrips(null, false);
        setTrips(tripList);
        // get all trips group by the vessel
        setTripsByVessel(TripTopiaDao.groupByVessel(tripList));
    }

    @Override
    protected void deletePreviousData() {
        for (Trip trip : trips) {
            // remove level0 data
            trip.deleteComputedDataLevel0();
            // remove level 1 data
            trip.deleteComputedDataLevel1();
            // remove level 2 data
            trip.deleteComputedDataLevel2();
            // remove level 3 data
            trip.deleteComputedDataLevel3();
        }
    }

    @Override
    protected boolean executeAction() {
        Set<Vessel> allVessel = tripsByVessel.keySet();
        boolean result = false;
        if (CollectionUtils.isNotEmpty(allVessel)) {
            setNbSteps(allVessel.size());
            for (Vessel vessel : allVessel) {
                log.debug(String.format("Treat vessel %s", vessel.getLabel1()));
                List<Trip> tripsForVessel = Lists.newArrayList(tripsByVessel.get(vessel));
                TripTopiaDao.sortTrips(tripsForVessel);
                result |= doExecuteForVessel(vessel, tripsForVessel);
            }
            addInfoMessage(l(locale, "t3.level0.computeRF1.totalCatchWeightRF1", totalCatchWeightRF1));
            addInfoMessage(l(locale, "t3.level0.computeRF1.totalLandingWeight", totalLandingWeight));
        }
        return result;
    }

    private boolean doExecuteForVessel(Vessel vessel, List<Trip> tripList) {
        incrementsProgression();
        String vesselStr = decorate(vessel);
        addInfoMessage(l(locale, "t3.level0.computeRF1.treat.vessel", vesselStr));
        if (CollectionUtils.isEmpty(tripList)) {
            // no trip for vessel: nothing to commit
            return false;
        }
        addInfoMessage(l(locale, "t3.level0.computeRF1.trips.to.use.for.vessel", tripList.size(), vesselStr));
        // get country of the vessel (always use the fleet country)
        Country country = vessel.getFleetCountry();
        // get species usable for this country
        Collection<Species> species = speciesByCountry.get(country);
        if (CollectionUtils.isEmpty(species)) {
            // means no rf1SpecieForFleet found, no rf1 to compute (use a rf1.0)
            String message =
                    l(locale, "t3.level0.computeRF1.warning.no.species.usable.for.country",
                            country.getLabel1());
            log.warn(message);
            addWarningMessage(message);
        } else {
            // there is some species to use for rf1 computation
            StringBuilder sb = new StringBuilder();
            for (Species aSpecies : species) {
                sb.append(", ").append(decorate(aSpecies));
            }
            String message = l(locale, "t3.level0.computeRF1.species.to.use", species.size(), sb.substring(2));
            log.info(message);
            addInfoMessage(message);
        }
        float minimumRate = getConfiguration().getMinimumRate();
        float maximumRate = getConfiguration().getMaximumRate();
        // split trips by complete trips
        List<CompleteTrip> completeTrips = tripDAO.toCompleteTrip(tripList);
        addInfoMessage(l(locale, "t3.level0.computeRF1.complete.trips.to.use.for.vessel",
                completeTrips.size(), vesselStr));
        nbCompleteTrips += completeTrips.size();
        for (CompleteTrip completeTrip : completeTrips) {
            // consume the completeTrip
            treatCompleteTrip(completeTrip, species, minimumRate, maximumRate);
            // complete trip was consumed
            completeTrip.removeTrips(tripList);
        }
        if (CollectionUtils.isNotEmpty(tripList)) {
            // still some trips not complete
            for (Trip trip : tripList) {
                String message = l(locale, "t3.level0.computeRF1.warning.trip.is.not.complete",
                        decorate(trip, DecoratorService.WITH_ID));
                log.warn(message);
                addWarningMessage(message);
            }
            // set completionStatus to 0
            applyCompletionStatus(tripList, 0);
        }
        return true;
    }

    private void treatCompleteTrip(CompleteTrip completeTrip, Collection<Species> species, float minimumRate, float maximumRate) {
        int completionStatus;
        if (completeTrip.getNbTrips() == 1) {
            // simple trip
            completionStatus = 1;
        } else {
            // partial trip
            completionStatus = 2;
        }
        applyCompletionStatus(completeTrip, completionStatus);
        boolean allWithLogBook = TripTopiaDao.isTripsAllWithLogBook(completeTrip);
        if (!allWithLogBook) {
            // there is some trips with no log book reject all the complete trip
            nbRejectedTrips += completeTrip.getNbTrips();
            nbCompleteRejectedTrips += 1;
            // update totalWeights
            updateTotalWeights(completeTrip, species);
            // apply on them a null rf1
            applyRF1(null, completeTrip, null);
        } else {
            // complete trip safe, can compute rf1 of it one more complete trip accepted
            nbCompleteAcceptedTrips++;
            // size of the complete trips accepted
            nbAcceptedTrips += completeTrip.getNbTrips();
            // compute rf1 for the complete trip
            float rf1;
            if (species == null) {
                // means there is so species for this complete trip so rf1 = 1.0
                rf1 = 1.0f;
            } else {
                boolean useLegacyBehaviourForTripWithoutLanding = getConfiguration().isUseLegacyBehaviourForTripWithoutLanding();
                rf1 = computeRF1ForCompleteTrip(useLegacyBehaviourForTripWithoutLanding, completeTrip, species);
                if (rf1 < minimumRate) {
                    String warnMessage =
                            l(locale, "t3.level0.computeRF1.warning.too.low.rf1",
                                    completeTrip.getDepartureTrip().getVesselLabel(),
                                    DecoratorService.formatDate(completeTrip.getDepartureTrip().getDepartureDate()),
                                    DecoratorService.formatDate(completeTrip.getLandingTrip().getLandingDate()),
                                    rf1, minimumRate);
                    addWarningMessage(warnMessage);
                    nbCompleteAcceptedTripsWithBadRF1++;
                }
                if (rf1 > maximumRate) {
                    String warnMessage =
                            l(locale, "t3.level0.computeRF1.warning.too.high.rf1",
                                    completeTrip.getDepartureTrip().getVesselLabel(),
                                    DecoratorService.formatDate(completeTrip.getDepartureTrip().getDepartureDate()),
                                    DecoratorService.formatDate(completeTrip.getLandingTrip().getLandingDate()),
                                    rf1, maximumRate);
                    addWarningMessage(warnMessage);
                    nbCompleteAcceptedTripsWithBadRF1++;
                }
            }
            applyRF1(rf1, completeTrip, species);
            // update totalWeights
            updateTotalWeights(completeTrip, species);
        }
    }

    private void applyCompletionStatus(Iterable<Trip> trips, int completionStatus) {
        for (Trip trip : trips) {
            trip.setCompletionStatus(completionStatus);
        }
    }

    private void applyRF1(Float rf1, CompleteTrip trips, Collection<Species> species) {
        for (Trip trip : trips) {
            String tripStr = decorate(trip);
            Float rf1ToUse = rf1;
            if (!trip.isWithLogbook()) {
                // means can not apply any rf1 : rf1 must stay at null...
                rf1ToUse = null;
                addWarningMessage(l(locale, "t3.level0.computeRF1.warning.no.logbook", tripStr));
            }
            trip.applyRf1(rf1ToUse, species);
            markTripAsTreated(trip);
            addInfoMessage(l(locale, "t3.level0.computeRF1.resume.rf1.for.trip", tripStr, rf1ToUse));
        }
    }

    private void updateTotalWeights(CompleteTrip completeTrip, Collection<Species> species) {
        float tripTotalCatchWeightRf1 = completeTrip.getElementaryCatchTotalWeightRf1(species);
        totalCatchWeightRF1 += tripTotalCatchWeightRf1;
        float tripTotalLandingWeight = completeTrip.getElementaryLandingTotalWeight(species);
        totalLandingWeight += tripTotalLandingWeight;
        log.info(String.format("After trip %s tripTotalCatchWeightRf1 = %s tripTotalLandingWeight = %s",
                decorate(completeTrip.getLandingTrip()), tripTotalCatchWeightRf1, tripTotalLandingWeight));
        log.debug(String.format("After trip %s totalCatchWeightRF1 = %s totalLandingWeight = %s",
                decorate(completeTrip.getLandingTrip()), totalCatchWeightRF1, totalLandingWeight));
    }

    private float computeRF1ForCompleteTrip(boolean useLegacyBehaviourForTripWithoutLanding, CompleteTrip completeTrip, Collection<Species> speciesList) {
        // do the computation of rf1 for all trips of the complete trip
        float sumLanding = 0;
        float sumCatch = 0;
        float sumLocalMarket = 0;
        float sumLocalMarketDetailled = 0;
        int tripWithoutLanding = 0;
        for (Trip trip : completeTrip) {
            String tripStr = decorate(trip, DecoratorService.WITH_ID);
            log.debug(String.format("Start count for trip %s", tripStr));
            float elementaryLandingTotalWeight = trip.getElementaryLandingTotalWeight(speciesList);
            float elementaryCatchTotalWeight = trip.getElementaryCatchTotalWeight(speciesList);
            boolean noLanding = elementaryLandingTotalWeight == 0;
            if (noLanding) {
                tripWithoutLanding++;
            }
            if (elementaryCatchTotalWeight == 0 && elementaryLandingTotalWeight > 0) {
                // Add a warning, seems not possible to have no catches and landing :
                // the logBookAvailability flag should be setted to 0.
                addWarningMessage(l(locale, "t3.level0.computeRF1.warning.no.catches.but.some.landings",
                        tripStr, elementaryLandingTotalWeight));
            }
            if (elementaryCatchTotalWeight > 0 && noLanding) {
                // Special case : no landing: do not take account of the catch weight
                // in fact (see https://gitlab.com/ultreiaio/ird-t3/issues/283), we do not want to loose this catches
                //elementaryCatchTotalWeight = 0;
                addWarningMessage(l(locale, "t3.level0.computeRF1.warning.no.landings.but.some.catches",
                        tripStr, elementaryLandingTotalWeight));
            }
            float localMarketTotalWeight = trip.getFalseFishesWeight();
            float localMarketTotalWeightDetailled = trip.getLocalMarketBatchTotalWeight(speciesList);
            addInfoMessage(l(locale, "t3.level0.computeRF1.resume.for.trip",
                    tripStr, elementaryCatchTotalWeight, elementaryLandingTotalWeight,
                    localMarketTotalWeight, localMarketTotalWeightDetailled));
            log.debug("elementaryLandingTotalWeight    = " + elementaryLandingTotalWeight);
            log.debug("elementaryCatchTotalWeight      = " + elementaryCatchTotalWeight);
            log.debug("localMarketTotalWeight          = " + localMarketTotalWeight);
            log.debug("localMarketTotalWeightDetailled = " + localMarketTotalWeightDetailled);
            sumLanding += elementaryLandingTotalWeight;
            sumCatch += elementaryCatchTotalWeight;
            sumLocalMarket += localMarketTotalWeight;
            sumLocalMarketDetailled += localMarketTotalWeightDetailled;
        }
        float rf1 = 1;
        boolean computeRF1 = sumCatch > 0 && (useLegacyBehaviourForTripWithoutLanding || tripWithoutLanding == 0);
        if (computeRF1) {
            rf1 = sumLanding / sumCatch;
        }
        log.debug(String.format("Computed rf1 %s", rf1));
        addInfoMessage(l(locale, "t3.level0.computeRF1.resume.for.complete.trip",
                sumCatch, sumLanding, sumLocalMarket, sumLocalMarketDetailled, rf1));
        return rf1;
    }

    private void setSpeciesByCountry(Multimap<Country, Species> speciesByCountry) {
        this.speciesByCountry = speciesByCountry;
    }

    private void setTripsByVessel(Multimap<Vessel, Trip> tripsByVessel) {
        this.tripsByVessel = tripsByVessel;
    }

}
