package fr.ird.t3.actions.data.level2;

/*-
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.type.T3Date;

import java.util.Collection;
import java.util.List;

/**
 * Created by tchemit on 21/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class L2StratumConfiguration extends StratumConfiguration<Level2Configuration, Level2Action> {

    L2StratumConfiguration(Level2InputContext inputContext,
                           SchoolType schoolType,
                           ZoneStratumAware zone,
                           WeightCategoryTreatment weightCategoryTreatment,
                           T3Date beginDate,
                           int stratumIndex,
                           Collection<ZoneStratumAware> zones,
                           List<WeightCategoryTreatment> weightCategoryTreatments) {
        super(
                inputContext,
                zone,
                schoolType,
                weightCategoryTreatment,
                beginDate,
                beginDate.incrementsMonths(inputContext.getConfiguration().getTimeStep() - 1),
                zones,
                null,
                weightCategoryTreatments,
                stratumIndex);
    }
}
