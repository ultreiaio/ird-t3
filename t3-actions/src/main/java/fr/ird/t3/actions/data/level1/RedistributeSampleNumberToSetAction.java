/*
 * #%L
 * T3 :: Actions
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequencyTopiaDao;
import fr.ird.t3.entities.data.StandardiseSampleSpecies;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequency;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.services.ioc.InjectDAO;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Redistribute number of fishes from sample to his owning set.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class RedistributeSampleNumberToSetAction extends AbstractLevel1Action {

    @InjectDAO(entityType = SampleSetSpeciesFrequency.class)
    private SampleSetSpeciesFrequencyTopiaDao sampleSetSpeciesFrequencyDAO;

    public RedistributeSampleNumberToSetAction() {
        super(Level1Step.REDISTRIBUTE_SAMPLE_SET_TO_SET);
    }

    public Set<Species> getSpeciesOfSample(Sample sample) {
        return SpeciesTopiaDao.getAllSpecies(sample.getStandardiseSampleSpecies());
    }

    public SpeciesModel getResultSpeciesModel(Sample sample, Species species) {
        SpeciesModel result = new SpeciesModel(species);
        // get the sum of number for the sample (for this given species)
        float sum = sample.getTotalStandardiseSampleSpeciesFrequencyNumber(species);
        result.setSampleSum(sum);
        for (SampleSet sampleSet : sample.getSampleSet()) {
            float sampleWellSum = sampleSet.getTotalSampleSetSpeciesFrequencyNumber(species);
            result.addSampleWellSum(sampleSet, sampleWellSum);
        }
        return result;
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 1 at first step)
        for (Sample sample : samplesByTrip.values()) {
            for (SampleSet sampleSet : sample.getSampleSet()) {
                sampleSet.clearSampleSetSpeciesFrequency();
            }
        }
    }

    @Override
    protected boolean executeAction() {
        setNbSteps(2 * samplesByTrip.size());
        for (Trip trip : samplesByTrip.keySet()) {
            Collection<Sample> samples = samplesByTrip.get(trip);
            logTreatedAndNotSamplesforATrip(trip, samples);
            for (Sample sample : samples) {
                // treat for current sample
                doExecuteSample(sample);
                // mark sample as treated for this step of level 1 treatment
                markAsTreated(sample);
            }
            // mar trip as treated for this level 1 step
            markAsTreated(trip);
        }
        return true;
    }

    private void doExecuteSample(Sample sample) {
        addInfoMessage(l(locale, "t3.level1.redistributeSampleNumberToSet.treat.sample", sample.getSampleNumber()));
        Map<SampleSet, Float> frequenciesSum = new HashMap<>();
        // remove all previous computed data for the sample
        for (SampleSet sampleSet : sample.getSampleSet()) {
            frequenciesSum.put(sampleSet, 0f);
        }
        incrementsProgression();
        long sampleTotalNumber = 0L;
        long sampleWellTotalNumber = 0L;
        for (StandardiseSampleSpecies sampleSpecies : sample.getStandardiseSampleSpecies()) {
            Species species = sampleSpecies.getSpecies();
            for (StandardiseSampleSpeciesFrequency sampleSpeciesFrequency : sampleSpecies.getStandardiseSampleSpeciesFrequency()) {
                float number = sampleSpeciesFrequency.getNumber();
                sampleTotalNumber += number;
                int lfLengthClass = sampleSpeciesFrequency.getLfLengthClass();
                for (SampleSet sampleSet : sample.getSampleSet()) {
                    float setNumber = number * sampleSet.getPropWeightedWeight();
                    Float sum = frequenciesSum.get(sampleSet);
                    sum += setNumber;
                    frequenciesSum.put(sampleSet, sum);
                    SampleSetSpeciesFrequency sampleSetSpeciesFrequency =
                            sampleSetSpeciesFrequencyDAO.create(
                                    SampleSetSpeciesFrequency.PROPERTY_LF_LENGTH_CLASS, lfLengthClass,
                                    SampleSetSpeciesFrequency.PROPERTY_SPECIES, species,
                                    SampleSetSpeciesFrequency.PROPERTY_NUMBER, setNumber);
                    sampleSet.addSampleSetSpeciesFrequency(sampleSetSpeciesFrequency);
                }
            }
        }
        incrementsProgression();
        for (SampleSet sampleSet : sample.getSampleSet()) {
            Float sum = frequenciesSum.get(sampleSet);
            sampleWellTotalNumber += sum;
        }
        String message = l(locale, "t3.level1.redistributeSampleNumberToSet.resume.for.sample",
                decorate(sample), sampleTotalNumber, sampleWellTotalNumber);
        addInfoMessage(message);
        for (SampleSet sampleSet : sample.getSampleSet()) {
            Float sum = frequenciesSum.get(sampleSet);
            sampleWellTotalNumber += sum;
            message = l(locale, "t3.level1.redistributeSampleNumberToSet.resume.for.sampleSet",
                    sampleSet.getTopiaId(), sampleSet.getPropWeightedWeight(), sum);
            addInfoMessage(message);
        }
    }

    public static class SpeciesModel {

        protected final Species species;
        final Map<SampleSet, Float> sampleWellSum;
        float sampleSum;

        SpeciesModel(Species species) {
            this.species = species;
            sampleWellSum = new HashMap<>();
        }

        public Species getSpecies() {
            return species;
        }

        public float getSampleSum() {
            return sampleSum;
        }

        public void setSampleSum(float sampleSum) {
            this.sampleSum = sampleSum;
        }

        public Map<SampleSet, Float> getSampleWellSum() {
            return sampleWellSum;
        }

        public void addSampleWellSum(SampleSet sampleSet, float sum) {
            sampleWellSum.put(sampleSet, sum);
        }

        public float getSampleWellSum(SampleSet sampleSet) {
            return sampleWellSum.get(sampleSet);
        }
    }
}
