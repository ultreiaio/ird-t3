/*
 * #%L
 * T3 :: Test
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3;

import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.user.T3Users;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import fr.ird.t3.test.T3H2Database;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Date;
import java.util.Locale;
import java.util.function.Supplier;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class FakeT3ServiceContext extends T3H2Database implements T3ServiceContext {

    private final T3ServiceFactory serviceFactory = new T3ServiceFactory();
    protected T3TopiaPersistenceContext transaction;
    private Locale locale;


    public FakeT3ServiceContext() {
        this(false);
    }

    public FakeT3ServiceContext(boolean injectReferential) {
        super(injectReferential);
        setLocale(Locale.FRANCE);
    }

    @Override
    public T3Users getT3Users() {
        return null;
    }

    /** May be used in test to get a fresh transaction. */
    @Override
    public Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        return () -> transaction == null ? transaction = newTransaction() : transaction;
    }

    @Override
    public T3TopiaApplicationContext getApplicationContext() {
        return topiaApplicationContext;
    }

    private T3TopiaPersistenceContext newTransaction() throws TopiaException {
        return topiaApplicationContext.newPersistenceContext();
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz, this);
    }

    @Override
    public void setTransaction(T3TopiaPersistenceContext transaction) {
        this.transaction = transaction;
    }

    @Override
    public Date getCurrentDate() {
        return new Date();
    }

}
