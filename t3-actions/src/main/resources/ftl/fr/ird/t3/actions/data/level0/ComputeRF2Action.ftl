<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header.ftl"/>

<#if configuration.configurationEmpty>

Aucun port de débarquement, type de navire et flotte sélectionné, le calcul de RF2 sera positionné à 1 pour toutes les
marées définies sur la plage de date.
</#if>

Date de début : ${configuration.beginDate}
Date de fin   : ${configuration.endDate}

<#if !configuration.configurationEmpty>
  <#list landingHarbours?values as landingHarbour>
  Port de débarquement : ${landingHarbour}
  </#list>

  <#list vesselSimpleTypes?values as vesselSimpleType>
  Type de navire sélectionné : ${vesselSimpleType}
  </#list>

  <#list fleets?values as fleet>
  Flotte sélectionné : ${fleet}
  </#list>
</#if>

Indicateurs
-----------

- Nombre de strates                   : ${action.nbStratum}
- Nombre de marées                    : ${action.nbTrips}
- Nombre de marées avec calcul du rf2 : ${action.nbTripsWithRF2}

<#include "/ftl/showMessages.ftl"/>
