<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#assign output  = action.outputContext/>
<#assign headerSize  = output.maximumSizeForStratum/>
<#function tableFormat arg0, arg1, arg2>
  <#return statics['java.lang.String'].format("%1$-"+headerSize+"s | %2$-15s | %3$-30s |", arg0, arg1, arg2)>
</#function>
<#include "/ftl/header_en.ftl"/>

Begin date: ${configuration.beginDate}
End date:   ${configuration.endDate}
Time step:  ${configuration.timeStep}

Treatment to apply to catches with samples:
<#if configuration.useAllSamplesOfStratum>
Apply length structures from the sample stratum
<#else>
Keep their own samples (do nothing)
</#if>

<#if configuration.useWeightCategoriesInStratum>
Use weight categories +10/-10 in stratification.
<#else>
Do not use weight categories +10/-10 in stratification.
</#if>

<#list oceans?values as ocean>
Selected ocean: ${ocean}
</#list>

<#list zoneTypes?values as zoneType>
Selected type of zone: ${zoneType}
</#list>

<#list zoneVersions?values as zoneVersion>
Selected version of zone : ${zoneVersion}
</#list>

<#list catchFleets?values as catchFleet>
Selected catches fleet: ${catchFleet}
</#list>

<#list species?values as speciesLabel>
Selected species: ${speciesLabel}
</#list>

<#list sampleFleets?values as sampleFleet>
Selected samples fleet: ${sampleFleet}
</#list>

<#list sampleFlags?values as sampleFlag>
Selected samples flags: ${sampleFlag}
</#list>

Quality of sample stratum:

- Minimum weight ratio for catches stratum and sample stratum: ${configuration.stratumWeightRatio}

- Minimum count of samples (by species):
<#list species?keys as speciesId>
  <#assign speciesLabel = species[speciesId]/>
  <#assign value = configuration.stratumMinimumSampleCount[speciesId]/>
  - Species ${speciesLabel} / free school: ${value.minimumCountForFreeSchool} / object school: ${value.minimumCountForObjectSchool}
</#list>

Indicators
----------

- Number of strata:                         ${output.nbStratum}
- Number of treated strata:                 ${output.nbStrataFixed}
- Total number of treated sets:             ${output.totalCatchActivities}
- Number of treated sets (with samples):    ${output.totalCatchActivitiesWithSample}
- Number of treated sets (without samples): ${output.totalCatchActivitiesWithoutSample}

${output.totalFishesCountResume}

Sample Stratum (by substitution level)
--------------------------------------

Level 0 is used when no catches was found in stratum (nothing to do here)
Level 999 is used when highest level was reached but could not pass quality tests

${tableFormat("Stratum","Number of sets","Number of sets with samples")}
<#list output.allSubstitutionLevels as substitutionLevel>
  <#assign stratums = output.getStratumResult(substitutionLevel)/>
- Substitution level ${substitutionLevel} (${stratums?size} strate(s))
  <#list stratums as stratum>
  ${tableFormat(stratum.label,stratum.nbActivities,stratum.nbActivitiesWithSample)}
  </#list>
</#list>

<#include "/ftl/showMessages_en.ftl"/>
