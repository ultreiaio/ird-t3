<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header.ftl"/>

Pilote d'acquisition de données : ${configuration.inputProvider.label}
Source de données               : ${configuration.inputFile.name}
Utilisation des plans de cuves  : ${configuration.useWells?string}
Autoriser à créer des activités : ${configuration.canCreateVirtualActivity?string}
Autoriser à créer des bateaux   : ${configuration.canCreateVessel?string}
<#if configuration.canCreateVessel>
Créer des bateaux virtuels      : ${configuration.createVirtualVessel?string}
</#if>

<#if configuration.tripsToDelete?size &gt; 0>
Nombre de marées à supprimer    : ${configuration.tripsToDelete?size}
</#if>
Nombre de marées à importer     : ${configuration.tripsToImport?size}

Indicateurs
-----------

<#if configuration.tripsToDelete?size &gt; 0>
- Nombre de marées supprimées   : ${configuration.tripsToDelete?size}
</#if>
- Nombre de marées importées    : ${configuration.tripsToImport?size}

<#if configuration.tripsToDelete?size &gt; 0>

Les marées suivantes ont été supprimées de la base :

<#list configuration.tripsToDelete as trip>
- ${tripDecorator.toString(trip)}
</#list>
</#if>

Liste des marées importées :

<#list configuration.tripsToImport as trip>
- ${tripDecorator.toString(trip)}
</#list>

<#include "/ftl/showMessages.ftl"/>
