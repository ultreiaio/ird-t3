<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header_en.ftl"/>

Begin date: ${configuration.beginDate}
End date:   ${configuration.endDate}

<#list vesselSimpleTypes?values as vesselSimpleType>
Selected simple vessel type: ${vesselSimpleType}
</#list>

<#list fleets?values as fleet>
Selected fleet: ${fleet}
</#list>

Indicators
----------

- Number of trips:   ${action.nbTrips}
- Total sea time:    ${action.totalTimeAtSeaN0}
- Total fish time:   ${action.totalFishingTimeN0}
- total search time: ${action.totalSearchTimeN0}

<#include "/ftl/showMessages_en.ftl"/>



