<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header_en.ftl"/>

Input Pilot:                 ${configuration.inputProvider.label}
Input file:                  ${configuration.inputFile.name}
Use wells:                   ${configuration.useWells?string}
Authorise to create activities: ${configuration.canCreateVirtualActivity?string}
Authorise to create vessels: ${configuration.canCreateVessel?string}
<#if configuration.canCreateVessel>
Create virtual vessels:      ${configuration.createVirtualVessel?string}
</#if>

<#if configuration.tripsToDelete?size &gt; 0>
Number of trips to delete : ${configuration.tripsToDelete?size}
</#if>
Number of trips to import : ${configuration.tripsToImport?size}

Indicators
----------

<#if configuration.tripsToDelete?size &gt; 0>
- Number of deleted trips  : ${configuration.tripsToDelete?size}
</#if>
- Number of imported trips : ${configuration.tripsToImport?size}

<#if configuration.tripsToDelete?size &gt; 0>

Following trips were deleted from the database:

<#list configuration.tripsToDelete as trip>
- ${tripDecorator.toString(trip)}
</#list>
</#if>

Following trips were imported in the database:

<#list configuration.tripsToImport as trip>
- ${tripDecorator.toString(trip)}
</#list>

<#include "/ftl/showMessages_en.ftl"/>
