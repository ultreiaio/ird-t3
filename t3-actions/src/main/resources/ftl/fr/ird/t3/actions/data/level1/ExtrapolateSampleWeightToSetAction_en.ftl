<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header_en.ftl"/>

Begin date: ${configuration.beginDate}
End date:   ${configuration.endDate}

<#list oceans?values as ocean>
Selected ocean: ${ocean}
</#list>

<#list fleets?values as fleet>
Selected fleet: ${fleet}
</#list>

<#list sampleQualities?values as sampleQualitity>
Selected sample quality: ${sampleQualitity}
</#list>

<#list sampleTypes?values as sampleType>
Selected sample type: ${sampleType}
</#list>

Indicators
----------

Number of treated trips:                          ${action.nbTripsTreated}
Number of treated sets:                           ${action.nbTreatedSets}
Number of treated samples:                        ${action.nbSamplesTreated}
Number of untreated samples:                      ${action.nbSamplesNotTreated}
Number of fishes treated in samples:              ${action.nbTreatedFishesInSamples}
Number of fishses created in SetSpeciesFrequency: ${action.nbCreatedFishesInSetSpeciesFrequency}
<#include "/ftl/showMessages_en.ftl"/>
