<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header.ftl"/>

Date de début : ${configuration.beginDate}
Date de fin   : ${configuration.endDate}

<#list oceans?values as ocean>
Océan sélectionné : ${ocean}
</#list>

<#list fleets?values as fleet>
Flotte sélectionné : ${fleet}
</#list>

<#list sampleQualities?values as sampleQualitity>
Qualité d'échantillon sélectionné : ${sampleQualitity}
</#list>

<#list sampleTypes?values as sampleType>
Type d'échantillon sélectionné : ${sampleType}
</#list>

Indicateurs
-----------

Nombre de marées traitées                         : ${action.nbTripsTreated}
Nombre de calées traitées                         : ${action.nbTreatedSets}
Nombre d'échantillons traités                     : ${action.nbSamplesTreated}
Nombre d'échantillons non traités                 : ${action.nbSamplesNotTreated}
Nombre d'individus traités dans les échantillons  : ${action.nbTreatedFishesInSamples}

<#include "/ftl/showMessages.ftl"/>
