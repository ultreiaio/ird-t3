<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header.ftl"/>

Date de début : ${configuration.beginDate}
Date de fin   : ${configuration.endDate}

<#list vesselSimpleTypes?values as vesselSimpleType>
Type de navire sélectionné : ${vesselSimpleType}
</#list>

<#list fleets?values as fleet>
Flotte sélectionnée : ${fleet}
</#list>

Indicateurs
-----------

- Nombre de marées                         : ${action.nbTrips}
- Nombre de cuves                          : ${action.nbWell}
- Nombre de cuves avec plan                : ${action.nbWellWithPlan}
- Nombre de cuves sans plan                : ${action.nbWellWithNoPlan}
- Total du poids depuis les plans de cuve  : ${action.totalWellPlanWeight}
- Total du poids depuis les nouveaux plans : ${action.totalWellSetAllSpeciesWeight}

<#include "/ftl/showMessages.ftl"/>


