<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#assign output  = action.outputContext/>
<#assign headerSize  = output.maximumSizeForStratum/>
<#function tableFormat arg0, arg1, arg2>
  <#return statics['java.lang.String'].format("%1$-" + headerSize + "s | %2$-15s | %3$-30s |", arg0, arg1, arg2)>
</#function>
<#include "/ftl/header.ftl"/>

Date de début    : ${configuration.beginDate}
Date de fin      : ${configuration.endDate}
Période de temps : ${configuration.timeStep}

Traitement à appliquer aux calées avec échantillon :
<#if configuration.useAllSamplesOfStratum>
Corriger avec les échantillons de leur strate échantillon
<#else>
Corriger avec leur propre échantillon
</#if>

<#if configuration.useWeightCategoriesInStratum>
Utilisation des catégories de poids +10/-10 dans la stratification.
<#else>
Catégories de poids +10/-10 non utilisés dans la stratification.
</#if>

<#list oceans?values as ocean>
Océan sélectionné : ${ocean}
</#list>

<#list zoneTypes?values as zoneType>
Type de zone sélectionnée : ${zoneType}
</#list>

<#list zoneVersions?values as zoneVersion>
Version de la zone sélectionnée : ${zoneVersion}
</#list>

<#list catchFleets?values as catchFleet>
Flotte capture sélectionnée : ${catchFleet}
</#list>

<#list species?values as species>
Espèce sélectionnée : ${species}
</#list>

<#list sampleFleets?values as sampleFleet>
Flotte d'échantillon sélectionnée : ${sampleFleet}
</#list>

<#list sampleFlags?values as sampleFlag>
Pavillon d'échantillon sélectionnée : ${sampleFlag}
</#list>


Qualité des strates échantillons :

- Nombre minimum d'effectif dans les échantillons :
- pour banc libre : ${configuration.stratumMinimumSampleCountFreeSchoolType}
- pour banc objet : ${configuration.stratumMinimumSampleCountObjectSchoolType}

- Ratio minimum entre le poids de la strate capture et échantillon : ${configuration.stratumWeightRatio}

Indicateurs
-----------

- Nombre de strates                              : ${output.nbStratum}
- Nombre de strates corrigés                     : ${output.nbStrataFixed}
- Nombre total d'activités traitées              : ${output.totalCatchActivities}
- Nombre d'activités traitées (avec échantillon) : ${output.totalCatchActivitiesWithSample}
- Nombre d'activités traitées (sans échantillon) : ${output.totalCatchActivitiesWithoutSample}
- Poids total (toute espèce confondue)           : ${output.totalCatchWeight}
- Poids total (pour les espèces à corriger)      : ${output.totalCatchWeightForSpeciesFoFix}

${output.inputCatchStratumLog}

${output.outputCatchStratumLog}

<#assign allSubstitutionLevels = output.allSubstitutionLevels/>

Strates Echantillon (par niveau de substitution)
------------------------------------------------

Niveau 0   est utilisé pour indiquer les strates sans capture
Niveau 999 est utilisé pour indiquer que le dernier niveau de substitution a été atteint sans pour autant passer les tests de qualité

  ${tableFormat("Strate","Nb de calées","Nb de calées avec échantillon")}
<#list output.allSubstitutionLevels as substitutionLevel>
  <#assign stratums = output.getStratumResult(substitutionLevel)/>

- Niveau de substitution ${substitutionLevel} (${stratums?size} strate(s))
  <#list stratums as stratum>
  ${tableFormat(stratum.label,stratum.nbActivities,stratum.nbActivitiesWithSample)}
  </#list>
</#list>

<#include "/ftl/showMessages.ftl"/>
