<#--
 #%L
 T3 :: Actions
 %%
 Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#assign locale = action.locale/>
<#assign outputProvider = configuration.outputProvider/>
<#include "/ftl/header.ftl"/>

Pilote d'export de données      : ${outputProvider.libelle}

Opérations sélectionnées :
<#list configuration.operationIds as operationId>
<#assign operation = outputProvider.getOperation(operationId)/>
- ${operation.getLibelle(locale)}
</#list>

Date de début      : ${configuration.beginDate}
Date de fin        : ${configuration.endDate}
<#list oceans?values as ocean>
Océan sélectionné  : ${ocean}
</#list>
<#list fleets?values as fleet>
Flotte sélectionné : ${fleet}
</#list>

Résumé des opérations
---------------------

<#list configuration.operationIds as operationId>
<#assign operation = outputProvider.getOperation(operationId)/>
Opération ${operation.getLibelle(locale)} :
${action.getOperationSummary(operation)}

</#list>

<#include "/ftl/showMessages.ftl"/>


