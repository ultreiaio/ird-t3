#!/bin/sh

file=postgis-data.sql.zip

tempdir="tmp`date +%N`"

echo "will use temp directory $tempdir"

mkdir ${tempdir}

cd ${tempdir}

for i in $(ls /tmp | grep fr); do (cp -v /tmp/${i}/testExecute/*.sql .) ; done

zip -9 ${file} *.sql

cp ${file} ~/projets/forge/t3/t3-installer/src/main/assembly/dist/scripts/

cd ..

rm -rfv ${tempdir}

