# T3 Console

## Create a database
This will create the database on postgres server, then fill it.
```
sh create-db.sh dbName
```
or 
```
create-db.bat dbName
```

**Please see Notes section for more details.**

## Fill a database
This will just fill a database on postgres server.
 
**but the database must be empty and have a t3 schema initialized.**
```
sh fill-db.sh dbName
```
or 
```
fill-db.bat dbName
```

**Please see Notes section for more details.**

## Translate application
This will open a GUI to translate application.
```
sh translate.sh
```
or 
```
translate.bat
```

## Notes

For ```create-db``` or ```fill-db``` commands, the software will ask you three questions:

 * login to connect to database
 * password to connect to database
 * url pattern to use

Those values will be stored in a file named ```t3-installer.conf``` and reused at next launch.

If stored value is ok with you, just press enter.

Note also that the url pattern must ends with %s (which represents the db name you have given on command line).

Example on a first launch:
```
sh create-db.sh myT3Db
Listening for transport dt_socket at address: 8000
Starting T3 Console with arguments: [--create-db, myT3Db] at Fri May 18 10:40:10 CEST 2018 
Login [Not defined]: t3-admin
Password [Not defined]: pass
Url [jdbc:postgresql:%s]: 
INFO [main] (fr.ird.t3.console.T3DatabaseTool:95) createOrFill - 1/5 - Setup and connect to database...
```

Next launch:
```
sh create-db.sh myT3Db
Listening for transport dt_socket at address: 8000
Starting T3 Console with arguments: [--create-db, myT3Db] at Fri May 18 10:40:10 CEST 2018 
Login [t3-admin]:
Password [pass]:
Url [jdbc:postgresql:%s]: 
INFO [main] (fr.ird.t3.console.T3DatabaseTool:95) createOrFill - 1/5 - Setup and connect to database...
```
