package fr.ird.t3.console;

/*-
 * #%L
 * T3 :: Installer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.i18n.editor.model.Project;
import org.nuiton.i18n.editor.ui.ProjectUI;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import java.net.URL;
import java.util.Objects;

/**
 * Created by tchemit on 14/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3I18nTool {

    @SuppressWarnings("unused")
    public void start() throws Exception {
        SwingUtil.initNimbusLoookAndFeel();
        URL url = Objects.requireNonNull(getClass().getClassLoader().getResource("META-INF/t3-i18n-definition.properties"));
        Project project = new Project(url);
        ProjectUI ui = new ProjectUI(null, project);
        ui.setVisible(true);
    }
}
