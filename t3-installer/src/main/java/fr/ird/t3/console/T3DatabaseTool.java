package fr.ird.t3.console;

/*-
 * #%L
 * T3 :: Installer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.t3.T3SqlScriptsImporter;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3JdbcHelper;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaApplicationContextBuilder;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.installer.T3InstallerConfigOption;
import io.ultreia.java4all.config.ApplicationConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Tool around T3 database.
 * <p>
 * Created by tchemit on 13/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3DatabaseTool {

    private static final Log log = LogFactory.getLog(T3DatabaseTool.class);

    private final ApplicationConfig config;

    public T3DatabaseTool(ApplicationConfig config) {
        this.config = config;
    }

    @SuppressWarnings("unused")
    public void create(String databaseName) throws IOException, SQLException {
        createOrFill(databaseName, true);
    }

    @SuppressWarnings("unused")
    public void fill(String databaseName) throws IOException, SQLException {
        createOrFill(databaseName, false);
    }

    private void createOrFill(String databaseName, boolean create) throws IOException, SQLException {

        Objects.requireNonNull(databaseName);

        File scriptsDirectory = new File(new File("scripts").getAbsolutePath());
        Preconditions.checkArgument(scriptsDirectory.exists(), String.format("%s was not found!", scriptsDirectory));

        File ddlDirectory = new File(scriptsDirectory, "ddl");
        Preconditions.checkArgument(ddlDirectory.exists(), String.format("%s was not found!", ddlDirectory));

        File referentialScriptsDirectory = new File(scriptsDirectory, "referential");
        Preconditions.checkArgument(referentialScriptsDirectory.exists(), referentialScriptsDirectory + " was not found!");

        File postgisDataScript = new File(scriptsDirectory, "zones");
        Preconditions.checkArgument(postgisDataScript.exists(), postgisDataScript + " was not found!");

        File[] postgisDataFiles = Objects.requireNonNull(postgisDataScript.listFiles((dir, name) -> name.endsWith(".zip")));
        log.info("1/5 - Setup and connect to database...");

        T3TopiaApplicationContext applicationContext = setup(databaseName, create);

        try {
            log.info("2/5 - Create database schema...");
            T3JdbcHelper jdbcHelper = openDatabase(applicationContext);

            T3SqlScriptsImporter dllScriptsImporter = jdbcHelper.createScriptsImporter(ddlDirectory);
            log.info(String.format("3/5 - Loading ddl from %d scripts.", dllScriptsImporter.getScriptsFile().size()));
            dllScriptsImporter.importScripts();

            T3SqlScriptsImporter referentialScriptsImporter = jdbcHelper.createScriptsImporter(referentialScriptsDirectory);
            log.info(String.format("4/5 - Load referential from %d scripts.", referentialScriptsImporter.getScriptsFile().size()));
            referentialScriptsImporter.importScripts();

            File unzipDirectory = createUnzipDirectory();

            log.info("5/5 - Import postGis data from " + postgisDataFiles.length + " scripts.");
            for (File postgisDataFile : postgisDataFiles) {
                log.info(String.format(" o Unzip postGis data...(%s)", postgisDataFile));
                File unzipFile = unzipFile(unzipDirectory, postgisDataFile);
                log.info(String.format(" o Load postGis data... from %s script.", unzipFile));
                jdbcHelper.executeSql(unzipFile);
            }
        } finally {
            T3EntityHelper.releaseRootContext(applicationContext);
        }
    }

    private T3TopiaApplicationContext setup(String databaseName, boolean create) throws SQLException {
        DefaultI18nInitializer i18nInitializer = new DefaultI18nInitializer("t3-i18n");
        i18nInitializer.setMissingKeyReturnNull(true);
        I18n.init(i18nInitializer, Locale.getDefault());

        String login = Objects.requireNonNull(config.getOption(T3InstallerConfigOption.LOGIN.getKey()));
        String password = Objects.requireNonNull(config.getOption(T3InstallerConfigOption.PASSWORD.getKey()));
        String urlPattern = Objects.requireNonNull(config.getOption(T3InstallerConfigOption.URL.getKey()));

        String adminUrl = String.format(urlPattern, "postgres");
        try (Connection connection = DriverManager.getConnection(adminUrl, login, password)) {
            boolean databaseExist;
            log.info(String.format("Check if database %s exists.", databaseName));
            try (PreparedStatement preparedStatement = connection.prepareStatement(String.format("SELECT 1 AS result FROM pg_database WHERE datname='%s';", quoteIdentifier(databaseName)))) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    databaseExist = resultSet.next();
                }
            }
            log.info(String.format("Database %s exists? %b", databaseName, databaseExist));
            if (!create && !databaseExist) {
                throw new IllegalStateException(String.format("Database %sdoes not exist, please use a create command instead of fill", databaseName));
            }
            if (!databaseExist) {
                log.info(String.format("Create database %s", databaseName));
                try (PreparedStatement preparedStatement = connection.prepareStatement(String.format("CREATE DATABASE %s ENCODING 'UTF-8' OWNER = %s TEMPLATE template1;", quoteIdentifier(databaseName), quoteIdentifier(login)))) {
                    preparedStatement.execute();
                }
            }
        }
        JdbcConfiguration jdbcConfiguration = new JdbcConfiguration();
        jdbcConfiguration.setUrl(String.format(urlPattern, databaseName));
        jdbcConfiguration.setLogin(login);
        jdbcConfiguration.setPassword(password);

        return T3TopiaApplicationContextBuilder.forPg(jdbcConfiguration).addMigration().checkConnexion().build();
    }

    private T3JdbcHelper openDatabase(T3TopiaApplicationContext applicationContext) {
        T3JdbcHelper t3JdbcHelper = applicationContext.newJdbcHelper();
        try {
            boolean schemaFound = !applicationContext.isSchemaEmpty();
            if (!schemaFound) {
                log.info("Will create schema for db (no schema found).");
                applicationContext.createSchema();
            }
            return t3JdbcHelper;
        } catch (Exception e) {
            throw new IllegalStateException("could not start db", e);
        }
    }

    private File unzipFile(File unzipDirectory, File script) throws IOException {
        List<String>[] lists = ZipUtil.scanAndExplodeZip(script, unzipDirectory, null);
        ZipUtil.uncompress(script, unzipDirectory);
        File result = new File(unzipDirectory, lists[0].get(0));
        result.deleteOnExit();
        return result;
    }

    private File createUnzipDirectory() {
        String tmpPath = System.getProperty("java.io.tmpdir");
        File tmpDir = new File(tmpPath);
        File unzipDirectory = new File(tmpDir, "postgis-data" + "_" + System.nanoTime());
        unzipDirectory.deleteOnExit();
        FileUtil.createDirectoryIfNecessary(unzipDirectory);
        return unzipDirectory;
    }

    private String quoteIdentifier(String dbName) {
        if (dbName.contains("-")) {
            dbName = "\"" + dbName + "\"";
        }
        return dbName;
    }
}
