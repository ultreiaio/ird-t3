package fr.ird.t3.console;

/*-
 * #%L
 * T3 :: Installer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.installer.T3InstallerConfig;
import io.ultreia.java4all.config.ApplicationConfigInit;
import io.ultreia.java4all.config.ApplicationConfigScope;
import io.ultreia.java4all.config.ArgumentsParserException;

import java.io.Console;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * Created by tchemit on 13/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class T3Console {

    public static void main(String... args) throws Exception {
        System.out.printf("Starting T3 Console with arguments: %s at %s %n", Arrays.toString(args), new Date());

        if (args.length == 0) {
            throw new IllegalArgumentException("Need at least one argument. Try with --help to get details.");
        }

        T3InstallerConfig installerConfig;

        String action = args[0];

        if (action.contains("db")) {

            if (args.length != 2) {
                throw new IllegalArgumentException(String.format("Need one arguments for command %s. Try with --help to get details.", action));
            }

            installerConfig = new T3InstallerConfig(ApplicationConfigInit.forAllScopesWithout(ApplicationConfigScope.HOME, ApplicationConfigScope.SYSTEM));
            installerConfig.get().parse(args);

            Console console = System.console();

            String login = readValue(console, "Login", installerConfig.getLogin());
            if (login != null) {
                installerConfig.setLogin(login);
            }
            String password = readValue(console, "Password", installerConfig.getPassword());
            if (password != null) {
                installerConfig.setPassword(password);
            }
            String url = readValue(console, "Url", installerConfig.getUrl());
            if (url != null) {
                installerConfig.setUrl(url);
            }
            installerConfig.get().saveForCurrent();
        } else {

            installerConfig = new T3InstallerConfig();
            installerConfig.get().parse(args);

        }
        installerConfig.get().doAllAction();
    }

    private static String readValue(Console console, String fieldName, String fieldValue) {
        String result = null;
        while (result == null) {
            console.printf("%s [%s]: ", fieldName, fieldValue == null ? "Not defined" : fieldValue);
            result = console.readLine().trim();
            if (fieldValue == null) {
                if (result.isEmpty()) {
                    result = null;
                }
            } else {
                if (result.isEmpty()) {
                    result = fieldValue;
                }
            }
        }

        return Objects.equals(fieldValue, result) ? null : result;
    }

    public void help() throws ArgumentsParserException {
        T3InstallerConfig config = new T3InstallerConfig();

        config.get().parse();
        System.out.println(config.getConfigurationDescription());
    }
}
