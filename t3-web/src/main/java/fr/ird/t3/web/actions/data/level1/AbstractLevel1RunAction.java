/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import com.google.common.collect.Multimap;
import fr.ird.t3.actions.data.level1.AbstractLevel1Action;
import fr.ird.t3.actions.data.level1.Level1Configuration;
import fr.ird.t3.actions.data.level1.Level1Step;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractRunAction;
import org.apache.commons.collections.MapUtils;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Abstract run action for all level1 actions.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractLevel1RunAction<A extends AbstractLevel1Action> extends AbstractRunAction<Level1Configuration, A> {

    private static final long serialVersionUID = 1L;

    private final Level1Step currentStep;
    @InjectDecoratedBeans(beanType = SampleQuality.class, filterById = true, pathIds = "sampleQualityIds")
    private Map<String, String> sampleQualities;
    @InjectDecoratedBeans(beanType = SampleType.class, filterById = true)
    private Map<String, String> sampleTypes;
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> fleets;
    @InjectDecoratedBeans(beanType = Ocean.class, filterById = true)
    private Map<String, String> oceans;
    private Map<String, String> useRfMinus10AndRfPlus10OrNot;

    AbstractLevel1RunAction(Class<A> actionType, Level1Step currentStep) {
        super(actionType);
        this.currentStep = currentStep;
    }

    @Override
    protected void executeAction(A action) throws Exception {

        // when beginning a step, remove from executed one all the step with higher ranks...

        Level1Configuration configuration = getConfiguration();
        Set<Level1Step> executedSteps = configuration.getExecutedSteps();
        int ordinal = currentStep.ordinal();

        executedSteps.removeIf(step -> step.ordinal() >= ordinal);

        super.executeAction(action);

        // if everything is ok, then mark this step as done
        configuration.addExecutedStep(currentStep);
    }

    public Map<String, String> getSampleQualities() {
        return sampleQualities;
    }

    public Map<String, String> getSampleTypes() {
        return sampleTypes;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public int getMatchingTripCount() {
        Map<String, Collection<String>> tripIds = getTripIds().asMap();
        return MapUtils.isEmpty(tripIds) ? 0 : tripIds.size();
    }

    public int getMatchingSampleCount() {
        Map<String, Collection<String>> tripIds = getTripIds().asMap();

        if (MapUtils.isEmpty(tripIds)) {
            return 0;
        }
        int nbSamples = 0;
        for (Collection<String> sampleIds : tripIds.values()) {
            nbSamples += sampleIds.size();
        }
        return nbSamples;
    }

    public int getNotMatchingSampleCount() {
        return getConfiguration().getNotTreatedSampleReason().size();
    }

    public Multimap<String, String> getTripIds() {
        return getConfiguration().getSampleIdsByTripId();
    }

    public Map<String, String> getUseRfMinus10AndRfPlus10OrNot() {
        return useRfMinus10AndRfPlus10OrNot;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(A action, Exception error, Date startDate, Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action, error, startDate, endDate);
        map.put("oceans", oceans);
        map.put("sampleQualities", sampleQualities);
        map.put("sampleTypes", sampleTypes);
        map.put("fleets", fleets);
        return map;
    }

    @Override
    public void prepare() throws Exception {
        useRfMinus10AndRfPlus10OrNot = createUseRfMinus10AndRfPlus10OrNot();
        super.prepare();
    }

}
