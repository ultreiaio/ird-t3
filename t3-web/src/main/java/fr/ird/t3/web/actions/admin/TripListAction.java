/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import fr.ird.t3.web.actions.T3ActionSupport;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * To obtain the list of trips of the database.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class TripListAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    @InjectDAO(entityType = Trip.class)
    private transient TripTopiaDao tripDAO;

    @InjectFromDAO(entityType = Trip.class, method = "findAllYearsUsedInTrip")
    private Collection<Integer> allYears;

    @InjectFromDAO(entityType = Country.class, method = "findAllFleetUsedInTrip")
    private Collection<Country> allFleets;

    @InjectFromDAO(entityType = Country.class, method = "findAllFlagUsedInTrip")
    private Collection<Country> allFlags;

    @InjectFromDAO(entityType = Vessel.class, method = "findAllUsedInTrip")
    private Collection<Vessel> allVessels;

    private Map<String, String> years;

    private Map<String, String> oceans;

    private Map<String, String> fleets;

    private Map<String, String> flags;

    private Map<String, String> vessels;

    private TripListModel tripListModel;

    private boolean back;

    public Map<String, String> getYears() {
        return years;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    public Map<String, String> getFlags() {
        return flags;
    }

    public Map<String, String> getVessels() {
        return vessels;
    }

    public TripListModel getTripListModel() {
        if (tripListModel == null) {
            tripListModel = getT3Session().getTripListModel();

            if (tripListModel == null) {
                tripListModel = new TripListModel();
            }
        }
        return tripListModel;
    }

    @Override
    public String input() throws Exception {

        injectOnly(InjectDAO.class, InjectFromDAO.class);

        if (getT3Session().getTripListModel() == null) {
            // must load it
            tripListModel = loadTripListModel(tripDAO);
        } else {
            // use existing one
            tripListModel = getT3Session().getTripListModel();
        }

        years = new LinkedHashMap<>();
        for (Integer year : allYears) {
            years.put(year + "", year + "");
        }
        oceans = sortAndDecorate(tripListModel.getOceans());
        fleets = sortAndDecorate(allFleets);
        flags = sortAndDecorate(allFlags);
        vessels = sortAndDecorate(allVessels);
        return INPUT;
    }

    public String loadGrid() {
        tripListModel = getT3Session().getTripListModel();
        return INPUT;
    }

    public String backToTrip() throws Exception {
        back = true;
        return input();
    }

    public boolean isBack() {
        return back;
    }
}
