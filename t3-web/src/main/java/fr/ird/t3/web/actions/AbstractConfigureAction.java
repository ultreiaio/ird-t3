/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.actions.T3ActionContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ObjectUtil;

/**
 * Abstract run action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractConfigureAction<C extends T3ActionConfiguration> extends T3ActionSupport implements Preparable {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(AbstractConfigureAction.class);
    private final Class<C> configurationType;
    protected transient T3ActionContext<C> t3ActionContext;
    protected transient C configuration;
    /** To confirm the configuration. */
    protected boolean confirm;

    public AbstractConfigureAction(Class<C> configurationType) {
        this.configurationType = configurationType;
    }

    public final T3ActionContext<C> getT3ActionContext() {
        return t3ActionContext;
    }

    public final C getConfiguration() {
        if (configuration == null) {
            // try in session
            configuration = getConfigurationFromSession();
            if (configuration == null) {
                // really a new configuration
                configuration = ObjectUtil.newInstance(configurationType);
            }
        }
        return configuration;
    }

    @SuppressWarnings("WeakerAccess")
    public final boolean isConfigurationInSession() {
        return getConfigurationFromSession() != null;
    }

    public boolean isConfirm() {
        return confirm;
    }

    protected void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    protected final void prepareActionContext() {
        t3ActionContext = getServiceFactory().newT3ActionContext(getConfiguration(), getServiceContext());
        log.info("Created action context " + t3ActionContext);
        getT3Session().setActionContext(t3ActionContext);
    }

    @SuppressWarnings("WeakerAccess")
    protected final C getConfigurationFromSession() {
        return getActionConfiguration(configurationType);
    }

    @SuppressWarnings("unused")
    protected final boolean isConfigurationValid() {
        return configuration != null && !hasFieldErrors();
    }

    protected final void storeActionConfiguration(C configuration) {
        getT3Session().setActionConfiguration(configuration);
    }

    protected final void removeConfigurationFromSession() {
        getT3Session().removeActionConfiguration(configurationType);
    }

    protected final void removeActionContextFromSession() {
        getT3Session().removeActionContext();
    }
}
