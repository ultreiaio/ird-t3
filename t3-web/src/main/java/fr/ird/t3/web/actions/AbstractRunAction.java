/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Version;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Abstract run action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractRunAction<C extends T3ActionConfiguration, A extends T3Action<C>> extends T3ActionSupport implements Preparable {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AbstractRunAction.class);
    private final Class<A> actionType;
    protected transient T3ActionContext<C> t3ActionContext;
    private ActionContext ac;

    protected AbstractRunAction(Class<A> actionType) {
        this.actionType = actionType;
    }

    public final Class<A> getActionType() {
        return actionType;
    }

    public final T3ActionContext<C> getT3ActionContext() {
        return t3ActionContext;
    }

    public final C getConfiguration() {
        return t3ActionContext.getConfiguration();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepare() throws Exception {

        // get action context from session
        t3ActionContext = (T3ActionContext<C>) getT3Session().getActionContext();

        // this is necessary since we wants after use it... in the thread
        ac = ActionContext.getContext();

        // load configuration
        getConfiguration();

        injectOnly(InjectDecoratedBeans.class);
    }

    @Override
    public final String execute() throws Exception {

        // while we are in a new thread, we wants to get back the action context
        // from the main thread (this is a bug of the interceptor execAndWait
        // I think...)
        ActionContext.setContext(ac);

        // push inside it the new service context
        t3ActionContext.setServiceContext(getServiceContext());

        t3ActionContext.clearMessages();

        // trick for exec and wait executor : a unique transaction is required
        // to perform the action but each http request will offers a new
        // transaction, so we must get a new fresh one once for all and keep it
        // until action is done (or has failed)
        try (T3TopiaPersistenceContext tx = getServiceContext().getApplicationContext().newPersistenceContext()) {

            t3ActionContext.setTransaction(tx);
            A action = getServiceFactory().newT3Action(getActionType(), t3ActionContext);
            executeAction(action);
        }

        return SUCCESS;
    }

    protected void executeAction(A action) throws Exception {
        Exception error = null;
        Date startDate = new Date();
        try {
            action.run();
        } catch (Exception e) {
            error = e;
            log.error("An error occurs while action", e);
        } finally {
            Date endDate = new Date();
            try {
                String resume = buildActionResume(action, error, startDate, endDate);
                t3ActionContext.setResume(resume);
                addContentInFile(getT3Session(), "\n" + LOG_LINE + "\n" + resume);
            } finally {
                if (error != null) {
                    throw error;
                }
            }
        }
    }

    @SuppressWarnings("unused")
    public String getTotalTime() {
        return StringUtil.convertTime(t3ActionContext.getTotalTime());
    }

    @SuppressWarnings("unused")
    protected <O> O getResult(String name, Class<O> type) {
        return t3ActionContext.getResult(name, type);
    }

    @SuppressWarnings("unused")
    protected <O> List<O> getResultAsList(String name, Class<O> type) {
        return t3ActionContext.getResultAsList(name, type);
    }

    @SuppressWarnings("unused")
    protected <K, V> Map<K, V> getResultAsMap(String name) {
        return t3ActionContext.getResultAsMap(name);
    }

    private String buildActionResume(A action, Exception error, Date startDate, Date endDate) throws Exception {
        String templateName = action.getRenderTemplateName();
        Map<String, Object> parameters = prepareResumeParameters(action, error, startDate, endDate);
        String resume = getFreeMarkerService().renderTemplate(templateName, getLocale(), parameters);
        resume = resume.trim();
        log.debug(String.format("Action resume =\n%s", resume));
        return resume;
    }

    protected Map<String, Object> prepareResumeParameters(A action, Exception error, Date startDate, Date endDate) {
        Map<String, Object> parameters = new TreeMap<>();
        parameters.put("startDate", startDate);
        parameters.put("endDate", endDate);
        parameters.put("action", action);
        parameters.put("error", error);
        parameters.put("configuration", action.getConfiguration());
        parameters.put("showError", true);
        parameters.put("showWarning", true);
        parameters.put("showInfo", false);
        parameters.put("statics", new BeansWrapperBuilder(new Version("2.3.0")).build().getStaticModels());

        return parameters;
    }

}
