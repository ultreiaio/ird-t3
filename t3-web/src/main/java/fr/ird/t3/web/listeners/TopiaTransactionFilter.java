package fr.ird.t3.web.listeners;

/*-
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Closeable;
import java.io.IOException;
import java.util.function.Supplier;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

/**
 * Created by tchemit on 04/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class TopiaTransactionFilter<PersistenceContext extends TopiaPersistenceContext> implements Filter {

    public static final String TOPIA_TRANSACTION_REQUEST_ATTRIBUTE = "topiaTransaction";

    /**
     * Name of the request attribute where to push the transaction.
     * <p>
     * By default will use value of
     * {@link #TOPIA_TRANSACTION_REQUEST_ATTRIBUTE}.
     *
     * @since 1.10
     */
    private String requestAttributeName = TOPIA_TRANSACTION_REQUEST_ATTRIBUTE;

    public void setRequestAttributeName(String requestAttributeName) {
        this.requestAttributeName = requestAttributeName;
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {


    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        // creates a proxy of a lazy transaction

        try (TopiaPersistenceContextSupplier transactionSupplier = new TopiaPersistenceContextSupplier(request)) {

            // push it in request as an attribute
            request.setAttribute(requestAttributeName, transactionSupplier);

            // chain to next filter
            chain.doFilter(request, response);

        }
    }

    protected abstract PersistenceContext newPersistenceContext(ServletRequest request);

    public class TopiaPersistenceContextSupplier implements Supplier<PersistenceContext>, Closeable {

        private final ServletRequest request;
        PersistenceContext persistenceContext;

        public TopiaPersistenceContextSupplier(ServletRequest request) {
            this.request = request;
        }

        @Override
        public PersistenceContext get() {
            if (persistenceContext == null) {
                persistenceContext = newPersistenceContext(request);
            }
            return persistenceContext;
        }


        @Override
        public void close() {
            if (persistenceContext != null) {
                persistenceContext.close();
            }
        }
    }
}
