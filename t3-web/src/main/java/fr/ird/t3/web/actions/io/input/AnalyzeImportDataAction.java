/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.input;

import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceAction;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceConfiguration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.web.actions.AbstractRunAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Analyze the given input source.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AnalyzeImportDataAction extends AbstractRunAction<AnalyzeInputSourceConfiguration, AnalyzeInputSourceAction> {

    private static final long serialVersionUID = 1L;

    private int nbUnsafeTrips;
    private int nbSafeTrips;
    private int nbTripsToReplace;
    private boolean needReplace;
    private boolean valid;

    /** List of all known input providers. */
    private List<T3InputProvider> inputProviders;

    public AnalyzeImportDataAction() {
        super(AnalyzeInputSourceAction.class);
    }

    @Override
    public void prepare() throws Exception {
        getT3Session().removeTripListModel();
        super.prepare();
        inputProviders = Collections.singletonList(getConfiguration().getInputProvider());
    }

    public String prepareResult() {
        T3ActionContext<AnalyzeInputSourceConfiguration> context = getT3ActionContext();
        Set<Trip> safeTrips = context.getResultAsSet(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, Trip.class);
        nbSafeTrips = safeTrips.size();
        Set<Trip> unsafeTrips = context.getResultAsSet(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, Trip.class);
        nbUnsafeTrips = unsafeTrips.size();
        boolean canImport = CollectionUtils.isEmpty(unsafeTrips) && CollectionUtils.isNotEmpty(safeTrips);
        // action is valid, only if there is some safe trips and no unsafe trip
        valid = canImport;
        Map<Trip, Trip> tripsToReplace;
        if (canImport) {
            // find out if there is some existing trip to re-import
            tripsToReplace = context.getResultAsMap(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE);
        } else {
            // no trip to replace
            tripsToReplace = Collections.emptyMap();
        }
        nbTripsToReplace = tripsToReplace.size();
        needReplace = MapUtils.isNotEmpty(tripsToReplace);
        return INPUT;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(AnalyzeInputSourceAction action, Exception error, Date startDate, Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action, error, startDate, endDate);
        T3ActionContext<AnalyzeInputSourceConfiguration> actionContext = getT3ActionContext();
        Set<Trip> safeTrips = actionContext.getResultAsSet(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, Trip.class);
        map.put(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, safeTrips);
        Set<Trip> unsafeTrips = actionContext.getResultAsSet(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, Trip.class);
        map.put(AnalyzeInputSourceAction.RESULT_UNSAFE_TRIPS, unsafeTrips);
        Map<Trip, Trip> tripsToReplace = actionContext.getResultAsMap(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE);
        map.put(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE, tripsToReplace);
        map.put("tripDecorator", getDecorator(Trip.class));
        map.put("tripDecorator2", getDecorator(Trip.class, DecoratorService.WITH_ID));
        return map;
    }

    @SuppressWarnings("unused")
    public List<T3InputProvider> getInputProviders() {
        return inputProviders;
    }

    @SuppressWarnings("unused")
    public int getNbUnsafeTrips() {
        return nbUnsafeTrips;
    }

    @SuppressWarnings("unused")
    public int getNbSafeTrips() {
        return nbSafeTrips;
    }

    @SuppressWarnings("unused")
    public int getNbTripsToReplace() {
        return nbTripsToReplace;
    }

    @SuppressWarnings("unused")
    public boolean isValid() {
        return valid;
    }

    @SuppressWarnings("unused")
    public boolean isNeedReplace() {
        return needReplace;
    }
}
