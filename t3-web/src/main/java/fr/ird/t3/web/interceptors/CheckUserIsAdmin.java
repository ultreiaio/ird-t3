/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.interceptors;

import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.ActionInvocation;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.web.T3Session;

/**
 * To check if logged user is admin.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class CheckUserIsAdmin extends AbstractCheckInterceptor {

    private static final long serialVersionUID = 1L;

    @Override
    protected boolean doCheck(ActionInvocation invocation) {
        T3Session t3Session =
                T3Session.getT3Session(invocation.getInvocationContext());

        T3User t3User = t3Session.getUser();
        Preconditions.checkNotNull(t3User, "No user found is session");

        return t3User.isAdmin();
    }

}
