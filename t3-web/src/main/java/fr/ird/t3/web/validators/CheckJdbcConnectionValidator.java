/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.validators;

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.user.JdbcConfiguration;

import java.nio.charset.StandardCharsets;

import static org.nuiton.i18n.I18n.t;

/**
 * Check the incoming jdbc configuration connection.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class CheckJdbcConnectionValidator extends T3BaseFieldValidatorSupport {

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        if (!getValidatorContext().hasFieldErrors()) {

            String fieldName = getFieldName();
            JdbcConfiguration db = (JdbcConfiguration) getFieldValue(fieldName, object);

            try {
                T3EntityHelper.checkJDBCConnection(db);
            } catch (Exception e) {
                // can not connect to database
                String errorMessage = new String(e.getMessage().getBytes(), StandardCharsets.UTF_8);
                addFieldError("database.url", t("t3.error.invalid.jdbc.connexion", errorMessage));
            }
        }
    }

    @Override
    public String getValidatorType() {
        return "jdbcConnection";
    }
}
