/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import fr.ird.t3.actions.data.level1.ExtrapolateSampleCountedAndMeasuredAction;
import fr.ird.t3.actions.data.level1.Level1Step;
import fr.ird.t3.entities.reference.Species;

import java.util.Date;
import java.util.Map;

/**
 * Run level 1 action {@link ExtrapolateSampleCountedAndMeasuredAction}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ExtrapolateSampleCountedAndMeasuredRunAction extends AbstractLevel1RunAction<ExtrapolateSampleCountedAndMeasuredAction> {

    private static final long serialVersionUID = 1L;

    public ExtrapolateSampleCountedAndMeasuredRunAction() {
        super(ExtrapolateSampleCountedAndMeasuredAction.class, Level1Step.EXTRAPOLATE_SAMPLE_COUNTED_AND_MEASURED);
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(ExtrapolateSampleCountedAndMeasuredAction action, Exception error, Date startDate, Date endDate) {
        Map<String, Object> parameters = super.prepareResumeParameters(action, error, startDate, endDate);
        parameters.put("speciesDecorator", getDecorator(Species.class));
        return parameters;
    }

}
