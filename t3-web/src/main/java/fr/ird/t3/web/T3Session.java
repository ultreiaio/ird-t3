/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import com.opensymphony.xwork2.ActionContext;
import fr.ird.t3.T3Config;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaApplicationContextBuilder;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.web.actions.T3ActionSupport;
import fr.ird.t3.web.actions.admin.TripListModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpSession;
import java.io.Closeable;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * The session object of T3 to put in servlet session
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3Session implements Closeable, Serializable {

    /** Current action context. */
    public static final String PROPERTY_ACTION_CONTEXT = "actionContext";
    /** Key to set User connected in this session. */
    private static final String PROPERTY_USER = "user";
    /** Prefix Key to set Current action configuration in this session. */
    private static final String PROPERTY_ACTION_CONFIGURATION = "actionConfiguration";
    /** Key to set the Date when user starts this session. */
    private static final String PROPERTY_START_DATE = "startDate";
    /** Key to set User log file in this session. */
    private static final String PROPERTY_USER_LOG_FILE = "userLogFile";
    /** Key to set trip list model each time coming on trip list screen. */
    private static final String PROPERTY_TRIP_LIST_MODEL = "tripListModel";
    /**
     * User file log pattern : use the login + day of year and year, in that
     * way one file maximum per day.
     */
    private static final String FILE_LOG_PATTERN = "%1$s-%2$td_%2$tm_%2$tY.log";
    /** Logger. */
    private static final Log log = LogFactory.getLog(T3Session.class);
    private static final long serialVersionUID = 1L;
    /** Key used to store this T3 session in application session */
    private static final String SESSION_PARAMETER_T3_SESSION = "t3Session";
    /** Key to set root context of t3 db selected by user (if any). */
    private static final String PROPERTY_ROOT_CONTEXT = "rootContext";
    /** To store all properties in this session. */
    private transient Map<String, Object> store;

    protected T3Session() {
    }

    /**
     * Obtain the user T3 session.
     * <p/>
     * If not found in application session, then will instantiate it and push it in it.
     * <p/>
     * At the creation time the session, it will also set his T3 factory.
     *
     * @param actionContext where to find user session
     * @return the user T3 session (never null)
     */
    public static T3Session getT3Session(ActionContext actionContext) {
        Map<String, Object> session = actionContext.getSession();
        T3Session t3Session = (T3Session) session.get(SESSION_PARAMETER_T3_SESSION);
        if (t3Session == null) {
            // let's create it
            session.put(SESSION_PARAMETER_T3_SESSION, t3Session = new T3Session());
        }
        return t3Session;
    }

    public static T3Session getT3Session(HttpSession session) {
        T3Session t3Session = (T3Session) session.getAttribute(T3Session.SESSION_PARAMETER_T3_SESSION);
        if (t3Session == null) {
            // let's create it
            session.setAttribute(SESSION_PARAMETER_T3_SESSION, t3Session = new T3Session());
        }
        return t3Session;
    }

    public static void removeT3Session(ActionContext actionContext) {
        Map<String, Object> session = actionContext.getSession();
        session.remove(T3Session.SESSION_PARAMETER_T3_SESSION);
    }

    /**
     * Tests if user is loggued (means the {@link #getUser} is not null).
     *
     * @return {@code true} if user is loggued, {@code false} otherwise
     */
    public boolean isUserInSession() {
        return getUser() != null;
    }

    /**
     * Tests if user has selected a T3 database (meansthe
     * {@link #getT3DatabaseUrl()} is not null).
     *
     * @return {@code true} if user has selected a T3 database,
     * {@code false} otherwise
     */
    public boolean isT3DatabaseSelected() {
        return getT3DatabaseUrl() != null;
    }

    /**
     * Gets the informations of user as soon as the user is loggued.
     *
     * @return the informations of loggued user or {@code null} if not in session
     */
    public T3User getUser() {
        return get(PROPERTY_USER, T3User.class);
    }

    /**
     * Sets in this session the loggued user.
     *
     * @param t3User the user loggued to use in this session
     */
    public void setUser(T3User t3User) {
        set(PROPERTY_USER, t3User);
    }

    /**
     * Gets the t3 database url selected by user.
     *
     * @return the t3 database url selected by user or {@code null} if not in session
     */
    @SuppressWarnings("WeakerAccess")
    public String getT3DatabaseUrl() {
        T3TopiaApplicationContext rootContext = getRootContext();
        String result = null;
        if (rootContext != null) {
            result = rootContext.getConfiguration().getJdbcConnectionUrl();
        }
        return result;
    }

    /**
     * Gets the configuration given his type.
     *
     * @param configurationClass the type of configuration to set in this session
     * @param <C>                the type of configuration
     * @return the configuration or {@code null} if not in session
     */
    public <C> C getActionConfiguration(Class<C> configurationClass) {
        String key = getActionConfigurationKey(configurationClass);
        return get(key, configurationClass);
    }

    /**
     * Sets in this session the given action configuration.
     *
     * @param actionConfiguration the action configuration to set in this session
     * @param <C>                 type of configuration
     */
    public <C> void setActionConfiguration(C actionConfiguration) {
        Objects.requireNonNull(actionConfiguration, "action configuration can not be null");
        String key = getActionConfigurationKey(actionConfiguration.getClass());
        set(key, actionConfiguration);
    }

    /**
     * Remove from this session the configuration given his type and returns it.
     *
     * @param configurationClass the type of configuration to remove from session
     * @param <C>                the type of configuration to remove from session
     * @return the configuration removed from session, or {@code null} if not
     * found in session
     */
    @SuppressWarnings("UnusedReturnValue")
    public <C> C removeActionConfiguration(Class<C> configurationClass) {
        String key = getActionConfigurationKey(configurationClass);
        Object remove = remove(key);
        return configurationClass.cast(remove);
    }

    /**
     * Gets the current action context useable.
     *
     * @return the current action context useable
     */
    public T3ActionContext<?> getActionContext() {
        return get(PROPERTY_ACTION_CONTEXT, T3ActionContext.class);
    }

    /**
     * Sets the given action context in this session.
     *
     * @param actionContext the action context to set in this session
     */
    public void setActionContext(T3ActionContext<?> actionContext) {
        set(PROPERTY_ACTION_CONTEXT, actionContext);
    }

    /** Remove from this session the current action context. */
    public void removeActionContext() {
        if (getActionContext() != null) {
            // remove it
            remove(PROPERTY_ACTION_CONTEXT);
        }
    }

    /**
     * Gets the user last session start date.
     *
     * @return the user last session start date, or {@code null} if user was
     * never loggued whil this session
     */
    public Date getStartDate() {
        return get(PROPERTY_START_DATE, Date.class);
    }

    /**
     * Sets the given session start date in this session.
     *
     * @param startDate the start session date to set in this session
     */
    public void setStartDate(Date startDate) {
        set(PROPERTY_START_DATE, startDate);
    }

    public TripListModel getTripListModel() {
        return get(PROPERTY_TRIP_LIST_MODEL, TripListModel.class);
    }

    public void setTripListModel(TripListModel tripListModel) {
        set(PROPERTY_TRIP_LIST_MODEL, tripListModel);
    }

    public void removeTripListModel() {
        remove(PROPERTY_TRIP_LIST_MODEL);
    }

    public T3TopiaApplicationContext getRootContext() {
        return get(PROPERTY_ROOT_CONTEXT, T3TopiaApplicationContext.class);
    }

    /**
     * Gets the user log file from the server.
     * <p/>
     * This file will be cached in this session the first time required.
     *
     * @return the user log file
     */
    public File getUserLogFile() {
        File result = get(PROPERTY_USER_LOG_FILE, File.class);
        if (result == null) {
            T3Config configuration = T3ActionSupport.getApplicationConfig();
            File userLogDirectory = configuration.getUserLogDirectory();
            String filename = String.format(FILE_LOG_PATTERN, getUser().getLogin(), getStartDate());
            result = new File(userLogDirectory, filename);
            log.info(String.format("User log file %s", result.getAbsolutePath()));
            set(PROPERTY_USER_LOG_FILE, result);
        }
        return result;
    }

    /**
     * Remove form this session, the object from his given key and returns it.
     *
     * @param key the key of object to remove from this session
     * @return the removed object from session, or {@code null} if not found
     */
    public Object remove(String key) {
        Object remove = getStore().remove(key);
        log.debug(String.format("Remove from user session data [%s] = %s", key, remove));
        return remove;
    }

    /**
     * Tests if for a given key, there is an associated object in this session.
     *
     * @param key the key to test in this session
     * @return {@code true} if an object was found in this session,
     * {@code false} otherwise
     */
    public boolean contains(String key) {
        return getStore().containsKey(key);
    }

    /**
     * Initialize the t3 database configuration from the given connection configuration.
     * <p/>
     * The given configuration only give the url, login and pasword to use.
     * Complete configuration (for ToPIA) will be loaded internaly and match
     * only a postgresql db.
     *
     * @param jdbcConfiguration the connection configuration to use
     */
    public void initDatabaseConfiguration(JdbcConfiguration jdbcConfiguration) {
        // close any previous db
        releaseDatabase();
        // creates a new topia root context from configuration
        T3TopiaApplicationContext rootContext = T3TopiaApplicationContextBuilder.forPg(jdbcConfiguration).addMigration().build();
        // store it in session
        set(PROPERTY_ROOT_CONTEXT, rootContext);
        log.info(String.format("User database initialized at %s", jdbcConfiguration.getUrl()));
    }

    private void releaseDatabase() {
        T3TopiaApplicationContext rootContext = getRootContext();
        if (rootContext != null) {
            remove(PROPERTY_ROOT_CONTEXT);
            T3EntityHelper.releaseRootContext(rootContext);
        }
    }

    private Map<String, Object> getStore() {
        if (store == null) {
            store = new TreeMap<>();
        }
        return store;
    }

    /**
     * Gets from this session an object given his key and his type.
     *
     * @param key  the key of object to obtain from this session
     * @param type the type of object to obtain from this session
     * @param <T>  the type of object to obtain from this session
     * @return the object found in this session, or {@code null} if not found
     */
    protected <T> T get(String key, Class<T> type) {
        Object o = getStore().get(key);
        if (o != null && !type.isInstance(o)) {
            throw new ClassCastException(
                    String.format("parameter %s should be of type %s but was %s", key, type.getName(), o.getClass().getName()));
        }
        return type.cast(o);
    }

    /**
     * Gets the action configuration key used to store configuration in session given his type.
     *
     * @param configurationClass the type of configuration
     * @param <C>                the type of configuration
     * @return the key used to store a configuration given his type
     */
    private <C> String getActionConfigurationKey(Class<C> configurationClass) {
        return PROPERTY_ACTION_CONFIGURATION + "#" + configurationClass.getSimpleName();
    }

    /**
     * Sets in this session the given object using the given key.
     *
     * @param key   the key where to store the object in this session
     * @param value the object to store in this session
     */
    protected void set(String key, Object value) {
        getStore().put(key, value);
        log.debug(String.format("Set in user session data [%s] = %s", key, value));
    }

    /**
     * Release any resources contained in the user session.
     * <p/>
     * Will also close any service (like the databaseService which contains connexions to db).
     *
     * @since 1.1
     */
    @Override
    public void close() {
        log.info(String.format("Close user session for [%s]", getUser().getLogin()));
        try {
            releaseDatabase();
        } finally {
            Set<String> keys = new HashSet<>(getStore().keySet());
            for (String key : keys) {
                remove(key);
            }
        }
    }
}
