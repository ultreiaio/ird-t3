/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.nuiton.decorator.Decorator;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * To obtain statistics of the selected T3 database.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3DatabaseDetailAction extends T3ActionSupport {

    private static final long serialVersionUID = -8408617621456216934L;
    /** All oceans used in any trip. */
    protected List<Ocean> oceans;
    @InjectDAO(entityType = Trip.class)
    private transient TripTopiaDao tripDAO;
    /** All trips of the database group by their owing ocean. */
    private transient Multimap<Ocean, Trip> tripsByOcean;
    /** Count of trips in database. */
    private long nbTrips;
    /** Count of activities in database. */
    @InjectFromDAO(entityType = Activity.class, method = "count")
    private long nbActivities;

    private TripListModel tripListModel;

    @SuppressWarnings("unused")
    public TripListModel getTripListModel() {
        if (tripListModel == null) {
            tripListModel = getT3Session().getTripListModel();
            if (tripListModel == null) {
                tripListModel = new TripListModel();
            }
        }
        return tripListModel;
    }

    @Override
    public String input() throws Exception {

        injectOnly(InjectDAO.class, InjectFromDAO.class);

        if (getT3Session().getTripListModel() == null) {

            // must load it
            tripListModel = loadTripListModel(tripDAO);
        } else {

            // use existing one
            tripListModel = getT3Session().getTripListModel();
        }

        oceans = tripListModel.getOceans();

        tripsByOcean = ArrayListMultimap.create();
        for (Ocean ocean : oceans) {
            List<String> ids = (List<String>) tripListModel.getTripIds(ocean);
            List<Trip> trips = tripDAO.findAllByIds(ids);
            tripsByOcean.putAll(ocean, trips);
        }
        nbTrips = tripListModel.getTripIdsByOcean().values().size();

        return INPUT;
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    @SuppressWarnings("unused")
    public long getNbTrips() {
        return nbTrips;
    }

    @SuppressWarnings("unused")
    public long getNbActivities() {
        return nbActivities;
    }

    @SuppressWarnings("unused")
    public long getNbActivities(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        long result = 0;
        for (Trip trip : trips) {
            result += trip.sizeActivity();
        }
        return result;
    }

    @SuppressWarnings("unused")
    public int getNbOceans() {
        return getOceans().size();
    }

    @SuppressWarnings("unused")
    public int getNbTrips(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        return trips.size();
    }

    @SuppressWarnings("unused")
    public String getFirstTripDate(Ocean ocean) {
        List<Trip> trips = (List<Trip>) getTrips(ocean);
        Trip trip = trips.get(0);
        return formatDate(trip.getLandingDate());
    }

    @SuppressWarnings("unused")
    public String getLastTripDate(Ocean ocean) {
        List<Trip> trips = (List<Trip>) getTrips(ocean);
        Trip trip = trips.get(trips.size() - 1);
        return formatDate(trip.getLandingDate());
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTripsWithNoDataComputed(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        Collection<Trip> filteredTrip = TripTopiaDao.getAllTripsWithNoDataComputed(trips);
        return getTripMap(filteredTrip);
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTripsWithSomeDataComputed(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        Collection<Trip> filteredTrip = TripTopiaDao.getAllTripsWithSomeDataComputed(trips);
        return getTripMap(filteredTrip);
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTripsWithAllDataComputed(Ocean ocean) {
        Collection<Trip> trips = getTrips(ocean);
        Collection<Trip> filteredTrip = TripTopiaDao.getAllTripsWithAllDataComputed(trips);
        return getTripMap(filteredTrip);
    }

    private Collection<Trip> getTrips(Ocean ocean) {
        return tripsByOcean.get(ocean);
    }

    private Map<String, String> getTripMap(Collection<Trip> trips) {
        Map<String, String> result = new LinkedHashMap<>();
        Decorator<Trip> decorator = getDecorator(Trip.class);
        for (Trip filteredTrip : trips) {
            result.put(filteredTrip.getTopiaId(), decorator.toString(filteredTrip));
        }
        return result;
    }

}
