/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.validators;

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.services.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Check user login.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class LoginValidator extends T3BaseFieldValidatorSupport {

    private static final Log log = LogFactory.getLog(LoginValidator.class);
    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        UserService userService = (UserService) getFieldValue("userService", object);
        String login = (String) getFieldValue("login", object);
        String password = (String) getFieldValue("password", object);

        if (log.isInfoEnabled()) {
            log.info("try to log for user " + login);
        }

        try {
            // check in db that user is ok
            Optional<T3User> user = userService.getUserByLogin(login);

            if (!user.isPresent()) {
                // user not found
                addFieldError("login", t("t3.error.login.unknown"));
                return;
            }

            boolean passwordOk = userService.checkPassword(user.get(), password);

            if (!passwordOk) {
                addFieldError("password", t("t3.error.bad.password"));
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not validate login", e);
            }
            throw new ValidationException(String.format("Could not validate login : %s", e.getMessage()));
        }
    }

    @Override
    public String getValidatorType() {
        return "login";
    }
}
