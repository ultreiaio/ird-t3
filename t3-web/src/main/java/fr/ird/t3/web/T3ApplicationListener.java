/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import fr.ird.converter.FloatConverter;
import fr.ird.t3.T3Config;
import fr.ird.t3.T3ConfigOption;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserJavaBeanDefinition;
import fr.ird.t3.entities.user.T3Users;
import fr.ird.t3.services.DefaultT3ServiceContext;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import fr.ird.t3.services.UserService;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.beans.Introspector;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

/**
 * To listen start or end of the application.
 * <p/>
 * On start we will load the configuration and check connection to internal
 * database, creates schema and create an admin user in none found in database.
 * <p/>
 * On stop, just release the application configuration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3ApplicationListener implements ServletContextListener {

    /** Logger. */
    protected static Log log = LogFactory.getLog(T3ApplicationListener.class);

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            log.error("Could not find pg driver", e);
        }
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            log.error("Could not find h2 driver", e);
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        String contextPath = StringUtils.removeStart(sce.getServletContext().getContextPath(), "/");
        if (contextPath.isEmpty()) {
            contextPath = "ROOT";
        }
        log.info(String.format("Application starting on [%s] at %s...", contextPath, new Date()));
        log.info(String.format("Boot log file [%s]", SystemUtils.getJavaIoTmpDir() + "/t3-boot.log"));

        // initialize configuration
        Properties defaultProps = new Properties();
        defaultProps.put(T3ConfigOption.CONTEXT_PATH.getKey(), contextPath);
        T3Config configuration = new T3Config(defaultProps);
        configuration.init();

        log = LogFactory.getLog(T3ApplicationListener.class);
        log.info("Configuration initialized.");

        T3ApplicationContext applicationContext = new T3ApplicationContext();
        T3ApplicationContext.setT3ApplicationContext(sce.getServletContext(), applicationContext);

        // initialize configuration
        applicationContext.setConfiguration(configuration);

        // init I18n
        DefaultI18nInitializer i18nInitializer = new DefaultI18nInitializer("t3-i18n");
        i18nInitializer.setMissingKeyReturnNull(true);
        I18n.init(i18nInitializer, Locale.getDefault());

        // register our not locale dependant converter
        Converter converter = ConverterUtil.getConverter(Float.class);
        if (converter != null) {
            ConvertUtils.deregister(Float.class);
        }
        ConvertUtils.register(new FloatConverter(), Float.class);

        try {
            T3Users t3Users = T3Users.create(configuration);
            applicationContext.setT3Users(t3Users);

        } catch (Exception e) {
            throw new IllegalStateException("could not initialize t3 users", e);
        }

        T3ServiceContext serviceContext = DefaultT3ServiceContext.newContext(
                Locale.getDefault(),
                applicationContext.getT3Users(),
                null,
                null,
                configuration,
                new T3ServiceFactory());

        try {
            createAdminUser(serviceContext);
        } catch (Exception e) {
            throw new IllegalStateException("could not create default admin user", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (log.isInfoEnabled()) {
            log.info("Application is ending at " + new Date() + "...");
        }

        ServletContext servletContext = sce.getServletContext();

        // get application context
        T3ApplicationContext applicationContext =
                T3ApplicationContext.getT3ApplicationContext(servletContext);

        // remove application context from container
        T3ApplicationContext.remove3ApplicationContext(servletContext);

        // release all user sessions
        releaseSessions(applicationContext);

        // see http://wiki.apache.org/commons/Logging/FrequentlyAskedQuestions#A_memory_leak_occurs_when_undeploying.2Fredeploying_a_webapp_that_uses_Commons_Logging._How_do_I_fix_this.3F
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        LogFactory.release(contextClassLoader);

        Introspector.flushCaches();
    }

    private void releaseSessions(T3ApplicationContext applicationContext) {
        Set<T3Session> t3Sessions = applicationContext.getT3Sessions();

        if (CollectionUtils.isNotEmpty(t3Sessions)) {
            for (T3Session t3Session : t3Sessions) {
                try {
                    applicationContext.destroyT3Session(t3Session);
                } catch (Exception e) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not close session " + t3Session, e);
                    }
                }
            }
        }

    }

    /**
     * Creates the administrator ({@code admin/admin}) on the internal database.
     *
     * @param serviceContext service context
     */
    private void createAdminUser(T3ServiceContext serviceContext) {
        UserService service = serviceContext.newService(UserService.class);
        List<T3User> users = service.getUsers();
        if (CollectionUtils.isEmpty(users)) {
            log.info("No user in database, will create default admin user (password admin).");
            T3User u = T3UserJavaBeanDefinition.instance().login("admin").password("admin").admin(true).build();
            service.createUser(u);
        }
    }

}
