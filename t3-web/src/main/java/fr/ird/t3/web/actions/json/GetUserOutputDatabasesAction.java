/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.json;

import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;

import java.util.Collection;

/**
 * Obtains for a given user id all the t3 database model.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class GetUserOutputDatabasesAction extends AbstractGetUserDatabasesAction {

    private static final long serialVersionUID = 1L;

    /**
     * <strong>Note:</strong> As this is a json action, we need to have at least a property on this very concrete class...
     *
     * @return the databases of the selected user.
     */
    @Override
    public Collection<UserDatabase> getDatabases() {
        return databases;
    }

    @Override
    protected Collection<UserDatabase> getDatabases(T3User user) {
        return user.getOutputs();
    }

}
