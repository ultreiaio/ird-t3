/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level2;

import fr.ird.t3.actions.data.level2.Level2Action;
import fr.ird.t3.actions.data.level2.Level2Configuration;
import fr.ird.t3.actions.stratum.SchoolTypeIndeterminate;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractRunAction;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * Run the level 2 action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class Level2RunAction extends AbstractRunAction<Level2Configuration, Level2Action> {

    private static final long serialVersionUID = 1L;

    private final Map<String, String> timeSteps = createTimeSteps();
    @InjectDecoratedBeans(beanType = ZoneStratumAwareMeta.class)
    private Map<String, String> zoneTypes;
    @InjectDecoratedBeans(beanType = ZoneVersion.class)
    private Map<String, String> zoneVersions;
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> catchFleets;
    @InjectDecoratedBeans(beanType = Ocean.class, filterById = true)
    private Map<String, String> oceans;
    @InjectDecoratedBeans(beanType = Species.class, filterById = true, pathIds = "speciesIds")
    private Map<String, String> species;
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> sampleFleets;
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> sampleFlags;
    private Map<String, String> schoolTypeIndeterminate;
    private Map<String, String> useSamplesOrNot;
    private Map<String, String> useWeightCategoriesInStratumOrNot;

    public Level2RunAction() {
        super(Level2Action.class);
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getCatchFleets() {
        return catchFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getZoneTypes() {
        return zoneTypes;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getZoneVersions() {
        return zoneVersions;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSpecies() {
        return species;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getOceans() {
        return oceans;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseWeightCategoriesInStratumOrNot() {
        return useWeightCategoriesInStratumOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSchoolTypeIndeterminate() {
        return schoolTypeIndeterminate;
    }

    @Override
    public void prepare() throws Exception {
        useSamplesOrNot = createLevel2UseSamplesOrNotMap();
        useWeightCategoriesInStratumOrNot = createUseWeightCategoriesInStratumOrNot();
        schoolTypeIndeterminate = sortAndDecorateIdAbles(Arrays.asList(SchoolTypeIndeterminate.values()));
        super.prepare();
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(Level2Action action, Exception error, Date startDate, Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action, error, startDate, endDate);
        map.put("oceans", oceans);
        map.put("species", species);
        map.put("sampleFleets", sampleFleets);
        map.put("sampleFlags", sampleFlags);
        map.put("catchFleets", catchFleets);
        map.put("zoneTypes", zoneTypes);
        map.put("zoneVersions", zoneVersions);
        return map;
    }
}
