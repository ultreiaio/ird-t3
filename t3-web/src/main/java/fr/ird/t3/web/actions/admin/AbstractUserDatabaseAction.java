/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;

/**
 * Operations of {@link UserDatabase}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractUserDatabaseAction extends T3ActionSupport implements Preparable {

    static final String INPUT_DATABASE = "inputDatabase";
    static final String OUTPUT_DATABASE = "outputDatabase";
    private static final Log log = LogFactory.getLog(AbstractUserDatabaseAction.class);
    private static final long serialVersionUID = 1L;
    UserDatabase databaseConfiguration;
    private T3User user;
    private String databaseEditAction;
    private String userEditAction;

    protected abstract UserDatabase getDatabase(String id) throws TopiaException;

    public abstract UserDatabase getDatabaseConfiguration();

    protected abstract Collection<UserDatabase> getDatabases(T3User user);

    public abstract String getDatabaseType();

    protected abstract void create(String userId, UserDatabase database);

    protected abstract void update(UserDatabase database);

    protected abstract void delete(String userId, UserDatabase database);

    @Override
    public void prepare() {
        String id = getDatabaseConfiguration().getId();
        if (StringUtils.isNotEmpty(id)) {
            // load configuration from db
            databaseConfiguration = getDatabase(id);
        }
    }

    public String doCreate() throws Exception {
        UserDatabase configuration = getDatabaseConfiguration();
        log.info("will create database configuration  " + configuration.getDescription());
        String userId = getUser().getId();
        create(userId, configuration);
        return SUCCESS;
    }

    public String doUpdate() throws Exception {
        UserDatabase configuration = getDatabaseConfiguration();
        log.info("will update user  " + configuration.getDescription());
        update(configuration);
        return SUCCESS;
    }

    public String doDelete() throws Exception {
        UserDatabase configuration = getDatabaseConfiguration();
        log.info("will update user  " + configuration.getDescription());
        String userId = getUser().getId();
        delete(userId, configuration);
        return SUCCESS;
    }

    @Override
    public void validate() {
        EditActionEnum action = getEditActionEnum();
        log.info("Edit action : " + action);

        if (action == null) {
            // no validation (no edit action)
            return;
        }
        UserDatabase configuration = getDatabaseConfiguration();
        String userId = getUser().getId();
        String description = configuration.getDescription();
        String login = configuration.getLogin();
        String password = configuration.getPassword();
        String url = configuration.getUrl();
        String id = configuration.getId();
        boolean noError = true;
        switch (action) {
            case CREATE:
                // url + description + login required
                if (StringUtils.isEmpty(description)) {
                    // empty description
                    addFieldError("databaseConfiguration.description", t("t3.error.required.description"));
                    noError = false;
                }
                if (StringUtils.isEmpty(url)) {
                    // empty url
                    addFieldError("databaseConfiguration.url", t("t3.error.required.url"));
                    noError = false;
                }
                if (StringUtils.isEmpty(login)) {
                    // empty user login
                    addFieldError("databaseConfiguration.login", t("t3.error.required.login"));
                    noError = false;
                }
                if (StringUtils.isEmpty(password)) {
                    // empty user password
                    addFieldError("databaseConfiguration.password", t("t3.error.required.password"));
                    noError = false;
                }
                if (noError) {
                    // check now the configuration does not already exist with same description for this user
                    T3User t3User = getUserService().getUserById(userId);
                    Collection<? extends UserDatabase> databases = getDatabases(t3User);
                    UserDatabase existingDatabase = null;
                    if (CollectionUtils.isNotEmpty(databases)) {
                        // look for an existing database with same description
                        for (UserDatabase database : databases) {
                            if (description.equals(database.getDescription())) {
                                existingDatabase = database;
                                break;
                            }
                        }
                    }
                    if (existingDatabase != null) {
                        addFieldError("databaseConfiguration.description", t("t3.error.configuration.description.already.used"));
                    }
                }

                break;
            case EDIT:
                // url + description + login required
                if (StringUtils.isEmpty(description)) {
                    // empty description
                    addFieldError("databaseConfiguration.description", t("t3.error.required.description"));
                    noError = false;
                }

                if (StringUtils.isEmpty(url)) {
                    // empty url
                    addFieldError("databaseConfiguration.url", t("t3.error.required.url"));
                    noError = false;
                }

                if (StringUtils.isEmpty(login)) {
                    // empty user login
                    addFieldError("databaseConfiguration.login", t("t3.error.required.login"));
                    noError = false;
                }
                if (StringUtils.isEmpty(password)) {
                    // empty user password
                    addFieldError("databaseConfiguration.password", t("t3.error.required.password"));
                    noError = false;
                }
                if (noError) {
                    // check now the configuration does not already exist with same description for this user
                    T3User t3User = getUserService().getUserById(userId);
                    Collection<? extends UserDatabase> databases = getDatabases(t3User);
                    UserDatabase existingDatabase = null;
                    if (CollectionUtils.isNotEmpty(databases)) {
                        // look for an existing database with same description
                        for (UserDatabase database : databases) {
                            if (description.equals(database.getDescription()) && !id.equals(database.getId())) {
                                existingDatabase = database;
                                break;
                            }
                        }
                    }
                    if (existingDatabase != null) {
                        addFieldError("databaseConfiguration.description", t("t3.error.configuration.description.already.used"));
                    }
                }
                break;
            case DELETE:
                // nothing to validate

            default:
                // nothing to validate
        }
    }

    public String getUserEditAction() {
        return userEditAction;
    }

    public void setUserEditAction(String userEditAction) {
        this.userEditAction = userEditAction;
    }

    public String getDatabaseEditAction() {
        return databaseEditAction;
    }

    public void setDatabaseEditAction(String databaseEditAction) {
        this.databaseEditAction = databaseEditAction;
    }

    public T3User getUser() {
        if (user == null) {
            user = new T3User();
        }
        return user;
    }

    protected EditActionEnum getEditActionEnum() {
        if (databaseEditAction == null) {
            return null;
        }
        return EditActionEnum.valueOf(databaseEditAction.toUpperCase());
    }
}
