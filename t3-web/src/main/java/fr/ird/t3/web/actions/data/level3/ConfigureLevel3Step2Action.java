/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level3;

import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.actions.stratum.SchoolTypeIndeterminate;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ParameterAware;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * To manager the step 2 of a level 3 treatment configuration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ConfigureLevel3Step2Action extends AbstractConfigureAction<Level3Configuration> implements ParameterAware {

    private static final Pattern BO_STRATUM_MINIMUM_COUNT_PATTERN = Pattern.compile("BO:(.*)?");
    private static final Pattern BL_STRATUM_MINIMUM_COUNT_PATTERN = Pattern.compile("BL:(.*)?");
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ConfigureLevel3Step2Action.class);
    private final Map<String, String> timeSteps = createTimeSteps();
    @InjectDAO(entityType = Country.class)
    private transient CountryTopiaDao countryDAO;
    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> sampleFleets;
    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> sampleFlags;
    @InjectDecoratedBeans(beanType = Species.class, filterById = true, pathIds = "speciesIds")
    private Map<String, String> species;
    private Map<String, String> schoolTypeIndeterminate;
    private Map<String, String> useSamplesOrNot;
    private Map<String, String> useWeightCategoriesInStratumOrNot;
    /**
     * Flag to know if some data are missing.
     * <p/>
     * This flag is sett in the {@link #prepare()} method while loading possibles data.
     */
    private boolean missingData;

    public ConfigureLevel3Step2Action() {
        super(Level3Configuration.class);
    }

    @Override
    public void prepare() throws Exception {
        useSamplesOrNot = createLevel3UseSamplesOrNotMap();
        useWeightCategoriesInStratumOrNot = createUseWeightCategoriesInStratumOrNot();
        schoolTypeIndeterminate = sortAndDecorateIdAbles(Arrays.asList(SchoolTypeIndeterminate.values()));
        // on level 3, can't treat Indeterminate school type
        schoolTypeIndeterminate.remove(SchoolTypeIndeterminate.IGNORE.name());
        Level3Configuration configuration = getConfiguration();
        log.info(String.format("Prepare with configuration %s", configuration));
        injectExcept(InjectDecoratedBeans.class);
        missingData = false;
        Set<Country> sampleFlags = new LinkedHashSet<>();
        Set<Country> sampleFleets = new LinkedHashSet<>();
        for (String oceanId : this.configuration.getOceanIds()) {
            sampleFlags.addAll(countryDAO.findAllFlagUsedInSample(oceanId));
            sampleFleets.addAll(countryDAO.findAllFleetUsedInSample(oceanId));
        }
        configuration.setSampleFlags(sortToList(sampleFlags));
        configuration.setSampleFleets(sortToList(sampleFleets));
        if (CollectionUtils.isEmpty(configuration.getSampleFleets())) {
            addFieldError("configuration.sampleFleetIds", t("t3.error.no.sample.fleet.found"));
            // need some data
            missingData = true;
        }
        if (CollectionUtils.isEmpty(configuration.getSampleFlags())) {
            addFieldError("configuration.sampleFlagIds", t("t3.error.no.sample.flag.found"));
            // need some data
            missingData = true;
        }
        // let's inject decorated values
        injectOnly(InjectDecoratedBeans.class);

        // prepare stratum minimum sample count for each species selected
        for (Species aSpecies : configuration.getSpecies()) {
            String specieId = aSpecies.getTopiaId();
            StratumMinimumSampleCount sampleCount = getStratumMinimumSampleCount(specieId);
            if (sampleCount == null) {
                sampleCount = new StratumMinimumSampleCount();
                Integer thresholdNumberLevel3FreeSchoolType = aSpecies.getThresholdNumberLevel3FreeSchoolType();
                Integer thresholdNumberLevel3ObjectSchoolType = aSpecies.getThresholdNumberLevel3ObjectSchoolType();
                sampleCount.setMinimumCountForFreeSchool(thresholdNumberLevel3FreeSchoolType);
                sampleCount.setMinimumCountForObjectSchool(thresholdNumberLevel3ObjectSchoolType);
                getStratumMinimumSampleCount().put(specieId, sampleCount);
            }
        }
        // prepare stratum weight ratio value
        float stratumWeightRatio = configuration.getStratumWeightRatio();
        if (stratumWeightRatio == 0) {
            // get the default value from application configuration
            stratumWeightRatio = getApplicationConfig().getStratumWeightRatio();
            // store it back in configuration
            configuration.setStratumWeightRatio(stratumWeightRatio);
        }
        if (log.isInfoEnabled()) {
            log.info("Selected sample fleet countries    : " + configuration.getSampleFleetIds());
            log.info("Selected sample flag  countries    : " + configuration.getSampleFlagIds());
            log.info("Selected weight ratio              : " + configuration.getStratumWeightRatio());
            log.info("Selected school type indeterminate : " + configuration.getSchoolTypeIndeterminate());
            for (String specieId : configuration.getSpeciesIds()) {
                String specieLabel = getSpecies().get(specieId);
                log.info("[" + specieLabel + "] min sample count BL       : " + getStratumMinimumSampleCountFreeSchoolType(specieId));
                log.info("[" + specieLabel + "] min sample count BO       : " + getStratumMinimumSampleCountObjectSchoolType(specieId));
            }
        }
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public Map<String, StratumMinimumSampleCount> getStratumMinimumSampleCount() {
        Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount = getConfiguration().getStratumMinimumSampleCount();
        if (stratumMinimumSampleCount == null) {
            stratumMinimumSampleCount = new TreeMap<>();
            getConfiguration().setStratumMinimumSampleCount(stratumMinimumSampleCount);
        }
        return stratumMinimumSampleCount;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public StratumMinimumSampleCount getStratumMinimumSampleCount(String specie) {
        return getStratumMinimumSampleCount().get(specie);
    }

    @Override
    public void validate() {
        Level3Configuration configuration = getConfiguration();
        if (CollectionUtils.isEmpty(configuration.getSampleFlagIds())) {
            addFieldError("configuration.sampleFlagIds", t("t3.error.no.sample.flag.selected"));
        }
        if (CollectionUtils.isEmpty(configuration.getSampleFleetIds())) {
            addFieldError("configuration.sampleFleetIds", t("t3.error.no.sample.fleet.selected"));
        }
        if (configuration.getSchoolTypeIndeterminate() == null) {
            addFieldError("configuration.schoolTypeIndeterminate", t("t3.error.no.schoolTypeIndeterminate.selected"));
        }
    }

    @Override
    public String execute() {
        Level3Configuration configuration = getConfiguration();
        // as the level 3 configuration is now fully valid, store it
        configuration.setValidStep2(true);
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    @SuppressWarnings("WeakerAccess")
    public Integer getStratumMinimumSampleCountObjectSchoolType(String specieId) {
        StratumMinimumSampleCount sampleCount = getStratumMinimumSampleCount(specieId);
        return sampleCount.getMinimumCountForObjectSchool();
    }

    @SuppressWarnings("unused")
    public void setStratumMinimumSampleCountObjectSchoolType(String specieId, Integer stratumMinimumSampleCountObjectSchoolType) {
        StratumMinimumSampleCount sampleCount = getStratumMinimumSampleCount(specieId);
        sampleCount.setMinimumCountForObjectSchool(stratumMinimumSampleCountObjectSchoolType);
    }

    @SuppressWarnings("WeakerAccess")
    public Integer getStratumMinimumSampleCountFreeSchoolType(String specieId) {
        StratumMinimumSampleCount sampleCount =
                getStratumMinimumSampleCount(specieId);
        return sampleCount.getMinimumCountForFreeSchool();
    }

    @SuppressWarnings("unused")
    public void setStratumMinimumSampleCountFreeSchoolType(String specieId, Integer stratumMinimumSampleCountFreeSchoolType) {
        StratumMinimumSampleCount sampleCount = getStratumMinimumSampleCount(specieId);
        sampleCount.setMinimumCountForFreeSchool(stratumMinimumSampleCountFreeSchoolType);
    }

    @SuppressWarnings("unused")
    public boolean isMissingData() {
        return missingData;
    }

    @Override
    public void setParameters(Map<String, String[]> parameters) {
        // to obtain back values for stratumMinimumCount...
        for (Map.Entry<String, String[]> e : parameters.entrySet()) {
            String name = e.getKey();
            String[] values = e.getValue();
            Matcher matcher = BO_STRATUM_MINIMUM_COUNT_PATTERN.matcher(name);
            if (matcher.matches()) {
                // specieId is second part
                String specieId = matcher.group(1);
                StratumMinimumSampleCount sampleCount = getStratumMinimumSampleCount(specieId);
                Integer realValue = null;
                if (values.length > 0) {
                    try {
                        realValue = Integer.valueOf(values[0]);
                    } catch (NumberFormatException e1) {
                        addFieldError(name, t("t3.error.invalid.integer"));
                    }
                }
                sampleCount.setMinimumCountForObjectSchool(realValue);
                continue;
            }
            matcher = BL_STRATUM_MINIMUM_COUNT_PATTERN.matcher(name);
            if (matcher.matches()) {
                // specieId is second part
                String specieId = matcher.group(1);
                StratumMinimumSampleCount sampleCount = getStratumMinimumSampleCount(specieId);
                Integer realValue = null;
                if (values.length > 0) {
                    try {
                        realValue = Integer.valueOf(values[0]);
                    } catch (NumberFormatException e1) {
                        addFieldError(name, t("t3.error.invalid.integer"));
                    }
                }
                sampleCount.setMinimumCountForFreeSchool(realValue);
            }
        }
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSpecies() {
        return species;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseWeightCategoriesInStratumOrNot() {
        return useWeightCategoriesInStratumOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSchoolTypeIndeterminate() {
        return schoolTypeIndeterminate;
    }
}
