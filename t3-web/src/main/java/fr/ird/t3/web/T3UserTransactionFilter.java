/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import fr.ird.t3.entities.T3TopiaApplicationContext;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.function.Supplier;

/**
 * Deliver for each http request an t3 db transaction (if user has it in
 * his session).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class T3UserTransactionFilter extends fr.ird.t3.web.listeners.TopiaTransactionFilter<T3TopiaPersistenceContext> {

    private static final Log log = LogFactory.getLog(T3UserTransactionFilter.class);

    private static final String USER_TRANSACTION = "userTransaction";

    @SuppressWarnings("unchecked")
    public static Supplier<T3TopiaPersistenceContext> getTransaction(ServletRequest request) {
        return (Supplier<T3TopiaPersistenceContext>) request.getAttribute(USER_TRANSACTION);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        setRequestAttributeName(USER_TRANSACTION);
    }

    @Override
    public T3TopiaPersistenceContext newPersistenceContext(ServletRequest request) {

        HttpSession session = ((HttpServletRequest) request).getSession();
        T3Session t3Session = T3Session.getT3Session(session);
        T3TopiaApplicationContext rootContext = t3Session.getRootContext();
        T3TopiaPersistenceContext transaction = rootContext.newPersistenceContext();
        log.debug(String.format("Starts a new t3 transaction %s", transaction));
        return transaction;
    }

}
