/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import fr.ird.t3.actions.data.level1.Level1Configuration;
import fr.ird.t3.actions.data.level1.Level1Step;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

/**
 * Abstract run action for all level1 actions.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AbstractLevel1ConfigureAction extends AbstractConfigureAction<Level1Configuration> {

    private static final Log log = LogFactory.getLog(AbstractLevel1ConfigureAction.class);
    private static final long serialVersionUID = 1L;

    private final Level1Step[] requiredSteps;

    @InjectDAO(entityType = Sample.class)
    private transient SampleTopiaDao sampleDAO;

    @InjectDecoratedBeans(beanType = SampleQuality.class, filterById = true, pathIds = "sampleQualityIds")
    private Map<String, String> sampleQualities;

    @InjectDecoratedBeans(beanType = SampleType.class, filterById = true)
    private Map<String, String> sampleTypes;

    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> fleets;

    @InjectDecoratedBeans(beanType = Ocean.class, filterById = true)
    private Map<String, String> oceans;

    private Map<String, String> useRfMinus10AndRfPlus10OrNot;

    AbstractLevel1ConfigureAction(Level1Step currentStep) {
        super(Level1Configuration.class);
        EnumSet<Level1Step> steps = EnumSet.allOf(Level1Step.class);
        // do not need this step
        steps.removeIf(step -> step.ordinal() >= currentStep.ordinal());
        requiredSteps = steps.toArray(new Level1Step[steps.size()]);
    }

    @Override
    public void prepare() throws Exception {
        useRfMinus10AndRfPlus10OrNot = createUseRfMinus10AndRfPlus10OrNot();
        Level1Configuration configuration = getConfiguration();
        injectOnly(InjectDecoratedBeans.class, InjectDAO.class);
        if (log.isInfoEnabled()) {
            log.info("Selected fleet countries  : " + configuration.getFleetIds());
            log.info("Selected sample qualities : " + configuration.getSampleQualityIds());
            log.info("Selected sample types     : " + configuration.getSampleTypeIds());
            log.info("Selected oceans           : " + configuration.getOceanIds());
            log.info("Selected begin date       : " + configuration.getBeginDate());
            log.info("Selected end   date       : " + configuration.getEndDate());
            log.info("Executed steps            : " + configuration.getExecutedSteps());
        }
    }

    @Override
    public String input() {
        return INPUT;
    }

    @Override
    public String execute() {
        prepareActionContext();
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (isRequiredStepsExecutedOnConfiguration()) {
            // all required step already executed with this configuration
            log.info(String.format("Current configuration already treats all required step %s", Arrays.toString(requiredSteps)));
        } else {
            // check on each selected sample of the configuration which steps are missing
            Multimap<Level1Step, Sample> missingSteps = getMissingRequiredStepsExecutedOnSample();
            if (missingSteps.isEmpty()) {
                // ok every steps were previously executed on selected sample
                log.info(String.format("All required steps %s were already executed on selected samples of current configuration.", Arrays.toString(requiredSteps)));
            } else {
                // there is some missing steps for some samples...
                for (Level1Step step : missingSteps.keySet()) {
                    addActionError(t("t3.error.level1.missing.step", t(step.getI18nKey()), missingSteps.get(step).size()));
                }
            }
        }
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleQualities() {
        return sampleQualities;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleTypes() {
        return sampleTypes;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getFleets() {
        return fleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getOceans() {
        return oceans;
    }

    @SuppressWarnings("unused")
    public int getMatchingTripCount() {
        Map<String, Collection<String>> tripIds = getTripIds().asMap();
        return MapUtils.isEmpty(tripIds) ? 0 : tripIds.size();
    }

    @SuppressWarnings("unused")
    public int getMatchingSampleCount() {
        Map<String, Collection<String>> tripIds = getTripIds().asMap();
        if (MapUtils.isEmpty(tripIds)) {
            return 0;
        }
        int nbSamples = 0;
        for (Collection<String> sampleIds : tripIds.values()) {
            nbSamples += sampleIds.size();
        }
        return nbSamples;
    }

    @SuppressWarnings("unused")
    public int getNotMatchingSampleCount() {
        return getConfiguration().getNotTreatedSampleReason().size();
    }

    @SuppressWarnings("unused")
    public Multimap<String, String> getTripIds() {
        return getConfiguration().getSampleIdsByTripId();
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseRfMinus10AndRfPlus10OrNot() {
        return useRfMinus10AndRfPlus10OrNot;
    }

    /**
     * Checks on the given {@link #getConfiguration()} that all required
     * steps were executed.
     *
     * @return {@code true} if all required step were executed on this
     * configuration, {@code false} otherwise.
     */
    private boolean isRequiredStepsExecutedOnConfiguration() {
        Set<Level1Step> executedSteps = getConfiguration().getExecutedSteps();
        for (Level1Step requiredStep : requiredSteps) {
            if (!executedSteps.contains(requiredStep)) {
                log.warn(String.format("A required step [%s] was still not executed with this configuration ", requiredStep));
                return false;
            }
        }
        // all required step were executed
        return true;
    }

    /**
     * Checks on selected samples of the {@link #getConfiguration()} that all
     * required step were previously executed (but not necessary
     * by this configuration).
     *
     * @return the multi-map of missing steps.
     */
    private Multimap<Level1Step, Sample> getMissingRequiredStepsExecutedOnSample() {
        Collection<String> sampleIds = getTripIds().values();
        Multimap<Level1Step, Sample> missingStates = HashMultimap.create();
        for (String sampleId : sampleIds) {
            Sample sample = sampleDAO.forTopiaIdEquals(sampleId).findUnique();
            for (Level1Step requiredStep : requiredSteps) {
                boolean sampleState = requiredStep.getState(sample);
                if (!sampleState) {
                    log.warn(String.format("Required step %s is missing for sample %s", requiredStep, sampleId));
                    missingStates.put(requiredStep, sample);
                }
            }
        }
        return missingStates;
    }
}
