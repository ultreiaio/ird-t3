/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.ActionContext;
import fr.ird.t3.T3Config;
import fr.ird.t3.entities.user.T3Users;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Application context loaded once for all when application starts and
 * is destroyed at the end of it.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class T3ApplicationContext {

    private static final Log log = LogFactory.getLog(T3ApplicationContext.class);

    /** Key to store the single instance of the application context */
    private static final String APPLICATION_CONTEXT_PARAMETER = "t3ApplicationContext";

    /** Application configuration. */
    private T3Config configuration;

    /** Root context for the internal database. */
    private T3Users t3Users;

    /** Set of all logged user sessions to be close at shutdown time. */
    private Set<T3Session> t3Sessions;

    public static T3ApplicationContext getT3ApplicationContext(ActionContext actionContext) {
        Map<String, Object> application = actionContext.getApplication();
        return (T3ApplicationContext) application.get(APPLICATION_CONTEXT_PARAMETER);
    }

    public static T3ApplicationContext getT3ApplicationContext(ServletContext servletContext) {
        return (T3ApplicationContext) servletContext.getAttribute(APPLICATION_CONTEXT_PARAMETER);
    }

    public static void setT3ApplicationContext(ServletContext servletContext, T3ApplicationContext applicationContext) {
        servletContext.setAttribute(APPLICATION_CONTEXT_PARAMETER, applicationContext);
    }

    public static void remove3ApplicationContext(ServletContext servletContext) {
        servletContext.removeAttribute(APPLICATION_CONTEXT_PARAMETER);
    }

    public Set<T3Session> getT3Sessions() {
        return t3Sessions;
    }

    public synchronized void registerT3Session(T3Session session) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(session.getUser());
        if (t3Sessions == null) {
            t3Sessions = new HashSet<>();
        }
        if (log.isInfoEnabled()) {
            log.info("Register user session for [" +
                    session.getUser().getLogin() + "]");
        }
        t3Sessions.add(session);
    }

    public synchronized void destroyT3Session(T3Session session) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(session.getUser());
        Preconditions.checkNotNull(t3Sessions);
        if (log.isInfoEnabled()) {
            log.info("Destroy user session for [" +
                    session.getUser().getLogin() + "]");
        }
        // remove session from active ones
        t3Sessions.remove(session);
        // close session
        session.close();
    }

    public T3Config getConfiguration() {
        return configuration;
    }

    public void setConfiguration(T3Config configuration) {
        this.configuration = configuration;
    }

    public T3Users getT3Users() {
        return t3Users;
    }

    public void setT3Users(T3Users t3Users) {
        this.t3Users = t3Users;
    }

}
