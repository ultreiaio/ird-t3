/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import fr.ird.t3.web.T3Session;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Interceptor to check if user has select a T3 database.
 * <p/>
 * If not then redirect to the {@link #redirectAction} action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class CheckT3DatabaseSelectedInterceptor extends AbstractCheckInterceptor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CheckT3DatabaseSelectedInterceptor.class);

    @Override
    protected boolean doCheck(ActionInvocation invocation) {
        T3Session t3Session =
                T3Session.getT3Session(invocation.getInvocationContext());
        boolean t3DatabaseSelected = t3Session.isT3DatabaseSelected();

        if (!t3DatabaseSelected) {
            if (log.isInfoEnabled()) {
                log.info("No t3 database selected!");
            }
        }
        return t3DatabaseSelected;
    }


}
