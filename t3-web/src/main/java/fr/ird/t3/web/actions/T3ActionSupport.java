/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multimap;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import fr.ird.t3.T3Config;
import fr.ird.t3.entities.T3TopiaPersistenceContext;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.DefaultT3ServiceContext;
import fr.ird.t3.services.FreeMarkerService;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3OutputService;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import fr.ird.t3.services.T3TopiaPersistenceContextAware;
import fr.ird.t3.services.UserService;
import fr.ird.t3.services.ZoneStratumService;
import fr.ird.t3.web.T3ApplicationContext;
import fr.ird.t3.web.T3Session;
import fr.ird.t3.web.T3UserTransactionFilter;
import fr.ird.t3.web.actions.admin.TripListModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsStatics;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.TimeLog;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;


/**
 * Action support for any t3 action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.1
 */
public class T3ActionSupport extends ActionSupport implements T3TopiaPersistenceContextAware {

    static final String LOG_LINE = "--------------------------------------------------------------------------------";
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(T3ActionSupport.class);
    private static final TimeLog TIME_LOG = new TimeLog(T3ActionSupport.class);
    private transient Supplier<T3TopiaPersistenceContext> transaction;
    /**
     * Provides a way to get a service.
     * <p/>
     * Actions may <strong>not</strong> call it directly by use
     * {@link #newService(Class)} instead.
     */
    private transient T3ServiceFactory serviceFactory;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat monthFormat;
    private transient T3ServiceContext serviceContext;

    public static T3ApplicationContext getT3ApplicationContext() {
        ActionContext actionContext = getActionContext();
        return T3ApplicationContext.getT3ApplicationContext(actionContext);
    }

    public static String getApplicationVersion() {
        return getApplicationConfig().getApplicationVersion();
    }

    public static T3Config getApplicationConfig() {
        return getT3ApplicationContext().getConfiguration();
    }

    protected static ActionContext getActionContext() {
        return ActionContext.getContext();
    }

    public T3ServiceFactory getServiceFactory() {
        if (serviceFactory == null) {
            serviceFactory = new T3ServiceFactory();
        }
        return serviceFactory;
    }

    public String t(String key, Object... args) {
        return getText(key, Arrays.asList(args));
    }

    /**
     * Fabrique pour récupérer le ServiceContext tel qu'il devrait être fourni
     * à la fabrication d'un service.
     *
     * @return service context
     */
    protected T3ServiceContext getServiceContext() {
        if (serviceContext == null) {
            serviceContext = DefaultT3ServiceContext.newContext(
                    getLocale(),
                    getT3ApplicationContext().getT3Users(),
                    getT3Session().getRootContext(),
                    getT3TopiaPersistenceContext(),
                    getApplicationConfig(),
                    getServiceFactory());
        }
        return serviceContext;
    }

    protected T3Session getT3Session() {
        return T3Session.getT3Session(getActionContext());
    }

    protected <C> C getActionConfiguration(Class<C> configurationClass) {
        return getT3Session().getActionConfiguration(configurationClass);
    }

    public final DecoratorService getDecoratorService() {
        return newService(DecoratorService.class);
    }

    public final FreeMarkerService getFreeMarkerService() {
        return newService(FreeMarkerService.class);
    }

    public final IOCService getIocService() {
        return newService(IOCService.class);
    }

    public final ZoneStratumService getZoneStratumService() {
        return newService(ZoneStratumService.class);
    }

    public final T3InputService getT3InputService() {
        return newService(T3InputService.class);
    }

    public final T3OutputService getT3OutputService() {
        return newService(T3OutputService.class);
    }

    public final UserService getUserService() {
        return newService(UserService.class);
    }

    public <O> Decorator<O> getDecorator(Class<O> type) {
        return getDecorator(type, null);
    }

    public String decorate(Object o) {
        Decorator<?> decorator = getDecorator(o.getClass());
        return decorator.toString(o);
    }

    public <O> Collection<String> decorate(Class<O> type, Collection<O> objects) {
        Decorator<O> decorator = getDecorator(type);
        Collection<String> result = new ArrayList<>();
        for (O object : objects) {
            String s = decorator.toString(object);
            result.add(s);
        }
        return result;
    }

    public <O> Decorator<O> getDecorator(Class<O> type, String context) {
        return getDecoratorService().getDecorator(getLocale(), type, context);
    }

    public String formatDate(Date date) {
        return getDateFormat().format(date);
    }

    public String formatMonth(Date date) {
        return getMonthFormat().format(date);
    }

    @Override
    public final Supplier<T3TopiaPersistenceContext> getT3TopiaPersistenceContext() {
        if (transaction == null) {
            HttpServletRequest request = (HttpServletRequest) getActionContext().get(StrutsStatics.HTTP_REQUEST);
            transaction = T3UserTransactionFilter.getTransaction(request);
        }
        return transaction;
    }

    protected void injectExcept(Class<?>... annotations) throws Exception {
        getIocService().injectExcept(this, annotations);
    }

    protected void injectOnly(Class<?>... annotations) throws Exception {
        getIocService().injectOnly(this, annotations);
    }

    protected <E extends T3Service> E newService(Class<E> clazz) {
        return getServiceContext().newService(clazz);
    }

    protected void addContentInFile(T3Session session, String content) {
        File userLogfile = session.getUserLogFile();
        try (FileWriter writer = new FileWriter(userLogfile, true)) {
            writer.append(content);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not write content in user log file %s", userLogfile), e);
        }
    }

    protected <E extends TopiaEntity> Map<String, String> sortAndDecorate(Collection<E> beans) {
        return sortAndDecorate(beans, null);
    }

    protected <E extends TopiaEntity> Map<String, String> sortAndDecorate(Collection<E> beans, String context) {
        return getDecoratorService().sortAndDecorate(getLocale(), beans, context);
    }

    protected <E extends Idable> Map<String, String> sortAndDecorateIdAbles(Collection<E> beans) {
        return sortAndDecorateIdAbles(beans, null);
    }

    protected <E extends Idable> Map<String, String> sortAndDecorateIdAbles(Collection<E> beans, String context) {
        return getDecoratorService().sortAndDecorateIdAbles(getLocale(), beans, context);
    }

    protected <E> List<E> sortToList(Collection<E> beans) {
        return sortToList(beans, null);
    }

    protected <E> List<E> sortToList(Collection<E> beans, String context) {
        return getDecoratorService().sortToList(getLocale(), beans, context);
    }

    protected Map<String, String> createTimeSteps() {
        Map<String, String> timeSteps = new LinkedHashMap<>();
        for (int i = 1; i < 13; i++) {
            timeSteps.put("" + i, "" + i);
        }
        return timeSteps;
    }

    protected SimpleDateFormat getDateFormat() {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        }
        return dateFormat;
    }

    protected SimpleDateFormat getMonthFormat() {
        if (monthFormat == null) {
            monthFormat = new SimpleDateFormat("mm-yyyy");
        }
        return monthFormat;
    }

    protected TripListModel loadTripListModel(TripTopiaDao tripDAO) throws TopiaException {
        TripTopiaDao.EMPTY_OCEAN.setLabel1(t("t3.common.nullOcean"));
        long t0 = TimeLog.getTime();
        log.info("Loading tripListModel...");
        TripListModel result = new TripListModel();
        Multimap<Ocean, String> allByOcean = tripDAO.findAllIdsByOcean();
        result.setTripIdsByOcean(allByOcean);
        getT3Session().setTripListModel(result);
        List<Ocean> allOceans = new ArrayList<>(allByOcean.keySet());
        TIME_LOG.log(t0, String.format("loadTripListModel for %d trip(s) (%d ocean(s)).", result.getNbTrips(), allOceans.size()));
        allOceans = sortToList(allOceans);
        result.setOceans(allOceans);
        getT3Session().setTripListModel(result);
        return result;
    }

    protected ImmutableMap<String, String> createUseWeightCategoriesInStratumOrNot() {
        return ImmutableMap.of(
                "false", t("t3.label.stratum.configuration.ignoreWeightCategoryInStratum"),
                "true", t("t3.label.stratum.configuration.useWeightCategoryInStratum"));
    }

    protected ImmutableMap<String, String> createLevel2UseSamplesOrNotMap() {
        return ImmutableMap.of(
                "false", t("t3.label.data.level2.configuration.useCatchSamples"),
                "true", t("t3.label.data.level2.configuration.useAllSamples"));
    }

    protected ImmutableMap<String, String> createLevel3UseSamplesOrNotMap() {
        return ImmutableMap.of(
                "false", t("t3.label.data.level3.configuration.useCatchSamples"),
                "true", t("t3.label.data.level3.configuration.useAllSamples"));
    }

    protected ImmutableMap<String, String> createUseRfMinus10AndRfPlus10OrNot() {
        return ImmutableMap.of(
                "true", t("t3.label.data.level1.configuration.useRfMinus10AndRfPlus10"),
                "false", t("t3.label.data.level1.configuration.useOnlyRfTot"));
    }


    protected ImmutableMap<String, String> createUseLegacyBehaviourForTripWithoutLandingOrNot() {
        return ImmutableMap.of(
                "false", t("t3.label.level0.configuration.rf1.useNewBehaviourForTripWithoutLanding"),
                "true", t("t3.label.level0.configuration.rf1.useLegacyBehaviourForTripWithoutLanding"));
    }

}
