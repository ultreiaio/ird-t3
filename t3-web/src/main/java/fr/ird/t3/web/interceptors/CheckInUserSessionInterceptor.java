/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.util.TextParseUtil;
import fr.ird.t3.web.T3Session;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * To check if some data are in the user session.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see T3Session
 * @since 1.0
 */
public class CheckInUserSessionInterceptor extends AbstractCheckInterceptor {

    protected static final Log log = LogFactory.getLog(CheckInUserSessionInterceptor.class);
    private static final long serialVersionUID = 1L;
    private Set<String> parameters = Collections.emptySet();

    /**
     * Allows <code>sessionParametersToClean</code> attribute to be set as comma-separated-values (csv).
     *
     * @param parameters the sessionParametersToClean to set
     */
    public void setParameters(String parameters) {
        this.parameters = TextParseUtil.commaDelimitedStringToSet(parameters);
    }

    @Override
    protected boolean doCheck(ActionInvocation invocation) {
        T3Session t3Session = T3Session.getT3Session(invocation.getInvocationContext());
        Set<String> missingKeys = null;
        for (String parameter : parameters) {
            boolean ok = t3Session.contains(parameter);
            if (!ok) {
                if (missingKeys == null) {
                    missingKeys = new HashSet<>();
                }
                missingKeys.add(parameter);
            }
        }
        boolean result = true;
        if (CollectionUtils.isNotEmpty(missingKeys)) {
            // there is something missing in the user session
            String message = String.format("Some objects were not found in session : %s, redirect to %s", missingKeys, redirectAction);
            log.info(message);
            result = false;
        }
        return result;
    }

}
