/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data;

import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * For user to download his log file.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class DownloadUserLogAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DownloadUserLogAction.class);

    protected transient InputStream inputStream;

    protected String contentDisposition;

    protected String contentType;

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public String getContentType() {
        return contentType;
    }

    @Override
    public String execute() throws Exception {

        File file = getT3Session().getUserLogFile();
        if (log.isInfoEnabled()) {
            log.info("Will download user log file " + file);
        }
        contentDisposition = String.format("attachment;filename=\"%s\"", file.getName());
        contentType = "text/plain";
        inputStream = new FileInputStream(file);
        return SUCCESS;
    }

}
