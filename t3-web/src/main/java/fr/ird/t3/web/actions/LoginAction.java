/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.opensymphony.xwork2.interceptor.I18nInterceptor;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.web.T3Session;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Login and Logout action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class LoginAction extends T3ActionSupport implements SessionAware {

    protected static final Log log = LogFactory.getLog(LoginAction.class);

    private static final long serialVersionUID = 1L;

    protected boolean logging;

    protected String login;

    protected String password;

    protected String redirectAction;

    private transient Map<String, Object> session;

    public boolean isLogging() {
        return logging;
    }

    public void setLogging(boolean logging) {
        this.logging = logging;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRedirectAction() {
        return redirectAction;
    }

    public void setRedirectAction(String redirectAction) {
        this.redirectAction = redirectAction;
    }

    public String doLogin() {

        T3User user = getUserService().getUserByLogin(login).orElse(null);

        T3Session userSession = getT3Session();

        // starts session
        userSession.setStartDate(new Date());

        // user is authorized, keep it in his t3Session
        userSession.setUser(user);

        // let's register
        getT3ApplicationContext().registerT3Session(userSession);

        // add locale in t3Session if required
        Object o = session.get(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE);
        if (o == null) {

            // push the locale in t3Session to be able to use it everywhere
            Locale locale = getActionContext().getLocale();
            session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        }
        addContentInFile(userSession, String.format("%s\nSession started at %s\n", LOG_LINE, userSession.getStartDate()));

        // redecode parameters
        log.info(String.format("success login for user %s, will redirect to %s", login, redirectAction));
        return "redirect";
    }

    public String doLogout() {

        T3Session userSession = getT3Session();

        // just clean session
        addContentInFile(userSession, String.format("%s\nSession stops at %s\n", LOG_LINE, new Date()));

        // clear user session
        session.clear();

        // remove user from session
        getT3ApplicationContext().destroyT3Session(userSession);

        // clean it from ActionContext (we never know...)
        T3Session.removeT3Session(getActionContext());

        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

}
