/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level3;

import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.actions.stratum.SchoolTypeIndeterminate;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolTypeTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * To configure level 3 action, mainly print the level 3 configuration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class Level3ConfigureAction extends AbstractConfigureAction<Level3Configuration> {

    private static final Log log = LogFactory.getLog(Level3ConfigureAction.class);
    private static final long serialVersionUID = 1L;

    private final Map<String, String> timeSteps = createTimeSteps();

    /** all zoneTypes. */
    @InjectDecoratedBeans(beanType = ZoneStratumAwareMeta.class)
    private Map<String, String> zoneTypes;
    /** all zoneVersions. */
    @InjectDecoratedBeans(beanType = ZoneVersion.class)
    private Map<String, String> zoneVersions;
    /** all fleet Countries. */
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> catchFleets;
    /** all oceans. */
    @InjectDecoratedBeans(beanType = Ocean.class, filterById = true)
    private Map<String, String> oceans;
    /** all species. */
    @InjectDecoratedBeans(beanType = Species.class, filterById = true, pathIds = "speciesIds")
    private Map<String, String> species;
    /** all sample fleets. */
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> sampleFleets;
    /** all sample flags. */
    @InjectDecoratedBeans(beanType = Country.class, filterById = true)
    private Map<String, String> sampleFlags;
    private Map<String, String> schoolTypeIndeterminate;
    private Map<String, String> useSamplesOrNot;
    private Map<String, String> useWeightCategoriesInStratumOrNot;

    public Level3ConfigureAction() {
        super(Level3Configuration.class);
    }

    @Override
    public void prepare() throws Exception {
        log.info(String.format("Prepare with configuration %s", getConfiguration()));
        useSamplesOrNot = createLevel3UseSamplesOrNotMap();
        useWeightCategoriesInStratumOrNot = createUseWeightCategoriesInStratumOrNot();
        schoolTypeIndeterminate = sortAndDecorateIdAbles(Arrays.asList(SchoolTypeIndeterminate.values()));
        // on level 3, can't treat Indeterminate school type
        schoolTypeIndeterminate.remove(SchoolTypeIndeterminate.IGNORE.name());
        // load configuration
        getConfiguration();
        // load decorated beans
        injectOnly(InjectDecoratedBeans.class);
        // on level 3, can't treat Indeterminate school type
        schoolTypeIndeterminate.remove(SchoolTypeTopiaDao.SCHOOL_TYPE_INDETERMINATE_ID);
    }

    @Override
    public String execute() {
        prepareActionContext();
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getCatchFleets() {
        return catchFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getZoneTypes() {
        return zoneTypes;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getZoneVersions() {
        return zoneVersions;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getOceans() {
        return oceans;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSpecies() {
        return species;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public Map<String, StratumMinimumSampleCount> getStratumMinimumSampleCount() {
        Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount = getConfiguration().getStratumMinimumSampleCount();
        if (stratumMinimumSampleCount == null) {
            stratumMinimumSampleCount = new TreeMap<>();
            getConfiguration().setStratumMinimumSampleCount(stratumMinimumSampleCount);
        }
        return stratumMinimumSampleCount;
    }

    @SuppressWarnings("unused")
    public StratumMinimumSampleCount getStratumMinimumSampleCount(String specie) {
        return getStratumMinimumSampleCount().get(specie);
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseWeightCategoriesInStratumOrNot() {
        return useWeightCategoriesInStratumOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSchoolTypeIndeterminate() {
        return schoolTypeIndeterminate;
    }
}
