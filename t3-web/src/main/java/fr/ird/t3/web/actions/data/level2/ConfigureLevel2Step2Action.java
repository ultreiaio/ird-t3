/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level2;

import fr.ird.t3.actions.data.level2.Level2Configuration;
import fr.ird.t3.actions.stratum.SchoolTypeIndeterminate;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryTopiaDao;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SchoolTypeTopiaDao;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * To manager the step 2 of a level 2 treatment configuration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ConfigureLevel2Step2Action extends AbstractConfigureAction<Level2Configuration> {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ConfigureLevel2Step2Action.class);
    private final Map<String, String> timeSteps = createTimeSteps();
    @InjectDAO(entityType = Country.class)
    private transient CountryTopiaDao countryDAO;
    @InjectDAO(entityType = SchoolType.class)
    private transient SchoolTypeTopiaDao schoolTypeDAO;
    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> sampleFleets;
    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> sampleFlags;
    private Map<String, String> schoolTypeIndeterminate;
    private Map<String, String> useSamplesOrNot;
    private Map<String, String> useWeightCategoriesInStratumOrNot;
    /**
     * Flag to know if some data are missing.
     * <p/>
     * This flag is set in the {@link #prepare()} method while loading possibles data.
     */
    private boolean missingData;

    public ConfigureLevel2Step2Action() {
        super(Level2Configuration.class);
    }

    @Override
    public void prepare() throws Exception {
        useSamplesOrNot = createLevel2UseSamplesOrNotMap();
        useWeightCategoriesInStratumOrNot = createUseWeightCategoriesInStratumOrNot();
        schoolTypeIndeterminate = sortAndDecorateIdAbles(Arrays.asList(SchoolTypeIndeterminate.values()));
        Level2Configuration configuration = getConfiguration();
        // let's inject everything except decorated values
        injectExcept(InjectDecoratedBeans.class);
        missingData = false;
        Set<Country> sampleFlags = new LinkedHashSet<>();
        Set<Country> sampleFleets = new LinkedHashSet<>();
        for (String oceanId : this.configuration.getOceanIds()) {
            sampleFlags.addAll(countryDAO.findAllFlagUsedInSample(oceanId));
            sampleFleets.addAll(countryDAO.findAllFleetUsedInSample(oceanId));
        }
        configuration.setSampleFlags(sortToList(sampleFlags));
        configuration.setSampleFleets(sortToList(sampleFleets));

        if (CollectionUtils.isEmpty(configuration.getSampleFlags())) {
            addFieldError("configuration.sampleFleetIds", t("t3.error.no.sample.fleet.found"));
            // need some data
            missingData = true;
        }
        if (CollectionUtils.isEmpty(configuration.getSampleFleets())) {
            addFieldError("configuration.sampleFlagIds", t("t3.error.no.sample.flag.found"));
            // need some data
            missingData = true;
        }
        // let's inject decorated values
        injectOnly(InjectDecoratedBeans.class);
        // prepare stratum minimum sample count for free school type value
        Integer stratumMinimumSampleCountFreeSchoolType = configuration.getStratumMinimumSampleCountFreeSchoolType();
        if (stratumMinimumSampleCountFreeSchoolType == null) {
            // get value from db
            SchoolType schoolType = schoolTypeDAO.forCodeEquals(2).findUnique();
            stratumMinimumSampleCountFreeSchoolType = schoolType.getThresholdNumberLevel2();
            // store it back in configuration
            configuration.setStratumMinimumSampleCountFreeSchoolType(stratumMinimumSampleCountFreeSchoolType);
        }
        // prepare stratum minimum sample count for object school type value
        Integer stratumMinimumSampleCountObjectSchoolType = configuration.getStratumMinimumSampleCountObjectSchoolType();
        if (stratumMinimumSampleCountObjectSchoolType == null) {
            SchoolType schoolType = schoolTypeDAO.forCodeEquals(1).findUnique();
            stratumMinimumSampleCountObjectSchoolType = schoolType.getThresholdNumberLevel2();
            // store it back in configuration
            configuration.setStratumMinimumSampleCountObjectSchoolType(stratumMinimumSampleCountObjectSchoolType);
        }
        SchoolTypeIndeterminate schoolTypeIndeterminate = configuration.getSchoolTypeIndeterminate();
        if (schoolTypeIndeterminate==null) {
            configuration.setSchoolTypeIndeterminate(schoolTypeIndeterminate = SchoolTypeIndeterminate.IGNORE);
        }
        // prepare stratum weight ratio value
        float stratumWeightRatio = configuration.getStratumWeightRatio();
        if (stratumWeightRatio == 0) {
            // get the default value from application configuration
            stratumWeightRatio = getApplicationConfig().getStratumWeightRatio();
            // store it back in configuration
            configuration.setStratumWeightRatio(stratumWeightRatio);
        }
        if (log.isInfoEnabled()) {
            log.info("Selected sample fleet countries    : " + configuration.getSampleFleetIds());
            log.info("Selected sample fleet countries    : " + configuration.getSampleFlagIds());
            log.info("Selected min sample count BL       : " + configuration.getStratumMinimumSampleCountFreeSchoolType());
            log.info("Selected min sample count BO       : " + configuration.getStratumMinimumSampleCountObjectSchoolType());
            log.info("Selected weight ratio              : " + configuration.getStratumWeightRatio());
            log.info("Selected school type indeterminate : " + schoolTypeIndeterminate);
        }
    }

    @Override
    public void validate() {
        Level2Configuration configuration = getConfiguration();
        if (CollectionUtils.isEmpty(configuration.getSampleFlagIds())) {
            addFieldError("configuration.sampleFlagIds", t("t3.error.no.sample.flag.selected"));
        }
        if (CollectionUtils.isEmpty(configuration.getSampleFleetIds())) {
            addFieldError("configuration.sampleFleetIds", t("t3.error.no.sample.fleet.selected"));
        }
        if (configuration.getSchoolTypeIndeterminate() == null) {
            addFieldError("configuration.schoolTypeIndeterminate", t("t3.error.no.schoolTypeIndeterminate.selected"));
        }
    }

    @Override
    public String execute() {
        Level2Configuration configuration = getConfiguration();
        // as the level 2 configuration is now fully valid, store it
        configuration.setValidStep2(true);
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    @SuppressWarnings("unused")
    public boolean isMissingData() {
        return missingData;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getUseWeightCategoriesInStratumOrNot() {
        return useWeightCategoriesInStratumOrNot;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSchoolTypeIndeterminate() {
        return schoolTypeIndeterminate;
    }
}
