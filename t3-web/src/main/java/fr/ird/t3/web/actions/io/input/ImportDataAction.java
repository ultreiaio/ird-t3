/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.input;

import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceAction;
import fr.ird.t3.actions.io.input.ImportInputSourceAction;
import fr.ird.t3.actions.io.input.ImportInputSourceConfiguration;
import fr.ird.t3.actions.io.input.InputSourceConfiguration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.web.actions.AbstractRunAction;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Analyze and import data from input source.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ImportDataAction extends AbstractRunAction<ImportInputSourceConfiguration, ImportInputSourceAction> {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ImportDataAction.class);

    private boolean replaceTrip;
    private boolean valid;
    private int nbImportedTrips;
    private int nbDeletedTrips;
    /**
     * List of all known input providers.
     */
    private List<T3InputProvider> inputProviders;

    public ImportDataAction() {
        super(ImportInputSourceAction.class);
    }

    @Override
    public void prepare() throws Exception {
        super.prepare();
        inputProviders = Collections.singletonList(getIncomingConfiguration().getInputProvider());
    }

    public String prepareImport() {
        // get the action context from analyze
        T3ActionContext<ImportInputSourceConfiguration> actionContext = getT3ActionContext();
        Set<Trip> safeTrips = actionContext.getResultAsSet(AnalyzeInputSourceAction.RESULT_SAFE_TRIPS, Trip.class);
        Map<Trip, Trip> tripToReplace = actionContext.getResultAsMap(AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE);
        log.info("--> Prepare import");
        log.info("Input provider   : " + getIncomingConfiguration().getInputProvider());
        log.info("Input file       : " + getIncomingConfiguration().getInputFile());
        log.info("Use well         : " + getIncomingConfiguration().isUseWells());
        log.info("Trip type        : " + getIncomingConfiguration().getTripType());
        log.info("Can Create vessel: " + getIncomingConfiguration().isCanCreateVessel());
        if (getIncomingConfiguration().isCanCreateVessel()) {
            log.info("Create virtual vessel: " + getIncomingConfiguration().isCreateVirtualVessel());
        }
        log.info("Trips to import  : " + safeTrips.size());
        log.info("Trips to replace : " + tripToReplace.size());
        Set<Trip> toImportTrips = new HashSet<>();
        Set<Trip> toDeleteTrips = new HashSet<>();
        if (isReplaceTrip()) {
            // will replace existing trips, so need to delete before all old trips
            toDeleteTrips.addAll(tripToReplace.keySet());
            // can import all safe trips
            toImportTrips.addAll(safeTrips);
        } else {
            // do NOT replace old trip
            // get all importable trips
            toImportTrips.addAll(safeTrips);
            if (MapUtils.isNotEmpty(tripToReplace)) {
                // remove the one which reflect existing trips
                toImportTrips.removeAll(tripToReplace.values());
            }
        }
        // creates the new configuration
        ImportInputSourceConfiguration configuration = ImportInputSourceConfiguration.newConfiguration(getIncomingConfiguration());
        configuration.setTripsToImport(toImportTrips);
        configuration.setTripsToDelete(toDeleteTrips);
        t3ActionContext = getServiceFactory().newT3ActionContext(configuration, getServiceContext());
        log.info("Created action context " + t3ActionContext);
        getT3Session().setActionContext(t3ActionContext);
        return SUCCESS;
    }

    public String prepareResult() {
        Set<Trip> importedTrips = getConfiguration().getTripsToImport();
        nbImportedTrips = importedTrips.size();
        Set<Trip> deletedTrips = getConfiguration().getTripsToDelete();
        nbDeletedTrips = deletedTrips.size();
        return INPUT;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(ImportInputSourceAction action, Exception error, Date startDate, Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action, error, startDate, endDate);
        map.put("tripDecorator", getDecorator(Trip.class, DecoratorService.WITH_ID));
        return map;
    }

    @SuppressWarnings("unused")
    public List<T3InputProvider> getInputProviders() {
        return inputProviders;
    }

    @SuppressWarnings("WeakerAccess")
    public InputSourceConfiguration getIncomingConfiguration() {
        Object configuration = getT3ActionContext().getConfiguration();
        return (InputSourceConfiguration) configuration;
    }

    @SuppressWarnings("unused")
    public boolean isValid() {
        return valid;
    }

    @SuppressWarnings("unused")
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @SuppressWarnings("unused")
    public int getNbImportedTrips() {
        return nbImportedTrips;
    }

    @SuppressWarnings("unused")
    public int getNbDeletedTrips() {
        return nbDeletedTrips;
    }

    @SuppressWarnings("WeakerAccess")
    public boolean isReplaceTrip() {
        return replaceTrip;
    }

    @SuppressWarnings("unused")
    public void setReplaceTrip(boolean replaceTrip) {
        this.replaceTrip = replaceTrip;
    }
}
