/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level3;

import com.google.common.collect.Maps;
import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryTopiaDao;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.cache.LengthWeightConversionWithContextCache;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanTopiaDao;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesTopiaDao;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Manage level 3 action configuration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ConfigureLevel3Step1Action extends AbstractConfigureAction<Level3Configuration> {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ConfigureLevel3Step1Action.class);
    private final Map<String, String> timeSteps = createTimeSteps();
    @InjectDAO(entityType = Species.class)
    private transient SpeciesTopiaDao specieDAO;
    @InjectDAO(entityType = Ocean.class)
    private transient OceanTopiaDao oceanDAO;
    @InjectDAO(entityType = Country.class)
    private transient CountryTopiaDao countryDAO;
    @InjectDAO(entityType = Trip.class)
    private transient TripTopiaDao tripDAO;
    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> catchFleets;
    @InjectDecoratedBeans(beanType = ZoneStratumAwareMeta.class)
    private Map<String, String> zoneTypes;
    @InjectDecoratedBeans(beanType = ZoneVersion.class)
    private Map<String, String> zoneVersions;
    @InjectDecoratedBeans(beanType = Species.class)
    private Map<String, String> species;
    @InjectDecoratedBeans(beanType = Ocean.class)
    private Map<String, String> oceans;
    /**
     * When validating configuration.
     */
    private boolean validating;
    /**
     * Flag to know if some data are missing.
     * <p/>
     * This flag is set in the {@link #prepare()} method while loading possibles data.
     */
    private boolean missingData;

    public ConfigureLevel3Step1Action() {
        super(Level3Configuration.class);
    }

    @Override
    public void prepare() throws Exception {
        boolean configurationInSession = isConfigurationInSession();
        Level3Configuration conf = getConfiguration();
        injectExcept(InjectDecoratedBeans.class);
        // assume data are available
        missingData = false;
        // invalidate step 1 configuration
        conf.setValidStep1(false);
        // invalidate step 2 configuration
        conf.setValidStep2(false);
        if (!configurationInSession) {
            conf.setCatchFleets(sortToList(countryDAO.findAllFleetUsedInCatch()));
            conf.setOceans(sortToList(oceanDAO.findAllUsedInActivity()));
            conf.setSpecies(sortToList(specieDAO.findAllSpeciesUsedInCatch()));
            conf.setZoneTypes(sortToList(getZoneStratumService().getZoneStratumAwareMetas()));
        }
        if (StringUtils.isNotEmpty(conf.getZoneTypeId())) {
            // let's fill the zone versions for the selected zone type
            ZoneStratumAwareMeta zoneType = getZoneStratumService().getZoneMetaById(conf.getZoneTypeId());
            conf.setZoneVersions(sortToList(zoneType.getAllZoneVersions(getT3TopiaPersistenceContext().get())));
            if (CollectionUtils.isEmpty(conf.getZoneVersions())) {
                addFieldError("configuration.zoneVersionId", t("t3.error.no.zoneVersion.found"));
                // need some data
                missingData = true;
            }
        }
        // let's inject decorated values
        injectOnly(InjectDecoratedBeans.class);
        if (CollectionUtils.isEmpty(conf.getCatchFleets())) {
            addFieldError("configuration.catchFleetIds", t("t3.error.no.catch.fleet.found"));
            missingData = true;
        }
        if (CollectionUtils.isEmpty(conf.getOceans())) {
            addFieldError("configuration.oceanIds", t("t3.error.no.ocean.found"));
            missingData = true;
        }
        if (CollectionUtils.isEmpty(conf.getSpecies())) {
            addFieldError("configuration.speciesIds", t("t3.error.no.species.found"));
            missingData = true;
        }
        if (CollectionUtils.isEmpty(conf.getZoneTypes())) {
            addFieldError("configuration.zoneTypeId", t("t3.error.no.zoneType.found"));
            missingData = true;
        }
        if (!isValidating() && !configurationInSession) {
            List<Integer> level3DefaultSpecies = getApplicationConfig().getLevel3DefaultSpeciesAsList();
            // keep default species of level 3
            for (Species aSpecies : conf.getSpecies()) {
                if (level3DefaultSpecies.contains(aSpecies.getCode())) {
                    conf.getSpeciesIds().add(aSpecies.getTopiaId());
                }
            }
            // by default time step is 3 months
            conf.setTimeStep(3);
            // use first and last landing date
            T3Date firstLandingDate = tripDAO.getFirstLandingDate(null);
            T3Date lastLandingDate = tripDAO.getLastLandingDate(null);
            conf.setMinDate(firstLandingDate);
            conf.setBeginDate(firstLandingDate);
            conf.setMaxDate(lastLandingDate);
            conf.setEndDate(lastLandingDate);
            // let's put the configuration is session, we don't want to redo all queries for validation...
            getT3Session().setActionConfiguration(conf);
        }
        if (log.isInfoEnabled()) {
            log.info("Selected species                 : " + conf.getSpeciesIds());
            log.info("Selected catch fleets            : " + conf.getCatchFleetIds());
            log.info("Selected ocean                   : " + conf.getOceanIds());
            log.info("Selected begin date              : " + conf.getBeginDate());
            log.info("Selected end date                : " + conf.getEndDate());
            log.info("Selected time step               : " + conf.getTimeStep());
            log.info("Selected zone type               : " + conf.getZoneTypeId());
            log.info("Selected zone version            : " + conf.getZoneVersionId());
        }
    }

    @Override
    public void validate() {
        Level3Configuration config = getConfiguration();
        boolean oceanOk = true;
        if (CollectionUtils.isEmpty(config.getOceanIds())) {
            addFieldError("configuration.oceanIds", t("t3.error.no.ocean.selected"));
            oceanOk = false;
        }
        if (StringUtils.isEmpty(config.getZoneTypeId())) {
            addFieldError("configuration.zoneTypeId", t("t3.error.no.zoneType.selected"));
        }
        if (StringUtils.isEmpty(config.getZoneVersionId())) {
            addFieldError("zoneVersionId", t("t3.error.no.zoneVersion.selected"));
        }
        if (CollectionUtils.isEmpty(config.getCatchFleetIds())) {
            addFieldError("configuration.catchFleetIds", t("t3.error.no.catch.fleet.selected"));
        }
        T3Date beginDate = config.getBeginDate();
        Set<String> speciesIds = config.getSpeciesIds();
        boolean speciesOk = true;
        if (CollectionUtils.isEmpty(speciesIds)) {
            addFieldError("configuration.speciesIds", t("t3.error.no.species.selected"));
            speciesOk = false;
        }
        boolean beginDateOk = true;
        if (beginDate == null) {
            addFieldError("configuration.beginDate", t("t3.error.no.beginDate.selected"));
            beginDateOk = false;
        }
        T3Date endDate = config.getEndDate();
        if (endDate == null) {
            addFieldError("configuration.endDate", t("t3.error.no.endDate.selected"));
        }
        if (beginDate != null && endDate != null) {
            if (beginDate.equals(endDate) || beginDate.after(endDate)) {
                // begin date must be strictly before end date
                addFieldError("configuration.beginDate", t("t3.error.beginDate.equals.or.after.endDate"));
            } else {
                // check number of mouths are a multiple of timeStep
                int timeStep = config.getTimeStep();
                if (!beginDate.isModuloMonths(endDate, timeStep)) {
                    addFieldError("configuration.endDate", t("t3.error.endDate.no.modulo.timestep"));
                }
            }
        }
        if (oceanOk && beginDateOk && speciesOk) {
            // check that for each species we have what we need
            List<Species> allSpecies = config.getSpecies();
            Map<String, Species> speciesById = Maps.uniqueIndex(allSpecies, Species::getTopiaId);
            for (String speciesId : speciesIds) {
                Species aSpecies = speciesById.get(speciesId);
                if (aSpecies.getThresholdNumberLevel3FreeSchoolType() == null) {
                    // no threshold for free school type
                    addFieldError("configuration.speciesIds",
                            t("t3.error.no.thresholdNumberLevel3FreeSchoolType.for.species", decorate(aSpecies)));
                }
                if (aSpecies.getThresholdNumberLevel3ObjectSchoolType() == null) {
                    // no threshold for object school type
                    addFieldError("configuration.speciesIds", t("t3.error.no.thresholdNumberLevel3ObjectSchoolType.for.species", decorate(aSpecies)));
                }
            }
            Date toBeginDate = configuration.getBeginDate().toBeginDate();
            for (String oceanId : config.getOceanIds()) {
                Ocean ocean = oceanDAO.forTopiaIdEquals(oceanId).findUnique();
                LengthWeightConversionWithContextCache conversionHelper = getT3TopiaPersistenceContext().get().newLengthWeightConversionWithContextCache(ocean, toBeginDate);
                for (String speciesId : config.getSpeciesIds()) {
                    Species aSpecies = speciesById.get(speciesId);
                    LengthWeightConversion conversions = conversionHelper.getConversions(aSpecies);
                    if (conversions == null) {
                        addFieldError("configuration.speciesIds", t("t3.error.no.converter.for.species", decorate(aSpecies), decorate(ocean)));
                    }
                }
            }
        }
    }

    @Override
    public String execute() {
        Level3Configuration config = getConfiguration();
        config.setValidStep1(true);
        storeActionConfiguration(config);
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public String getZoneVersionId() {
        return getConfiguration().getZoneVersionId();
    }

    @SuppressWarnings("unused")
    public void setZoneVersionId(String zoneVersionId) {
        getConfiguration().setZoneVersionId(zoneVersionId);
    }

    @SuppressWarnings("unused")
    public Map<String, String> getCatchFleets() {
        return catchFleets;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getOceans() {
        return oceans;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getZoneTypes() {
        return zoneTypes;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getZoneVersions() {
        return zoneVersions;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getSpecies() {
        return species;
    }

    @SuppressWarnings("unused")
    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public boolean isValidating() {
        return validating;
    }

    @SuppressWarnings("unused")
    public void setValidating(boolean validating) {
        this.validating = validating;
    }

    @SuppressWarnings("unused")
    public boolean isMissingData() {
        return missingData;
    }

    @SuppressWarnings("unused")
    public void setMissingData(boolean missingData) {
        this.missingData = missingData;
    }
}
