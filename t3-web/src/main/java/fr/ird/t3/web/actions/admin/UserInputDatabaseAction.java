/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collection;

/**
 * Operations of {@link UserDatabase}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class UserInputDatabaseAction extends AbstractUserDatabaseAction {

    private static final long serialVersionUID = -1L;

    @Override
    public String getDatabaseType() {
        return INPUT_DATABASE;
    }

    @Override
    protected UserDatabase getDatabase(String id) throws TopiaException {
        return getUserService().getUserInputDatabase(id);
    }

    @Override
    protected void create(String userId, UserDatabase database) {
        getUserService().addUserInputDatabase(userId, database);
    }

    @Override
    protected void update(UserDatabase database) {
        getUserService().updateUserInputDatabase(database);
    }

    @Override
    protected void delete(String userId, UserDatabase database) {
        getUserService().removeUserInputDatabase(userId, database.getId());
    }

    @Override
    public UserDatabase getDatabaseConfiguration() {
        if (databaseConfiguration == null) {
            databaseConfiguration = new UserDatabase();
        }
        return databaseConfiguration;
    }

    @Override
    protected Collection<UserDatabase> getDatabases(T3User user) {
        return user.getInputs();
    }
}
