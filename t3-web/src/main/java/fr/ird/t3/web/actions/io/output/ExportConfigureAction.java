/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.output;

import fr.ird.t3.actions.io.output.ExportConfiguration;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryTopiaDao;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanTopiaDao;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselTopiaDao;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.io.output.T3OutputOperation;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * To configure first step of export action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ExportConfigureAction extends AbstractConfigureAction<ExportConfiguration> {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ExportConfigureAction.class);

    @InjectDAO(entityType = Trip.class)
    private transient TripTopiaDao tripDAO;

    @InjectDAO(entityType = Vessel.class)
    private transient VesselTopiaDao vesselDAO;

    @InjectDAO(entityType = Ocean.class)
    private transient OceanTopiaDao oceanDAO;

    @InjectDAO(entityType = Country.class)
    private transient CountryTopiaDao countryDAO;

    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> fleets;

    @InjectDecoratedBeans(beanType = Ocean.class)
    private Map<String, String> oceans;

    /** List of all known output providers. */
    private List<T3OutputProvider<?, ?>> outputProviders;

    private Map<String, String> operations;

    private String databaseId;

    private Map<String, String> databases;

    public ExportConfigureAction() {
        super(ExportConfiguration.class);
    }

    @Override
    public void prepare() throws Exception {

        T3User user = getUserService().getUserById(getT3Session().getUser().getId());

        Collection<UserDatabase> dbs = user.getOutputs();

        if (CollectionUtils.isEmpty(dbs)) {
            addActionMessage(t("t3.message.no.output.database.registred"));
            databases = new HashMap<>();
        } else {

            databases = sortAndDecorateIdAbles(dbs);
        }

        // get all available output providers
        outputProviders = Arrays.asList(getT3OutputService().getProviders());

        // make sur configuration is inited before all
        getConfiguration();

        operations = new LinkedHashMap<>();

        for (T3OutputOperation operation : getT3OutputService().getOperations(configuration.getOutputProviderId())) {
            operations.put(operation.getId(), operation.getLibelle(getLocale()));
        }

        // inject everything needed (dao, ...)
        injectExcept(InjectDecoratedBeans.class);

        boolean configurationInSession = isConfigurationInSession();

        ExportConfiguration conf = getConfiguration();

        if (!configurationInSession) {

            // grab data and store once for all in configuration

            T3Date beginDate = tripDAO.getFirstLandingDate(null);
            conf.setMinDate(beginDate);
            conf.setBeginDate(beginDate);

            T3Date endDate = tripDAO.getLastLandingDate(null);
            conf.setMaxDate(endDate);
            conf.setEndDate(endDate);

            conf.setFleets(sortToList(countryDAO.findAllFleetUsedInTrip(null)));

            conf.setOceans(sortToList(oceanDAO.findAllUsedInActivity()));

        }

        if (CollectionUtils.isEmpty(conf.getFleets())) {
            addFieldError("configuration.fleetId", t("t3.error.no.fleet"));
        }

        if (CollectionUtils.isEmpty(conf.getOceans())) {
            addFieldError("configuration.oceanId", t("t3.error.no.ocean"));
        }

        if (!configurationInSession && !hasFieldErrors()) {

            // store configuration in session
            getT3Session().setActionConfiguration(conf);
        }

        if (log.isInfoEnabled()) {
            log.info("Selected oceans     : " + conf.getOceanId());
            log.info("Selected fleet      : " + conf.getFleetId());
            log.info("Selected begin date : " + conf.getBeginDate());
            log.info("Selected end date   : " + conf.getEndDate());
            log.info("Selected pilot      : " + conf.getOutputProviderId());
            log.info("Selected operations : " + conf.getOperationIds());
        }

        injectOnly(InjectDecoratedBeans.class);
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public Map<String, String> getDatabases() {
        return databases;
    }

    public List<String> getOperationIds() {
        List<String> operationIds = getConfiguration().getOperationIds();
        if (operationIds == null) {
            setOperationIds(operationIds = new ArrayList<>());
        }
        return operationIds;
    }

    public void setOperationIds(List<String> operationIds) {
        getConfiguration().setOperationIds(operationIds);
    }

    @Override
    public void validate() {
        log.info("Will validate...");
        ExportConfiguration conf = getConfiguration();
        boolean jdbcError = false;
        if (StringUtils.isEmpty(conf.getOutputProviderId())) {
            addFieldError("configuration.outputProviderId", t("t3.error.no.output.pilot.selected"));
        }
        if (CollectionUtils.isEmpty(conf.getOperationIds())) {
            addFieldError("operationIds", t("t3.error.no.operation.selected"));
        }
        if (StringUtils.isEmpty(conf.getOceanId())) {
            addFieldError("configuration.oceanId", t("t3.error.no.ocean.selected"));
        }
        if (StringUtils.isEmpty(conf.getFleetId())) {
            addFieldError("configuration.fleetId", t("t3.error.no.fleet.selected"));
        }
        if (StringUtils.isEmpty(conf.getUrl())) {
            addFieldError("configuration.url", t("t3.error.required.url"));
            jdbcError = true;
        }
        if (StringUtils.isEmpty(conf.getLogin())) {
            addFieldError("configuration.login", t("t3.error.required.login"));
            jdbcError = true;
        }
        if (StringUtils.isEmpty(conf.getPassword())) {
            addFieldError("configuration.password", t("t3.error.required.password"));
            jdbcError = true;
        }
        T3Date beginDate = conf.getBeginDate();
        if (beginDate == null) {
            addFieldError("configuration.beginDate", t("t3.error.no.beginDate.selected"));
        }
        T3Date endDate = conf.getEndDate();
        if (endDate == null) {
            addFieldError("configuration.endDate", t("t3.error.no.endDate.selected"));
        }
        if (beginDate != null && endDate != null) {
            if (beginDate.equals(endDate) || beginDate.after(endDate)) {
                // begin date must be strictly before end date
                addFieldError("configuration.beginDate", t("t3.error.beginDate.equals.or.after.endDate"));
            }
        }
        if (!jdbcError) {
            // check jdbc connection
            try {
                T3EntityHelper.checkJDBCConnection(conf.getOutputConfiguration());
            } catch (Exception e) {
                // can not connect to database
                addFieldError("configuration.url", t("t3.error.invalid.jdbc.connexion", e.getMessage()));
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public final String execute() {
        String id = getConfiguration().getOutputProviderId();
        T3OutputProvider t3OutputProvider = getT3OutputService().getProvider(id);
        getConfiguration().setOutputProvider(t3OutputProvider);
        prepareActionContext();
        return SUCCESS;
    }

    public List<T3OutputProvider<?, ?>> getOutputProviders() {
        return outputProviders;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Map<String, String> getOperations() {
        if (MapUtils.isEmpty(operations)) {
            // try to load it from configuration
            String id = getConfiguration().getOutputProviderId();
            if (id != null && CollectionUtils.isNotEmpty(getOperationIds())) {
                //  reload operations
                for (T3OutputOperation operation : getT3OutputService().getOperations(id)) {
                    operations.put(operation.getId(), operation.getLibelle(getLocale()));
                }
            }
        }
        return operations;
    }

}
