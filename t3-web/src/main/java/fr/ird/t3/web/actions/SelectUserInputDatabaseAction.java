/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Database to select and connect to a t3 database.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class SelectUserInputDatabaseAction extends T3ActionSupport implements Preparable {

    protected static final Log log = LogFactory.getLog(SelectUserInputDatabaseAction.class);

    private static final long serialVersionUID = 1L;

    private String databaseId;
    private JdbcConfiguration database;
    private Map<String, String> databases;

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public JdbcConfiguration getDatabase() {
        if (database == null) {
            database = new JdbcConfiguration();
        }
        return database;
    }

    public Map<String, String> getDatabases() {
        return databases;
    }

    @Override
    public void prepare() {
        T3User user = getT3Session().getUser();
        user = getUserService().getUserById(user.getId());
        Collection<UserDatabase> dbs = user.getInputs();
        if (CollectionUtils.isEmpty(dbs)) {
            databases = new HashMap<>();
            addActionMessage(t("t3.message.no.t3.database.registred"));
        } else {
            databases = sortAndDecorateIdAbles(dbs);
        }
    }

    @Override
    public String execute() {
        // set the selected database in session
        try {
            T3EntityHelper.checkJDBCConnection(getDatabase());
        } catch (SQLException e) {
            addActionError(t("t3.message.error.open.database", getDatabase().getUrl(), e.getMessage()));
            return ERROR;
        }
        getT3Session().initDatabaseConfiguration(getDatabase());
        return SUCCESS;
    }
}
