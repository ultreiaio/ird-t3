/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import fr.ird.t3.actions.data.level1.ComputeWeightOfCategoriesForSetAction;
import fr.ird.t3.actions.data.level1.Level1Step;

/**
 * Launch the level 1 action {@link ComputeWeightOfCategoriesForSetAction}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ComputeWeightOfCategoriesForSetRunAction extends AbstractLevel1RunAction<ComputeWeightOfCategoriesForSetAction> {

    private static final long serialVersionUID = 1L;

    public ComputeWeightOfCategoriesForSetRunAction() {
        super(ComputeWeightOfCategoriesForSetAction.class, Level1Step.COMPUTE_WEIGHT_OF_CATEGORIES_FOR_SET);
    }
}
