/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level1;

import com.google.common.collect.Multimap;
import fr.ird.t3.actions.data.level1.Level1Configuration;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripTopiaDao;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryTopiaDao;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanTopiaDao;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleQualityTopiaDao;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.entities.reference.SampleTypeTopiaDao;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.services.ioc.InjectFromDAO;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.util.TopiaEntityHelper;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Action to manage the configuration of level 1.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ManageLevel1ConfigurationAction extends AbstractConfigureAction<Level1Configuration> {

    private static final long serialVersionUID = 1L;

    @InjectDecoratedBeans(beanType = SampleQuality.class)
    private Map<String, String> sampleQualities;

    @InjectDecoratedBeans(beanType = SampleType.class)
    private Map<String, String> sampleTypes;

    @InjectDecoratedBeans(beanType = Country.class)
    private Map<String, String> fleets;

    @InjectDecoratedBeans(beanType = Ocean.class)
    private Map<String, String> oceans;

    @InjectFromDAO(entityType = Trip.class, method = "findAllForLevel1")
    private Collection<Trip> trips;

    private Map<String, Trip> tripByTopiaIds;

    private Map<String, String> useRfMinus10AndRfPlus10OrNot;

    public ManageLevel1ConfigurationAction() {
        super(Level1Configuration.class);
    }

    @Override
    public void prepare() throws Exception {

        useRfMinus10AndRfPlus10OrNot = createUseRfMinus10AndRfPlus10OrNot();

        // always invalidate configuration status
        setConfirm(false);

        boolean configExists = isConfigurationInSession();

        Level1Configuration conf = getConfiguration();

        injectExcept(InjectDecoratedBeans.class);

        tripByTopiaIds = T3EntityHelper.splitByTopiaId(trips);

        if (!configExists) {

            MutablePair<Date, Date> landingBound =
                    TripTopiaDao.getActivityBoundDate(trips);

            Date firstDate = landingBound.getLeft();
            Date lastDate = landingBound.getRight();

            if (firstDate != null) {
                conf.setMinDate(T3Date.newDate(firstDate));
                conf.setBeginDate(T3Date.newDate(firstDate));
            }
            if (lastDate != null) {
                conf.setMaxDate(T3Date.newDate(lastDate));
                conf.setEndDate(T3Date.newDate(lastDate));
            }

            conf.setSampleQualities(sortToList(SampleQualityTopiaDao.getAllSampleQualities(trips)));
            conf.setSampleTypes(sortToList(SampleTypeTopiaDao.getAllSampleTypes(trips)));
            conf.setFleets(sortToList(CountryTopiaDao.getAllFleetCountries(trips)));
            conf.setOceans(sortToList(OceanTopiaDao.getAllOcean(trips)));
//            if (StringUtils.isEmpty(conf.getOceanId()) &&
//                CollectionUtils.isNotEmpty(conf.getOceans())) {
//                conf.setOceanId(conf.getOceans().get(0).getTopiaId());
//            }

            // set ids list to empty lists
            List<String> ids;

            // use default sample qualities
            ids = T3EntityHelper.selectIdsByCodes(conf.getSampleQualities(), 1);
            conf.setSampleQualityIds(ids);

            // use default sample types
            ids = T3EntityHelper.selectIdsByCodes(conf.getSampleTypes(), 1, 2, 3, 9);
            conf.setSampleTypeIds(ids);

            // use all fleets by default
            ids = TopiaEntityHelper.getTopiaIdList(conf.getFleets());
            conf.setFleetIds(ids);

            // default rftotMax
            conf.setRfTotMax(getApplicationConfig().getRfTotMax().intValue());

            // default rfMinus10Max
            conf.setRfMinus10Max(getApplicationConfig().getRfMinus10Max().intValue());

            // default rfPlus10Max
            conf.setRfPlus10Max(getApplicationConfig().getRfPlus10Max().intValue());

            // default rfMinus10MinNumber
            conf.setRfMinus10MinNumber(getApplicationConfig().getRfMinus10MinNumber());

            // default rfPlus10MinNumber
            conf.setRfPlus10MinNumber(getApplicationConfig().getRfPlus10MinNumber());

        }

        injectOnly(InjectDecoratedBeans.class);

        DecoratorService decoratorService = newService(DecoratorService.class);
        Multimap<String, String> matchingTrips = conf.getMatchingTrips(trips, getLocale(), decoratorService);
        conf.setSampleIdsByTripId(matchingTrips);
    }

    @Override
    public void validate() {
        if (CollectionUtils.isEmpty(trips)) {

            // no matching trips
            addFieldError("matchingTripCount",t("t3.error.level1.no.matching.trips"));
        }
    }

    public final String prepareConfiguration() {

        if (!isConfigurationInSession() && !hasFieldErrors()) {

            // store configuration in session
            storeActionConfiguration(configuration);
        }
        return INPUT;
    }

    public String saveConfiguration() {
        Level1Configuration config = getConfiguration();

        // each time configuration is modified, reset executed steps for
        // this configuration
        config.getExecutedSteps().clear();

        storeActionConfiguration(config);

        // configuration was successfull validated
        setConfirm(true);

        return SUCCESS;
    }

    @SkipValidation
    public String removeConfiguration() {

        boolean saved = isConfigurationInSession();
        if (saved) {

            removeConfigurationFromSession();
        }
        return SUCCESS;
    }

    public int getMatchingTripCount() {
        Map<String, Collection<String>> tripIds = getTripIds();
        return MapUtils.isEmpty(tripIds) ? 0 : tripIds.size();
    }

    public int getMatchingSampleCount() {
        Map<String, Collection<String>> tripIds = getTripIds();

        if (MapUtils.isEmpty(tripIds)) {
            return 0;
        }
        int nbSamples = 0;
        for (Collection<String> sampleIds : tripIds.values()) {
            nbSamples += sampleIds.size();
        }
        return nbSamples;
    }

    public int getNotMatchingSampleCount() {
        return getConfiguration().getNotTreatedSampleReason().size();
    }

    public Map<String, Collection<String>> getTripIds() {
        return getConfiguration().getSampleIdsByTripId().asMap();
    }

    public Map<String, String> getSampleQualities() {
        return sampleQualities;
    }

    public Map<String, String> getSampleTypes() {
        return sampleTypes;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Decorator<Trip> getTripDecorator() {
        return getDecorator(Trip.class);
    }

    public Map<String, Trip> getTripByTopiaIds() {
        return tripByTopiaIds;
    }

    public Map<String, String> getUseRfMinus10AndRfPlus10OrNot() {
        return useRfMinus10AndRfPlus10OrNot;
    }

}
