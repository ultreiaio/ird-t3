/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level0;

import fr.ird.t3.actions.data.level0.ConvertCatchesWeightCategoriesConfiguration;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversion;
import fr.ird.t3.entities.reference.WeightCategoryLogBookConversionTopiaDao;
import fr.ird.t3.services.ioc.InjectDAO;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Compute Raising factor 1 for some selected trips.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ConvertCatchesWeightCategoriesConfigureAction extends AbstractLevel0ConfigureAction<ConvertCatchesWeightCategoriesConfiguration> {

    private static final long serialVersionUID = 1L;

    @InjectDAO(entityType = WeightCategoryLogBookConversion.class)
    private transient WeightCategoryLogBookConversionTopiaDao dao;

    public ConvertCatchesWeightCategoriesConfigureAction() {
        super(ConvertCatchesWeightCategoriesConfiguration.class);
    }

    @Override
    protected void loadDefaultConfiguration(ConvertCatchesWeightCategoriesConfiguration config) {
        super.loadDefaultConfiguration(config);
        List<String> versions = dao.findAll().stream().map(WeightCategoryLogBookConversion::getVersion).distinct().collect(Collectors.toCollection(LinkedList::new));
        if (versions.isEmpty()) {
            throw new IllegalStateException("can't find a version of weight category logBook conversion in database");
        }
        String defaultVersion = versions.get(0);
        versions.add(ConvertCatchesWeightCategoriesConfiguration.LEGACY_VERSION);
        config.setVersions(versions);
        config.setVersion(defaultVersion);
    }
}
