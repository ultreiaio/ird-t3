/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.input;

import com.opensymphony.xwork2.inject.Inject;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceConfiguration;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsConstants;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Action to import data fro input sources.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ConfigureImportDataAction extends AbstractConfigureAction<AnalyzeInputSourceConfiguration> {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ConfigureImportDataAction.class);

    /**
     * Input file that was just uploaded on server.
     */
    private File sourceToLoad;
    /**
     * Name of file to upload.
     */
    private String sourceToLoadFileName;
    /**
     * Flag to use strict mode with wells.
     */
    private boolean useWells;
    /**
     * flag to authorize creation of missing vessels.
     *
     * @since 1.3.1
     */
    private boolean canCreateVessel;
    /**
     * flag to create or not virtual vessels.
     *
     * @since 1.3.1
     */
    private boolean createVirtualVessel;
    /**
     * Flag to load only samples only trips
     *
     * @see TripType#SAMPLEONLY
     */
    private boolean useSamplesOnly;
    /**
     * Flag to authorize virtual activity creation (when logbooks are missing).
     * <p>
     * By default, let's always authorize it.
     * <p>
     * <b>Note:</b> This option is always at false if {@link #useSamplesOnly} is on.
     *
     * @see TripType#LOGBOOKMISSING
     * @see TripType#STANDARD
     */
    private boolean canCreateVirtualActivity;

    private long maxSize;
    /** List of all known input providers. */
    private List<T3InputProvider> inputProviders;
    /** Where to keep data for treatment */
    private String treatmentDirectoryPath;
    /** Name of uploaded source */
    private String loadedSource;
    /** Id of selected InputProvider */
    private String inputProviderId;

    public ConfigureImportDataAction() {
        super(AnalyzeInputSourceConfiguration.class);
    }

    @Override
    public void prepare() throws Exception {
        injectOnly(InjectDAO.class);
        if (!isConfigurationInSession()) {
            // no yet configuration, can use all input providers
            inputProviders = Arrays.asList(getT3InputService().getProviders());
        } else {
            // as soon as there is a configuration, can only use the inputProvider from it
            inputProviders = Collections.singletonList(getConfiguration().getInputProvider());
        }
        injectOnly(InjectDecoratedBeans.class);
        if (getTreatmentDirectoryPath() == null) {
            // first time coming here get a new treatment directory path
            File workingDirectory = getApplicationConfig().getTreatmentWorkingDirectory();
            long l = System.nanoTime();
            File treatmentDirectory = new File(workingDirectory, "importData-" + l);
            FileUtil.createDirectoryIfNecessary(treatmentDirectory);
            log.info(String.format("Create a new treatment directory path : %s", treatmentDirectory));
            setTreatmentDirectoryPath(treatmentDirectory.getAbsolutePath());
        } else {
            log.info(String.format("Use existing treatment directory %s", getTreatmentDirectoryPath()));
        }
        if (getConfiguration().getInputProvider() == null) {
            inputProviderId = getApplicationConfig().getDefaultInputPilotId();
        }
    }

    public String doAddSource() throws Exception {
        // this action execution purpose is to add a new source to the treatment configuration
        // It will upload the file and place it the correct place (says the temporary directory of the treatment configuration)
        File upload = getSourceToLoad();
        if (upload == null) {
            String message = t("t3.error.required.file.to.upload");
            addFieldError("sourceToLoad", message);
            log.error(message);
            return ERROR;
        }
        // treatment directory
        File targetDirectory = getTreatmentDirectory();
        String filename = getSourceToLoadFileName();
        if (ZipUtil.isZipFile(upload)) {
            // let's decompress input stream it (always in iso 8869_1)
            ZipFile zipfile = new ZipFile(upload, StandardCharsets.ISO_8859_1);
            Enumeration<? extends ZipEntry> entries = zipfile.entries();
            if (!entries.hasMoreElements()) {
                String message = t("t3.error.required.one.entry.in.zip.to.upload");
                addFieldError("sourceToLoad", message);
                log.error(message);
                return ERROR;
            }
            // get first entry
            ZipEntry zipEntry = entries.nextElement();
            // keep the filename of the zip entry
            filename = new String(zipEntry.getName().getBytes(),StandardCharsets.ISO_8859_1);
            File target = new File(targetDirectory, filename);
            log.info(String.format("Will copy loaded zipped entry file %s to treatment configuration directory %s", filename, target));
            try (InputStream in = zipfile.getInputStream(zipEntry)) {
                FileUtils.copyInputStreamToFile(in, target);
            }
        } else {
            // target file
            File target = new File(targetDirectory, filename);
            log.info(String.format("Will copy loaded file %s to treatment configuration directory %s", upload, target));
            // just copy file
            FileUtils.copyFile(upload, target);
        }
        // add the new file to the sources
        setLoadedSource(filename);
        // go back to the configuration definition
        return INPUT;
    }

    public String doDeleteSource() throws Exception {
        String filename = getLoadedSource();
        File targetDirectory = getTreatmentDirectory();
        log.info(String.format("Will delete loaded file %s from %s", filename, targetDirectory));
        File f = new File(targetDirectory, filename);
        if (f.exists()) {
            boolean delete = f.delete();
            if (!delete) {
                throw new IOException(String.format("Could not delete file %s on server.", f));
            }
        }
        setLoadedSource(null);
        // go back to the configuration definition
        return INPUT;
    }

    public String doPrepareAnalyze() {
        T3InputProvider inputProvider = getT3InputService().getProvider(getInputProviderId());
        File path = getTreatmentDirectory();
        String name = getLoadedSource();
        File inputFile = new File(path, name);
        TripType tripType = TripType.getTripType(useSamplesOnly, canCreateVirtualActivity);
        log.info(String.format("input provider           : %s", inputProvider));
        log.info(String.format("input file               : %s", inputFile));
        log.info(String.format("userWells                : %s", useWells));
        log.info(String.format("tripType                 : %s", tripType));
        log.info(String.format("canCreateVessel          : %s", canCreateVessel));
        log.info(String.format("createVirtualVessel      : %s", createVirtualVessel));
        log.info(String.format("canCreateVirtualActivity : %s", canCreateVirtualActivity));
        log.info(String.format("useSamplesOnly           : %s", useSamplesOnly));
        configuration = AnalyzeInputSourceConfiguration.newConfiguration(inputProvider, inputFile, useWells, canCreateVessel, createVirtualVessel, useSamplesOnly, canCreateVirtualActivity);
        prepareActionContext();
        return SUCCESS;
    }

    @SuppressWarnings("unused")
    public String getMaxSize() {
        return StringUtil.convertMemory(maxSize);
    }

    @SuppressWarnings("unused")
    @Inject(StrutsConstants.STRUTS_MULTIPART_MAXSIZE)
    public void setMaxSize(String maxSize) {
        this.maxSize = Long.parseLong(maxSize);
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public File getSourceToLoad() {
        return sourceToLoad;
    }

    @SuppressWarnings("unused")
    public void setSourceToLoad(File sourceToLoad) {
        this.sourceToLoad = sourceToLoad;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getSourceToLoadFileName() {
        return sourceToLoadFileName;
    }

    @SuppressWarnings("unused")
    public void setSourceToLoadFileName(String sourceToLoadFileName) {
        this.sourceToLoadFileName = sourceToLoadFileName;
    }

    @SuppressWarnings("unused")
    public boolean isUseWells() {
        return useWells;
    }

    @SuppressWarnings("unused")
    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    @SuppressWarnings("unused")
    public boolean isCanCreateVessel() {
        return canCreateVessel;
    }

    @SuppressWarnings("unused")
    public void setCanCreateVessel(boolean canCreateVessel) {
        this.canCreateVessel = canCreateVessel;
    }

    @SuppressWarnings("unused")
    public boolean isCreateVirtualVessel() {
        return createVirtualVessel;
    }

    @SuppressWarnings("unused")
    public void setCreateVirtualVessel(boolean createVirtualVessel) {
        this.createVirtualVessel = createVirtualVessel;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getInputProviderId() {
        return inputProviderId;
    }

    @SuppressWarnings("unused")
    public void setInputProviderId(String inputProviderId) {
        this.inputProviderId = inputProviderId;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getLoadedSource() {
        return loadedSource;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setLoadedSource(String loadedSource) {
        this.loadedSource = loadedSource;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getTreatmentDirectoryPath() {
        return treatmentDirectoryPath;
    }

    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setTreatmentDirectoryPath(String treatmentDirectoryPath) {
        this.treatmentDirectoryPath = treatmentDirectoryPath;
    }

    @SuppressWarnings("unused")
    public boolean isUseSamplesOnly() {
        return useSamplesOnly;
    }

    @SuppressWarnings("unused")
    public void setUseSamplesOnly(boolean useSamplesOnly) {
        this.useSamplesOnly = useSamplesOnly;
    }

    @SuppressWarnings("unused")
    public boolean isCanCreateVirtualActivity() {
        return canCreateVirtualActivity;
    }

    @SuppressWarnings("unused")
    public void setCanCreateVirtualActivity(boolean canCreateVirtualActivity) {
        this.canCreateVirtualActivity = canCreateVirtualActivity;
    }

    @SuppressWarnings("unused")
    public final List<T3InputProvider> getInputProviders() {
        return inputProviders;
    }

    private File getTreatmentDirectory() {
        String path = getTreatmentDirectoryPath();
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        return new File(path);
    }

}
