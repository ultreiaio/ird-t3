/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * To listen creation or destroying of user session (this is needed to close
 * nicely any resources attached to this user session such as db connexions).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1
 */
public class T3SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        // at creation time, nothing to do since we do not use directly
        // the httpSession but the wrap offers by xworks
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        T3Session t3Session = T3Session.getT3Session(se.getSession());
        if (t3Session.isUserInSession()) {

            // only destroy the session if a user is loggued (otherwise there
            // is nothing to clear)
            ServletContext servletContext = se.getSession().getServletContext();
            T3ApplicationContext applicationContext =
                    T3ApplicationContext.getT3ApplicationContext(servletContext);
            applicationContext.destroyT3Session(t3Session);
        }
    }

}
