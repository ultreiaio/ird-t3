/*
 * #%L
 * T3 :: Web
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserJavaBeanDefinition;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Objects;
import java.util.Optional;

/**
 * Action to manage user (create - update - change password,...)
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class UserAction extends T3ActionSupport implements Preparable {

    protected static final Log log = LogFactory.getLog(UserAction.class);

    private static final long serialVersionUID = 1L;

    private static final String BACK_TO_LIST = "backToList";

    private T3User user;
    private String checkPassword;
    private String userEditAction;

    public String getUserEditAction() {
        return userEditAction;
    }

    public void setUserEditAction(String userEditAction) {
        this.userEditAction = userEditAction;
    }

    public String getCheckPassword() {
        return checkPassword;
    }

    public void setCheckPassword(String checkPassword) {
        this.checkPassword = checkPassword;
    }

    @Override
    public void prepare() {
        String userId = getUser().getId();
        if (!StringUtils.isEmpty(userId)) {
            // load user
            T3User userById = getUserService().getUserById(userId);
            user = T3UserJavaBeanDefinition.instance()
                    .id(userById.getId())
                    .admin(userById.isAdmin())
                    .login(userById.getLogin()).build();
        }
    }

    public String doCreate() {
        T3User t3User = getUser();
        String userLogin = t3User.getLogin();
        log.info(String.format("will create user  %s", userLogin));
        // create user
        getUserService().createUser(t3User);
        return BACK_TO_LIST;
    }

    public String doUpdate() {
        T3User t3User = getUser();
        log.info(String.format("will update user  %s", t3User.getLogin()));
        if (!t3User.isAdmin() && getT3Session().getUser().isAdmin() && getT3Session().getUser().getLogin().equals(t3User.getLogin())) {
            // never change admin flag on connected user
            t3User.setAdmin(true);
        }
        // update user
        getUserService().updateUser(t3User);
        return SUCCESS;
    }

    public String doDelete() {
        T3User t3User = getUser();
        log.info(String.format("will delete user %s", t3User.getLogin()));
        getUserService().deleteUser(t3User.getId());
        return BACK_TO_LIST;
    }

    @Override
    public void validate() {
        EditActionEnum action = getEditActionEnum();
        log.info(String.format("Edit action : %s", action));
        if (action != null) { // validate only if a action is set
            T3User t3User = getUser();
            String userLogin = t3User.getLogin();
            String userPassword = t3User.getPassword();
            switch (action) {
                case CREATE:

                    // login + password required

                    if (StringUtils.isEmpty(userLogin)) {
                        // empty user login
                        addFieldError("user.login", t("t3.error.required.login"));
                    } else {
                        // check login not already used
                        Optional<T3User> login = getUserService().getUserByLogin(userLogin);
                        login.ifPresent(t3User1 -> addFieldError("user.login", t("t3.error.login.already.used")));
                    }

                    if (StringUtils.isEmpty(userPassword)) {
                        // empty user password
                        addFieldError("user.password", t("t3.error.required.password"));
                    } else {
                        if (!Objects.equals(checkPassword, userPassword)) {
                            addFieldError("user.password", t("t3.error.password.mismatch"));
                        }
                    }
                    break;
                case EDIT:
                    if (!StringUtils.isEmpty(userPassword)) {
                        if (!Objects.equals(checkPassword, userPassword)) {
                            addFieldError("user.password", t("t3.error.password.mismatch"));
                        }
                    }
                    break;
                case DELETE:
                    // nothing to validate
                default:
                    // nothing to validate
            }
        }
    }

    public T3User getUser() {
        if (user == null) {
            user = new T3User();
        }
        return user;
    }

    private EditActionEnum getEditActionEnum() {
        EditActionEnum result = null;
        if (userEditAction != null) {
            result = EditActionEnum.valueOf(userEditAction.toUpperCase());
        }
        return result;
    }
}
