<%@ page import="fr.ird.t3.web.actions.T3ActionSupport" %>
<%--
#%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<hr/>
<div id='footer'>
  <ul class="clearfix">
    <li>
      <a href="http://ultreiaio.gitlab.io/ird-t3" title="Documentation" target="_blank">
        T3+ version <%=T3ActionSupport.getApplicationVersion()%>
      </a>
    </li>
    <li>
      <a href="https://gitlab.com/ultreiaio/ird-t3" title="Projet" target="_blank">
        Projet (GitLab)
      </a>
    </li>
    <li>
      <a href="http://www.gnu.org/licenses/agpl.html" title="Licence AGPL v3"
         target="_blank">
        AGPLv3
      </a>
    </li>
    <li>
      Copyright 2010 - 2018
      <a href="http://www.ird.fr">IRD</a>
      <a href="http://www.codelutin.com" title="Code Lutin" target="_blank">Code Lutin</a>
      <a href="https://www.ultreia.io" title="Ultreia.io" target="_blank">Ultreia.io</a>
    </li>
  </ul>
</div>
