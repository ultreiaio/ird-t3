<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="cleanBoth">
  <s:text name="t3.label.language"/>
  <ul>
    <li>
      <s:if
        test="%{#session['WW_TRANS_I18N_LOCALE'] != null && #session['WW_TRANS_I18N_LOCALE'].language == 'en'}">
        <s:text name="t3.label.locale.english"/>
      </s:if>
      <s:else>
        <s:a action="changeLang" namespace="/">
          <s:param name="request_locale">en_GB</s:param>
          <s:text name="t3.action.locale.english"/>
        </s:a>
      </s:else>
    </li>
    <li>
      <s:if
        test="%{#session['WW_TRANS_I18N_LOCALE'] == null || #session['WW_TRANS_I18N_LOCALE'].language == 'fr'}">
        <s:text name="t3.label.locale.french"/>
      </s:if>
      <s:else>
        <s:a action="changeLang" namespace="/">
          <s:param name="request_locale">fr_FR</s:param>
          <s:text name="t3.action.locale.french"/>
        </s:a>
      </s:else>
    </li>
  </ul>
</div>
