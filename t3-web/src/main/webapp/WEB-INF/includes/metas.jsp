<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="d" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>T3+ - <d:title default="T3+"/></title>
  <d:head/>
  <link rel="stylesheet" type="text/css"
        href="<s:url value='/css/screen.css' />"/>
  <link rel="icon" type="image/png"
        href="<s:url value='/images/logo_codelutin.png' />"/>
  <sj:head jqueryui="true" jquerytheme="start"/>
</head>
