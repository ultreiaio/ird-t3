<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" charset="UTF-8"
        src="<s:url value='/js/monthpicker.js' />"></script>
<style type="text/css">
  .ui-datepicker-calendar {
    display: none;
  }
</style>
<title><s:text name="t3.label.data.level1.configuration"/></title>

<h2><s:text name="t3.label.data.level1.configuration"/></h2>

<s:form method="post" validate="true" namespace="/level1">

  <fieldset>

    <legend>
      <s:text name="t3.label.configure"/>
    </legend>

      <%-- selected sample quality --%>
    <s:checkboxlist key="configuration.sampleQualityIds" requiredLabel="true"
                    label='%{getText("t3.common.sampleQuality")}'
                    list="sampleQualities" template="mycheckboxlist"/>

      <%-- selected sample type --%>
    <s:checkboxlist key="configuration.sampleTypeIds" requiredLabel="true"
                    label='%{getText("t3.common.sampleType")}'
                    list="sampleTypes" template="mycheckboxlist"/>

      <%-- selected fleet country --%>
    <s:checkboxlist key="configuration.fleetIds" requiredLabel="true"
                    label='%{getText("t3.common.fleetCountry")}'
                    list="fleets" template="mycheckboxlist"/>

      <%-- selected ocean --%>
    <s:checkboxlist key="configuration.oceanIds" requiredLabel="true"
                    label='%{getText("t3.common.ocean")}'
                    list="oceans" template="mycheckboxlist"/>

      <%-- selected begin date --%>
    <sj:datepicker key="configuration.beginDate" requiredLabel="true"
                   label='%{getText("t3.common.beginDate")}'
                   appendText=" (mm-yyyy)"/>

      <%-- selected end date --%>
    <sj:datepicker key="configuration.endDate" requiredLabel="true"
                   label='%{getText("t3.common.endDate")}'
                   appendText=" (mm-yyyy)"/>

      <%-- rfTotMax --%>
    <s:textfield key="configuration.rfTotMax" requiredLabel="true"
                 label='%{getText("t3.common.rfTotMax")}'/>

      <%-- rfMinus10Max --%>
    <s:textfield key="configuration.rfMinus10Max" requiredLabel="true"
                 label='%{getText("t3.common.rfMinus10Max")}'/>

      <%-- rfPlus10Max --%>
    <s:textfield key="configuration.rfPlus10Max" requiredLabel="true"
                 label='%{getText("t3.common.rfPlus10Max")}'/>

      <%-- rfMinus10MinNumber --%>
    <s:textfield key="configuration.rfMinus10MinNumber" requiredLabel="true"
                 label='%{getText("t3.common.rfMinus10MinNumber")}'/>

      <%-- rfPlus10MinNumber --%>
    <s:textfield key="configuration.rfPlus10MinNumber" requiredLabel="true"
                 label='%{getText("t3.common.rfPlus10MinNumber")}'/>

      <%-- use or not rf-10 and rf+10 --%>
    <s:radio key="configuration.useRfMinus10AndRfPlus10" requiredLabel="true" list="useRfMinus10AndRfPlus10OrNot"
             label='%{getText("t3.label.data.level1.configuration.useRfMinus10AndRfPlus10OrNot")}'/>

      <%-- selected matching trip count --%>
    <s:label key="matchingTripCount" requiredLabel="true"
             label='%{getText("t3.level1.matching.trip.count")}'/>

      <%-- selected matching sample count --%>
    <s:label key="matchingSampleCount" requiredLabel="true"
             label='%{getText("t3.level1.matching.sample.count")}'/>

      <%-- not matching sample count --%>
    <s:label key="notMatchingSampleCount"
             label='%{getText("t3.level1.not.matching.sample.count")}'/>

  </fieldset>
  <br/>
  <div class="actions">
  <s:if test="configurationInSession">

    <%--Can remove configuration--%>
    <s:submit action="removeLevel1Configuration" key="t3.action.remove.configuration" align="right"/>
  </s:if>

  <%--validate + save configuration--%>
  <s:submit action="validateLevel1Configuration" key="t3.action.save.configuration" align="right"/>
  </div>
</s:form>

<s:if test="confirm && configurationInSession">
  <ul class="floatLeft">
    <li>

      <s:a action="configureExtrapolateSampleCountedAndMeasured"
           namespace="/level1">
        <s:text
                name="t3.menu.treatment.level1.extrapolateSampleMeasuredAndCounted"/>
      </s:a>
    </li>
  </ul>
</s:if>

<script type="text/javascript">

    jQuery(document).ready(function () {

        $.prepareMonthPickers(
            {
                minDateAsMonth: '<s:property value="configuration.minDate"/>',
                maxDateAsMonth: '<s:property value="configuration.maxDate"/>'
            });

    });
</script>
