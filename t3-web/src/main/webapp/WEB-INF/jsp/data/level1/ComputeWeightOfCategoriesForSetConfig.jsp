<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<style type="text/css">
  .wwlbl {
    width: 500px;
  }
</style>
<title><s:text name="t3.label.data.treatment.level1"/> :
  <s:text name="t3.level1.step.computeWeightOfCategoriesForSet"/></title>

<h2><s:text name="t3.label.data.treatment.level1"/> :
  <s:text name="t3.level1.step.computeWeightOfCategoriesForSet"/></h2>

<h3><s:text name="t3.label.confirm"/></h3>

<jsp:include page="level1ConfigurationResume.jsp"/>

<s:if test="actionErrors.empty">

  <%-- Can execute this level 1 action --%>

  <s:form namespace="/level1">

    <%-- launch action --%>
    <s:submit action="run-ComputeWeightOfCategoriesForSet"
              key="t3.action.runAction" align="right"/>

  </s:form>
</s:if>
<s:else>

  <%-- Can not execute this level 1 action --%>

  <strong>
    <s:text name="t3.label.can.not.execute.level1.action"/>
  </strong>
  <s:actionerror/>
</s:else>
