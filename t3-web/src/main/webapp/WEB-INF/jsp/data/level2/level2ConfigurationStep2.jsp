<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<style type="text/css">
  .wwlbl {
    width: 500px;
  }
</style>
<title><s:text name="t3.label.data.treatment.level2"/></title>

<h2><s:text name="t3.label.data.treatment.level2"/></h2>

<s:form method="post" validate="true" namespace="/level2">

  <fieldset>

    <legend>
      <s:text name="t3.label.data.level2.configuration.step2"/>
    </legend>

      <%-- selected sample fleet countries --%>
    <s:checkboxlist key="configuration.sampleFleetIds" requiredLabel="true" list="sampleFleets"
                    label='%{getText("t3.common.sampleFleetCountries")}' template="mycheckboxlist"/>

      <%-- selected sample flag countries --%>
    <s:checkboxlist key="configuration.sampleFlagIds" requiredLabel="true" list="sampleFlags"
                    label='%{getText("t3.common.sampleFlagCountries")}' template="mycheckboxlist"/>

      <%-- selected stratumWeightRatio  --%>
    <s:textfield key="configuration.stratumWeightRatio" requiredLabel="true"
                 label='%{getText("t3.common.stratumWeightRatio")}'/>

      <%-- selected stratumMinimumSampleCountObjectSchoolType  --%>
    <s:textfield key="configuration.stratumMinimumSampleCountObjectSchoolType" requiredLabel="true"
                 label='%{getText("t3.common.stratumMinimumSampleCountObjectSchoolType")}'/>

      <%-- selected stratumMinimumSampleCountFreeSchoolType  --%>
    <s:textfield key="configuration.stratumMinimumSampleCountFreeSchoolType" requiredLabel="true"
                 label='%{getText("t3.common.stratumMinimumSampleCountFreeSchoolType")}'/>

      <%--use weight categories in stratum--%>
    <s:radio key="configuration.useWeightCategoriesInStratum" requiredLabel="true"
             list="useWeightCategoriesInStratumOrNot" label='%{getText("t3.label.data.useWeightCategoriesInStratum")}'/>

      <%-- use all samples --%>
    <s:radio key="configuration.useAllSamplesOfStratum" requiredLabel="true" list="useSamplesOrNot"
             label='%{getText("t3.label.data.level2.configuration.samplesToUse")}'/>

      <%--schoolTypeIndeterminate strategy in sample stratum--%>
    <s:radio key="configuration.schoolTypeIndeterminate" requiredLabel="true" list="schoolTypeIndeterminate"
             label='%{getText("t3.common.schoolTypeIndeterminate")}' value="%{configuration.schoolTypeIndeterminate}"/>

  </fieldset>
  <br/>
  <div class="actions">
  <%-- back to step 1 --%>
  <s:submit action="configureLevel2Step1!input" key="t3.action.back.to.configuration.step1" align="right"/>

  <s:if test="!missingData">

    <%--validate + go to resume --%>
    <s:submit action="configureLevel2Step2" key="t3.action.validate.configuration" align="right"/>
  </s:if>
  </div>
</s:form>
