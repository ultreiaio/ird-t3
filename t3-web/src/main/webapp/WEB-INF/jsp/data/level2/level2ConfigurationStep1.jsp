<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" charset="UTF-8"
        src="<s:url value='/js/monthpicker.js'/>"></script>
<style type="text/css">
  .ui-datepicker-calendar {
    display: none;
  }

  .wwlbl {
    width: 500px;
  }
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.struts2_jquery.myBindingOptions = {
            datatype: "json",
            type: 'select',
            name: "zoneVersionId",
            list: "zoneVersions",
            buttonset: false,
            jqueryaction: "select",
            reloadtopics: "reloadList",
            id: $('select[name$="zoneVersionId"]')[0].id
        };
        $('[name="configuration.zoneTypeId"]').change(function (event) {
            var myOptions = jQuery.struts2_jquery.myBindingOptions;
            myOptions.href = "<s:url action='getZoneVersions' namespace='/json'><s:param name='zoneTypeId'/></s:url>" + this.value;
            var myDiv = $(jQuery.struts2_jquery.escId(myOptions.id));
            $.unsubscribe('reloadList', myDiv);
            jQuery.struts2_jquery.bind(myDiv, myOptions);
        });
    });
</script>
<title><s:text name="t3.label.data.treatment.level2"/></title>

<h2><s:text name="t3.label.data.treatment.level2"/></h2>

<s:form method="post" validate="true" namespace="/level2">

  <fieldset>

    <legend>
      <s:text name="t3.label.configuration.step1"/>
    </legend>
    <s:hidden name="validating" value="true"/>

      <%-- selected oceans --%>
    <s:checkboxlist key="configuration.oceanIds" requiredLabel="true" list="oceans"
                    label='%{getText("t3.common.ocean")}' template="mycheckboxlist"/>

      <%-- selected zone type--%>
    <s:select key="configuration.zoneTypeId" requiredLabel="true" list="zoneTypes"
              headerKey="" headerValue="" label='%{getText("t3.common.zoneType")}'/>

      <%-- selected zone version--%>
    <s:url id='jsonUrl' action="getZoneVersions" namespace="/json">
      <s:param name="zoneTypeId" value="%{configuration.zoneTypeId}"/>
    </s:url>
    <sj:select key="zoneVersionId" requiredLabel="true" list="zoneVersions" href="%{jsonUrl}"
               label='%{getText("t3.common.zoneVersion")}'/>

      <%-- selected catch fleets --%>
    <s:checkboxlist key="configuration.catchFleetIds" requiredLabel="true" list="catchFleets"
                    label='%{getText("t3.common.catchFleet")}' template="mycheckboxlist"/>

      <%-- selected time step  --%>
    <s:select key="configuration.timeStep" requiredLabel="true" list="timeSteps"
              label='%{getText("t3.common.timeStep")}'/>

      <%-- begin date --%>
    <sj:datepicker key="configuration.beginDate" requiredLabel="true"
                   label='%{getText("t3.common.beginDate")}' appendText=" (mm-yyyy)"/>

      <%-- end date --%>
    <sj:datepicker key="configuration.endDate" requiredLabel="true"
                   label='%{getText("t3.common.endDate")}' appendText=" (mm-yyyy)"/>

      <%-- selected species --%>
    <s:checkboxlist key="configuration.speciesIds" requiredLabel="true" list="species"
                    label='%{getText("t3.common.species")}' template="mycheckboxlist"/>

  </fieldset>
  <s:if test="!missingData">
    <br/>
    <%--validate + save configuration--%>
    <s:submit action="configureLevel2Step1" key="t3.action.validate.configuration" align="right"/>
  </s:if>
</s:form>

<script type="text/javascript">

    jQuery(document).ready(function () {

        $.prepareMonthPickers(
            {
                minDateAsMonth: '<s:property value="configuration.minDate"/>',
                maxDateAsMonth: '<s:property value="configuration.maxDate"/>'
            });

    });
</script>
