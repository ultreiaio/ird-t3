<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<fieldset>

  <legend>
    <s:text name="t3.label.config.resume"/>
  </legend>

  <s:if test="configuration.configurationEmpty">
    <s:text name="t3.label.data.level0.computeRF2.emptyConfiguration"/>

    <s:textfield key="configuration.beginDate" disabled="true"
                 label='%{getText("t3.common.beginDate")}'/>

    <s:textfield key="configuration.endDate" disabled="true"
                 label='%{getText("t3.common.endDate")}'/>

  </s:if>
  <s:else>
    <s:textfield key="configuration.beginDate" disabled="true"
                 label='%{getText("t3.common.beginDate")}'/>

    <s:textfield key="configuration.endDate" disabled="true"
                 label='%{getText("t3.common.endDate")}'/>

    <s:checkboxlist key="configuration.vesselSimpleTypeIds" disabled="true"
                    list="vesselSimpleTypes" template="mycheckboxlist"
                    label='%{getText("t3.common.vesselSimpleType")}'/>

    <s:checkboxlist key="configuration.landingHarbourIds" disabled="true"
                    list="landingHarbours" template="mycheckboxlist"
                    label='%{getText("t3.common.landingHarbour")}'/>

    <s:checkboxlist key="configuration.fleetIds" disabled="true"
                    list="fleets" template="mycheckboxlist"
                    label='%{getText("t3.common.fleetCountry")}'/>
  </s:else>


</fieldset>
