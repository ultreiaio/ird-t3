<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<title><s:text name="t3.label.data.treatment.level0"/> : <s:text
  name="t3.label.data.level0.computeRF1"/></title>

<h2><s:text name="t3.label.data.treatment.level0"/> : <s:text
  name="t3.label.data.level0.computeRF1"/></h2>

<h3><s:text name="t3.label.result"/></h3>

<s:form>
  <jsp:include page="ComputeRF1ConfigResume.jsp"/>
</s:form>

<s:if test="t3ActionContext.errorMessages.empty">
  <p>
    <s:a action="configureComputeRF2" namespace="/level0">
      <s:text name="t3.menu.treatment.level1.nextStep"/> :
      <s:text name="t3.menu.data.level0.computeRF2"/>
    </s:a>
  </p>
</s:if>
