<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<fieldset>

  <legend>
    <s:text name="t3.label.config.resume"/>
  </legend>

  <%-- selected oceans --%>
  <s:checkboxlist key="configuration.oceanIds" disabled="true" list="oceans" template="mycheckboxlist"
                  label='%{getText("t3.common.ocean")}'/>

  <%-- selected zone type--%>
  <s:select key="configuration.zoneTypeId" disabled="true" list="zoneTypes" label='%{getText("t3.common.zoneType")}'/>

  <%-- selected catch fleet --%>
  <s:select key="configuration.catchFleetIds" disabled="true" list="catchFleets"
            label='%{getText("t3.common.catchFleet")}'/>

  <%-- selected time step  --%>
  <s:select key="configuration.timeStep" disabled="true" list="timeSteps" label='%{getText("t3.common.timeStep")}'/>

  <%-- selected begin date --%>
  <s:textfield key="configuration.beginDate" disabled="true" label='%{getText("t3.common.beginDate")}'/>

  <%-- selected end date --%>
  <s:textfield key="configuration.endDate" disabled="true" label='%{getText("t3.common.endDate")}'/>

  <div class="formSeparator"></div>
  <%-- selected sample fleet countries --%>
  <s:checkboxlist key="configuration.sampleFleetIds" disabled="true" list="sampleFleets" template="mycheckboxlist"
                  label='%{getText("t3.common.sampleFleetCountries")}'/>

  <div class="formSeparator"></div>
  <%-- selected sample flag countries --%>
  <s:checkboxlist key="configuration.sampleFlagIds" disabled="true" list="sampleFlags" template="mycheckboxlist"
                  label='%{getText("t3.common.sampleFlagCountries")}'/>

  <%-- selected stratumWeightRatio --%>
  <s:textfield key="configuration.stratumWeightRatio" disabled="true"
               label='%{getText("t3.common.stratumWeightRatio")}'/>

  <div class="formSeparator"></div>

  <%-- use all samples --%>
  <s:radio key="configuration.useAllSamplesOfStratum" disabled="true" list="useSamplesOrNot"
           label='%{getText("t3.label.data.level3.configuration.samplesToUse")}'/>

  <%--use weight categories in stratum--%>
  <s:radio key="configuration.useWeightCategoriesInStratum" disabled="true"
           list="useWeightCategoriesInStratumOrNot" label='%{getText("t3.label.data.useWeightCategoriesInStratum")}'/>

  <%--schoolTypeIndeterminate strategy in sample stratum--%>
  <s:radio key="configuration.schoolTypeIndeterminate" disabled="true" list="schoolTypeIndeterminate"
           label='%{getText("t3.common.schoolTypeIndeterminate")}'/>

  <div class="formSeparator"></div>
  <table class="cleanBoth">
    <tr>
      <th class="wwlbl"><s:text name="t3.common.species"/></th>
      <th><s:text name="t3.common.stratumMinimumSampleCountObjectSchoolType"/></th>
      <th><s:text name="t3.common.stratumMinimumSampleCountFreeSchoolType"/></th>
    </tr>
    <s:set var="speciesData" value="%{stratumMinimumSampleCount}"/>
    <s:iterator value="species" status="status" var="entry">
      <s:set var="speciesId">${entry.key}</s:set>
      <s:set var="boId">BO:${speciesId}</s:set>
      <s:set var="blId">BL:${speciesId}</s:set>
      <tr>
        <td class="wwlbl nowrap"><s:property value="value"/></td>
        <td class="nowrap">
          <s:set var="v">${speciesData[speciesId].minimumCountForObjectSchool}</s:set>
          <s:textfield disabled="true" value="%{v}"/>
        </td>
        <td class="nowrap">
          <s:set var="v">${speciesData[speciesId].minimumCountForFreeSchool}</s:set>
          <s:textfield disabled="true" value="%{v}"/>
        </td>
      </tr>
    </s:iterator>
  </table>

</fieldset>
