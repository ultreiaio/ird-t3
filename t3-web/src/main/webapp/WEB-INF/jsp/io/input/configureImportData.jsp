<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<script type="text/javascript">

    jQuery(document).ready(function () {
        function changeCanCreateVessel(val) {
            var container = $('[name="createVirtualVessel"]');
            if (val) {
                container.attr('disabled', false);
                container.prop('checked', true);
            } else {
                container.attr('disabled', true);
            }
        }
        function changeUseSamplesOnly(val) {
            var container = $('[name="canCreateVirtualActivity"]');
            if (val) {
                container.attr('disabled', true);
                container.attr('checked', false);
            } else {
                container.attr('disabled', false);
                container.prop('checked', true);
            }
        }

        $('[name="canCreateVessel"]').change(function () {
            changeCanCreateVessel($(this).prop('checked'));
        });
        $('[name="useSamplesOnly"]').change(function () {
            changeUseSamplesOnly($(this).prop('checked'));
        });
        changeCanCreateVessel(<s:property value="%{canCreateVessel}"/>);
        changeUseSamplesOnly(<s:property value="%{useSamplesOnly}"/>);
    });
</script>
<title><s:text name="t3.label.data.configureImportData"/></title>
<h2><s:text name="t3.label.data.configureImportData"/></h2>

<s:form method="post" validate="true" enctype="multipart/form-data" namespace="/io">
  <s:hidden key="treatmentDirectoryPath" label=''/>
  <s:select key="inputProviderId" list="inputProviders" label='%{getText("t3.common.inputProvider")}' listKey="id"
            listValue="label" requiredLabel="true"/>
  <s:if test="loadedSource==null">
    <s:hidden name="useWells" value="%{useWells}"/>
    <s:file name="sourceToLoad" requiredLabel="true" label='%{getText("t3.common.sourceToUpload")} (*)'/>
    <br/>

    <div class="cleanBoth">
      (*) <s:text name="t3.information.upload.maxsize">
      <s:param value="%{maxSize}"/>
    </s:text>
    </div>
    <div class="actions">
      <s:submit action="configureImportData!doAddSource" key="t3.action.importData.addSource" align="right"/>
    </div>
  </s:if>
  <s:else>
    <s:hidden key="loadedSource" label=''/>
    <s:textfield value="%{loadedSource}" key="t3.common.uploaded.source" disabled="true" requiredLabel="true"
                 size="40"/>
    <s:checkbox key="useWells" label='%{getText("t3.common.force.useWells")}'/>
    <s:checkbox key="useSamplesOnly" label='%{getText("t3.common.useSamplesOnly")}'/>
    <s:checkbox key="canCreateVirtualActivity" label='%{getText("t3.common.canCreateVirtualActivity")}'/>
    <s:checkbox key="canCreateVessel" label='%{getText("t3.common.canCreateVessel")}'/>
    <s:checkbox key="createVirtualVessel" label='%{getText("t3.common.createVirtualVessel")}'/>
    <div class="actions">
    <s:submit action="configureImportData!doDeleteSource" key="t3.action.importData.deleteSource" align="right"/>
    <s:submit action="configureImportData!doPrepareAnalyze" key="t3.action.importData.analyze" align="right"/>
    </div>
  </s:else>

</s:form>
