<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<title><s:text name="t3.label.data.analyzeImportData"/></title>

<h2><s:text name="t3.label.data.analyzeImportData"/> - <s:text name="t3.label.result"/></h2>

<jsp:include page="importDataConfigResume.jsp"/>

<s:if test="valid">
  <%-- Can do the import --%>
  <p>
    <s:text name="t3.label.message.trips.safe.to.import">
      <s:param>
        <s:property value="%{nbSafeTrips}"/>
      </s:param>
    </s:text>
  </p>

  <s:form method="post" validate="true" namespace="/io">
    <s:if test="needReplace">
      <strong>
        <s:text name="t3.label.message.trips.to.replace">
          <s:param>
            <s:property value="%{nbTripsToReplace}"/>
          </s:param>
        </s:text>
      </strong>
      <s:checkbox name="replaceTrip" key="t3.question.confirm.to.replace.trip" value="%{false}"/>
    </s:if>
    <s:submit action="prepareImportData" key="t3.action.importData.doImport" align="right"/>
  </s:form>
</s:if>
<s:else>
  <p>
    <s:text name="t3.label.message.trips.unsafe.to.import">
      <s:param>
        <s:property value="%{nbUnsafeTrips}"/>
      </s:param>
    </s:text>
  </p>
  <s:a action="configureImportData!input" namespace="/io">
    <s:text name="t3.menu.io.importData.again"/>
  </s:a>
</s:else>
