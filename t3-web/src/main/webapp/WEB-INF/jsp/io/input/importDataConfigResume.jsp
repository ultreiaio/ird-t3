<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<fieldset>
  <legend>
    <s:text name="t3.label.importData.config.resume"/>
  </legend>
  <s:form>
    <s:select key="t3.common.inputProvider" list="inputProviders" listKey="id" listValue="label"
              value="%{configuration.inputProvider.id}" disabled="true"/>
    <s:textfield value="%{configuration.inputFile.name}" key="t3.common.uploaded.source" disabled="true" size="40"/>
    <s:label key="t3.common.useWells" value=""/>
    <s:checkbox name="useWells" value="%{configuration.useWells}" disabled="true" key="t3.common.force.useWells"/>
    <s:checkbox name="useSamplesOnly" value="%{configuration.useSamplesOnly}" disabled="true" key="t3.common.useSamplesOnly"/>
    <s:checkbox name="canCreateVirtualActivity" value="%{configuration.canCreateVirtualActivity}" disabled="true"
                key="t3.common.canCreateVirtualActivity"/>
    <s:checkbox name="canCreateVessel" value="%{configuration.canCreateVessel}" disabled="true"
                key="t3.common.canCreateVessel"/>
    <s:if test="configuration.canCreateVessel">
      <s:checkbox name="createVirtualVessel" value="%{configuration.createVirtualVessel}" disabled="true"
                  key="t3.common.createVirtualVessel"/>
    </s:if>
  </s:form>
</fieldset>
