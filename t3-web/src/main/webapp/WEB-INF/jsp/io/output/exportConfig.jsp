<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<title><s:text name="t3.label.configureExport"/></title>

<h2><s:text name="t3.label.configureExport"/></h2>

<s:url id="loadUrl" action='getUserOuputDatabase' namespace='/json'/>

<script type="text/javascript" charset="UTF-8"
        src="<s:url value='/js/monthpicker.js' />"></script>
<style type="text/css">
  .ui-datepicker-calendar {
    display: none;
  }

  .wwlbl {
    width: 500px;
  }
</style>
<script type="text/javascript">
  jQuery(document).ready(function () {
    jQuery.struts2_jquery.myBindingOptions = {
      datatype : "json",
      type : 'checkbox',
      name : "operationIds",
      list : "operations",
      buttonset : false,
      jqueryaction : "buttonset",
      reloadtopics : "reloadList",
      id:$('input[name$="operationIds"]')[0].parentNode.id
    };
    $('[name="configuration.outputProviderId"]').change(function(event) {
      var myOptions = jQuery.struts2_jquery.myBindingOptions;
      myOptions.href = "<s:url action='getOutputProviderOperations' namespace='/json'><s:param name='outputProviderId'/></s:url>" + this.value;
      var myDiv = $(jQuery.struts2_jquery.escId(myOptions.id));
      $.unsubscribe('reloadList', myDiv);
      jQuery.struts2_jquery_ui.bind(myDiv, myOptions);
    });

    $('[name="databaseId"]').change(function(event) {
      var url = "${loadUrl}?" + $.param({ databaseId:this.value});
      $.getJSON(url,
                function(data) {
                  var database = data.database;
                  $('[name="configuration.url"]').attr('value', database.url ? database.url : '');
                  $('[name="configuration.login"]').attr('value', database.login ? database.login : '');
                  $('[name="configuration.password"]').attr('value', "");
                }
      );
    });
  });
</script>

<s:form method="post" validate="true" namespace="/io">

  <fieldset>
    <legend><s:text name="t3.label.export.config.pilot"/></legend>

    <s:select key="configuration.outputProviderId"
              label='%{getText("t3.output.outputProvider")}'
              list="outputProviders"
              listKey="id"
              listValue="libelle"
              headerKey="" headerValue=""
              requiredLabel="true"/>

    <sj:checkboxlist key="operationIds" buttonset="false"
                     label='%{getText("t3.output.operations")}'
                     list="operations" requiredLabel="true"/>
  </fieldset>

  <fieldset>
    <legend><s:text name="t3.label.export.config.jdbc"/></legend>

    <s:select key="databaseId" list="databases"
              label='%{getText("t3.common.userOutputDatabase")}'
              headerKey="" headerValue=""/>

    <s:textfield name="configuration.url" requiredLabel="true" size="40"
                 label='%{getText("t3.common.url")}'/>

    <s:textfield name="configuration.login" requiredLabel="true" size="40"
                 label='%{getText("t3.common.login")}'/>

    <s:password name="configuration.password" requiredLabel="true" size="40"
                label='%{getText("t3.common.password")}'
                autocomplete='off'/>

  </fieldset>

  <fieldset>
    <legend><s:text name="t3.label.export.config.data"/></legend>

      <%-- selected ocean --%>
    <s:select key="configuration.oceanId" list="oceans"
              label='%{getText("t3.common.ocean")}' requiredLabel="true"/>

      <%-- selected fleet country --%>
    <s:select key="configuration.fleetId" list="fleets"
              label='%{getText("t3.common.fleetCountry")}' requiredLabel="true"/>

      <%-- begin date --%>
    <sj:datepicker key="configuration.beginDate" requiredLabel="true"
                   label='%{getText("t3.common.beginDate")}'
                   appendText=" (mm-yyyy)"/>

      <%-- end date --%>
    <sj:datepicker key="configuration.endDate" requiredLabel="true"
                   label='%{getText("t3.common.endDate")}'
                   appendText=" (mm-yyyy)"/>

  </fieldset>
  <s:submit action="configureExport!execute"
            key="t3.action.configuration.validateConfiguration"
            align="right"/>

</s:form>


<script type="text/javascript">

  jQuery(document).ready(function () {

    $.prepareMonthPickers(
      {
        minDateAsMonth:'<s:property value="configuration.minDate"/>',
        maxDateAsMonth:'<s:property value="configuration.maxDate"/>'
      });

  });

</script>
