<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<fieldset>

  <legend>
    <s:text name="t3.label.export.config.resume"/>
  </legend>

  <s:select key="configuration.outputProviderId"
            label='%{getText("t3.output.outputProvider")}'
            list="outputProviders" listKey="id" listValue="libelle"
            disabled="true"/>

  <s:checkboxlist key="configuration.operationIds" list="operations"
                  label='%{getText("t3.output.operations")}' disabled="true"/>

  <s:textfield key="configuration.url"
               label='%{getText("t3.common.url")}'
               disabled="true"
               size="40"/>

  <s:textfield key="configuration.login"
               label='%{getText("t3.common.login")}'
               disabled="true"
               size="40"/>

  <%-- selected fleet country --%>
  <s:select key="configuration.fleetId"
            label='%{getText("t3.common.fleetCountry")}'
            list="fleets" disabled="true"/>

  <%-- selected ocean --%>
  <s:select key="configuration.oceanId" list="oceans"
            label='%{getText("t3.common.ocean")}' disabled="true"/>

  <s:textfield key="configuration.beginDate"
               label='%{getText("t3.common.beginDate")}'
               disabled="true"/>

  <s:textfield key="configuration.endDate"
               label='%{getText("t3.common.endDate")}'
               disabled="true"/>

</fieldset>
