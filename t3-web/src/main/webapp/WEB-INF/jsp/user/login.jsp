<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>T3+ - <s:text name="t3.label.login"/></title>
  <link rel="stylesheet" type="text/css"
        href="<s:url value='/css/screen.css' />"/>
  <link rel="icon" type="image/png"
        href="<s:url value='/images/logo_codelutin.png' />"/>
  <sj:head jqueryui="true" jquerytheme="start"/>
</head>

<body>

<%--header--%>

<div class='displayBlock'>
  <div class='floatLeft'>
    <s:a action="home" namespace="/">T3+</s:a>
  </div>
  <div id='headerRight'>
    <%@ include file="/WEB-INF/includes/i18n.jsp" %>
  </div>
</div>
<div class="cleanBoth"></div>
<hr/>

<%-- content --%>

<h2><s:text name="t3.label.login"/></h2>

<s:form method="POST" namespace="/user">
  <s:hidden key="redirectAction" label=""/>
  <s:textfield name="login" key="t3.common.login" requiredLabel="true"/>
  <s:password name="password" key="t3.common.password" requiredLabel="true"/>
  <s:submit action="login" key="t3.action.login" align="right"/>
</s:form>

<br/>
<%-- footer --%>
<%@ include file="/WEB-INF/includes/footer.jsp" %>

</body>
</html>
