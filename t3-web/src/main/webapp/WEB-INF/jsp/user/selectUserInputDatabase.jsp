<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:url id="loadUrl" action='getUserT3Database' namespace='/json'/>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $('[name="databaseId"]').change(function(event) {
      var url = "${loadUrl}?" + $.param({ databaseId:this.value});
      $.getJSON(url,
                function(data) {
                  var database = data.database;
                  $('[name="database.url"]').attr('value', database.url ? database.url : '');
                  $('[name="database.login"]').attr('value', database.login ? database.login : '');
                  $('[name="database.password"]').attr('value', database.password ? database.password : '');
                }
      );
    });
  });
</script>
<title><s:text name="t3.label.user.selectT3Database"/></title>

<h2><s:text name="t3.label.user.selectT3Database"/></h2>

<s:form method="post" validate="true" namespace="/user">

  <fieldset>
    <legend><s:text name="t3.label.t3.config.jdbc"/></legend>

    <s:select key="databaseId" list="databases"
            label='%{getText("t3.common.userT3Database")}'
            headerKey="" headerValue=""/>
    
    <s:textfield name="database.url" requiredLabel="true" size="40"
                 label='%{getText("t3.common.url")}'/>

    <s:textfield name="database.login" requiredLabel="true" size="40"
                 label='%{getText("t3.common.login")}'/>

    <s:password name="database.password" requiredLabel="true" size="40"
                label='%{getText("t3.common.password")}'
                autocomplete='off'/>
  </fieldset>
  <br/>

  <s:submit action="selectUserInputDatabase" method="execute"
            key="t3.action.connectToDatabase" align="right"/>
</s:form>
