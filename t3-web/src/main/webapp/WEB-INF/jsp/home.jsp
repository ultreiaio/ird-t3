<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>
<title><s:text name="t3.label.welcome"/></title>

<h2><s:text name="t3.label.welcome"/></h2>

<hr/>
<div class='menu'>

  <s:if test="withTrips">
    <ul>
      <strong><s:text name="t3.menu.treatment.level0"/></strong>
      <li>
        <s:a action="configureComputeRF1" namespace="/level0">
          <s:text name="t3.menu.treatment.level0.launch"/>
        </s:a>
      </li>
    </ul>

    <ul>
      <strong><s:text name="t3.menu.treatment.level1"/></strong>
      <li>
        <s:a action="configureLevel1Configuration" namespace="/level1">
          <s:text name="t3.menu.treatment.level1.launch"/>
        </s:a>
      </li>
      <s:if test="containsLevel1Configuration">
        <li>
          <s:a action="removeLevel1Configuration" namespace="/level1">
            <s:text name="t3.menu.remove.level1.configuration"/>
          </s:a>
        </li>
      </s:if>
    </ul>

    <ul>
      <strong><s:text name="t3.menu.treatment.level2"/></strong>
      <li>
        <s:a action="configureLevel2Step1!input" namespace="/level2">
          <s:text name="t3.menu.treatment.level2.launch"/>
        </s:a>
      </li>

    </ul>

    <ul>
      <strong><s:text name="t3.menu.treatment.level3"/></strong>
      <li>
        <s:a action="configureLevel3Step1!input" namespace="/level3">
          <s:text name="t3.menu.treatment.level3.launch"/>
        </s:a>
      </li>
    </ul>
  </s:if>
  <s:if test="withReferences">
    <ul>
      <strong><s:text name="t3.menu.importExport"/></strong>
      <li>
        <s:a action="configureImportData!input" namespace="/io">
          <s:text name="t3.menu.io.importData"/>
        </s:a>
      </li>
      <s:if test="withTrips">
        <li>
          <s:a action="configureExport!input" namespace="/io">
            <s:text name="t3.menu.io.exportData"/>
          </s:a>
        </li>
      </s:if>
    </ul>
  </s:if>

  <ul>
    <strong><s:text name="t3.menu.admin"/></strong>
    <li>
      <s:a action="t3DatabaseDetail" namespace="/trip">
        <s:text name="t3.menu.admin.t3DatabaseDetail"/>
      </s:a>
    </li>
    <s:if test="userIsAdmin">
      <li>
        <s:a action="userList" namespace="/user">
          <s:text name="t3.menu.admin.user"/>
        </s:a>
      </li>
      <s:if test="withTrips">
        <li>
          <s:a action="tripList" namespace="/trip">
            <s:text name="t3.menu.admin.trips"/>
          </s:a>
        </li>
      </s:if>
    </s:if>
  </ul>

</div>
