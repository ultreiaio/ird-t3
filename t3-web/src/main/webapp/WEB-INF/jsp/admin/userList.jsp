<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>
<title><s:text name="t3.label.admin.user.list"/></title>
<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>
<s:url id="loadUrl" action="getUsers" namespace="/json" escapeAmp="false"/>

<s:url id="addUrl" action="userForm!input" namespace="/user" escapeAmp="false">
  <s:param name="userEditAction" value="'create'"/>
</s:url>

<s:url id="editUrl" action="userForm!input" namespace="/user" escapeAmp="false">
  <s:param name="userEditAction" value="'edit'"/>
</s:url>

<s:url id="delUrl" action="userForm!input" namespace="/user" escapeAmp="false">
  <s:param name="userEditAction" value="'delete'"/>
</s:url>

<script type="text/javascript">

  jQuery(document).ready(function () {
    $.addRowSelectTopic('users');
    $.addClearSelectTopic('users');
    $.addAddRowTopic('users', '${addUrl}');
    $.addSingleRowTopic('users', 'Update', '${editUrl}', 'user.id');
    $.addSingleRowTopic('users', 'Delete', '${delUrl}', 'user.id');
  });
</script>

<h2><s:text name="t3.label.admin.user.list"/></h2>
<br/>

<sjg:grid id="users" caption="%{getText('t3.common.users')}"
          dataType="json" href="%{loadUrl}" gridModel="users"
          pager="true" pagerButtons="false" pagerInput="false"
          navigator="true"
          rownumbers="false"
          autowidth="true"
          onSelectRowTopics='users-rowSelect'
          onCompleteTopics='users-cleanSelect'
          navigatorEdit="false"
          navigatorDelete="false"
          navigatorSearch="false"
          navigatorRefresh="false"
          navigatorAdd="false"
          editinline="false" resizable="true"
          height="100"
          navigatorExtraButtons="{
                add: { title : 'Ajouter', icon: 'ui-icon-plus', topic: 'users-rowAdd' },
                update: { title : 'Mettre à jour', icon: 'ui-icon-pencil', topic: 'users-rowUpdate' },
                delete : { title : 'Supprimer', icon: 'ui-icon-trash', topic: 'users-rowDelete' }
        }">
  <sjg:gridColumn name="id" title="id" hidden="true"/>
  <sjg:gridColumn name="login" width="600" title='%{getText("t3.common.login")}'
                  sortable="false"/>
  <sjg:gridColumn name="admin" title='%{getText("t3.common.admin")}'
                  sortable="false" width="100" formatter="checkbox"/>
</sjg:grid>

