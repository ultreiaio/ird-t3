<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<s:if test="databaseType== 'inputDatabase'">
  <s:set name="databaseAction">userInputDatabaseForm</s:set>
  <s:set name="databaseLabel">
    <s:text name="t3.common.userT3Database"/>
  </s:set>
</s:if>
<s:elseif test="databaseType =='outputDatabase'">
  <s:set name="databaseAction">userOutputDatabaseForm</s:set>
  <s:set name="databaseLabel">
    <s:text name="t3.common.userOutputDatabase"/>
  </s:set>
</s:elseif>

<s:if test="databaseEditAction == 'create'">
  <s:set name="title"><s:text name="t3.label.admin.databaseconfiguration.create"/></s:set>
</s:if>
<s:elseif test="databaseEditAction =='edit'">
  <s:set name="title"><s:text name="t3.label.admin.databaseconfiguration.edit"/></s:set>
</s:elseif>
<s:elseif test="databaseEditAction == 'delete'">
  <s:set name="title"><s:text name="t3.label.admin.databaseconfiguration.delete"/></s:set>
</s:elseif>

<title><s:property value="#title"/></title>

<h2><s:property value="#title"/></h2>

<s:if test="databaseEditAction == 'create'">
  <%--Create configuration--%>
  <s:form method="post" validate="true" namespace="/user">
    <fieldset>
      <legend>
        <s:property value="#databaseLabel"/>
      </legend>
      <s:hidden key="databaseEditAction" label=""/>
      <s:hidden key="databaseType" label=""/>
      <s:hidden name="userEditAction" value="edit"/>
      <s:hidden key="user.id" label=""/>
      <s:textfield key="databaseConfiguration.url" label="%{getText('t3.common.url')}" size="40" requiredLabel="true"/>
      <s:textfield key="databaseConfiguration.description" label="%{getText('t3.common.description')}" size="40"
                   requiredLabel="true"/>
      <s:textfield key="databaseConfiguration.login" label="%{getText('t3.common.login')}" size="40"
                   requiredLabel="true"/>
      <s:textfield key="databaseConfiguration.password" label="%{getText('t3.common.password')}" size="40"
                   requiredLabel="true"/>
    </fieldset>
    <br/>
    <s:submit action="%{databaseAction}!doCreate" key="t3.action.create" align="right"/>
  </s:form>
</s:if>
<s:elseif test="databaseEditAction == 'edit'">
  <%--Update configuration--%>
  <s:form method="post" namespace="/user">

    <fieldset>
      <legend>
        <s:property value="#databaseLabel"/>
      </legend>
      <s:hidden key="databaseEditAction" label=""/>
      <s:hidden key="databaseType" label=""/>
      <s:hidden name="userEditAction" value="edit"/>
      <s:hidden key="user.id" label=""/>
      <s:hidden name="databaseConfiguration.id" label=""/>
      <s:textfield key="databaseConfiguration.url" label="%{getText('t3.common.url')}" size="40" requiredLabel="true"/>
      <s:textfield key="databaseConfiguration.description" label="%{getText('t3.common.description')}" size="40"
                   requiredLabel="true"/>
      <s:textfield key="databaseConfiguration.login" label="%{getText('t3.common.login')}" size="40"
                   requiredLabel="true"/>
      <s:textfield key="databaseConfiguration.password" label="%{getText('t3.common.password')}" size="40"
                   requiredLabel="true"/>
    </fieldset>
    <br/>
    <s:submit action="%{databaseAction}!doUpdate" key="t3.action.save" align="right"/>
  </s:form>
</s:elseif>
<s:elseif test="databaseEditAction == 'delete'">
  <%--Delete configuration--%>
  <s:form method="post" validate="true" namespace="/user">
    <fieldset>
      <legend>
        <s:property value="#databaseLabel"/>
      </legend>
      <s:hidden name="databaseEditAction" label=""/>
      <s:hidden key="databaseType" label=""/>
      <s:hidden key="userEditAction" label="" value="edit"/>
      <s:hidden key="user.id" label=""/>
      <s:hidden name="databaseConfiguration.id" label=""/>
      <s:textfield key="databaseConfiguration.url" label="%{getText('t3.common.url')}" size="40" disabled="true"/>
      <s:textfield key="databaseConfiguration.description" label="%{getText('t3.common.description')}" size="40" disabled="true"/>
      <s:textfield key="databaseConfiguration.login" label="%{getText('t3.common.login')}" size="40" disabled="true"/>
    </fieldset>
    <br/>
    <s:submit action="%{databaseAction}!doDelete" key="t3.action.delete" align="right"/>
  </s:form>
</s:elseif>
<s:form method="post" namespace="/user">
  <s:hidden name="userEditAction" value="edit"/>
  <s:hidden key="user.id" label=""/>
  <s:submit action="userForm!input" key="t3.label.admin.backToUser" align="right"/>
</s:form>


