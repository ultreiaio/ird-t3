<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>
<s:set var="connectedUserLogin" value="%{#session.t3Session.user.login}"/>

<s:url id="inputLoadUrl" action="getUserT3Databases" namespace="/json" escapeAmp="false">
  <s:param name="userId" value="%{user.id}"/>
</s:url>

<s:url id="inputAddUrl" action="userInputDatabaseForm!input" namespace="/user" escapeAmp="false">
  <s:param name="user.id" value="%{user.id}"/>
  <s:param name="databaseEditAction" value="'create'"/>
</s:url>

<s:url id="inputUpdateUrl" action="userInputDatabaseForm!input" namespace="/user" escapeAmp="false">
  <s:param name="user.id" value="%{user.id}"/>
  <s:param name="databaseEditAction" value="'edit'"/>
</s:url>

<s:url id="inputDeleteUrl" action="userInputDatabaseForm!input" namespace="/user" escapeAmp="false">
  <s:param name="user.id" value="%{user.id}"/>
  <s:param name="databaseEditAction" value="'delete'"/>
</s:url>

<s:url id="outputLoadUrl" action="getUserOuputDatabases" namespace="/json" escapeAmp="false">
  <s:param name="userId" value="%{user.id}"/>
</s:url>

<s:url id="outputAddUrl" action="userOutputDatabaseForm!input" namespace="/user" escapeAmp="false">
  <s:param name="user.id" value="%{user.id}"/>
  <s:param name="databaseEditAction" value="'create'"/>
</s:url>

<s:url id="outputUpdateUrl" action="userOutputDatabaseForm!input" namespace="/user" escapeAmp="false">
  <s:param name="user.id" value="%{user.id}"/>
  <s:param name="databaseEditAction" value="'edit'"/>
</s:url>

<s:url id="outputDeleteUrl" action="userOutputDatabaseForm!input" namespace="/user" escapeAmp="false">
  <s:param name="user.id" value="%{user.id}"/>
  <s:param name="databaseEditAction" value="'delete'"/>
</s:url>

<script type="text/javascript" src="<s:url value='/js/gridHelper.js' />"></script>

<s:if test="userEditAction == 'create'">
  <s:set name="title"><s:text name="t3.label.admin.user.create"/></s:set>
</s:if>
<s:elseif test="userEditAction =='edit'">
  <s:set name="title"><s:text name="t3.label.admin.user.edit"/></s:set>
  <script type="text/javascript">

    jQuery(document).ready(function () {

      var paramId = 'databaseConfiguration.id';

      var gridId = 'inputDatabases';
      $.addRowSelectTopic(gridId);
      $.addClearSelectTopic(gridId);
      $.addAddRowTopic(gridId, '${inputAddUrl}');
      $.addSingleRowTopic(gridId, 'Update', '${inputUpdateUrl}', paramId);
      $.addSingleRowTopic(gridId, 'Delete', '${inputDeleteUrl}', paramId);

      gridId = 'outputDatabases';
      $.addRowSelectTopic(gridId);
      $.addClearSelectTopic(gridId);
      $.addAddRowTopic(gridId, '${outputAddUrl}');
      $.addSingleRowTopic(gridId, 'Update', '${outputUpdateUrl}', paramId);
      $.addSingleRowTopic(gridId, 'Delete', '${outputDeleteUrl}', paramId);
    });
  </script>
</s:elseif>
<s:elseif test="userEditAction == 'delete'">
  <s:set name="title"><s:text name="t3.label.admin.user.delete"/></s:set>
</s:elseif>

<title><s:property value="#title"/></title>

<h2><s:property value="#title"/></h2>

<s:if test="userEditAction == 'create'">

  <%--Create user--%>
  <s:form method="post" validate="true" namespace="/user">
    <fieldset>
      <legend>
        <s:text name="t3.common.configuration"/>
      </legend>
      <s:hidden key="userEditAction" label=""/>
      <s:textfield key="user.login" label="%{getText('t3.common.login')}" size="40" requiredLabel="true"/>
      <s:textfield key="user.password" label="%{getText('t3.common.password')}" size="40" requiredLabel="true"/>
      <s:textfield name="checkPassword" value="" key="t3.common.checkPassword" size="40" requiredLabel="true"/>
      <s:checkbox key="user.admin" label="%{getText('t3.common.admin')}"/>
    </fieldset>
    <br/>
    <div class="actions">
    <s:submit action="userForm!doCreate" key="t3.action.create" align="right"/>
    <s:if test="userIsAdmin">
      <s:submit action="userList" key="t3.label.admin.backToUserList" align="right"/>
    </s:if>
    </div>
  </s:form>
</s:if>
<s:elseif test="userEditAction == 'edit'">
  <%--Update user--%>
  <s:form method="post" namespace="/user">
    <fieldset>
      <legend>
        <s:text name="t3.common.configuration"/>
      </legend>
      <s:hidden key="user.id" label=""/>
      <s:hidden key="user.login" label=""/>
      <s:hidden key="userEditAction" label=""/>
      <s:textfield key="user.login" label="%{getText('t3.common.login')}" size="40" disabled="true"/>
      <s:textfield name="user.password" value="" key="t3.common.password" size="40"/>
      <s:textfield name="checkPassword" value="" key="t3.common.checkPassword" size="40"/>
      <s:if test="userIsAdmin">
        <s:checkbox key="user.admin" value="%{user.admin}" label="%{getText('t3.common.admin')}"/>
        <s:else>
          <s:hidden key="user.admin" label=""/>
          <s:checkbox value="%{user.admin}" key="t3.common.admin" disabled="true"/>
        </s:else>
      </s:if>
    </fieldset>
    <p><s:text name="t3.label.info.changePassword"/></p>
    <br/>
    <div class="actions">
    <s:submit action="userForm!doUpdate" key="t3.action.save" align="right"/>
    <s:if test="userIsAdmin">
      <s:submit action="userList" key="t3.label.admin.backToUserList" align="right"/>
    </s:if>
    </div>
  </s:form>

  <br/>
  <sjg:grid id="inputDatabases" caption="%{getText('t3.common.user.inputDatabases')}"
            dataType="json" href="%{inputLoadUrl}" gridModel="databases"
            pager="true" pagerButtons="false" pagerInput="false"
            navigator="true"
            rownumbers="false"
            onSelectRowTopics='inputDatabases-rowSelect'
            onCompleteTopics='inputDatabases-cleanSelect'
            navigatorEdit="false"
            autowidth="true"
            navigatorDelete="false"
            navigatorSearch="false"
            navigatorRefresh="false"
            navigatorAdd="false"
            editinline="false" resizable="true"
            height="100"
            navigatorExtraButtons="{
                add: { title : 'Ajouter', icon: 'ui-icon-plus', topic: 'inputDatabases-rowAdd' },
                update: { title : 'Mettre à jour', icon: 'ui-icon-pencil', topic: 'inputDatabases-rowUpdate' },
                delete : { title : 'Supprimer', icon: 'ui-icon-trash', topic: 'inputDatabases-rowDelete' }
        }">
    <sjg:gridColumn name="id" title="id" hidden="true"/>
    <sjg:gridColumn name="description" width="400" title='%{getText("t3.common.description")}' sortable="false"/>
    <sjg:gridColumn name="url" title='%{getText("t3.common.url")}' sortable="false" width="400"/>
    <sjg:gridColumn name="login" title='%{getText("t3.common.login")}' sortable="false" width="400"/>
  </sjg:grid>

  <br/>
  <sjg:grid id="outputDatabases"
            caption="%{getText('t3.common.user.outputDatabases')}"
            dataType="json" href="%{outputLoadUrl}" gridModel="databases"
            pager="true" pagerButtons="false" pagerInput="false"
            navigator="true"
            rownumbers="false"
            autowidth="true"
            onSelectRowTopics='outputDatabases-rowSelect'
            onCompleteTopics='outputDatabases-cleanSelect'
            navigatorEdit="false"
            navigatorDelete="false"
            navigatorSearch="false"
            navigatorRefresh="false"
            navigatorAdd="false"
            editinline="false" resizable="true"
            height="100"
            navigatorExtraButtons="{
                add: { title : 'Ajouter', icon: 'ui-icon-plus', topic: 'outputDatabases-rowAdd' },
                update: { title : 'Mettre à jour', icon: 'ui-icon-pencil', topic: 'outputDatabases-rowUpdate' },
                delete : { title : 'Supprimer', icon: 'ui-icon-trash', topic: 'outputDatabases-rowDelete' }
        }">
    <sjg:gridColumn name="id" title="id" hidden="true"/>
    <sjg:gridColumn name="description" width="400" title='%{getText("t3.common.description")}' sortable="false"/>
    <sjg:gridColumn name="url" title='%{getText("t3.common.url")}' sortable="false" width="400"/>
    <sjg:gridColumn name="login" title='%{getText("t3.common.login")}' sortable="false" width="400"/>
  </sjg:grid>
</s:elseif>
<s:elseif test="userEditAction == 'delete'">
  <%--Delete user--%>
  <s:form method="post" validate="true" namespace="/user">
    <fieldset>
      <legend>
        <s:text name="t3.common.configuration"/>
      </legend>
      <s:hidden name="user.id" label=""/>
      <s:hidden name="user.login" label=""/>
      <s:hidden name="user.admin" label=""/>
      <s:hidden name="userEditAction" label=""/>
      <s:textfield key="user.login" label="%{getText('t3.common.login')}" size="40" disabled="true"/>
      <s:checkbox value="%{user.admin}" key="t3.common.admin" disabled="true"/>
    </fieldset>
    <br/>
    <div class="actions">
    <s:submit action="userForm!doDelete" key="t3.action.delete" align="right"/>
    <s:if test="userIsAdmin">
      <s:submit action="userList" key="t3.label.admin.backToUserList" align="right"/>
    </s:if>
    </div>
  </s:form>
</s:elseif>



