<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>
<s:set name="title"><s:text name="t3.label.admin.trip.delete.trips"/></s:set>

<title><s:property value="#title"/></title>

<h2><s:property value="#title"/></h2>

<s:form method="post" namespace="/trip">
  <fieldset>
    <legend>
      <s:text name="t3.common.trips.to.treat"/>
    </legend>
    <ul>
      <s:iterator value="tripsToDelete" status="status">
        <s:hidden name="tripIds" value="%{key}"/>
        <li>
          <s:property value="value"/>
        </li>
      </s:iterator>
    </ul>
    <s:hidden name="tripEditAction" label=""/>
  </fieldset>
  <br/>
  <div class="actions">
  <s:submit action="confirmTripDelete!doDelete" key="t3.action.deleteTrips" align="right"/>
  <s:submit action="confirmTripDelete!doDeleteComputedData" key="t3.action.deleteComputedData" align="right"/>
  <s:submit action="backToTripList" key="t3.label.admin.backToTripList" align="right"/>
  </div>
</s:form>





