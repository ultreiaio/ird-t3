<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="d" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<%-- metas in head --%>
<%@ include file="/WEB-INF/includes/metas.jsp" %>

<body>

<%-- header --%>
<%@ include file="/WEB-INF/includes/header.jsp" %>

<%-- body --%>
<style type="text/css">
  .wwlbl {
    width: 500px;
  }
</style>
<div id="body">

  <s:if test="hasActionMessages()">
    <div class="info_success">
      <s:actionmessage/>
    </div>
  </s:if>

  <s:if test="hasActionErrors()">
    <div class="info_error">
      <s:actionerror/>
    </div>
  </s:if>

  <d:body/>

  <p>
    <s:if test="t3ActionContext.errorMessages.empty">
      <s:text name="t3.label.treatment.result.ok">
        <s:param>
          <s:property value="%{totalTime}"/>
        </s:param>
      </s:text>
    </s:if>
    <s:else>
      <s:text name="t3.label.treatment.result.fail">
        <s:param>
          <s:property value="%{totalTime}"/>
        </s:param>
      </s:text>
    </s:else>
  </p>

  <sj:tabbedpanel id="messagesTabs" cssClass="hide">

    <sj:tab id="tab0" target="tresumes"
            label="%{getText('t3.label.treatment.result.resumeMessages')}"/>
    <s:if test="!t3ActionContext.errorMessages.empty">
      <sj:tab id="tab1" target="terrors"
              label="%{getText('t3.label.treatment.result.errorMessages')} (%{t3ActionContext.errorMessages.size})"/>
    </s:if>
    <s:if test="!t3ActionContext.warnMessages.empty">
      <sj:tab id="tab2" target="twarnings"
              label="%{getText('t3.label.treatment.result.warningMessages')} (%{t3ActionContext.warnMessages.size})"/>
    </s:if>
    <s:if test="!t3ActionContext.infoMessages.empty">
      <sj:tab id="tab3" target="tinfos"
              label="%{getText('t3.label.treatment.result.infoMessages')} (%{t3ActionContext.infoMessages.size})"/>
    </s:if>
    <div id="tresumes">
      <pre><s:property value="%{t3ActionContext.resume}"/></pre>
    </div>
    <s:if test="!t3ActionContext.errorMessages.empty">
      <div id="terrors">
        <pre><s:property value="t3ActionContext.errorMessagesAsStr"/></pre>
      </div>
    </s:if>

    <s:if test="!t3ActionContext.warnMessages.empty">
      <div id="twarnings">
        <pre><s:property value="t3ActionContext.warnMessagesAsStr"/></pre>
      </div>
    </s:if>

    <s:if test="!t3ActionContext.infoMessages.empty">
      <div id="tinfos">
        <pre><s:property value="t3ActionContext.infoMessagesAsStr" /></pre>
      </div>
    </s:if>
  </sj:tabbedpanel>

</div>

<%-- footer --%>
<%@ include file="/WEB-INF/includes/footer.jsp" %>


<script type="text/javascript">
  jQuery(document).ready(function () {
    $('#messagesTabs').show();
  });
</script>
</body>
</html>
