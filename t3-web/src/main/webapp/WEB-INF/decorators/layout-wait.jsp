<%--
  #%L
  T3 :: Web
  %%
  Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="d" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<%-- metas in head --%>
<%@ include file="/WEB-INF/includes/metas.jsp" %>

<body>

<%-- header --%>
<%@ include file="/WEB-INF/includes/header.jsp" %>
<style type="text/css">
  .wwlbl {
    width: 500px;
  }
</style>
<%-- body --%>

<div id="body">

  <d:body/>

  <%--<br/>--%>
  <%--<sj:progressbar value="%{t3ActionContext.progression}"/>--%>

</div>

<%-- footer --%>
<%@ include file="/WEB-INF/includes/footer.jsp" %>

<sj:dialog id="waitDialog" modal='true' resizable='false'
           title="%{getText('t3.title.action.inprogress')}" width="500">
  <p>
    <s:text name="t3.information.action.inprogress">
      <s:param><s:property value="t3ActionContext.actionName"/></s:param>
    </s:text>
    <hr/>
    <s:text name="t3.information.action.inprogress.wait"/>
  </p>
  <sj:progressbar value="%{t3ActionContext.progression}"/>
</sj:dialog>

<script type="text/javascript">
  $(document).ready(function () {
    $("#waitDialog").dialog();
    $('[role="button"]').hide()
  });
</script>
</body>
</html>
