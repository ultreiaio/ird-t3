# Modélisation

## Présentation

On donne ici des explications sur le modèle de T3.

Le modèle conçu sous argoUML est téléchargeable [ici](model/t3-persistence.zargo).

## Référentiel

- [Toutes les entités du référentiel](model/ReferencesAll.png).
- [Bateau](model/ReferencesVessel.png).
- [Catégories poids](model/ReferencesWeightCategories.png).
- [Espece](model/ReferencesSpecies.png).
- [Ocean](model/ReferencesOcean.png).
- [Port](model/ReferencesHarbour.png).
- [Pays](model/ReferencesCountry.png).
- [Zone](model/ReferencesZones.png).

## Données thématiques

- [Toutes les données thématiques (sans calcul)](model/DataAll.png)
- [Marée](model/DataTrip.png)
- [Activité (sans calcul)](model/DataActivity.png)
- [Activité N0](model/DataActivityN0.png)
- [Activité N1](model/DataActivityN1.png)
- [Activité N3](model/DataActivityN3.png)
- [Echantillon (sans calcul)](model/DataSample.png)
- [Echantillon N1](model/DataSampleN1.png)
- [Cuve (sans calcul)](model/DataWell.png)
- [Cuve N1](model/DataWellN1.png)
- [Raising Factor 2](model/DataRaisingFactor2.png)
