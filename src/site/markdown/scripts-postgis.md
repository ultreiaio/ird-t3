# Scripts postgis disponibles

## Présentation

Cette page permet de télécharger les scripts de remplissage des zones
optionnelles lors d'une installation de T3+.

 * [Zones FAO](postgis/zones-fao-2011.sql.zip) : Zone FAO version 2011
 * [Zones EE](postgis/zones-ee-detail-2011.sql.zip)  : Zone EE détaillée 2011
 * [Zones CWP 1x1](postgis/zones-cwp-1x1-2011.sql.zip)  : Zone CWP de carée 1 2011
 * [Zones CWP 5x5](postgis/zones-cwp-5x5-2011.sql.zip)  : Zone CWP de carée 5 2011
 * [Zones CWP 10x10](postgis/zones-cwp-10x10-2011.sql.zip)  : Zone CWP de carée 10 2011

Veuillez placer les archives dans le répertoire **scripts/postigs-data** avant
de lancer l'installeur.
