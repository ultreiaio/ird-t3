# Installation T3

## Présentation

Ce document présente l'installation de l'application T3+.

L'application s'installe dans un conteneur web java (tomcat, jetty, ...).

Elle se déroule en deux étapes :

 * récupération de l'application
 * préparation de la configuration
 * installation de l'application

## Récupération de l'application

Récupérez la dernière application depuis [la page des téléchargements](https://gitlab.com/ultreiaio/ird-t3/blob/develop/CHANGELOG.md).

## Préparation de la configuration

L'application utilise un fichier de configuration nommé **t3.conf**.

### Emplacement du fichier de configuration

Ce fichier sera recherché à 5 niveaux :

 * dans le class-path (dans un des jar de l'application)
 * dans le répertoire système (/etc sous linux et C:\windows\System32 sous windows)
 * dans le répertoire utilisateur (~/.config sous linux et ~/Application Data)
 * dans le répertoire où est lancé l'application

Nous préconisons d'utiliser une configuration dans le répertoire système car en
toute logique il s'agit d'une application en mode serveur et que cela nous évite
des problèmes avec les utilisateurs réels utilisés sous windows.

Ce fichier est optionnel si on utilise toutes les options de configuration de
l'application (uniquement sous linux).

Pour connaître tous les options disponibles, rendez-vous à la [page de configuration](./application-config-report.html).

#### Installation sous linux

Pour une installation sur un serveur linux, on préconise l'utilisation du
répertoire */var/local/t3* pour placer les fichiers de l'application, vu que
c'est la configuration par défaut, on peut donc éviter de créer cette configuration.

#### Installation sous windows

Pour installer l'application sous Windows, il faut cependant toujours
surcharger la propriété **data.dir** de ce fichier car sa valeur par défaut est
*/var/local/t3* qui ne fonctionne pas sous windows.

Créer un fichier *t3.conf* dans le répertoire *C:\windows\System32* et y
placer le contenu

```
data.directory=c:\\MonRepertoireOuMettreLesDonneesDeT3\\${context.path}
```

Attention à bien échapper tous les \ par des \\

## Installation de l'application

Il suffit ensuite juste de déployer le war récupérer dans votre conteneur web.

# Création de la base de données

L'application utilise une base postgres avec l'extension postgis.

## Création template postgis

Cette étape n'est pas obligatoire sous windows (cela peut être fait via
l'installeur d'extension de pg).

Pour postgres 9.1

```
sudo su postgres
createdb template_postgis -E UTF-8 -T template1
createlang plpgsql template_postgis
psql -f /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql template_postgis
psql -f /usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql template_postgis
psql -c "GRANT ALL ON geometry_columns TO public" template_postgis
psql -c "GRANT ALL ON spatial_ref_sys TO public" template_postgis
psql -c "VACUUM FREEZE" template_postgis
psql -c "UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template_postgis'"
psql -c "UPDATE pg_database SET datallowconn = FALSE WHERE datname = 'template_postgis'"
exit
```

Pour postgres 8.4

```
sudo su postgres
createdb template_postgis -E UTF-8 -T template1
createlang plpgsql template_postgis
psql -f /usr/share/postgresql/8.4/contrib/postgis.sql template_postgis
psql -f /usr/share/postgresql/8.4/contrib/spatial_ref_sys.sql template_postgis
psql -c "GRANT ALL ON geometry_columns TO public" template_postgis
psql -c "GRANT ALL ON spatial_ref_sys TO public" template_postgis
psql -c "VACUUM FREEZE" template_postgis
psql -c "UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template_postgis'"
psql -c "UPDATE pg_database SET datallowconn = FALSE WHERE datname = 'template_postgis'"
exit
```

## Création base postgres

```
sudo su postgres
createuser -U postgres -sdRP t3-admin
createdb -U postgres -E UTF-8 -O t3-admin -T template_postgis t3
exit
```

# Remplissage de la base de données

Une fois la base physique t3+ créée, il faut désormais la remplir avec :

- son schéma
- son référentiel
- ses zones postgis

Un utilitaire est fourni pour effectuer ce remplissage.

Veuillez vous rendre sur la page d'explication pour utiliser [l'installeur](./installer.html) et
terminer la configuration de l'application.

## Préparation balbaya

Ajout de la séquence pour obtenir le numéro de traitement

```
CREATE SEQUENCE a_jeu_seq;
select max(id_jeu_d) from a_jeu_d;
alter sequence a_jeu_seq start with ?;

```