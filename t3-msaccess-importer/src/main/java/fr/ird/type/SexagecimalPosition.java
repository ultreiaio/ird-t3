/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ird.type;

import java.io.Serializable;
import java.util.Objects;

/**
 * Le modèle d'une position au format sexagecimal (degre - minute - seconde).
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.2
 */
public class SexagecimalPosition implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer degre;
    private Integer minute;
    private Integer seconde;

    private SexagecimalPosition() {
        // contructeur non publique, on prefere l'utilisation de la methode
        // #valueOf()
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * decimal.
     * <p/>
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur au format decimal
     * @return une nouvelle instance de position convertie
     */
    public static SexagecimalPosition valueOf(Float decimal) {
        SexagecimalPosition r = new SexagecimalPosition();
        r.update(decimal);
        return r;
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * degre-minute-seconde.
     *
     * @param d la valeur des degres
     * @param m la valeur des minutes
     * @param s la valeur des secondes
     * @return une nouvelle instance de position convertie
     */
    public static SexagecimalPosition valueOf(int d, int m, int s) {
        SexagecimalPosition r = new SexagecimalPosition();
        r.setDegre(d);
        r.setMinute(m);
        r.setSeconde(s);

        return r;
    }

    /**
     * @return {@code true} si aucune composante n'est renseigné, {@code false} autrement.
     */
    public boolean isNull() {
        return degre == null && minute == null && seconde == null;
    }

    /**
     * Mets a jour les composants de la position a partir d'une valeur decimal.
     * <p/>
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur decimale a convertir (qui peut etre nulle).
     */
    public void update(Float decimal) {
        Integer d = null;
        Integer m = null;
        Integer s = null;
        if (decimal != null) {
            int remain = 0;

            d = (int) (Math.round(decimal + 0.5) - 1);
            m = 0;
            s = 0;
            decimal = 60 * (decimal - d);
            if (decimal > 0) {
                m = (int) (Math.round(decimal + 0.5) - 1);
                decimal = 60 * (decimal - m);
                if (decimal > 0) {
                    s = (int) (Math.round(decimal + 0.5) - 1);
                    remain = (int) (10 * (decimal - s));
                }
            }
            if (remain > 9) {
                s++;
//                remain = 0;
            }
            if (s == 60) {
                m++;
                s = 0;
            }
            if (m == 60) {
                d++;
                m = 0;
            }
        }
        degre = d;
        minute = m;
        seconde = s;
    }

    public Float toDecimal() {
        if (isNull()) {
            return null;
        }
        Integer d = degre == null ? 0 : degre;
        Integer m = minute == null ? 0 : minute;
        Integer s = seconde == null ? 0 : seconde;
        Float result = Float.valueOf(d);
        if (m > 0) {
            result += (float) m / 60;
            if (s == 0) {
                result += 0.5f / 3600;
            }
        }
        if (s > 0) {
            result += ((float) s + 0.5f) / 3600;
        }
        return result;
    }

    public Integer getDegre() {
        return degre;
    }

    public void setDegre(Integer degre) {
        this.degre = degre;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSeconde() {
        return seconde;
    }

    public void setSeconde(Integer seconde) {
        this.seconde = seconde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SexagecimalPosition that = (SexagecimalPosition) o;
        return Objects.equals(degre, that.degre) &&
                Objects.equals(minute, that.minute) &&
                Objects.equals(seconde, that.seconde);
    }

    @Override
    public int hashCode() {

        return Objects.hash(degre, minute, seconde);
    }
}
