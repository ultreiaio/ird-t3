/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.converter;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;


import static org.nuiton.i18n.I18n.t;

/**
 * A float converter which is not dependant on user locale to obtain the locale
 * {@code dot} representation.
 * <p/>
 * It can transform {@code 0.2} and also {@code 0, 2}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class FloatConverter implements Converter {

    @Override
    public Object convert(Class aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    t("nuitonutil.error.convertor.noValue", this));
        }
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return result;
            }
            if (value instanceof String) {
                result = valueOf((String) value);
                return result;
            }
        }
        throw new ConversionException(
                t("nuitonutil.error.no.convertor", aClass.getName(), value));
    }

    protected Float valueOf(String value) {
        try {
            if (value.contains(",")) {
                value = value.replaceAll(",", ".");
            }
            Float result;
            result = Float.valueOf(value);
            return result;
        } catch (NumberFormatException e) {
            throw new ConversionException(
                    t("nuitonutil.error.float.convertor", value, this, e.getMessage()));
        }
    }


    protected boolean isEnabled(Class<?> aClass) {
        return Float.class.equals(aClass);
    }

    public Class<?> getType() {
        return Float.class;
    }
}
