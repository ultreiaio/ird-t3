/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.type;

import fr.ird.type.SexagecimalPosition;

/**
 * Un type pour l'import access qui permet de récupérer un coordonnee decimale, à
 * partir d'un {@code int}.
 * <p/>
 * Les deux derniers de l'int sont les minutes et optionellement les premiers sont des degrès.
 * <p/>
 * Exemples:
 * Voici les valeurs utilisées :
 * <ul>
 * <li>00 = 0°0'</li>
 * <li>59 = 0°59'</li>
 * <li>100 = 1°0'</li>
 * <li>159 = 1°59'</li>
 * <li>1159 = 11°59'</li>
 * </ul>
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.2
 */
public class IntToCoordonne {

    protected final Integer intValue;

    public IntToCoordonne(Integer intValue) {
        this.intValue = intValue;
    }

    public Integer getIntValue() {
        return intValue;
    }

    public Float getFloatValue() {
        if (intValue == null) {
            return null;
        }
        String strValue = (intValue + "").trim();
        int degre = 0;
        int minite = 0;
        switch (strValue.length()) {
            case 1:
            case 2:
                minite = intValue;
                break;
            case 3:
                degre = Integer.valueOf(strValue.charAt(0) + "");
                minite = Integer.valueOf(strValue.substring(1));
                break;
            case 4:
                degre = Integer.valueOf(strValue.substring(0, 2));
                minite = Integer.valueOf(strValue.substring(2));
        }

        SexagecimalPosition position =
                SexagecimalPosition.valueOf(degre, minite, 0);
        return position.toDecimal();
    }
}
