/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Un petit utilitaire pour obtenir les informations sur les tables d'une base access.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AnalyseAccessDB {

    @SuppressWarnings({"unchecked", "ConstantForZeroLengthArrayAllocation"})
    private static final AbstractAccessEntityMeta<TopiaEntityEnum>[] ABSTRACT_ACCESS_ENTITY_METAS = new AbstractAccessEntityMeta[0];

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Must at least have one argument to db location");
        }
        File f = new File(args[0]);

        AbstractAccessDataSource<?, ?> source = new DummyAccessDataSource(f);

        source.init();

        Set<String> tableNames = source.getUnusedTableNames();
        StringBuilder buffer = new StringBuilder();
        buffer.append("Detect ").append(tableNames.size()).append(" tables on db ").append(f);
        for (String tableName : tableNames) {
            Set<String> columns = source.getUnusedTableColumns(tableName);
            buffer.append("\n").append(tableName).append(" : ").append(columns);
            Database sourceDb = source.getConnexion();
            Table table = sourceDb.getTable(tableName);
            buffer.append(" with ").append(source.getRowCount(table)).append(" entries.");
        }
        buffer.append("\n");
        System.out.append(buffer.toString());
        System.out.flush();
    }

    public static class DummyMetaProvider implements AccessEntityMetaProvider<TopiaEntityEnum, AbstractAccessEntityMeta<TopiaEntityEnum>> {

        @SuppressWarnings("unchecked")
        @Override
        public Set getMetas() {
            return Collections.emptySet();
        }
    }

    private static class DummyAccessDataSource extends AbstractAccessDataSource<TopiaEntityEnum, AbstractAccessEntityMeta<TopiaEntityEnum>> {
        DummyAccessDataSource(File f) {
            super(AbstractAccessEntityMeta.class, DummyMetaProvider.class, f);
        }

        @Override
        protected AbstractAccessEntityMeta<TopiaEntityEnum>[] newMetaArray(Collection<AbstractAccessEntityMeta<TopiaEntityEnum>> iterable) {
            return ABSTRACT_ACCESS_ENTITY_METAS;
        }

        @Override
        protected void onTableMissing(AbstractAccessEntityMeta<TopiaEntityEnum> meta) {
        }

        @Override
        protected void onPropertyMissing(AbstractAccessEntityMeta<TopiaEntityEnum> meta, String property, String column) {
        }

        @Override
        protected void onPKeyMissing(AbstractAccessEntityMeta<TopiaEntityEnum> meta, String pKey) {
        }
    }
}
