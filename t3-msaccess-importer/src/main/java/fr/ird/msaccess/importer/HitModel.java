/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Modèle générique de hits.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class HitModel<K extends Serializable, H extends HitModel<K, H>> implements Iterable<Map.Entry<K, Long>>, Serializable {

    private static final long serialVersionUID = 1L;

    /** for properties change support */
    private final PropertyChangeSupport pcs;

    /** l'enregistreur de hit */
    protected final SortedMap<K, Long> hits;

    private final Class<K> type;

    protected abstract H newModel();

    public HitModel(Class<K> type) {
        this.type = type;
        pcs = new PropertyChangeSupport(this);
        hits = new TreeMap<>();
    }

    public long getTotalHit() {
        long result = 0L;
        for (Map.Entry<K, Long> entry : this) {
            result += entry.getValue();
        }
        return result;
    }

    public long getHit(K type) {
        Long result = hits.get(type);
        return result == null ? 0 : result;
    }

    @Override
    public Iterator<Map.Entry<K, Long>> iterator() {
        return hits.entrySet().iterator();
    }

    public void addHit(K type) {
        long result = getHit(type);
        addHit0(type, true, result, result + 1);
    }

    public H getSnapshot() {
        H result = newModel();
        for (Map.Entry<K, Long> entry : this) {
            result.addHit0(entry.getKey(), false, 0L, entry.getValue());
        }
        return result;
    }

    public H applyTo(H before) {
        H result = newModel();
        for (Map.Entry<K, Long> entry : this) {
            K key = entry.getKey();
            long beforeHit = before.getHit(key);
            long nowHit = entry.getValue();
            result.addHit0(key, false, 0L, nowHit - beforeHit);
        }
        return result;
    }

    @SuppressWarnings({"unchecked"})
    public K[] getKeys() {
        Set<K> set = hits.keySet();
        K[] result = (K[]) Array.newInstance(type, set.size());
        int i = 0;
        for (K k : set) {
            result[i++] = k;
        }
        return result;
    }

    public void clear() {
        hits.clear();
    }

    protected void addHit0(K type,
                           boolean fire,
                           Long old,
                           Long result) {
        hits.put(type, result);
        if (fire) {
            firePropertyChange(type.toString(), old, result);
        }
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }


    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void firePropertyChange(String propertyName, Object newValue) {
        pcs.firePropertyChange(propertyName, null, newValue);
    }
}
