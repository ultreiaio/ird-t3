/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * A visitor of {@link AccessEntity}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AccessEntityVisitor<T extends TopiaEntityEnum, M extends AbstractAccessEntityMeta<T>, E extends AccessEntity<T>> implements TopiaEntityVisitor {

    private static final Log log = LogFactory.getLog(AccessEntityVisitor.class);

    protected boolean strictCheck;

    public abstract void onStart(E entity, M meta);

    public abstract void onEnd(E entity, M meta);

    public abstract void onVisitSimpleProperty(String propertyName, Class<?> type, E entity, M meta);

    public abstract void onVisitComposition(String propertyName, Class<?> type, E entity, M meta);

    public abstract void onVisitReverseAssociation(String propertyName, E entity, M meta);

    public abstract void onVisitAssociation(String propertyName, Class<?> type, E entity, M meta);

    public boolean isStrictCheck() {
        return strictCheck;
    }

    public void setStrictCheck(boolean strictCheck) {
        this.strictCheck = strictCheck;
    }

    @Override
    public final void start(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug(entity);
        }
        E e = (E) entity;
        M meta = (M) e.getMeta();

        onStart(e, meta);
    }

    @Override
    public final void end(TopiaEntity entity) {
        if (log.isDebugEnabled()) {
            log.debug(entity);
        }
        E e = (E) entity;
        M meta = (M) e.getMeta();
        onEnd(e, meta);
    }

    @Override
    public final void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
        log.debug(entity);
        E current = (E) entity;
        M meta = (M) current.getMeta();
        if (TopiaEntity.class.isAssignableFrom(type)) {
            if (meta.containsReverseAssociation(propertyName)) {
                // il s'agit d'une composition (le reverse d'une association)
                onVisitReverseAssociation(propertyName, current, meta);
            } else {
                boolean safe = checkComposition(propertyName, meta);
                if (safe) {
                    // chargement d'une composition
                    onVisitComposition(propertyName, type, current, meta);
                }
            }
        } else {
            boolean safe = checkSimpleProperty(propertyName, meta);
            if (safe) {
                // visite d'une propriété simple
                onVisitSimpleProperty(propertyName, type, current, meta);
            }
        }
    }

    @Override
    public final void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
        if (!TopiaEntity.class.isAssignableFrom(type)) {
            // ce n'est pas une association (ce cas ne devrait jamais arrivé)
            return;
        }
        log.debug(entity);
        E current = (E) entity;
        M meta = (M) current.getMeta();
        boolean safe = checkAssociation(propertyName, meta);
        if (safe) {
            // by default we visit the association
            onVisitAssociation(propertyName, type, current, meta);
        }
    }

    @Override
    public final void visit(TopiaEntity entity,
                            String propertyName,
                            Class<?> collectionType,
                            Class<?> type,
                            int index,
                            Object value) {
        // on n'utilise jamais cette methode
    }

    public Serializable getProperty(String propertyName, M meta, Map<String, Object> row) {
        String colName = meta.getPropertyColumnName(propertyName);
        return getProperty(colName, row);
    }

    public Serializable getProperty(String colName, Map<String, Object> row) {
        Serializable newValue = (Serializable) row.get(colName);
        if (newValue == null) {
            // rien a faire si la composition est nulle
            return null;
        }
        if (newValue instanceof Short) {
            // on ne peut pas utiliser de short (les clef metiers du referentiel sont des int)
            newValue = Integer.valueOf(newValue + "");
        }
        return newValue;
    }

    protected final <C extends TopiaEntity> C getEntityForPKey(Object[] pKey, Collection<C> entities) {
        if (entities == null) {
            return null;
        }
        C result = null;
        for (C entity : entities) {
            E accessEntity = (E) entity;
            Object[] entityPKey = accessEntity.getPKey();
            if (Arrays.equals(entityPKey, pKey)) {
                // found the correct tank which match the pKey
                result = entity;
                break;
            }
        }
        return result;
    }

    protected boolean checkSimpleProperty(String propertyName,
                                          M meta) {
        if (!meta.containsProperty(propertyName)) {
            String message = String.format("Skip simpleProperty [%s - %s], not supported (should not be visited...)", meta.getType(), propertyName);

            if (strictCheck) {
                throw new IllegalStateException(message);
            } else {
                log.warn(message);
                return false;
            }
        }
        return true;
    }

    protected boolean checkComposition(String propertyName, M meta) {
        if (!meta.containsProperty(propertyName)) {
            String message = String.format("Skip composition [%s - %s], not supported (should not be visited...)", meta.getType(), propertyName);
            if (strictCheck) {
                throw new IllegalStateException(message);
            } else {
                log.warn(message);
                return false;
            }
        }
        return true;
    }

    protected boolean checkReverseAssociation(String propertyName, M meta) {
        if (!meta.containsReverseAssociation(propertyName)) {
            String message = String.format("Skip reverse association [%s - %s], not supported (should not be visited...)", meta.getType(), propertyName);
            if (strictCheck) {
                throw new IllegalStateException(message);
            } else {
                log.warn(message);
                return false;
            }
        }
        return true;
    }

    protected boolean checkAssociation(String propertyName, M meta) {
        if (!meta.containsAssociation(propertyName)) {
            String message = String.format("Skip association [%s - %s], not supported (should not be visited...)", meta.getType(), propertyName);
            if (strictCheck) {
                throw new IllegalStateException(message);
            } else {
                log.warn(message);
                return false;
            }
        }
        return true;
    }

    @Override
    protected void finalize() throws Throwable {
        clear();
        super.finalize();
    }
}
