/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import org.apache.commons.collections.primitives.ArrayIntList;
import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Une data source sur une base access
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractAccessDataSource<T extends TopiaEntityEnum, M extends AbstractAccessEntityMeta<T>> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractAccessDataSource.class);
    private final Class<? extends AccessEntityMetaProvider<T, M>> providerClass;
    private final File dbFile;
    /** le cache des données (on lie une seule fois les tables) */
    protected Map<M, Map<String, Object>[]> cache;
    protected DataSourceState state;
    /** La liste des méta données reconnues pour la base ouverte. */
    private M[] metas;
    private Map<String, Set<String>> tableColumns;
    private Map<String, Set<String>> unusedTableColumns;

    @SuppressWarnings({"unchecked"})
    public AbstractAccessDataSource(Class<? extends AbstractAccessEntityMeta> metaClass,
                                    Class<? extends AccessEntityMetaProvider<T, M>> providerClass,
                                    File dbFile) {

        if (metaClass == null) {
            throw new NullPointerException("metaClass parameter can not be null");
        }
        if (providerClass == null) {
            throw new NullPointerException("providerClass parameter can not be null");
        }
        this.providerClass = providerClass;

        if (dbFile == null) {
            throw new NullPointerException("dbFile parameter can not be null");
        }

        if (!dbFile.exists()) {
            throw new IllegalArgumentException("dbFile " + dbFile + " does not exists");
        }

        this.dbFile = dbFile;
    }

    protected boolean isInit() {
        return state != null && state.ordinal() >= DataSourceState.INIT.ordinal();
    }

    protected boolean isLoad() {
        return state != null && state.ordinal() >= DataSourceState.LOAD.ordinal();
    }

    private void checkIfInit() {
        if (!isInit()) {
            throw new IllegalStateException(String.format("Data source %s was not init!", dbFile));
        }
    }

    private void checkIfLoad() {
        if (!isLoad()) {
            throw new IllegalStateException(String.format("Data source %s was not loaded!", dbFile));
        }
    }

    protected abstract M[] newMetaArray(Collection<M> iterable);

    protected abstract void onTableMissing(M meta);

    protected abstract void onPropertyMissing(M meta, String property, String column);

    protected abstract void onPKeyMissing(M meta, String pKey);

    public void init() throws Exception {
        if (isInit()) {
            if (log.isWarnEnabled()) {
                log.warn("Datasource " + dbFile + " was already init, will skip it.");
            }
            return;
        }

        // instanciate provider of metas
        AccessEntityMetaProvider<T, M> metaProvider = providerClass.newInstance();

        // get metas from the provider
        Set<M> metas = metaProvider.getMetas();

        // get a new connexion to access db
        Database connexion = getConnexion();

        try {

            // get the list of table names required (says all the one in meta)
            Set<String> requiredTables = new HashSet<>();
            for (M meta : metas) {
                requiredTables.add(meta.getTableName());
            }

            tableColumns = new TreeMap<>();
            unusedTableColumns = new TreeMap<>();

            // scan table
            for (Table table : connexion) {
                String tableName = table.getName();
                Map<String, Set<String>> result;
                if (requiredTables.contains(tableName)) {
                    result = tableColumns;
                } else {
                    result = unusedTableColumns;
                }
                Set<String> columnNames = new TreeSet<>();
                List<? extends Column> columns = table.getColumns();
                for (Column column : columns) {
                    columnNames.add(column.getName());
                }
                log.debug(String.format("detected ms-access table %s with columns %s", tableName, columnNames));
                Set<String> value = Collections.unmodifiableSet(columnNames);
                result.put(tableName, value);
            }

            // scan metas and match them to detected structure
            for (M meta : metas) {
                if (log.isInfoEnabled()) {
                    log.info("Will init meta : " + meta);
                }
                initMeta(meta);
            }

            this.metas = newMetaArray(metas);

            // coming here make the init was safely done
            state = DataSourceState.INIT;

        } finally {

            closeConnexion(connexion);
        }
    }

    public void load() throws IOException {
        if (isLoad()) {
            log.warn(String.format("Data source %s was already loaded, will skip it.", dbFile));
            return;
        }

        Database connexion = getConnexion();

        try {
            cache = new HashMap<>();

            for (M meta : metas) {
                Map<String, Object>[] data = loadTable(meta, connexion);
                cache.put(meta, data);
            }

            // coming here make the loading was safely done
            state = DataSourceState.LOAD;

        } finally {

            closeConnexion(connexion);
        }
    }

    private void initMeta(M meta) {
        log.debug(String.format("check entity :\n%s", meta));
        // check table name
        String tableName = meta.getTableName();
        if (!tableColumns.containsKey(tableName)) {
            onTableMissing(meta);
            return;
        }

        Set<String> columns = tableColumns.get(tableName);

        List<AbstractAccessEntityMeta.PropertyMapping> mapping = new ArrayList<>();

        // check property mapping
        for (AbstractAccessEntityMeta.PropertyMapping entry : meta.getPropertyMapping()) {
            String property = entry.getProperty();
            String column = entry.getColumn();
            if (columns.contains(column)) {
                mapping.add(entry);
            } else {
                onPropertyMissing(meta, property, column);
            }
        }

        // load mapping
        meta.setPropertyMapping(mapping.toArray(new AbstractAccessEntityMeta.PropertyMapping[mapping.size()]));

        // check pKeys
        for (String pKey : meta.getPKeys()) {
            if (!columns.contains(pKey)) {
                onPKeyMissing(meta, pKey);
            }
        }

    }

    /**
     * Obtains the number of rows of a table by iterating on each row of it.
     * <p>
     * This is due to a bug in the jackess library (should fill a bug
     * report for it) using the {@link Table#getRowCount()} method (sometines
     * it misses one row...)
     *
     * @param table the table to seek
     * @return the number of rows of the given table.
     */
    public int getRowCount(Table table) {
        int result = 0;
        for (Object aTable : table) {
            result++;
        }
        return result;
    }

    @SuppressWarnings({"unchecked"})
    private Map<String, Object>[] loadTable(M meta, Database connexion) throws IOException {

        String tableName = meta.getTableName();

        // load table from access
        Table table = connexion.getTable(tableName);
//        int rowCount = table.getRowCount();
        int rowCount = getRowCount(table);
        log.debug("Load table " + tableName + " with " + rowCount + " row(s).");
        Map<String, Object>[] result = new Map[rowCount];
        int i = 0;
        Iterator<Row> itr = table.iterator();
        IntList errors = new ArrayIntList();
        while (i < rowCount && itr.hasNext()) {
            Map<String, Object> map = null;
            try {
                map = itr.next();
            } catch (Exception e) {
                log.error("Could not read row " + i + " of table " + tableName, e);
                errors.add(i);
            }
            result[i++] = map;
        }
        if (!errors.isEmpty()) {
            log.warn("[" + meta.getType() + "] Could not load " + errors.size() + " row(s) : " + errors);
        }
        meta.setErrorRows(errors.toArray(new int[errors.size()]));

        return result;
    }

    protected Database getConnexion() throws IOException {
        return new DatabaseBuilder(dbFile).open();
    }

    private void closeConnexion(Database connexion) {
        if (connexion != null /*&& connexion.getPageChannel() != null && connexion.getPageChannel().isOpen()*/) {
            try {
                connexion.close();
            } catch (Exception e) {
                log.error("Could not close access connexion", e);
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        destroy();
        super.finalize();
    }

    public final void destroy() {

        if (isInit()) {
            tableColumns.clear();
            unusedTableColumns.clear();
            for (M meta : metas) {
                meta.clear();
            }
        }

        if (isLoad()) {
            for (M m : cache.keySet()) {
                Map<String, Object>[] maps = cache.get(m);
                for (Map<String, Object> map : maps) {
                    if (map != null) {
                        map.clear();
                    }
                }
            }
            cache.clear();
        }
        metas = null;
        tableColumns = null;
        unusedTableColumns = null;
        cache = null;
        state = null;
    }

    protected final Set<String> getTableNames() {
        checkIfInit();
        return tableColumns.keySet();
    }

    public final Set<String> getUnusedTableNames() {
        checkIfInit();
        return unusedTableColumns.keySet();
    }

//    public Set<String> getTableColumns(String tableName) {
//        checkIfInit();
//        return tableColumns.get(tableName);
//    }

    public Set<String> getUnusedTableColumns(String tableName) {
        checkIfInit();
        return unusedTableColumns.get(tableName);
    }

    public final boolean hasError() {
        checkIfInit();
        for (M meta : metas) {
            if (meta.hasError()) {
                return true;
            }
        }
        return false;
    }

    public final boolean hasWarning() {
        checkIfInit();
        for (M meta : metas) {
            if (meta.hasWarning()) {
                return true;
            }
        }
        return false;
    }

    protected final M[] getMetaWithError() {
        checkIfInit();
        List<M> result = new ArrayList<>();
        for (M meta : metas) {
            if (meta.hasError()) {
                result.add(meta);
            }
        }
        return newMetaArray(result);
    }

    protected final M[] getMetaWithWarning() {
        checkIfInit();
        List<M> result = new ArrayList<>();
        for (M meta : metas) {
            if (meta.hasWarning()) {
                result.add(meta);
            }
        }
        return newMetaArray(result);
    }

    protected M[] getMetas() {
        checkIfInit();
        return metas;
    }

    public M[] getMetaForType(Class<?> type) {
        checkIfInit();
        List<M> result = new ArrayList<>();
        for (M meta : metas) {
            if (type.isAssignableFrom(meta.getType().getContract())) {
                result.add(meta);
            }
        }
        return newMetaArray(result);
    }

    public M getMeta(T type) {
        checkIfInit();
        for (M meta : metas) {
            if (type.equals(meta.getType())) {
                return meta;
            }
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    public final Map<String, Object>[] getTableData(M meta) {
        checkIfLoad();
        return cache.get(meta);
    }

    public final Map<String, Object> getTableDataRow(M meta, int row) {
        checkIfLoad();
        Map<String, Object>[] result = getTableData(meta);
        return result[row];
    }

    @SuppressWarnings({"unchecked"})
    public final <E extends TopiaEntity> E[] loadAssociation(M meta,
                                                             M container,
                                                             Object[] pKeys) {
        checkIfLoad();
        List<E> result = new ArrayList<>();
        int row = 0;
        Map<String, Object>[] data = getTableData(meta);
        for (Map<String, Object> map : data) {
            if (map == null) {
                row++;
                continue;
            }
            Object[] pKey = getPKey(container.getPKeys(), map);
            if (Arrays.equals(pKeys, pKey)) {
                E e = (E) meta.newEntity(row, getPKey(meta, map));
                result.add(e);
            }
            row++;
        }
        E[] r = (E[]) Array.newInstance(meta.getType().getContract(), result.size());
        int i = 0;
        for (E e : result) {
            r[i++] = e;
        }
        return r;
    }

    @SuppressWarnings({"unchecked"})
    public final <E extends TopiaEntity> List<E> loadEntities(M meta) {
        checkIfLoad();
        List<E> result = new ArrayList<>();

        int row = 0;
        for (Map<String, Object> map : getTableData(meta)) {
            Object[] pKey = getPKey(meta, map);
            E e = (E) meta.newEntity(row++, pKey);
            result.add(e);
        }
        return result;
    }

    private Object[] getPKey(M meta, Map<String, Object> map) {
        return getPKey(meta.getPKeys(), map);
    }

    public Object[] getPKey(List<String> keys, Map<String, Object> map) {
        Object[] result = new Object[keys.size()];
        int i = 0;
        for (String key : keys) {
            Object o = map.get(key);
            result[i++] = o;
        }
        return result;
    }

    public enum DataSourceState {
        INIT,
        LOAD
    }

}
