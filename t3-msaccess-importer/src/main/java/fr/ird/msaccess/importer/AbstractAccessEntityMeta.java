/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Les méta données d'une entité récupéré depuis une base access.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractAccessEntityMeta<T extends TopiaEntityEnum> {

    protected static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];
    private static final Log log = LogFactory.getLog(AbstractAccessEntityMeta.class);
    /** le type de l'entité */
    protected final T type;
    /** le liste des colonnes de la table access associée utilisée comme clef d'unicité */
    protected final List<String> pKeys;
    /** le type du proxy utilisé */
    private final Class<? extends AccessEntity<?>> proxyType;
    /** la liste des warnings enregistrés pendant l'analyse de la base */
    private final List<String> warnings;
    /** la liste des erreurs enregistrés pendant l'analyse de la base */
    private final List<String> errors;
    /** le nom de la table access associée */
    protected String tableName;
    /** les propriétés simples + composition (mais pas les reverse) */
    private PropertyMapping[] propertyMapping;
    /** les association définies sur l'entité */
    private AssociationMapping[] reverseAssociationMapping;
    /** les composition (reverse d'une association) de l'entité */
    private AssociationMapping[] associationMapping;
    /** le numéro de la ligne dans la table access associée */
    private Long rowCount;
    /** les lignes qui n'ont pas pu etre lues. */
    private int[] errorRows;

    protected AbstractAccessEntityMeta(AbstractAccessEntityMeta<T> source) {
        type = source.type;
        proxyType = source.proxyType;
        pKeys = source.pKeys;
        tableName = source.tableName;
        propertyMapping = source.propertyMapping;
        associationMapping = source.associationMapping;
        reverseAssociationMapping = source.reverseAssociationMapping;
        warnings = new ArrayList<>(source.warnings);
        errors = new ArrayList<>(source.errors);
    }

    protected <P extends AccessEntity<T>> AbstractAccessEntityMeta(Class<P> proxyType,
                                                                   T type,
                                                                   String tableName,
                                                                   String[] pKeys,
                                                                   Object[] association,
                                                                   Object[] reverseProperties,
                                                                   Object... properties) {
        if (properties.length % 3 != 0) {
            throw new IllegalArgumentException("'properties' parameter length must be modulo 3 but was " + properties.length);
        }
        if (reverseProperties.length % 2 != 0) {
            throw new IllegalArgumentException("'reverseProperties' parameter length must be modulo 2 but was " + association.length);
        }
        if (association.length % 2 != 0) {
            throw new IllegalArgumentException("'association' parameter length must be modulo 2 but was " + association.length);
        }
        this.proxyType = proxyType;
        this.type = type;
        this.tableName = tableName;
        this.pKeys = Arrays.asList(pKeys);
        int nbProperties = properties.length / 3;
        int nbAssociations = association.length / 2;
        int nbReverseProperties = reverseProperties.length / 2;
        propertyMapping = new PropertyMapping[nbProperties];
        associationMapping = new AssociationMapping[nbAssociations];
        reverseAssociationMapping = new AssociationMapping[nbReverseProperties];
        warnings = new ArrayList<>();
        errors = new ArrayList<>();
        if (nbProperties == 0) {
            throw new IllegalStateException(String.format("Can not have no properties for %s", type));
        }
        log.debug(String.format("[%s] will add %d properties.", type, nbProperties));
        for (int i = 0, max = properties.length; i < max; i += 3) {
            String property = (String) properties[i];
            Class<?> typeP = (Class<?>) properties[i + 1];
            String column = (String) properties[i + 2];
            log.debug(String.format("[%s] property %s for column %s", type, property, column));
            propertyMapping[i / 3] = new PropertyMapping(typeP, property, column);
        }
        if (nbAssociations > 0) {
            log.debug(String.format("[%s] will add %d associations.", type, nbAssociations));
            for (int i = 0, max = association.length; i < max; i += 2) {
                String property = (String) association[i];
                Class<?> typeP = (Class<?>) association[i + 1];
                log.debug(String.format("[%s] association %s type : %s", type, property, typeP));
                associationMapping[i / 2] = new AssociationMapping(typeP, property);
            }
        }
        if (nbReverseProperties > 0) {
            if (log.isDebugEnabled()) {
                log.debug("[" + type + "] will add " + nbReverseProperties + " reverse associations.");
            }
            for (int i = 0, max = reverseProperties.length; i < max; i += 2) {
                String property = (String) reverseProperties[i];
                Class<?> typeP = (Class<?>) reverseProperties[i + 1];
                log.debug(String.format("[%s] reverse association %s type : %s", type, property, typeP));
                reverseAssociationMapping[i / 2] = new AssociationMapping(typeP, property);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbstractAccessEntityMeta<?> that = (AbstractAccessEntityMeta<?>) o;
        return type.equals(that.type) && tableName.equals(that.tableName);
    }

    @Override
    public int hashCode() {
        int result = tableName.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    public void addError(String message) {
        errors.add(message);
    }

    public void addWarning(String message) {
        warnings.add(message);
    }

    public int[] getErrorRows() {
        return errorRows;
    }

    public void setErrorRows(int[] errorRows) {
        this.errorRows = errorRows;
    }

    public abstract String getTopiaNaturalId();

    protected abstract InvocationHandler newHandler(int rowId, Object[] pKey) throws Exception;

    public TopiaEntity newEntity(int rowId, Object[] pKey) {
        try {
            Object e = Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{proxyType, getType().getContract()}, newHandler(rowId, pKey));
            log.debug(String.format("Create a proxy [%s:%s] %s", getType(), Arrays.toString(pKey), e));
            return (TopiaEntity) e;
        } catch (Exception e1) {
            throw new RuntimeException("Could not instantiate entity of type " + getType(), e1);
        }
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public PropertyMapping getPropertyMapping(String propertyName) {
        for (PropertyMapping propertyMapping : this.propertyMapping) {
            if (propertyMapping.getProperty().equals(propertyName)) {
                return propertyMapping;
            }
        }
        return null;
    }

    public AssociationMapping getReverseAssociationMapping(String propertyName) {
        for (AssociationMapping propertyMapping : reverseAssociationMapping) {
            if (propertyMapping.getProperty().equals(propertyName)) {
                return propertyMapping;
            }
        }
        return null;
    }

    public AssociationMapping getAssociationMapping(String propertyName) {
        for (AssociationMapping propertyMapping : associationMapping) {
            if (propertyMapping.getProperty().equals(propertyName)) {
                return propertyMapping;
            }
        }
        return null;
    }

    public String getPropertyColumnName(String propertyName) {
        PropertyMapping mapping = getPropertyMapping(propertyName);
        return mapping == null ? null : mapping.getColumn();
    }

    public boolean containsProperty(String propertyName) {
        PropertyMapping mapping = getPropertyMapping(propertyName);
        return mapping != null;
    }

    public boolean containsReverseAssociation(String propertyName) {
        AssociationMapping mapping = getReverseAssociationMapping(propertyName);
        return mapping != null;
    }

    public boolean containsAssociation(String propertyName) {
        AssociationMapping mapping = getAssociationMapping(propertyName);
        return mapping != null;
    }

    public List<String> getPropertyNames() {
        List<String> result = new ArrayList<>();
        for (PropertyMapping m : propertyMapping) {
            result.add(m.getProperty());
        }
        return result;
    }

    public PropertyMapping[] getPropertyMapping() {
        return propertyMapping;
    }

    public void setPropertyMapping(PropertyMapping[] propertyMapping) {
        this.propertyMapping = propertyMapping;
    }

    public AssociationMapping[] getAssociationMapping() {
        return associationMapping;
    }

    public AssociationMapping[] getReverseAssociationMapping() {
        return reverseAssociationMapping;
    }

    public T getType() {
        return type;
    }

    public List<String> getPKeys() {
        return pKeys;
    }

    public Long getRowCount() {
        return rowCount;
    }

    public void setRowCount(Long rowCount) {
        this.rowCount = rowCount;
    }

    public boolean hasError() {
        return !errors.isEmpty();
    }

    public boolean hasWarning() {
        return !warnings.isEmpty();
    }

    public String[] getWarnings() {
        return warnings.toArray(new String[warnings.size()]);
    }

    public String[] getErrors() {
        return errors.toArray(new String[errors.size()]);
    }

    public void checkProperty(String name) throws IllegalArgumentException {
        if (!containsProperty(name)) {
            throw new IllegalArgumentException(
                    String.format("%s is not a property of %s, available: %s", name, this, getPropertyNames()));
        }
    }

    @Override
    public String toString() {
        ToStringBuilder sb = new ToStringBuilder(this);
        sb.append("type", getType());
        sb.append("tableName", getTableName());
        sb.append("pKeys", getPKeys());
        sb.append("propertyNames", getPropertyNames());
        return sb.toString();
    }

    public void clear() {
        errors.clear();
        warnings.clear();
    }

    public static class PropertyMapping implements Serializable {

        private static final long serialVersionUID = 1L;

        protected final Class<?> type;
        protected final String property;
        protected final String column;

        PropertyMapping(Class<?> type, String property, String column) {
            this.type = type;
            this.property = property;
            this.column = column;
        }

        public Class<?> getType() {
            return type;
        }

        public String getProperty() {
            return property;
        }

        public String getColumn() {
            return column;
        }

    }

    public static class AssociationMapping implements Serializable {

        private static final long serialVersionUID = 1L;

        protected final Class<?> type;
        protected final String property;

        AssociationMapping(Class<?> type, String property) {
            this.type = type;
            this.property = property;
        }

        public Class<?> getType() {
            return type;
        }

        public String getProperty() {
            return property;
        }

    }

}
