/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.topia.persistence.util.EntityOperator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Collection;

/**
 * Le handler de proxy utilisé sur chaque entité à importer depuis access.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractAccessEntityProxyHandler<T extends TopiaEntityEnum> implements InvocationHandler {

    private static final Log log = LogFactory.getLog(AbstractAccessEntityProxyHandler.class);

    /** la méta-donnée de l'entité. */
    protected final AbstractAccessEntityMeta<T> meta;
    /** la clef metier recupere de la base access. */
    protected final Object[] pKey;
    /** l'entité à charger depuis les données access (première passe). */
    protected final TopiaEntity entity;
    /** l'opérateur de l'entité. */
    protected final EntityOperator<TopiaEntity> operator;
    /**
     * la ligne ou a ete recupere l'entite dans la table de la base access (cette
     * données est optionelle et peut valoir -1 si la valeur n'est pas
     * renseignée).
     */
    private final int rowId;

    public AbstractAccessEntityProxyHandler(AbstractAccessEntityMeta<T> meta, int rowId, Object[] pKey) throws Exception {
        this.meta = meta;
        this.rowId = rowId;
        this.pKey = pKey;
        TopiaEntityEnum type = meta.getType();
        operator = getOperator(type);
        entity = type.getImplementation().newInstance();
    }

    protected abstract EntityOperator<TopiaEntity> getOperator(TopiaEntityEnum type);

    protected abstract Object getPropertyValue(Class<?> type, String propertyName, Object value);

    protected abstract Object getPropertyValueFromMetaType(TopiaEntityEnum metaType, String propertyName, Object value);

    public TopiaEntity getEntity() {
        return entity;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        if ("getPKey".equals(methodName) && args == null) {
            return pKey;
        }
        if ("getRowId".equals(methodName) && args == null) {
            return rowId;
        }
        if ("getMeta".equals(methodName) && args == null) {
            return meta;
        }

        if ("getTopiaEntity".equals(methodName) && args == null) {
            return entity;
        }
        if ("setProperty".equals(methodName) && args != null && args.length == 2) {
            String propertyName = (String) args[0];
            Object value = args[1];
            setProperty(propertyName, value);
            return null;
        }
        if ("getProperty".equals(methodName) && args != null && args.length == 1) {
            String propertyName = (String) args[0];
            return getProperty(propertyName);
        }

        if ("addAssociationProperty".equals(methodName) && args != null && args.length == 2) {
            String propertyName = (String) args[0];
            Object value = args[1];
            addListProperty(propertyName, value);
            return null;
        }
        if ("setAssociationProperty".equals(methodName) && args != null && args.length == 2) {
            String propertyName = (String) args[0];
            Collection<?> value = (Collection<?>) args[1];
            setListProperty(propertyName, value);
            return null;
        }
        if ("accept".equals(methodName) && args != null && args.length == 1 && args[0] instanceof TopiaEntityVisitor) {
            TopiaEntityVisitor visitor = (TopiaEntityVisitor) args[0];
            accept((TopiaEntity) proxy, visitor);
            return null;
        }
        // on delegue a l'entite
        log.debug(String.format("Will invoke on entity %s", method));
        return method.invoke(entity, args);
    }

    public void setProperty(String propertyName, Object value) {
        if (value == null) {
            // rien a faire si on veut setter une valeur nulle
            return;
        }
        try {
            if (!(value instanceof TopiaEntity)) {
                AbstractAccessEntityMeta.PropertyMapping mapping = meta.getPropertyMapping(propertyName);
                Class<?> type = mapping.getType();
                value = getPropertyValue(type, propertyName, value);
                TopiaEntityEnum entityType = meta.getType();
                value = getPropertyValueFromMetaType(entityType, propertyName, value);
            }
            log.debug(String.format("Will set [%s:%s] to %s", propertyName, value, entity));
            // sauvegarde de la propriété dans l'entité de chargement
            operator.set(propertyName, entity, value);
        } catch (RuntimeException e) {
            log.error(String.format("Could not set [%s:%s] on %s", propertyName, value, entity), e);
            throw e;
        }
    }

    public Object getProperty(String propertyName) {
        return operator.get(propertyName, entity);
    }

    public void addListProperty(String name, Object value) {
        if (entity == null) {
            throw new IllegalStateException(
                    String.format("Can not add any association while entity [%s] is not created...", this));
        }
        operator.addChild(name, entity, value);
    }

    public void setListProperty(String name, Collection<?> value) {
        if (entity == null) {
            throw new IllegalStateException(
                    String.format("Can not add any association while entity [%s] is not created...", this));
        }
        operator.set(name, entity, value);
    }

    public void accept(TopiaEntity entity, TopiaEntityVisitor visitor) {
        visitor.start(entity);
        try {
            // parcours des propriétés simples + compositions
            for (AbstractAccessEntityMeta.PropertyMapping map : meta.getPropertyMapping()) {
                visitor.visit(entity, map.getProperty(), map.getType(), null);
            }
            // parcours des reverse associations
            for (AbstractAccessEntityMeta.AssociationMapping map : meta.getReverseAssociationMapping()) {
                visitor.visit(entity, map.getProperty(), map.getType(), null);
            }
            // parcours des associations
            for (AbstractAccessEntityMeta.AssociationMapping map : meta.getAssociationMapping()) {
                visitor.visit(entity, map.getProperty(), Collection.class, map.getType(), null);
            }
        } finally {
            visitor.end(entity);
        }
    }

}
