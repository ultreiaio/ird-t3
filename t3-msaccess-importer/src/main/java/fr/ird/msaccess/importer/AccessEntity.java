/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;

import java.util.Collection;

/**
 * Le contrat d'une entité mappé sur base access.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface AccessEntity<T extends TopiaEntityEnum> extends TopiaEntity {

    AbstractAccessEntityMeta<T> getMeta();

    Object[] getPKey();

    int getRowId();

    void setProperty(String name, Object value);

    Object getProperty(String name);

    void addAssociationProperty(String name, Object value);

    void setAssociationProperty(String name, Collection<?> value);

    TopiaEntity getTopiaEntity();
}
