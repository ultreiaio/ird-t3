/*
 * #%L
 * T3 :: MS Access Importer
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.msaccess.importer;

import org.nuiton.topia.persistence.TopiaEntityEnum;

/**
 * Le modèle pour écouter le nombre d'entité créées.
 *
 * @since 1.0
 */
public abstract class AbstractAccessHitModel<T extends TopiaEntityEnum> extends HitModel<T, AbstractAccessHitModel<T>> {

    private static final long serialVersionUID = 1L;

    protected AbstractAccessHitModel(Class<T> type) {
        super(type);
    }

    @Override
    protected abstract AbstractAccessHitModel<T> newModel();

    @Override
    public void addHit0(T type, boolean fire, Long old, Long result) {
        super.addHit0(type, fire, old, result);
    }
}
