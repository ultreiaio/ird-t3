# T3

[![Maven Central status](https://img.shields.io/maven-central/v/fr.ird.t3/t3.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22fr.ird.t3%22%20AND%20a%3A%22t3%22)
![Build Status](https://gitlab.com/ultreiaio/ird-t3/badges/develop/build.svg)
[![GNU AFFERO GENERAL PUBLIC LICENSE, Version 3.0](https://img.shields.io/badge/license-AGPL3-green.svg)](http://www.gnu.org/licenses/agpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/ird-t3/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/ird-t3)
* [Mailing-list (Devel)](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/t3-devel)
* [Mailing-list (Commits)](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/t3-commits)
* [Contact](mailto:dev@tchemit.fr)

# Demo

* [Demo (latest)](https://demo.ultreia.io/t3-latest)
* [Demo (2.0.1)](https://demo.ultreia.io/t3-2.0.1)
