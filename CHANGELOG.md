# T3 changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2020-02-03 17:02.

## Version [2.9](https://gitlab.com/ultreiaio/ird-t3/milestones/39)

**Closed at 2018-05-18.**

### Download
* [Installer (t3-2.9.zip)](https://ultreia.io/release/t3-2.9.zip)
* [Application (t3-2.9.war)](https://ultreia.io/release/t3-2.9.war)

### Issues
  * [[Anomalie 328]](https://gitlab.com/ultreiaio/ird-t3/issues/328) **Problème avec l&#39;installeur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 329]](https://gitlab.com/ultreiaio/ird-t3/issues/329) **improve readme of installer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.8](https://gitlab.com/ultreiaio/ird-t3/milestones/38)

**Closed at 2018-05-04.**

### Download
* [Installer (t3-2.8.zip)](https://ultreia.io/release/t3-2.8.zip)
* [Application (t3-2.8.war)](https://ultreia.io/release/t3-2.8.war)

### Issues
  * [[Anomalie 278]](https://gitlab.com/ultreiaio/ird-t3/issues/278) **Prévenir une exception au N3** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Anomalie 318]](https://gitlab.com/ultreiaio/ird-t3/issues/318) **[N3] Exception multi-zone activity** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Anomalie 319]](https://gitlab.com/ultreiaio/ird-t3/issues/319) **[CONNEXION] Traiter l&#39;exception lorsque la base PG accédée n&#39;existe pas** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Anomalie 320]](https://gitlab.com/ultreiaio/ird-t3/issues/320) **[N3] Exception NullPointerException** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Anomalie 321]](https://gitlab.com/ultreiaio/ird-t3/issues/321) **[N2] Les données des non senneurs ne sont pas reportés dans correctedCatchWeight** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Anomalie 322]](https://gitlab.com/ultreiaio/ird-t3/issues/322) **[N2] Des espèces accessoires ne sont pas recopiées de catchWeight vers correctedCatchWeight lors d&#39;un N2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 315]](https://gitlab.com/ultreiaio/ird-t3/issues/315) **[INSTALLEUR] Améliorer le lancement de l&#39;installeur (ne plus éditer un fichier avant le démarrage)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 316]](https://gitlab.com/ultreiaio/ird-t3/issues/316) **[N2] Question sur le log N2** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 323]](https://gitlab.com/ultreiaio/ird-t3/issues/323) **Améliorer le log de répartition si une seule catégorie utilisée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 324]](https://gitlab.com/ultreiaio/ird-t3/issues/324) **Améliorer les temps de traitement du N2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 317]](https://gitlab.com/ultreiaio/ird-t3/issues/317) **[N2] A propos des résultats d&#39;un traitement avec catégories de poids dans la stratification** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [2.7](https://gitlab.com/ultreiaio/ird-t3/milestones/37)

**Closed at 2018-04-13.**

### Download
* [Installer (t3-2.7.zip)](https://ultreia.io/release/t3-2.7.zip)
* [Application (t3-2.7.war)](https://ultreia.io/release/t3-2.7.war)

### Issues
  * [[Anomalie 310]](https://gitlab.com/ultreiaio/ird-t3/issues/310) **Anomalie suite au chargement d&#39;une base dont le nom contient un lettre accentuée** (Thanks to Tony CHEMIT) (Reported by Julien Lebranchu)
  * [[Anomalie 311]](https://gitlab.com/ultreiaio/ird-t3/issues/311) **[N0.2] Questions sur le log** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 312]](https://gitlab.com/ultreiaio/ird-t3/issues/312) **[N0.2] Ne calculer le RF2 que si l&#39;une des marées de la strate a un défaut de logbook** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 313]](https://gitlab.com/ultreiaio/ird-t3/issues/313) **[N0.2] Lors du calcul du RF2, utiliser la liste rf1speciesforfleet** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [2.6](https://gitlab.com/ultreiaio/ird-t3/milestones/36)

**Closed at 2018-04-05.**

### Download
* [Installer (t3-2.6.zip)](https://ultreia.io/release/t3-2.6.zip)
* [Application (t3-2.6.war)](https://ultreia.io/release/t3-2.6.war)

### Issues
  * [[Evolution 306]](https://gitlab.com/ultreiaio/ird-t3/issues/306) **Mise en page de certains écrans de configuration** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 307]](https://gitlab.com/ultreiaio/ird-t3/issues/307) **[N3] Option à supprimer sur le N3** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Tâche 303]](https://gitlab.com/ultreiaio/ird-t3/issues/303) **Problème avec l&#39;installeur 2.5** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Tâche 304]](https://gitlab.com/ultreiaio/ird-t3/issues/304) **[IMPORT] Il manque un lien &quot;Importer une autre marée&quot;** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Tâche 305]](https://gitlab.com/ultreiaio/ird-t3/issues/305) **Pour une base OI 2018, l&#39;assistant import exige un code port a priori non pertinent** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)

## Version [2.5](https://gitlab.com/ultreiaio/ird-t3/milestones/35)

**Closed at 2018-03-23.**

### Download
* [Installer (t3-2.5.zip)](https://ultreia.io/release/t3-2.5.zip)
* [Application (t3-2.5.war)](https://ultreia.io/release/t3-2.5.war)

### Issues
  * [[Evolution 263]](https://gitlab.com/ultreiaio/ird-t3/issues/263) **[N2][N3] Gestion des catégories de poids -10/+10kg dans la stratification N2 et N3** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 301]](https://gitlab.com/ultreiaio/ird-t3/issues/301) **[N1.5] Remplacer SetSpeciesCatWeight.WeightCategorySample par WeightCategoryTreatment** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 302]](https://gitlab.com/ultreiaio/ird-t3/issues/302) **Simplification des bases en entrée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.4](https://gitlab.com/ultreiaio/ird-t3/milestones/34)

**Closed at 2018-03-19.**

### Download
* [Installer (t3-2.4.zip)](https://ultreia.io/release/t3-2.4.zip)
* [Application (t3-2.4.war)](https://ultreia.io/release/t3-2.4.war)

### Issues
  * [[Anomalie 239]](https://gitlab.com/ultreiaio/ird-t3/issues/239) **[N0.2][LOG] Petite bizarerie dans les logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 280]](https://gitlab.com/ultreiaio/ird-t3/issues/280) **Problème d&#39;UI en cas de traitement N2 (et N3) avec période de 1 mois** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Anomalie 283]](https://gitlab.com/ultreiaio/ird-t3/issues/283) **[N0] Amélioration du calcul de RF1 pour les marées n&#39;ayant pas de bons de débarquement** (Thanks to Tony CHEMIT) (Reported by Norbert Billet)
  * [[Evolution 245]](https://gitlab.com/ultreiaio/ird-t3/issues/245) **[REFERENTIEL] Rajouter des champs de traduction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 288]](https://gitlab.com/ultreiaio/ird-t3/issues/288) **Spatialiser la table Ocean** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 291]](https://gitlab.com/ultreiaio/ird-t3/issues/291) **[IMPORT] Sélectionner le driver AVDTH 3.5 par défaut** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 293]](https://gitlab.com/ultreiaio/ird-t3/issues/293) **Création de base en ligne de commande** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 294]](https://gitlab.com/ultreiaio/ird-t3/issues/294) **Internationalisation des tables** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 295]](https://gitlab.com/ultreiaio/ird-t3/issues/295) **Copyright** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Tâche 172]](https://gitlab.com/ultreiaio/ird-t3/issues/172) **Revoir les traductions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.3](https://gitlab.com/ultreiaio/ird-t3/milestones/33)

**Closed at 2018-03-12.**

### Download
* [Installer (t3-2.3.zip)](https://ultreia.io/release/t3-2.3.zip)
* [Application (t3-2.3.war)](https://ultreia.io/release/t3-2.3.war)

### Issues
  * [[Evolution 116]](https://gitlab.com/ultreiaio/ird-t3/issues/116) **Marées sans logbooks : gestion de leurs échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 261]](https://gitlab.com/ultreiaio/ird-t3/issues/261) **[N2][N3] Les calées de type banc indéterminé ne sont pas traitées au N2 et N3** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 269]](https://gitlab.com/ultreiaio/ird-t3/issues/269) **[N2][N3] Stockage des niveaux de substitution atteints** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 276]](https://gitlab.com/ultreiaio/ird-t3/issues/276) **Règles d&#39;acceptation des marées dans les traitements** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 287]](https://gitlab.com/ultreiaio/ird-t3/issues/287) **[N0.3] Conversion des catégories logbook - rendre générique la codification des catégories inconnues** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 290]](https://gitlab.com/ultreiaio/ird-t3/issues/290) **Spatialisation de la table Harbour** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.2](https://gitlab.com/ultreiaio/ird-t3/milestones/32)
Replace user database by a simple **yaml** file.

**Closed at 2018-03-02.**

### Download
* [Installer (t3-2.2.zip)](https://ultreia.io/release/t3-2.2.zip)
* [Application (t3-2.2.war)](https://ultreia.io/release/t3-2.2.war)

### Issues
  * [[Anomalie 201]](https://gitlab.com/ultreiaio/ird-t3/issues/201) **[H2] Base H2 parfois corrompue sur déconnexion/reconnexion à l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 286]](https://gitlab.com/ultreiaio/ird-t3/issues/286) **Remplacer la base interne h2 par un simple fichier au format **yaml**** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.1](https://gitlab.com/ultreiaio/ird-t3/milestones/31)
First part of contract 3.0

**Closed at 2018-02-25.**

### Download
* [Installer (t3-2.1.zip)](https://ultreia.io/release/t3-2.1.zip)
* [Application (t3-2.1.war)](https://ultreia.io/release/t3-2.1.war)

### Issues
  * [[Anomalie 154]](https://gitlab.com/ultreiaio/ird-t3/issues/154) **[WIN] Windows/Mauvais affichage des caractères accentués dans le navigateur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 160]](https://gitlab.com/ultreiaio/ird-t3/issues/160) **[N0.4][LOG] Calcul du nombre de calées et durée des calées - Petite erreur dans le log** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 232]](https://gitlab.com/ultreiaio/ird-t3/issues/232) **[UI] Souci de mise en page web** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 236]](https://gitlab.com/ultreiaio/ird-t3/issues/236) **[PG] Lorsqu&#39;on arrête l&#39;application, les connexions à la base PostgreSQL ne sont pas libérées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 246]](https://gitlab.com/ultreiaio/ird-t3/issues/246) **[LOGS] Erreur d&#39;affichage sur le log du RF1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 247]](https://gitlab.com/ultreiaio/ird-t3/issues/247) **[LOGS] Inversion de libellé dans le log d&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 250]](https://gitlab.com/ultreiaio/ird-t3/issues/250) **Les topiaid de LocalMarketPackagingType sont mal préfixés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 270]](https://gitlab.com/ultreiaio/ird-t3/issues/270) **Erreur de libellé dans le log N0.1 (calcul des RF1)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 85]](https://gitlab.com/ultreiaio/ird-t3/issues/85) **[N0] Marée Echantillons seulement et gestion de l&#39;UI du N0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 196]](https://gitlab.com/ultreiaio/ird-t3/issues/196) **[PERSISTENCE] Renommage de tables et champs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 240]](https://gitlab.com/ultreiaio/ird-t3/issues/240) **[N0] Calcul des temps de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 241]](https://gitlab.com/ultreiaio/ird-t3/issues/241) **[N0.4] Calcul des durées de calées pour les activités avec setcount &gt; 1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 243]](https://gitlab.com/ultreiaio/ird-t3/issues/243) **[UI][N2][N3] Changer la sélection de la flotte d&#39;une sélection unique vers une sélection multiple** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 244]](https://gitlab.com/ultreiaio/ird-t3/issues/244) **[N0.3] Rendre la conversion des catégories de poids AVDTH-&gt;T3 générique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 249]](https://gitlab.com/ultreiaio/ird-t3/issues/249) **Supprimer localmarketpackaging.status** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Evolution 251]](https://gitlab.com/ultreiaio/ird-t3/issues/251) **Améliorations dans la gestion des profils utilisateurs** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Evolution 253]](https://gitlab.com/ultreiaio/ird-t3/issues/253) **Ajouter un lien entre ports et pays** (Thanks to ) (Reported by Tony CHEMIT)
  * [[Evolution 258]](https://gitlab.com/ultreiaio/ird-t3/issues/258) **L&#39;importeur plante si les coordonnées d&#39;un port sont à NULL** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 259]](https://gitlab.com/ultreiaio/ird-t3/issues/259) **Ordonnacement du traitement de marées au 1.4 (distribution des échantillons sur les calées de l&#39;échantillon)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 260]](https://gitlab.com/ultreiaio/ird-t3/issues/260) **[N1][N2][N3] Permettre la sélection mutliple d&#39;océans** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 271]](https://gitlab.com/ultreiaio/ird-t3/issues/271) **Amélioration du log du RF1 (N0.1)** (Thanks to Tony CHEMIT) (Reported by Pascal Cauquil)
  * [[Evolution 285]](https://gitlab.com/ultreiaio/ird-t3/issues/285) **Ajouter un nouveau package avec des bases h2 (vide et avec le référentiel)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.1](https://gitlab.com/ultreiaio/ird-t3/milestones/28)

**Closed at 2017-05-12.**

### Download
* [Installer (t3-2.0.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/2.0.1/t3-2.0.1.zip)
* [Application (t3-2.0.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/2.0.1/t3-2.0.1.war)

### Issues
  * [[Anomalie 273]](https://gitlab.com/ultreiaio/ird-t3/issues/273) **Problème d&#39;ouverture de base en version 2.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 268]](https://gitlab.com/ultreiaio/ird-t3/issues/268) **Changer le mot de passe sur les installeurs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0](https://gitlab.com/ultreiaio/ird-t3/milestones/25)

**Closed at 2017-04-10.**

### Download
* [Installer (t3-2.0.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/2.0/t3-2.0.zip)
* [Application (t3-2.0.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/2.0/t3-2.0.war)

### Issues
  * [[Anomalie 199]](https://gitlab.com/ultreiaio/ird-t3/issues/199) **[IMPORT] Imperfection dans l&#39;import AVDTH** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 204]](https://gitlab.com/ultreiaio/ird-t3/issues/204) **[N1.5] Réorganisation du calcul des poids des échantillons et du RFContext** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 218]](https://gitlab.com/ultreiaio/ird-t3/issues/218) **[N0.5] Améliorations sur le calcul des efforts de pêche (surtout le temps de pêche)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 219]](https://gitlab.com/ultreiaio/ird-t3/issues/219) **Le bouton &quot;importer une nouvelle base&quot; ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 220]](https://gitlab.com/ultreiaio/ird-t3/issues/220) **[N2] Comportement de l&#39;algorithme lorsque la dernière strate échantillon est atteinte** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 224]](https://gitlab.com/ultreiaio/ird-t3/issues/224) **Erreur lors de l&#39;accès à la page Statistiques sur la base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 225]](https://gitlab.com/ultreiaio/ird-t3/issues/225) **[N2] Lors du traitement, exception en lien avec le champ zone FPA** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 228]](https://gitlab.com/ultreiaio/ird-t3/issues/228) **[N0] Sur 2 calées (sur 1642) les poids avant/après conversion des catégories de poids ne sont pas conservés** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 230]](https://gitlab.com/ultreiaio/ird-t3/issues/230) **[N2] Certaines calées ne sont pas traitées au N2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 235]](https://gitlab.com/ultreiaio/ird-t3/issues/235) **[INSTALLEUR] L&#39;installeur génère des erreurs (sans conséquences)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 252]](https://gitlab.com/ultreiaio/ird-t3/issues/252) **La suppression d&#39;un profil de base de sortie ne marche pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 16]](https://gitlab.com/ultreiaio/ird-t3/issues/16) **Mise à niveau des calculs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 20]](https://gitlab.com/ultreiaio/ird-t3/issues/20) **Retour sur les calculs du niveau 2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 58]](https://gitlab.com/ultreiaio/ird-t3/issues/58) **labellisation d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 169]](https://gitlab.com/ultreiaio/ird-t3/issues/169) **Adapter driver, import et modèle de données  à AVDTH v3.5 ou v3.6** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 186]](https://gitlab.com/ultreiaio/ird-t3/issues/186) **Gestion de valeurs nulles dans le référentiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 197]](https://gitlab.com/ultreiaio/ird-t3/issues/197) **[N2][N3] Permettre de sélectionner un sous-ensemble d&#39;échantillons (sur les critères type et qualité) à la configuration du N2 et du N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 198]](https://gitlab.com/ultreiaio/ird-t3/issues/198) **[UI] Sur chaque page de traitement, mettre un bouton qui permettrait de rejouer le traitement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 202]](https://gitlab.com/ultreiaio/ird-t3/issues/202) **[IMPORT] Améliorer la lisibilité des logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 214]](https://gitlab.com/ultreiaio/ird-t3/issues/214) **[N1] Changements sur le calcul et l&#39;utilisation des rfX** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 221]](https://gitlab.com/ultreiaio/ird-t3/issues/221) **[N1] Inhibition des rfminus10 et rfplus10 lorsqu&#39;un rfminus10 se retrouve calculé suite au glissement d&#39;individus dans cette catégorie par la clé de conversion LD1/LF** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 222]](https://gitlab.com/ultreiaio/ird-t3/issues/222) **[N3] Nouvelle option de configuration du traitement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 223]](https://gitlab.com/ultreiaio/ird-t3/issues/223) **[N3] Changer l&#39;ordre d&#39;affichage et la valeur par défaut d&#39;un paramètre de configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 231]](https://gitlab.com/ultreiaio/ird-t3/issues/231) **[N0] Ajouter 3 clauses d&#39;exclusion sur le calcul du temps de pêche** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 234]](https://gitlab.com/ultreiaio/ird-t3/issues/234) **[REFERENTIEL] Mettre à jour les scripts d&#39;initialisation de l&#39;installeur** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 242]](https://gitlab.com/ultreiaio/ird-t3/issues/242) **Rendre le calcul du RF1 plus générique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 256]](https://gitlab.com/ultreiaio/ird-t3/issues/256) **Amélioration de la configuration de l&#39;application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 255]](https://gitlab.com/ultreiaio/ird-t3/issues/255) **Migrate project to gitlab.com** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 257]](https://gitlab.com/ultreiaio/ird-t3/issues/257) **Mise à jour des dépendances** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.6.1](https://gitlab.com/ultreiaio/ird-t3/milestones/16)
&#10;&#10;*(from redmine: created on 2015-11-30)*

**Closed at 2015-12-07.**

### Download
* [Installer (t3-1.6.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.6.1/t3-1.6.1.zip)
* [Application (t3-1.6.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.6.1/t3-1.6.1.war)

### Issues
  * [[Anomalie 209]](https://gitlab.com/ultreiaio/ird-t3/issues/209) **[N0.2] Shunt du RF2 invalide** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 187]](https://gitlab.com/ultreiaio/ird-t3/issues/187) **Mise à jour des données des référentiels vessel et vesselactivity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 191]](https://gitlab.com/ultreiaio/ird-t3/issues/191) **Mettre à jour le fichier referentiel-data.sql avec celui-ci** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 205]](https://gitlab.com/ultreiaio/ird-t3/issues/205) **[N1.5] Nouvelles options de configuration N1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 206]](https://gitlab.com/ultreiaio/ird-t3/issues/206) **[N1.5] Corriger les modes d&#39;annulation de rfp10 et rfm10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 207]](https://gitlab.com/ultreiaio/ird-t3/issues/207) **[UI] A la fin d&#39;un import AVDTH, ajouter un bouton &quot;Importer une autre base&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 208]](https://gitlab.com/ultreiaio/ird-t3/issues/208) **[N1.1][RF1] Mise à jour d&#39;un paramètres de traitement par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 210]](https://gitlab.com/ultreiaio/ird-t3/issues/210) **[INSTALLEUR] Mettre à jour le fichier referentiel-data.sql** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 211]](https://gitlab.com/ultreiaio/ird-t3/issues/211) **[INSTALLEUR] Mettre à jour le fichier referentiel-data.sql, 2ème prise** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 212]](https://gitlab.com/ultreiaio/ird-t3/issues/212) **[N2] Comportement de l&#39;algorithme lorsque la dernière strate échantillon est atteinte** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 213]](https://gitlab.com/ultreiaio/ird-t3/issues/213) **[N1] Modifier l&#39;ordre d&#39;exécution de deux traitements** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Evolution 215]](https://gitlab.com/ultreiaio/ird-t3/issues/215) **[N1.5] Lorsque l&#39;un des deux rfMinus10 ou rfPlus10 est à 0 ou NULL, faire en sorte qu&#39;aucun des deux ne soit utilisé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 216]](https://gitlab.com/ultreiaio/ird-t3/issues/216) **[N2] Rajouter une option dans la configuration N2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 217]](https://gitlab.com/ultreiaio/ird-t3/issues/217) **[N3] Rajouter une option dans la configuration N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.6](https://gitlab.com/ultreiaio/ird-t3/milestones/14)
&#10;&#10;*(from redmine: created on 2013-04-12)*

**Closed at 2015-11-30.**

### Download
* [Installer (t3-1.6.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.6/t3-1.6.zip)
* [Application (t3-1.6.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.6/t3-1.6.war)

### Issues
  * [[Anomalie 170]](https://gitlab.com/ultreiaio/ird-t3/issues/170) **Page d&#39;infos &quot;Marée&quot; : &quot;Niveau 2 calculé&quot; et &quot;Niveau 3 calculé&quot; non gérés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 171]](https://gitlab.com/ultreiaio/ird-t3/issues/171) **Même quand n2 et n3 sont faits, la page de stats n&#39;affiche que des marées avec données &quot;partiellement calculés&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 173]](https://gitlab.com/ultreiaio/ird-t3/issues/173) **Ajouter des index sur les champs Postgis** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 183]](https://gitlab.com/ultreiaio/ird-t3/issues/183) **Can&#39;t redeploy t3 in a tomcat** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 174]](https://gitlab.com/ultreiaio/ird-t3/issues/174) **Mettre à jour les seuils et ratios de qualité  des N2 et N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 177]](https://gitlab.com/ultreiaio/ird-t3/issues/177) **Paramètres par défaut sur N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 178]](https://gitlab.com/ultreiaio/ird-t3/issues/178) **Modification de l&#39;algo de test de qualité N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 179]](https://gitlab.com/ultreiaio/ird-t3/issues/179) **Renommage de tables et champs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 185]](https://gitlab.com/ultreiaio/ird-t3/issues/185) **Initialisation correcte des index spatiaux** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 194]](https://gitlab.com/ultreiaio/ird-t3/issues/194) **[N1.4] Petite correction dans la conversion des individus mesurés en poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 195]](https://gitlab.com/ultreiaio/ird-t3/issues/195) **[PERSISTENCE] Renommer les champs libelle en label** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 200]](https://gitlab.com/ultreiaio/ird-t3/issues/200) **[N0.2] Possibilité de shunter le calcul RF2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 203]](https://gitlab.com/ultreiaio/ird-t3/issues/203) **Mise à jour des paramètres de traitement par défaut** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 181]](https://gitlab.com/ultreiaio/ird-t3/issues/181) **Updates mavenpom to 6.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 182]](https://gitlab.com/ultreiaio/ird-t3/issues/182) **Updates topia to 2.8.1.3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 184]](https://gitlab.com/ultreiaio/ird-t3/issues/184) **Avoid commons-loggin memory leak when hot-deploy in tomcat** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 193]](https://gitlab.com/ultreiaio/ird-t3/issues/193) **Assistance pour compiler le projet dans un IDE** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.5.1](https://gitlab.com/ultreiaio/ird-t3/milestones/15)
&#10;&#10;*(from redmine: created on 2013-04-13)*

**Closed at 2013-04-13.**

### Download
* [Installer (t3-1.5.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.5.1/t3-1.5.1.zip)
* [Application (t3-1.5.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.5.1/t3-1.5.1.war)

### Issues
  * [[Anomalie 168]](https://gitlab.com/ultreiaio/ird-t3/issues/168) **Arrrg ! bug jsp au lancement du n2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.5](https://gitlab.com/ultreiaio/ird-t3/milestones/13)
Validation du niveau 2, 3 et exports des données&#10;&#10;*(from redmine: created on 2013-02-24)*

**Closed at 2013-04-12.**

### Download
* [Installer (t3-1.5.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.5/t3-1.5.zip)
* [Application (t3-1.5.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.5/t3-1.5.war)

### Issues
  * [[Anomalie 149]](https://gitlab.com/ultreiaio/ird-t3/issues/149) **Exception sur N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 150]](https://gitlab.com/ultreiaio/ird-t3/issues/150) **Valeurs suspectes sur correctedcatchweight.catchweight (n0) alors qu&#39;elles étaient bonnes auparavant** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 151]](https://gitlab.com/ultreiaio/ird-t3/issues/151) **MAJ d&#39;un libellé** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 152]](https://gitlab.com/ultreiaio/ird-t3/issues/152) **Résultats suspects au N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 153]](https://gitlab.com/ultreiaio/ird-t3/issues/153) **Micro bug dans le script Windows installer/install.bat** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 156]](https://gitlab.com/ultreiaio/ird-t3/issues/156) **Libellés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 157]](https://gitlab.com/ultreiaio/ird-t3/issues/157) **Exception si tentative N1 sans N0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 158]](https://gitlab.com/ultreiaio/ird-t3/issues/158) **Vérification de l&#39;algo n0.4 (durée des calées)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 159]](https://gitlab.com/ultreiaio/ird-t3/issues/159) **Petite correction nécessaire sur le calcul du rf2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 162]](https://gitlab.com/ultreiaio/ird-t3/issues/162) **Mises au point du calcul des durées des calées (n0.3)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 164]](https://gitlab.com/ultreiaio/ird-t3/issues/164) **Sélection impossible des marées et échantillons lors de la config du N1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 165]](https://gitlab.com/ultreiaio/ird-t3/issues/165) **Non utilisation du banc de type dans la sélection du SetDuration à utiliser** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 155]](https://gitlab.com/ultreiaio/ird-t3/issues/155) **Packager les zip de zones géo avec l&#39;installeur** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 161]](https://gitlab.com/ultreiaio/ird-t3/issues/161) **Mettre à jour le rérférentiel setduration avec les INSERT fournis ici** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 167]](https://gitlab.com/ultreiaio/ird-t3/issues/167) **Mise à jour des librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.4](https://gitlab.com/ultreiaio/ird-t3/milestones/10)
Calcul du niveau 3 + export des données&#10;&#10;*(from redmine: created on 2012-04-26)*

**Closed at 2013-02-25.**

### Download
* [Installer (t3-1.4.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.4/t3-1.4.zip)
* [Application (t3-1.4.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.4/t3-1.4.war)

### Issues
  * [[Anomalie 73]](https://gitlab.com/ultreiaio/ird-t3/issues/73) **N3 n&#39;est pas implémenté** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 123]](https://gitlab.com/ultreiaio/ird-t3/issues/123) **Il manque des conversion taille-poids pour des espèces** (Thanks to Pascal Cauquil) (Reported by Tony CHEMIT)
  * [[Anomalie 139]](https://gitlab.com/ultreiaio/ird-t3/issues/139) **Sélection correcte des activités (&#61;calées) situées dans une zone (pour N2 et N3)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 127]](https://gitlab.com/ultreiaio/ird-t3/issues/127) **Indicateurs N3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 140]](https://gitlab.com/ultreiaio/ird-t3/issues/140) **Mettre à jour le fichier referentiel-data.sql de l&#39;installer.bat avec celui-ci** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 145]](https://gitlab.com/ultreiaio/ird-t3/issues/145) **Add natural id on extrapolatedallsetspeciesfrequency** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 135]](https://gitlab.com/ultreiaio/ird-t3/issues/135) **Export balbaya - Mapping catégories de poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 141]](https://gitlab.com/ultreiaio/ird-t3/issues/141) **Export balbaya - Operation Marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 142]](https://gitlab.com/ultreiaio/ird-t3/issues/142) **Export balbaya - Operation Activité** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 143]](https://gitlab.com/ultreiaio/ird-t3/issues/143) **Export balbaya - Operation Echantillon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 144]](https://gitlab.com/ultreiaio/ird-t3/issues/144) **Remove ExtrapolatedAllSetSpeciesCatWeight entity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3.2](https://gitlab.com/ultreiaio/ird-t3/milestones/12)
&#10;&#10;*(from redmine: created on 2013-01-16)*

**Closed at 2013-01-16.**

### Download
* [Installer (t3-1.3.2.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.3.2/t3-1.3.2.zip)
* [Application (t3-1.3.2.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.3.2/t3-1.3.2.war)

### Issues
  * [[Anomalie 137]](https://gitlab.com/ultreiaio/ird-t3/issues/137) **Plantage sur N2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 138]](https://gitlab.com/ultreiaio/ird-t3/issues/138) **Problème d&#39;encoding dans les fichiers javascripts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 136]](https://gitlab.com/ultreiaio/ird-t3/issues/136) **Ajout d&#39;une nouvelle étape au niveau 1 (conversion des échantillon calée en poids)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 134]](https://gitlab.com/ultreiaio/ird-t3/issues/134) **Mise à jour de librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3.1](https://gitlab.com/ultreiaio/ird-t3/milestones/11)
Mise au point avant le niveau 3&#10;&#10;*(from redmine: created on 2012-04-29)*

**Closed at 2012-06-15.**

### Download
* [Installer (t3-1.3.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.3.1/t3-1.3.1.zip)
* [Application (t3-1.3.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.3.1/t3-1.3.1.war)

### Issues
  * [[Anomalie 122]](https://gitlab.com/ultreiaio/ird-t3/issues/122) **La migration 1.3 ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 126]](https://gitlab.com/ultreiaio/ird-t3/issues/126) **Pb avec l&#39;import des marées où l&#39;on fait une réattribution du code bateau (surtout pour marées sampleOnly)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 129]](https://gitlab.com/ultreiaio/ird-t3/issues/129) **Exception sur N1.5 (extrapolation échantillons aux poids de leurs calées respectives)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 130]](https://gitlab.com/ultreiaio/ird-t3/issues/130) **Améliorer le temps de traitement du N1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 120]](https://gitlab.com/ultreiaio/ird-t3/issues/120) **Split ExtrapolateSampleWeightToSetAction in two actions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 121]](https://gitlab.com/ultreiaio/ird-t3/issues/121) **Ne pas charger tous les navires lors d&#39;un import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 125]](https://gitlab.com/ultreiaio/ird-t3/issues/125) **Permettre l&#39;import de bases AVDTH zippées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 128]](https://gitlab.com/ultreiaio/ird-t3/issues/128) **Pour le YFT, la relation taille-poids doit être différenciée en deça et au-delà d&#39;une taille seuil** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 131]](https://gitlab.com/ultreiaio/ird-t3/issues/131) **Renseignement de la colonne species.faoid** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 133]](https://gitlab.com/ultreiaio/ird-t3/issues/133) **Configure which species must be selected by default in level 2 and 3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 124]](https://gitlab.com/ultreiaio/ird-t3/issues/124) **Mise à jour de librairies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3](https://gitlab.com/ultreiaio/ird-t3/milestones/5)
Calcul du niveau 2&#10;&#10;*(from redmine: created on 2012-02-06)*

**Closed at 2012-04-28.**

### Download
* [Installer (t3-1.3.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.3/t3-1.3.zip)
* [Application (t3-1.3.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.3/t3-1.3.war)

### Issues
  * [[Anomalie 51]](https://gitlab.com/ultreiaio/ird-t3/issues/51) **Blocage au lancement des traitements N1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 71]](https://gitlab.com/ultreiaio/ird-t3/issues/71) **N2 : java.lang.IllegalStateException: Can not find activities for sample stratum at level 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 72]](https://gitlab.com/ultreiaio/ird-t3/issues/72) **Problèmes de mise en page sur la la page &quot;Traitement N2 en cours...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 97]](https://gitlab.com/ultreiaio/ird-t3/issues/97) **La page des statistiques des marées ne fonctionne plus** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 98]](https://gitlab.com/ultreiaio/ird-t3/issues/98) **LEs amrées sans plan de cuve ne sont pas indiquée comme traitée pour le traitement 0 - 6** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 100]](https://gitlab.com/ultreiaio/ird-t3/issues/100) **Pb sur runComputeWeightOfCategoriesForSet** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 101]](https://gitlab.com/ultreiaio/ird-t3/issues/101) **Problem with Level 1 -  5** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 102]](https://gitlab.com/ultreiaio/ird-t3/issues/102) **l&#39;algorithme de conversion de catégorie de poids n&#39;est pas bon** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 103]](https://gitlab.com/ultreiaio/ird-t3/issues/103) **Calcul des rf1 et rf2 lorque la marée n&#39;a pas de débaquements (elementaryLanding)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 104]](https://gitlab.com/ultreiaio/ird-t3/issues/104) **Revoir le N0.3 : conversion des catégories de poids** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 105]](https://gitlab.com/ultreiaio/ird-t3/issues/105) **N1.1 : Erreur dans le log** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 106]](https://gitlab.com/ultreiaio/ird-t3/issues/106) **L&#39;écran de config N1 ne permet pas de sélectionner la flotte Espagnole** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 107]](https://gitlab.com/ultreiaio/ird-t3/issues/107) **Marées traitées N0 mais qui ne sont pas prises en compte dans le N1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 108]](https://gitlab.com/ultreiaio/ird-t3/issues/108) **N1.1 (comptés/mesurés) : Problème de précision sur un integer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 109]](https://gitlab.com/ultreiaio/ird-t3/issues/109) **Le niveau 0.4 (ComputeSetDurationAndPositiveSetCount) est très lent** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 110]](https://gitlab.com/ultreiaio/ird-t3/issues/110) **CalculN1.2 (standardisation LD/LF) : +/- 15% d&#39;échantillons non standardisés** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 111]](https://gitlab.com/ultreiaio/ird-t3/issues/111) **Pb potentiel sur N1.4 : Redistribution des effectifs des fréquences de taille de l’échantillon-cuve entre les différentes calées qui constituent la cuve** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 114]](https://gitlab.com/ultreiaio/ird-t3/issues/114) **Certaines marées ne sont pas marquées comme traitées au niveau 0.4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 119]](https://gitlab.com/ultreiaio/ird-t3/issues/119) **Manque un champ ExtrapolatedAllSetSpeciesFrequency#lfLengthClass** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 19]](https://gitlab.com/ultreiaio/ird-t3/issues/19) **Résumé des niveaux 2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 95]](https://gitlab.com/ultreiaio/ird-t3/issues/95) **Remove level2Computed and compute value on trip board** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 99]](https://gitlab.com/ultreiaio/ird-t3/issues/99) **Ajout de 2 colonnes pour codes standard dans table espèces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 112]](https://gitlab.com/ultreiaio/ird-t3/issues/112) **N1.5 Action très longue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 113]](https://gitlab.com/ultreiaio/ird-t3/issues/113) **rendre configurable les seuils maximum rfTot, rfMinus10 et rfPlus10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 115]](https://gitlab.com/ultreiaio/ird-t3/issues/115) **Ne pas importer des marées en mode samplesOnly si elles n&#39;ont pas d&#39;échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.2.1](https://gitlab.com/ultreiaio/ird-t3/milestones/9)
&#10;&#10;*(from redmine: created on 2012-04-20)*

**Closed at 2012-04-20.**

### Download
* [Installer (t3-1.2.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.2.1/t3-1.2.1.zip)
* [Application (t3-1.2.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.2.1/t3-1.2.1.war)

### Issues
  * [[Anomalie 96]](https://gitlab.com/ultreiaio/ird-t3/issues/96) **Fix migration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.2](https://gitlab.com/ultreiaio/ird-t3/milestones/2)
Phase 2 - lot 2 (validation niveau 0 et 1)&#10;&#10;*(from redmine: created on 2011-09-07)*

**Closed at 2012-04-19.**

### Download
* [Installer (t3-1.2.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.2/t3-1.2.zip)
* [Application (t3-1.2.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.2/t3-1.2.war)

### Issues
  * [[Anomalie 48]](https://gitlab.com/ultreiaio/ird-t3/issues/48) **OutOfMemoryError / Java heap Space sur calcul des RF1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 65]](https://gitlab.com/ultreiaio/ird-t3/issues/65) **Exception suite à l&#39;expiration de session** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 66]](https://gitlab.com/ultreiaio/ird-t3/issues/66) **page de métadonnées d&#39;une marée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 67]](https://gitlab.com/ultreiaio/ird-t3/issues/67) **renommer champ** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 68]](https://gitlab.com/ultreiaio/ird-t3/issues/68) **Champ trip.samplestandardised pas renseigné** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 69]](https://gitlab.com/ultreiaio/ird-t3/issues/69) **Niveau 0.3 (CONVERT_CATCHES_WEIGHT_CATEGORIES) : rajouter le convertisseur pour le type &quot;banc indéterminé&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 70]](https://gitlab.com/ultreiaio/ird-t3/issues/70) **Echantillons non traités durant le N1 - améliorer le log** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 74]](https://gitlab.com/ultreiaio/ird-t3/issues/74) **Bug Niveau 1.3 (Calcul du poids des catégories dans les calées) : table wellsetallspecies non remplie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 75]](https://gitlab.com/ultreiaio/ird-t3/issues/75) **Cas d&#39;une marée partielle, non complète, mal gérée** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 77]](https://gitlab.com/ultreiaio/ird-t3/issues/77) **Rajouter un port (TAKORADI) dans l&#39;installeur du référnetiel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 79]](https://gitlab.com/ultreiaio/ird-t3/issues/79) **Mise à jour de la liste des bateaux** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 80]](https://gitlab.com/ultreiaio/ird-t3/issues/80) **Problème sur import d&#39;une base AVDTH si un code bateau est manquant dans entité t3+ &quot;Vessel&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 88]](https://gitlab.com/ultreiaio/ird-t3/issues/88) **Si une marée n&#39;a pas d&#39;activité, on ne la voit pas dans la page des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 91]](https://gitlab.com/ultreiaio/ird-t3/issues/91) **Problème lors d&#39;un import sur une base pas valide** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 92]](https://gitlab.com/ultreiaio/ird-t3/issues/92) **Rename ExtraploatedAllSetSpeciesFrequency to ExtrapolatedAllSetSpeciesFrequency** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 17]](https://gitlab.com/ultreiaio/ird-t3/issues/17) **Retour sur les calculs des niveaux 0 et 1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 76]](https://gitlab.com/ultreiaio/ird-t3/issues/76) **Ajout d&#39;une option &quot;Echantillons seulement&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 78]](https://gitlab.com/ultreiaio/ird-t3/issues/78) **Effacement correct des données lors de recalculs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 81]](https://gitlab.com/ultreiaio/ird-t3/issues/81) **Permettre d&#39;expliquer dans les logs pourquoi un échantillon n&#39;est pas sélectionné (Configuration niveau 1)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 82]](https://gitlab.com/ultreiaio/ird-t3/issues/82) **Amélioration de la progression d&#39;un traitement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 83]](https://gitlab.com/ultreiaio/ird-t3/issues/83) **Rajouter 2 champs String sur table Vessel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 86]](https://gitlab.com/ultreiaio/ird-t3/issues/86) **Ajout de 3 bateaux destinées aux bases pures Echantillons (ES notamment)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 87]](https://gitlab.com/ultreiaio/ird-t3/issues/87) **Gestion des bases AVDTH échantillons : changement de code bateau** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 89]](https://gitlab.com/ultreiaio/ird-t3/issues/89) **Créer une nouveau traitement au niveau 0 : ComputeWellPlanWeightCategoriesProportions** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 90]](https://gitlab.com/ultreiaio/ird-t3/issues/90) **Revoir la gestion des plans de cuve lors d&#39;un import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 93]](https://gitlab.com/ultreiaio/ird-t3/issues/93) **Remove obsolete data** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 94]](https://gitlab.com/ultreiaio/ird-t3/issues/94) **Add new fields on Activity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 84]](https://gitlab.com/ultreiaio/ird-t3/issues/84) **Remove spanish i18n** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.2](https://gitlab.com/ultreiaio/ird-t3/milestones/8)
&#10;&#10;*(from redmine: created on 2012-02-29)*

**Closed at 2012-03-02.**

### Download
* [Installer (t3-1.1.2.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.1.2/t3-1.1.2.zip)
* [Application (t3-1.1.2.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.1.2/t3-1.1.2.war)

### Issues
  * [[Anomalie 57]](https://gitlab.com/ultreiaio/ird-t3/issues/57) **V 1.1.1 - Erreur lors de l&#39;import des données** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 59]](https://gitlab.com/ultreiaio/ird-t3/issues/59) **transalation is not good for log book** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 60]](https://gitlab.com/ultreiaio/ird-t3/issues/60) **L&#39;ocean sélectionné n&#39;est pas pris en compte dans la configuration du niveau 1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 62]](https://gitlab.com/ultreiaio/ird-t3/issues/62) **L&#39;action supprimer une configuration level 1 ne fonctionne pas** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 63]](https://gitlab.com/ultreiaio/ird-t3/issues/63) **Session is not well serialized** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 64]](https://gitlab.com/ultreiaio/ird-t3/issues/64) **Problèmes liés à la sélection de l&#39;océan sur N0 et N1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 61]](https://gitlab.com/ultreiaio/ird-t3/issues/61) **Updates to mavenpom 3.1.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1.1](https://gitlab.com/ultreiaio/ird-t3/milestones/7)
&#10;&#10;*(from redmine: created on 2012-02-15)*

**Closed at 2012-02-21.**

### Download
* [Installer (t3-1.1.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.1.1/t3-1.1.1.zip)
* [Application (t3-1.1.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.1.1/t3-1.1.1.war)

### Issues
  * [[Anomalie 50]](https://gitlab.com/ultreiaio/ird-t3/issues/50) **org.hibernate.SessionException: Session is closed!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 53]](https://gitlab.com/ultreiaio/ird-t3/issues/53) **Modification dans le calcul des RF1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 54]](https://gitlab.com/ultreiaio/ird-t3/issues/54) **Erreur au lancement de l&#39;utilitaire d&#39;initialisation de la base, v1.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 55]](https://gitlab.com/ultreiaio/ird-t3/issues/55) **Amélioration de la gestion des logs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 56]](https://gitlab.com/ultreiaio/ird-t3/issues/56) **Ajout des liens pour voir les détails d&#39;une base depuis la page des statistiques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.1](https://gitlab.com/ultreiaio/ird-t3/milestones/6)
Correction de bugs sur la 1.0 (hors calcul)&#10;&#10;*(from redmine: created on 2012-02-06)*

**Closed at 2012-02-08.**

### Download
* [Installer (t3-1.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.1/t3-1.1.zip)
* [Application (t3-1.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.1/t3-1.1.war)

### Issues
  * [[Anomalie 41]](https://gitlab.com/ultreiaio/ird-t3/issues/41) **Import de bases de données aggrégées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 42]](https://gitlab.com/ultreiaio/ird-t3/issues/42) **Reimport de marées avec remplacement des marées existantes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 43]](https://gitlab.com/ultreiaio/ird-t3/issues/43) **Le retour du bug &quot;no suitable driver found for database...&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 45]](https://gitlab.com/ultreiaio/ird-t3/issues/45) **Problème lors de l&#39;import de marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 47]](https://gitlab.com/ultreiaio/ird-t3/issues/47) **Clic sur &quot;Tout selectionner&quot; renvoie une page quasi vide** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 49]](https://gitlab.com/ultreiaio/ird-t3/issues/49) **Calcul des rf1 : plage de validité des datepickers** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 44]](https://gitlab.com/ultreiaio/ird-t3/issues/44) **Améliorer le tableau des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 46]](https://gitlab.com/ultreiaio/ird-t3/issues/46) **Pass model to 1.1 (add index on foreign keys)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 52]](https://gitlab.com/ultreiaio/ird-t3/issues/52) **Can manage user even if no db was selected** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/ird-t3/milestones/4)
&#10;&#10;*(from redmine: created on 2011-11-28)*

**Closed at 2011-11-28.**

### Download
* [Installer (t3-1.0.2.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.0.2/t3-1.0.2.zip)
* [Application (t3-1.0.2.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.0.2/t3-1.0.2.war)

### Issues
  * [[Anomalie 31]](https://gitlab.com/ultreiaio/ird-t3/issues/31) **Can not connect to a pg database** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 32]](https://gitlab.com/ultreiaio/ird-t3/issues/32) **Updates to ToPIA 2.6.4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 33]](https://gitlab.com/ultreiaio/ird-t3/issues/33) **Updates to nuiton-web 1.7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 34]](https://gitlab.com/ultreiaio/ird-t3/issues/34) **Updates to nuiton-utils 2.3.1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 35]](https://gitlab.com/ultreiaio/ird-t3/issues/35) **Updates to jquery 3.2.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 36]](https://gitlab.com/ultreiaio/ird-t3/issues/36) **updates to selenium 2.13.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 37]](https://gitlab.com/ultreiaio/ird-t3/issues/37) **Updates to h2 1.3.162** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 38]](https://gitlab.com/ultreiaio/ird-t3/issues/38) **Updates to postgresql 9.1-901.jdbc4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 39]](https://gitlab.com/ultreiaio/ird-t3/issues/39) **Updates to sl4j 1.6.4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 40]](https://gitlab.com/ultreiaio/ird-t3/issues/40) **Updates to mavenpom 3.0.6** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/ird-t3/milestones/3)
&#10;&#10;*(from redmine: created on 2011-10-26)*

**Closed at 2011-10-31.**

### Download
* [Installer (t3-1.0.1.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.0.1/t3-1.0.1.zip)
* [Application (t3-1.0.1.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.0.1/t3-1.0.1.war)

### Issues
  * [[Anomalie 25]](https://gitlab.com/ultreiaio/ird-t3/issues/25) **L&#39;installeur ne crée pas le schéma de la base** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 26]](https://gitlab.com/ultreiaio/ird-t3/issues/26) **Corriger quelques erreurs dans la documentation technique** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 4]](https://gitlab.com/ultreiaio/ird-t3/issues/4) **Écran de détail d&#39;une marée avec statistiques** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 27]](https://gitlab.com/ultreiaio/ird-t3/issues/27) **Ajout des zones CWP 1x1** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 28]](https://gitlab.com/ultreiaio/ird-t3/issues/28) **Ajouter un filtre sur les pavillons dans la liste des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 29]](https://gitlab.com/ultreiaio/ird-t3/issues/29) **Pouvoir afficher les détail de plusieurs marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 30]](https://gitlab.com/ultreiaio/ird-t3/issues/30) **Mise en place du service de migration (version 1.0.1)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0](https://gitlab.com/ultreiaio/ird-t3/milestones/1)
Phase 2 - lot 1&#10;&#10;*(from redmine: created on 2011-07-07)*

**Closed at 2011-10-25.**

### Download
* [Installer (t3-1.0.zip)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.0/t3-1.0.zip)
* [Application (t3-1.0.war)](https://repo1.maven.org/maven2/fr/ird/t3/t3/1.0/t3-1.0.war)

### Issues
  * [[Evolution 1]](https://gitlab.com/ultreiaio/ird-t3/issues/1) **Amélioration des interfaces de traitement** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 2]](https://gitlab.com/ultreiaio/ird-t3/issues/2) **Gestion des marées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 3]](https://gitlab.com/ultreiaio/ird-t3/issues/3) **Écran pour voir l&#39;ensemble des marées en base et  permettre les suppressions en masse** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 5]](https://gitlab.com/ultreiaio/ird-t3/issues/5) **Suppression en masse des marées et/ou des données  calculées** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 6]](https://gitlab.com/ultreiaio/ird-t3/issues/6) **API de sortie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 7]](https://gitlab.com/ultreiaio/ird-t3/issues/7) **Définition de l&#39;API de sortie** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 8]](https://gitlab.com/ultreiaio/ird-t3/issues/8) **Écran de lancement d&#39;un expor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 9]](https://gitlab.com/ultreiaio/ird-t3/issues/9) **Écriture du pilote de sortie &quot;Balbaya&quot;** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 10]](https://gitlab.com/ultreiaio/ird-t3/issues/10) **Export Marée – lots commerciaux** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 11]](https://gitlab.com/ultreiaio/ird-t3/issues/11) **Export Activité – capture** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 12]](https://gitlab.com/ultreiaio/ird-t3/issues/12) **Export échantillons** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 13]](https://gitlab.com/ultreiaio/ird-t3/issues/13) **Gestion des comptes utilisateurs et des bases cibles** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 14]](https://gitlab.com/ultreiaio/ird-t3/issues/14) **Utiliser une base autonome pour le stockage des  utilisateurs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 15]](https://gitlab.com/ultreiaio/ird-t3/issues/15) **Page de sélection de la base de données T3+ à utiliser** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 18]](https://gitlab.com/ultreiaio/ird-t3/issues/18) **Calcul des efforts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 21]](https://gitlab.com/ultreiaio/ird-t3/issues/21) **Gestion de l&#39;historisation des zones** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 22]](https://gitlab.com/ultreiaio/ird-t3/issues/22) **Mécanisme d&#39;historisation des zones** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 23]](https://gitlab.com/ultreiaio/ird-t3/issues/23) **Généralisation de l&#39;utilisation du mécanisme d&#39;historisation dans les niveaux 2 et 3** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 24]](https://gitlab.com/ultreiaio/ird-t3/issues/24) **Ajout des zones ZEE et FAO** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

