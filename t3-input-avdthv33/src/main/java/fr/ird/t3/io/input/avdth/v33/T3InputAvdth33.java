/*
 * #%L
 * T3 :: Input AVDTH v 33
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.avdth.v33;

import com.google.common.collect.ImmutableSet;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.io.input.T3Input;
import fr.ird.t3.io.input.access.AbstractT3InputMSAccess;
import fr.ird.t3.io.input.access.T3AccessDataSource;
import fr.ird.t3.io.input.access.T3DataEntityVisitor;

import java.io.File;

/**
 * The implementation of the {@link T3Input} for AVDTH databases.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3InputAvdth33 extends AbstractT3InputMSAccess {

    private static final ImmutableSet<T3EntityEnum> REFERENCE_TYPES =
            ImmutableSet.of(
                    T3EntityEnum.VesselActivity,
                    T3EntityEnum.VesselSizeCategory,
                    T3EntityEnum.VesselType,
                    T3EntityEnum.Country,
                    T3EntityEnum.Vessel,
                    T3EntityEnum.Species,
                    T3EntityEnum.WeightCategoryLanding,
                    T3EntityEnum.SchoolType,
                    T3EntityEnum.FishingContext,
                    T3EntityEnum.Ocean,
                    T3EntityEnum.Harbour,
                    T3EntityEnum.SampleQuality,
                    T3EntityEnum.SampleType,
                    T3EntityEnum.WeightCategoryLogBook,
                    T3EntityEnum.WellDestiny,
                    T3EntityEnum.WeightCategoryWellPlan);
    /**
     * Set of all importable data types from ms-access.
     *
     * @since 2.0
     */
    private static final ImmutableSet<T3EntityEnum> IMPORTABLE_DATA_TYPES =
            ImmutableSet.of(
                    T3EntityEnum.Activity,
                    T3EntityEnum.ActivityFishingContext,
                    T3EntityEnum.ElementaryCatch,
                    T3EntityEnum.ElementaryLanding,
                    T3EntityEnum.Sample,
                    T3EntityEnum.SampleSet,
                    T3EntityEnum.SampleSpecies,
                    T3EntityEnum.SampleSpeciesFrequency,
                    T3EntityEnum.Trip,
                    T3EntityEnum.Well,
                    T3EntityEnum.WellPlan);

    @Override
    public T3AccessDataSource newDataSource(File inputFile) {
        return new T3AccessDataSource(inputFile, T3AccessEntityMetaProviderAvdth33.class);
    }

    @Override
    public T3DataEntityVisitor newDataVisitor(T3AccessDataSource dataSource, ReferenceEntityMap safeReferences) {
        return new T3DataEntityVisitorAvdth33(dataSource, safeReferences, getConfiguration());
    }

    @Override
    public ImmutableSet<T3EntityEnum> getReferenceTypes() {
        return REFERENCE_TYPES;
    }

    @Override
    public ImmutableSet<T3EntityEnum> getDataTypes() {
        return IMPORTABLE_DATA_TYPES;
    }
}
