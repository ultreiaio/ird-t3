/*
 * #%L
 * T3 :: Input AVDTH v 33
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.avdth.v33;

import com.google.auto.service.AutoService;
import fr.ird.t3.io.input.T3Input;
import fr.ird.t3.io.input.T3InputProvider;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.io.File;

/**
 * Provides input for avdth.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@AutoService(T3InputProvider.class)
public class T3InputProviderAvdth33 implements T3InputProvider {

    private static final long serialVersionUID = 1L;
    private static final Version VERSION = Versions.valueOf("3.3");
    private static final String NAME = "AVDTH";
    public static final String ID = NAME + "__" + VERSION;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getName() {
        return "AVDTH";
    }

    @Override
    public Version getVersion() {
        return VERSION;
    }

    @Override
    public String getInputType() {
        return "MS-ACCESS";
    }

    @Override
    public String getLabel() {
        return String.format("%s - v.%s (%s)", getName(), getVersion(), getInputType());
    }

    @Override
    public T3Input newInstance(File inputFile) {
        return new T3InputAvdth33();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof T3InputProvider)) {
            return false;
        }
        T3InputProvider that = (T3InputProvider) o;
        return ID.equals(that.getId());
    }

    @Override
    public int hashCode() {
        return ID.hashCode();
    }
}
