/*
 * #%L
 * T3 :: Input AVDTH v 35
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.avdth.v35;

import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.io.input.T3InputConfiguration;
import fr.ird.t3.io.input.access.T3AccessDataSource;
import fr.ird.t3.io.input.access.T3AvdthDataEntityVisitor;
import fr.ird.t3.io.input.access.T3DataEntityVisitor;

/**
 * Implementation of {@link T3DataEntityVisitor} for the avdth v35 db.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
class T3DataEntityVisitorAvdth35 extends T3AvdthDataEntityVisitor {

    T3DataEntityVisitorAvdth35(T3AccessDataSource dataSource, ReferenceEntityMap referentiel, T3InputConfiguration configuration) {
        super(dataSource, referentiel, configuration);
    }
}
