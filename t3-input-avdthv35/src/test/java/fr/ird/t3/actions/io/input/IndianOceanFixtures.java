package fr.ird.t3.actions.io.input;

/*-
 * #%L
 * T3 :: Input AVDTH v 35
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.io.input.test.OceanFixtures;

/**
 * Created by tchemit on 10/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum IndianOceanFixtures implements OceanFixtures {

    OI_1998(183, 1, 183, 1),
    OI_1999(55, 65, 195, 2),
    OI_2000(55, 65, 220, 0),
    OI_2001(70, 118, 188, 0),
    OI_2002(103, 125, 227, 1),
    OI_2003(83, 136, 217, 2),
    OI_2004(78, 118, 196, 0),
    OI_2005(78, 110, 187, 1),
    OI_2006(181, 12, 193, 0),
    OI_2007(155, 12, 167, 0),
    OI_2008(146, 34, 180, 0),
    OI_2009(122, 19, 138, 3),
    OI_2010(112, 0, 112, 0),
    OI_2011(141, 0, 126, 0),
    OI_2012(145, 0, 119, 0),
    OI_2013(124, 0, 124, 0),
    OI_2014(149, 0, 149, 1),
    OI_2015(111, 1, 147, 0),
    OI_2016(111, 1, 169, 0),
    OI_2017(111, 1, 168, 4);

    private final int nbSafe;
    private final int nbUnsafe;
    private final int nbSafeWithoutWell;
    private final int nbUnsafeWithoutWell;

    IndianOceanFixtures(int nbSafe, int nbUnsafe) {
        this(nbSafe, nbUnsafe, nbSafe, nbUnsafe);
    }

    IndianOceanFixtures(int nbSafe, int nbUnsafe, int nbSafeWithoutWell, int nbUnsafeWithoutWell) {
        this.nbSafe = nbSafe;
        this.nbUnsafe = nbUnsafe;
        this.nbSafeWithoutWell = nbSafeWithoutWell;
        this.nbUnsafeWithoutWell = nbUnsafeWithoutWell;
    }

    public int nbSafe() {
        return nbSafe;
    }

    public int nbUnsafe() {
        return nbUnsafe;
    }

    public int nbSafeWithoutWell() {
        return nbSafeWithoutWell;
    }

    public int nbUnsafeWithoutWell() {
        return nbUnsafeWithoutWell;
    }

}
