package fr.ird.t3.actions.io.input;

/*-
 * #%L
 * T3 :: Input AVDTH v 35
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.io.input.test.OceanFixtures;

/**
 * Created by tchemit on 10/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum AtlanticOceanFixtures implements OceanFixtures {


    OA_1960(151, 0),
    OA_1961(159, 0),
    OA_1962(203, 0),
    OA_1963(807, 0),
    OA_1964(709, 0),
    OA_1965(911, 0),
    OA_1966(942, 0),
    OA_1967(993, 0),
    OA_1968(1339, 0),
    OA_1969(453, 1179),
    OA_1970(289, 970),
    OA_1971(146, 1047),
    OA_1972(143, 1069),
    OA_1973(235, 890),
    OA_1974(254, 948),
    OA_1975(248, 709),
    OA_1976(149, 692),
    OA_1977(40, 761),
    OA_1978(56, 766),
    OA_1979(76, 618),
    OA_1980(324, 299),
    OA_1981(365, 280),
    OA_1982(416, 247),
    OA_1983(343, 257),
    OA_1984(297, 111),
    OA_1985(325, 115),
    OA_1986(214, 121),
    OA_1987(186, 165),
    OA_1988(212, 156),
    OA_1989(192, 154),
    OA_1990(141, 192),
    OA_1991(268, 0),
    OA_1992(250, 0),
    OA_1993(281, 2),
    OA_1994(278, 0),
    OA_1995(243, 3),
    OA_1996(90, 142),
    OA_1997(36, 178),
    OA_1998(206, 1),
    OA_1999(326, 2),
    OA_2000(318, 36, 352, 2),
    OA_2001(298, 63, 358, 3),
    OA_2002(184, 133, 316, 1),
    OA_2003(218, 157, 369, 6),
    OA_2004(121, 61, 181, 1),
    OA_2005(93, 31, 124, 0),
    OA_2006(70, 27, 97, 0),
    OA_2007(53, 27, 77, 3),
    OA_2008(46, 14, 60, 0),
    OA_2009(64, 21, 86, 0),
    OA_2010(75, 13, 87, 0),
    OA_2011(75, 13, 91, 1),
    OA_2012(75, 13, 97, 0),
    OA_2013(75, 13, 103, 0),
    OA_2014(75, 13, 90, 0),
    OA_2015(75, 13, 103, 0),
    OA_2016(75, 13, 52, 55);

    private final int nbSafe;
    private final int nbUnsafe;
    private final int nbSafeWithoutWell;
    private final int nbUnsafeWithoutWell;

    AtlanticOceanFixtures(int nbSafeWithoutWell, int nbUnsafeWithoutWell) {
        this(nbSafeWithoutWell, nbUnsafeWithoutWell, nbSafeWithoutWell, nbUnsafeWithoutWell);
    }

    AtlanticOceanFixtures(int nbSafe, int nbUnsafe, int nbSafeWithoutWell, int nbUnsafeWithoutWell) {
        this.nbSafe = nbSafe;
        this.nbUnsafe = nbUnsafe;
        this.nbSafeWithoutWell = nbSafeWithoutWell;
        this.nbUnsafeWithoutWell = nbUnsafeWithoutWell;
    }

    public int nbSafe() {
        return nbSafe;
    }

    public int nbUnsafe() {
        return nbUnsafe;
    }

    public int nbSafeWithoutWell() {
        return nbSafeWithoutWell;
    }

    public int nbUnsafeWithoutWell() {
        return nbUnsafeWithoutWell;
    }

}
