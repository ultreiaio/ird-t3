package fr.ird.t3.actions.io.input;

/*-
 * #%L
 * T3 :: Input AVDTH v 35
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.io.input.test.AnalyzeInputSourceActionTestSupport;
import fr.ird.t3.actions.io.input.test.MSAccessTestConfiguration;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.io.input.avdth.v35.T3InputProviderAvdth35;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by tchemit on 04/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Ignore
public class AnalyzeInputSourceActionTest extends AnalyzeInputSourceActionTestSupport {

    @Override
    protected MSAccessTestConfiguration createConfiguration() {
        return new MSAccessTestConfiguration("testExecute_([^_]*)_(\\d+)(_.+)*", "%s_%s_V35.mdb", T3InputProviderAvdth35.ID);
    }

    @Test
    public void testExecute_OA_2000() throws Exception {
        testExecute(AtlanticOceanFixtures.OA_2000);
    }

    @Test
    public void testExecute_OA_1980() throws Exception {
        testExecute(AtlanticOceanFixtures.OA_1980, TripType.LOGBOOKMISSING);
    }

    @Test
    public void testExecute_OI_2000() throws Exception {
        testExecute(IndianOceanFixtures.OI_2000);
    }

}
