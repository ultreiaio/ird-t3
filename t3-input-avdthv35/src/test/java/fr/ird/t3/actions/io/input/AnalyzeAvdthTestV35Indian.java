package fr.ird.t3.actions.io.input;

/*-
 * #%L
 * T3 :: Input AVDTH v 35
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.actions.io.input.test.AnalyzeAvdthTestSupport;
import fr.ird.t3.actions.io.input.test.MSAccessTestConfiguration;
import fr.ird.t3.entities.data.TripType;
import fr.ird.t3.io.input.avdth.v35.T3InputProviderAvdth35;
import org.junit.Ignore;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tchemit on 08/03/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Ignore
public class AnalyzeAvdthTestV35Indian extends AnalyzeAvdthTestSupport {

    @Parameterized.Parameters
    public static List<IndianOceanFixtures> data() {
        return Arrays.asList(IndianOceanFixtures.values());
    }

    @Override
    protected MSAccessTestConfiguration createConfiguration() {
        MSAccessTestConfiguration configuration = new MSAccessTestConfiguration("testExecute_([^_]*)_(\\d+)(_.+)*", "%s_%s_V35.mdb", T3InputProviderAvdth35.ID);
        configuration.setUseWells(false);
        configuration.setTripType(TripType.LOGBOOKMISSING);
        return configuration;
    }
}
