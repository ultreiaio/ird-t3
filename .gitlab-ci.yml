stages:
  - Docker
  - Build
  - Test
  - Release

image: registry.gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/docker:latest

.stage_docker: &stage_docker
  stage: Docker
.stage_build: &stage_build
  stage: Build
.stage_test: &stage_test
  stage: Test
.stage_release: &stage_release
  stage: Release

.create-docker: &create-docker
  environment:
    name: docker
  image: registry.gitlab.com/ultreiaio-infra/docker/docker:latest
  services:
    - docker:dind
  script:
    - if [ -n "${CREATE_DOCKER}" ]; then ultreiaio-docker-generate-image gitlab-ci-token $CI_BUILD_TOKEN; fi

.build-for-release: &build-for-release
  environment:
    name: test
  script:
    - if [ -n "${BUILD_FOR_RELEASE}" ]; then ultreiaio-maven-execute 'clean verify -U -DperformRelease -Psnapshot'; fi

.deploy-snapshot: &deploy-snapshot
  environment:
    name: snapshot
  script:
    - if [ -n "${DEPLOY_SNAPSHOT}" ]; then ultreiaio-maven-execute 'clean deploy -am -pl t3-domain -DperformRelease -Psnapshot'; fi

.publish-site: &publish-site
  environment:
    name: site
  script:
    - if [ -n "${PUBLISH_SITE}" ]; then ultreiaio-site-publish; fi

.make-release: &make-release
  environment:
    name: release
  script:
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-release-gitlab-init; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-milestone-close; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-release-start; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-release-finish; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-stage-close-and-release; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-changelog-update; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-milestone-create; fi

.make-stage: &make-stage
  environment:
    name: release
  script:
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-release-gitlab-init; fi
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-milestone-close; fi
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-release-start; fi
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-release-finish; fi
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-stage-close; fi
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-changelog-update-staging; fi
    - if [ -n "${MAKE_STAGE}" ]; then ultreiaio-milestone-create; fi

.deploy-demo: &deploy-demo
  environment:
    name: demo
  script:
    - if [ -n "${DEPLOY_DEMO}" ]; then ultreiaio-war-deploy-latest-to-demo 't3-web'; fi

###################################################################################################
### TRIGGERS JOBS                                                                               ###
###################################################################################################

.triggers: &triggers
  only:
    - triggers

trigger-create-docker:
  <<: *stage_docker
  <<: *triggers
  <<: *create-docker

trigger-build-for-release:
  <<: *stage_build
  <<: *triggers
  <<: *build-for-release

trigger-deploy-snapshot:
  <<: *stage_release
  <<: *triggers
  <<: *deploy-snapshot

trigger-deploy-demo:
  <<: *stage_test
  <<: *triggers
  <<: *deploy-demo

trigger-make-release:
  <<: *stage_release
  <<: *triggers
  <<: *make-release

trigger-make-stage:
  <<: *stage_release
  <<: *triggers
  <<: *make-stage

###################################################################################################
### AUTOMATIC JOBS                                                                              ###
###################################################################################################

.automatic: &automatic
  only:
    - develop
  except:
    - triggers

build:
  <<: *stage_build
  <<: *automatic
  script:
    - ultreiaio-maven-execute 'clean install -U -DskipITs'

build-for-release:
  <<: *stage_test
  <<: *automatic
  <<: *build-for-release
  before_script:
    - export BUILD_FOR_RELEASE=true

###################################################################################################
### AUTOMATIC RELEASE JOBS                                                                      ###
###################################################################################################

.automatic-release: &automatic-release
  only:
    - tags

release-create-docker:
  <<: *stage_docker
  <<: *automatic-release
  <<: *create-docker
  before_script:
    - export CREATE_DOCKER=true

release-publish-site:
  <<: *stage_build
  <<: *automatic-release
  <<: *publish-site
  before_script:
    - export PUBLISH_SITE=true

release-deploy-demo:
  <<: *stage_build
  <<: *automatic-release
  environment:
    name: demo
  script:
    - ultreiaio-war-deploy-to-demo 't3-web'

###################################################################################################
### MANUAL JOBS                                                                                 ###
###################################################################################################

.manual: &manual
  only:
    - develop
  when: manual

create-docker:
  <<: *stage_docker
  <<: *manual
  <<: *create-docker
  before_script:
    - export CREATE_DOCKER=true

deploy-snapshot:
  <<: *stage_release
  <<: *manual
  <<: *deploy-snapshot
  before_script:
    - export DEPLOY_SNAPSHOT=true

make-release:
  <<: *stage_release
  <<: *manual
  <<: *make-release
  before_script:
    - export MAKE_RELEASE=true

make-stage:
  <<: *stage_release
  <<: *manual
  <<: *make-stage
  before_script:
    - export MAKE_STAGE=true

publish-site:
  <<: *stage_release
  <<: *manual
  <<: *publish-site
  before_script:
    - export PUBLISH_SITE=true

deploy-demo:
  <<: *stage_release
  <<: *manual
  <<: *deploy-demo
  before_script:
    - export DEPLOY_DEMO=true
