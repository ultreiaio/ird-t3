/*
 * #%L
 * T3 :: Output Balbaya v 32
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSet;
import fr.ird.t3.entities.data.SampleSetSpeciesCatWeight;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.WeightCategorySample;
import org.apache.commons.collections.MapUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.l;

/**
 * Execution of operation {@link T3OutputOperationBalbayaImpl#TRIP_AND_LANDING}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class BalbayaOperationExecutionSampleImpl extends AbstractBalbayaOperationExecution {

    private static final String CHECK_SAMPLE = "SELECT count(*) FROM echant where c_bat = %1$s AND d_dbq = '%2$s'::date AND n_ech = %3$s AND id_cal = %4$s;";
    /**
     * To insert a new line into echant table (from Sample and SampleSet entity).
     * <p/>
     * c_bat        | numeric(4,0) | not null
     * d_dbq        | date         | not null
     * n_ech        | numeric(5,0) | not null
     * id_cal       | numeric(2,0) | not null
     * d_act        | date         | not null
     * c_qz_act     | numeric(2,0) | not null
     * c_ocea       | numeric(2,0) | not null
     * c_port       | numeric(3,0) | not null
     * v_la_act     | numeric(5,2) | not null
     * v_lo_act     | numeric(5,2) | not null
     * q_act        | numeric(1,0) |
     * cwp11_act    | numeric(7,0) |
     * cwp55_act    | numeric(7,0) |
     * c_tban       | numeric(2,0) | not null
     * c_strate_ech | numeric(1,0) | not null
     * c_typ_ech    | numeric(2,0) | not null
     * c_qual_ech   | numeric(2,0) | not null
     * v_p_tot_ech  | numeric(8,4) | not null
     * v_p_tot_str  | numeric(8,4) | not null
     * v_rf_tot     | numeric(8,4) | not null
     * v_rf_m10     | numeric(8,4) | not null
     * v_rf_p10     | numeric(8,4) | not null
     * id_jeu_d     | numeric(4,0) | not null
     * c_pays_d     | numeric(3,0) | not null
     * the_geom     | geometry     |
     */
    private static final String INSERT_ECHANT = "INSERT INTO echant (" +
            "c_bat, " +
            "d_dbq," +
            "n_ech," +
            "id_cal, " +
            "d_act, " +
            "c_qz_act, " +
            "c_ocea, " +
            "c_port, " +
            "v_la_act, " +
            "v_lo_act, " +
            "q_act, " +
            "c_tban, " +
            "c_strate_ech, " +
            "c_typ_ech, " +
            "c_qual_ech, " +
            "v_p_tot_ech, " +
            "v_p_tot_str, " +
            "v_rf_tot, " +
            "v_rf_m10, " +
            "v_rf_p10, " +
            "id_jeu_d," +
            "c_pays_d" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s," +
            "'%5$s'::date," +
            "%6$s," +
            "%7$s," +
            "%8$s," +
            "%9$s," +
            "%10$s," +
            "%11$s," +
            "%12$s," +
            "%13$s," +
            "%14$s," +
            "%15$s," +
            "%16$s," +
            "%17$s," +
            "%18$s," +
            "%19$s," +
            "%20$s," +
            "%21$s," +
            "%22$s" +
            ");";
    /**
     * To insert a new line into ech_freqt (from SampleSetSpecieFrequency entity).
     * <p/>
     * c_bat  | numeric(4,0) | not null
     * d_dbq  | date         | not null
     * n_ech  | numeric(5,0) | not null
     * id_cal | numeric(2,0) | not null
     * c_esp  | numeric(3,0) | not null
     * v_long | numeric(3,0) | not null
     * v_eff  | numeric(7,2) | not null
     */
    private static final String INSERT_ECH_FREQT = "INSERT INTO ech_freqt (" +
            "c_bat, " +
            "d_dbq," +
            "n_ech," +
            "id_cal, " +
            "c_esp, " +
            "v_long, " +
            "v_eff" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s," +
            "%5$s," +
            "%6$s," +
            "%7$s" +
            ");";
    /**
     * To insert a new line into ech_esp (from sampleSetSpeciesFrequency and
     * SampleSetSpeciesCatWeight entity).
     * <p/>
     * c_bat       | numeric(4,0) | not null
     * d_dbq       | date         | not null
     * n_ech       | numeric(5,0) | not null
     * id_cal      | numeric(2,0) | not null
     * c_esp       | numeric(3,0) | not null
     * v_nb_total  | numeric(7,2) | not null
     * v_p_esp_ech | numeric(8,4) | not null
     * v_p_esp_str | numeric(8,4) | not null
     */
    private static final String INSERT_ECH_ESP = "INSERT INTO ech_esp (" +
            "c_bat, " +
            "d_dbq," +
            "n_ech," +
            "id_cal, " +
            "c_esp, " +
            "v_nb_total, " +
            "v_p_esp_ech," +
            "v_p_esp_str" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s," +
            "%5$s," +
            "%6$s," +
            "%7$s," +
            "%8$s" +
            ");";
    /**
     * To insert a new line into pp_eecp (from SampleSetSpeciesCatWeight entity).
     * <p/>
     * c_bat     | numeric(4,0) | not null
     * d_dbq     | date         | not null
     * n_ech     | numeric(5,0) | not null
     * id_cal    | numeric(2,0) | not null
     * c_esp     | numeric(3,0) | not null
     * c_cat_p   | numeric(2,0) | not null
     * v_pp_eecp | numeric(9,4) | not null
     */
    private static final String INSERT_PP_EECP = "INSERT INTO pp_eecp (" +
            "c_bat, " +
            "d_dbq," +
            "n_ech," +
            "id_cal, " +
            "c_esp, " +
            "c_cat_p, " +
            "v_pp_eecp" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s," +
            "%5$s," +
            "%6$s," +
            "%7$s" +
            ");";
    private int nbEchant;
    private int nbPPEECP;
    private int nbEchEsp;
    private int nbEchFreqt;

    BalbayaOperationExecutionSampleImpl(Connection connection) {
        super(connection, T3OutputOperationBalbayaImpl.SAMPLE);
    }

    @Override
    protected boolean checkDatas(T3OutputBalbayaImpl output, List<Trip> trips) throws SQLException {
        // check pKeys before all
        for (Trip trip : trips) {
            if (trip.isSampleNotEmpty()) {
                int tripVesselCode = trip.getVessel().getCode();
                Date tripLandingDate = trip.getLandingDate();
                for (Sample sample : trip.getSample()) {
                    int sampleNumber = sample.getSampleNumber();
                    if (sample.isSampleSetNotEmpty()) {
                        for (SampleSet sampleSet : sample.getSampleSet()) {
                            Activity activity = sampleSet.getActivity();
                            int activityNumber = activity.getNumber();
                            boolean exists = checkPKey(CHECK_SAMPLE, tripVesselCode, tripLandingDate, sampleNumber, activityNumber);
                            if (exists) {
                                // this trip already exists, can not import it, stop operation
                                Locale locale = output.getLocale();
                                output.getMessager().addErrorMessage(
                                        l(locale, "t3.output.balbaya.error.sample.already.exists",
                                                tripVesselCode,
                                                tripLandingDate,
                                                sampleNumber,
                                                activityNumber));
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    protected void buildRequests(T3OutputBalbayaImpl output, T3OutputBalbayaImpl.TreatmentId id, List<Trip> trips) {
        nbEchFreqt = nbEchant = nbPPEECP = nbEchEsp = 0;
        int countryCode = id.getCountryCode();
        int treatmentNumber = id.getNumber();
        Map<WeightCategorySample, Integer> weightCategoryTreatmentMapping = output.getWeightCategorySampleMapping();
        for (Trip trip : trips) {
            if (!trip.isSampleEmpty()) {
                int tripVesselCode = trip.getVessel().getCode();
                Date tripLandingDate = trip.getLandingDate();
                int tripLandingHarbourCode = trip.getLandingHarbour().getCode();
                for (Sample sample : trip.getSample()) {
                    int sampleNumber = sample.getSampleNumber();
                    int sampleTypeCode = sample.getSampleType().getCode();
                    int sampleQualityCode = sample.getSampleQuality().getCode();
                    if (!sample.isSampleSetEmpty()) {
                        for (SampleSet sampleSet : sample.getSampleSet()) {
                            // add sample Well
                            Activity activity = sampleSet.getActivity();
                            int activityNumber = activity.getNumber();
                            float sampleWellTotalWeight = 0f;
                            if (!activity.isSetSpeciesCatWeightEmpty()) {
                                for (SetSpeciesCatWeight setSpeciesCatWeight : activity.getSetSpeciesCatWeight()) {
                                    sampleWellTotalWeight += setSpeciesCatWeight.getWeight();
                                }
                            }
                            nbEchant++;
                            addRequest(INSERT_ECHANT,
                                    requests,
                                    tripVesselCode,
                                    tripLandingDate,
                                    sampleNumber,
                                    activityNumber,
                                    activity.getDate(),
                                    activity.getFortnight(),
                                    activity.getOcean().getCode(),
                                    tripLandingHarbourCode,
                                    activity.getLatitude(),
                                    activity.getLongitude(),
                                    activity.getQuadrant(),
                                    activity.getSchoolType().getCode(),
                                    1,
                                    sampleTypeCode,
                                    sampleQualityCode,
                                    sampleWellTotalWeight,
                                    sampleSet.getWeightedWeight(),
                                    sampleSet.getRfTot(),
                                    sampleSet.getRfMinus10(),
                                    sampleSet.getRfPlus10(),
                                    treatmentNumber,
                                    countryCode);

                            // will contains for each species sum of his number from SampleSetSpeciesFrequency
                            Map<Integer, Float> speciesNumber = new TreeMap<>();
                            // will contains for each species sum of his weight from SampleSetSpeciesCatWeight
                            Map<Integer, Float> speciesWeight = new TreeMap<>();
                            if (!sampleSet.isSampleSetSpeciesFrequencyEmpty()) {
                                for (SampleSetSpeciesFrequency sampleSetSpeciesFrequency : sampleSet.getSampleSetSpeciesFrequency()) {
                                    nbEchFreqt++;
                                    // add sample frequency
                                    int speciesCode = sampleSetSpeciesFrequency.getSpecies().getCode();
                                    Float number = sampleSetSpeciesFrequency.getNumber();
                                    addValueInMap(speciesNumber, speciesCode, number);
                                    addRequest(INSERT_ECH_FREQT,
                                            requests,
                                            tripVesselCode,
                                            tripLandingDate,
                                            sampleNumber,
                                            activityNumber,
                                            speciesCode,
                                            sampleSetSpeciesFrequency.getLfLengthClass(),
                                            number);
                                }
                            }
                            if (!sampleSet.isSampleSetSpeciesCatWeightEmpty()) {
                                for (SampleSetSpeciesCatWeight sampleSetSpeciesCatWeight : sampleSet.getSampleSetSpeciesCatWeight()) {
                                    nbPPEECP++;
                                    // add sample weight
                                    int speciesCode = sampleSetSpeciesCatWeight.getSpecies().getCode();
                                    float weight = sampleSetSpeciesCatWeight.getWeight();
                                    addValueInMap(speciesWeight, speciesCode, weight);
                                    addRequest(INSERT_PP_EECP,
                                            requests,
                                            tripVesselCode,
                                            tripLandingDate,
                                            sampleNumber,
                                            activityNumber,
                                            speciesCode,
                                            weightCategoryTreatmentMapping.get(sampleSetSpeciesCatWeight.getWeightCategorySample()),
                                            weight);
                                }
                            }

                            if (MapUtils.isNotEmpty(speciesNumber)) {
                                for (Integer speciesCode : speciesNumber.keySet()) {
                                    nbEchEsp++;
                                    addRequest(INSERT_ECH_ESP,
                                            requests,
                                            tripVesselCode,
                                            tripLandingDate,
                                            sampleNumber,
                                            activityNumber,
                                            speciesCode,
                                            speciesNumber.get(speciesCode),
                                            speciesWeight.get(speciesCode),
                                            0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    protected String getSuccessSummary(T3OutputBalbayaImpl output, T3OutputBalbayaImpl.TreatmentId id) {
        return l(output.getLocale(), "t3.output.balbabya.operation.sample.success",
                id.getNumber(), nbEchant, nbEchFreqt, nbPPEECP, nbEchEsp);
    }

    private void addValueInMap(Map<Integer, Float> map, int key, Float value) {
        if (value != null) {
            Float aFloat = map.get(key);
            if (aFloat == null) {
                aFloat = 0f;
            }
            map.put(key, aFloat + value);
        }
    }
}
