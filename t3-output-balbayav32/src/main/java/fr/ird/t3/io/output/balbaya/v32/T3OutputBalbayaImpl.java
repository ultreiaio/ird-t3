/*
 * #%L
 * T3 :: Output Balbaya v 32
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import com.google.common.base.Charsets;
import fr.ird.t3.T3CsvUtil;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.entities.reference.WeightCategorySampleTopiaDao;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.io.output.T3Output;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.services.T3ServiceSupport;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntityById;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The implementation of {@link T3Output} for the balbaya database.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class T3OutputBalbayaImpl extends T3ServiceSupport implements T3Output<T3OutputOperationBalbayaImpl, T3OutputConfiguration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3OutputBalbayaImpl.class);

    protected final T3OutputConfiguration configuration;

    private final T3Messager messager;

    @InjectEntityById(entityType = Country.class)
    protected Country fleet;

    @InjectEntityById(entityType = Ocean.class)
    protected Ocean ocean;
    @InjectDAO(entityType = WeightCategorySample.class)
    private WeightCategorySampleTopiaDao weightCategorySampleDAO;

    private Map<WeightCategorySample, Integer> weightCategorySampleMapping;

    T3OutputBalbayaImpl(T3OutputConfiguration configuration, T3Messager messager) {
        this.configuration = configuration;
        this.messager = messager;
    }

    @Override
    public T3OutputConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public T3Messager getMessager() {
        return messager;
    }

    @Override
    public String executeOperation(List<Trip> trips, T3OutputOperationBalbayaImpl operation) throws Exception {
        // prepare the sql connection.
        try (Connection connection = initConnection(configuration)) {
            log.info("Starts operation " + operation + " with connection " + connection);
            AbstractBalbayaOperationExecution execution = operation.newExecution(connection);
            return execution.execute(trips, this);

        }
    }

    private Connection initConnection(T3OutputConfiguration configuration) {
        try {
            Connection conn = T3EntityHelper.newJDBCConnection(configuration);
            conn.setAutoCommit(false);
            return conn;
        } catch (SQLException e) {
            throw new IllegalStateException(String.format("Can not connect to %s with login %s", configuration.getUrl(), configuration.getLogin()), e);
        }
    }

    public Map<WeightCategorySample, Integer> getWeightCategorySampleMapping() {
        if (weightCategorySampleMapping == null) {
            weightCategorySampleMapping = new HashMap<>();
            List<WeightCategorySample> categories = weightCategorySampleDAO.findAll();
            Map<String, WeightCategorySample> categoriesById = T3EntityHelper.splitByTopiaId(categories);
            T3CsvUtil.AbstractT3ImportExportModel<WeightCategoryMapping> importModel = new WeightCategoryMappingImportModel(categoriesById);
            try (Reader reader = new InputStreamReader(getClass().getResourceAsStream("/weightCategories.csv"), Charsets.UTF_8)) {
                try (Import<WeightCategoryMapping> importer = Import.newImport(importModel, reader)) {
                    for (WeightCategoryMapping bean : importer) {
                        weightCategorySampleMapping.put(bean.getWeightCategorySample(), bean.getBalbayaCatCode());
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("Could not import mapping file", e);
            }
        }
        return weightCategorySampleMapping;
    }

    public int getFleetCode() {
        return fleet.getCode();
    }

    public int getOceanCode() {
        return ocean.getCode();
    }

    public static class TreatmentId {

        protected final int countryCode;

        protected final int oceanCode;

        protected final int number;

        protected final int type;

        protected final String comment;

        protected final String origin;

        TreatmentId(int number,
                    int type,
                    String comment,
                    String origin,
                    int countryCode,
                    int oceanCode) {
            this.number = number;
            this.type = type;
            this.comment = comment;
            this.origin = origin;
            this.countryCode = countryCode;
            this.oceanCode = oceanCode;
        }

        public int getNumber() {
            return number;
        }

        public int getType() {
            return type;
        }

        public String getComment() {
            return comment;
        }

        public String getOrigin() {
            return origin;
        }

        public int getCountryCode() {
            return countryCode;
        }

        public int getOceanCode() {
            return oceanCode;
        }
    }

    /**
     * To map the t3 {@link WeightCategoryTreatment} to a balbaya cat code.
     *
     * @author Tony Chemit - dev@tchemit.fr
     * @since 1.4
     */
    public static class WeightCategoryMapping {

        public static final String PROPERTY_WEIGHT_CATEGORY_SAMPLE = "weightCategorySample";

        public static final String PROPERTY_BALBAYA_CAT_CODE = "balbayaCatCode";

        protected WeightCategorySample weightCategorySample;

        protected int balbayaCatCode;

        public WeightCategorySample getWeightCategorySample() {
            return weightCategorySample;
        }

        public void setWeightCategorySample(WeightCategorySample weightCategorySample) {
            this.weightCategorySample = weightCategorySample;
        }

        public int getBalbayaCatCode() {
            return balbayaCatCode;
        }

        public void setBalbayaCatCode(int balbayaCatcode) {
            this.balbayaCatCode = balbayaCatcode;
        }
    }

    public class WeightCategoryMappingImportModel extends T3CsvUtil.AbstractT3ImportExportModel<WeightCategoryMapping> {

        WeightCategoryMappingImportModel(Map<String, WeightCategorySample> caracteristicMap) {
            super(';');
            newForeignKeyColumn(WeightCategoryMapping.PROPERTY_WEIGHT_CATEGORY_SAMPLE,
                    WeightCategorySample.class,
                    WeightCategorySample.PROPERTY_TOPIA_ID,
                    caracteristicMap);

            newMandatoryColumn(WeightCategoryMapping.PROPERTY_BALBAYA_CAT_CODE,
                    T3CsvUtil.PRIMITIVE_INTEGER);
        }

        @Override
        public WeightCategoryMapping newEmptyInstance() {
            return new WeightCategoryMapping();
        }
    }
}
