/*
 * #%L
 * T3 :: Output Balbaya v 32
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityFishingContext;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.WeightCategorySample;
import org.nuiton.topia.persistence.TopiaException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Execution of operation {@link T3OutputOperationBalbayaImpl#ACTIVITY_AND_CATCHES}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class BalbayaOperationExecutionActivityImpl extends AbstractBalbayaOperationExecution {

    public static final String CHECK_ACTIVITY = "SELECT count(*) FROM activite where " +
            "c_bat = %1$s AND d_act = '%2$s'::date AND n_act = %3$s;";
    /**
     * To insert a new line into activite table (from Activity entity).
     * <p/>
     * c_bat          | numeric(4,0) | not null
     * d_act          | date         | not null
     * n_act          | numeric(4,0) | not null
     * h_act          | numeric(2,0) |
     * c_qz_act       | numeric(2,0) | not null
     * c_trim_act     | numeric(1,0) | not null
     * v_la_act       | numeric(5,2) | not null
     * v_lo_act       | numeric(5,2) | not null
     * c_ocea         | numeric(2,0) | not null
     * q_act          | numeric(1,0) |
     * cwp11_act      | numeric(7,0) |
     * cwp55_act      | numeric(7,0) |
     * c_zet          | numeric(4,0) |
     * c_zee          | numeric(2,0) |
     * id_zfao        | numeric(7,0) |
     * c_tban         | numeric(2,0) | not null
     * v_tmer         | numeric(9,3) | not null
     * v_tpec         | numeric(9,3) | not null
     * v_nb_calees    | numeric(2,0) | not null
     * v_nb_calee_pos | numeric(2,0) | not null
     * v_dur_cal      | numeric(9,3) | not null
     * c_qual_act     | numeric(2,0) |
     * v_temp_s       | numeric(5,2) |
     * v_cour_dir     | numeric(3,0) |
     * v_cour_vit     | numeric(5,2) |
     * d_dbq          | date         | not null
     * c_port         | numeric(3,0) | not null
     * id_jeu_d       | numeric(4,0) | not null
     * c_pays_d       | numeric(3,0) | not null
     * c_opera        | numeric(2,0) |
     * the_geom       | geometry     |
     */
    public static final String INSERT_ACTIVITE = "INSERT INTO activite (" +
            "c_bat, " +
            "d_act," +
            "n_act," +
            "h_act, " +
            "c_qz_act," +
            "c_trim_act," +
            "v_la_act," +
            "v_lo_act," +
            "c_ocea," +
            "q_act," +
            "c_tban," +
            "v_tmer," +
            "v_tpec," +
            "v_nb_calees," +
            "v_nb_calee_pos," +
            "v_dur_cal," +
            "c_qual_act," +
            "v_temp_s," +
            "v_cour_dir," +
            "v_cour_vit," +
            "d_dbq," +
            "c_port," +
            "id_jeu_d," +
            "c_pays_d," +
            "c_opera," +
            "rf3" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s," +
            "%5$s," +
            "%6$s," +
            "%7$s," +
            "%8$s," +
            "%9$s," +
            "%10$s," +
            "%11$s," +
            "%12$s," +
            "%13$s," +
            "%14$s," +
            "%15$s," +
            "%16$s," +
            "%17$s," +
            "%18$s," +
            "%19$s," +
            "%20$s," +
            "'%21$s'::date," +
            "%22$s," +
            "%23$s," +
            "%24$s," +
            "%25$s," +
            "%26$s" +
            ");";
    /**
     * To insert a new line into capture table (from ElementaryCatch entity).
     * <p/>
     * c_bat        | numeric(4,0) | not null
     * d_act        | date         | not null
     * n_act        | numeric(4,0) | not null
     * c_esp        | numeric(3,0) | not null
     * c_cat_p      | numeric(2,0) | not null
     * v_poids_capt | numeric(9,4) | not null
     */
    public static final String INSERT_CAPTURE = "INSERT INTO capture (" +
            "c_bat, " +
            "d_act," +
            "n_act," +
            "c_esp, " +
            "c_cat_p," +
            "v_poids_capt" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s," +
            "%5$s," +
            "%6$s" +
            ");";
    /**
     * To insert a new line into act_assoc table (from ActivityFishingContext entity).
     * <p/>
     * c_bat   | numeric(4,0) | not null
     * d_act   | date         | not null
     * n_act   | numeric(4,0) | not null
     * c_assoc | numeric(2,0) | not null
     */
    public static final String INSERT_ACT_ASSOC = "INSERT INTO act_assoc (" +
            "c_bat, " +
            "d_act," +
            "n_act," +
            "c_assoc" +
            ") VALUES(" +
            "%1$s," +
            "'%2$s'::date," +
            "%3$s," +
            "%4$s" +
            ");";
    protected int nbActivities;
    protected int nbCorrectedElementaryCatches;

    public BalbayaOperationExecutionActivityImpl(Connection connection) {
        super(connection, T3OutputOperationBalbayaImpl.ACTIVITY_AND_CATCHES);
    }

    @Override
    protected boolean checkDatas(T3OutputBalbayaImpl output,
                                 List<Trip> trips) throws SQLException {

        // check pKeys before all
        for (Trip trip : trips) {

            if (!trip.isActivityEmpty()) {

                int tripVesselCode = trip.getVessel().getCode();

                for (Activity activity : trip.getActivity()) {

                    int activityNumber = activity.getNumber();

                    Date activityDate = activity.getDate();

                    boolean exists = checkPKey(CHECK_ACTIVITY,
                            tripVesselCode,
                            activityDate,
                            activityNumber);

                    if (exists) {

                        // this trip already exists, can not import it, stop operation
                        Locale locale = output.getLocale();
                        output.getMessager().addErrorMessage(
                                l(locale, "t3.output.balbaya.error.activity.already.exists",
                                        tripVesselCode, activityDate, activityNumber)
                        );
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    protected void buildRequests(T3OutputBalbayaImpl output,
                                 T3OutputBalbayaImpl.TreatmentId id,
                                 List<Trip> trips) throws TopiaException {

        nbActivities = nbCorrectedElementaryCatches = 0;
        int countryCode = id.getCountryCode();
        int treatmentNumber = id.getNumber();

        Map<WeightCategorySample, Integer> weightCategoryTreatmentMapping = output.getWeightCategorySampleMapping();

        for (Trip trip : trips) {

            if (!trip.isActivityEmpty()) {

                int tripVesselCode = trip.getVessel().getCode();
                Date tripLandingDate = trip.getLandingDate();
                int tripLandingHarbourCode = trip.getLandingHarbour().getCode();
                Float rf2 = trip.getRf2();

                for (Activity activity : trip.getActivity()) {

                    int activityNumber = activity.getNumber();

                    Date activityDate = activity.getDate();

                    nbActivities++;
                    // add activity
                    addRequest(INSERT_ACTIVITE,
                            requests,
                            tripVesselCode,
                            activityDate,
                            activityNumber,
                            activity.getHour(),
                            activity.getFortnight(),
                            activity.getTrimester(),
                            activity.getLatitude(),
                            activity.getLongitude(),
                            activity.getOcean().getCode(),
                            activity.getQuadrant(),
                            activity.getSchoolType().getCode(),
                            activity.getTimeAtSea(),
                            activity.getFishingTime(),
                            activity.getSetCount(),
                            activity.getPositiveSetCount(),
                            activity.getSetDuration(),
                            9, // force c_qual_act to 9
                            activity.getSurfaceTemperature(),
                            activity.getCurrentDirection(),
                            activity.getCurrentVelocity(),
                            tripLandingDate,
                            tripLandingHarbourCode,
                            treatmentNumber,
                            countryCode,
                            activity.getVesselActivity().getCode(),
                            rf2
                    );

                    if (activity.isCorrectedElementaryCatchNotEmpty()) {

                        for (CorrectedElementaryCatch aCatch : activity.getCorrectedElementaryCatch()) {

//                            FIXME Something is wrong here, not using correct weight category
                            // obtain category code for balbalya
                            int weightCategoryCode = weightCategoryTreatmentMapping.get(aCatch.getWeightCategoryTreatment());

                            nbCorrectedElementaryCatches++;
                            // add elementary catch
                            addRequest(INSERT_CAPTURE,
                                    requests,
                                    tripVesselCode,
                                    activityDate,
                                    activityNumber,
                                    aCatch.getSpecies().getCode(),
                                    weightCategoryCode,
                                    aCatch.getCorrectedCatchWeight()
                            );
                        }
                    }

                    if (activity.isActivityFishingContextNotEmpty()) {

                        // to collect distinct codes (in balbaya it must be unique)
                        Set<Integer> codes = new HashSet<>();

                        for (ActivityFishingContext activityFishingContext : activity.getActivityFishingContext()) {

                            int code = activityFishingContext.getFishingContext().getCode();
                            if (!codes.add(code)) {

                                addRequest(INSERT_ACT_ASSOC,
                                        requests,
                                        tripVesselCode,
                                        activityDate,
                                        activityNumber,
                                        code
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    protected String getSuccessSummary(T3OutputBalbayaImpl output,
                                       T3OutputBalbayaImpl.TreatmentId id) {
        Locale locale = output.getLocale();
        return l(locale, "t3.output.balbabya.operation.activityAndCatches.success",
                id.getNumber(), nbActivities, nbCorrectedElementaryCatches);
    }


}
