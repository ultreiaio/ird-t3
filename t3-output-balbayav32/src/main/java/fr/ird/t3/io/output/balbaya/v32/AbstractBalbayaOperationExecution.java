/*
 * #%L
 * T3 :: Output Balbaya v 32
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.data.Trip;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Abstract operation execution.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public abstract class AbstractBalbayaOperationExecution {

    /**
     * To insert a new line into a_jeu_d table .
     * <p/>
     * id_jeu_d    | numeric(4,0)            | not null
     * c_pays_d    | numeric(3,0)            | not null
     * c_ocea      | numeric(2,0)            | not null
     * c_typ_d     | numeric(2,0)            | not null
     * d_deb_d     | date                    | not null
     * d_fin_d     | date                    | not null
     * l_fic_d     | character varying(128)  | not null
     * l_org_fic_d | character varying(256)  | not null
     * d_dispo     | date                    | not null
     * l_com_ins   | character varying(1024) | not null
     * d_suppr     | date                    |
     * l_com_suppr | character varying(1024) |
     */
    private static final String INSERT_A_JEU_D = "INSERT INTO a_jeu_d (" +
            "id_jeu_d, " +
            "c_typ_d," +
            "l_com_ins," +
            "c_pays_d, " +
            "c_ocea," +
            "d_deb_d," +
            "d_fin_d," +
            "l_fic_d," +
            "l_org_fic_d," +
            "d_dispo" +
            ") VALUES(" +
            "%1$s," +
            "%2$s," +
            "'%3$s'," +
            "%4$s," +
            "%5$s," +
            "'%6$s'::date," +
            "'%7$s'::date," +
            "'%8$s'," +
            "'%9$s'," +
            "'%10$s'::date" +
            ");";
    private static final Log log = LogFactory.getLog(AbstractBalbayaOperationExecution.class);
    protected final Connection connection;
    protected final T3OutputOperationBalbayaImpl operation;
    final List<String> requests;

    AbstractBalbayaOperationExecution(Connection connection,
                                      T3OutputOperationBalbayaImpl operation) {
        this.connection = connection;
        this.operation = operation;
        this.requests = new ArrayList<>();
    }

    public String execute(List<Trip> trips, T3OutputBalbayaImpl output) throws Exception {
        String summary;
        boolean check = checkDatas(output, trips);
        Locale locale = output.getLocale();
        if (!check) {
            // stop operation
            summary = l(locale, "t3.output.balbaya.error.export.checkData");
        } else {
            T3Messager messager = output.getMessager();
            // create new treatment id
            T3OutputBalbayaImpl.TreatmentId treatmentId = createNewTreatmentEntry(output);
            messager.addInfoMessage(l(locale, "t3.output.balbaya.created_id", treatmentId.getType(), treatmentId.getNumber()));
            buildRequests(output, treatmentId, trips);
            try {
                for (String request : requests) {
                    try (PreparedStatement sta = connection.prepareStatement(request)) {
                        log.info("Execute request " + request);
                        messager.addInfoMessage(request);
                        sta.executeUpdate();
                    }
                }
                // commit statements
                connection.commit();
                summary = getSuccessSummary(output, treatmentId);
            } catch (Exception e) {
                try {
                    // rollback statements
                    connection.rollback();
                } finally {
                    log.error(String.format("Could not execute operation %s for reason %s", operation, e.getMessage()));
                    messager.addErrorMessage(e.getMessage());
                }
                summary = l(locale, "t3.output.balbaya.error.export.exception", e.getMessage());
            }
        }


        return summary;
    }

    protected abstract boolean checkDatas(T3OutputBalbayaImpl output,
                                          List<Trip> trips) throws SQLException;

    protected abstract void buildRequests(T3OutputBalbayaImpl output, T3OutputBalbayaImpl.TreatmentId treatmentId, List<Trip> trips);

    protected abstract String getSuccessSummary(T3OutputBalbayaImpl output, T3OutputBalbayaImpl.TreatmentId id);

    /**
     * @param output output operation
     * @return the treatment request with new freshly id.
     * @throws SQLException if any sql error while asking a treatment id
     */
    private T3OutputBalbayaImpl.TreatmentId createNewTreatmentEntry(T3OutputBalbayaImpl output) throws SQLException {
        T3OutputBalbayaImpl.TreatmentId id = newTreatmentId(output);
        addRequest(INSERT_A_JEU_D,
                requests,
                id.getNumber(),
                id.getType(),
                id.getComment(),
                output.getFleetCode(),
                output.getOceanCode(),
                output.getConfiguration().getBeginDate().toBeginSqlDate(),
                output.getConfiguration().getEndDate().toEndSqlDate(),
                id.getOrigin(),
                id.getComment(),
                new java.sql.Date(new Date().getTime()));
        return id;
    }

    private T3OutputBalbayaImpl.TreatmentId newTreatmentId(T3OutputBalbayaImpl output) throws SQLException {
        // must obtain a new treatment id from the sql connection
        T3OutputBalbayaImpl.TreatmentId result;
        //fixme Use the currentValue + 1 and then only pass sequence to next val when everything is ok
        try (PreparedStatement ps = connection.prepareStatement("SELECT nextval('a_jeu_seq');")) {
            int treatmentNumber;
            try (ResultSet resultSet = ps.executeQuery()) {
                resultSet.next();
                treatmentNumber = resultSet.getInt(1);
            }
            result = new T3OutputBalbayaImpl.TreatmentId(
                    treatmentNumber,
                    operation.getTreatmentId(),
                    "Export T3+  " + operation.getLibelle(output.getLocale()),
                    "T3+ v" + output.getApplicationConfiguration().getApplicationVersion(),
                    output.getFleetCode(),
                    output.getOceanCode());
        }
        return result;
    }

    void addRequest(String format, List<String> requests, Object... parameters) {
        String request = String.format(format, parameters);
        requests.add(request);
        log.debug(request);
    }

    boolean checkPKey(String checkPattern, Object... params) throws SQLException {

        String sql = String.format(checkPattern, params);
        try (PreparedStatement sta = connection.prepareStatement(sql)) {
            log.info("Execute request " + sql);
            sta.execute();
            try (ResultSet resultSet = sta.getResultSet()) {
                resultSet.next();
                int nbResult = resultSet.getInt(1);
                return nbResult > 0;
            }
        }
    }
}
