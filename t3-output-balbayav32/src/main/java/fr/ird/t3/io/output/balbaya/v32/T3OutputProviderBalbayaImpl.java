/*
 * #%L
 * T3 :: Output Balbaya v 32
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import com.google.auto.service.AutoService;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Provider of {@link T3OutputBalbayaImpl}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@AutoService(T3OutputProvider.class)
public class T3OutputProviderBalbayaImpl implements T3OutputProvider<T3OutputOperationBalbayaImpl, T3OutputConfiguration> {

    private static final long serialVersionUID = 1L;

    public static final Version VERSION = Versions.valueOf("3.2");

    public static final String NAME = "balbaya";

    public static final String ID = NAME + "__" + VERSION;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Version getVersion() {
        return VERSION;
    }

    @Override
    public String getOutputType() {
        return "SQL";
    }

    @Override
    public String getLibelle() {
        return getName() + " - v." + getVersion() + " (" + getOutputType() + ")";
    }

    @Override
    public T3OutputBalbayaImpl newInstance(T3OutputConfiguration configuration,
                                           T3Messager messager,
                                           T3ServiceContext serviceContext) {
        T3OutputBalbayaImpl output = new T3OutputBalbayaImpl(configuration,
                                                             messager
        );
        output.setServiceContext(serviceContext);

        return output;
    }

    @Override
    public T3OutputOperationBalbayaImpl[] getOperations() {
        return T3OutputOperationBalbayaImpl.values();
    }

    @Override
    public T3OutputOperationBalbayaImpl getOperation(String operationId) {
        T3OutputOperationBalbayaImpl result = null;
        for (T3OutputOperationBalbayaImpl operation : getOperations()) {
            if (operationId.equals(operation.getId())) {
                result = operation;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof T3OutputProvider)) {
            return false;
        }

        T3OutputProviderBalbayaImpl that = (T3OutputProviderBalbayaImpl) o;

        return ID.equals(that.getId());

    }

    @Override
    public int hashCode() {
        return ID.hashCode();
    }

}
