package fr.ird.t3.io.output.balbaya.v32;

/*
 * #%L
 * T3 :: Output Balbaya v 32
 * %%
 * Copyright (C) 2010 - 2018 IRD, Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ird.t3.FakeT3ServiceContext;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.services.IOCService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class T3OutputBalbayaImplTest implements T3Messager {

    @Rule
    public final FakeT3ServiceContext serviceContext = new FakeT3ServiceContext(true);
    private T3OutputBalbayaImpl pilot;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        boolean initOk = serviceContext.isInitOk();
        Assume.assumeTrue("Could not init db", initOk);
        T3OutputProvider outputProvider = new T3OutputProviderBalbayaImpl();
        // get output configuration
        T3OutputConfiguration outputConfiguration = new T3OutputConfiguration();
        // instantiate the output pilot from the provider
        pilot = (T3OutputBalbayaImpl) outputProvider.newInstance(outputConfiguration, this, serviceContext);
        // inject in output pilot
        serviceContext.newService(IOCService.class).injectExcept(pilot);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void getWeightCategoryTreatmentMapping() throws TopiaException {

        Map<WeightCategorySample, Integer> mapping = pilot.getWeightCategorySampleMapping();

        Assert.assertNotNull(mapping);
        Assert.assertEquals(4, mapping.size());

    }

    @Override
    public void addInfoMessage(String message) {
    }

    @Override
    public void addWarningMessage(String message) {
    }

    @Override
    public void addErrorMessage(String message) {
    }

    @Override
    public List<String> getInfoMessages() {
        return null;
    }

    @Override
    public List<String> getWarnMessages() {
        return null;
    }

    @Override
    public List<String> getErrorMessages() {
        return null;
    }
}
